<h3>You have a new Contact via the Contact form</h3>
<div>
	<p><strong>Name:</strong> {{ $name }}</p>
	{{ $comment }}
</div>
<p>Sent via {{ $email }}</p>