<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .wrapper {
            width: 700px;
            overflow: hidden;
            margin: 0 auto;
            border: 1px solid #ddd;
        }
        .section {
            padding: 15px;
            overflow: hidden;
        }
        .header.section {
            border-bottom: 1px solid #ddd;
            background: #eee;
            text-align: center;
        }
        .header.section img {
            width: 100px;
            height: auto;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }
        th {
            background: #3498db;
            color: white;
            font-weight: bold;
        }
        td, th {
            padding: 10px;
            border: 1px solid #ccc;
            text-align: left;
            font-size: 18px;
        }

    </style>
</head>
<body>
<div class="wrapper">
    <div class="header section">
        <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="">
    </div>
    <div class="section table">
        <h3>Order Summary</h3>
        @if($order_type == 'orders')
            <table>
                <tr>
                    <th>ID</th>
                    <th>Customer Info</th>
                    <th>Total</th>
                    <th>Subtotal</th>
                    <th>Date</th>
                </tr>
                <?php $date = new DateTime($order_details->order_date); ?>
                <tr>
                    <td>{{$order_details->id}}</td>
                    <td>{{$order_details->name}}<br>{{$order_details->phone}}</td>
                    <td>৳ {{number_format($order_details->total, 2)}}</td>
                    <td>৳ {{number_format($order_details->subtotal, 2)}}</td>
                    <td>{{$date->format('d-F-Y')}}</td>
                </tr>
            </table>
        @endif
        @if($order_type == 'invoice')
            <table>
                <tr>
                    <th>ID</th>
                    <th>Customer Info</th>
                    <th>Total Amount</th>
                    <th>Date</th>
                </tr>
                <?php $date = new DateTime($order_details->modify_time); ?>
                <tr>
                    <td>{{$order_details->id}}</td>
                    <td>{{$order_details->name}}<br>{{$order_details->phone}}</td>
                    <td>৳ {{number_format($order_details->total_amount, 2)}}</td>
                    <td>{{$date->format('d-F-Y')}}</td>
                </tr>
            </table>
        @endif
    </div>
    <div class="section table">
        <h3>Order Items</h3>
        <table>
            <tr>
                <th>#</th>
                <th>Shop</th>
                <th>Product</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Subtotal</th>
            </tr>
            <?php $i = 0; ?>
            @foreach($order_items as $row)
                <?php $i++; ?>
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$row->store_name}}</td>
                    <td>{{$row->product_name}}</td>
                    <td>৳ {{number_format($row->price, 2)}}</td>
                    <td>{{$row->quantity}}</td>
                    <td>৳ {{number_format($row->sub_total, 2)}}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
</body>
</html>