@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block text-center">
                <h2>404 Not Found</h2>
            </div>
        </div>
    </div>
</div>
@endsection