@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Price Group List</h4>
                <div class="back-button">
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#add_price_group_modal"><i class="fa fa-plus"></i>Add Price Group</a>
                </div>
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($price_group_list) > 0)
                            
                            <?php $i = 0; ?>
                            @foreach($price_group_list as $row)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Name">{{ $row->name }}</td>
                                <td data-title="Action">
                                    <a href="#" class="btn btn-success" data-toggle="modal" data-target=".edit_price_group_modal" data-id="{{$row->id}}" data-name="{{ $row->name }}"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure want to delete?');" href="{{ url('/admin/delete-price-group/'.$row->id) }}"><i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@include('backend.price_groups.add')
@include('backend.price_groups.edit')

<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    });
</script>
@endsection