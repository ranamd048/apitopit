@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Edit Store</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/store-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                @if (Session::has('message')) 
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                <div class="form">
                    <form class="form-horizontal" action="{{ url('/admin/update_store') }}" method="POST" enctype="multipart/form-data">
                    	{{ csrf_field() }}
                        <input type="hidden" name="store_id" value="{{ $store->id }}">
                        <input type="hidden" name="existing_baner_img" value="{{ $store->baner_image }}">
                        <input type="hidden" name="existing_logo" value="{{ $store->logo }}">
                        <!-- <input type="hidden" name="shop_type" value="{{ $store->shop_type }}"> -->
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group row">
                                    <?php $col = '12'; ?>
                                    @if(count($shop_categories) > 0)
                                    <?php $col = '6'; ?>
                                    <div class="col-md-{{$col}}">
                                        <label for="">Shop Category *</label>
                                        <select name="shop_category" class="form-control" required="">
                                            <option value="">Select Shop Category</option>
                                            @foreach ($shop_categories as $row)
                                                <option {{ ($store->shop_category_id == $row->id) ? 'selected' : '' }} value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                    <div class="col-sm-{{$col}}">
                                        <label for="">Store Type *</label>
                                        <select name="shop_type" class="form-control">
                                            <option {{ ($store->shop_type == 'shop') ? 'selected' : '' }} value="shop">Shop</option>
                                            <option {{ ($store->shop_type == 'service') ? 'selected' : '' }} value="service">Service</option>
                                            <option {{ ($store->shop_type == 'both') ? 'selected' : '' }} value="both">Shop & Service</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Store Name *</label>
                                        <input type="text" name="store_name" class="form-control {{ ($errors->has('store_name')) ? 'is-invalid' : '' }}" required="" value="{{ $store->store_name }}">
                                        @if($errors->has('store_name'))
                                        <div class="invalid-feedback">
                                          {{ $errors->first('store_name') }}
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Update Baner<!-- <span class="image_size">(Size: 900 x 350 Max: 1024kb)</span>--></label>
                                        
                                        <input type="file" name="baner_image" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Update Logo <!--<span class="image_size">(Size: 75 x 75)</span>--></label>

                                        <input type="file" name="logo" class="form-control">
                                    </div>
                                </div>
                                <?php $divisions = Helpers::getDivisionList(); ?>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label" for="">Division</label>
                                        <select name="division" id="division_list" class="form-control">
                                            <option value="">Select Division</option>
                                            @foreach($divisions as $row)
                                            <option @if($store->division == $row->id) selected @endif value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label" for="">District</label>
                                        <select name="district" id="district_list" class="form-control">
                                            <option value="{{$store->district}}">{{Helpers::districtName($store->district)}}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label" for="">Upazila</label>
                                        <select name="thana" id="upazila_list" class="form-control">
                                        <option value="{{$store->thana}}">{{Helpers::upazilaName($store->thana)}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Email</label>
                                        <input type="email" name="email" value="{{ $store->email }}" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Phone Number</label>
                                        <input type="text" name="phone" value="{{ $store->phone }}" class="form-control {{ ($errors->has('phone')) ? 'is-invalid' : '' }}">
                                        @if($errors->has('phone'))
                                        <div class="invalid-feedback">
                                          {{ $errors->first('phone') }}
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Address</label>
                                        <input type="text" name="address" value="{{ $store->address }}" class="form-control {{ ($errors->has('address')) ? 'is-invalid' : '' }}">
                                        @if($errors->has('address'))
                                        <div class="invalid-feedback">
                                          {{ $errors->first('address') }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">TIN Number</label>
                                        <input type="text" name="tin_no" value="{{ $store->tin_no }}" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Serial Number</label>
                                        <input type="text" name="serial_no" value="{{ $store->serial_no }}" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">License Number</label>
                                        <input type="text" name="license_no" value="{{ $store->license_no }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Min Order Amount for Shipping</label>
                                        <input type="text" name="min_order_amount" value="{{ $store->min_order_amount }}" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Own District Shipping</label>
                                        <input type="text" name="shipping_cost" value="{{ $store->shipping_cost }}" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Other District Shipping</label>
                                        <input type="text" name="other_shipping_cost" value="{{ $store->other_shipping_cost }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Show Phone Number</label>
                                        <select name="show_phone_number" class="form-control">
                                            <option {{ ($store->show_phone_number == 1) ? 'selected' : '' }} value="1">Yes</option>
                                            <option {{ ($store->show_phone_number == 0) ? 'selected' : '' }} value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Opening/Closing Time</label>
                                        <input type="text" name="opening_time" value="{{ $store->opening_time }}" class="form-control">
                                    </div>
                                    <?php
                                      $close_days = explode(',', $store->close_day);
                                    ?>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Select Close Day</label>
                                        <select name="close_day[]" class="form-control" multiple>
                                            <option {{ in_array('Friday', $close_days) ? 'selected' : '' }} value="Friday">Friday</option>
                                            <option {{ in_array('Sataurday', $close_days) ? 'selected' : '' }} value="Sataurday">Sataurday</option>
                                            <option {{ in_array('Sunday', $close_days) ? 'selected' : '' }} value="Sunday">Sunday</option>
                                            <option {{ in_array('Monday', $close_days) ? 'selected' : '' }} value="Monday">Monday</option>
                                            <option {{ in_array('Tuesday', $close_days) ? 'selected' : '' }} value="Tuesday">Tuesday</option>
                                            <option {{ in_array('Wednesday', $close_days) ? 'selected' : '' }} value="Wednesday">Wednesday</option>
                                            <option {{ in_array('Thursday', $close_days) ? 'selected' : '' }} value="Thursday">Thursday</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label">Facebook Link</label>
                                        <input type="text" name="fb_link" value="{{ $store->fb_link }}" class="form-control">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label">Youtube Link</label>
                                        <input type="text" name="youtube_link" value="{{ $store->youtube_link }}" class="form-control">
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label">Map Link</label>
                                        <input type="text" name="maps_iframe" value="{{ $store->maps_iframe }}" class="form-control">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label">Promo Video Link</label>
                                        <input type="text" name="video_link" value="{{ $store->video_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label">Map Latitude</label>
                                        <input type="text" name="map_lat" value="{{ $store->map_lat }}" class="form-control">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label">Map Longitude</label>
                                        <input type="text" name="map_long" value="{{ $store->map_long }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Due SMS Text</label>
                                        <textarea name="due_sms_text" class="form-control">{{ $store->due_sms_text }}</textarea>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Note</label>
                                        <textarea name="note" class="form-control">{{ $store->note }}</textarea>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Home Delivery</label>
                                        <select name="home_delivery" class="form-control">
                                            <option {{ ($store->home_delivery == 'Yes') ? 'selected' : '' }} value="Yes">Yes</option>
                                            <option {{ ($store->home_delivery == 'No') ? 'selected' : '' }} value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <?php 
                                    if(Session::get('role') == 0) {
                                        $col = '6';
                                    } else {
                                        $col = '4';
                                    }
                                ?>
                                    <div class="col-sm-{{$col}}">
                                        <label for="">Show Price</label>
                                        <select name="show_price" class="form-control">
                                            <option {{ ($store->show_price == 1) ? 'selected' : '' }} value="1">Yes</option>
                                            <option {{ ($store->show_price == 0) ? 'selected' : '' }} value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-{{$col}}">
                                        <label for="">Show Stock</label>
                                        <select name="show_stock" class="form-control">
                                            <option {{ ($store->show_stock == 1) ? 'selected' : '' }} value="1">Yes</option>
                                            <option {{ ($store->show_stock == 0) ? 'selected' : '' }} value="0">No</option>
                                        </select>
                                    </div>
                                    @if(Session::get('role') == 1)
                                    <div class="col-sm-{{$col}}">
                                        <label for="">Remaining SMS</label>
                                        <input type="text" readonly="" class="form-control" value="{{$store->sms_limit}}">
                                    </div>
                                    @endif
                                </div>
                                @if(Session::get('role') == 0)
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Product Upload Qty</label>
                                        <input type="text" name="product_qty" value="{{ $store->product_qty }}" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Product Image Qty</label>
                                        <input type="text" name="product_image" value="{{ $store->product_image }}" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label col-form-label">Service Upload Qty</label>
                                        <input type="text" name="service_qty" value="{{ $store->service_qty }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="">Image Upload Required?</label>
                                        <select name="image_required" class="form-control">
                                            <option {{ ($store->image_required == 1) ? 'selected' : '' }} value="1">Yes</option>
                                            <option {{ ($store->image_required == 0) ? 'selected' : '' }} value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Product Upload Permission</label>
                                        <select name="upload_permission" class="form-control">
                                            <option {{ ($store->upload_permission == 1) ? 'selected' : '' }} value="1">Yes</option>
                                            <option {{ ($store->upload_permission == 0) ? 'selected' : '' }} value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Featured?</label>
                                        <select name="is_featured" class="form-control">
                                            <option {{ ($store->is_featured == 1) ? 'selected' : '' }} value="1">Yes</option>
                                            <option {{ ($store->is_featured == 0) ? 'selected' : '' }} value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="">Status</label>
                                        <select name="status" class="form-control">
                                            <option {{ ($store->status == 1) ? 'selected' : '' }} value="1">Active</option>
                                            <option {{ ($store->status == 0) ? 'selected' : '' }} value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="">Print Type</label>
                                        <select name="print_type" class="form-control">
                                            <option {{ ($store->print_type == 'a4') ? 'selected' : '' }} value="a4">A4 Size</option>
                                            <option {{ ($store->print_type == 'thermal') ? 'selected' : '' }} value="thermal">Thermal Size</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="">SMS Limit</label>
                                        <input type="number" class="form-control" name="sms_limit" value="{{$store->sms_limit}}"> 
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="control-label col-form-label">Expire Date </label>
                                        <input type="date" name="expiry_date" class="form-control" value="{{ $store->expiry_date }}">
                                    </div>
                                </div>
                                @endif
                                
                                <div lass="form-group row">
                                    <button type="submit" class="btn btn-info btn-default">Update</button>
                                </div>
                            </div>
                        </div>
                    	
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@if(Session::get('role') == 1 && Session::has('store_id'))
<?php 
    $products = DB::table('products')->select('id', 'product_name')->where('store_id', Session::get('store_id'))->get();
?>
<!--Send offer sms Modal-->
<div class="modal fade" id="offer_sms_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <form action="{{ url('admin/send_offer_sms') }}" method="POST">
        {{csrf_field()}}
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Send Offer SMS to All Customer</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="quantity">Select Product:</label>
                <select class="form-control" name="product_name" required="">
                    <option value="">Select Offer Product</option>
                    @foreach($products as $row)
                        <option value="{{ $row->product_name }}">{{ $row->product_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="quantity">Offer Price:</label>
                <input type="text" name="offer_price" class="form-control" required="">
            </div>
            <div class="form-group">
                <label for="quantity">Offer SMS Text: <span style="font-size: 13px;">(After sms text will show product name and price)</span></label>
                <textarea name="offer_text" class="form-control"></textarea>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
      </form>
    </div>
</div>
@endif

<script>
        $(document).ready(function() {
            //get district/upazila
            function get_selected_district_upazila(id, type) {
                var url = "{{ url('get_district_upazila') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, type: type, data_format: 'html'},
                    cache: false,
                    success: function (response) {
                        $('#'+type).html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            $('#division_list').change(function() {
                var division_id = $(this).val();
                if(division_id == '') {
                    $('#district_list').html('<option value="">Select District</option>');
                } else {
                    get_selected_district_upazila(division_id, 'district_list');
                }
                $('#upazila_list').html('<option value="">Select Upazila</option>');
            });

            $('#district_list').change(function() {
                var district_id = $(this).val();
                get_selected_district_upazila(district_id, 'upazila_list');
            });
        });
    </script>
@endsection