@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Add New Store</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/store-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    <form class="form-horizontal" action="{{ url('/admin/save_store') }}" method="POST" enctype="multipart/form-data">
                    	{{ csrf_field() }}
                        <div class="row">

                            <div class="col-md-12">
                            @if(count($shop_categories) > 0)
                                <div class="form-group">
                                    <label for="">Shop Category *</label>
                                    <select name="shop_category" class="form-control" required="">
                                        <option value="">Select Shop Category</option>
                                        @foreach ($shop_categories as $row)
                                            <option value="{{$row->id}}">{{$row->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif

                                <div class="form-group">
                                    <label for="">Store Type *</label>
                                    <select name="shop_type" class="form-control">
                                        <option value="shop">Shop</option>
                                        <option value="service">Service</option>
                                        <option value="both">Shop & Service</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Store Name *</label>
                                    <input type="text" class="form-control {{ ($errors->has('store_name')) ? 'is-invalid' : '' }}" name="store_name">
                                    @if($errors->has('store_name'))
                                    <div class="invalid-feedback">
                                      {{ $errors->first('store_name') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Store Baner Image *<!-- <span class="image_size">(Size: 900 x 350 Max: 1024kb)</span>--></label>
                                    <input type="file" class="form-control {{ ($errors->has('baner_image')) ? 'is-invalid' : '' }}" name="baner_image">
                                    @if($errors->has('baner_image'))
                                    <div class="invalid-feedback">
                                      {{ $errors->first('baner_image') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Store Logo <!--<span class="image_size">(Size: 75 x 75)</span>--></label>
                                    <input type="file" class="form-control {{ ($errors->has('store_logo')) ? 'is-invalid' : '' }}" name="store_logo">
                                    @if($errors->has('store_logo'))
                                    <div class="invalid-feedback">
                                      {{ $errors->first('store_logo') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Store Address *</label>
                                    <input type="text" class="form-control {{ ($errors->has('address')) ? 'is-invalid' : '' }}" name="address">
                                    @if($errors->has('address'))
                                    <div class="invalid-feedback">
                                      {{ $errors->first('address') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Propiter Name *</label>
                                    <input type="text" class="form-control {{ ($errors->has('name')) ? 'is-invalid' : '' }}" name="name">
                                    @if($errors->has('name'))
                                    <div class="invalid-feedback">
                                      {{ $errors->first('name') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="">Phone * (Ex: 01779221842)</label>
                                    <input type="text" class="form-control {{ ($errors->has('phone')) ? 'is-invalid' : '' }}" name="phone">
                                    @if($errors->has('phone'))
                                    <div class="invalid-feedback">
                                      {{ $errors->first('phone') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Password *</label>
                                    <input type="password" class="form-control {{ ($errors->has('password')) ? 'is-invalid' : '' }}" name="password">
                                    @if($errors->has('password'))
                                    <div class="invalid-feedback">
                                      {{ $errors->first('password') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Confirm Password *</label>
                                    <input type="password" class="form-control {{ ($errors->has('password_confirmation')) ? 'is-invalid' : '' }}" name="password_confirmation">
                                    @if($errors->has('password_confirmation'))
                                    <div class="invalid-feedback">
                                      {{ $errors->first('password_confirmation') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    	
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection