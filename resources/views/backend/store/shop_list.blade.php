<div id="responsive_table">          
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Phone</th>
                <th>Baner</th>
                <th>Upload</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($store_list) > 0)
            <?php $i = 0; ?>
            @foreach($store_list as $row)
            <?php $i++; 
                if($row->status == 1) {
                    $status = '<span class="badge badge-success">Active</span>';
                } else {
                    $status = '<span class="badge badge-danger">Inactive</span>';
                }
                if($row->upload_permission == 1) {
                    $product_upload = '<span class="badge badge-success">Yes</span>';
                } else {
                    $product_upload = '<span class="badge badge-danger">No</span>';
                }
            ?>
            <tr>
                <td data-title="Sl">{{ $i }}</td>
                <td data-title="Name">{{ $row->store_name }}</td>
                <td data-title="Type">{{ $row->shop_type }}</td>
                <td data-title="Phone">{{ $row->phone }}</td>
                <td data-title="Baner"><img src="{{ asset('/public/uploads/store/'.$row->baner_image) }}" width="100" alt=""></td>
                <td data-title="Upload"><?php echo $product_upload; ?></td>
                <td data-title="Status"><?php echo $status; ?></td>
                <td data-title="Action">
                    <a class="btn btn-success" href="{{ url('/admin/edit-store/'.$row->id) }}"><i class="fa fa-pencil"></i>
                    </a>
                    <!-- <a class="btn btn-danger" href="{{ url('/admin/delete-store/'.$row->id) }}" onclick="return confirm('Do you want to delete this shop?')"><i class="fa fa-trash"></i>
                    </a> -->
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".password_check_modal" data-id="{{$row->id}}" data-for="stores"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>