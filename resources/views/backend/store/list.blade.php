@extends('backend.layout')

@section('maincontent')
<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Store List</h4>
                <div class="back-button">
                    @if(Session::get('role') == 0)
                        <a href="{{ url('admin/number_export/stores/phone') }}" class="btn btn-success"><i class="fa fa-download"></i>Number Export</a>
                    @endif
                    <a href="{{ url('admin/create-store') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Store</a>
                </div>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <div class="product-filter">
                            <?php $divisions = Helpers::getDivisionList(); ?>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label class="control-label col-form-label" for="">Division</label>
                                    <select name="division" id="division_list" class="form-control">
                                        <option value="">Select Division</option>
                                        @foreach($divisions as $row)
                                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label col-form-label" for="">District</label>
                                    <select name="district" id="district_list" class="form-control">
                                        <option value="">Select District</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label col-form-label" for="">Upazila</label>
                                    <select name="thana" id="upazila_list" class="form-control">
                                    <option value="">Select Upazila</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(Session::has('message')) 
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                @if(Session::has('error')) 
                    <div class="alert alert-danger">
                        {{ Session::get('error') }}
                    </div> 
                @endif
                <div id="load_shop_list"></div>
            </div>
        </div>
    </div>
</div>
@include('backend.super_admin.password_check_modal')

<script>
    $(document).ready(function() {
        //load shop list
        function load_shop_list(id= '', type= '') {
            var url = APP_URL + '/admin/load-store-list';
            var loading = '<div class="col-md-12 text-center"><i class="fa fa-spinner fa-spin"></i></div>';
            $('#load_shop_list').html(loading);
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: {id: id, type: type},
                cache: false,
                success: function(response) {
                    $('#load_shop_list').html(response);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        load_shop_list();

        //get district/upazila
        function get_selected_district_upazila(id, type) {
            var url = "{{ url('get_district_upazila') }}";
            $.ajax({
                method: "POST",
                url: url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {id: id, type: type, data_format: 'html'},
                cache: false,
                success: function (response) {
                    $('#'+type).html(response);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $('#division_list').change(function() {
            var division_id = $(this).val();
            if(division_id == '') {
                $('#district_list').html('<option value="">Select District</option>');
            } else {
                load_shop_list(division_id, 'division');
                get_selected_district_upazila(division_id, 'district_list');
            }
            $('#upazila_list').html('<option value="">Select Upazila</option>');
        });
        $('#district_list').change(function() {
            var district_id = $(this).val();
            load_shop_list(district_id, 'district');
            get_selected_district_upazila(district_id, 'upazila_list');
        });
        $('#upazila_list').change(function() {
            var upazila_id = $(this).val();
            load_shop_list(upazila_id, 'upazila');
        });
    });
</script>
@endsection