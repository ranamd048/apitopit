@extends('backend.layout')
@section('maincontent')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Date Expire Shop List</h4>
            </div>
            <div class="card-block">
                @if(Session::has('message')) 
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                <div id="responsive_table">
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Baner</th>
                                <th>District</th>
                                <th>Expire</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($shop_list) > 0)
                            <?php $i = 1;?>
                            @foreach($shop_list as $row)
                            <?php
                                $date = new DateTime($row->expiry_date);
                            ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Name">{{ $row->store_name }}</td>
                                <td data-title="Phone">{{ $row->phone }}</td>
                                <td data-title="Baner"><img src="{{ asset('/public/uploads/store/'.$row->baner_image) }}" width="100" alt=""></td>
                                <td data-title="District">{{ Helpers::districtName($row->district) }}</td>
                                <td data-title="Expire">{{$date->format('d-F-Y')}}</td>
                                <td data-title="Action">
                                    <a class="btn btn-success" href="{{ url('/admin/edit-store/'.$row->id) }}"><i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    } );
</script>
@endsection