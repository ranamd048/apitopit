@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Add Gallery Type</h4>
                <div class="back-button">
                    <a href="{{ url('admin/gallery-types') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">

                <div class="form">
                    <form class="form-horizontal" action="{{ url('/admin/save_gallery_type') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Type Title</label>
                            <div class="col-sm-10">
                                <input type="text" name="type_title" required="" class="form-control">
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
