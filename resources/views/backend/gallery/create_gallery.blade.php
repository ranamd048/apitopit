@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Add New Gallery Image</h4>
                <div class="back-button">
                    <a href="{{ url('admin/gallery-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">

                <div class="form">
                    @if (Session::has('message')) 
                        <div class="alert alert-danger">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/save_gallery') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Select Types</label>
                            <div class="col-sm-10">
                                @if(count($gallery_types) > 0)
                                    @foreach($gallery_types as $row)
                                    <input  type="checkbox" name="gallery_type[]" value="{{ $row->types_id }}"> {{ $row->type_title }}
                                               <br>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Gallery Image</label>
                            <div class="col-sm-10">
                                <input type="file" name="gallery_image" class="form-control" required="">
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
