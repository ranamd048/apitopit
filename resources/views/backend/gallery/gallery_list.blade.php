@extends('backend.layout')

@section('maincontent')
<style>
.add-new {
    float: right;
    display: block;
    margin-top: -50px;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Gallery Image List</h4>
                <div class="back-button">
                    <a href="{{ url('admin/create-gallery') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Image</a>
                </div>
            </div>
            <div class="card-block">
                <div class="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover">
                        <tbody>
                            @if(count($gallery_list) != 0)
                            <tr>
                                <th>Serial No</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            <?php $i = 0; ?>
                            @foreach($gallery_list as $gallery)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Image"><img src="{{ asset('public/uploads/gallery/'.$gallery->gallery_image) }}" alt="" width="300" height="150"></td>
                                <td data-title="Action">
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure want to delete this image?')" href="{{ url('/admin/delete-gallery/'.$gallery->id) }}"><i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td width="100%">Data Not Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
