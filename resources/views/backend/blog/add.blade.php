@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Add Blog</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/blog-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">

                    <div class="form">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <form class="form-horizontal" action="{{ url('/admin/save_news') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right control-label col-form-label">Type </label>
                                    <div class="col-sm-10">
                                        <select name="type" class="form-control">
                                            <option value="news">News</option>
                                            <option value="notice">Notice</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right control-label col-form-label">Title * </label>
                                    <div class="col-sm-10">
                                        <input type="text" name="title" class="form-control" required placeholder="Title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right control-label col-form-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="image" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right control-label col-form-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea name="news_description" class="form-control" placeholder="Page Content"></textarea>
                                    </div>
                                </div>

                            <div lass="form-group row">
                                <div class="offset-sm-2 col-sm-8">
                                    <button type="submit" class="btn btn-info btn-default">Create</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('/') }}/assets/plugins/ckeditor/ckeditor.js"></script>

    <script>
        CKEDITOR.replace( 'news_description' );
    </script>

@endsection
