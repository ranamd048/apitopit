@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Edit Blog</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/blog-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="form">
                        <form class="form-horizontal" action="{{ url('/admin/update_news') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="news_id" value="{{ $news->id }}">
                            <input type="hidden" name="news_image" value="{{ $news->image_url }}">
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right control-label col-form-label">Type </label>
                                    <div class="col-sm-10">
                                        <select name="type" class="form-control">
                                            <option @if($news->type == 'news') selected @endif value="news">News</option>
                                            <option @if($news->type == 'notice') selected @endif value="notice">Notice</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right control-label col-form-label">Title * </label>
                                    <div class="col-sm-10">
                                        <input type="text" name="title" class="form-control" required placeholder="Title" value="{{ $news->title }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right control-label col-form-label">Image * </label>
                                    <div class="col-sm-10">
                                        <img src="{{ asset('public/uploads/blog/'.$news->image_url) }}" width="100" alt=""><br><br>
                                        <input type="file" name="image" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right control-label col-form-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea name="news_description" class="form-control" placeholder="Page Content">{{ $news->description }}</textarea>
                                    </div>
                                </div>

                            <div lass="form-group row">
                                <div class="offset-sm-2 col-sm-8">
                                    <button type="submit" class="btn btn-info btn-default">Update</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('/') }}/assets/plugins/ckeditor/ckeditor.js"></script>

    <script>
        CKEDITOR.replace( 'news_description' );
    </script>

@endsection
