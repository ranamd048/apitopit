@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Blog List</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/add-blog') }}" class="btn btn-primary"><i
                                    class="fa fa-plus"></i> Add Blog</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Type</th>
                                <th>Image</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($news_list) > 0)
                                <?php $i = 0; ?>
                                @foreach($news_list as $news)
                                    <?php $i++; ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Title">{{ $news->title }}</td>
                                        <td data-title="Type">{{ $news->type }}</td>
                                        <td data-title="Image">
                                            <img src="{{ asset('public/uploads/blog/'.$news->image_url) }}" alt="" width="150">
                                        </td>
                                        <td data-title="Date">{{ date_format (new DateTime($news->created_at), 'd M Y') }}</td>
                                        <td data-title="Action">
                                            <a class="btn btn-success"
                                               href="{{ url('/admin/edit-blog/'.$news->id) }}"><i
                                                        class="fa fa-pencil"></i></a>
                                            <a class="btn btn-danger"
                                               onclick="return confirm('Are you sure want to delete this?')"
                                               href="{{ url('/admin/delete-blog/'.$news->id) }}"><i
                                                        class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#common_datatable_id').DataTable();
        });
    </script>
@endsection
