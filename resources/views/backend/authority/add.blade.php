@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Add New Authority</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/authority/list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="form">
                        <form class="form-horizontal" action="{{ url('/admin/authority/save') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Name *</label>
                                        <input type="text" class="form-control" name="name" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="email" class="form-control" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Phone * (Ex: 01779221842)</label>
                                        <input type="text" class="form-control" name="phone" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Photo</label>
                                        <input type="file" class="form-control" name="photo">
                                    </div>
                                    <div class="form-group">
                                        <?php $divisions = Helpers::getDivisionList(); ?>
                                        <label for="">Division</label>
                                        <select name="division" id="division_list" class="form-control">
                                            <option value="">Select Division</option>
                                            @foreach($divisions as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">District</label>
                                        <select name="district" id="district_list" class="form-control">
                                            <option value="">Select District</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Upazila</label>
                                        <select name="thana" id="upazila_list" class="form-control">
                                            <option value="">Select Upazila</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Password *</label>
                                        <input type="password" class="form-control {{ ($errors->has('password')) ? 'is-invalid' : '' }}" name="password">
                                        @if($errors->has('password'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Confirm Password *</label>
                                        <input type="password" class="form-control {{ ($errors->has('password_confirmation')) ? 'is-invalid' : '' }}" name="password_confirmation">
                                        @if($errors->has('password_confirmation'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password_confirmation') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group" id="module_permission" style="display: block;">
                                        <label for="">Module Permission *</label><br>
                                        <input type="checkbox" checked="" name="product_access" id=""> Product <br>
                                        <input type="checkbox" checked="" name="shop_settings" id=""> Shop<br>
                                        <input type="checkbox" checked="" name="website_settings_access" id=""> Website Settings<br>
                                        <input type="checkbox" checked="" name="product_review_access" id=""> Product Review<br>
                                        <input type="checkbox" name="people_access" id=""> People <br>
                                        <input type="checkbox" name="report_access" id=""> Report <br>
                                        @if($private_settings->coupon_option == 1)
                                        <input type="checkbox" name="coupon_access" id=""> Coupon <br>
                                        @endif
                                        <input type="checkbox" name="order_access" id=""> Order <br>
                                        <input type="checkbox" name="service_access" id=""> Service <br>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            //get district/upazila
            function get_selected_district_upazila(id, type) {
                var url = "{{ url('get_district_upazila') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, type: type, data_format: 'html'},
                    cache: false,
                    success: function (response) {
                        $('#'+type).html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            $('#division_list').change(function() {
                var division_id = $(this).val();
                if(division_id == '') {
                    $('#district_list').html('<option value="">Select District</option>');
                } else {
                    get_selected_district_upazila(division_id, 'district_list');
                }
                $('#upazila_list').html('<option value="">Select Upazila</option>');
            });

            $('#district_list').change(function() {
                var district_id = $(this).val();
                get_selected_district_upazila(district_id, 'upazila_list');
            });
        });
    </script>
@endsection