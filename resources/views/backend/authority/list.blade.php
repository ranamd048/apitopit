@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Authority List</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/authority/add') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add  Authority</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Photo</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($authority_list) > 0)
                                <?php $i = 0; ?>
                                @foreach($authority_list as $row)
                                    <?php $i++; ?>
                    <?php 
                        if(empty($row->profile_photo)) {
                            $profile_photo =  asset('/').'public/uploads/admin.jpg';
                        } else {
                            $profile_photo =  asset('public/uploads/'.$row->profile_photo);
                        }
                    ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Name">{{ $row->name }}</td>
                                        <td data-title="Email">{{ $row->email }}</td>
                                        <td data-title="Phone">{{ $row->phone }}</td>
                                        <td data-title="Photo"><img src="{{$profile_photo}}" width="120" alt=""></td>
                                        <td data-title="Action">
                                            <a href="{{url('/admin/authority/edit/'.$row->id)}}" class="btn btn-success" title="Edit"><i class="fa fa-pencil"></i></a>
                                            <a href="{{url('/admin/authority/delete/'.$row->id)}}" class="btn btn-danger" title="Delete" onclick="return confirm('Are you sure want to delete?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable();
        } );
    </script>
@endsection