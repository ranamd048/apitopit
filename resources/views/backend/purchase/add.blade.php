@extends('backend.layout')

@section('maincontent')
<style>
div#search_result ul {
    border: 1px solid #ddd;
    margin-top: 5px;
    padding: 0;
    position: absolute;
    list-style: none;
    width: 100%;
    z-index: 9999999;
    background: #fafafa;
}
div#search_result ul li {
    border-bottom: 1px solid #ddd;
    padding: 8px;
    cursor: pointer;
}
div#search_result ul li:last-child {
    border: 0;
}
div#search_result {
    position: relative;
}
input.td_input {
    width: 80px;
    padding: 6px;
    text-align: center;
}
.delete_item {
    cursor: pointer;
    font-size: 18px;
    color: red;
}
table tbody:empty + tfoot {
    display: none;
}

table tbody + tfoot {
    display: table-footer-group;
}
table tfoot {
    background: #555;
    color: #fff;
}
table tfoot td {
    font-weight: 500;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Add Purchase</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/purchase-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/save_purchase') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Date </label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Supplier *</label>
                                    <select class="form-control" name="supplier_id" id="supplier_id" required="">
                                        <option value="">Select Supplier</option>
                                        @foreach($supplier_list as $row)
                                        <option value="{{$row->id}}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="received">Received</option>
                                        <option value="pending">Pending</option>
                                        <option value="ordered">Ordered</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" id="search_product" class="form-control" placeholder="Please add products to order list">
                                    <div id="search_result"></div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <a href="{{url('admin/add-product')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Product</a>
                                </div>
                            </div>
                            <div class="col-md-12 order_items">
                                <input type="hidden" id="item_serial" value="0">
                                <table class="table table-bordered table-hover" id="purchase_items">
                                    <thead style="background: #555; color: #fff">
                                    <tr>
                                        <th>Product</th>
                                        <th>Sale Price</th>
                                        <th>Cost Price</th>
                                        <th>Quantity</th>
                                        <th>Subtotal</th>
                                        <th><i class="fa fa-trash"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2">Total</td>
                                            <td colspan="1"><span id="total_cost_value"></span></td>
                                            <td colspan="1"><span id="total_quantity_value"></span></td>
                                            <td colspan="2"><span id="subtotal_value"></span></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Shipping Address</label>
                                    <input type="text" name="shipping" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Discount </label>
                                    <input type="text" name="discount" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Paid Amount <span style="font-size: 12px;">(You can't advance payment from here. If you want advance payment  <a href="{{ url('admin/supplier-list') }}">Click Here</a>)</span></label>
                                    <input type="text" name="paid_amount" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Attachment </label>
                                    <input type="file" name="attachment" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Payment Note</label>
                                    <textarea name="payment_note" class="form-control" placeholder="Payment Note"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Purchase Note</label>
                                    <textarea name="note" class="form-control" placeholder="Purchase Note"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var total_quantity = 0;
        var total_cost = 0;
        var subtotal = 0;

        function updateFooterTotal() {
            $('#total_quantity_value').html(total_quantity);
            $('#total_cost_value').html(total_cost.toFixed(2));
            $('#subtotal_value').html(subtotal.toFixed(2));
        }

        function tableRowItemLoop() {
            total_cost = 0;
            total_quantity = 0;
            subtotal = 0;
            $("table tbody tr").each(function() {
                var cost = parseFloat($(this).find("input.cost_item").val());
                var quantity = parseInt($(this).find("input.quantity_item").val());
                total_cost += cost;
                total_quantity += quantity;
                subtotal += (cost * quantity);
            });
            updateFooterTotal();
        }

        updateFooterTotal(); 

        $(document).on('keyup', '#search_product', function() {
            var supplier_id = $('#supplier_id').val();
            if(supplier_id == '') {
                alert('Please Select Supplier');
                $(this).val('');
                return false;
            }
            var value = $(this).val();
            var url = APP_URL + '/admin/purchase_product_search';
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { value: value },
                cache: false,
                success: function(response) {
                    $('#search_result').html(response);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });

        $(document).on('click', '.add_item', function() {
            var serial = parseInt($('#item_serial').val());
            var id = $(this).data('id');
            var name = $(this).data('name');
            var price = $(this).data('price');
            var cost = $(this).data('cost');
            var stock = $(this).data('stock');
            var tr_item = '<tr id="tr_item_'+id+'">';
            tr_item += '<td><input type="hidden" name="items['+serial+'][id]" value='+id+'><input type="hidden" name="items['+serial+'][stock]" value='+stock+'><input type="hidden" name="items['+serial+'][name]" value='+name+'>'+name+'</td><td><input type="text" name="items['+serial+'][price]" class="td_input" value='+price+'></td><td><input type="text" name="items['+serial+'][cost]" class="td_input cost_item" data-rowid="'+id+'" value='+cost+'></td><td><input type="text" name="items['+serial+'][quantity]" class="td_input quantity_item" data-rowid="'+id+'" value="1"></td><td class="sub_amount">'+cost+'</td><td><span class="delete_item" data-id='+id+'>x</span></td>';
            tr_item += '</tr>';
            $('#purchase_items tbody').append(tr_item);
            $('#search_result').html('');
            $('#search_product').val('');
            $('#item_serial').val(serial + 1);

            total_quantity += 1;
            total_cost += cost;
            subtotal += cost;

            updateFooterTotal();
        });

        $(document).on('click', '.delete_item', function() {
            var id = $(this).data('id');
            $('#tr_item_'+id).remove();
            tableRowItemLoop();
        });

        $(document).on('change', '.quantity_item', function() {
            tableRowItemLoop();
            var rowId = parseInt($(this).data('rowid'));
            var thisQuantity = parseInt($(this).val());
            var thisCost = parseFloat($('#tr_item_'+rowId+' td input.cost_item').val());
            var thisSubTotal = (thisQuantity * thisCost).toFixed(2);
            $('#tr_item_'+rowId+' td.sub_amount').html(thisSubTotal);
        });

        $(document).on('change', '.cost_item', function() {
            tableRowItemLoop();
            var rowId = parseInt($(this).data('rowid'));
            var thisQuantity = parseInt($('#tr_item_'+rowId+' td input.quantity_item').val());
            var thisCost = parseFloat($(this).val());
            var thisSubTotal = (thisQuantity * thisCost).toFixed(2);
            $('#tr_item_'+rowId+' td.sub_amount').html(thisSubTotal);
        });
    });
</script>
@endsection
