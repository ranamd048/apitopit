@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">View Purchase</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/purchase-list') }}" class="btn btn-primary text-capitalize"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                            <?php $date = new DateTime($purchase->created_at); ?>
                            <?php
                                if($purchase->payment_status == 0) {
                                    $payment_status = '<span class="badge badge-danger">Due</span>';
                                }
                                if($purchase->payment_status == 1) {
                                    $payment_status = '<span class="badge badge-success">Paid</span>';
                                }
                            ?>
                                <td>Date</td>
                                <td>{{$date->format('d-F-Y')}}</td>
                            </tr>
                            <tr>
                                <td>Supplier</td>
                                <td>{{$purchase->name}}</td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>৳ {{number_format($purchase->total, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Discount</td>
                                <td>৳ {{number_format($purchase->discount, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Grand Total</td>
                                <td>৳ {{number_format($purchase->grand_total, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Paid Amount</td>
                                <td>৳ {{number_format($purchase->paid_amount, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Due Amount</td>
                                <td>৳ {{number_format($purchase->due_amount, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Payment Status</td>
                                <td><?php echo $payment_status; ?></td>
                            </tr>
                            <tr>
                                <td>Note</td>
                                <td>{{$purchase->note}}</td>
                            </tr>
                            <tr>
                                <td>Attachment</td>
                                <td>@if($purchase->attachment)<a href="{{asset('public/uploads/purchase_attachments/'.$purchase->attachment)}}" target="_blank" class="btn btn-primary">View Attachment</a>@endif</td>
                            </tr>
                            <tr>
                                <td>Shipping</td>
                                <td>{{$purchase->shipping}}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td class="text-capitalize">{{$purchase->status}}</td>
                            </tr>
                            <tr>
                                <td>Created By</td>
                                <td>{{$purchase->admin_name}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="responsive_table">
                        <table class="table table-bordered table-hover">
                            <thead style="background: #555; color: #fff;">
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Sale Price</th>
                                <th>Cost Price</th>
                                <th>Quantity</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($purchase_items) > 0)
                                <?php $i = 0; ?>
                                @foreach($purchase_items as $row)
                                    <?php $i++; ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Product">{{ $row->product_name }}</td>
                                        <td data-title="Sale Price">৳ {{number_format($row->sale_price, 2)}}</td>
                                        <td data-title="Cost Price">৳ {{number_format($row->cost_price, 2)}}</td>
                                        <td data-title="Quantity">{{$row->quantity}}</td>
                                        <td data-title="Subtotal">৳ {{number_format($row->subtotal, 2)}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection