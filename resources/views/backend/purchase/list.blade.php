@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Purchase List</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/add-purchase') }}" class="btn btn-primary text-capitalize"><i class="fa fa-plus"></i>Add Purchase</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Supplier</th>
                                <th>Grand Total</th>
                                <th>Due</th>
                                <th>Payment Status</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($result) > 0)
                                <?php $i = 0; ?>
                                @foreach($result as $row)
                                    <?php $i++; $date = new DateTime($row->created_at); ?>
                                    <?php
                                        if($row->payment_status == 0) {
                                            $payment_status = '<span class="badge badge-danger">Due</span>';
                                        }
                                        if($row->payment_status == 1) {
                                            $payment_status = '<span class="badge badge-success">Paid</span>';
                                        }
                                    ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Supplier">{{ $row->name }}</td>
                                        <td data-title="Grand Total">৳ {{number_format($row->grand_total, 2)}}</td>
                                        <td data-title="Due">৳ {{number_format($row->due_amount, 2)}}</td>
                                        <td class="text-capitalize" data-title="Payment Status"><?php echo $payment_status; ?></td>
                                        <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                        <td data-title="Action">
                                            <a href="{{url('/admin/view-purchase/'.$row->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                            <a style="display:none;" href="{{url('/admin/edit-purchase/'.$row->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                            <a href="{{url('/admin/delete-purchase/'.$row->id)}}" onclick="return confirm('Are you sure want to delete?');" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable();
        } );
    </script>
@endsection