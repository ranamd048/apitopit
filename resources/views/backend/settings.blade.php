@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Update Site Information</h4>
            </div>
            <div class="card-block">
                @if(Session::get('message'))
                    <p class="alert alert-success">{{ Session::get('message') }}</p>
                @endif
                <div class="form">
                    <form class="form-horizontal" action="{{ url('/admin/update_siteinfo') }}" method="POST" enctype="multipart/form-data">
                    	{{ csrf_field() }}
                        <input type="hidden" name="settings_id" value="{{ $theme_settings->id }}">
                    	<input type="hidden" name="existing_logo" value="{{ $theme_settings->logo }}">
                    	<input type="hidden" name="existing_banner" value="{{ $theme_settings->banner }}">
                    	<input type="hidden" name="existing_favicon" value="{{ $theme_settings->favicon }}">
                    	<input type="hidden" name="existing_about_img" value="{{ $theme_settings->about_img }}">
                        <div class="row">

                            <div class="col-md-6">

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Website Title</label>
                                        <input type="text" name="web_title" value="{{ $theme_settings->web_title }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Address</label>
                                        <input type="text" name="address" value="{{ $theme_settings->address }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Email</label>
                                        <input type="email" name="email" value="{{ $theme_settings->email }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Phone Number</label>
                                        <input type="text" name="phone_number" value="{{ $theme_settings->phone_number }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Facebook Link</label>
                                        <input type="text" name="fb_link" value="{{ $theme_settings->fb_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Twitter Link</label>
                                        <input type="text" name="twitter_link" value="{{ $theme_settings->twitter_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Google Plus Link</label>
                                        <input type="text" name="googlePlus_link" value="{{ $theme_settings->googlePlus_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Youtube Link</label>
                                        <input type="text" name="youtube_link" value="{{ $theme_settings->youtube_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Linkedin Link</label>
                                        <input type="text" name="linkedin_link" value="{{ $theme_settings->linkedin_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Instagram Link</label>
                                        <input type="text" name="instagram_link" value="{{ $theme_settings->instagram_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Theme Color</label>
                                        <input type="color" name="theme_color" value="{{ $theme_settings->theme_color }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12" style="margin-top: 56px;">
                                        <label class="control-label col-form-label">Select Active Tab</label>
                                        <select name="active_tab" class="form-control">
                                            <option @if($theme_settings->active_tab == 'categories') selected="" @endif value="categories">Categories</option>
                                            <option @if($theme_settings->active_tab == 'brands') selected="" @endif value="brands">Brands</option>
                                            <option @if($theme_settings->active_tab == 'services') selected="" @endif value="services">Services</option>
                                            <option @if($theme_settings->active_tab == 'shops') selected="" @endif value="shops">Shops</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-info btn-default">Update</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Map Link</label>
                                        <input type="text" name="maps_iframe" value="{{ $theme_settings->maps_iframe }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <img src="{{ asset('public/uploads/'.$theme_settings->logo) }}" width="120" alt=""> <br>
                                        <label class="control-label col-form-label">Update Logo</label>
                                        <input type="file" name="logo" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <img src="{{ asset('public/uploads/'.$theme_settings->favicon) }}" width="30" alt=""> <br>
                                        <label class="control-label col-form-label">Update Favicon</label>
                                        <input type="file" name="favicon" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Copyright Text</label>
                                        <input type="text" name="copyright_text" value="{{ $theme_settings->copyright_text }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">About Title</label>
                                        <input type="text" name="about_title" value="{{ $theme_settings->about_title }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        @if($theme_settings->about_img)
                                        <img src="{{ asset('public/uploads/'.$theme_settings->about_img) }}" width="120" alt=""> <br>
                                        @endif
                                        <label class="control-label col-form-label">About Image</label>
                                        <input type="file" name="about_img" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">About Details</label>
                                        <textarea name="about_desc" class="form-control">{{$theme_settings->about_desc}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <img src="{{ asset('public/uploads/'.$theme_settings->banner) }}" width="100%" height="50px" alt="Opps"> <br>
                                        <label class="control-label col-form-label">Update Banner [size: 1400px*50px]</label>
                                        <input type="file" name="banner" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
                {{-- @if(Session::has('success'))
                	<br>
					<p class="alert alert-success">{{ Session::get('success') }}</p>
                @endif --}}
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/') }}assets/plugins/ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'about_desc' );
</script>
@endsection
