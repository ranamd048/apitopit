@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Slider List</h4>
                <div class="back-button">
                    <a href="{{ url('admin/create-slider') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Slider</a>
                </div>
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover">
                        <tbody>
                            @if(count($slider_list) != 0)
                            <tr>
                                <th>Serial No</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Sort Order</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <?php $i = 0; ?>
                            @foreach($slider_list as $slider)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Title">{{ $slider->slider_title }}</td>
                                <td data-title="Image"><img src="{{ asset('public/uploads/'.$slider->slider_image) }}" alt="" width="200"></td>
                                <td data-title="Sort Order">{{ $slider->sort_order }}</td>
                                <td data-title="Status"><?php echo ($slider->status == 1) ? 'Active' : 'Inactive'; ?></td>
                                <td data-title="Action">
                                    <a class="btn btn-success" href="{{ url('/admin/edit-slider/'.$slider->id) }}"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure want to delete this slider?')" href="{{ url('/admin/delete-slider/'.$slider->id) }}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td width="100%">Data Not Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection