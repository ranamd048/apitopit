@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Edit Slider</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/slider-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/update_slider') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="slider_id" value="{{ $slider_data->id }}">
                        <input type="hidden" name="existing_slider_img" value="{{ $slider_data->slider_image }}">
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Slider Title * </label>
                            <div class="col-sm-10">
                                <input type="text" name="slider_title" class="form-control" required value="{{ $slider_data->slider_title }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Slider Image <span class="image_size">(Size: 1050 x 420 Max: 1024 kb)</span></label>
                            <div class="col-sm-10">
                                <img src="{{ asset('public/uploads/'.$slider_data->slider_image) }}" alt="" width="200"><br><br>
                                <input type="file" name="slider_image" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Sort Order </label>
                            <div class="col-sm-10">
                                <input type="text" name="sort_order" class="form-control" value="{{ $slider_data->sort_order }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option @if($slider_data->status == 1) {{ 'selected' }} @endif value="1">Active</option>
                                    <option @if($slider_data->status == 0) {{ 'selected' }} @endif value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Update</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection