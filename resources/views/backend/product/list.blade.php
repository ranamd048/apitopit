@extends('backend.layout')

@section('maincontent')

<div class="row">
    <input type="hidden" id="file_name" value="product.product_list">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Product List</h4>
                <div class="back-button">
                    <a href="{{ url('admin/add-product') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Product</a>
                </div>
            </div>
            <div class="card-block">
                @include('backend.product.filter')
                @if(Session::has('message')) 
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                @if(Session::has('error')) 
                    <div class="alert alert-danger">
                        {{ Session::get('error') }}
                    </div> 
                @endif
                <div id="load_all_products"></div>
            </div>
        </div>
    </div>
</div>
@include('backend.super_admin.password_check_modal')

<script src="{{ asset('frontend_assets/admin/js/product.js') }}"></script>
<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    $(document).ready(function() {
        //selected shop
        var shop_id = $('#current_shop_id').val();
        load_products(shop_id);
        get_shop_categories(shop_id);
    })
</script>
@endsection