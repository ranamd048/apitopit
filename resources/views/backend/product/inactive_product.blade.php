@extends('backend.layout')
@section('maincontent')
<style>
    .filter_btn {
        margin-top: 32px;
    }
    @media only screen and (max-width: 767px) {
        .filter_btn {
            margin-top: 0px;
            float: right;
        } 
    }
</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Inactive Products</h4>
            </div>
            <div class="card-block">
                <?php $shop_id = isset($_GET['shop_id']) ? $_GET['shop_id'] : ''; ?>
                <form action="" method="GET">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Shop</label>
                                <select name="shop_id" id="shop_list" class="form-control" required="">
                                    <option value="">Select Shop</option>
                                    @foreach($shop_list as $shop)
                                    <option @if($shop_id == $shop->id) selected="" @endif value="{{ $shop->id }}">{{ $shop->store_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group filter_btn">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                                <a href="{{ url('admin/inactive-product') }}" class="btn btn-danger"><i class="fa fa-refresh"></i> Reset</a>
                            </div>
                        </div>
                    </div>
                </form>
                @if(Session::has('message')) 
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                <div id="responsive_table">
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Shop</th>
                                <th>Image</th>
                                <th>Brand</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($product_list) > 0)
                            <?php $i = 1;?>
                            @foreach($product_list as $row)
                            <?php
                                $date = new DateTime($row->created_at);
                            ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Name">{{ $row->product_name }}</td>
                                <td data-title="Shop">{{ $row->store_name }}</td>
                                <td data-title="Image"><img src="{{ asset('/public/uploads/products/thumbnail/'.$row->product_image) }}" width="100" alt=""></td>
                                <td data-title="Brand">{{ Helpers::getBrandName($row->brand_id) }}</td>
                                <td data-title="Category">{{ Helpers::getCategoryName($row->category_id) }}</td>
                                <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                <td data-title="Action">
                                    <a class="btn btn-success" onclick="return confirm('Are you sure want to publish this product?')" href="{{ url('/admin/publish-product/'.$row->id) }}"><i class="fa fa-check"></i>
                                    </a>
                                    <a class="btn btn-primary" target="_blank" href="{{ url('/product/'.$row->id.'/'.$row->product_slug) }}"><i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#shop_list').select2();
        $('#common_datatable_id').DataTable();
    });
</script>
@endsection