@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Add New Product</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/product-list') }}" class="btn btn-primary"><i
                                    class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    @if(Session::has('error')) 
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div> 
                    @endif
                    <div class="form">
                        @if($errors->any())
                            @foreach ($errors->all() as $error)
                                <p class="alert alert-danger">{{$error}}</p>
                            @endforeach
                        @endif
                        <form class="form-horizontal" action="{{ url('/admin/save_product') }}" method="POST"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @if(!Session::has('store_id'))
                                <div class="form-group">
                                    <label class="control-label col-form-label">Store List *</label>
                                    <select class="form-control" name="store_id" required="">
                                        <option value="">Select Store</option>
                                        @foreach($store_list as $row)
                                            <option value="{{ $row->id }}">{{ $row->store_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Name * </label>
                                        <input type="text" name="product_name" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Image  {{ Session::get('is_image_required') == 1 ? '*' : '' }} <span class="image_size">(Max: 1024 kb)</span></label>
                                        <input type="file" name="product_image" class="form-control" @if(Session::get('is_image_required') == 1) required="" @endif>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Brand</label>
                                        <select class="form-control" name="brand_id">
                                            <option value="">Select Brand</option>
                                            @foreach($brand_list as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Category *</label>
                                        <select class="form-control" name="category_id" id="category_id" required="">
                                            <option value="">Select Category</option>
                                            @foreach($categories as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Sub Category</label>
                                        <select class="form-control" name="subcategory_id" id="subcategory_id">
                                            <option value="">Select Sub Category</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Second Sub Category</label>
                                        <select class="form-control" name="sub_subcategory_id" id="sub_subcategory_id">
                                            <option value="">Select Second Sub Category</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Price * </label>
                                        <input type="text" name="price" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Video</label>
                                        <input type="text" name="product_video" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Gallery</label>
                                        <input type="file" name="product_gallery[]" class="form-control" multiple="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Units *</label>
                                        <select class="form-control" name="unit_id" required="" id="unit_list">
                                            @foreach($unit_list as $row)
                                                <option value="{{ $row->id }}">{{ $row->unit_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Location </label>
                                        <input type="text" name="product_location" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Code </label>
                                        <input type="text" name="product_code" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Quantity *</label>
                                        <input type="number" name="quantity" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Promo Start Date </label>
                                        <input type="date" name="promotion_start_date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Promo Price</label>
                                        <input type="text" name="promotion_price" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Cost </label>
                                        <input type="text" name="cost" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Promo End Date </label>
                                        <input type="date" name="promotion_end_date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Alert Quantity</label>
                                        <input type="text" name="alert_quantity" class="form-control">
                                    </div>
                                    @if(Session::get('role') == 0)
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Status</label>
                                        <select class="form-control" name="status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="is_checking_stock" class="control-label col-form-label">Stock Check</label>
                                            <input type="checkbox" checked="" name="is_checking_stock" id="is_checking_stock">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="featured_product"
                                                   class="control-label col-form-label">Featured </label>
                                            <input type="checkbox" name="featured" id="featured_product">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="is_used"
                                                   class="control-label col-form-label">Is Used ? </label>
                                            <input type="checkbox" name="is_used" id="is_used">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="is_home_delivery"
                                                   class="control-label col-form-label">Is Home Delivery ? </label>
                                            <input type="checkbox" checked="" name="is_home_delivery" id="is_home_delivery">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="show_per_price"
                                                   class="control-label col-form-label">Show Price ? </label>
                                            <input type="checkbox" checked="" name="show_per_price" id="show_per_price">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Description</label>
                                        <textarea name="product_description" class="form-control"
                                                  placeholder="Product Description"></textarea>
                                    </div>
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </div>


                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('/') }}assets/plugins/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('product_description');

        $(document).ready(function () {
            $('#unit_list').select2();

            $('#category_id').change(function () {
                var id = $(this).val();
                if (id == '') {
                    var output = '<option value="">Select Sub Category</option>';
                    $('#subcategory_id').html(output);
                } else {
                    var url = '{{ url('/admin/subcategory_items/') }}' + '/' + id;
                    $.ajax({
                        url: url,
                        method: "GET",
                        success: function (response) {
                            $('#subcategory_id').html(response);
                        },
                    });
                }
            });

            $('#subcategory_id').change(function () {
                var id = $(this).val();
                if (id == '') {
                    var output = '<option value="">Select Second Sub Category</option>';
                    $('#sub_subcategory_id').html(output);
                } else {
                    var url = '{{ url('/admin/subcategory_items/') }}' + '/' + id;
                    $.ajax({
                        url: url,
                        method: "GET",
                        success: function (response) {
                            $('#sub_subcategory_id').html(response);
                        },
                    });
                }
            });
        });
    </script>
@endsection