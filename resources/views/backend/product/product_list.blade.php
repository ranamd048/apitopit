<div id="responsive_table">
    @if(Session::get('role') == 0)
    <form action="{{ url('admin/copy_products') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-4 offset-md-6">
                <div class="form-group">
                    <select class="form-control" id="copy_to_shop_id" name="copy_to_shop_id" required="true">
                        <option value="">Select Shop</option>
                        @foreach($shop_list as $shop)
                        <option value="{{ $shop->id }}">{{ $shop->store_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-info"><i class="fa fa-copy"></i> Copy Products</button>
                </div>
            </div>
        </div>
        <input type="hidden" name="copy_from_shop_id" value="{{$shop_id}}">
    @endif
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>@if(Session::get('role') == 0) <input type="checkbox" onchange="checkAll(this)"> @else # @endif</th>
                <th>Name</th>
                <th>Image</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Location</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($products) > 0)
            <?php $i = 1;?>
            @foreach($products as $row)
            <?php
                if($row->status == 1) {
                    $product_status = '<span class="badge badge-success">Active</span>';
                } else {
                    $product_status = '<span class="badge badge-danger">Inactive</span>';
                }
            ?>
            <tr>
                <td data-title="Sl">@if(Session::get('role') == 0) <input type="checkbox" name="product_id[]" value="{{$row->id}}"> @else {{ $i }} @endif</td>
                <td data-title="Name">{{ $row->product_name }}</td>
                <td data-title="Image">@if(!empty($row->product_image)) <img src="{{ asset('/public/uploads/products/thumbnail/'.$row->product_image) }}" width="100" alt=""> @endif</td>
                <td data-title="Category">{{ Helpers::getCategoryName($row->category_id) }}</td>
                <td data-title="Sub Category">{{ Helpers::getCategoryName($row->subcategory_id) }}</td>
                <td data-title="Location">{{ $row->product_location }}</td>
                <td data-title="Status"><?php echo $product_status; ?></td>
                <td data-title="Action">
                    <a class="btn btn-success" href="{{ url('/admin/edit-product/'.$row->id) }}"><i class="fa fa-pencil"></i>
                    </a>
                    @if(Session::get('role') == 0)
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".password_check_modal" data-id="{{$row->id}}" data-for="products"><i class="fa fa-trash"></i></button>
                    @endif
                </td>
            </tr>
            <?php $i++; ?>
            @endforeach
            @endif
        </tbody>
    </table>
    @if(Session::get('role') == 0)
    </form>
    @endif
</div>
<script>
    $(document).ready(function() {
        $('#copy_to_shop_id').select2();
    });
</script>