@extends('backend.layout')

@section('title_text') Product Stock Report @endsection

@section('maincontent')
<style>
  .row.stock_summary {
    width: 100%;
    overflow: hidden;
    padding-top: 20px;
  }
  table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after {
    display: none;
  }
  table tfoot tr td { font-weight: 500; }
  @media only screen and (max-width: 767px) {
    .card-block {
      margin-top: 0;
    }
  }
</style>
<div class="row">
<input type="hidden" id="file_name" value="product.stock_list">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Product Stock</h4>
            </div>
            <div class="card-block">
                @include('backend.product.filter')
                @if (Session::has('message')) 
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                <div id="load_all_products"></div>
            </div>
        </div>
    </div>
</div>
<!--Stock Edit Modal-->
<div class="modal fade edit_stock_modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form action="{{ url('admin/update_product_stock') }}" method="POST">
      {{csrf_field()}}
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Stock</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="product_id" id="product_id">
		  <div class="form-group">
            <label for="sell_price" class="col-form-label">Sell Price:</label>
            <input type="text" class="form-control" name="sell_price" id="sell_price" required="">
          </div>
		  <div class="form-group">
            <label for="cost_price" class="col-form-label">Cost Price:</label>
            <input type="text" class="form-control" name="cost_price" id="cost_price" required="">
          </div>
          <div class="form-group">
            <label for="quantity" class="col-form-label">Quantity:</label>
            <input type="number" class="form-control" name="quantity" id="quantity" required="">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </div>
    </form>
  </div>
</div>

<script src="{{ asset('assets/js/datatable/buttons.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/jszip.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/js/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/buttons.print.min.js') }}"></script>

<script src="{{ asset('frontend_assets/admin/js/product.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.edit_stock_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
			var costPrice = button.data('cost');
			var sellPrice = button.data('price');
            var modal = $(this);
            modal.find('.modal-title').text('Update Quantity for ' + name);
            modal.find('.modal-body input#product_id').val(id);
			modal.find('.modal-body input#cost_price').val(costPrice);
			modal.find('.modal-body input#sell_price').val(sellPrice);
        });

        //selected shop
        var shop_id = $('#current_shop_id').val();
        load_products(shop_id);
        get_shop_categories(shop_id);
    })
</script>
@endsection