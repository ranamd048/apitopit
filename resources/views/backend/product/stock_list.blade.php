<?php $total_cost_price = 0; $total_sale_price = 0; $total_stock = 0; ?>
<div id="responsive_table">
<table class="table table-bordered table-hover" id="common_datatable_id">
    <thead>
        <tr>
            <th>#</th>
            <th>Product Name</th>
            {{-- <th>Image</th> --}}
            <th>Per Price</th>
            <th>Cost Price</th>
            <th>Sale Price</th>
            <th>Stock</th>
			<th>Updated</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if(count($products) > 0)
        <?php $i = 1; ?>
        @foreach($products as $row)
        <?php
            $stock_count = ($row->quantity - $row->sale_quantity);
            $cost_price = ($row->cost * $stock_count);
            $sale_price = ($row->price * $stock_count);
            $total_cost_price += $cost_price;
            $total_sale_price += $sale_price;
            $total_stock += $stock_count;
        ?>
        <tr>
            <td data-title="Sl">{{ $i }}</td>
            <td data-title="Product Name">{{ $row->product_name }}</td>
            {{-- <td data-title="Image"><img src="{{ asset('/public/uploads/products/thumbnail/'.$row->product_image) }}" width="100" alt=""></td> --}}
            <td data-title="Per Price">৳{{number_format($row->price, 2)}}</td>
            <td data-title="Cost Price">৳{{number_format($cost_price, 2)}}</td>
            <td data-title="Sale Price">৳{{number_format($sale_price, 2)}}</td>
            <td data-title="Stock"><?php echo $stock_count; ?></td>
			<td data-title="Updated">
				<?php if($row->updated_at) { $last_update = new DateTime($row->updated_at); echo $last_update->format('d-F-Y h:i a');  } ?>
			</td>
            <td data-title="Action">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target=".edit_stock_modal" data-id="{{$row->id}}" data-name="{{ $row->product_name }}" data-price="{{ $row->price }}" data-cost="{{ $row->cost }}"><i class="fa fa-pencil"></i></button>
            </td>
        </tr>
        <?php $i++; ?>
        @endforeach
        @endif
    </tbody>
    <tfoot>
        <tr>
            <td>Profit</td>
            <td>৳{{number_format($total_sale_price - $total_cost_price, 2)}}</td>
            <td>Sale Price</td>
            <td>৳{{number_format($total_sale_price, 2)}}</td>
            <td>Cost Price</td>
            <td>৳{{number_format($total_cost_price, 2)}}</td>
            <td>Stock</td>
            <td>{{ $total_stock }}</td>
        </tr>
    </tfoot>
</table>
</div>

<div class="row stock_summary">
    <div class="col-md-3 col-6 text-center">
        <div class="card bg-info">
            <div class="card-block" style="color:#fff; font-size:17px">Stock Quantity <br> {{ $total_stock }}</div>
        </div>
    </div>
    <div class="col-md-3 col-6 text-center">
        <div class="card bg-success">
            <div class="card-block" style="color:#fff; font-size:17px">Cost Price <br> ৳{{number_format($total_cost_price, 2)}}</div>
        </div>
    </div>
    <div class="col-md-3 col-6 text-center">
        <div class="card bg-info">
            <div class="card-block" style="color:#fff; font-size:17px">Sale Price <br> ৳{{number_format($total_sale_price, 2)}}</div>
        </div>
    </div>
    <div class="col-md-3 col-6 text-center">
        <div class="card bg-primary">
            <div class="card-block" style="color:#fff; font-size:17px">Profit <br> ৳{{number_format($total_sale_price - $total_cost_price, 2)}}</div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable({
            dom: 'Bfrtip',
            // buttons: [
            //     'csv', 'excel', 'pdf', 'print'
            // ]
            buttons: [
                { extend: 'excelHtml5', footer: true },
                { extend: 'csvHtml5', footer: true },
                { extend: 'pdfHtml5', footer: true },
                { extend: 'print', footer: true }
            ]
        });
    });
</script>