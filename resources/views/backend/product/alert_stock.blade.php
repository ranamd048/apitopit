@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Product Stock Alert</h4>
                <div class="back-button">
                    <a href="{{ url('admin/product-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Product List</a>
                </div>
            </div>
            <div class="card-block">
                <?php $stock_alert = Helpers::productStockAlert(); ?>
                <div id="responsive_table">
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Image</th>
                                <th>Stock</th>
                                <th>Alert Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($stock_alert) > 0)
                            <?php $i = 0; ?>
                            @foreach($stock_alert as $row)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Product Name">{{ $row->product_name }}</td>
                                <td data-title="Image"><img src="{{ asset('public/uploads/products/thumbnail/'.$row->product_image) }}" width="120" alt=""></td>
                                <td data-title="Stock">{{ $row->quantity - $row->sale_quantity }}</td>
                                <td data-title="Alert Quantity">{{ $row->alert_quantity}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@include('backend.price_groups.add')
@include('backend.price_groups.edit')

<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    });
</script>
@endsection