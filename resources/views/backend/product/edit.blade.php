@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Edit Product</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/product-list') }}" class="btn btn-primary"><i
                                    class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="form">
                    @if($errors->any())
                            @foreach ($errors->all() as $error)
                                <p class="alert alert-danger">{{$error}}</p>
                            @endforeach
                        @endif
                        <form class="form-horizontal" action="{{ url('/admin/update_product') }}" method="POST"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            <input type="hidden" name="existing_img" value="{{ $product->product_image }}">
							<input type="hidden" name="quantity" value="{{ $product->quantity }}">

                            @if(!Session::has('store_id'))
                                <div class="form-group">
                                    <label class="control-label col-form-label">Store List *</label>
                                    <select class="form-control" name="store_id" required="">
                                        <option value="">Select Store</option>
                                        @foreach($store_list as $row)
                                            <option @if($product->store_id == $row->id) selected=""
                                                    @endif value="{{ $row->id }}">{{ $row->store_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Name * </label>
                                        <input type="text" name="product_name" class="form-control" required=""
                                               value="{{ $product->product_name }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Image   <span class="image_size">(Max: 1024 kb)</span> </label>
                                        <img src="{{asset('public/uploads/products/thumbnail/'.$product->product_image)}}" width="80" alt=""><br>
                                        <input type="file" name="product_image" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Brand</label>
                                        <select class="form-control" name="brand_id">
                                            <option value="">Select Brand</option>
                                            @foreach($brand_list as $row)
                                                <option @if($row->id == $product->brand_id) selected @endif value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Category *</label>
                                        <select class="form-control" name="category_id" id="category_id" required="">
                                            <option value="">Select Category</option>
                                            @foreach($categories as $row)
                                                <option @if($product->category_id == $row->id) selected=""
                                                        @endif value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Sub Category</label>
                                        <select class="form-control" name="subcategory_id" id="subcategory_id">
                                            <option value="">Select Sub Category</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Second Sub Category</label>
                                        <select class="form-control" name="sub_subcategory_id" id="sub_subcategory_id">
                                            <option value="">Select Second Sub Category</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Price * </label>
                                        <input type="text" name="price" class="form-control" required=""
                                               value="{{ $product->price }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Video</label>
                                        <input type="text" name="product_video" class="form-control" value="{{$product->product_video}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Gallery <a href="" class="btn btn-small btn-success" data-toggle="modal" data-target="#view_gallery_modal">View Images</a></label>
                                        <input type="file" name="product_gallery[]" class="form-control" multiple="">
                                    </div>
                                </div>
                            </div>
                            @if(count($price_groups) > 0)
                            <div class="row" style="background: #eee; padding: 10px 0;">
                                <div class="col-md-12">
                                    <h4>Product Price Groups</h4>
                                </div>
                                @foreach($price_groups as $price_group)
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label"> {{$price_group->name}} </label>
                                        <input type="text" name="PriceGroup[{{$price_group->id}}]" class="form-control" value="{{ Helpers::getProductPriceGroupValue($product->id, $price_group->id) }}">
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            @endif

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Units *</label>
                                        <select class="form-control" name="unit_id" required="" id="unit_list">
                                            @foreach($unit_list as $row)
                                                <option {{ ($row->id == $product->unit_id) ? 'selected' : '' }} value="{{ $row->id }}">{{ $row->unit_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Location </label>
                                        <input type="text" name="product_location" class="form-control" value="{{ $product->product_location }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Code </label>
                                        <input type="text" name="product_code" class="form-control"
                                               value="{{ $product->product_code }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Quantity</label>
                                        <input type="text" class="form-control" value="{{ $product->quantity }}" disabled="">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Promo Start Date </label>
                                        <input type="date" name="promotion_start_date" class="form-control"
                                               value="{{ $product->promotion_start_date }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Promo Price</label>
                                        <input type="text" name="promotion_price" class="form-control"
                                               value="{{ $product->promotion_price }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Product Cost </label>
                                        <input type="text" name="cost" class="form-control"
                                               value="{{ $product->cost }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Promo End Date </label>
                                        <input type="date" name="promotion_end_date" class="form-control"
                                               value="{{ $product->promotion_end_date }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Alert Quantity</label>
                                        <input type="text" name="alert_quantity" class="form-control"
                                               value="{{ $product->alert_quantity }}">
                                    </div>
                                    @if(Session::get('role') == 0)
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Status</label>
                                        <select class="form-control" name="status">
                                            <option @if($product->status == 1) selected="" @endif value="1">Active
                                            </option>
                                            <option @if($product->status == 0) selected="" @endif value="0">Inactive
                                            </option>
                                        </select>
                                    </div>
                                    @endif

                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="is_checking_stock" class="control-label col-form-label">Stock
                                                Check</label>
                                            <input type="checkbox" @if($product->is_checking_stock == 1) checked="" @endif  name="is_checking_stock" id="is_checking_stock">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="featured_product"
                                                   class="control-label col-form-label">Featured </label>
                                            <input type="checkbox" @if($product->featured == 1) checked="" @endif  name="featured" id="featured_product">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="is_used"
                                                   class="control-label col-form-label">Is Used ? </label>
                                            <input type="checkbox" @if($product->is_used == 1) checked="" @endif  name="is_used" id="is_used">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="is_home_delivery"
                                                   class="control-label col-form-label">Is Home Delivery ? </label>
                                            <input type="checkbox" @if($product->is_home_delivery == 1) checked="" @endif  name="is_home_delivery" id="is_home_delivery">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="show_per_price"
                                                   class="control-label col-form-label">Show Price ? </label>
                                            <input type="checkbox" @if($product->show_per_price == 1) checked="" @endif  name="show_per_price" id="show_per_price">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-form-label">Description</label>
                                        <textarea name="product_description" class="form-control"
                                                  placeholder="Product Description">{{ $product->product_description }}</textarea>
                                    </div>
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-info">Update</button>
                                    </div>
                                </div>
                            </div>


                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .modal {
            z-index: 999999999;
        }
        .gallery-image {
            position: relative;
        }
        .gallery-image span.badge {
            position: absolute;
            top: 0;
            right: 15px;
            cursor: pointer;
        }
    </style>
    <!--view gallery images modal-->
    <div class="modal fade" id="view_gallery_modal" tabindex="-1" role="dialog" aria-labelledby="view_gallery_modal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Gallery Images</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @if(count($gallery_images) > 0)
                            @foreach($gallery_images as $image)
                            <div class="col-md-4 col-6 gallery-image" style="margin-bottom: 15px;" id="gallery_item_{{$image->id}}">
                                <img class="img-fluid" src="{{asset('public/uploads/products/gallery/'.$image->image_url)}}" alt="">
                                <span class="badge badge-danger delete-gallery-image" data-id="{{$image->id}}"><i class="fa fa-trash"></i></span>
                            </div>
                            @endforeach
                        @else
                        <div class="col-md-12">
                            <p>Gallery Image Not Found</p>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('/') }}assets/plugins/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('product_description');

        $(document).ready(function () {
            $('#unit_list').select2();
            
            function get_selected_category(id, selecttor) {
                if (id) {
                    var url = '{{ url('/admin/selected_subcategory/') }}' + '/' + id;
                    $.ajax({
                        url: url,
                        method: "GET",
                        success: function (response) {
                            $('#'+selecttor).html(response);
                        },
                    });
                }
            }

            var subcategory_id = '{{ $product->subcategory_id }}';
            get_selected_category(subcategory_id, 'subcategory_id');
            var sub_subcategory_id = '{{ $product->sub_subcategory_id }}';
            get_selected_category(sub_subcategory_id, 'sub_subcategory_id');

            $('#category_id').change(function () {
                var id = $(this).val();
                if (id == '') {
                    var output = '<option value="">Select Sub Category</option>';
                    $('#subcategory_id').html(output);
                } else {
                    var url = '{{ url('/admin/subcategory_items/') }}' + '/' + id;
                    $.ajax({
                        url: url,
                        method: "GET",
                        success: function (response) {
                            $('#subcategory_id').html(response);
                        },
                    });
                }
            });

            $('#subcategory_id').change(function () {
                var id = $(this).val();
                if (id == '') {
                    var output = '<option value="">Select Second Sub Category</option>';
                    $('#sub_subcategory_id').html(output);
                } else {
                    var url = '{{ url('/admin/subcategory_items/') }}' + '/' + id;
                    $.ajax({
                        url: url,
                        method: "GET",
                        success: function (response) {
                            $('#sub_subcategory_id').html(response);
                        },
                    });
                }
            });

            $('.delete-gallery-image').click(function() {
                var id = $(this).data('id');
                var url = '{{ url('/admin/delete_gallery_image/') }}' + '/' + id;
                $.ajax({
                    url: url,
                    method: "GET",
                    success: function (response) {
                        if(response == 'DONE') {
                            $('#gallery_item_'+id).remove();
                        }
                    },
                });
            });

        });
    </script>
@endsection