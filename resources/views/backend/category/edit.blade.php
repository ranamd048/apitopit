@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Edit Category</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/category-list/'.$category->category_type) }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    <form class="form-horizontal" action="{{ url('/admin/update_category') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="category_id" value="{{ $category->id }}">
                        <input type="hidden" name="existing_img" value="{{ $category->image }}">
                        <input type="hidden" name="category_type" value="{{ $category->category_type }}">
                        <input type="hidden" name="parent_id" value="0">
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Category Type</label>
                            <div class="col-sm-10">
                                <select class="form-control" disabled="">
                                    <option @if($category->category_type == 'shop') selected="" @endif value="shop">Shop</option>
                                    <option @if($category->category_type == 'service') selected="" @endif value="service">Service</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Category Name * </label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" required="" value="{{ $category->name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Category Image</label>
                            <div class="col-sm-10">
                                @if($category->image)
                                <img src="{{ asset('public/uploads/category/'.$category->image) }}" width="120" alt=""><br><br>
                                @endif
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Sort Order</label>
                            <div class="col-sm-10">
                                <input type="number" name="sort_order" class="form-control" value="{{ $category->sort_order}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option @if($category->status == 1) selected="" @endif value="1">Active</option>
                                    <option @if($category->status == 0) selected="" @endif value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection