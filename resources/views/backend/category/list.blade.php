@extends('backend.layout')

@section('maincontent')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title text-capitalize">{{ $type }} Category List</h4>
                @if(Session::get('role') == 0)
                <div class="back-button">
                    <a href="{{ url('admin/create-category?type='.$type) }}" class="btn btn-primary text-capitalize"><i class="fa fa-plus"></i>Add {{ $type }} category</a>
                </div>
                @endif
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Sort Order</th>
                                <th>status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($categories) > 0)
                            
                            <?php $i = 0; ?>
                            @foreach($categories as $row)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Name">{{ $row->name }}</td>
                                <td data-title="Image"><img src="{{ asset('/public/uploads/category/'.$row->image) }}" width="120" alt=""></td>
                                <td data-title="Sort Order">{{$row->sort_order}}</td>
                                <td data-title="Status">{{ ($row->status == 1) ? 'Active' : 'Inactive' }}</td>
                                <td data-title="Action">
                                    <a class="btn btn-primary" href="{{ url('/admin/sub-categories/'.$row->id) }}">Sub Items</i>
                                    </a>
                                    @if(Session::get('role') == 0)
                                    <a class="btn btn-success" href="{{ url('/admin/edit-category/'.$row->id) }}"><i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure want to delete?');" href="{{ url('/admin/delete-category/'.$row->id) }}"><i class="fa fa-trash"></i>
                                    </a>
                                    @endif

                                    @if(Session::get('role') == 1 && Session::get('store_id') == $row->store_id)
                                    <a class="btn btn-success" href="{{ url('/admin/edit-category/'.$row->id) }}"><i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure want to delete?');" href="{{ url('/admin/delete-category/'.$row->id) }}"><i class="fa fa-trash"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    } );
</script>
@endsection