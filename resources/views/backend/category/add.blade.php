@extends('backend.layout')
<?php $type = isset($_GET['type']) ? $_GET['type'] : 'shop';?>

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Add New Category</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/category-list/'.$type) }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/save_category') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="parent_id" value="0">
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Category Type</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="category_type">
                                    <option @if($type == 'shop') selected @endif value="shop">Shop</option>
                                    <option @if($type == 'service') selected @endif value="service">Service</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Category Name * </label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Category Image</label>
                            <div class="col-sm-10">
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Sort Order</label>
                            <div class="col-sm-10">
                                <input type="number" name="sort_order" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection