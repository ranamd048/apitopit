@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title"><strong>{{ $category->name }}</strong> Sub Category List</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/sub-categories/'.$category->parent_id) }}" class="btn btn-primary"><i
                                    class="fa fa-arrow-left"></i> Back</a>
                        <a href="{{ url('admin/add-subcategory/'.$category->id.'?level=2') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Image</th>
                                <th>Sort Order</th>
                                <th>status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($sub_categories) > 0)

                                <?php $i = 0; ?>
                                @foreach($sub_categories as $row)
                                    <?php $i++; ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Name">{{ $row->name }}</td>
                                        <td data-title="Type">{{ $row->category_type }}</td>
                                        <td data-title="Image"><img src="{{ asset('/public/uploads/category/'.$row->image) }}" width="120"
                                                 alt=""></td>
                                        <td data-title="Sort Order">{{ $row->sort_order }}</td>
                                        <td data-title="Status">{{ ($row->status == 1) ? 'Active' : 'Inactive' }}</td>
                                        <td data-title="Action">
                                            @if(Session::get('role') == 0)
                                                <a class="btn btn-success"
                                                   href="{{ url('/admin/edit-subcategory/'.$row->id) }}"><i
                                                            class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-danger"
                                                   onclick="return confirm('Are you sure want to delete?');"
                                                   href="{{ url('/admin/delete-category/'.$row->id) }}"><i
                                                            class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                            @if(Session::get('role') == 1 && Session::get('store_id') == $row->store_id)
                                                <a class="btn btn-success"
                                                   href="{{ url('/admin/edit-subcategory/'.$row->id) }}"><i
                                                            class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-danger"
                                                   onclick="return confirm('Are you sure want to delete?');"
                                                   href="{{ url('/admin/delete-category/'.$row->id) }}"><i
                                                            class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#common_datatable_id').DataTable();
        });
    </script>
@endsection