@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Upload Documents</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/staff-list') }}" class="btn btn-primary"><i
                                    class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    @if(count($document_list) > 0)
                    <div id="responsive_table">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Document Title</th>
                                <th>View Document</th>
                                <th>Uploaded By</th>
                                <th>Uploaded Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($document_list as $row)
                                <?php
                                $i++;
                                $date = new DateTime($row->modified_date);
                                ?>
                                <tr>
                                    <td data-title="Sl">{{ $i }}</td>
                                    <td data-title="Title">{{$row->doc_title}}</td>
                                    <td data-title="View"><a target="_blank" href="{{asset('public/uploads/documents/'.$row->doc_url)}}" class="btn btn-success"><i class="fa fa-eye"></i> View Document</a></td>
                                    <td data-title="Uploaded By">{{$row->name}}</td>
                                    <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                    <div class="form">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <form class="form-horizontal" action="{{ url('/admin/save_document_items') }}" method="POST"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" id="doc_serial_num" value="1">
                            <input type="hidden" name="table_name" value="{{$table}}">
                            <input type="hidden" name="row_id" value="{{$row_id}}">
                            <div class="row">
                                <input type="hidden" name="doc_number[]" value="1">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="">Document Title *</label>
                                        <input type="text" name="doc_title_1" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="">Upload Document *</label>
                                        <input type="file" name="doc_file_1" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="">Sort Order *</label>
                                        <input type="number" name="sort_order_1" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                            <div id="show_another_document"></div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-success" id="add_another_document"><i
                                                class="fa fa-plus"></i> Add Another
                                    </button>
                                    <br><br>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("#add_another_document").click(function () {
                var serial = parseInt($('#doc_serial_num').val());
                serial = (serial + 1);
                $('#doc_serial_num').val(serial);

                var doc_html = '<div class="row" id="doc_serial_' + serial + '">\n' +
                    '                                <input type="hidden" name="doc_number[]" value="' + serial + '">\n' +
                    '                                <div class="col-sm-4">\n' +
                    '                                    <div class="form-group">\n' +
                    '                                        <label for="">Document Title *</label>\n' +
                    '                                        <input type="text" name="doc_title_' + serial + '" class="form-control" required="">\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                                <div class="col-sm-4">\n' +
                    '                                    <div class="form-group">\n' +
                    '                                        <label for="">Upload Document *</label>\n' +
                    '                                        <input type="file" name="doc_file_' + serial + '" class="form-control" required="">\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                                <div class="col-sm-3">\n' +
                    '                                    <div class="form-group">\n' +
                    '                                        <label for="">Sort Order *</label>\n' +
                    '                                        <input type="number" name="sort_order_' + serial + '" class="form-control" required="">\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                                <div class="col-sm-1">\n' +
                    '                                    <div class="form-group" style="margin-top: 23px;">\n' +
                    '                                        <button type="button" class="btn btn-danger remove_doc" data-num="' + serial + '"><i class="fa fa-trash"></i></button>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                            </div>';

                $('#show_another_document').append(doc_html);
            });

            $(document).on('click', '.remove_doc', function () {
                var num = $(this).data('num');
                $('#doc_serial_' + num).remove();
            });
        });
    </script>
@endsection