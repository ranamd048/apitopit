<style>
@media only screen and (max-width: 767px) {

}
</style>
<?php $shop_count = DB::table('stores')->select('id')->count(); ?>
<div id="responsive_table">
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone No</th>
            <th>Address</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @if(count($customer_list) > 0)
            <?php $i = 0;?>
            @foreach($customer_list as $row)
                <?php 
                    $i++; 
                    if($row->status == 1) {
                        $status = '<span class="badge badge-success">Active</span>';
                    } else {
                        $status = '<span class="badge badge-danger">Inactive</span>';
                    } 
                ?>
                <tr>
                    <td data-title="Sl">{{ $i }}</td>
                    <td data-title="Name">{{ $row->name }}</td>
                    <td data-title="Email">{{ $row->email }}</td>
                    <td data-title="Phone">{{ $row->phone }}</td>
                    <td data-title="Address">{{ $row->address }}</td>
                    <td data-title="Status">
                        <?php echo $status; ?>
                        <a href="#" class="btn btn-primary btn-sm load_modal" data-toggle="modal" data-action="{{ url('admin/customer-pay-now?due='.'&user_id='.$row->id) }}">Add Payment</a>
                    </td>
                    @if(Session::get('role') == 0 && $shop_count != 1)
                    <td data-title="Action">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".edit_customer_modal" data-id="{{$row->id}}" data-name="{{ $row->name }}" data-email="{{ $row->email }}" data-status="{{ $row->status }}"><i class="fa fa-pencil"></i></button>
                    </td>
                    @endif
                    @if(Session::get('role') == 1 || $shop_count == 1)
                    <td data-title="Action">
                        <div class="dropdown">
                            <a class="btn btn-info dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-list"></i>
                            </a>
                             
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a href="{{ url('admin/customer-details/'.$row->id) }}" class="dropdown-item">Customer Details</a>
                                <a href="{{ url('admin/customer-payment-history/'.$row->id) }}" class="dropdown-item">Paypent History</a>
                                <a href="{{ url('admin/customer-balance-statement/'.$row->id) }}" class="dropdown-item">Balance Statement</a>
                                <a href="{{ url('admin/edit-customer/'.$row->id) }}" class="dropdown-item">Edit Customer</a>
                            </div>
                        </div>
                    </td>
                    @endif
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>