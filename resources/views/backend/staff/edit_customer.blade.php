@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Edit Customer</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/customer-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="form">
                        @if($errors->any())
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach ($errors->all() as $error)
                                        <p class="alert alert-danger">{{$error}}</p>
                                    @endforeach
                                </div>
                            </div>
                            <br>
                        @endif
                        <form class="form-horizontal" action="{{ url('/admin/customer/update') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Name *</label>
                                        <input type="text" class="form-control" name="name" value="{{ $customer->name }}" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="email" class="form-control" value="{{ $customer->email }}" name="email">
                                    </div>
                                    <div class="form-group">
                                    <?php $divisions = Helpers::getDivisionList(); ?>
                                        <label for="">Division *</label>
                                        <select name="division" id="division_list" class="form-control" required>
                                            <option value="">Select Division</option>
                                            @foreach($divisions as $row)
                                            <option @if($customer->division == $row->id) selected="" @endif value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">District *</label>
                                        <select name="district" id="district_list" class="form-control" required>
                                            <option value="{{$customer->district}}">{{Helpers::districtName($customer->district)}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Upazila *</label>
                                        <select name="upazila" id="upazila_list" class="form-control" required>
                                            <option value="{{$customer->thana}}">{{Helpers::upazilaName($customer->thana)}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Address *</label>
                                        <input type="text" class="form-control" name="address" value="{{$customer->address}}" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            //get district/upazila
            function get_selected_district_upazila(id, type) {
                var url = "{{ url('get_district_upazila') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, type: type, data_format: 'html'},
                    cache: false,
                    success: function (response) {
                        $('#'+type).html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            $('#division_list').change(function() {
                var division_id = $(this).val();
                if(division_id == '') {
                    $('#district_list').html('<option value="">Select District</option>');
                } else {
                    get_selected_district_upazila(division_id, 'district_list');
                }
                $('#upazila_list').html('<option value="">Select Upazila</option>');
            });

            $('#district_list').change(function() {
                var district_id = $(this).val();
                get_selected_district_upazila(district_id, 'upazila_list');
            });
        });
    </script>
@endsection