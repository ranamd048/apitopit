@extends('backend.layout')
@section('title_text') ({{ $customer->name }}) Balance Statement @endsection

@section('maincontent')
<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
</style>
<?php 
    if((isset($_GET['start_date']) && !empty($_GET['start_date'])) && (isset($_GET['end_date']) && !empty($_GET['end_date']))) {
        $date_filter_params = '?start_date='.$_GET['start_date'].'&end_date='.$_GET['end_date'];
    } else {
        $date_filter_params = '';
    }
?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Customer Balance Statement</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/customer-balance-statement-print/'.$customer->id.$date_filter_params) }}" class="btn btn-success"><i class="fa fa-print"></i>Print</a>
                        <a href="{{ url('admin/customer-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <h3>Customer Details</h3>
                    <div id="responsive_table">
                        <table class="table table-bordered table-hover">
                            <thead class="bg-light">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-title="Name">{{ $customer->name }}</td>
                                    <td data-title="Email">{{ $customer->email }}</td>
                                    <td data-title="Phone">{{ $customer->phone }}</td>
                                    <td data-title="Address">{{ $customer->address }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-filter">
                                <form action="" method="GET">
                                    <div class="form-group row">
                                        <div class="col-md-5">
                                            <label for="start_date">From Date</label>
                                            <input type="date" name="start_date" id="start_date" class="form-control" value="{{isset($_GET['start_date']) ? $_GET['start_date'] : ''}}">
                                        </div>
                                        <div class="col-md-5">
                                            <label for="end_date">To Date</label>
                                            <input type="date" name="end_date" id="end_date" class="form-control" value="{{isset($_GET['end_date']) ? $_GET['end_date'] : ''}}">
                                        </div>
                                        <div class="col-md-2">
                                            <button style="margin-top: 34px; float:left; margin-right: 5px;" type="submit" class="btn btn-success"><i class="fa fa-check-square-o" aria-hidden="true"></i></button>
                                            <a style="margin-top: 34px; float: left; color: #fff;" class="btn btn-danger" href="{{ url('admin/customer-balance-statement/'.$customer->id) }}"><i class="fa fa-close"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <h3>Balance Statement</h3>
                    <div id="responsive_table">
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead class="bg-light">
                            <tr>
                                <th>Date</th>
                                <th>Reference</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-title="Date">-----</td>
                                    <td data-title="Reference">Pre Statement</td>
                                    <td data-title="Debit">৳{{number_format($pre_statement['pre_debit'], 2)}}</td>
                                    <td data-title="Credit">৳{{number_format($pre_statement['pre_credit'], 2)}}</td>
                                    <td data-title="Balance">৳{{number_format($pre_statement['pre_balance'], 2)}}</td>
                                </tr>
                                <?php
                                    $total_debit = $pre_statement['pre_debit'];
                                    $total_credit = $pre_statement['pre_credit'];
                                ?>
                                @if(count($customer_order_list) > 0)
                                    <?php $current_balance = $pre_statement['pre_balance']; ?>
                                    @foreach($customer_order_list as $row)
                                    <?php 
                                        $order_date = new DateTime($row->order_date);
                                        $current_balance += $row->subtotal;
                                        $total_debit += $row->subtotal;
                                    ?>
                                    <tr>
                                        <td data-title="Date">{{$order_date->format('d-F-Y')}}</td>
                                        <td data-title="Reference">Order {{$row->id}}</td>
                                        <td data-title="Debit">৳{{number_format($row->subtotal, 2)}}</td>
                                        <td data-title="Credit"></td>
                                        <td data-title="Balance">৳{{number_format($current_balance, 2)}}</td>
                                    </tr>
                                    <?php $order_payments = Helpers::getInvoicePaymentList($customer->id, $row->invoice_id); ?>
                                    @if(count($order_payments) > 0)
                                    @foreach($order_payments as $payment)
                                    <?php 
                                        $payment_date = new DateTime($payment->payment_date);
                                        $payment_method = ($payment->payment_method == 'cod') ? 'Cash Payment' : $payment->payment_method;
                                        $current_balance -= $payment->paid_amount;
                                        $total_credit += $payment->paid_amount;
                                    ?>
                                    <tr>
                                        <td data-title="Date">{{$payment_date->format('d-F-Y')}}</td>
                                        <td data-title="Reference">{{$payment_method}}</td>
                                        <td data-title="Debit"></td>
                                        <td data-title="Credit">৳{{number_format($payment->paid_amount, 2)}}</td>
                                        <td data-title="Balance">৳{{number_format($current_balance, 2)}}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    @endforeach
                                @endif
                            </tbody>
                            <tfoot class="bg-light">
                                <tr>
                                    <th></th>
                                    <th>Total Summary =</th>
                                    <th>৳{{number_format($total_debit, 2)}}</th>
                                    <th>৳{{number_format($total_credit, 2)}}</th>
                                    <th>৳{{number_format($total_debit - $total_credit, 2)}}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/datatable/buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/buttons.print.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable({
                dom: 'Bfrtip',
                // buttons: [
                //     'csv', 'excel', 'pdf', 'print'
                // ]
                buttons: [
                    { extend: 'excelHtml5', footer: true },
                    { extend: 'csvHtml5', footer: true },
                    { extend: 'pdfHtml5', footer: true },
                    { extend: 'print', footer: true }
                ]
            });
        });
    </script>
@endsection