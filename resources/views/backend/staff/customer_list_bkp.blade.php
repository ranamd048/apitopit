@extends('backend.layout')

@section('maincontent')
<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
    @media only screen and (max-width: 767px)  {
        #responsive_table table, #responsive_table thead, #responsive_table tbody, #responsive_table th, #responsive_table td, #responsive_table tr {
            overflow: unset !important;
        }
    }
</style>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Customer List</h4>
                    <div class="back-button">
                    @if(Session::get('role') == 0)
                        <a href="{{ url('admin/number_export/users/phone') }}" class="btn btn-success"><i class="fa fa-download"></i>Number Export</a>
                    @endif
                        <a href="{{ url('admin/customer/export') }}" class="btn btn-success"><i class="fa fa-download"></i>Export Data</a>
                        <a href="{{ url('admin/customer/add') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add  Customer</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-filter">
                            <input type="hidden" id="current_shop_id" value="{{$shop_id}}">
                                <?php $divisions = Helpers::getDivisionList(); ?>
                                <div class="form-group row">
                                    <?php $col_num = 4; ?>
                                    @if(!Session::has('store_id'))
                                    <div class="col-md-3">
                                        <label for="">Search by Shop</label>
                                        <select name="shop_id" id="shop_list" class="form-control">
                                            @foreach($shop_list as $shop)
                                            <option value="{{$shop->id}}">{{$shop->store_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <?php $col_num = 3; ?>
                                    @endif

                                    <div class="col-sm-{{$col_num}}">
                                        <label class="control-label col-form-label" for="">Division</label>
                                        <select name="division" id="division_list" class="form-control">
                                            <option value="">Select Division</option>
                                            @foreach($divisions as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-{{$col_num}}">
                                        <label class="control-label col-form-label" for="">District</label>
                                        <select name="district" id="district_list" class="form-control">
                                            <option value="">Select District</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-{{$col_num}}">
                                        <label class="control-label col-form-label" for="">Upazila</label>
                                        <select name="thana" id="upazila_list" class="form-control">
                                        <option value="">Select Upazila</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div id="load_customer_list"></div>
                </div>
            </div>
        </div>
    </div>
    <!--Customer Edit Modal-->
    <div class="modal fade edit_customer_modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <form action="{{ url('admin/update_customer_info') }}" method="POST">
        {{csrf_field()}}
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Update Customer</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="customer_id" id="customer_id">
            <div class="form-group">
                <label for="email" class="col-form-label">Email :</label>
                <input type="email" class="form-control" name="email" id="email">
            </div>
            <div class="form-group">
                <label for="password" class="col-form-label">Password :</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
            <div class="form-group">
                <label class="control-label col-form-label">Status :</label>
                <select class="form-control" name="status" id="status">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </div>
        </form>
    </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.edit_customer_modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var id = button.data('id');
                var name = button.data('name');
                var email = button.data('email');
                var status = button.data('status');
                var modal = $(this);
                modal.find('.modal-title').text('Update for ' + name);
                modal.find('.modal-body input#customer_id').val(id);
                modal.find('.modal-body input#email').val(email);
                modal.find('.modal-body select#status').val(status);
            });

            var shop_id = $('#current_shop_id').val();
            //load shop list
        function load_customer_list(shop_id, id= '', type= '') {
            var url = APP_URL + '/admin/load-customer-list';
            var loading = '<div class="col-md-12 text-center"><i class="fa fa-spinner fa-spin"></i></div>';
            $('#load_customer_list').html(loading);
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: {shop_id: shop_id, id: id, type: type},
                cache: false,
                success: function(response) {
                    $('#load_customer_list').html(response);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        load_customer_list(shop_id);

        //get district/upazila
        function get_selected_district_upazila(id, type) {
            var url = "{{ url('get_district_upazila') }}";
            $.ajax({
                method: "POST",
                url: url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {id: id, type: type},
                cache: false,
                success: function (response) {
                    $('#'+type).html(response);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $('#shop_list').change(function() {
            var id = $(this).val();
            if (id != '') {
                $('#current_shop_id').val(id);
                load_customer_list(id);
            }
        });

        $('#division_list').change(function() {
            var shop_id = $('#current_shop_id').val();
            var division_id = $(this).val();
            if(division_id == '') {
                $('#district_list').html('<option value="">Select District</option>');
            } else {
                load_customer_list(shop_id, division_id, 'division');
                get_selected_district_upazila(division_id, 'district_list');
            }
            $('#upazila_list').html('<option value="">Select Upazila</option>');
        });
        $('#district_list').change(function() {
            var shop_id = $('#current_shop_id').val();
            var district_id = $(this).val();
            load_customer_list(shop_id, district_id, 'district');
            get_selected_district_upazila(district_id, 'upazila_list');
        });
        $('#upazila_list').change(function() {
            var shop_id = $('#current_shop_id').val();
            var upazila_id = $(this).val();
            load_customer_list(shop_id, upazila_id, 'upazila');
        });

    });
</script>
@endsection