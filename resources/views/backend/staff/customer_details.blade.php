@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Customer Details</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/customer-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <h3>Customer Details</h3>
                    @if (Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td><strong>Name</strong></td>
                                <td>{{ $result->name }}</td>
                            </tr>
                            <tr>
                                <td><strong>Email</strong></td>
                                <td>{{ $result->email }}</td>
                            </tr>
                            <tr>
                                <td><strong>Phone</strong></td>
                                <td>{{ $result->phone }}</td>
                            </tr>
                            <tr>
                                <td><strong>Address</strong></td>
                                <td>{{ $result->address }}</td>
                            </tr>
                            <tr>
                                <td><strong>Upazila</strong></td>
                                <td>{{ Helpers::upazilaName($result->thana) }}</td>
                            </tr>
                            <tr>
                                <td><strong>District</strong></td>
                                <td>{{ Helpers::districtName($result->district) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Division</strong></td>
                                <td>{{ Helpers::divisionName($result->division) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Photo</strong></td>
                                <td><img width="120" src="{{ asset('public/uploads/customer/'.$result->photo) }}" alt="{{ $result->name }}"></td>
                            </tr>
                            <tr>
                                <td><strong>Balance</strong></td>
                                <td>৳{{ number_format($result->current_balance, 2) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Total Due</strong></td>
                                <td>৳{{ number_format($result->total_due_amount, 2) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Total Paid</strong></td>
                                <td>৳{{ number_format($result->total_paid_amount, 2) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Pay Now</strong></td>
                                <td><a href="#" class="btn btn-primary load_modal" data-toggle="modal" data-action="{{ url('admin/customer-pay-now?due='.$result->total_due_amount.'&user_id='.$result->id) }}">Click Here To Pay Now</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection