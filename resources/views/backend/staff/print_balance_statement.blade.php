<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Customer Balance Statement</title>
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <style>
        .system-logo img {
            width: 160px !important;
        }
        .system-logo {overflow: hidden;margin-top: 15px;margin-bottom: 20px;}
        .system-logo h1 {
            display: inline-block;
            margin-left: 20px;
            font-size: 26px;
            text-transform: uppercase;
            font-weight: 700;
        }
        .print-section-wrapper {
            padding: 20px;
            overflow: hidden;
        }
        .system-info h1 {
            font-size: 42px;
            text-transform: uppercase;
        }
        .print-section-wrapper .header {
            overflow: hidden;
            margin-bottom: 40px;
        }
        .print-section-wrapper .header img {
            width: 100%;
            height: auto;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #333 !important;
        }
        .system-info p {
            margin-bottom: 5px;
            font-weight: 700;
        }
        .invoice-text {
            text-align: center;
            margin-top: 20px;
        }
        .invoice-text p {
            background-color: #333;
            display: inline-block;
            color: #fff;
            padding: 10px 30px;
            text-transform: uppercase;
        }
        .customer-details {
            margin-bottom: 30px;
            overflow: hidden;
        }
        .customer-details p {
            font-weight: 700;
            border-bottom: 1px dashed #333;
            display: block;
        }
        .nit-price {
            overflow: hidden;
        }
        .nit-price p {
            font-weight: 700;
            text-align: right;
            margin-bottom: 5px;
        }
        p.in-word {
            font-weight: 700;
            margin-top: 40px;
        }
        .signature-wrapper {
            overflow: hidden;
            margin-top: 40px;
        }
        .signature-wrapper p {
            font-weight: 700;
            border-top: 1px dashed #333;
            display: inline-block;
        }
        @media print {
            body {background-color: #FFF4AA !important; -webkit-print-color-adjust: exact;}
        }
        @media only screen and (max-width: 767px) {
            .print-section-wrapper {
                overflow: scroll;
            }
            .row.system-info .text-right {
                text-align: left !important;
            }
            .nit-price p {
                text-align: left;
            }

        }
    </style>
</head>
<body>
    <div class="print-section-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <h6 class="text-center">Bismillahir Rahmanir Rahim</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="system-logo">
                                <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid">
                                <h1>{{$shop_details->store_name}}</h1>
                                </div>
                                
                            </div>
                        </div>
                        <div class="invoice-text">
                            <p>Customer Balance Statement</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="customer-details">
                <div class="row">
                    <div class="col-md-6">
                        <p>Customer Name : {{$customer->name}}</p>
                    </div>
                    <div class="col-md-6">
                        <p>Email : {{$customer->email}}</p>
                    </div>
                    <div class="col-md-6">
                        <p>Phone : {{$customer->phone}}</p>
                    </div>
                    <div class="col-md-6">
                        <p>Address : {{$customer->address}}</p>
                    </div>
                    <?php if(!empty($from_date) && !empty($to_date)) : ?>
                    <div class="col-md-6">
                        <p>From Date : {{date("F d, Y", strtotime($from_date))}}</p>
                    </div>
                    <div class="col-md-6">
                        <p>To Date : {{date("F d, Y", strtotime($to_date))}}</p>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="order-summary">
                        <table class="table table-bordered table-hover">
                            <thead class="bg-light">
                            <tr>
                                <th>Date</th>
                                <th>Reference</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-title="Date">-----</td>
                                    <td data-title="Reference">Pre Statement</td>
                                    <td data-title="Debit">৳{{number_format($pre_statement['pre_debit'], 2)}}</td>
                                    <td data-title="Credit">৳{{number_format($pre_statement['pre_credit'], 2)}}</td>
                                    <td data-title="Balance">৳{{number_format($pre_statement['pre_balance'], 2)}}</td>
                                </tr>
                                <?php
                                    $total_debit = $pre_statement['pre_debit'];
                                    $total_credit = $pre_statement['pre_credit'];
                                ?>
                                @if(count($customer_order_list) > 0)
                                    <?php $current_balance = $pre_statement['pre_balance']; ?>
                                    @foreach($customer_order_list as $row)
                                    <?php 
                                        $order_date = new DateTime($row->order_date);
                                        $current_balance += $row->subtotal;
                                        $total_debit += $row->subtotal;
                                    ?>
                                    <tr>
                                        <td data-title="Date">{{$order_date->format('d-F-Y')}}</td>
                                        <td data-title="Reference">Order {{$row->id}}</td>
                                        <td data-title="Debit">৳{{number_format($row->subtotal, 2)}}</td>
                                        <td data-title="Credit"></td>
                                        <td data-title="Balance">৳{{number_format($current_balance, 2)}}</td>
                                    </tr>
                                    <?php $order_payments = Helpers::getInvoicePaymentList($customer->id, $row->invoice_id); ?>
                                    @if(count($order_payments) > 0)
                                    @foreach($order_payments as $payment)
                                    <?php 
                                        $payment_date = new DateTime($payment->payment_date);
                                        $payment_method = ($payment->payment_method == 'cod') ? 'Cash Payment' : $payment->payment_method;
                                        $current_balance -= $payment->paid_amount;
                                        $total_credit += $payment->paid_amount;
                                    ?>
                                    <tr>
                                        <td data-title="Date">{{$payment_date->format('d-F-Y')}}</td>
                                        <td data-title="Reference">{{$payment_method}}</td>
                                        <td data-title="Debit"></td>
                                        <td data-title="Credit">৳{{number_format($payment->paid_amount, 2)}}</td>
                                        <td data-title="Balance">৳{{number_format($current_balance, 2)}}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    @endforeach
                                @endif
                            </tbody>
                            <tfoot class="bg-light">
                                <tr>
                                    <th></th>
                                    <th>Total Summary =</th>
                                    <th>৳{{number_format($total_debit, 2)}}</th>
                                    <th>৳{{number_format($total_credit, 2)}}</th>
                                    <th>৳{{number_format($total_debit - $total_credit, 2)}}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.print();
    </script>
</body>
</html>