@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Staff List</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/create-staff') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add  Staff</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Shop</th>
                                <th>Position</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($result) > 0)
                                <?php $i = 0; ?>
                                @foreach($result as $row)
                                    <?php $i++; ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Name">{{ $row->name }}</td>
                                        <td data-title="Email">{{ $row->email }}</td>
                                        <td data-title="Phone">{{ $row->phone }}</td>
                                        <td data-title="Shop">{{ $row->store_name }}</td>
                                        <td data-title="Position"><span class="badge badge-success">{{Helpers::authorityPosition($row->position_id)}}</span></td>
                                        <td data-title="Action">
                                            <a href="{{url('/admin/upload_documents/admins/'.$row->id)}}" class="btn btn-success" title="Upload Document"><i class="fa fa-upload"></i></a>
                                            <a href="{{url('/admin/edit-staff/'.$row->id)}}" class="btn btn-info" title="Edit"><i class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable();
        } );
    </script>
@endsection