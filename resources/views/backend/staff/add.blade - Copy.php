@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Add New {{(Session::get('admin_type') == 1) ? 'Staff' : 'Sales Representative'}}</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/staff-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="form">
                        <form class="form-horizontal" action="{{ url('/admin/save_staff') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="row">

                                <div class="col-md-12">
                                    @if(!Session::has('store_id'))
                                    <div class="form-group">
                                        <label for="">Shop List *</label>
                                        <select name="store_id" id="store_id" class="form-control {{ ($errors->has('store_id')) ? 'is-invalid' : '' }}" required="">
                                            <option value="">Select Shop</option>
                                            @foreach($shop_list as $row)
                                            <option value="{{$row->id}}">{{$row->store_name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('store_id'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('store_id') }}
                                            </div>
                                        @endif
                                    </div>
                                    @else
                                        <input type="hidden" name="store_id" value="{{Session::get('store_id')}}">
                                    @endif
                                    @if(Session::get('admin_type') == 1)
                                    <div class="form-group">
                                        <label for="">Staff Type *</label>
                                        <select name="staff_role" id="staff_role" class="form-control" required="">
                                            <option value="">Select Type</option>
                                            <option value="2">Sales Representative</option>
                                            <option value="1">Sales Executive</option>
                                        </select>
                                    </div>
                                    @else
                                            <input type="hidden" name="staff_role" value="2">
                                    @endif
                                    <div class="form-group" id="module_permission" style="display: none;">
                                        <label for="">Module Permission *</label><br>
                                        <input type="checkbox" checked="" name="product_access" id=""> Product <br>
                                        <input type="checkbox" checked="" name="shop_settings" id=""> Shop Settings<br>
                                        <input type="checkbox" name="order_access" id=""> Order <br>
                                        <input type="checkbox" name="service_access" id=""> Service <br>
                                    </div>
                                    @if(Session::get('admin_type') == 1)
                                    <div class="form-group" id="reference_by" style="display: none;">
                                        <label for="">Reference By *</label>
                                        <select name="parent_id" id="show_references" class="form-control">
                                            <option value="">Please Select a Shop at First</option>
                                        </select>
                                    </div>
                                    @else
                                            <input type="hidden" name="parent_id" value="{{Session::get('admin_id')}}">
                                    @endif

                                    <div class="form-group">
                                        <label for="">Name *</label>
                                        <input type="text" class="form-control {{ ($errors->has('name')) ? 'is-invalid' : '' }}" name="name">
                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="email" class="form-control" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Phone * (Ex: 01779221842)</label>
                                        <input type="text" class="form-control {{ ($errors->has('phone')) ? 'is-invalid' : '' }}" name="phone">
                                        @if($errors->has('phone'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('phone') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Division</label>
                                        <select name="division" id="division_list" class="form-control">
                                            <option value="">Select Division</option>
                                            @foreach($divisions as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">District</label>
                                        <select name="district" id="district_list" class="form-control">
                                            <option value="">Select District</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Upazila</label>
                                        <select name="thana" id="upazila_list" class="form-control">
                                            <option value="">Select Upazila</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Password *</label>
                                        <input type="password" class="form-control {{ ($errors->has('password')) ? 'is-invalid' : '' }}" name="password">
                                        @if($errors->has('password'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Confirm Password *</label>
                                        <input type="password" class="form-control {{ ($errors->has('password_confirmation')) ? 'is-invalid' : '' }}" name="password_confirmation">
                                        @if($errors->has('password_confirmation'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password_confirmation') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            function get_shop_staff_list(shop_id) {
                var url = '{{url('admin/get_shop_owner')}}';
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {shop_id: shop_id},
                    cache: false,
                    success: function (response) {
                        $('#show_references').html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            @if(Session::has('store_id'))
                get_shop_staff_list('{{Session::get('store_id')}}');
            @endif

            $('#store_id').change(function() {
                var shop_id = $(this).val();
                get_shop_staff_list(shop_id);
            });

            $('#staff_role').change(function() {
                var value = $(this).val();
                if(value == '2') {
                    $('#reference_by').show();
                    $('#module_permission').hide();
                } else if(value == '1') {
                    $('#module_permission').show();
                    $('#reference_by').hide();
                }
                else {
                    $('#reference_by').hide();
                    $('#module_permission').hide();
                }

            });

            //get district/upazila
            function get_selected_district_upazila(id, type) {
                var url = "{{ url('get_district_upazila') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, type: type, data_format: 'html'},
                    cache: false,
                    success: function (response) {
                        $('#'+type).html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            $('#division_list').change(function() {
                var division_id = $(this).val();
                if(division_id == '') {
                    $('#district_list').html('<option value="">Select District</option>');
                } else {
                    get_selected_district_upazila(division_id, 'district_list');
                }
                $('#upazila_list').html('<option value="">Select Upazila</option>');
            });

            $('#district_list').change(function() {
                var district_id = $(this).val();
                get_selected_district_upazila(district_id, 'upazila_list');
            });
        });
    </script>
@endsection