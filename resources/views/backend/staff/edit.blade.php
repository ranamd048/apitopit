@extends('backend.layout')

@section('maincontent')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Edit Staff</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/staff-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="form">
                        <form class="form-horizontal" action="{{ url('/admin/update_staff') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="staff_id" value="{{$result->id}}">
                            <div class="row">

                                <div class="col-md-12">
                                    @if(!Session::has('store_id'))
                                        <div class="form-group">
                                            <label for="">Shop List *</label>
                                            <select name="store_id" id="store_id" class="form-control {{ ($errors->has('store_id')) ? 'is-invalid' : '' }}" required="" disabled="">
                                                <option value="">Select Shop</option>
                                                @foreach($shop_list as $row)
                                                    <option value="{{$row->id}}" @if($row->id == $result->store_id) selected="" @endif>{{$row->store_name}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('store_id'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('store_id') }}
                                                </div>
                                            @endif
                                        </div>
                                    @else
                                        <input type="hidden" name="store_id" value="{{Session::get('store_id')}}">
                                    @endif
                                    <div class="form-group">
                                        <label for="">Position *</label>
                                        <select name="position" id="position" class="form-control" disabled="">
                                            <option value="">Select Position</option>
                                            @foreach($admin_positions as $row)
                                            <option @if($result->position_id == $row->id) selected="" @endif value="{{$row->id}}" data-role="{{$row->role_type}}">{{$row->position}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if(Session::get('admin_type') == 1)
                                        <div class="form-group" id="reference_by">
                                            <label for="">Reference By *</label>
                                            <select name="parent_id" id="show_references" class="form-control">
                                                <option value="">Please Select a Shop at First</option>
                                            </select>
                                        </div>
                                    @else
                                        <input type="hidden" name="parent_id" value="{{$result->parent_id}}">
                                    @endif

                                    <div class="form-group">
                                        <label for="">Name *</label>
                                        <input type="text" class="form-control {{ ($errors->has('name')) ? 'is-invalid' : '' }}" name="name" value="{{$result->name}}">
                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="email" class="form-control" name="email" value="{{$result->email}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Division</label>
                                        <select name="division" id="division_list" class="form-control">
                                            <option value="">Select Division</option>
                                            @foreach($divisions as $row)
                                            <option @if($result->division == $row->id) selected @endif value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">District</label>
                                        <select name="district" id="district_list" class="form-control">
                                            <option value="{{$result->district}}">{{Helpers::districtName($result->district)}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Upazila</label>
                                        <select name="thana" id="upazila_list" class="form-control">
                                        <option value="{{$result->thana}}">{{Helpers::upazilaName($result->thana)}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Update Password</label>
                                        <input type="password" class="form-control" name="password">
                                    </div>

                                    <div class="form-group" id="module_permission" @if($result->role !== 1) style="display: none;" @endif>
                                        <label for="">Module Permission *</label><br>
                                        <input type="checkbox" @if($result->product_access == 1) checked="" @endif name="product_access" id=""> Product <br>
                                        <input type="checkbox" @if($result->shop_settings == 1) checked="" @endif name="shop_settings" id=""> Shop Settings<br>
                                        <input type="checkbox" @if($result->order_access == 1) checked="" @endif name="order_access" id=""> Order <br>
                                        <input type="checkbox" @if($result->service_access == 1) checked="" @endif name="service_access" id=""> Service <br>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            function get_shop_staff_list(shop_id) {
                var url = '{{url('admin/get_shop_owner')}}';
                var parent_id = '{{$result->parent_id}}';
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {shop_id: shop_id},
                    cache: false,
                    success: function (response) {
                        $('#show_references').html(response);
                        $('#show_references option[value="'+parent_id+'"]').prop("selected", true);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            get_shop_staff_list('{{$result->store_id}}');
            //select 2
            $('#show_references').select2();

            //get district/upazila
            function get_selected_district_upazila(id, type) {
                var url = "{{ url('get_district_upazila') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, type: type, data_format: 'html'},
                    cache: false,
                    success: function (response) {
                        $('#'+type).html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            $('#division_list').change(function() {
                var division_id = $(this).val();
                if(division_id == '') {
                    $('#district_list').html('<option value="">Select District</option>');
                } else {
                    get_selected_district_upazila(division_id, 'district_list');
                }
                $('#upazila_list').html('<option value="">Select Upazila</option>');
            });

            $('#district_list').change(function() {
                var district_id = $(this).val();
                get_selected_district_upazila(district_id, 'upazila_list');
            });
        });
    </script>
@endsection