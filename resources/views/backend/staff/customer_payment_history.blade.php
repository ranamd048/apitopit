@extends('backend.layout')

@section('maincontent')
<style>
a.summary-link {
    display: block;
    padding: 30px 0;
    text-align: center;
    border-radius: 5px;
    margin-bottom: 30px;
}
a.summary-link i {
    color: #fff;
    font-size: 30px;
}
a.summary-link h3 {
    color: #fff;
    text-transform: uppercase;
    font-size: 15px;
    margin-bottom: 0;
}
a.summary-link span {
    color: #fff;
    font-size: 20px;
    display: block;
    margin-bottom: 5px;
}
</style>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">View Payment History</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/customer-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <h3>Customer Details</h3>
                    <div id="responsive_table">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-title="Name">{{ $customer->name }}</td>
                                    <td data-title="Email">{{ $customer->email }}</td>
                                    <td data-title="Phone">{{ $customer->phone }}</td>
                                    <td data-title="Address">{{ $customer->address }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <h3>Payment Summary</h3>
                    <div class="row">
                        <div class="col-md-6 col-6">
                            <a class="summary-link bg bg-primary">
                                <span>৳ {{ number_format($payment_summary->total_due_amount) }}</span>
                                <h3>Total Due</h3>
                            </a>
                        </div>
                        <div class="col-md-6 col-6">
                            <a class="summary-link bg bg-success">
                                <span>৳ {{ number_format($payment_summary->total_paid_amount) }}</span>
                                <h3>Total Paid</h3>
                            </a>
                        </div>
                    </div>
                    <h3>Payment History</h3>
                    <div id="responsive_table">
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Paid Amount</th>
                                <th>Payment Method</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($payment_history as $row)
                            <?php $i++; $date = new DateTime($row->payment_date); ?>
                                <tr>
                                    <td data-title="SL">{{ $i }}</td>
                                    <td data-title="Amount">৳ {{ number_format($row->paid_amount, 2) }}</td>
                                    <td data-title="Method">{{ $row->payment_method }}</td>
                                    <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    } );
</script>
@endsection