@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Menu List</h4>
                <div class="back-button">
                    <a href="{{ url('admin/create-menu') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Menu</a>
                </div>
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Menu Name</th>
                                <th>Sort Order</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            @if(count($menu_list) > 0)
                            @foreach($menu_list as $menu)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Menu Name">{{ $menu->name }}</td>
                                <td data-title="Sort Order">{{ $menu->sort_order }}</td>
                                <td data-title="Status"><?php echo ($menu->status == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>'; ?></td>
                                <td data-title="Action">
                                    <a class="btn btn-success" href="{{ url('/admin/edit-menu/'.$menu->id) }}"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure want to delete this menu?')" href="{{ url('/admin/delete-menu/'.$menu->id) }}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    } );
</script>
@endsection