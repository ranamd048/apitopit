<ul id="sidebarnav">
    <li>
        <a class="aj scrollTop" href="{{ url('/admin/dashboard') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
    </li>
    @if((Session::get('role') == 0 || Session::get('role') == 1)  && Session::get('admin_type') == 1)
    <li>
        <a class="aj scrollTop" href="{{ url('/admin/pos') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">POS</span></a>
    </li>
    @endif

    {{-- @if(Session::get('role') == 1 && Session::get('admin_type') == 0)
        <li>
            <a class="aj scrollTop" aria-expanded="false" href="{{ url('/admin/staff-list') }}"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Staff List</span></a>
        </li>
    @endif --}}

    @if($admin_permission->order_access == 1)
    <li>
    <?php $pending_online_order = Helpers::pendingOrderCount(); ?>
    <?php $pending_pos_order = Helpers::pendingOrderCount('pos'); ?>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Manage Orders</span>  <span class="badge badge-danger">{{$pending_online_order + $pending_pos_order}}</span></a>
        <ul aria-expanded="false" class="collapse">
            @if(Session::get('role') == 0 || Session::get('store_type') == 'both')
            <li>
                <a class="aj scrollTop" aria-expanded="false" href="{{ url('/admin/orders') }}"><span class="hide-menu">Online Order</span> <span class="badge badge-danger">{{$pending_online_order}}</span></a>
            </li>
            <li>
                <a class="aj scrollTop" aria-expanded="false" href="{{ url('/admin/pos-orders') }}"><span class="hide-menu">POS Order</span> <span class="badge badge-danger">{{$pending_pos_order}}</span></a>
            </li>
            <li><a class="aj scrollTop" href="{{ url('/admin/service-request') }}">Service Request <span class="badge badge-danger">{{Helpers::pendingServiceRequestCount()}}</span></a></li>
            @endif
            @if(Session::get('store_type') == 'shop')
            <li>
                <a class="aj scrollTop" aria-expanded="false" href="{{ url('/admin/orders') }}"><span class="hide-menu">Online Order</span> <span class="badge badge-danger">{{$pending_online_order}}</span></a>
            </li>
            <li>
                <a class="aj scrollTop" aria-expanded="false" href="{{ url('/admin/pos-orders') }}"><span class="hide-menu">POS Order</span> <span class="badge badge-danger">{{$pending_pos_order}}</span></a>
            </li>
            @endif
            @if(Session::get('store_type') == 'service')
            <li><a class="aj scrollTop" href="{{ url('/admin/service-request') }}">Service Request <span class="badge badge-danger">{{Helpers::pendingServiceRequestCount()}}</span></a></li>
            @endif
        </ul>
    </li>
    @endif

    @if(Session::get('store_id'))
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Expenses</span></a>
        <ul aria-expanded="false" class="collapse">
            <li><a class="aj scrollTop" href="{{ url('/admin/expense-categories') }}">Expense Categories</a></li>
            <li><a class="aj scrollTop" href="{{ url('/admin/expense-list') }}">Add Expense</a></li>
        </ul>
    </li>
    @endif
    @if(Session::get('store_id'))
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Purchases</span></a>
        <ul aria-expanded="false" class="collapse">
            <li><a class="aj scrollTop" href="{{ url('/admin/add-purchase') }}">Add Purchase</a></li>
            <li><a class="aj scrollTop" href="{{ url('/admin/purchase-list') }}">Purchase List</a></li>
        </ul>
    </li>
    @endif
    
    @if((Session::get('role') == 0 || Session::get('role') == 1) && Session::get('admin_type') == 1)
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">People</span></a>
        <ul aria-expanded="false" class="collapse">
            {{-- @if($private_settings->login_type == 1)
            <li><a class="aj scrollTop" href="{{ url('/admin/staff-list') }}">Staff List</a></li>
            @endif --}}
            <li><a class="aj scrollTop" href="{{ url('/admin/customer-list') }}">Customer List</a></li>
            @if(Session::get('store_id'))
            <li><a class="aj scrollTop" href="{{ url('/admin/supplier-list') }}">Supplier List</a></li>
            @endif
            {{-- @if(Session::get('role') == 0 && Session::get('admin_type') == 1)
            <li><a class="aj scrollTop" href="{{ url('/admin/authority/list') }}">Authority List</a></li>
            @endif --}}
            @if(Session::has('store_id'))
            <li><a class="aj scrollTop" href="{{ url('/admin/shop-staffs') }}">Shop Staffs</a></li>
            @endif
        </ul>
    </li>
    @endif

    @if(Session::get('role') == 1 && (Session::get('store_type') == 'both' || Session::get('store_type') == 'shop'))
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Reports</span></a>
        <ul aria-expanded="false" class="collapse">
            @if((Session::get('role') == 0 || Session::get('role') == 1) && Session::get('admin_type') == 1)
            <li>
                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Accounts</span></a>
                <ul aria-expanded="false" class="collapse">
                    <li><a class="aj scrollTop" href="{{ url('/admin/account/due-list') }}">Due List</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/account/paid-list') }}">Paid List</a></li>
                </ul>
            </li>
            @endif
            @if(Session::get('role') == 1 && (Session::get('store_type') == 'both' || Session::get('store_type') == 'shop'))
            <li><a class="aj scrollTop" href="{{ url('/admin/product-stock') }}">Product Stock</a></li>
            <li><a class="aj scrollTop" href="{{ url('/admin/today-sale-report') }}">Today Sale Report</a></li>
            <li><a class="aj scrollTop" href="{{ url('/admin/purchase-report') }}">Purchase Report</a></li>
            <li><a class="aj scrollTop" href="{{ url('/admin/income-expense-report') }}">Income & Expense</a></li>
            @endif
            @if($private_settings->sales_representive_orders == 1)
            <li><a class="aj scrollTop" href="{{ url('/admin/sales-representive-orders') }}">Sales Representive Orders</a></li>
            @endif
        </ul>
    </li>
    @endif


    

    @if(Session::get('role') == 0 && $private_settings->coupon_option == 1)
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Coupons</span></a>
        <ul aria-expanded="false" class="collapse">
            <li><a class="aj scrollTop" href="{{ url('/admin/coupon/add') }}">Add Coupon</a></li>
            <li><a class="aj scrollTop" href="{{ url('/admin/coupon/list') }}">Coupon List</a></li>
        </ul>
    </li>
    @endif

    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Settings</span></a>
        <ul aria-expanded="false" class="collapse">
        @if(Session::get('role') == 0)
            <li>
                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Website Settings</span></a>
                <ul aria-expanded="false" class="collapse">
                    <li><a class="aj scrollTop" href="{{ url('/admin/site_settings') }}">Site Information</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/shop_advertisement') }}">Advertisement</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/slider-list') }}">Slider List</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/page-list') }}">Page List</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/menu-list') }}">Menu List</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/blog-list') }}">Blog List</a></li>
                    <li>
                        <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Gallery</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a class="aj scrollTop" href="{{ url('/admin/gallery-types') }}">Gallery Types</a></li>
                            <li><a class="aj scrollTop" href="{{ url('/admin/gallery-list') }}">Gallery List</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        @endif
            <li>
                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Shop Settings</span></a>
                <ul aria-expanded="false" class="collapse">
                    @if(Session::get('role') == 0)
                    <li>
                        <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Stores</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a class="aj scrollTop" href="{{ url('/admin/create-store') }}">Create store</a></li>
                            <li><a class="aj scrollTop" href="{{ url('/admin/store-list') }}">Store List</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="aj scrollTop" aria-expanded="false" href="{{ url('/admin/store_categories') }}"><span class="hide-menu">Store Categories</span></a>
                    </li>
                    <li>
                        <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Units</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a class="aj scrollTop" href="{{ url('/admin/create-unit') }}">Create Unit</a></li>
                            <li><a class="aj scrollTop" href="{{ url('/admin/unit-list') }}">Unit List</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="aj scrollTop" aria-expanded="false" href="{{ url('/admin/price-group-list') }}"><span class="hide-menu">Price Groups</span></a>
                    </li>
                    @endif
                    @if(Session::get('role') == 1 && $admin_permission->shop_settings == 1)
                        <li>
                            <a class="aj scrollTop" aria-expanded="false" href="{{ url('/admin/shop_settings') }}"><span class="hide-menu">Update Shop</span></a>
                        </li>
                    @endif
                    @if($admin_permission->product_access == 1)
                        <li>
                            <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Categories</span></a>
                            <ul aria-expanded="false" class="collapse">
                                @if(Session::get('role') == 0 && !Session::has('store_id'))
                                <li><a class="aj scrollTop" href="{{ url('/admin/create-category') }}">Create Category</a></li>
                                @endif

                                @if(Session::get('role') == 0 || Session::get('store_type') == 'both')
                                <li><a class="aj scrollTop" href="{{ url('/admin/category-list/shop') }}">Shop Category</a></li>
                                <li><a class="aj scrollTop" href="{{ url('/admin/category-list/service') }}">Service Category</a></li>
                                @endif
                                @if(Session::get('role') == 1 && Session::get('store_type') == 'service')
                                <li><a class="aj scrollTop" href="{{ url('/admin/category-list/service') }}">Service Category</a></li>
                                @endif
                                @if(Session::get('role') == 1 && Session::get('store_type') == 'shop')
                                <li><a class="aj scrollTop" href="{{ url('/admin/category-list/shop') }}">Shop Category</a></li>
                                @endif
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Brands</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a class="aj scrollTop" href="{{ url('/admin/add-brand') }}">Add Brand</a></li>
                                <li><a class="aj scrollTop" href="{{ url('/admin/brand-list') }}">Brand List</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(Session::get('role') == 0)
                        <li>
                            <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Products</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a class="aj scrollTop" href="{{ url('/admin/add-product') }}">Add Product</a></li>
                                <li><a class="aj scrollTop" href="{{ url('/admin/product-list') }}">Product List</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Services</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a class="aj scrollTop" href="{{ url('/admin/add-service') }}">Add Service</a></li>
                                <li><a class="aj scrollTop" href="{{ url('/admin/service-list') }}">Service List</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(Session::get('role') == 1 && Session::get('store_type') == 'shop')
                            @if($admin_permission->product_access == 1)
                        <li>
                            <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Products</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a class="aj scrollTop" href="{{ url('/admin/add-product') }}">Add Product</a></li>
                                <li><a class="aj scrollTop" href="{{ url('/admin/product-list') }}">Product List</a></li>
                            </ul>
                        </li>
                            @endif
                        @endif
                        @if(Session::get('role') == 1 && Session::get('store_type') == 'service')
                            @if($admin_permission->service_access == 1)
                        <li>
                            <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Services</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a class="aj scrollTop" href="{{ url('/admin/add-service') }}">Add Service</a></li>
                                <li><a class="aj scrollTop" href="{{ url('/admin/service-list') }}">Service List</a></li>
                            </ul>
                        </li>
                                @endif
                        @endif
                        @if(Session::get('role') == 1 && Session::get('store_type') == 'both')
                            @if($admin_permission->product_access == 1)
                                <li>
                                    <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Products</span></a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a class="aj scrollTop" href="{{ url('/admin/add-product') }}">Add Product</a></li>
                                        <li><a class="aj scrollTop" href="{{ url('/admin/product-list') }}">Product List</a></li>
                                    </ul>
                                </li>
                            @endif
                        @if($admin_permission->service_access == 1)
                        <li>
                            <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Services</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a class="aj scrollTop" href="{{ url('/admin/add-service') }}">Add Service</a></li>
                                <li><a class="aj scrollTop" href="{{ url('/admin/service-list') }}">Service List</a></li>
                            </ul>
                        </li>
                            @endif
                        @endif
                </ul>
            </li>
        </ul>
    </li>
    
    @if(Session::get('role') == 0)
    <?php $inactive_product = Helpers::inactiveProductCount(); ?>
    <li>
        <a class="aj scrollTop" href="{{ url('/admin/inactive-product') }}" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Inactive Product</span> <span class="badge badge-danger">{{$inactive_product}}</span></a>
    </li>
    @endif

    @if(Session::get('role') == 0)
    <?php $pending_review = Helpers::pendingProductReviewCount(); ?>
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Product Reviews</span> <span class="badge badge-danger">{{$pending_review}}</span></a>
        <ul aria-expanded="false" class="collapse">
            <li><a class="aj scrollTop" href="{{ url('/admin/reviews/pending') }}">Pending Reviews</a></li>
            <li><a class="aj scrollTop" href="{{ url('/admin/reviews/approved') }}">Approved Reviews</a></li>
        </ul>
    </li>
    @endif
</ul>