<ul class="mobile_menu">
    <li>
        <a class="aj scrollTop" href="{{ url('/admin/dashboard') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu"> Dashboard</span></a>
    </li>
    @if((Session::get('role') == 0 || Session::get('role') == 1)  && Session::get('admin_type') == 1)
    <li>
        <a class="aj scrollTop" href="{{ url('/admin/pos') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu"> POS</span></a>
    </li>
    @endif

    @if($admin_permission->order_access == 1)
    <li>
    <?php $pending_order = Helpers::pendingOrderCount(); ?>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu"> Manage Orders</span>  <span class="badge badge-danger">{{$pending_order}}</span></a>
        <ul aria-expanded="false" class="submenu">
            <li>
                <a class="aj scrollTop" aria-expanded="false" href="{{ url('/admin/orders') }}"><span class="hide-menu">Sales Order</span> <span class="badge badge-danger">{{$pending_order}}</span></a>
            </li>
            <li><a class="aj scrollTop" href="{{ url('/admin/service-request') }}">Service Request <span class="badge badge-danger">{{Helpers::pendingServiceRequestCount()}}</span></a></li>
        </ul>
    </li>
    @endif
    

    @if($admin_permission->people_access == 1)
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu"> People</span></a>
        <ul aria-expanded="false" class="submenu">
            {{-- @if($private_settings->login_type == 1)
            <li><a class="aj scrollTop" href="{{ url('/admin/staff-list') }}">Staff List</a></li>
            @endif --}}
            <li><a class="aj scrollTop" href="{{ url('/admin/customer-list') }}">Customer List</a></li>
            {{-- @if(Session::get('role') == 0 && Session::get('admin_type') == 1)
            <li><a class="aj scrollTop" href="{{ url('/admin/authority/list') }}">Authority List</a></li>
            @endif --}}
        </ul>
    </li>
    @endif
    @if($admin_permission->report_access == 1)
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu"> Reports</span></a>
        <ul aria-expanded="false" class="submenu">
            <li>
                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Accounts</span></a>
                <ul aria-expanded="false" class="submenu">
                    <li><a class="aj scrollTop" href="{{ url('/admin/account/due-list') }}">Due List</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/account/paid-list') }}">Paid List</a></li>
                </ul>
            </li>
            <li><a class="aj scrollTop" href="{{ url('/admin/product-stock') }}">Product Stock</a></li>
        </ul>
    </li>
    @endif

    

    @if($admin_permission->coupon_access == 1 && $private_settings->coupon_option == 1)
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu"> Coupons</span></a>
        <ul aria-expanded="false" class="submenu">
            <li><a class="aj scrollTop" href="{{ url('/admin/coupon/add') }}">Add Coupon</a></li>
            <li><a class="aj scrollTop" href="{{ url('/admin/coupon/list') }}">Coupon List</a></li>
        </ul>
    </li>
    @endif

    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu"> Settings</span></a>
        <ul aria-expanded="false" class="submenu">
        @if($admin_permission->website_settings_access == 1)
            <li>
                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Website Settings</span></a>
                <ul aria-expanded="false" class="submenu">
                    <li><a class="aj scrollTop" href="{{ url('/admin/site_settings') }}">Site Information</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/shop_advertisement') }}">Advertisement</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/slider-list') }}">Slider List</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/page-list') }}">Page List</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/menu-list') }}">Menu List</a></li>
                    <li><a class="aj scrollTop" href="{{ url('/admin/blog-list') }}">Blog List</a></li>
                    <li>
                        <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Gallery</span></a>
                        <ul aria-expanded="false" class="submenu">
                            <li><a class="aj scrollTop" href="{{ url('/admin/gallery-types') }}">Gallery Types</a></li>
                            <li><a class="aj scrollTop" href="{{ url('/admin/gallery-list') }}">Gallery List</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        @endif
            <li>
                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Shop Settings</span></a>
                <ul aria-expanded="false" class="submenu">
                        @if($admin_permission->shop_settings == 1)
                        <li>
                            <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Stores</span></a>
                            <ul aria-expanded="false" class="submenu">
                                <li><a class="aj scrollTop" href="{{ url('/admin/create-store') }}">Create store</a></li>
                                <li><a class="aj scrollTop" href="{{ url('/admin/store-list') }}">Store List</a></li>
                            </ul>
                        </li>
                        @endif
                        @if($admin_permission->product_access == 1)
                            <li>
                                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Units</span></a>
                                <ul aria-expanded="false" class="submenu">
                                    <li><a class="aj scrollTop" href="{{ url('/admin/create-unit') }}">Create Unit</a></li>
                                    <li><a class="aj scrollTop" href="{{ url('/admin/unit-list') }}">Unit List</a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Categories</span></a>
                                <ul aria-expanded="false" class="submenu">
                                    @if(Session::get('role') == 0 && !Session::has('store_id'))
                                    <li><a class="aj scrollTop" href="{{ url('/admin/create-category') }}">Create Category</a></li>
                                    @endif
                                    <li><a class="aj scrollTop" href="{{ url('/admin/category-list/shop') }}">Shop Category</a></li>
                                    <li><a class="aj scrollTop" href="{{ url('/admin/category-list/service') }}">Service Category</a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Brands</span></a>
                                <ul aria-expanded="false" class="submenu">
                                    <li><a class="aj scrollTop" href="{{ url('/admin/add-brand') }}">Add Brand</a></li>
                                    <li><a class="aj scrollTop" href="{{ url('/admin/brand-list') }}">Brand List</a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Products</span></a>
                                <ul aria-expanded="false" class="submenu">
                                    <li><a class="aj scrollTop" href="{{ url('/admin/add-product') }}">Add Product</a></li>
                                    <li><a class="aj scrollTop" href="{{ url('/admin/product-list') }}">Product List</a></li>
                                </ul>
                            </li>
                        @endif
                        @if($admin_permission->service_access == 1)
                        <li>
                            <a class="has-arrow" aria-expanded="false"><span class="hide-menu">Services</span></a>
                            <ul aria-expanded="false" class="submenu">
                                <li><a class="aj scrollTop" href="{{ url('/admin/add-service') }}">Add Service</a></li>
                                <li><a class="aj scrollTop" href="{{ url('/admin/service-list') }}">Service List</a></li>
                            </ul>
                        </li>
                        @endif
                </ul>
            </li>
        </ul>
    </li>
    

    @if($admin_permission->product_review_access == 1)
    <?php $pending_review = Helpers::pendingProductReviewCount(); ?>
    <li>
        <a class="has-arrow" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu"> Product Reviews</span> <span class="badge badge-danger">{{$pending_review}}</span></a>
        <ul aria-expanded="false" class="submenu">
            <li><a class="aj scrollTop" href="{{ url('/admin/reviews/pending') }}">Pending Reviews</a></li>
            <li><a class="aj scrollTop" href="{{ url('/admin/reviews/approved') }}">Approved Reviews</a></li>
        </ul>
    </li>
    @endif
    <li>
        <a class="aj scrollTop" href="{{ url('/admin/logout') }}" aria-expanded="false"><i class="fa fa-sign-in"></i><span class="hide-menu"> Logout</span></a>
    </li>
</ul>