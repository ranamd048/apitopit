@extends('backend.layout')

@section('title_text') {{ $title }} @endsection

@section('maincontent')
<style>
  .row.stock_summary {
    width: 100%;
    overflow: hidden;
    padding-top: 20px;
  }
  table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after {
    display: none;
  }
  table tfoot tr td { font-weight: 500; }
  @media only screen and (max-width: 767px) {
    .card-block {
      margin-top: 0;
    }
    .product-filter .btn {
        margin-top: 0;
    }
  }
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
    .product-filter button, .product-filter a {
        margin-top: 32px;
    }
    .product-filter .btn {
        margin-top: 40px;
    }
</style>
<?php
    $from_date = isset($_GET['from_date']) ? $_GET['from_date'] : '';
    $to_date = isset($_GET['to_date']) ? $_GET['to_date'] : '';
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">{{ $title }}</h4>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="GET">
                            <div class="product-filter">
                                <div class="form-group row">
                                    <div class="col-md-5">
                                        <label>From Date *</label>
                                        <input type="date" name="from_date" required="" value="{{ $from_date }}" class="form-control">
                                    </div>
                                    <div class="col-md-5">
                                        <label>To Date *</label>
                                        <input type="date" name="to_date" required="" value="{{ $to_date }}" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Filter</button>
                                        <a href="{{ url('admin/income-expense-report') }}" class="btn btn-danger btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="responsive_table">
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Income</th>
                                <th>Amount</th>
                                <th>Expense</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <?php 
                            $i = 2;
                            $total_income = $pre_balance;
                            $total_expense = 0;
                        ?>
                        <tbody>
                            <tr>
                                <td data-title="Sl">1</td>
                                <td data-title="Income">Opening Balance</td>
                                <td data-title="Amount">৳{{ number_format($pre_balance, 2) }}</td>
                                <td data-title="Expense">----</td>
                                <td data-title="Amount">----</td>
                            </tr>
                            @if(count($income_expense_reports) > 0)
                                @foreach($income_expense_reports as $row)
                                    @if($row['type'] == 'income')
                                        <?php $total_income += $row['amount']; ?>
                                        <tr>
                                            <td data-title="Sl">{{ $i }}</td>
                                            <td data-title="Income">{{ $row['title'] }}</td>
                                            <td data-title="Amount">৳{{ number_format($row['amount'], 2) }}</td>
                                            <td data-title="Expense">----</td>
                                            <td data-title="Amount">----</td>
                                        </tr>
                                    @else
                                        <?php $total_expense += $row['amount']; ?>
                                        <tr>
                                            <td data-title="Sl">{{ $i }}</td>
                                            <td data-title="Income">----</td>
                                            <td data-title="Amount">----</td>
                                            <td data-title="Expense">{{ $row['title'] }}</td>
                                            <td data-title="Amount">৳{{ number_format($row['amount'], 2) }}</td>
                                        </tr>
                                    @endif
                                <?php $i++; ?>
                                @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td>Total Income</td>
                                <td>৳{{ number_format($total_income, 2) }}</td>
                                <td>Total Expense</td>
                                <td>৳{{ number_format($total_expense, 2) }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="row stock_summary">
                    <div class="col-md-4 col-6 text-center">
                        <div class="card bg-info">
                            <div class="card-block" style="color:#fff; font-size:17px">Total Income <br> ৳{{ number_format($total_income, 2) }}</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center">
                        <div class="card bg-primary">
                            <div class="card-block" style="color:#fff; font-size:17px">Total Expense <br> ৳{{ number_format($total_expense, 2) }}</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center">
                        <div class="card bg-success">
                            <div class="card-block" style="color:#fff; font-size:17px">Balance <br> ৳{{ number_format($total_income - $total_expense, 2) }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/datatable/buttons.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/jszip.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/js/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/buttons.print.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable({
            dom: 'Bfrtip',
            // buttons: [
            //     'csv', 'excel', 'pdf', 'print'
            // ]
            buttons: [
                { extend: 'excelHtml5', footer: true },
                { extend: 'csvHtml5', footer: true },
                { extend: 'pdfHtml5', footer: true },
                { extend: 'print', footer: true }
            ]
        });
    });
</script>
@endsection