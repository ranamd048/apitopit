@extends('backend.layout')

@section('title_text') Today Sale Report @endsection

@section('maincontent')
<style>
  .row.stock_summary {
    width: 100%;
    overflow: hidden;
    padding-top: 20px;
  }
  table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after {
    display: none;
  }
  table tfoot tr td { font-weight: 500; }
  @media only screen and (max-width: 767px) {
    .card-block {
      margin-top: 0;
    }
  }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Today Sale Report</h4>
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Sale Quantity</th>
                                <th>Sale Price</th>
                                <th>Discount</th>
                                <th>Profit</th>
                            </tr>
                        </thead>
                        <?php 
                            $i = 1;
                            $total_stock_minus = 0;
                            $total_sale_price = 0;
                            $total_discount = 0;
                            $total_profit = 0;
                        ?>
                        @if(count($today_sale_report) > 0)
                        <tbody>
                            @foreach($today_sale_report as $row)
                            <?php

                                $sale_price = ($row->price * $row->total_quantity);
                                $discount = Helpers::todaySaleProductDiscount($row->product_id);
                                $cost = ($row->cost * $row->total_quantity);
                                $profit = ($sale_price - $discount) - $cost; 
                                //total calculate
                                $total_stock_minus += $row->total_quantity;
                                $total_sale_price += $sale_price;
                                $total_discount += $discount;
                                $total_profit += $profit;
                            ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Product Name">{{ $row->product_name }}</td>
                                <td data-title="Sale Quantity">{{ $row->total_quantity }}</td>
                                <td data-title="Sale Price">৳{{number_format($sale_price, 2)}}</td>
                                <td data-title="Discount">৳{{number_format($discount, 2)}}</td>
                                <td data-title="Profit">৳{{number_format($profit, 2)}}</td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr class="text-center">
                                <td></td>
                                <td></td>
                                <td>Stock Minus <br> {{ $total_stock_minus }}</td>
                                <td>Total Sale <br> ৳{{number_format($total_sale_price, 2)}}</td>
                                <td>Total Discount <br> ৳{{number_format($total_discount, 2)}}</td>
                                <td>Total Profit <br> ৳{{number_format($total_profit, 2)}}</td>
                            </tr>
                        </tfoot>
                        @endif
                    </table>
                </div>
                <div class="row stock_summary">
                    <div class="col-md-3 col-6 text-center">
                        <div class="card bg-info">
                            <div class="card-block" style="color:#fff; font-size:17px">Stock Minus <br> {{ $total_stock_minus }}</div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6 text-center">
                        <div class="card bg-success">
                            <div class="card-block" style="color:#fff; font-size:17px">Sale Price <br> ৳{{number_format($total_sale_price, 2)}}</div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6 text-center">
                        <div class="card bg-info">
                            <div class="card-block" style="color:#fff; font-size:17px">Discount <br> ৳{{number_format($total_discount, 2)}}</div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6 text-center">
                        <div class="card bg-primary">
                            <div class="card-block" style="color:#fff; font-size:17px">Profit <br> ৳{{number_format($total_profit, 2)}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/datatable/buttons.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/jszip.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/js/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/js/datatable/buttons.print.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable({
            dom: 'Bfrtip',
            // buttons: [
            //     'csv', 'excel', 'pdf', 'print'
            // ]
            buttons: [
                { extend: 'excelHtml5', footer: true },
                { extend: 'csvHtml5', footer: true },
                { extend: 'pdfHtml5', footer: true },
                { extend: 'print', footer: true }
            ]
        });
    });
</script>
@endsection