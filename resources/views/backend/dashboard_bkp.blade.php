@extends('backend.layout')

@section('maincontent')
<style>
a.summary-link {
    display: block;
    padding: 30px 0;
    text-align: center;
    border-radius: 5px;
    margin-bottom: 30px;
}
a.summary-link i {
    color: #fff;
    font-size: 30px;
}
a.summary-link h3 {
    color: #fff;
    text-transform: uppercase;
    font-size: 15px;
    margin-bottom: 0;
}
a.summary-link span {
    color: #fff;
    font-size: 20px;
    display: block;
    margin-bottom: 5px;
}
.row.filter-report {
    margin-bottom: 30px;
    overflow: hidden;
}
</style>
<?php
    $admin_permission = DB::table('admins')->where('id', Session::get('admin_id'))->first();
?>
<div class="row hide-mobile">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
            	@if(Session::get('role') == 0)
            	<form action="{{ url('/admin/dashboard') }}" method="GET">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group text-right">
                                <label class="control-label col-form-label" for="store_id">Select Store</label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="store_id" class="form-control" id="store_id">
                                    <option value="">Select Store</option>
                                    @foreach($store_list as $store)
                                    <option @if(Session::get('store_id') == $store->id) selected="" @endif value="{{ $store->id }}">{{ $store->store_name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Select</button>
                            </div>
                        </div>
                    </div>
                </form>
                @else
                <h1>Welcome To Dashboard</h1>
                @endif
            </div>
        </div>
    </div>
</div>
@if(Session::get('role') == 0)
<?php
    $years_array = array_combine(range(date("Y"), 2019), range(date("Y"), 2019));
    $months_array = array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
    $current_month = Carbon\Carbon::now()->month;
    $current_year = Carbon\Carbon::now()->year;
    if(isset($_GET['year'])) {
        $current_year = $_GET['year'];
    }
    if(isset($_GET['month'])) {
        $current_month = $_GET['month'];
    }
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-success">
                <h4 class="card-title">Monthly Summary</h4>
            </div>
            <div class="card-block">
                <form action="" method="GET">
                    <div class="row filter-report">
                        <div class="col-5">
                            <label for="">Select Year</label>
                            <select name="year" class="form-control">
                                @foreach($years_array as $key => $value)
                                    <option  @if($current_year == $key) selected @endif value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-5">
                            <label for="">Month</label>
                            <select name="month" class="form-control">
                                    <option value="">Select Month</option>
                                @foreach($months_array as $key => $value)
                                    <option @if($current_month == $key) selected @endif value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-2">
                            <button type="submit" style="margin-top:33px; margin-left: -25px" class="btn btn-info">Submit</button>
                        </div>
                    </div>
                </form>
            	<div class="row">
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-info">
                            <span>{{Helpers::monthlyCustomerCount($current_year, $current_month)}}</span>
                            <h3>Customer Added</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success">
                            <span>{{Helpers::monthlyShopCount($current_year, $current_month)}}</span>
                            <h3>Shop Added</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-primary">
                            <span>{{Helpers::monthlyOrderCount($current_year, $current_month)}}</span>
                            <h3>New Order</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-primary">
            @if(Session::has('store_id'))
            <?php 
                $store = DB::table('stores')->select('store_name')->where('id', Session::get('store_id'))->first();
            ?>
            <h4 class="card-title" style="float: left !important">{{ $store->store_name }}</h4>
            @else
                <h4 class="card-title">Quick Links </h4>
            @endif    
            </div>
            <div class="card-block">
            	<div class="row">
                    @if(Session::get('role') == 0)
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success" href="{{url('admin/product-list')}}">
                            <i class="fa fa-tree"></i>
                            <h3>Products</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-danger" href="{{url('admin/service-list')}}">
                            <i class="fa fa-clock-o"></i>
                            <h3>Services</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success" href="{{url('admin/category-list/shop')}}">
                            <i class="fa fa-list"></i>
                            <h3>Categories</h3>
                        </a>
                    </div>
                    @endif

                    @if(Session::get('role') == 1 && Session::get('store_type') == 'both')
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-primary" href="{{url('admin/pos')}}">
                            <i class="fa fa-shopping-bag"></i>
                            <h3>Pos</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success" href="{{url('admin/product-list')}}">
                            <i class="fa fa-tree"></i>
                            <h3>Products</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-danger" href="{{url('admin/service-list')}}">
                            <i class="fa fa-clock-o"></i>
                            <h3>Services</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success" href="{{url('admin/category-list/shop')}}">
                            <i class="fa fa-list"></i>
                            <h3>Categories</h3>
                        </a>
                    </div>
                    @endif
                    @if(Session::get('role') == 1 && Session::get('store_type') == 'shop')
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-primary" href="{{url('admin/pos')}}">
                            <i class="fa fa-shopping-bag"></i>
                            <h3>Pos</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success" href="{{url('admin/product-list')}}">
                            <i class="fa fa-tree"></i>
                            <h3>Products</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-danger" href="{{url('admin/brand-list')}}">
                            <i class="fa fa-list"></i>
                            <h3>Brands</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success" href="{{url('admin/category-list/shop')}}">
                            <i class="fa fa-list"></i>
                            <h3>Categories</h3>
                        </a>
                    </div>
                    @endif
                    @if(Session::get('role') == 1 && Session::get('store_type') == 'service')
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-danger" href="{{url('admin/service-list')}}">
                            <i class="fa fa-clock-o"></i>
                            <h3>Services</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success" href="{{url('admin/category-list/service')}}">
                            <i class="fa fa-list"></i>
                            <h3>Categories</h3>
                        </a>
                    </div>
                    @endif

                    @if($admin_permission->order_access == 1)
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-primary" href="{{url('admin/orders')}}">
                            <i class="fa fa-book"></i>
                            <h3>Orders</h3>
                        </a>
                    </div>
                    @endif
                    @if(Session::get('role') == 0)
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-info" href="{{url('admin/store-list')}}">
                            <i class="fa fa-building"></i>
                            <h3>Shop List</h3>
                        </a>
                    </div>
                    @endif
                    @if(Session::get('role') == 1 && $admin_permission->shop_settings == 1)
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-info" href="{{url('admin/shop_settings')}}">
                            <i class="fa fa-building"></i>
                            <h3>Update Shop</h3>
                        </a>
                    </div>
                    @endif
                    @if((Session::get('role') == 0 || Session::get('role') == 1) && Session::get('admin_type') == 1)
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-warning" href="{{url('admin/customer-list')}}">
                            <i class="fa fa-users"></i>
                            <h3>Customers</h3>
                        </a>
                    </div>
                    @endif

                    @if(Session::get('role') == 1)
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success" href="{{url('admin/expense-categories')}}">
                            <i class="fa fa-money"></i>
                            <h3>Expenses</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-danger" href="{{url('admin/purchase-list')}}">
                            <i class="fa fa-list"></i>
                            <h3>Purchases</h3>
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-success">
                <h4 class="card-title" style="float: left !important">Notifications</h4>
            </div>
            <div class="card-block">
            	<div class="row">
                    @if(Session::get('role') == 0)
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-primary" href="{{url('admin/inactive-shop')}}">
                            <span>{{Helpers::inactiveShopCount()}}</span>
                            <h3>Inactive Shop</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-info" href="{{url('admin/date-expire-shop')}}">
                            <span>{{Helpers::dateExpireShopCount()}}</span>
                            <h3>Date Expire Shop</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-danger" href="{{url('admin/inactive-product')}}">
                            <span>{{Helpers::inactiveProductCount()}}</span>
                            <h3>Inactive Product</h3>
                        </a>
                    </div>
                    @endif
                    @if(Session::has('store_id') && $private_settings->sms_service == 1)
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success" href="{{url('admin/shop_settings')}}">
                            <span>{{Helpers::smsLimitCount()}}</span>
                            <h3>Remaining SMS</h3>
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection