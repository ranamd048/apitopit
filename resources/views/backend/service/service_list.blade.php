<div id="responsive_table">
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Image</th>
                <th>Category</th>
                <th>Subcategory</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($services) > 0)
            
            <?php $i = 0; ?>
            @foreach($services as $row)
            <?php $i++; ?>
            <tr>
                <td data-title="Sl">{{ $i }}</td>
                <td data-title="Name">{{ $row->service_name }}</td>
                <td data-title="Image"><img src="{{ asset('/public/uploads/services/'.$row->service_image) }}" width="120" alt=""></td>
                <td data-title="Category">{{ Helpers::getCategoryName($row->category_id) }}</td>
                <td data-title="Subcategory">
                    {{ Helpers::getCategoryName($row->subcategory_id) }}
                </td>
                <td data-title="Status">{{ ($row->status == 1) ? 'Active' : 'Inactive' }}</td>
                <td data-title="Action">
                    <a class="btn btn-success" href="{{ url('/admin/edit-service/'.$row->id) }}"><i class="fa fa-pencil"></i>
                    </a>
                    <a class="btn btn-danger" onclick="return confirm('Are you sure want to delete this service?')" href="{{ url('/admin/delete-service/'.$row->id) }}"><i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>

</div>