@extends('backend.layout')

@section('maincontent')
<style>
    button.custom-btn {
        cursor: pointer;
        background: none;
        border: none;
    }
    .btn i {
        font-size: 12px;
    }
    .btn {
        padding: 4px 8px;
    }
    .modal-dialog {
        max-width: 600px;
        margin: 30px auto;
    }
</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Service Request List</h4>
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                @if(Session::get('role') == 0)
                                <th>Store</th>
                                @endif
                                <th>Customer Name</th>
                                <th>Service Name</th>
                                <th>Contact No</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($request_list) > 0)
                            
                            <?php $i = 0; ?>
                            @foreach($request_list as $row)
                            <?php $i++;
                                $date = new DateTime($row->created_at);
                                if($row->status == 0) {
                                    $status = '<span class="badge badge-primary">Pending <i class="fa fa-caret-down"></i></span>';
                                }
                                if($row->status == 1) {
                                    $status = '<span class="badge badge-info">Processing <i class="fa fa-caret-down"></i></span>';
                                }
                                if($row->status == 2) {
                                    $status = '<span class="badge badge-success">Completed</span>';
                                }
                                if($row->status == 3) {
                                    $status = '<span class="badge badge-danger">Canceled</span>';
                                }
                            ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                @if(Session::get('role') == 0)
                                <td data-title="Shop">{{ $row->store_name }}</td>
                                @endif
                                <td data-title="Customer Name">{{ $row->name }}</td>
                                <td data-title="Service Name"><a target="_blank" href="{{url('service/'.$row->service_id.'/'.$row->service_slug)}}">{{ $row->service_name }}</a></td>
                                <td data-title="Contact No">{{$row->contact_no}}</td>
                                <td data-title="Status"><button class="custom-btn" title="Update Status" data-toggle="modal" data-target="#request_status_{{ $row->id }}"><?php echo $status; ?></button></td>
                                <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                <td data-title="Action">
                                <a class="btn btn-success" href="{{ url('/admin/view-service-request/'.$row->id) }}"><i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                            @include('backend.service.update_status', ['id' => $row->id, 'status' =>$row->status])
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    } );
</script>
@endsection