@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Service Request Details</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/service-request') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                <?php
                    $date = new DateTime($request->created_at);
                    if($request->status == 0) {
                        $status = '<span class="badge badge-primary">Pending <i class="fa fa-caret-down"></i></span>';
                    }
                    if($request->status == 1) {
                        $status = '<span class="badge badge-info">Processing <i class="fa fa-caret-down"></i></span>';
                    }
                    if($request->status == 2) {
                        $status = '<span class="badge badge-success">Completed</span>';
                    }
                    if($request->status == 3) {
                        $status = '<span class="badge badge-danger">Canceled</span>';
                    }
                ?>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td><strong>Store :</strong></td>
                                    <td>{{$request->store_name}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Customer Name :</strong></td>
                                    <td>{{$request->name}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Customer Address :</strong></td>
                                    <td>{{$request->address}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Service Name :</strong></td>
                                    <td><a target="_blank" href="{{url('service/'.$request->service_id.'/'.$request->service_slug)}}">{{ $request->service_name }}</a></td>
                                </tr>
                                <tr>
                                    <td><strong>Contact Number :</strong></td>
                                    <td>{{$request->contact_no}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Why Need :</strong></td>
                                    <td>{{$request->why_need}}</td>
                                </tr>
                                <tr>
                                    <td><strong>When Need :</strong></td>
                                    <td>{{$request->when_need}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Others Details :</strong></td>
                                    <td>{{$request->other_details}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Status :</strong></td>
                                    <td><button class="custom-btn" title="Update Status" data-toggle="modal" data-target="#request_status_{{ $request->id }}"><?php echo $status; ?></button></td>
                                </tr>
                                <tr>
                                    <td><strong>Request Date :</strong></td>
                                    <td>{{$date->format('d-F-Y')}}</td
                                </tr>
                            </tbody>
                        </table>
                        @include('backend.service.update_status', ['id' => $request->id, 'status' =>$request->status])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection