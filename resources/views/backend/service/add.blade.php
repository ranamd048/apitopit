@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Add New Service</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/service-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/save_service') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @if(!Session::has('store_id'))
                        <div class="form-group">
                            <label class="control-label col-form-label">Store List *</label>
                            <select class="form-control" name="store_id" required="">
                                <option value="">Select Store</option>
                                @foreach($store_list as $row)
                                    <option value="{{ $row->id }}">{{ $row->store_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Service Name * </label>
                                    <input type="text" name="service_name" class="form-control" required="" value="{{old('service_name')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Service Image {{ Session::get('is_image_required') == 1 ? '*' : '' }}<span class="image_size">(Size: 770 x 310 Max: 1024kb)</span></label>
                                    <input type="file" name="service_image" class="form-control" @if(Session::get('is_image_required') == 1) required="" @endif>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Category *</label>
                                    <select class="form-control" name="category_id" id="category_id" required="">
                                        <option value="">Select Category</option>
                                        @foreach($categories as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Sub Category</label>
                                    <select class="form-control" name="subcategory_id" id="subcategory_id">
                                        <option value="">Select Sub Category</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Second Sub Category</label>
                                    <select class="form-control" name="sub_subcategory_id" id="sub_subcategory_id">
                                        <option value="">Select Second Sub Category</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Experience </label>
                                    <input type="text" name="experience" class="form-control" value="{{ old('experience') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Service Type</label>
                                    <select class="form-control" name="service_type">
                                        <option value="Daily">Daily</option>
                                        <option value="Weekly">Weekly</option>
                                        <option value="Monthly">Monthly</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Video Link </label>
                                    <input type="text" name="video_link" class="form-control" value="{{old('video_link')}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-form-label">Description</label>
                                    <textarea name="service_description" class="form-control" placeholder="Service Description">{{old('service_description')}}</textarea>
                                </div>
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/') }}assets/plugins/ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'service_description' );

    $(document).ready(function() {
        $('#category_id').change(function() {
            var id = $(this).val();
            if(id == '') {
                var output = '<option value="">Select Sub Category</option>';
                $('#subcategory_id').html(output);
            } else {
                var url = '{{ url('/admin/subcategory_items/') }}'+'/'+id;
                $.ajax({
                    url:url,
                    method:"GET",
                    success:function(response)
                    {
                        $('#subcategory_id').html(response);
                    },
                });
            }
        });

        $('#subcategory_id').change(function () {
                var id = $(this).val();
                if (id == '') {
                    var output = '<option value="">Select Second Sub Category</option>';
                    $('#sub_subcategory_id').html(output);
                } else {
                    var url = '{{ url('/admin/subcategory_items/') }}' + '/' + id;
                    $.ajax({
                        url: url,
                        method: "GET",
                        success: function (response) {
                            $('#sub_subcategory_id').html(response);
                        },
                    });
                }
            });
    });
</script>
@endsection
