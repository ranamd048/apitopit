<div class="modal fade" id="request_status_{{ $id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title" id="exampleModalLongTitle">Update Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" action="{{ url('/admin/update_service_request_status') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="request_id" value="{{$id}}">
                <div class="modal-body">
                    <div class="modal-form">

                        <div class="form-group row">
                            <div class="col-md-12">
                                <select name="status" class="form-control" required="">
                                    <option value="">Select Status</option>
                                    @if($status == 0)
                                        <option value="1">Processing</option>
                                        <option value="2">Completed</option>
                                        <option value="3">Cancel</option>
                                    @endif
                                    @if($status == 1)
                                        <option value="2">Completed</option>
                                        <option value="3">Cancel</option>
                                    @endif
                                    @if($status == 2)
                                        <option value="2">Completed</option>
                                    @endif
                                    @if($status == 3)
                                        <option value="3">Cancel</option>
                                    @endif
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>