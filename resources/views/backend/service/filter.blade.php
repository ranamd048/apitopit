<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="product-filter">
            <input type="hidden" id="current_shop_id" value="{{$shop_id}}">
            <div class="form-group row">
                <?php $col_num = 4; ?>
                @if(!Session::has('store_id'))
                <div class="col-md-3">
                    <label for="">Search by Shop</label>
                    <select name="shop_id" id="shop_list" class="form-control">
                        @foreach($shop_list as $shop)
                        <option value="{{$shop->id}}">{{$shop->store_name}}</option>
                        @endforeach
                    </select>
                </div>
                <?php $col_num = 3; ?>
                @endif
                <div class="col-md-{{$col_num}}">
                    <label for="">Search by Category</label>
                    <select name="category_id" id="category_list" class="form-control">
                        <option value="">Select Shop</option>
                    </select>
                </div>
                <div class="col-md-{{$col_num}}">
                    <label for="">Search by Sub Category</label>
                    <select name="sub_category_id" id="sub_category_list" class="form-control">
                        <option value="">Select Sub Category</option>
                    </select>
                </div>
                <div class="col-md-{{$col_num}}">
                    <label for="">Search by Name</label>
                    <input type="text" class="form-control" id="search_query_string">
                </div>
            </div>
        </div>
    </div>
</div>