<div class="modal fade password_check_modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{ url('admin/password_check_for_delete') }}" method="POST">
        {{csrf_field()}}
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Password Verification</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="delete_id" id="delete_id">
                <input type="hidden" name="delete_for" id="delete_for">
                <div class="form-group">
                    <label class="col-form-label">Enter Password :</label>
                    <input type="password" class="form-control" name="password" required="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
  </div>
</div>
<script>
    $(document).ready(function() {
        $('.password_check_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var delete_for = button.data('for');
            var modal = $(this);
            modal.find('.modal-body input#delete_id').val(id);
            modal.find('.modal-body input#delete_for').val(delete_for);
        });
    });
</script>