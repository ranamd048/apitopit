@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Update Site Information</h4>
                <div class="form">

                    <form class="form-horizontal" action="{{ url('/admin/update_school') }}" method="POST" enctype="multipart/form-data">
                    	{{ csrf_field() }}
                        <input type="hidden" name="school_id" value="{{ $school->id }}">
                    	<input type="hidden" name="existing_logo" value="{{ $school->logo }}">
                    	<input type="hidden" name="existing_footer_logo" value="{{ $school->footer_logo }}">
                        {{-- <input type="hidden" name="existing_pp" value="{{ $school->principal_photo }}"> --}}
                        <div class="row">

                            <div class="col-md-6">
                                @if(Session::get('message'))
                                <p class="alert alert-success">{{ Session::get('message') }}</p>
                                @endif
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Select Template</label>
                                        <select class="form-control" name="template_name">
                                            <option <?php if($school->template_name == 'theme_one') { echo "selected"; } ?> value="theme_one">Theme One</option>
                                            <option <?php if($school->template_name == 'polytechnic') { echo "selected"; } ?> value="polytechnic">Theme Two</option>
                                            <option <?php if($school->template_name == 'theme_three') { echo "selected"; } ?> value="polytechnic">Theme Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Website Title</label>
                                        <input type="text" name="web_title" value="{{ $school->web_title }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Address</label>
                                        <input type="text" name="address" value="{{ $school->address }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Email</label>
                                        <input type="email" name="email" value="{{ $school->email }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Phone Number</label>
                                        <input type="text" name="phone_number" value="{{ $school->phone_number }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Facebook Link</label>
                                        <input type="text" name="fb_link" value="{{ $school->fb_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Twitter Link</label>
                                        <input type="text" name="twitter_link" value="{{ $school->twitter_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Google Plus Link</label>
                                        <input type="text" name="googlePlus_link" value="{{ $school->googlePlus_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Youtube Link</label>
                                        <input type="text" name="youtube_link" value="{{ $school->youtube_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Linkedin Link</label>
                                        <input type="text" name="linkedin_link" value="{{ $school->linkedin_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Instagram Link</label>
                                        <input type="text" name="instagram_link" value="{{ $school->instagram_link }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Map Link</label>
                                        <input type="text" name="maps_iframe" value="{{ $school->maps_iframe }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <img src="{{ FILE_BASE_URL.'public/new_uploads/'.$school->logo }}" width="120" alt=""> <br>
                                        <label class="control-label col-form-label">Update Logo</label>
                                        <input type="file" name="logo" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <img src="{{ FILE_BASE_URL.'public/new_uploads/'.$school->footer_logo }}" width="120" alt=""> <br>
                                        <label class="control-label col-form-label">Update Footer Logo</label>
                                        <input type="file" name="footer_logo" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Copyright Text</label>
                                        <input type="text" name="copyright_text" value="{{ $school->copyright_text }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">School Name</label>
                                        <input type="text" name="school_name" value="{{ $school->school_name }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Status</label>
                                        <select class="form-control" name="school_status">
                                            <option <?php if($school->school_status == 1) { echo "selected"; } ?> value="1">Active</option>
                                            <option <?php if($school->school_status == 0) { echo "selected"; } ?> value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Principal Name</label>
                                        <input type="text" name="principal_name" value="{{ $school->principal_name }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <img src="{{ FILE_BASE_URL.'public/new_uploads/'.$school->principal_photo) }}" width="120" alt=""> <br>
                                        <label class="control-label col-form-label">Update Principal Photo</label>
                                        <input type="file" name="principal_photo" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Principal Lecture</label>
                                        <textarea name="principal_desc" class="form-control">{{ $school->principal_desc }}</textarea>
                                    </div>
                                </div> --}}
                                <div lass="form-group row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-info btn-default">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    	
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection