@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Create New School</h4>
                <div class="form">
                    @if(Session::has('message'))
                    <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/create_school') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Select Template</label>
                                        <select class="form-control" name="template_name">
                                            <option value="edumart">Edumart</option>
                                            <option value="learnedu">Learnedu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">School Name</label>
                                        <input type="text" name="school_name" required="" class="form-control">
                                    </div>
                                </div>
                                <div lass="form-group row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-info btn-default">Create</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection