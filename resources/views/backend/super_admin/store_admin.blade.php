@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Create School Admin</h4>
                <div class="form">
                    @if(Session::get('message'))
                        <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/create_school_admin') }}" method="POST">
                    	{{ csrf_field() }}
                        <input type="hidden" name="school_id" value="{{ $school_id }}">
                        <input type="hidden" name="admin_id" value="{{ (!empty($admin->id)) ? $admin->id : '' }}">
                        <div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Name *</label>
                                        <input type="text" name="name" value="{{ (!empty($admin->name)) ? $admin->name : '' }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                        @if($errors->has('name'))
                                        <div class="invalid-feedback">
                                          {{ $errors->first('name') }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Email *</label>
                                        <input type="text" name="email" value="{{ (!empty($admin->email)) ? $admin->email : '' }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}">
                                        @if($errors->has('email'))
                                        <div class="invalid-feedback">
                                          {{ $errors->first('email') }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Phone</label>
                                        <input type="text" name="phone" value="{{ (!empty($admin->phone)) ? $admin->phone : '' }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Password *</label>
                                        <input type="password" name="password" value="" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}">
                                        @if($errors->has('password'))
                                        <div class="invalid-feedback">
                                          {{ $errors->first('password') }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div lass="form-group row">
                                    <button type="submit" class="btn btn-info btn-default">Create</button>
                                </div>
                            </div>
                        </div>
                    	
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection