@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">store List</h4>
                <div id="responsive_table">
                @if(Session::has('message'))
                    <p class="alert alert-success">{{ Session::get('message') }}</p>
                @endif
                    <table class="table table-bordered table-hover">
                        <tbody>
                            @if(count($store_list) != 0)
                            <tr>
                                <th>Sl No</th>
                                <th>Store Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <?php $i = 0; ?>
                            @foreach($store_list as $store)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Shop Name">{{ $store->store_name }}</td>
                                <td data-title="Status">{{ ($store->store_status == 1) ? 'Active' : 'Inactive' }}</td>
                                <td data-title="Action">
                                    <a class="btn btn-success" href="{{ url('/admin/edit-store/'.$store->id) }}"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-primary" href="{{ url('/admin/store-admin/'.$store->id) }}"><i class="fa fa-user"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td width="100%">Data Not Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection