@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Add New Menu</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/menu-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/save_menu') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Menu Title * </label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" required placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Parent Menu</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="parent_id">
                                    <option value="0">Don't Need Parent</option>
                                    @if(count($menu_list) > 0)
                                    	@foreach($menu_list as $menu)
                                			<option value="{{ $menu->id }}">{{ $menu->name }}</option>
                                		@endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Menu Type</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="menu_type" id="menu_type">
                                    <option value="page">Page</option>
                                    <option value="custom">Custom</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="page_menu">
                            <label class="col-sm-2 text-right control-label col-form-label">Select Page</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="page_link">
                                	@if(count($page_list) > 0)
                                		@foreach($page_list as $page)
                                			<option value="page/{{ $page->slug }}">{{ $page->title }}</option>
                                		@endforeach
                                	@else
                                		<option>At First Create Some Page</option>
                                	@endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="custom_menu" style="display: none;">
                            <label class="col-sm-2 text-right control-label col-form-label">Custom Link</label>
                            <div class="col-sm-10">
                                <input type="text" name="custom_link" class="form-control" placeholder="Custom Link">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Sort Order </label>
                            <div class="col-sm-10">
                                <input type="text" name="sort_order" class="form-control" placeholder="Sort Order">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
		$("#menu_type").change(function(){
			var menu_type = $(this).val();
			if(menu_type == 'page'){
				$("#page_menu").show();
				$("#custom_menu").hide();
			} else {
				$("#page_menu").hide();
				$("#custom_menu").show();
			}
			
		});
	});
</script>
@endsection