@extends('backend.layout')

@section('maincontent')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Shop Advertisement List</h4>
                <div class="back-button">
                    <a href="{{ url('admin/add-advertisement') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add</a>
                </div>
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Sort Order</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($shop_advertisements) > 0)
                            
                            <?php $i = 0; ?>
                            @foreach($shop_advertisements as $row)
                            <?php $i++; 
                                if($row->status == 1) {
                                    $status = '<span class="badge badge-success">Active</span>';                                
                                } else {
                                    $status = '<span class="badge badge-danger">Inactive</span>';  
                                }
                            ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Image"><img src="{{asset('public/uploads/'.$row->image_url)}}" width="150" alt=""></td>
                                <td data-title="Sort Order">{{ $row->sort_order }}</td>
                                <td data-title="Status"><?php echo $status; ?></td>
                                <td data-title="Action">
                                    <a class="btn btn-success" href="{{ url('/admin/edit-advertisement/'.$row->id) }}"><i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-danger" href="{{ url('/admin/delete-advertisement/'.$row->id) }}" onclick="return confirm('Are you sure want to delete?');"><i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    } );
</script>
@endsection