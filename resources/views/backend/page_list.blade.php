@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Page List</h4>
                <div class="back-button">
                    <a href="{{ url('admin/create-page') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Page</a>
                </div>
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if(Session::has('message'))
                        <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif
                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th>Serial No</th>
                                <th>Title</th>
                                <th>Page Type</th>
                                <th>Slug</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <?php $i = 0; ?>
                            @if(count($page_list) > 0)
                            @foreach($page_list as $page)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Title">{{ $page->title }}</td>
                                <td data-title="Page Type">{{ $page->page_type }}</td>
                                <td data-title="Slug">{{ $page->slug }}</td>
                                <td data-title="Status"><?php echo ($page->status == 1) ? 'Active' : 'Inactive'; ?></td>
                                <td data-title="Action">
                                    <a class="btn btn-success" href="{{ url('/admin/edit-page/'.$page->id) }}"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure want to delete this page?')" href="{{ url('/admin/delete-page/'.$page->id) }}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection