<form action="{{ url('admin/update_supplier') }}" method="POST">
      {{csrf_field()}}
<div class="modal-header">
  <h5 class="modal-title" id="add_expense_label">Edit Supplier</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
        <input type="hidden" name="supplier_id" value="{{$result->id}}" />
        <div class="form-group">
            <label for="">Name *</label>
            <input type="text" class="form-control" name="name" value="{{$result->name}}" required>
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="email" class="form-control" value="{{$result->email}}" name="email">
        </div>
        <div class="form-group">
            <label for="">Phone * (Ex: 01779221842)</label>
            <input type="text" class="form-control" name="phone" value="{{$result->phone}}" required>
        </div>
        <div class="form-group">
        <?php $divisions = Helpers::getDivisionList(); ?>
            <label for="">Division *</label>
            <select name="division" id="division_list" class="form-control" required>
                <option value="">Select Division</option>
                @foreach($divisions as $row)
                <option @if($result->division == $row->id) selected @endif value="{{ $row->id }}">{{ $row->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="">District *</label>
            <select name="district" id="district_list" class="form-control" required>
                <option value="{{$result->district}}">{{Helpers::districtName($result->district)}}</option>
            </select>
        </div>
        <div class="form-group">
            <label for="">Upazila *</label>
            <select name="upazila" id="upazila_list" class="form-control" required>
                <option value="{{$result->thana}}">{{Helpers::upazilaName($result->thana)}}</option>
            </select>
        </div>
        <div class="form-group">
            <label for="">Address *</label>
            <input type="text" class="form-control" name="address" value="{{$result->address}}" required>
        </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
    <script>
        $(document).ready(function() {
            //get district/upazila
            function get_selected_district_upazila(id, type) {
                var url = "{{ url('get_district_upazila') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, type: type, data_format: 'html'},
                    cache: false,
                    success: function (response) {
                        $('#'+type).html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            $('#division_list').change(function() {
                var division_id = $(this).val();
                if(division_id == '') {
                    $('#district_list').html('<option value="">Select District</option>');
                } else {
                    get_selected_district_upazila(division_id, 'district_list');
                }
                $('#upazila_list').html('<option value="">Select Upazila</option>');
            });

            $('#district_list').change(function() {
                var district_id = $(this).val();
                get_selected_district_upazila(district_id, 'upazila_list');
            });
        });
    </script>