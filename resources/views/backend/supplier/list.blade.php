@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Supplier List</h4>
                    <div class="back-button">
                        <a href="#" class="btn btn-primary text-capitalize load_modal" data-toggle="modal" data-action="{{ url('admin/add-supplier') }}"><i class="fa fa-plus"></i>Add Supplier</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        @if($errors->any())
                            <div class="alert alert-danger">
                                {{ $errors->first('phone') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>District</th>
                                <th>Payment</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($result) > 0)
                                <?php $i = 0; ?>
                                @foreach($result as $row)
                                    <?php $i++; ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Name">{{ $row->name }}</td>
                                        <td data-title="Email">{{ $row->email }}</td>
                                        <td data-title="Phone">{{ $row->phone }}</td>
                                        <td data-title="Address">{{ $row->address }}</td>
                                        <td data-title="District">{{ Helpers::districtName($row->district) }}</td>
                                        <td data-title="Add Payment"><a href="#" class="btn btn-primary btn-sm load_modal" data-toggle="modal" data-action="{{ url('admin/supplier-pay-now?due='.'&user_id='.$row->id) }}">Add Payment</a></td>
                                        <td data-title="Action">
                                            <div class="dropdown">
                                                <a class="btn btn-info dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-list"></i>
                                                </a>
                                                 
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                    <a href="{{ url('admin/supplier-details/'.$row->id) }}" class="dropdown-item">Supplier Details</a>
                                                    <a href="{{ url('admin/supplier-payment-history/'.$row->id) }}" class="dropdown-item">Paypent History</a>
                                                    <a href="#" class="dropdown-item load_modal" data-toggle="modal" data-action="{{url('/admin/edit-supplier/'.$row->id)}}">Edit Supplier</a>
                                                    <a href="{{url('/admin/delete-supplier/'.$row->id)}}" onclick="return confirm('Are you sure want to delete?');" class="dropdown-item">Delete Supplier</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable();
        } );
    </script>
@endsection



