<form class="form-horizontal" action="{{ url('admin/supplier-pay-now') }}" method="POST">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supplier Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
      <div class="modal-body">
          <input type="hidden" name="user_id" value="{{$user_id}}">
          <input type="hidden" name="payment_method" value="cod">
            <div class="form-group">
                <label for="name" class="col-form-label">Amount *</label>
                <input type="number" name="total_amount" class="form-control" value="{{ $due_amount }}" required="">
            </div>
            <div class="form-group">
                <label for="name" class="col-form-label">Note</label>
                <textarea name="payment_note" class="form-control"></textarea>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>