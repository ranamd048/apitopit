@extends('backend.layout')
<?php $type = isset($_GET['type']) ? $_GET['type'] : 'shop';?>

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Add New Coupon</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/coupon/list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    <form class="form-horizontal" action="{{ url('/admin/coupon/save_coupon') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Coupon Title * </label>
                            <div class="col-sm-10">
                                <input type="text" name="cupon_title" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Start Code * </label>
                            <div class="col-sm-10">
                                <input type="text" name="coupon_slug" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Quantity *</label>
                            <div class="col-sm-10">
                                <input type="number" name="cupon_qty" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Price *</label>
                            <div class="col-sm-10">
                                <input type="text" name="coupon_price" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Start Date *</label>
                            <div class="col-sm-10">
                                <input type="date" name="start_date" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">End Date *</label>
                            <div class="col-sm-10">
                                <input type="date" name="end_date" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Instruction</label>
                            <div class="col-sm-10">
                                <textarea name="cupon_instruction" class="form-control" placeholder="Coupon Instruction"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Show Homepage ?</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="show_homepage">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Description</label>
                            <div class="col-sm-10">
                                <textarea name="cupon_desc" class="form-control" placeholder="Coupon Description"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/') }}/assets/plugins/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'cupon_desc' );
    CKEDITOR.replace( 'cupon_instruction' );
</script>
@endsection