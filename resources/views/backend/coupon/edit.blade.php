@extends('backend.layout')
<?php $type = isset($_GET['type']) ? $_GET['type'] : 'shop';?>

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Edit Coupon</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/coupon/list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    <form class="form-horizontal" action="{{ url('/admin/coupon/update_coupon') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="coupon_id" value="{{$result->id}}">
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Coupon Title * </label>
                            <div class="col-sm-10">
                                <input type="text" name="cupon_title" class="form-control" value="{{$result->cupon_title}}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Start Code * </label>
                            <div class="col-sm-10">
                                <input type="text" name="coupon_slug" class="form-control" value="{{$result->coupon_slug}}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Quantity *</label>
                            <div class="col-sm-10">
                                <input type="number" name="cupon_qty" class="form-control" value="{{$result->cupon_qty}}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Price *</label>
                            <div class="col-sm-10">
                                <input type="text" name="coupon_price" class="form-control" value="{{$result->coupon_price}}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Start Date *</label>
                            <div class="col-sm-10">
                                <input type="date" name="start_date" class="form-control" value="{{$result->start_date}}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">End Date *</label>
                            <div class="col-sm-10">
                                <input type="date" name="end_date" class="form-control" value="{{$result->end_date}}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Instruction</label>
                            <div class="col-sm-10">
                                <textarea name="cupon_instruction" class="form-control" placeholder="Coupon Instruction">{{$result->cupon_instruction}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Show Homepage ?</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="show_homepage">
                                    <option @if($result->show_homepage == 1) selected @endif value="1">Yes</option>
                                    <option @if($result->show_homepage == 0) selected @endif value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Description</label>
                            <div class="col-sm-10">
                                <textarea name="cupon_desc" class="form-control" placeholder="Coupon Description">{{$result->cupon_desc}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option @if($result->status == 1) selected @endif value="1">Active</option>
                                    <option @if($result->status == 0) selected @endif value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Update</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/') }}/assets/plugins/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'cupon_desc' );
    CKEDITOR.replace( 'cupon_instruction' );
</script>
@endsection