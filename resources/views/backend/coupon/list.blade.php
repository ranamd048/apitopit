@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Coupon List</h4>
                    <div class="back-button">
                        <a href="{{ url('admin/coupon/add') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Coupon</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Start Code</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($coupon_list) > 0)
                                <?php $i = 0; ?>
                                @foreach($coupon_list as $row)
                                    <?php $i++; ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Title">{{ $row->cupon_title }}</td>
                                        <td data-title="Start Code">{{ $row->coupon_slug }}</td>
                                        <td data-title="Quantity">{{ $row->cupon_qty }}</td>
                                        <td data-title="Price">৳ {{ number_format($row->coupon_price, 2) }}</td>
                                        <td data-title="Start">{{ $row->start_date }}</td>
                                        <td data-title="End">{{ $row->end_date }}</td>
                                        <td data-title="Status">{{ ($row->status == 1) ? 'Active' : 'Inactive' }}</td>
                                        <td data-title="Action">
                                            <a target="_blank" href="{{url('/admin/coupon/coupon_users/'.$row->id)}}" class="btn btn-primary" title="Coupon User List"><i class="fa fa-list"></i></a>
                                            <a href="{{url('/admin/coupon/edit/'.$row->id)}}" class="btn btn-info" title="Edit"><i class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable();
        } );
    </script>
@endsection