@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">{{$coupon->cupon_title}}</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/coupon/list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>User Phone</th>
                                <th>User Address</th>
                                <th>Code</th>
                                <th>Recive Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($user_list) > 0)
                                <?php $i = 0; ?>
                                @foreach($user_list as $row)
                                    <?php $i++; $date = new DateTime($row->recive_date); ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="User Name">{{ $row->name }}</td>
                                        <td data-title="User Phone">{{ $row->phone }}</td>
                                        <td data-title="User Address">{{ $row->address }}</td>
                                        <td data-title="Code">{{ $row->coupon_code }}</td>
                                        <td data-title="Recive Date">{{$date->format('d-F-Y')}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable();
        } );
    </script>
@endsection