@extends('backend.layout')

@section('maincontent')
<style>
p.not_matched {
    color: red;
}
p.matched {
    color: green;
}
</style>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Add New Customer</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/customer-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="form">
                        @if($errors->any())
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach ($errors->all() as $error)
                                        <p class="alert alert-danger">{{$error}}</p>
                                    @endforeach
                                </div>
                            </div>
                            <br>
                        @endif
                        <form class="form-horizontal" action="{{ url('/admin/customer/save') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="row">

                                <div class="col-md-12">
                                    @if(!Session::has('store_id'))
                                    <div class="form-group">
                                        <label for="">Shop List *</label>
                                        <select name="store_id" id="store_id" class="form-control" required="">
                                            <option value="">Select Shop</option>
                                            @foreach($shop_list as $row)
                                            <option value="{{$row->id}}">{{$row->store_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @else
                                        <input type="hidden" name="store_id" value="{{Session::get('store_id')}}">
                                    @endif
                                    
                                    @if($private_settings->reference_registration == 1)
                                    <div class="form-group">
                                        <label for="reference_id">Reference</label>
                                        <input id="reference_id" type="text" class="form-control">
                                        <div id="ref_result"></div>
                                        <input type="hidden" id="reference_id_value" name="reference_id" value="0">
                                    </div>
                                    @endif

                                    <div class="form-group">
                                        <label for="">Name *</label>
                                        <input type="text" class="form-control" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="email" class="form-control" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Phone * (Ex: 01779221842)</label>
                                        <input type="text" class="form-control" name="phone" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Due Amount</label>
                                        <input type="number" class="form-control" name="due_amount" value="0">
                                    </div>
                                    <div class="form-group">
                                    <?php $divisions = Helpers::getDivisionList(); ?>
                                        <label for="">Division *</label>
                                        <select name="division" id="division_list" class="form-control" required>
                                            <option value="">Select Division</option>
                                            @foreach($divisions as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">District *</label>
                                        <select name="district" id="district_list" class="form-control" required>
                                            <option value="">Select District</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Upazila *</label>
                                        <select name="upazila" id="upazila_list" class="form-control" required>
                                            <option value="">Select Upazila</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Address *</label>
                                        <input type="text" class="form-control" name="address" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Gender *</label>
                                        <select name="gender" id="gender" class="form-control">
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Password *</label>
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Confirm Password *</label>
                                        <input type="password" class="form-control" name="password_confirmation">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            //get district/upazila
            function get_selected_district_upazila(id, type) {
                var url = "{{ url('get_district_upazila') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, type: type, data_format: 'html'},
                    cache: false,
                    success: function (response) {
                        $('#'+type).html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            $('#division_list').change(function() {
                var division_id = $(this).val();
                if(division_id == '') {
                    $('#district_list').html('<option value="">Select District</option>');
                } else {
                    get_selected_district_upazila(division_id, 'district_list');
                }
                $('#upazila_list').html('<option value="">Select Upazila</option>');
            });

            $('#district_list').change(function() {
                var district_id = $(this).val();
                get_selected_district_upazila(district_id, 'upazila_list');
            });

            $('#reference_id').keyup(function() {
                var reference_id = $(this).val();
                var url = "{{ url('customer/check_reference_id') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {reference_id: reference_id},
                    cache: false,
                    success: function (response) {
                        if(response == 0) {
                            $('#ref_result').html('<p class="not_matched">Reference Not matched</p>');
                            $('#reference_id_value').val('0');
                        } else {
                            $('#ref_result').html('<p class="matched">Reference matched</p>');
                            $('#reference_id_value').val(response);
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            });
        });
    </script>
@endsection