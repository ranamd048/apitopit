@extends('backend.layout')

@section('maincontent')
<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
    .product-filter button, .product-filter a {
        margin-top: 32px;
    }
    .product-filter .btn {
        margin-top: 40px;
    }
    @media only screen and (max-width: 767px)  {
        #responsive_table table, #responsive_table thead, #responsive_table tbody, #responsive_table th, #responsive_table td, #responsive_table tr {
            overflow: unset !important;
        }
        .product-filter .btn {
            margin-top: 0;
        }
        #responsive_table td {
            padding-left: 32%;
        }
        #responsive_table table, #responsive_table thead, #responsive_table tbody, #responsive_table th, #responsive_table td, #responsive_table tr {
            overflow: hidden;
        }
    }
</style>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Customer List</h4>
                    <div class="back-button">
                    @if(Session::get('role') == 0)
                        <a href="{{ url('admin/number_export/users/phone') }}" class="btn btn-success"><i class="fa fa-download"></i>Number Export</a>
                    @endif
                        <a href="{{ url('admin/customer/export') }}" class="btn btn-success"><i class="fa fa-download"></i>Export Data</a>
                        <a href="{{ url('admin/customer/add') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add  Customer</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" method="GET">
                                <div class="product-filter">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label>Search By Name</label>
                                            <input type="text" name="name" value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}" class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Search By Mobile Number</label>
                                            <input type="text" name="mobile_number" value="{{ isset($_GET['mobile_number']) ? $_GET['mobile_number'] : '' }}" class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Search</button>
                                            <a href="{{ url('admin/customer-list') }}" class="btn btn-danger btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @if (Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div id="responsive_table">
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone No</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($customer_list) > 0)
                                <?php $i = 0;?>
                                @foreach($customer_list as $row)
                                    <?php 
                                        $i++; 
                                        if($row->status == 1) {
                                            $status = '<span class="badge badge-success">Active</span>';
                                        } else {
                                            $status = '<span class="badge badge-danger">Inactive</span>';
                                        } 
                                    ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Name">{{ $row->name }}</td>
                                        <td data-title="Email">{{ $row->email }}</td>
                                        <td data-title="Phone">{{ $row->phone }}</td>
                                        <td data-title="Address">{{ $row->address }}</td>
                                        <td data-title="Status">
                                            <?php echo $status; ?>
                                        </td>
                                        <td data-title="Action">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".edit_customer_modal" data-id="{{$row->id}}" data-name="{{ $row->name }}" data-email="{{ $row->email }}" data-status="{{ $row->status }}"><i class="fa fa-pencil"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Customer Edit Modal-->
    <div class="modal fade edit_customer_modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <form action="{{ url('admin/update_customer_info') }}" method="POST">
        {{csrf_field()}}
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Update Customer</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="customer_id" id="customer_id">
            <div class="form-group">
                <label for="email" class="col-form-label">Email :</label>
                <input type="email" class="form-control" name="email" id="email">
            </div>
            <div class="form-group">
                <label for="password" class="col-form-label">Password :</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
            <div class="form-group">
                <label class="control-label col-form-label">Status :</label>
                <select class="form-control" name="status" id="status">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </div>
        </form>
    </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.edit_customer_modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var id = button.data('id');
                var name = button.data('name');
                var email = button.data('email');
                var status = button.data('status');
                var modal = $(this);
                modal.find('.modal-title').text('Update for ' + name);
                modal.find('.modal-body input#customer_id').val(id);
                modal.find('.modal-body input#email').val(email);
                modal.find('.modal-body select#status').val(status);
            });
            $('#common_datatable_id').DataTable();
        });
    </script>
@endsection