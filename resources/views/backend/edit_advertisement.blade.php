@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Edit Shop Advertisement</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/shop_advertisement') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/update_advertisement') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="advertisement_id" value="{{$result->id}}">
                        <input type="hidden" name="existing_image" value="{{$result->image_url}}">
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Shop Link * </label>
                            <div class="col-sm-10">
                                <input type="text" name="shop_link" class="form-control" value="{{$result->shop_link}}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Image *</label>
                            
                            <div class="col-sm-10">
                            <img src="{{asset('public/uploads/'.$result->image_url)}}" width="200" alt=""> <br><br>
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Sort Order *</label>
                            <div class="col-sm-10">
                                <input type="text" name="sort_order" value="{{$result->sort_order}}" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option @if($result->status == 1) selected @endif value="1">Active</option>
                                    <option @if($result->status == 0) selected @endif value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Update</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection