<form action="{{ url('admin/update_expense_category') }}" method="POST">
      {{csrf_field()}}
<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Expense Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <input type="hidden" name="category_id" id="category_id" value="{{$result->id}}">
            <div class="form-group">
                <label for="name" class="col-form-label">Name *</label>
                <input type="text" class="form-control" name="name" id="name" value="{{$result->name}}" required="">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>