<?php $store_id = Session::get('store_id'); ?>
<form action="{{ url('admin/save_expense_category') }}" method="POST">
      {{csrf_field()}}
<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Expense Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            @if($store_id)
            <input type="hidden" name="store_id" value="{{ $store_id }}">
            @else
            <div class="form-group">
                <label for="store_id" class="col-form-label">Store *</label>
                <select name="store_id" class="form-control" required="">
                    <option value="">Select a Store</option>
                    @foreach($store_list as $row)
                    <option value="{{$row->id}}">{{$row->store_name}}</option>
                    @endforeach
                </select>
            </div>
            @endif
            <div class="form-group">
                <label for="name" class="col-form-label">Name *</label>
                <input type="text" class="form-control" name="name" id="name" required="">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>