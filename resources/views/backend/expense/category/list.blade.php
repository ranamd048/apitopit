@extends('backend.layout')

@section('maincontent')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title text-capitalize">Expense Category List</h4>
                <div class="back-button">
                    <a href="#" class="btn btn-primary text-capitalize load_modal" data-toggle="modal" data-action="{{ url('admin/add-expense-category') }}"><i class="fa fa-plus"></i>Add Expense Category</a>
                </div>
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($result) > 0)
                            
                            <?php $i = 0; ?>
                            @foreach($result as $row)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Name">{{ $row->name }}</td>
                                <td data-title="Action">
                                    @if($row->name !== 'Staff')
                                    <a href="#" class="btn btn-success load_modal" data-toggle="modal" data-action="{{ url('admin/edit-expense-category/'.$row->id) }}"><i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure want to delete?');" href="{{ url('/admin/delete-expense-category/'.$row->id) }}"><i class="fa fa-trash"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    } );
</script>
@endsection