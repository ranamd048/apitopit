<form action="{{ url('admin/save_expense') }}" method="POST" enctype="multipart/form-data">
      {{csrf_field()}}
<div class="modal-header">
  <h5 class="modal-title" id="add_expense_label">Add Expense</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
      <div class="form-group">
          <label class="col-form-label">Date</label>
          <input type="date" class="form-control" name="date" value="{{date('Y-m-d')}}">
      </div>
      <div class="form-group">
          <label class="col-form-label">Expense Category *</label>
          <select name="category_id" class="form-control"  required="">
              <option value="">Select Expense Category</option>
              @if(count($expense_categories) > 0)
                @foreach($expense_categories as $row)
                <option value="{{$row->id}}">{{$row->name}}</option>
                @endforeach
              @endif
          </select>
      </div>
      <div class="form-group">
          <label for="amount" class="col-form-label">Amount *</label>
          <input type="text" class="form-control" name="amount" id="amount" required="">
      </div>
      <div class="form-group">
          <label for="note" class="col-form-label">Note</label>
          <textarea class="form-control" name="note" id="note"></textarea>
      </div>
      <div class="form-group">
          <label for="attachment" class="col-form-label">Attachment</label>
          <input type="file" class="form-control" name="attachment" id="attachment">
      </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>