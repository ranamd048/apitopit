<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
    .product-filter button, .product-filter a {
        margin-top: 32px;
    }
</style>
<?php
    $from_date = isset($_GET['from_date']) ? $_GET['from_date'] : '';
    $to_date = isset($_GET['to_date']) ? $_GET['to_date'] : '';
    $category_id = isset($_GET['category_id']) ? $_GET['category_id'] : '';
?>
<div class="row">
    <div class="col-md-12">
        <form action="" method="GET">
            <div class="product-filter">
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>From Date *</label>
                        <input type="date" name="from_date" required="" value="{{ $from_date }}" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label>To Date *</label>
                        <input type="date" name="to_date" required="" value="{{ $to_date }}" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label>Category</label>
                        <select name="category_id" class="form-control" id="category_list">
                            <option value="">Select Category</option>
                            @if(count($expense_categories) > 0)
                                @foreach ($expense_categories as $category)
                                    <option @if($category_id == $category->id) selected="" @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Filter</button>
                        <a href="{{ url('admin/expense-list') }}" class="btn btn-danger"><i class="fa fa-refresh"></i> Reset</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#category_list').select2();
    });
</script>