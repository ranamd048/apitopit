@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Edit Store Category</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/store_categories') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    <form class="form-horizontal" action="{{ url('/admin/update_store_category') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="store_category_id" value="{{$store_category->id}}">                      
                        <input type="hidden" name="existing_img" value="{{$store_category->icon}}">                      
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Name * </label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" required="" value="{{$store_category->name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Image</label>
                            <div class="col-sm-10">
                                @if($store_category->icon)
                                <img src="{{ asset('public/uploads/store_category/'.$store_category->icon) }}" width="120" alt=""><br><br>
                                @endif
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Sort Order</label>
                            <div class="col-sm-10">
                                <input type="number" name="sort_order" class="form-control" value="{{$store_category->sort_order}}">
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection