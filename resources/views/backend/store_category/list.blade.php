@extends('backend.layout')

@section('maincontent')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title text-capitalize">Store Category List</h4>
                @if(Session::get('role') == 0)
                <div class="back-button">
                    <a href="{{ url('admin/create-store-category') }}" class="btn btn-primary text-capitalize"><i class="fa fa-plus"></i>Add New</a>
                </div>
                @endif
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Sort Order</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($store_categories) > 0)
                            
                            <?php $i = 0; ?>
                            @foreach($store_categories as $row)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Name">{{ $row->name }}</td>
                                <td data-title="Image"><img src="{{ asset('/public/uploads/store_category/'.$row->icon) }}" width="120" alt=""></td>
                                <td data-title="Sort Order">{{$row->sort_order}}</td>
                                <td data-title="Action">
                                    <a class="btn btn-success" href="{{ url('/admin/edit-store-category/'.$row->id) }}"><i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-danger" href="{{ url('/admin/delete-store-category/'.$row->id) }}"><i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    } );
</script>
@endsection