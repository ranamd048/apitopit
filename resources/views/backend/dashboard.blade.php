@extends('backend.layout')

@section('maincontent')
<style>
a.summary-link {
    display: block;
    padding: 30px 0;
    text-align: center;
    border-radius: 5px;
    margin-bottom: 30px;
}
a.summary-link i {
    color: #fff;
    font-size: 30px;
}
a.summary-link h3 {
    color: #fff;
    text-transform: uppercase;
    font-size: 15px;
    margin-bottom: 0;
}
a.summary-link span {
    color: #fff;
    font-size: 20px;
    display: block;
    margin-bottom: 5px;
}
.row.filter-report {
    margin-bottom: 30px;
    overflow: hidden;
}
.shop_overview_section {
    display: none;
}
.custom h4.card-title {
    float: unset !important;
    color: #333;
    margin: 0;
    text-align: center !important;
    text-transform: uppercase;
    font-weight: 700;
}
.card-block.custom {
    margin-top: 0;
}
.custom_card {
    overflow: hidden;
    border: 1px solid #ddd;
    margin-top: 10px;
    margin-bottom: 10px;
    padding: 10px;
    text-align: center;
    border-radius: 5px;
    background: #f2f7f8;
    color: #333;
    display: block;
}
.custom_card a {
    color: #333333;
}
.custom_card p {
    margin-bottom: 5px;
}
.custom_card h3 {
    margin: 0;
    font-size: 16px;
    line-height: 22px;
}
.custom_card img.icon {
    width: 75px;
    height: 75px;
}
.report_summary_btn {
    margin-top: 32px;
}
div#desktop_menu_only {
    display: block;
    opacity: 1;
    visibility: visible;
}
div#mobile_menu_only {
    display: none;
    opacity: 0;
    visibility: hidden;
}

/*** Responsive CSS ***/
@media only screen and (max-width: 767px) {
    div#desktop_menu_only {
        display: none;
        opacity: 0;
        visibility: hidden;
    }
    div#mobile_menu_only {
        display: block;
        opacity: 1;
        visibility: visible;
    }
    .shop_overview_section {
        overflow: hidden;
        display: block;
    }
    .card-block.custom.shop_bg {
        padding: 0;
        margin: 0;
    }
    .shop_overview_section .shop-background img {
        width: 100%;
        height: 150px;
    }
    .report_summary_btn {
        margin-top: 0px;
    }
    .report_summary_filter .form-group {
        margin-bottom: 10px;
    }
    .report_summary_filter .form-group label {
        margin-bottom: 1px;
    }
}

</style>
<?php
    $admin_permission = DB::table('admins')->where('id', Session::get('admin_id'))->first();
?>
@if(Session::get('role') == 0)
<div class="row hide-mobile">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
            	<form action="{{ url('/admin/dashboard') }}" method="GET">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group text-right">
                                <label class="control-label col-form-label" for="store_id">Select Store</label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="store_id" class="form-control" id="store_id">
                                    <option value="">Select Store</option>
                                    @foreach($store_list as $store)
                                    <option @if(Session::get('store_id') == $store->id) selected="" @endif value="{{ $store->id }}">{{ $store->store_name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Select</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

@if(Session::get('role') == 0)
<?php
    $years_array = array_combine(range(date("Y"), 2019), range(date("Y"), 2019));
    $months_array = array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
    $current_month = Carbon\Carbon::now()->month;
    $current_year = Carbon\Carbon::now()->year;
    if(isset($_GET['year'])) {
        $current_year = $_GET['year'];
    }
    if(isset($_GET['month'])) {
        $current_month = $_GET['month'];
    }
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-success">
                <h4 class="card-title">Monthly Summary</h4>
            </div>
            <div class="card-block">
                <form action="" method="GET">
                    <div class="row filter-report">
                        <div class="col-5">
                            <label for="">Select Year</label>
                            <select name="year" class="form-control">
                                @foreach($years_array as $key => $value)
                                    <option  @if($current_year == $key) selected @endif value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-5">
                            <label for="">Month</label>
                            <select name="month" class="form-control">
                                    <option value="">Select Month</option>
                                @foreach($months_array as $key => $value)
                                    <option @if($current_month == $key) selected @endif value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-2">
                            <button type="submit" style="margin-top:33px; margin-left: -25px" class="btn btn-info">Submit</button>
                        </div>
                    </div>
                </form>
            	<div class="row">
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-info">
                            <span>{{Helpers::monthlyCustomerCount($current_year, $current_month)}}</span>
                            <h3>Customer Added</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-success">
                            <span>{{Helpers::monthlyShopCount($current_year, $current_month)}}</span>
                            <h3>Shop Added</h3>
                        </a>
                    </div>
                    <div class="col-md-4 col-6">
                        <a class="summary-link bg bg-primary">
                            <span>{{Helpers::monthlyOrderCount($current_year, $current_month)}}</span>
                            <h3>New Order</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(Session::has('store_id'))
<div class="row shop_overview_section">
    <div class="col-12">
        <div class="card">
            <div class="card-header custom text-center">
            <?php 
                $store = DB::table('stores')->where('id', Session::get('store_id'))->first();
            ?>
                <h4 class="card-title">{{ $store->store_name }}</h4>
            </div>
            <div class="card-block custom shop_bg">
            	<div class="row">
                    <div class="col-md-12">
                        <div class="shop-background">
                            <img src="{{ asset('public/uploads/store/'.$store->baner_image) }}" alt="{{ $store->store_name }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(Session::has('store_id') && (Session::get('store_type') == 'both' || Session::get('store_type') == 'shop'))
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header custom text-center">
                <h4 class="card-title">Report Summary</h4>
            </div>
            <div class="card-block custom">
                <a href="{{ url('admin/report-summary') }}" class="btn btn-info btn-block"><i class="fa fa-eye"></i> View Today Report Summary</a>
            </div>
        </div>
    </div>
</div>
@endif

<!-- Start Mobile Menu-->
<div id="mobile_menu_only">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block custom">
                    <div class="row">
                        @if(Session::get('role') == 0)
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/product-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/product.png') }}" alt="">
                                    <h3>Products</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/service-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/service.png') }}" alt="">
                                    <h3>Services</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/category-list/shop')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/product_category.png') }}" alt="">
                                    <h3>Categories</h3>
                                </a>
                            </div>
                        </div>
                        @endif
    
                        @if(Session::get('role') == 1 && Session::get('store_type') == 'both')
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/pos')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/pos.png') }}" alt="">
                                    <h3>Pos</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/product-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/product.png') }}" alt="">
                                    <h3>Products</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/service-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/service.png') }}" alt="">
                                    <h3>Services</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/category-list/shop')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/product_category.png') }}" alt="">
                                    <h3>Categories</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                        @if(Session::get('role') == 1 && Session::get('store_type') == 'shop')
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/pos')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/pos.png') }}" alt="">
                                    <h3>Pos</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/product-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/product.png') }}" alt="">
                                    <h3>Products</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/category-list/shop')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/product_category.png') }}" alt="">
                                    <h3>Categories</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                        @if(Session::get('role') == 1 && Session::get('store_type') == 'service')
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/service-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/service.png') }}" alt="">
                                    <h3>Services</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/category-list/service')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/service.png') }}" alt="">
                                    <h3>Service Categories</h3>
                                </a>
                            </div>
                        </div>
                        @endif
    
                        @if($admin_permission->order_access == 1)
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/orders')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/shop_order.png') }}" alt="">
                                    <h3>Orders</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                        @if(Session::get('role') == 0)
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/store-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/shop_settings.png') }}" alt="">
                                    <h3>Shop List</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                        @if(Session::get('role') == 1 && $admin_permission->shop_settings == 1)
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/shop_settings')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/shop_settings.png') }}" alt="">
                                    <h3>Shop Settings</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                        @if(Session::has('store_id') && $private_settings->sms_service == 1)
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/sms-page')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/sms.png') }}" alt="">
                                    <h3>SMS Page</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                        @if(Session::has('store_id'))
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/product-stock-alert')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/product.png') }}" alt="">
                                    <h3>Stock Alert ({{count(Helpers::productStockAlert())}})</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @if(Session::get('role') == 1 && Session::get('admin_type') == 1)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Order</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        @if(Session::get('store_type') == 'both')
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/orders')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/shop_order.png') }}" alt="">
                                    <h3>Online Orders</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/pos-orders')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/pos.png') }}" alt="">
                                    <h3>POS Orders</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/service-request')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/service_request.png') }}" alt="">
                                    <h3>Service Request</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                        @if(Session::get('store_type') == 'shop')
                        <div class="col-md-6 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/orders')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/shop_order.png') }}" alt="">
                                    <h3>Online Orders</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/pos-orders')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/pos.png') }}" alt="">
                                    <h3>POS Orders</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                        @if(Session::get('store_type') == 'service')
                        <div class="col-md-6 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/service-request')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/service_request.png') }}" alt="">
                                    <h3>Service Request</h3>
                                </a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    @if((Session::get('role') == 0 || Session::get('role') == 1) && Session::get('admin_type') == 1)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Customer</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/customer-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/customer.png') }}" alt="">
                                    <h3>Customer List</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/customer/add')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/customer.png') }}" alt="">
                                    <h3>Add Customer</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{ url('/admin/account/due-list') }}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/customer.png') }}" alt="">
                                    <h3>Due Customer</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{ url('/admin/account/paid-list') }}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/customer.png') }}" alt="">
                                    <h3>Paid Customer</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    @if(Session::get('role') == 1 && Session::get('admin_type') == 1)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Expense</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/expense-categories')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/expense.png') }}" alt="">
                                    <h3>Expense Category</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="#" class="load_modal" data-toggle="modal" data-action="{{url('admin/add-expense-category')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/expense.png') }}" alt="">
                                    <h3>Add Expense Category</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/expense-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/expense.png') }}" alt="">
                                    <h3>Expense List</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="#" class="load_modal" data-toggle="modal" data-action="{{url('admin/add-expense')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/expense.png') }}" alt="">
                                    <h3>Add Expense</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Purchase</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/supplier-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/supplier.png') }}" alt="">
                                    <h3>Supplier List</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="#" class="load_modal" data-toggle="modal" data-action="{{url('admin/add-supplier')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/supplier.png') }}" alt="">
                                    <h3>Add Supplier</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/purchase-list')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/purchase.png') }}" alt="">
                                    <h3>Purchase List</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/add-purchase')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/purchase.png') }}" alt="">
                                    <h3>Add Purchase</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Staff</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/shop-staffs')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/staff.png') }}" alt="">
                                    <h3>Staff List</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="#" class="load_modal" data-toggle="modal" data-action="{{url('admin/add-shop-staff')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/staff.png') }}" alt="">
                                    <h3>Add Staff</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/shop-staff-attendance')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/staff.png') }}" alt="">
                                    <h3>Staff Attendance</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/shop-staff-payments')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/staff.png') }}" alt="">
                                    <h3>Staff Payment</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    @if(Session::get('role') == 0)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Notifications</h4>
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/inactive-shop')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/shop_settings.png') }}" alt="">
                                    <h3>Inactive Shop ({{Helpers::inactiveShopCount()}})</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/date-expire-shop')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/shop_settings.png') }}" alt="">
                                    <h3>Date Expire Shop ({{Helpers::dateExpireShopCount()}})</h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="custom_card">
                                <a href="{{url('admin/inactive-product')}}">
                                    <img class="icon" src="{{ asset('public/beasy_icons/shop_settings.png') }}" alt="">
                                    <h3>Inactive Product ({{Helpers::inactiveProductCount()}})</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<!-- End Mobile Menu-->

<!-- Start Desktop Menu-->
<div id="desktop_menu_only">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block custom">
                    <div class="row">
                        @if(Session::get('role') == 0)
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/product-list')}}" class="summary-link bg bg-primary">
                                <h3>Products</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/service-list')}}" class="summary-link bg bg-info">
                                <h3>Services</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/category-list/shop')}}" class="summary-link bg bg-success">
                                <h3>Categories</h3>
                            </a>
                        </div>
                        @endif
    
                        @if(Session::get('role') == 1 && Session::get('store_type') == 'both')
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/pos')}}" class="summary-link bg bg-danger">
                                <h3>Pos</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/product-list')}}" class="summary-link bg bg-primary">
                                <h3>Products</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/service-list')}}" class="summary-link bg bg-success">
                                <h3>Services</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/category-list/shop')}}" class="summary-link bg bg-success">
                                <h3>Categories</h3>
                            </a>
                        </div>
                        @endif
                        @if(Session::get('role') == 1 && Session::get('store_type') == 'shop')
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/pos')}}" class="summary-link bg bg-danger">
                                <h3>Pos</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/product-list')}}" class="summary-link bg bg-warning">
                                <h3>Products</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/category-list/shop')}}" class="summary-link bg bg-success">
                                <h3>Categories</h3>
                            </a>
                        </div>
                        @endif
                        @if(Session::get('role') == 1 && Session::get('store_type') == 'service')
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/service-list')}}" class="summary-link bg bg-info">
                                <h3>Services</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/category-list/service')}}" class="summary-link bg bg-primary">
                                <h3>Service Categories</h3>
                            </a>
                        </div>
                        @endif
    
                        @if($admin_permission->order_access == 1)
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/orders')}}" class="summary-link bg bg-info">
                                <h3>Orders</h3>
                            </a>
                        </div>
                        @endif
                        @if(Session::get('role') == 0)
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/store-list')}}" class="summary-link bg bg-sucess">
                                <h3>Shop List</h3>
                            </a>
                        </div>
                        @endif
                        @if(Session::get('role') == 1 && $admin_permission->shop_settings == 1)
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/shop_settings')}}" class="summary-link bg bg-primary">
                                <h3>Shop Settings</h3>
                            </a>
                        </div>
                        @endif
                        @if(Session::has('store_id') && $private_settings->sms_service == 1)
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/sms-page')}}" class="summary-link bg bg-info">
                                <h3>SMS Page</h3>
                            </a>
                        </div>
                        @endif
                        @if(Session::has('store_id'))
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/product-stock-alert')}}" class="summary-link bg bg-warning">
                                <h3>Stock Alert ({{count(Helpers::productStockAlert())}})</h3>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @if(Session::get('role') == 1 && Session::get('admin_type') == 1)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Order</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        @if(Session::get('store_type') == 'both')
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/orders')}}" class="summary-link bg bg-primary">
                                <h3>Online Orders</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/pos-orders')}}" class="summary-link bg bg-danger">
                                <h3>POS Orders</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/service-request')}}" class="summary-link bg bg-success">
                                <h3>Service Request</h3>
                            </a>
                        </div>
                        @endif
                        @if(Session::get('store_type') == 'shop')
                        <div class="col-md-6 col-6">
                            <a href="{{url('admin/orders')}}" class="summary-link bg bg-primary">
                                <h3>Online Orders</h3>
                            </a>
                        </div>
                        <div class="col-md-6 col-6">
                            <a href="{{url('admin/pos-orders')}}" class="summary-link bg bg-danger">
                                <h3>POS Orders</h3>
                            </a>
                        </div>
                        @endif
                        @if(Session::get('store_type') == 'service')
                        <div class="col-md-6 col-6">
                            <a href="{{url('admin/service-request')}}" class="summary-link bg bg-warning">
                                <h3>Service Request</h3>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    @if((Session::get('role') == 0 || Session::get('role') == 1) && Session::get('admin_type') == 1)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Customer</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/customer-list')}}" class="summary-link bg bg-info">
                                <h3>Customer List</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/customer/add')}}" class="summary-link bg bg-success">
                                <h3>Add Customer</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{ url('/admin/account/due-list') }}" class="summary-link bg bg-danger">
                                <h3>Due Customer</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{ url('/admin/account/paid-list') }}" class="summary-link bg bg-primary">
                                <h3>Paid Customer</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    @if(Session::get('role') == 1 && Session::get('admin_type') == 1)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Expense</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/expense-categories')}}" class="summary-link bg bg-warning">
                                <h3>Expense Category</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="#" class="load_modal summary-link bg bg-success" data-toggle="modal" data-action="{{url('admin/add-expense-category')}}">
                                <h3>Add Expense Category</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/expense-list')}}" class="summary-link bg bg-primary">
                                <h3>Expense List</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="#" class="load_modal summary-link bg bg-info" data-toggle="modal" data-action="{{url('admin/add-expense')}}">
                                <h3>Add Expense</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Purchase</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/supplier-list')}}" class="summary-link bg bg-info">
                                <h3>Supplier List</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="#" class="load_modal summary-link bg bg-primary" data-toggle="modal" data-action="{{url('admin/add-supplier')}}">
                                <h3>Add Supplier</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/purchase-list')}}" class="summary-link bg bg-danger">
                                <h3>Purchase List</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/add-purchase')}}" class="summary-link bg bg-success">
                                <h3>Add Purchase</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Staff</h4>  
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/shop-staffs')}}" class="summary-link bg bg-warning">
                                <h3>Staff List</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="#" class="load_modal summary-link bg bg-danger" data-toggle="modal" data-action="{{url('admin/add-shop-staff')}}">
                                <h3>Add Staff</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/shop-staff-attendance')}}" class="summary-link bg bg-primary">
                                <h3>Staff Attendance</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{url('admin/shop-staff-payments')}}" class="summary-link bg bg-info">
                                <h3>Staff Payment</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    @if(Session::get('role') == 0)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header custom">
                    <h4 class="card-title">Notifications</h4>
                </div>
                <div class="card-block custom">
                    <div class="row">
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/inactive-shop')}}" class="summary-link bg bg-danger">
                                <h3>Inactive Shop ({{Helpers::inactiveShopCount()}})</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/date-expire-shop')}}" class="summary-link bg bg-warning">
                                <h3>Date Expire Shop ({{Helpers::dateExpireShopCount()}})</h3>
                            </a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="{{url('admin/inactive-product')}}" class="summary-link bg bg-info">
                                <h3>Inactive Product ({{Helpers::inactiveProductCount()}})</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<!-- End Desktop Menu-->

@endsection