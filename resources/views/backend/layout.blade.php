<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Ginilab">
    <base href="{{ url('/') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {{-- {{$site_settings->web_title}} --}}
    <title>@yield('title_text', 'Apitopit Multi Store Platform')</title>
    <link href="{{ asset('/') }}assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('/') }}assets/css/style.css" rel="stylesheet">
    <link href="{{ asset('/') }}assets/css/colors/red.css" id="theme" rel="stylesheet">
    <link href="{{ asset('/') }}assets/css/custom.css" id="theme" rel="stylesheet">
    <link href="{{ asset('/') }}assets/css/intlTelInput.css" rel="stylesheet">
    <link href="{{ asset('/') }}assets/plugins/global-calendars/jquery.calendars.picker.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}assets/css/summernote.css" rel="stylesheet">
    <link href="{{ asset('/') }}assets/css/select2.min.css" rel="stylesheet">
    <link href="{{ asset('/') }}assets/css/custom_2.css" rel="stylesheet">
    <link href="{{ asset('/') }}assets/css/jquery-simple-mobilemenu-slide.css" rel="stylesheet">
    <link href="{{ asset('/') }}assets/css/responsive.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="{{ asset('/') }}assets/plugins/jquery/jQuery-2.1.4.min.js"></script>
<script src="{{ asset('/') }}assets/js/datatable.min.js"></script>
<script src="{{ asset('/') }}assets/js/select2.min.js"></script>
<script src="{{ asset('/') }}assets/js/summernote.js"></script>
<script src="{{ asset('/') }}assets/js/jquery-simple-mobilemenu.js"></script>
<style>
div#load_all_products i.fa-spin {font-size: 40px;}label span.image_size {
    font-size: 12px;
}
.select2-container .select2-selection--single {
    height: 37px;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 32px;
}
:root {
    --theme-color: {{$site_settings->theme_color}};
}
input[type=checkbox] {
    position: relative;
	cursor: pointer;
}
input[type=checkbox]:before {
    content: "";
    display: block;
    position: absolute;
    width: 24px;
    height: 24px;
    top: 0;
    left: 0;
    border: 2px solid #555555;
    border-radius: 3px;
    background-color: white;
}
input[type=checkbox]:checked:after {
    content: "";
    display: block;
    width: 10px;
    height: 15px;
    border: solid black;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    position: absolute;
    top: 2px;
    left: 6px;
}
::-webkit-scrollbar {
  width: 5px;
}
::-webkit-scrollbar-track {
  background: #f1f1f1;
}
::-webkit-scrollbar-thumb {
  background: #888;
}
::-webkit-scrollbar-thumb:hover {
  background: #555;
}
</style>
<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
</script>
</head>
<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar topbarSticky no-print">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('admin/dashboard') }}">
                     Admin Panel
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <div class="school-name text-center">
                        @if(Session::has('store_id'))
                        <?php
                            $store = DB::table('stores')->select('store_name')->where('id', Session::get('store_id'))->first();
                        ?>
                        <h3>{{ $store->store_name }}</h3>
                        @endif
                    </div>
                </div>
            </nav>
        </header>

        <?php
            $admin_permission = DB::table('admins')->where('id', Session::get('admin_id'))->first();
        ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar enableSlimScroller no-print" style="padding-bottom:60px;">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" >
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <?php
                        if(empty($admin_permission->profile_photo)) {
                            $profile_photo =  asset('/').'public/uploads/admin.jpg';
                        } else {
                            $profile_photo =  asset('public/uploads/'.$admin_permission->profile_photo);
                        }
                    ?>
                    <div class="profile-img"> <img src="{{ $profile_photo }}" alt="user" width="50" height="50" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <a href="javascript:void(0)" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{ $admin_permission->name }} <span class="caret"></span></a>
                        <div class="dropdown-menu animated flipInY">
                            <a href="{{ url('/admin/profile') }}" class="dropdown-item"><i class="ti-user"></i> Profile</a>
                            <a href="{{ url('/admin/password_change') }}" class="dropdown-item"><i class="ti-settings"></i> Change Password</a>                            <div class="dropdown-divider"></div> <a href="{{ url('/admin/logout') }}" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                        </div>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    @if($admin_permission->role == 0 && $admin_permission->is_main == 0)
                    @include('backend.sidebar_menu.sub_admin')
                    @else
                    @include('backend.sidebar_menu.admin')
                    @endif
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item-->
                <a href="{{ url('/admin/password_change') }}" class="link" data-toggle="tooltip" title="Change Password"><i class="ti-settings"></i></a>
                <?php $messageCount = Helpers::messageCount(); ?>
                <a id="live_message" href="{{ url('admin/messages') }}" class="link" data-toggle="tooltip" title="Messages"><i class="fa fa-commenting-o"></i> @if($messageCount > 0)<span class="badge badge-danger">{{ $messageCount }}</span>@endif</a>
                <!-- item-->
                <a href="{{ url('/admin/logout') }}" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
            </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div id='parentDBArea'></div>


                @yield('maincontent')

            </div>
            <div class="preloader" id="overlay" style="opacity:0.9;">
                <svg class="circular" viewBox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                </svg>
            </div>
            <footer class="footer text-center">
                Developed By <a href="http://ginilab.com/" target="_blank">Ginilab Bangladesh.</a>
            </footer>
        </div>

        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

    <!--mobile view start-->
    <div class="mobile_view">
        <div class="mobile-menu-area">
            @if($admin_permission->role == 0 && $admin_permission->is_main == 0)
            @include('backend.sidebar_menu.mobile_sub_admin')
            @else
            @include('backend.sidebar_menu.mobile_admin')
            @endif
        </div>
        <div class="sticky-area-mobile-view">
            <a href="{{ url('admin/dashboard') }}">
                <div class="mobile-home-btn text-center mobile-sticky-bar-common-style">
                    <i class="fa fa-home"></i>
                    <p>dashboard</p>
                </div>
            </a>
            <a href="{{ url('admin/pos') }}">
                <div class="mobile-user-btn text-center mobile-sticky-bar-common-style">
                    <i class="fa fa-shopping-cart"></i>
                    <p>POS</p>
                </div>
            </a>
            <a href="{{ url('admin/orders') }}">
                <div class="mobile-call-btn text-center mobile-sticky-bar-common-style">
                    <i class="fa fa-book"></i>
                    <p>Orders</p>
                </div>
            </a>
            <?php $messageCount = Helpers::messageCount(); ?>
            <a href="{{ url('admin/messages') }}">
                <div class="mobile-call-btn text-center mobile-sticky-bar-common-style">
                    <i class="fa fa-envelope"></i>
                    <p>Inbox @if($messageCount > 0)<span class="badge badge-danger">{{ $messageCount }}</span>@endif</p>
                </div>
            </a>
        </div>
    </div>
    <!--mobile view end-->

    <!--Load Dynamic Modal-->
    <div class="modal fade" id="dynamicModal" tabindex="-1" role="dialog" aria-labelledby="dynamicModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content load_modal_content"></div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $(".mobile_menu").simpleMobileMenu()
            //call dynamic modal
            $(document).on('click', '.load_modal', function(e){
                e.preventDefault();
                var action = $(this).attr('data-action');
                $.get(action, function( data ) {
                    $('#dynamicModal').modal();
                    $('#dynamicModal').on('shown.bs.modal', function(){
                        $('#dynamicModal .load_modal_content').html(data);
                    });
                    $('#dynamicModal').on('hidden.bs.modal', function(){
                        $('#dynamicModal .modal-body').data('');
                    });
                });
            });
        });
    </script>

    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('/') }}assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="{{ asset('/') }}assets/plugins/bootstrap/js/popper.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('/') }}assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('/') }}assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="{{ asset('/') }}assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('/') }}assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="{{ asset('/') }}assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('/') }}assets/plugins/echarts/echarts-all.js"></script>

    <script src="{{ asset('/') }}assets/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{ asset('/') }}assets/js/OraSchool.js" type="text/javascript"></script>
    <script src="{{ asset('/') }}assets/js/intlTelInput.min.js"></script>
    <script src="{{ asset('/') }}assets/plugins/toast-master/js/jquery.toast.js"></script>
    <script src="{{ asset('/') }}assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="{{ asset('/') }}assets/js/jquery.colorbox-min.js"></script>
    <script src="{{ asset('/') }}assets/js/moment.min.js"></script>
    <script src="{{ asset('/') }}assets/plugins/humanize-duration/humanize-duration.js"></script>

    <script type="text/javascript" src="{{ asset('/') }}assets/plugins/global-calendars/jquery.plugin.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}assets/plugins/global-calendars/jquery.calendars.all.js"></script>

    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>
        @if(Session::has('customer_id'))
        let channel_name = 'user_' + {{ Session::get('customer_id') }};
        @elseif(Session::has('admin_id') && Session::get('role') == 0)
        let channel_name = 'admin';
        @elseif(Session::has('admin_id') &&  Session::get('role') == 1)
        let channel_name = 'shop_' + {{ Session::get('store_id') }};
        @endif

        var pusher = new Pusher('b2c9e350e5a95fe56339', {
            cluster: 'ap1'
        });

        var channel = pusher.subscribe(channel_name);
        channel.bind('live-message', function(data) {
            you_message(data.message);
        });

        function you_message (obj) {
            let message = '<li class="you">'+
                '<div class="entete">'+
                '<h2>'+obj.name+'</h2> '+
                '<h3>'+obj.time+'</h3>'+
                '</div>'+
                '<div class="message">'+
                obj.message +
                '</div>'+
                '</li>';

            if($('#chat').length) {
                setTimeout(function(){
                    var d = $("#chat");
                    d.scrollTop(d[0].scrollHeight);
                }, 1);
                $('#chat').append(message);
            } else {
                let is_span = $('#live_message span').length;
                if (is_span) {
                    let len = parseFloat($('#live_message span').text());
                    $('#live_message span').text(len+1);
                } else {
                    $('#live_message').append('<span class="badge badge-danger">1</span>');
                }
                notifyMe(obj.name, obj.message, obj.photo);
            }
            new Audio('{{ asset('frontend_assets/tone') }}' + '/tone_1.wav').play();
        }

        function notifyMe(title, message, icon) {
            if (Notification.permission !== 'granted') {
                if(Notification.requestPermission()) {
                    notifier (title, message, icon);
                } else {
                    $.toast({
                        heading: title,
                        text: message,
                        icon: 'info',
                        loader: true,
                        loaderBg: '#9EC600',
                        hideAfter: 5000
                    })
                }
            } else {
                notifier (title, message, icon);
            }
        }

        function notifier (title, message, icon) {
            let notification = new Notification(title, {
                icon: icon,
                body: message,
            });
            notification.onclick = function() {
                window.location.href = "{{url('customer/messages')}}";
            };
        }
    </script>

    </body>

</html>
