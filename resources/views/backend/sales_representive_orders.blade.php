@extends('backend.layout')

@section('maincontent')
<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
    .bg-success.total-order-amount {
        color: #fff;
        padding: 15px;
        overflow: hidden;
        width: 100%;
        margin-top: 37px;
        text-align: right;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Sales Representive Order List</h4>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <div class="product-filter">
                            <?php $selected_representive = isset($_GET['sales_representive']) ? $_GET['sales_representive'] : ''; ?>
                            <form action="" method="GET">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label for="">Sales Representive</label>
                                        <select name="sales_representive" id="sales_representive_list" class="form-control" required="">
                                            <option value="">Select Sales Representive</option>
                                            @if(count($sales_representive_list) > 0)
                                                @foreach($sales_representive_list as $row)
                                                <option {{ ($selected_representive == $row->id) ? 'selected' : '' }} value="{{$row->id}}">{{ $row->name }} - {{$row->phone}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="start_date">From Date</label>
                                        <input type="date" name="start_date" id="start_date" class="form-control" value="{{isset($_GET['start_date']) ? $_GET['start_date'] : ''}}">
                                    </div>
                                    <div class="col-md-3">
                                        <label for="end_date">To Date</label>
                                        <input type="date" name="end_date" id="end_date" class="form-control" value="{{isset($_GET['end_date']) ? $_GET['end_date'] : ''}}">
                                    </div>
                                    <div class="col-md-2">
                                        <button style="margin-top: 34px" type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="responsive_table">
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Subtotal</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($sales_representive_orders) > 0)
                                <?php $i = 0; $total_order_amount = 0; ?>
                                @foreach($sales_representive_orders as $row)
                                    <?php
                                    $date = new DateTime($row->order_date);
                                    if($row->order_status == 0) {
                                        $order_status = '<span class="badge badge-primary">Pending</span>';
                                    }
                                    if($row->order_status == 1) {
                                        $order_status = '<span class="badge badge-info">Processing</span>';
                                    }
                                    if($row->order_status == 2) {
                                        $order_status = '<span class="badge badge-success">Completed</span>';
                                    }
                                    if($row->order_status == 3) {
                                        $order_status = '<span class="badge badge-danger">Canceled</span>';
                                    }
                                    $i++;
                                    $total_order_amount += $row->subtotal;
                                    ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Name">{{ $row->name }}</td>
                                        <td data-title="Phone">{{ $row->phone }}</td>
                                        <td data-title="Address">{{ $row->address }}</td>
                                        <td data-title="Status"><?php echo $order_status; ?></td>
                                        <td data-title="Subtotal">৳ {{number_format($row->subtotal, 2)}}</td>
                                        <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                        <td data-title="Action">
                                            <div class="dropdown">
                                                <a class="btn btn-info dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-list"></i>
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                    <a target="_blank" class="dropdown-item" href="{{ url('/admin/view-order/'.$row->id) }}">Order Details</a>
                                                    <a target="_blank" class="dropdown-item" href="{{ url('/admin/order-items/'.$row->id) }}">Order Items</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                    </table>
                </div>
                @if(count($sales_representive_orders) > 0)
                <div class="bg-success total-order-amount">
                    Tota Order Amount = ৳ {{number_format($total_order_amount, 2)}}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function () {
            $('#sales_representive_list').select2();

            $('#common_datatable_id').DataTable();
        });
    </script>
@endsection