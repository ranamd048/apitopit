<style>
.form-group.default {
    margin-bottom: 15px;
}
</style>
<div class="modal fade" id="add_customer_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title" id="exampleModalLongTitle">Add New Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" action="{{ url('/admin/pos/save_customer') }}" method="POST">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="modal-form">
                    <input type="hidden" name="store_id" id="store_id" value="{{$shop_id}}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group default">
                                <label for="">Name *</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                            <div class="form-group default">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email">
                            </div>
                            <div class="form-group default">
                                <label for="">Phone * (Ex: 01779221842)</label>
                                <input type="text" class="form-control" name="phone" required>
                            </div>
                            <div class="form-group default">
                                <label for="">Due Amount</label>
                                <input type="number" class="form-control" name="due_amount" value="0">
                            </div>
                            <div class="form-group default">
                            <?php $divisions = Helpers::getDivisionList(); ?>
                                <label for="">Division *</label>
                                <select name="division" id="division_list" class="form-control" required>
                                    <option value="">Select Division</option>
                                    @foreach($divisions as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group default">
                                <label for="">District *</label>
                                <select name="district" id="district_list" class="form-control" required>
                                    <option value="">Select District</option>
                                </select>
                            </div>
                            <div class="form-group default">
                                <label for="">Upazila *</label>
                                <select name="upazila" id="upazila_list" class="form-control" required>
                                    <option value="">Select Upazila</option>
                                </select>
                            </div>
                            <div class="form-group default">
                                <label for="">Address *</label>
                                <input type="text" class="form-control" name="address" required>
                            </div>
                            <div class="form-group default">
                                <label for="">Gender *</label>
                                <select name="gender" id="gender" class="form-control">
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>
                            </div>
                            <div class="form-group default">
                                <label for="">Password *</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group default">
                                <label for="">Confirm Password *</label>
                                <input type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </div>
        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
        $(document).ready(function() {            
            //get district/upazila
            function get_selected_district_upazila(id, type) {
                var url = "{{ url('get_district_upazila') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, type: type, data_format: 'html'},
                    cache: false,
                    success: function (response) {
                        $('#'+type).html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            $('#division_list').change(function() {
                var division_id = $(this).val();
                if(division_id == '') {
                    $('#district_list').html('<option value="">Select District</option>');
                } else {
                    get_selected_district_upazila(division_id, 'district_list');
                }
                $('#upazila_list').html('<option value="">Select Upazila</option>');
            });

            $('#district_list').change(function() {
                var district_id = $(this).val();
                get_selected_district_upazila(district_id, 'upazila_list');
            });
        });
    </script>