<table class="table table-bordered">
    <tbody>
        <!-- <tr>
            <td>Quantity</td>
            <td style="font-weight: 700;">{{$cart_item->count()}}</td>
            <td>Total</td>
            <td style="font-weight: 700;">
                <span id="total">{{number_format($total_amount, 2)}}</span>
            </td>
        </tr> -->
        <tr>
            <td>D/Charge                                          
                <a href="#" data-toggle="modal" data-target="#edit_delivery_charge_modal">
                    <i class="fa fa-edit"></i>
                </a>
            </td>
            <td class="text-right" style="font-weight:bold;">
                <span id="pos_delivery_charge"></span>
            </td>
            <td>Discount                                                               
                <a href="#" data-toggle="modal" data-target="#edit_discount_modal">
                  <i class="fa fa-edit"></i>
                </a>
            </td>
            <td class="text-right" style="font-weight:bold;">
                <span>({{$product_discount}})</span>
                <span id="pos_discount_amount"></span>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background: #333; color: #fff;">
                Grand total
            </td>
            <td class="text-right" style="font-weight:700; background: #333; color: #fff;" colspan="2">
                <span id="gtotal"></span>
            </td>
        </tr>
    </tbody>
</table>

<!--Update Discount Modal-->
<div class="modal fade" id="edit_discount_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Discount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="discount_type" class="col-form-label">Discount Type :</label>
            <select id="discount_type" class="form-control">
              <option value="flat">Flat</option>
              <option value="percentange">Percentange</option>
            </select>
          </div>
          <div class="form-group">
            <label for="quantity" class="col-form-label">Discount :</label>
            <input type="text" class="form-control" id="discount_amount" value="0">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="update_discount_btn_{{$unique_key}}" data-totalamount="{{$total_amount}}">Update</button>
      </div>
    </div>
  </div>
</div>
<!--Update delivery charge Modal-->
<div class="modal fade" id="edit_delivery_charge_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Update Delivery Charge</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="quantity" class="col-form-label">Amount :</label>
            <input type="number" class="form-control" id="delivery_charge" value="0">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="update_delivery_charge_btn">Update</button>
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function() {
        var unique_key = '{{$unique_key}}';
        function updateGrandTotal() {
            var total_amount = '{{$total_amount}}';
            var discount = (localStorage.getItem('pos_discount')) ? localStorage.getItem('pos_discount') : 0;
            var delivery_charge = (localStorage.getItem('pos_delivery')) ? localStorage.getItem('pos_delivery') : 0;
            var grand_total_amount = (total_amount - discount);
            grand_total_amount = (grand_total_amount + parseInt(delivery_charge));
            localStorage.setItem('grand_total', grand_total_amount);
            $('#gtotal').html('৳ '+parseFloat(grand_total_amount).toFixed(2));
            var mobile_summary = '{{$cart_item->count()}} '+ '(৳'+parseFloat(grand_total_amount).toFixed(2)+')';
            $('#mobile_show_summary').html(mobile_summary);
        }
        
        function updateDiscountAmount() {
            updateGrandTotal();
            var discount = (localStorage.getItem('pos_discount')) ? localStorage.getItem('pos_discount') : 0;
            var type = (localStorage.getItem('discount_type')) ? localStorage.getItem('discount_type') : 'flat';
            var value = (localStorage.getItem('discount_value')) ? localStorage.getItem('discount_value') : 0;
            $('#discount_amount').val(value);
            $('#discount_type').val(type);
            discount = parseFloat(discount).toFixed(2);
            $('#pos_discount_amount').html(discount);
        }
        updateDiscountAmount();

        function updateDeliveryCharge() {
            updateGrandTotal();
            var delivery = (localStorage.getItem('pos_delivery')) ? localStorage.getItem('pos_delivery') : 0;
            $('#delivery_charge').val(delivery);
            delivery = parseFloat(delivery).toFixed(2);
            $('#pos_delivery_charge').html(delivery);
        }
        updateDeliveryCharge();

        $(document).on('click', '#update_discount_btn_'+unique_key, function(e) {
            e.preventDefault();
            localStorage.removeItem('grand_total');
            var totalamount = $(this).data('totalamount');
            var discount_type = $('#discount_type').val();
            if(discount_type == 'flat') {
              var discount_amount = $('#discount_amount').val();
            }
            if(discount_type == 'percentange') {
              var discount_amount = (totalamount * $('#discount_amount').val()) / 100;
            }
            localStorage.setItem('pos_discount', discount_amount);
            localStorage.setItem('discount_type', discount_type);
            localStorage.setItem('discount_value', $('#discount_amount').val());
            updateDiscountAmount();
            $('#edit_discount_modal').modal('toggle');
            return false;
        });

        $(document).on('click', '#update_delivery_charge_btn', function(e) {
            e.preventDefault();
            var delivery_charge = $('#delivery_charge').val();
            localStorage.setItem('pos_delivery', delivery_charge);
            updateDeliveryCharge();
            $('#edit_delivery_charge_modal').modal('toggle');
            return false;
        });
        
    });
</script>