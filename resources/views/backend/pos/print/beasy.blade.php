<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Print</title>
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <style>
        .system-logo img {
            width: 160px !important;
        }
        .system-logo {overflow: hidden;margin-top: 15px;margin-bottom: 20px;}
        .system-logo h1 {
            display: inline-block;
            margin-left: 20px;
            font-size: 26px;
            text-transform: uppercase;
            font-weight: 700;
        }
        .print-section-wrapper {
            padding: 20px;
            overflow: hidden;
        }
        .system-info h1 {
            font-size: 42px;
            text-transform: uppercase;
        }
        .print-section-wrapper .header {
            overflow: hidden;
            margin-bottom: 40px;
        }
        .print-section-wrapper .header img {
            width: 100%;
            height: auto;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #333 !important;
        }
        .system-info p {
            margin-bottom: 5px;
            font-weight: 700;
        }
        .invoice-text {
            text-align: center;
            margin-top: 20px;
        }
        .invoice-text p {
            background-color: #333;
            display: inline-block;
            color: #fff;
            padding: 10px 30px;
            text-transform: uppercase;
        }
        .customer-details {
            margin-bottom: 30px;
            overflow: hidden;
        }
        .customer-details p {
            font-weight: 700;
            border-bottom: 1px dashed #333;
            display: block;
        }
        .nit-price {
            overflow: hidden;
        }
        .nit-price p {
            font-weight: 700;
            text-align: right;
            margin-bottom: 5px;
        }
        p.in-word {
            font-weight: 700;
            margin-top: 40px;
        }
        .signature-wrapper {
            overflow: hidden;
            margin-top: 40px;
        }
        .signature-wrapper p {
            font-weight: 700;
            border-top: 1px dashed #333;
            display: inline-block;
        }
        button.print_btn {
            float: right;
            top: 10px;
            cursor: pointer;
            background: #555;
            color: #fff;
            border: 0;
            padding: 3px 10px;
            left: 10px;
            position: fixed;
            text-transform: uppercase;
            z-index: 999999999;
        }
        @media print {
            body {background-color: #FFF4AA !important; -webkit-print-color-adjust: exact;}
            button.print_btn {
                display: none;
            }
        }
        @media only screen and (max-width: 767px) {
            .print-section-wrapper {
                overflow: scroll;
            }
            .row.system-info .text-right {
                text-align: left !important;
            }
            .nit-price p {
                text-align: left;
            }

        }
    </style>
</head>
<body>
    <div class="print-section-wrapper">
        <button class="print_btn" onclick="window.print()">Print</button>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <h6 class="text-center">Bismillahir Rahmanir Rahim</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="system-logo">
                                    <img src="{{ asset('public/uploads/store/'.$shop->logo) }}" alt="" class="img-fluid">
                                    <h1>{{$shop->store_name}}</h1>
                                </div>
                            </div>
                        </div>
                        <div class="row system-info">
                            <div class="col-md-4 text-left">
                            <p>TIN NO : {{$shop->tin_no}}</p>
                            </div>
                            <div class="col-md-4 text-center">
                            <p>SR NO : {{$shop->serial_no}}</p>
                            </div>
                            <div class="col-md-4 text-right">
                            <p>License NO : {{$shop->license_no}}</p>
                            </div>
                        </div>
                        <div class="row system-info">
                            <div class="col-md-6 text-left">
                                <p>Email Address : {{$shop->email}}</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p>Mobile Number : {{$shop->phone}}</p>
                                <p>Date : {{ date('d F Y h:i:a') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="customer-details">
                <div class="row">
                    <div class="col-md-6">
                        <p>Customer's Name : {{$customer->name}}</p>
                    </div>
                    <?php
                        $length = 6;
                        $char = 0;
                        $type = 'd';
                        $format = "%{$char}{$length}{$type}"; // or "$010d";
                        $code_no = sprintf($format, $customer->id);
                    ?>
                    <div class="col-md-6">
                        <p>Code No : <?php echo $code_no; ?></p>
                    </div>
                    <div class="col-md-12">
                        <p>Address : {{$customer->address}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="order-summary">
                        <table class="table table-bordered">
                            <thead class="thead-default">
                            <tr>
                                <th>Sr.</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Amount</th>
                                <th>Discount</th>
                                <th>SubTotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; $subtotal = 0; $total_qty = 0; $total_rate = 0; $_total_amount = 0; $_total_discount = 0; $_total_per_subtotal = 0; ?>
                            @foreach($selected_items as $row)
        <?php
            $i++; 
            $total_amount = ($row->quantity * $row->price);
            $per_dsicount = $row->attributes->discount ? $row->attributes->discount : 0;
            $product_discount_percent = round((($per_dsicount * $row->quantity) * 100) / $total_amount, 2);
            $per_subtotal = $total_amount - ($per_dsicount * $row->quantity);
            $subtotal += $per_subtotal;
            $total_qty += $row->quantity;           
            $total_rate += $row->price;           
            $_total_amount += $total_amount;           
            $_total_discount += ($per_dsicount * $row->quantity);           
            $_total_per_subtotal += $per_subtotal;           
        ?>
                                <tr>
                                    <th>{{$i}}</th>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->quantity}}</td>
                                    <td>৳{{number_format($row->price, 2)}}</td>
                                    <td>৳{{number_format($total_amount, 2)}}</td>
                                    <td>{{ $product_discount_percent }}% (৳{{number_format($per_dsicount * $row->quantity, 2)}})</td>
                                    <td>৳{{number_format($per_subtotal, 2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2">Total <span style="float: right">{{ $i }}</span></th>
                                    <th>{{ $total_qty}}</th>
                                    <th>৳{{ number_format($total_rate, 2)}}</th>
                                    <th>৳{{ number_format($_total_amount, 2)}}</th>
                                    <th>৳{{ number_format($_total_discount, 2)}}</th>
                                    <th>৳{{ number_format($_total_per_subtotal, 2)}}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <?php 
                    $discount_amount = isset($_GET['discount']) ? $_GET['discount'] : 0; 
                    $delivery_charge = isset($_GET['delivery']) ? $_GET['delivery'] : 0; 
                    $nit_price = (($subtotal + $delivery_charge) - $discount_amount); 
                    $order_discount_percent =  round(($discount_amount * 100) / $subtotal, 2)
                ?>
                <div class="col-md-6">
                    <p class="in-word text-capitalize">In word of Nit Price : {{ Helpers::convertNumberToWord($nit_price) }}</p>
                </div>
                
                <div class="col-md-6 nit-price">
                    <p>Total Amount of all Product = ৳{{number_format($subtotal,2 )}}</p>
                    <p class="border-bottom">Discount Amount = {{$order_discount_percent}}%  (৳{{number_format($discount_amount,2 )}})</p>
                    @if($delivery_charge > 0)
                    <p>Delivery Charge = ৳{{number_format($delivery_charge, 2)}}</p>
                    @endif
                    <p>Nit Price = ৳{{number_format($nit_price, 2)}}</p>
                </div>
            </div>
            <div class="row signature-wrapper">
                <div class="col-md-6">
                    <p>Signature of Salesman</p>
                </div>
                <div class="col-md-6 text-right">
                    <p>Signature of Customer</p>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.print();
    </script>
</body>
</html>