@if(count($products) > 0)
@foreach($products as $row)
<?php
    $check_promo = Helpers::is_promo_product($row->id);
    if($check_promo) {
        $product_price = $row->promotion_price;
    } else {
        $product_price = $row->price;
    }
?>
    <div class="col-md-6 col-12">
        <div class="list-item add_to_cart" data-id="{{$row->id}}" data-name="{{ $row->product_name }}" data-price="{{$product_price}}" data-slug="{{ $row->product_slug }}" data-image="{{ $row->product_image }}" data-shop="{{ $row->store_id }}">
            <div class="product_img">
                <img src="{{ asset('public/uploads/products/thumbnail/'.$row->product_image) }}" alt="">
            </div>
            <div class="product_info">
                <p>{{ $row->product_name }}</p>
                <p>৳ {{ $product_price }}</p>
            </div>
            <div class="spining {{$row->id}}"></div>
        </div>
    </div>
@endforeach
@else
    <div class="col-md-12 text-center">
        <h3>Product Not Found...</h3>
    </div>
@endif