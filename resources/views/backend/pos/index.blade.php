@extends('backend.layout')

@section('maincontent')
<style>
.form-group {
    margin-bottom: 0;
}
.container-fluid {
    padding: 0 !important;
}
.card {
    margin-bottom: 0;
}
.all-products {
    max-height: 505px;
    overflow-y: scroll;
    position: relative;
}
.card-no-border .left-sidebar {
    display: none;
}
.page-wrapper {
    margin-left: 0;
}
.footer {
    left: 0;
}
.product-list .list-item img {
    height: 100%;
    width: 100%;
    margin-bottom: 0px;
}
.product-list .list-item {
    margin-bottom: 15px;
    height: 100px;
    padding: 0;
}
.all-products .product-filter {
    position: fixed;
    z-index: 999;
    background: #eee;
    overflow: hidden;
    padding: 10px;
    width: 56%;
}
.product-list {
    overflow: hidden;
    margin-top: 100px;
}
.spining {
    position: absolute;
    bottom: 5px;
    right: 5px;
}
i.remove-cart-item {
    color: red;
    cursor: pointer;
}
tbody tr:nth-child(even) {background-color: #f2f7f8;}
tbody#show_selected_items i.fa.fa-spinner {
    left: 230px;
    position: absolute;
    top: 165px;
    font-size: 35px;
}
::-webkit-scrollbar {
  width: 5px;
}
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
::-webkit-scrollbar-thumb {
  background: #888; 
}
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
table.tableBodyScroll tbody {
  display: block;
  height: 180px;
  overflow-y: scroll;
}

table.tableBodyScroll thead, table.tableBodyScroll tbody tr {
  display: table;
  width: 100%;
  table-layout: fixed;
}
.page-wrapper {
    padding-bottom: 40px;
}
.card-block {
    padding: 12px;
}
.product-search.add-customer {
    background: #eee;
    padding: 5px;
}
.product-search.add-customer .form-group {
    margin-bottom: 5px;
}
.product-search.add-customer .form-group.customer {
    position: relative;
}
.product-search.add-customer .form-group.customer button {
    position: absolute;
    top: 0;
    right: 0;
    background: #028ee1;
    color: #fff;
    border: 0;
    cursor: pointer;
    padding: 7px 12px;
    border-radius: 0 3px 3px 0;
}
.search_result_show ul {
    background: #fff;
    box-shadow: 0 8px 30px #eee;
    margin: 0;
    padding: 10px 0;
    position: absolute;
    width: 100%;
    top: 0;
    z-index: 9999;
    list-style: none;
    left: 0;
}
.search_result_show ul li {
    display: block;
    border-bottom: 1px solid #ddd;
    padding: 5px 10px;
    cursor: pointer;
    transition: all 0.3s;
}
.search_result_show ul li:hover {
    background-color: #028ee1;
    color: #fff;
}
.search_result_show ul li:last-child {
    border-bottom: 0;
}
.search_result_show {
    position: relative;
}
.pos-mobile-topbar {
    display: none;
}
/* .product-list .list-item p {
    position: absolute;
    top: 40px;
    left: 0;
    width: 100%;
    text-align: center;
    padding: 5px;
    color: #fff;
    margin-bottom: 0;
    z-index: 99;
}
.product-list .list-item p::after {
    position: absolute;
    background: #000;
    width: 100%;
    height: 100%;
    content: "";
    left: 0;
    top: 0;
    opacity: 0.5;
    z-index: -1;
} */
.product-list .list-item .product_img {
    width: 40%;
    height: 100%;
    float: left;
    margin-right: 5%;
}
.product-list .list-item .product_info {
    float: left;
    width: 55%;
    padding-top: 10px;
    font-weight: 700;
}
.product-list .list-item .product_info p {
    margin-bottom: 5px;
    font-size: 13px;
}
@media only screen and (max-width: 767px) {
    .card-block {
        margin-top: 0;
    }
    .container-fluid {
        padding: 75px 15px 15px 15px !important;
    }
    .pos-mobile-topbar {
        background: #fff;
        color: #333;
        border-bottom: 2px solid #1643D0;
        position: fixed;
        width: 100%;
        display: block;
        z-index: 999;
        top: 0;
        left: 0;
        overflow: hidden;
    }
    .pos-mobile-topbar .menu {
        float: left;
        width: 50%;
        font-weight: 700;
        padding: 15px 0;
        text-align: center;
        text-transform: uppercase;
    }
    .pos-mobile-topbar .menu.active {
        background: #1643D0;
        color: #fff;
    }
    .all-products {
        max-height: 100%;
        overflow-y: unset;
    }
    .mobile-item-screen {
        display: none;
    }
    .all-products .product-filter {
        width: 100%;
        top: 56px;
        left: 0;
    }
    label {
        margin-bottom: 0;
    }
    .product-list {
        margin-top: 70px;
    }
    .product-search.add-customer {
        position: fixed;
        width: 100%;
        top: 56px;
        left: 0;
        z-index: 9999;
    }
    .product-cart-table {
        margin-top: 75px;
    }
    .table td, .table th {
        padding: 5px;
    }
    .modal {
        z-index: 99999999 !important;
    }
    .d-flex.cart-plus-minus i {
        display: block;
        width: 100%;
        font-size: 12px;
    }
    .d-flex.cart-plus-minus {
        display: unset !important;
    }
    .d-flex.cart-plus-minus span {
        display: block;
        padding: 2px 0;
    }
    .d-flex.cart-plus-minus i.fa-minus {
        margin-bottom: 10px !important;
    }
    .d-flex.cart-plus-minus i.fa-plus {
        margin-top: 10px !important;
    }
    .product-filter .col-md-4 {
        padding-left: 5px;
        padding-right: 5px;
    }
    .product-filter .col-md-4 label {
        margin-bottom: 2px;
    }
    .per_product_discount select {
        display: block;
        margin-bottom: 5px;
        width: 100%;
        margin-top: 5px;
        padding: 6px;
    }
    .per_product_discount input {
        display: block;
        padding: 5px !important;
        width: 100% !important;
    }
}
.d-flex.cart-plus-minus i {
    cursor: pointer;
    background-color: #ddd;
}
.sticky-area-mobile-view {
    z-index: 999 !important;
}
.per_product_discount input.product-discount {
    width: 28%;
    padding: 2px;
    border: 1px solid #999;
}
.per_product_discount select {
    padding: 4px;
    border: 1px solid #999;
}
</style>
<div class="row">
<input type="hidden" id="selected_customer_id" value="{{ isset($selected_customer) ? $selected_customer->id : '' }}">
<input type="hidden" id="old_order_id" value="{{ isset($old_order_id) ? $old_order_id : 0 }}">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pos-mobile-topbar">
                            <div class="menu active show_products">
                                Products
                            </div>
                            <div class="menu show_items">
                                Items <span id="mobile_show_summary"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 mobile-item-screen">
                        <div class="product-search add-customer">
                            <div class="form-group customer">
                                <input type="text" class="form-control" id="search_customer" placeholder="Search Customer..." autocomplete="off" @if(isset($selected_customer)) value="{{ $selected_customer->name }} ({{ $selected_customer->phone }})" @endif>
                                <button data-toggle="modal" data-target="#add_customer_modal"><i class="fa fa-plus"></i> Add Customer</button>
                                <div class="search_result_show" id="customer_search_result"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="search_product" placeholder="Search Product...">
                                <div class="search_result_show" id="product_search_result"></div>
                            </div>
                        </div>
                        <div class="product-cart-table">
                            <table class="table table-bordered table-hover tableBodyScroll">
                                <thead class="bg-success">
                                    <tr>
                                        <th width="45%">Product</th>
                                        <th width="25%">Qty</th>
                                        <th width="20%">Subtotal</th>
                                        <th width="10%"><i class="fa fa-trash"></i></th>
                                    </tr>
                                </thead>
                                <tbody id="show_selected_items"></tbody>
                            </table>
                        </div>
                        <div class="summary-view">
                            <div id="show_pos_summary"></div>
                            <div class="row">
                                <div class="col-md-4 col-4">
                                    <a href="#" class="btn btn-danger btn-block cancel-pos-order">Cancel</a>
                                </div>
                                <div class="col-md-4 col-4">
                                    <button type="button" class="btn btn-primary btn-block" id="place_order_btn">Place Order</button>
                                </div>
                                <div class="col-md-4 col-4">
                                    <button type="button" class="btn btn-info btn-block" id="print_only">Print Only</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 all-products mobile-product-screen">
                        <div class="product-filter">
                            <input type="hidden" id="file_name" value="pos.product_list">
                            <input type="hidden" id="current_shop_id" value="{{$shop_id}}">
                            <div class="form-group row">
                                <?php $col_num = 4; ?>
                                @if(!Session::has('store_id'))
                                <div class="col-md-4 col-4">
                                    <label for="">Shop</label>
                                    <select name="shop_id" id="shop_list" class="form-control change_store_id">
                                        @foreach($shop_list as $shop)
                                        <option value="{{$shop->id}}">{{$shop->store_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <?php $col_num = 4; ?>
                                @endif
                                <div class="col-md-{{$col_num}} col-{{$col_num}}">
                                    <label for="">Category</label>
                                    <select name="category_id" id="category_list" class="form-control">
                                        <option value="">Select Shop</option>
                                    </select>
                                </div>
                                <div class="col-md-{{$col_num}} col-{{$col_num}}">
                                    <label for="">Sub Category</label>
                                    <select name="sub_category_id" id="sub_category_list" class="form-control">
                                        <option value="">Select Sub Category</option>
                                    </select>
                                </div>
                                @if(Session::has('store_id'))
                                <div class="col-md-{{$col_num}} col-{{$col_num}}">
                                    <label for="">Search</label>
                                    <input type="text" class="form-control" id="search_query_string">
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="product-list">
                            <div class="row" id="load_all_products"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('backend.pos.add_customer')


<!-- Payment Modal-->
<div class="modal fade in" data-backdrop="static" data-keyboard="false"  id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">POS Sale Payment</h5>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Amount <span style="font-size: 12px;">(If sale without payment just enter 0)</span></label>
                            <input type="text" class="form-control" id="payment_amount">
                        </div>
                        <div class="form-group">
                            <label for="">Note</label>
                            <textarea name="" class="form-control" id="payment_note"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="submit_pos_payment">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Payment Modal-->


<script src="{{ asset('frontend_assets/admin/js/product.js') }}"></script>

@if(isset($selected_customer))
<script>
    var pos_delivery = '{{ $selected_customer->delivery_charge }}';
    localStorage.setItem('pos_delivery', pos_delivery);
</script>
@endif

<script>
    $(document).ready(function() {
        //mobile tab
        $(document).on('click', '.show_products', function() {
            $('.mobile-product-screen').fadeIn();
            $('.mobile-item-screen').fadeOut();
            $(this).addClass('active');
            $('.show_items').removeClass('active');
        });
        $(document).on('click', '.show_items', function() {
            $('.mobile-item-screen').fadeIn();
            $('.mobile-product-screen').fadeOut();
            $(this).addClass('active');
            $('.show_products').removeClass('active');
        });
        //selected shop
        var shop_id = $('#current_shop_id').val();
        load_products(shop_id);
        get_shop_categories(shop_id);

        //search product
        $(document).on('keyup', '#search_product', function() {
            var shop_id = $('#current_shop_id').val();
            var value = $(this).val();
            var url = APP_URL + '/admin/pos/search_products';
            var loading = '<div class="col-md-12 text-center"><ul><i class="fa fa-spinner fa-spin"></i></ul></div>';
            if($.trim(value) == '') {
                $('#product_search_result').html('');
            } else {
                $('#product_search_result').html(loading);
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: {shop_id: shop_id, value: value },
                    cache: false,
                    success: function(response) {
                        $('#product_search_result').html(response);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            }
        });
        //search customer
        $(document).on('keyup', '#search_customer', function() {
            var shop_id = $('#current_shop_id').val();
            var value = $(this).val();
            var url = APP_URL + '/admin/pos/search_customer';
            var loading = '<div class="col-md-12 text-center"><ul><i class="fa fa-spinner fa-spin"></i></ul></div>';
            if($.trim(value) == '') {
                $('#customer_search_result').html('');
            } else {
                $('#customer_search_result').html(loading);
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: {shop_id: shop_id, value: value},
                    cache: false,
                    success: function(response) {
                        $('#customer_search_result').html(response);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            }
        });

        //select customer
        $(document).on('click', '.add_to_customer', function() {
            var id = $(this).data('id');
            var name = $(this).text();
            $('#customer_search_result').html('');
            $('#search_customer').val(name);
            $('#selected_customer_id').val(id);
        });
        
        //load cart summary
        function load_pos_summary() {
            var url = APP_URL + '/admin/pos/pos_summary';
            $.ajax({
                method: "GET",
                url: url,
                success: function(response) {
                    $('#show_pos_summary').html(response);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
        //show selected items
        function show_selected_items() {
            var url = APP_URL + '/admin/pos/show_selected_items';
            $('#show_selected_items').html('<i class="fa fa-spinner fa-spin"></i>');
            $.ajax({
                method: "GET",
                url: url,
                success: function(response) {
                    $('#show_selected_items').html(response);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
        show_selected_items();
        load_pos_summary();

        //change store id
        $(document).on('change', '.change_store_id', function() {
            var id = $(this).val();
            $('#store_id').val(id);
            localStorage.removeItem('pos_discount');
            localStorage.removeItem('pos_delivery');
            localStorage.removeItem('discount_type');
            localStorage.removeItem('discount_value');
            load_pos_summary();
        });

        //add to cart
        $(document).on('click', '.add_to_cart', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var price = $(this).data('price');
            var name = $(this).data('name');
            var slug = $(this).data('slug');
            var image = $(this).data('image');
            var shop = $(this).data('shop');
            var url = APP_URL + '/admin/pos/add_product';
            $('.spining'+'.'+id).html('<i class="fa fa-spinner fa-spin"></i>');
            $('#product_search_result').html('');
            $('#search_product').val('');
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { id: id, name: name, price: price, quantity: 1, slug: slug, image: image, shop: shop },
                cache: false,
                context: this,
                success: function(response) {
                    $('.spining'+'.'+id).html('');
                    if (response == 'DONE') {
                        show_selected_items();
                        load_pos_summary();
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
        //remove cart item
        $(document).on('click', '.remove-cart-item', function() {
            var id = $(this).data('rowid');
            var url = APP_URL + '/admin/pos/remove_cart_item/' + id;
            $.ajax({
                method: "GET",
                url: url,
                success: function(response) {
                    if (response == 'DONE') {
                        $('#seleted_item_id_' + id).remove();
                        load_pos_summary();
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });

        //update cart quantity
        function update_cart_quantity(id, action, quantity = '') {
            var url = APP_URL + '/admin/pos/update_quantity';
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { id: id, action: action, quantity: quantity },
                cache: false,
                success: function(response) {
                    if (response == 'DONE') {
                        show_selected_items();
                        load_pos_summary();
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
        //cart quantity plus
        $(document).on('click', '.plus-quantity', function() {
            var id = $(this).data('id');
            update_cart_quantity(id, 'plus');

        });
        //cart quantity minus
        $(document).on('click', '.minus-quantity', function() {
            var id = $(this).data('id');
            var quantity = $('#qty_value_' + id).text();
            if (parseInt(quantity) == 1) {
                alert('You can not update quantity less than 1');
                return false;
            } else {
                update_cart_quantity(id, 'minus');
            }
        });
        //change cart quantity
        $(document).on('change', '.change_quantity', function() {
            var id = $(this).data('id');
            var quantity = $(this).val();
            update_cart_quantity(id, 'input', quantity);
        });

        //product discount
        $(document).on('change', '.product-discount', function() {
            var id = $(this).data('id');
            var price = $(this).data('price');
            var discount = $(this).val();
            var discount_type = $('#product_discount_type_'+id).val();
            var url = APP_URL + '/admin/pos/product_discount';
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { id: id, price: price, discount: discount, discount_type: discount_type },
                cache: false,
                success: function(response) {
                    if (response == 'DONE') {
                        show_selected_items();
                        load_pos_summary();
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });

        //place order
        $(document).on('click', '#place_order_btn', function(e) {
            e.preventDefault();
            var customer_id = $('#selected_customer_id').val();
            var grand_total = (localStorage.getItem('grand_total')) ? localStorage.getItem('grand_total') : 0;
            if(customer_id == '') {
                alert('Please Select a Customer');
                return false;
            }
            if(grand_total == 0) {
                alert('Please add some Product');
                return false;
            }
            $('#payment_amount').val(grand_total);
            $('#paymentModal').appendTo("body").modal('show');
        });

        $(document).on('click', '#submit_pos_payment', function(e) {
            e.preventDefault();
            var payment_amount = $('#payment_amount').val();
            var payment_note = $('#payment_note').val();
            var customer_id = $('#selected_customer_id').val();
            var old_order_id = $('#old_order_id').val();
            var discount = (localStorage.getItem('pos_discount')) ? localStorage.getItem('pos_discount') : 0;
            var delivery_cost = (localStorage.getItem('pos_delivery')) ? localStorage.getItem('pos_delivery') : 0;
            var grand_total = (localStorage.getItem('grand_total')) ? localStorage.getItem('grand_total') : 0;
            
            var url = APP_URL + '/admin/pos/place_order';
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { customer_id: customer_id, old_order_id: old_order_id, discount: discount, delivery_charge: delivery_cost, grand_total: grand_total, payment_amount: payment_amount, payment_note: payment_note},
                cache: false,
                success: function(response) {
                    $('#selected_customer_id').val('');
                    localStorage.removeItem('pos_discount');
                    localStorage.removeItem('pos_delivery');
                    localStorage.removeItem('grand_total');
                    localStorage.removeItem('discount_type');
                    localStorage.removeItem('discount_value');
                    if(response == 'DONE') {
                        alert('Order Place Successfully');
                        location.reload();
                    } else {
                        window.location.href = response;
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });

        //print only
        $(document).on('click', '#print_only', function(e) {
            e.preventDefault();
            var customer_id = $('#selected_customer_id').val();
            var grand_total = (localStorage.getItem('grand_total')) ? localStorage.getItem('grand_total') : 0;
            var pos_discount = (localStorage.getItem('pos_discount')) ? localStorage.getItem('pos_discount') : 0;
            var pos_delivery = (localStorage.getItem('pos_delivery')) ? localStorage.getItem('pos_delivery') : 0;
            if(customer_id == '') {
                alert('Please Select a Customer');
                return false;
            }
            if(grand_total == 0) {
                alert('Please add some Product');
                return false;
            }
            window.location.href = "{{ url('admin/pos/print_only') }}"+"?customer="+customer_id+"&discount="+pos_discount+'&delivery='+pos_delivery;
        });

        $(document).on('click', '.cancel-pos-order', function(e) {
            e.preventDefault();
            
            if(confirm('Are you sure want to cancel?')) {
                var url = APP_URL + '/admin/pos/cancel';
                $.ajax({
                    method: "GET",
                    url: url,
                    success: function(response) {
                        if (response == 'DONE') {
                            localStorage.removeItem('pos_discount');
                            localStorage.removeItem('pos_delivery');
                            localStorage.removeItem('grand_total');
                            localStorage.removeItem('discount_type');
                            localStorage.removeItem('discount_value');
                            location.reload();
                        }
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            }
        });
        
    })
</script>
@endsection