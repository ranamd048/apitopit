@if(count($selected_items) > 0)
    @foreach($selected_items as $row)
        <?php
        if($row->attributes->discount == '') {
            $price = $row->price;
        } else {
            $price = ($row->price - $row->attributes->discount);
        }
        $total_price = ($price * $row->quantity);

        if($row->attributes->discount_type == 'percent') {
            $discount_value = ($row->attributes->discount * 100) / $row->price;
        } else {
            $discount_value = $row->attributes->discount;
        }
        ?>
        <tr id="seleted_item_id_{{$row->id}}">
            <td width="45%">
                {{$row->name}} <br> <span style="font-size: 12px" class="per_product_discount">৳{{number_format($row->price, 2)}}
                <select id="product_discount_type_{{$row->id}}">
                    <option @if($row->attributes->discount_type == '' || $row->attributes->discount_type == 'flat') selected="" @endif value="flat">Flat</option>
                    <option @if($row->attributes->discount_type == 'percent') selected="" @endif value="percent">Percent</option>
                </select>
                <input type="text" class="m-auto text-center product-discount" value="{{ $discount_value }}" data-id="{{$row->id}}" data-price="{{ $row->price }}" placeholder="Discount">
                </span>
            </td>
            <td width="25%">
                <div class="c-qty d-flex mt-2 cart-plus-minus text-center">
                    <i class="fa fa-minus m-auto bg-light p-2 minus-quantity" data-id="{{$row->id}}"></i>
                    {{-- <span class="m-auto" id="qty_value_{{$row->id}}">{{$row->quantity}}</span> --}}
                    <input type="text" class="m-auto form-control text-center change_quantity" data-id="{{$row->id}}" id="qty_value_{{$row->id}}" value="{{$row->quantity}}">
                    <i class="fa fa-plus m-auto bg-light p-2 plus-quantity" data-id="{{$row->id}}"s></i>
                </div>
            </td>
            <td width="20%" style="padding-top: 25px">{{number_format($total_price, 2)}}</td>
            <td width="10%" style="padding-top: 25px"><i class="fa fa-times remove-cart-item" data-rowid="{{$row->id}}"></i></td>
        </tr>
    @endforeach
@endif