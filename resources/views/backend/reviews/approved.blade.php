@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Approved Reviews</h4>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Reviewer Name</th>
                                <th>Rating</th>
                                <th>Comment</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($product_reviews) > 0)
                                <?php $i = 0; ?>
                                @foreach($product_reviews as $row)
                                    <?php $i++; $date = new DateTime($row->created_at); ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Product"><a target="_blank" href="{{url('product/'.$row->product_id.'/'.$row->product_slug)}}">{{ $row->product_name }}</a></td>
                                        <td data-title="Reviewer Name">{{ $row->name }}</td>
                                        <td data-title="Rating">{{ $row->rating }}</td>
                                        <td data-title="Comment">{{ $row->comment }}</td>
                                        <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable();
        } );
    </script>
@endsection