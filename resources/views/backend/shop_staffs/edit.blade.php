<form class="form-horizontal" action="{{ url('admin/edit-shop-staff') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Shop Staff</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
      <div class="modal-body">
            <input type="hidden" name="staff_id" value="{{ $result->id }}">
            <input type="hidden" name="existing_photo" value="{{ $result->profile_picture }}">
            <div class="form-group">
                <label for="name" class="col-form-label">Name *</label>
                <input type="text" name="name" class="form-control" required="" value="{{ $result->name }}">
            </div>
            <div class="form-group">
                <label for="name" class="col-form-label">Contact No *</label>
                <input type="text" name="contact" class="form-control" required="" value="{{ $result->contact }}">
            </div>
            <div class="form-group">
                <label for="name" class="col-form-label">Address *</label>
                <input type="text" name="address" class="form-control" required="" value="{{ $result->address }}">
            </div>
            <div class="form-group">
                <label for="name" class="col-form-label">Profile Photo</label>
                @if(!empty($result->profile_picture))
                <br>
                <img src="{{ asset('public/uploads/'.$result->profile_picture) }}" alt="" width="100"><br>
                @endif
                <input type="file" name="profile_picture" class="form-control">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>