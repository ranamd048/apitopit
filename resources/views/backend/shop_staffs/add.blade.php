<form class="form-horizontal" action="{{ url('admin/add-shop-staff') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Shop Staff</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
      <div class="modal-body">
            <div class="form-group">
                <label for="name" class="col-form-label">Name *</label>
                <input type="text" name="name" class="form-control" required="">
            </div>
            <div class="form-group">
                <label for="name" class="col-form-label">Contact No *</label>
                <input type="text" name="contact" class="form-control" required="">
            </div>
            <div class="form-group">
                <label for="name" class="col-form-label">Address *</label>
                <input type="text" name="address" class="form-control" required="">
            </div>
            <div class="form-group">
                <label for="name" class="col-form-label">Profile Photo</label>
                <input type="file" name="profile_picture" class="form-control">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>