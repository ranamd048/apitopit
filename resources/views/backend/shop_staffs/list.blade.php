@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Shop Staff List</h4>
                    <div class="back-button">
                        <a href="#" class="btn btn-primary text-capitalize load_modal" data-toggle="modal" data-action="{{ url('admin/add-shop-staff') }}"><i class="fa fa-plus"></i>Add New</a>
                        <a href="{{ url('admin/shop-staff-attendance') }}" class="btn btn-primary"><i class="fa fa-calendar"></i> Attendance</a>
                        <a href="{{ url('admin/shop-staff-payments') }}" class="btn btn-primary"><i class="fa fa-money"></i> Payments</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        @if(Session::has('error_message'))
                            <div class="alert alert-danger">
                                {{ Session::get('error_message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Contact</th>
                                <th>Photo</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($result) > 0)
                                <?php $i = 0; ?>
                                @foreach($result as $row)
                                    <?php $i++; ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Name">{{ $row->name }}</td>
                                        <td data-title="Contact">{{ $row->contact }}</td>
                                        <td data-title="Photo"><img src="{{ asset('public/uploads/'.$row->profile_picture) }}" alt="" width="100"></td>
                                        <td data-title="Address">{{ $row->address }}</td>
                                        <td data-title="Action">
                                            <a href="#" class="btn btn-success load_modal" data-toggle="modal" data-action="{{ url('/admin/edit-shop-staff/'.$row->id) }}"><i class="fa fa-edit"></i></a>
                                            <a href="#" class="btn btn-danger load_modal" data-toggle="modal" data-action="{{ url('/admin/delete-shop-staff/'.$row->id) }}"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable();
        } );
    </script>
@endsection