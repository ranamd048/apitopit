<form class="form-horizontal" action="{{ url('admin/delete-shop-staff') }}" method="POST">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Shop Staff ({{ $result->name }})</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
      <div class="modal-body">
            <input type="hidden" name="staff_id" value="{{ $result->id }}">
            <div class="form-group">
                <label class="col-form-label">Enter Password *</label>
                <input type="password" name="password" class="form-control" required="">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>