<form class="form-horizontal" action="{{ url('admin/add-staff-payment') }}" method="POST">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
      <div class="modal-body">
            <div class="form-group">
                <label class="col-form-label">Staff *</label>
                <select name="staff_id" class="form-control" required="">
                    <option value="">Select Staff</option>
                    @if(count($staff_list) > 0)
                        @foreach ($staff_list as $staff)
                            <option value="{{ $staff->id }}">{{ $staff->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="amount" class="col-form-label">Amount *</label>
                <input type="text" name="amount" class="form-control" required="">
            </div>
            <div class="form-group">
                <label for="note" class="col-form-label">Note</label>
                <input type="text" name="note" class="form-control">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>