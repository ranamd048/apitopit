<form class="form-horizontal" action="{{ url('admin/take-staff-attendance') }}" method="POST">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Take Staff Attendance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
      <div class="modal-body">
        @if(count($result) > 0)
            <div class="form-group">
                <label for="attendance_date">Attendance Date *</label>
                <input type="date" id="attendance_date" name="attendance_date" class="form-control" required="">
            </div>
            @foreach($result as $row)
            <div class="row">
                <input type="hidden" name="staffs[]" value="{{ $row->id }}">
                <div class="col-md-3">
                    <div class="form-group text-right" style="margin-top: 5px"><strong>{{ $row->name }}</strong></div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <select name="status[{{ $row->id }}]" id="" class="form-control" required="">
                            <option value="">Status</option>
                            <option value="present">Present</option>
                            <option value="absent">Absent</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name="in_time[{{ $row->id }}]" class="form-control" placeholder="In Time">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name="out_time[{{ $row->id }}]" class="form-control" placeholder="Out Time">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" name="note[{{ $row->id }}]" class="form-control" placeholder="Note">
                    </div>
                </div>
            </div>
            @endforeach
        @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>