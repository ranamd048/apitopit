<form class="form-horizontal" action="{{ url('admin/edit-staff-attendance') }}" method="POST">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Staff Attendance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
      <div class="modal-body">
            <input type="hidden" name="attendance_id" value="{{ $result->id }}">
            <input type="hidden" name="staff_id" value="{{ $result->staff_id }}">
            <div class="form-group">
                <label for="attendance_date">Attendance Date *</label>
                <input type="date" id="attendance_date" name="attendance_date" class="form-control" value="{{ $result->attendance_date }}" required="">
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-right" style="margin-top: 5px"><strong>{{ $result->name }}</strong></div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <select name="status" class="form-control" required="">
                            <option value="">Status</option>
                            <option @if($result->status == 'present') selected="" @endif value="present">Present</option>
                            <option @if($result->status == 'absent') selected="" @endif value="absent">Absent</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name="in_time" class="form-control" value="{{ $result->in_time }}" placeholder="In Time">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" name="out_time" value="{{ $result->out_time }}" class="form-control" placeholder="Out Time">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" name="note" class="form-control" value="{{ $result->note }}" placeholder="Note">
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>