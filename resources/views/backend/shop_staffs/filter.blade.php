<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
    .product-filter button, .product-filter a {
        margin-top: 32px;
    }
</style>
<?php
    $from_date = isset($_GET['from_date']) ? $_GET['from_date'] : '';
    $to_date = isset($_GET['to_date']) ? $_GET['to_date'] : '';
    $staff_id = isset($_GET['staff_id']) ? $_GET['staff_id'] : '';
?>
<div class="row">
    <div class="col-md-12">
        <form action="" method="GET">
            <div class="product-filter">
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>From Date *</label>
                        <input type="date" name="from_date" required="" value="{{ $from_date }}" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label>To Date *</label>
                        <input type="date" name="to_date" required="" value="{{ $to_date }}" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label>Staff *</label>
                        <select name="staff_id" class="form-control" id="staff_list" required="">
                            <option value="">Select Staff</option>
                            @if(count($staff_list) > 0)
                                @foreach ($staff_list as $staff)
                                    <option @if($staff_id == $staff->id) selected="" @endif value="{{ $staff->id }}">{{ $staff->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Filter</button>
                        <a href="{{ $reset_link }}" class="btn btn-danger"><i class="fa fa-refresh"></i> Reset</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#staff_list').select2();
    });
</script>