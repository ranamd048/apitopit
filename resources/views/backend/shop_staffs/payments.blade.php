@extends('backend.layout')

@section('maincontent')    
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Staff Payments Report</h4>
                    <div class="back-button">
                        <a href="#" class="btn btn-primary load_modal" data-toggle="modal" data-action="{{ url('admin/add-staff-payment') }}"><i class="fa fa-plus"></i>Add Payment</a>
                        <a href="{{ url('admin/shop-staffs') }}" class="btn btn-primary"><i class="fa fa-users"></i>Staff List</a>
                    </div>
                </div>
                <div class="card-block">
                    @include('backend.shop_staffs.filter', ['reset_link' => url('admin/shop-staff-payments')])
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover" id="common_datatable_id">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Amount</th>
                                <th>Note</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_amount = 0; ?>
                            @if(count($payments) > 0)
                                <?php $i = 0; ?>
                                @foreach($payments as $row)
                                    <?php $i++; $payment_date = new DateTime($row->created_at); ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Name">{{ $row->name }}</td>
                                        <td data-title="Amount">{{ number_format($row->amount, 2) }}</td>
                                        <td data-title="Note">{{ $row->note }}</td>
                                        <td data-title="Date">{{ $payment_date->format('d-M-Y') }}</td>
                                        <td data-title="Action">
                                            <a href="#" class="btn btn-success load_modal" data-toggle="modal" data-action="{{ url('admin/edit-staff-payment/'.$row->id) }}"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                    <?php $total_amount += $row->amount; ?>
                                @endforeach
                            @endif
                            </tbody>
                            @if($total_amount > 0)
                            <tfoot>
                                <tr>
                                    <td colspan="2"><strong>Total Amount</strong></td>
                                    <td colspan="4"><strong>{{ number_format($total_amount, 2) }}</strong></td>
                                </tr>
                            </tfoot>
                            @endif
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#common_datatable_id').DataTable();
        });
    </script>
@endsection