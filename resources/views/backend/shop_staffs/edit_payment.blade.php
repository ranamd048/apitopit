<form class="form-horizontal" action="{{ url('admin/edit-staff-payment') }}" method="POST">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
      <div class="modal-body">
            <input type="hidden" name="payment_id" value="{{ $result->id }}">
            <input type="hidden" name="staff_id" value="{{ $result->staff_id }}">
            <div class="form-group">
                <label class="col-form-label">Staff *</label>
                <select class="form-control" disabled="">
                    <option value="{{ $result->staff_id }}">{{ $result->name }}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="amount" class="col-form-label">Amount *</label>
                <input type="text" name="amount" class="form-control" value="{{ $result->amount }}" required="">
            </div>
            <div class="form-group">
                <label for="note" class="col-form-label">Note</label>
                <input type="text" name="note" class="form-control" value="{{ $result->note }}">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>