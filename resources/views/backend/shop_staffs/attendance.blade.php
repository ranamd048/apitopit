@extends('backend.layout')

@section('maincontent')
    <style>
        .modal-dialog {
            max-width: 800px;
        }
    </style>
    
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Staff Attendance Report</h4>
                    <div class="back-button">
                        <a href="#" class="btn btn-primary load_modal" data-toggle="modal" data-action="{{ url('admin/take-staff-attendance') }}"><i class="fa fa-plus"></i>Take Attendance</a>
                        <a href="{{ url('admin/shop-staffs') }}" class="btn btn-primary"><i class="fa fa-users"></i>Staff List</a>
                    </div>
                </div>
                <div class="card-block">
                    @include('backend.shop_staffs.filter', ['reset_link' => url('admin/shop-staff-attendance')])
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>In Time</th>
                                <th>Out Time</th>
                                <th>Note</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($result) > 0)
                                <?php $i = 0; ?>
                                @foreach($result as $row)
                                    <?php $i++; $attendance_date = new DateTime($row->attendance_date); ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Name">{{ $row->name }}</td>
                                        <td data-title="Status">{{ ucwords($row->status) }}</td>
                                        <td data-title="In Time">{{ $row->in_time }}</td>
                                        <td data-title="Out Time">{{ $row->out_time }}</td>
                                        <td data-title="Note">{{ $row->note }}</td>
                                        <td data-title="Date">{{ $attendance_date->format('d-M-Y') }}</td>
                                        <td data-title="Action">
                                            <a href="#" class="btn btn-success load_modal" data-toggle="modal" data-action="{{ url('admin/edit-staff-attendance/'.$row->id) }}"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection