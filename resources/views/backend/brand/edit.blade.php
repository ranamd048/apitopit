@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Edit Brand</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/brand-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    <form class="form-horizontal" action="{{ url('/admin/update_brand') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="brand_id" value="{{$brand->id}}">                      
                        <input type="hidden" name="existing_img" value="{{$brand->image}}">                      
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Brand Name * </label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" required="" value="{{$brand->name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Brand Image</label>
                            <div class="col-sm-10">
                                @if($brand->image)
                                <img src="{{ asset('public/uploads/brands/'.$brand->image) }}" width="120" alt=""><br><br>
                                @endif
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Sort Order</label>
                            <div class="col-sm-10">
                                <input type="number" name="sort_order" class="form-control" value="{{$brand->sort_order}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option @if($brand->status == 1) selected="" @endif value="1">Active</option>
                                    <option @if($brand->status == 0) selected="" @endif value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection