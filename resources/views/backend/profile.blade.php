@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Update Profile</h4>
            </div>
            <div class="card-block">
                <div class="form">

                    <form class="form-horizontal" action="{{ url('/admin/update_profile') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="existing_profile" value="{{$result->profile_photo}}">
                        <div class="row">
                            <div class="col-md-6">
                                @if(Session::get('success'))
                                    <p class="alert alert-success">{{ Session::get('success') }}</p>
                                @endif
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Name</label>
                                        <input type="text" name="name" class="form-control" value="{{ $result->name }}" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Email</label>
                                        <input type="text" name="email" class="form-control" value="{{ $result->email }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Phone</label>
                                        <input type="text" readonly class="form-control" value="{{ $result->phone }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Upload Profile Photo</label>
                                        <input type="file" name="profile_photo" class="form-control">
                                    </div>
                                </div>

                                <div lass="form-group row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-info">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    	
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection