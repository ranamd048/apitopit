@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Edit Unit</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/unit-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">

                    <form class="form-horizontal" action="{{ url('/admin/update_unit') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="unit_id" value="{{ $unit->id }}">
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Unit Name * </label>
                            <div class="col-sm-10">
                                <input type="text" name="unit_name" class="form-control" required="" value="{{ $unit->unit_name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Unit Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="unit_status">
                                    <option @if($unit->unit_status == 1) selected="" @endif value="1">Active</option>
                                    <option @if($unit->unit_status == 0) selected="" @endif value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Update</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection