@extends('backend.layout')

@section('maincontent')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Unit List</h4>
                <div class="back-button">
                    <a href="{{ url('admin/create-unit') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Unit</a>
                </div>
            </div>
            <div class="card-block">
                <div id="responsive_table">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <table class="table table-bordered table-hover" id="common_datatable_id">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($unit_list) > 0)
                            
                            <?php $i = 0; ?>
                            @foreach($unit_list as $row)
                            <?php $i++; ?>
                            <tr>
                                <td data-title="Sl">{{ $i }}</td>
                                <td data-title="Name">{{ $row->unit_name }}</td>
                                <td data-title="Status">{{ ($row->unit_status == 1) ? 'Active' : 'Inactive' }}</td>
                                <td data-title="Action">
                                    <a class="btn btn-success" href="{{ url('/admin/edit-unit/'.$row->id) }}"><i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-danger" href="{{ url('/admin/delete-unit/'.$row->id) }}"><i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#common_datatable_id').DataTable();
    } );
</script>
@endsection