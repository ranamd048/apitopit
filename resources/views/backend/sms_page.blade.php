@extends('backend.layout')

@section('maincontent')
<style>
    nav.custom-tab a {
        color: #222;
        font-weight: 400;
    }
    .custom-tab .nav-link.active {
        background-color: #eeeeee;
    }
    .custom-tab-content {
        padding: 10px 30px;
    }
    .select2-container {
        width: 100% !important;
    }
    .custom-tab-content .tab-pane.customactive {
        display: block !important;
    }
    .custom-tab-content .tab-pane.active {
        display: none;
    }
    .card-block.custom {
        margin-top: 0;
    }
    .custom h4.card-title {
        float: unset !important;
        color: #333;
        margin: 0;
        text-align: center !important;
        text-transform: uppercase;
        font-weight: 700;
    }
    a.summary-link {
        display: block;
        padding: 30px 0;
        text-align: center;
        border-radius: 5px;
        margin-bottom: 30px;
    }
    a.summary-link h3 {
        color: #fff;
        text-transform: uppercase;
        font-size: 15px;
        margin-bottom: 0;
    }
</style>

<link rel="stylesheet" href="{{ asset('assets/css/vanillaSelectBox.css') }}">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">SMS Page</h4>
            </div>
            <div class="card-block">
                @if(Session::has('error'))
                <p class="alert alert-danger">{{ Session::get('error') }}</p>
                @endif
                @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
                @endif
                <nav class="custom-tab">
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" data-nav="nav-due" id="nav-due-tab" data-toggle="tab" href="#nav-due" role="tab" aria-controls="nav-due" aria-selected="true">Send Due SMS</a>
                        <a class="nav-item nav-link" data-nav="nav-offer" id="nav-offer-tab" data-toggle="tab" href="#nav-offer" role="tab" aria-controls="nav-offer" aria-selected="false">Send Offer SMS</a>
                    </div>
                </nav>
                <div class="tab-content custom-tab-content" id="nav-tabContent">
                    <div class="tab-pane show customactive" id="nav-due" role="tabpanel" aria-labelledby="nav-due-tab">
                        <div class="tab-content1">
                            @if(count($due_customer_list) > 0)
                                <form action="{{ url('admin/send_due_sms_new') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="">Select All Due Customer (All/Manual):</label>
                                        <select id="select_due_customer" class="form-control" multiple="" name="due_customers[]">
                                            @foreach($due_customer_list as $due_customer)
                                            <option value="{{$due_customer->phone}}#{{ $due_customer->total_due_amount }}" selected="true">{{$due_customer->name}} - ৳{{ number_format($due_customer->total_due_amount, 2) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Due SMS Text: <span style="font-size: 13px;">(SMS Text + Due Amount + Some Text) (For due amount write ##. Example: Test Message ## Other Text)</span></label>
                                        <textarea name="due_text" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-primary">SEND DUE SMS</button>
                                    </div>
                                </form>
                            @else
                            <h4>Due Customer Not Found to Sent SMS</h4>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane" id="nav-offer" role="tabpanel" aria-labelledby="nav-offer-tab">
                        <div class="tab-content2">
                            @if(count($shop_customers) > 0)
                            <?php 
                                $products = DB::table('products')->select('id', 'product_name')->where('store_id', Session::get('store_id'))->get();
                            ?>
                                <form action="{{ url('admin/send_offer_sms_new') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="">Select All Customer (All/Manual):</label>
                                        <select id="select_offer_customer" class="form-control" multiple="" name="offer_customers[]">
                                            @foreach($shop_customers as $row)
                                            <option value="{{$row->phone}}" selected="true">{{$row->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Select Product:</label>
                                                <select class="form-control" id="product_list" name="product_name" required="">
                                                    <option value="">Select Offer Product</option>
                                                    @foreach($products as $row)
                                                        <option value="{{ $row->product_name }}">{{ $row->product_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Offer Price:</label>
                                                <input type="text" name="offer_price" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Offer SMS Text: <span style="font-size: 13px;">(After sms text will show product name and price)</span></label>
                                        <textarea name="offer_text" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-primary">SEND OFFER SMS</button>
                                    </div>
                                </form>
                            @else
                            <h4>Customer Not Found to Sent SMS</h4>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header custom">
                                <h4 class="card-title">SMS Report</h4>  
                            </div>
                            <div class="card-block custom">
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <a class="summary-link bg bg-primary">
                                            <h3>Total Send SMS ( {{ $sms_report->total_sms_sent }} )</h3>
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <a class="summary-link bg bg-info">
                                            <h3>Remaining SMS ( {{ $sms_report->sms_limit }} )</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/') }}assets/plugins/ckeditor/ckeditor.js"></script>
<script src="{{ asset('assets/js/vanillaSelectBox.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.nav-item').click(function() {
            var nav = $(this).data('nav');
            if(nav == 'nav-offer') {
                $('#nav-due').removeClass(' customactive');
                $('#nav-offer').addClass(' customactive');
            } else {
                $('#nav-offer').removeClass(' customactive');
                $('#nav-due').addClass(' customactive');
            }
        });

        $('#product_list').select2();

        selectDueBoxTest = new vanillaSelectBox("#select_due_customer", {
            "maxHeight": 200, 
            "search": true ,
            "translations": { "all": "All", "items": "items","selectAll":"Select All","clearAll":"Clear All"}
        });

        selectBoxTest = new vanillaSelectBox("#select_offer_customer", {
            "maxHeight": 200, 
            "search": true ,
            "translations": { "all": "All", "items": "items","selectAll":"Select All","clearAll":"Clear All"}
        });
    });
</script>
@endsection