@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Update Password</h4>
            </div>
            <div class="card-block">
                <div class="form">

                    <form class="form-horizontal" action="{{ url('/admin/update_password') }}" method="POST" enctype="multipart/form-data">
                    	{{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                @if(Session::get('success'))
                                    <p class="alert alert-success">{{ Session::get('success') }}</p>
                                @endif
                                @if(Session::get('error'))
                                    <p class="alert alert-danger">{{ Session::get('error') }}</p>
                                @endif
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Old Password</label>
                                        <input type="password" name="old_password" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">New Password</label>
                                        <input type="password" name="new_password" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="control-label col-form-label">Confirm Password</label>
                                        <input type="password" name="confirm_password" class="form-control" required="">
                                    </div>
                                </div>
                                <div lass="form-group row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-info btn-default">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    	
                    </form>

                </div>
                {{-- @if(Session::has('success'))
                	<br>
					<p class="alert alert-success">{{ Session::get('success') }}</p>
                @endif --}}
            </div>
        </div>
    </div>
</div>
@endsection