<style>
    .customer-details ul {
        margin: 0;
        padding: 0;
        list-style: none;
        display: block;
    }
    .customer-details ul li {
        display: block;
        width: 100%;
        border-bottom: 1px solid #ddd;
        padding-bottom: 5px;
        margin-bottom: 5px;
    }
    .customer-details ul li:last-child {
        border-bottom: 0;
        padding-bottom: 0;
        margin-bottom: 0;
    }
</style>
<?php
    $customer = DB::table('users')->where('id', $user_id)->first();
?>
<div class="modal fade" id="customer_details_{{ $user_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title" id="exampleModalLongTitle">Customer Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <div class="customer-details">
                        <ul>
                            <li><strong>Name :</strong> {{$customer->name}}</li>
                            <li><strong>Email :</strong> {{$customer->email}}</li>
                            <li><strong>Phone  :</strong> {{$customer->phone }}</li>
                            <li><strong>Address :</strong> {{$customer->address}}</li>
                            <li><strong>Thana :</strong> {{Helpers::upazilaName($customer->thana)}}</li>
                            <li><strong>District :</strong> {{Helpers::districtName($customer->district)}}</li>
							<li><strong>Division :</strong> {{Helpers::divisionName($customer->division)}}</li>
                            <li><strong>Post Code :</strong> {{$customer->post_code}}</li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
        </div>
    </div>
</div>