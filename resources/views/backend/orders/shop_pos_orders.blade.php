@extends('backend.layout')

@section('maincontent')
    <style>
        button.custom-btn {
            cursor: pointer;
            background: none;
            border: none;
        }
        .btn i {
            font-size: 12px;
        }
        .btn {
            padding: 4px 8px;
        }
        .modal-dialog {
            max-width: 600px;
            margin: 30px auto;
        }
    </style>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Shop POS Order List</h4>
                </div>
                <div class="card-block">
                    @include('backend.orders.shop_order_filter', ['reset_link' => url('admin/pos-orders')])
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                @if(Session::get('role') == 0)
                                <th>Shop</th>
                                @endif
                                <th>Customer</th>
                                <th>Biller</th>
                                <th>Total</th>
                                <th>P.Status</th>
                                <th>Due</th>
                                <th>O.Status</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($order_list) > 0)

                                <?php
                                    if($paginate) {
                                        $i = ($order_list->currentpage()-1)* $order_list->perpage() + 1;
                                    } else {
                                        $i = 1;
                                    }
                                ?>
                                @foreach($order_list as $row)
                                    <?php
                                    $date = new DateTime($row->created_at);
                                    if($row->invoice_status == 0) {
                                        $invoice_status = '<span class="badge badge-primary">Pending <i class="fa fa-caret-down"></i></span>';
                                    }
                                    if($row->invoice_status == 1) {
                                        $invoice_status = '<span class="badge badge-info">Processing <i class="fa fa-caret-down"></i></span>';
                                    }
                                    if($row->invoice_status == 2) {
                                        $invoice_status = '<span class="badge badge-success">Completed</span>';
                                    }
                                    if($row->invoice_status == 3) {
                                        $invoice_status = '<span class="badge badge-danger">Canceled</span>';
                                    }
                                    if($row->payment_status == 0) {
                                        $payment_status = '<span class="badge badge-danger">Due</span>';
                                    }
                                    if($row->payment_status == 1) {
                                        $payment_status = '<span class="badge badge-success">Paid</span>';
                                    }
                                    ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        @if(Session::get('role') == 0)
                                        <td data-title="Shop"><a target="_blank" href="{{url('shop/'.$row->slug)}}">{{$row->store_name}}</a></td>
                                        @endif
                                        <td data-title="Customer">{{ $row->name }}<br>{{$row->phone}} <button title="View Customer Details" class="custom-btn" data-toggle="modal" data-target="#customer_details_{{ $row->user_id }}"><span class="badge badge-success"><i class="fa fa-eye"></i></span></button></td>
                                        <td data-title="Biller"><?php echo Helpers::billerName($row->biller_id); ?></td>
                                        <td data-title="Total">৳ {{number_format($row->total_amount, 2)}}</td>
                                        <td data-title="Payment Status"><?php echo $payment_status; ?></td>
                                        <td data-title="Due">{{$row->due_amount}}</td>
                                        <td data-title="Order Status"><button title="Update Order Status" class="custom-btn" data-toggle="modal" data-target="#order_status_{{ $row->id }}"><?php echo $invoice_status; ?></button></td>
                                        <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                        <td data-title="Action">
                                            <div class="dropdown">
                                                <a class="btn btn-info dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-list"></i>
                                                </a>

                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                    <a target="_blank" class="dropdown-item" href="{{ url('print_order/'.$row->id.'?type=orders_invoice') }}">Print Order</a>
                                                    <a class="dropdown-item" href="{{ url('/admin/order-items/'.$row->id.'?type=invoice') }}">Order Items</a>
                                                    <a class="dropdown-item" href="{{ url('/admin/view-invoice/'.$row->id) }}">Order Details</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                    @include('backend.orders.update_order_status', ['invoice_id' => $row->id, 'order_id' => $row->order_id, 'status' => $row->invoice_status])
                                    @include('backend.orders.customer_details', ['user_id' => $row->user_id])
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        @if($paginate)
                        {{$order_list->links()}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection