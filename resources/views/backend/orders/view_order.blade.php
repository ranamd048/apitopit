@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Order Details</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/orders') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>Biller :</td>
                                <td><?php echo Helpers::billerName($result->biller_id); ?></td>
                            </tr>
                            <tr>
                                <td>Total :</td>
                                <td>৳ {{number_format($result->total, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Sub Total :</td>
                                <td>৳ {{number_format($result->subtotal, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Discount :</td>
                                <td>৳ {{number_format($result->discount, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Shipping Charge :</td>
                                <td>৳ {{number_format($result->delivery_charge, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Payment Status :</td>
                                <td>
                                    <?php if($result->payment_status == 0) {
                                        echo '<span class="badge badge-danger">Due</span>';
                                    } else {
                                        echo '<span class="badge badge-success">Paid</span>';
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Paid Amount :</td>
                                <td>৳ {{number_format($result->paid_amount, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Shipping Address :</td>
                                <td>{{$result->shipping_address}}</td>
                            </tr>
                            <?php
                            $date = new DateTime($result->order_date);
                            if($result->order_status == 0) {
                                $order_status = '<span class="badge badge-primary">Pending</span>';
                            }
                            if($result->order_status == 1) {
                                $order_status = '<span class="badge badge-info">Processing</span>';
                            }
                            if($result->order_status == 2) {
                                $order_status = '<span class="badge badge-success">Completed</span>';
                            }
                            if($result->order_status == 3) {
                                $order_status = '<span class="badge badge-danger">Canceled</span>';
                            }
                            ?>
                            <tr>
                                <td>Order Date :</td>
                                <td>{{$date->format('d-F-Y')}}</td>
                            </tr>
                            <tr>
                                <td>Order Status :</td>
                                <td><?php echo $order_status; ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection