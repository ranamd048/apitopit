@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Order Details</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/orders') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>Biller :</td>
                                <td><?php echo Helpers::billerName($result->biller_id); ?></td>
                            </tr>
                            <tr>
                                <td>Total :</td>
                                <td>৳ {{number_format($result->total_amount, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Discount :</td>
                                <td>৳ {{number_format($result->discount, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Paid Amount :</td>
                                <td>৳ {{number_format($result->paid_amount, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Due Amount :</td>
                                <td>৳ {{number_format($result->due_amount, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Shipping Charge :</td>
                                <td>৳ {{number_format($result->delivery_charge, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Payment Status :</td>
                                <td>
                                    <?php if($result->payment_status == 0) {
                                        echo '<span class="badge badge-danger">Due</span>';
                                    } else {
                                        echo '<span class="badge badge-success">Paid</span>';
                                    } ?>
                                </td>
                            </tr>

                            <?php
                            $date = new DateTime($result->created_at);
                            if($result->invoice_status == 0) {
                                $invoice_status = '<span class="badge badge-primary">Pending</span>';
                            }
                            if($result->invoice_status == 1) {
                                $invoice_status = '<span class="badge badge-info">Processing</span>';
                            }
                            if($result->invoice_status == 2) {
                                $invoice_status = '<span class="badge badge-success">Completed</span>';
                            }
                            if($result->invoice_status == 3) {
                                $invoice_status = '<span class="badge badge-danger">Canceled</span>';
                            }
                            ?>
                            <tr>
                                <td>Date :</td>
                                <td>{{$date->format('d-F-Y')}}</td>
                            </tr>
                            <tr>
                                <td>Invoice Status :</td>
                                <td><?php echo $invoice_status; ?></td>
                            </tr>
                            <tr>
                                <td>Shipping Address :</td>
                                <td>{{ Helpers::orderShippingAddress($result->order_id) }}</td>
                            </tr>
                            <tr>
                                <td>Customer Comments :</td>
                                <td>{{$result->customer_comments}}</td>
                            </tr>
                            <tr>
                                <td>Staff Note :</td>
                                <td>{{$result->staff_note}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection