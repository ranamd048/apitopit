@extends('backend.layout')

@section('maincontent')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">Order Items</h4>
                    <div class="back-button">
                        <a href="{{ url('/admin/orders') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <div class="card-block">
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Shop</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($order_items as $row)
                                <?php
                                $i++;
                                ?>
                                <tr>
                                    <td data-title="Sl">{{$i}}</td>
                                    <td data-title="Shop"><a target="_blank" href="{{url('shop/'.$row->slug)}}">{{$row->store_name}}</a></td>
                                    <td data-title="Product"><a target="_blank" href="{{url('product/'.$row->product_id.'/'.$row->product_slug)}}">{{$row->product_name}}</a></td>
                                    <td data-title="Price">৳ {{number_format($row->price, 2)}}</td>
                                    <td data-title="Quantity">{{$row->quantity}}</td>
                                    <td data-title="Subtotal">৳ {{number_format($row->sub_total, 2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection