<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
    .product-filter button, .product-filter a {
        margin-top: 35px;
    }
    .product-filter .col-md-1 {
        margin: 0;
        padding: 0;
    }
    @media only screen and (max-width: 767px) {
        .product-filter .col-md-1 {
            margin: unset;
            padding-right: 15px;
            padding-left: 15px;
        }
        .product-filter button, .product-filter a {
            margin-top: 15px;
            padding: 10px 50px;
        }
        .product-filter button i, .product-filter a i {
            font-size: 15px;
        }
    }
</style>
<?php
    $from_date = isset($_GET['from_date']) ? $_GET['from_date'] : '';
    $to_date = isset($_GET['to_date']) ? $_GET['to_date'] : '';
    $order_status = isset($_GET['order_status']) ? $_GET['order_status'] : '';
    $customer_id = isset($_GET['customer_id']) ? $_GET['customer_id'] : '';
?>
<div class="row">
    <div class="col-md-12">
        <form action="" method="GET">
            <div class="product-filter">
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>From Date *</label>
                        <input type="date" name="from_date" class="form-control" value="{{ $from_date }}" required="">
                    </div>
                    <div class="col-md-3">
                        <label>To Date *</label>
                        <input type="date" name="to_date" class="form-control" value="{{ $to_date }}" required="">
                    </div>
                    <div class="col-md-2">
                        <label>Order Status *</label>
                        <select name="order_status" class="form-control" required="">
                            <option value="">Select Status</option>
                            <option @if($order_status == '0') selected="" @endif value="0">Pending</option>
                            <option @if($order_status == '1') selected="" @endif value="1">Processing</option>
                            <option @if($order_status == '2') selected="" @endif value="2">Completed</option>
                            <option @if($order_status == '3') selected="" @endif value="3">Canceled</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Customer List</label>
                        <select name="customer_id" class="form-control" id="customer_list">
                            <option value="">Select Customer</option>
                            @foreach($customer_list as $customer)
                            <option @if($customer_id == $customer->id) selected="" @endif value="{{ $customer->id }}">{{ $customer->name }} ({{ $customer->phone }})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-primary" title="Search"><i class="fa fa-search"></i></button>
                        <a href="{{ $reset_link }}" title="Reset" class="btn btn-danger"><i class="fa fa-refresh"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#customer_list').select2();
    });
</script>