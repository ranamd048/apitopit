@extends('backend.layout')

@section('maincontent')
    <style>
        button.custom-btn {
            cursor: pointer;
            background: none;
            border: none;
        }
        .btn i {
            font-size: 12px;
        }
        .btn {
            padding: 4px 8px;
        }
        .modal-dialog {
            max-width: 600px;
            margin: 30px auto;
        }
    </style>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="card-title">POS Order List</h4>
                </div>
                <div class="card-block">
                    @include('backend.orders.filter')
                    <div id="responsive_table">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Customer</th>
                                <th>Total</th>
                                <th>Subtotal</th>
                                <th>Paid</th>
                                <th>P.Status</th>
                                <th>O.Status</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($order_list) > 0)

                                <?php
                                    if($paginate) {
                                        $i = ($order_list->currentpage()-1)* $order_list->perpage() + 1;
                                    } else {
                                        $i = 1;
                                    }
                                ?>
                                @foreach($order_list as $row)
                                    <?php
                                    $date = new DateTime($row->order_date);
                                    if($row->order_status == 0) {
                                        $order_status = '<span class="badge badge-primary">Pending</span>';
                                    }
                                    if($row->order_status == 1) {
                                        $order_status = '<span class="badge badge-info">Processing</span>';
                                    }
                                    if($row->order_status == 2) {
                                        $order_status = '<span class="badge badge-success">Completed</span>';
                                    }
                                    if($row->order_status == 3) {
                                        $order_status = '<span class="badge badge-danger">Canceled</span>';
                                    }
                                    if($row->payment_status == 0) {
                                        $payment_status = '<span class="badge badge-danger">Due</span>';
                                    }
                                    if($row->payment_status == 1) {
                                        $payment_status = '<span class="badge badge-success">Paid</span>';
                                    }
                                    ?>
                                    <tr>
                                        <td data-title="Sl">{{ $i }}</td>
                                        <td data-title="Customer">{{ $row->name }}<br>{{$row->phone}} <button title="View Customer Details" class="custom-btn" data-toggle="modal" data-target="#customer_details_{{ $row->user_id }}"><span class="badge badge-success"><i class="fa fa-eye"></i></span></button></td>
                                        <td data-title="Total">৳ {{number_format($row->total, 2)}}</td>
                                        <td data-title="Subtotal">৳ {{number_format($row->subtotal, 2)}}</td>
                                        <td data-title="Paid">৳ {{number_format($row->paid_amount, 2)}}</td>
                                        <td data-title="Payment Status"><?php echo $payment_status; ?></td>
                                        <td data-title="Order Status"><?php echo $order_status; ?></td>
                                        <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                        <td data-title="Action">
                                            <div class="dropdown">
                                                <a class="btn btn-info dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-list"></i>
                                                </a>

                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                    <a target="_blank" class="dropdown-item" href="{{ url('print_order/'.$row->id) }}">Print Order</a>
                                                    <a class="dropdown-item" href="{{ url('/admin/order-items/'.$row->id) }}">Order Items</a>
                                                    <a class="dropdown-item" href="{{ url('/admin/view-order/'.$row->id) }}">Order Details</a>
                                                    <a target="_blank" class="dropdown-item" href="{{ url('/admin/order-invoices/'.$row->id) }}">Order Invoice</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                    @include('backend.orders.customer_details', ['user_id' => $row->user_id])
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        @if($paginate)
                            {{$order_list->links()}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection