<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
    .product-filter button, .product-filter a {
        margin-top: 35px;
    }
</style>
<?php
    $shop_id = isset($_GET['shop_id']) ? $_GET['shop_id'] : '';
    $customer_id = isset($_GET['customer_id']) ? $_GET['customer_id'] : '';
?>
<div class="row">
    <div class="col-md-12">
        <form action="" method="GET">
            <div class="product-filter">
                <div class="form-group row">
                    <div class="col-md-5">
                        <label>Shop List *</label>
                        <select name="shop_id" class="form-control" id="shop_list" required="">
                            <option value="">Select Shop</option>
                            @foreach($shop_list as $shop)
                            <option @if($shop_id == $shop->id) selected="" @endif value="{{ $shop->id }}">{{ $shop->store_name }} ({{ Helpers::upazilaName($shop->thana) }}, {{ Helpers::districtName($shop->district) }})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-5">
                        <label>Customer List</label>
                        <select name="customer_id" class="form-control" id="customer_list">
                            <option value="">Select Customer</option>
                            @foreach($customer_list as $customer)
                            <option @if($customer_id == $customer->id) selected="" @endif value="{{ $customer->id }}">{{ $customer->name }} ({{ $customer->phone }})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Filter</button>
                        <a href="{{ url('admin/orders') }}" class="btn btn-danger"><i class="fa fa-refresh"></i> Reset</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#shop_list').select2();
        $('#customer_list').select2();
    });
</script>