@extends('backend.layout')

@section('maincontent')
<style>
    h4.report-section-title {
        background: #eee;
        padding: 5px 10px;
        text-align: center;
        margin-bottom: 15px;
        margin-top: 15px;
    }
    .custom_card {
        overflow: hidden;
        border: 1px solid #ddd;
        margin-top: 10px;
        margin-bottom: 10px;
        padding: 10px;
        text-align: center;
        border-radius: 5px;
        background: #f2f7f8;
        color: #333;
        display: block;
    }
    .custom_card a {
        color: #333333;
    }
    .custom_card p {
        margin-bottom: 5px;
    }
    .custom_card h3 {
        margin: 0;
        font-size: 16px;
        line-height: 22px;
    }
    .custom_card img.icon {
        width: 75px;
        height: 75px;
    }
    .report_summary_btn {
        margin-top: 32px;
    }
    /*** Responsive CSS ***/
    @media only screen and (max-width: 767px) {
        .report_summary_btn {
            margin-top: 0px;
        }
        .report_summary_filter .form-group {
            margin-bottom: 10px;
        }
        .report_summary_filter .form-group label {
            margin-bottom: 1px;
        }
    }
    
</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Report Summary</h4>
                <div class="back-button">
                    <a href="{{ url('admin/dashboard') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back to Dashboard</a>
                </div>
            </div>
            <div class="card-block">
                <?php
                    $from_date = isset($_GET['from_date']) ? $_GET['from_date'] : '';
                    $to_date = isset($_GET['to_date']) ? $_GET['to_date'] : '';
                ?>
                <form action="" method="GET">
                    <div class="row report_summary_filter">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Form Date *</label>
                                <input type="date" name="from_date" class="form-control" required="" value="{{ $from_date }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To Date *</label>
                                <input type="date" name="to_date" class="form-control" required="" value="{{ $to_date }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group report_summary_btn">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search Report</button>
                                <a href="{{ url('admin/report-summary') }}" class="btn btn-danger"><i class="fa fa-refresh"></i> Today Report</a>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <div class="custom_card">
                            <h3>{{$report_summary_title}}</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="report-section-title">Total Sale Report</h4>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Opening Balance</p>
                            <p>৳{{ number_format($report_summary['opening_balance'], 2) }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Sale Invoice</p>
                            <p>{{ $report_summary['number_of_sale'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Sale Amount</p>
                            <p>৳{{ number_format($report_summary['total_sale_price'], 2) }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Sale Paid Amount</p>
                            <p>৳{{ number_format($report_summary['total_paid_amount'], 2) }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Sale Due Amount</p>
                            <p>৳{{ number_format($report_summary['total_due_amount'], 2) }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Sale Product Number</p>
                            <p>{{ $report_summary['sale_product_number'] }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="report-section-title">Total Purchase Report</h4>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Purchase Invoice</p>
                            <p>{{ $report_summary['number_of_purchase'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Purchase Amount</p>
                            <p>৳{{ number_format($report_summary['total_purchase_amount'], 2) }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Purchase Paid Amount</p>
                            <p>৳{{ number_format($report_summary['purchase_paid_amount'], 2) }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Purchase Due Amount</p>
                            <p>৳{{ number_format($report_summary['purchase_due_amount'], 2) }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Purchase Product Number</p>
                            <p>{{ $report_summary['number_of_purchase_product'] }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="report-section-title">Others Report</h4>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="custom_card">
                            <p>New Customer</p>
                            <p>{{ $report_summary['total_new_customer'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="custom_card">
                            <p>Service Request</p>
                            <p>{{ $report_summary['total_service_request'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="custom_card">
                            <p>Expense Amount</p>
                            <p>৳{{ number_format($report_summary['total_expense'], 2) }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="custom_card">
                            <h3>All Time Report Summary</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <a href="{{ url('admin/product-list') }}">
                                <p>Total Products</p>
                                <p>{{ $shop_report['total_products'] }}</p>
                                <span class="badge badge-success">Details</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Total Cost Price</p>
                            <p>৳{{ $shop_report['total_cost_price'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Total Sale Price</p>
                            <p>৳{{ $shop_report['total_sale_price'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Total Profit</p>
                            <p>৳{{ $shop_report['total_profit'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <a href="{{ url('admin/shop-staffs') }}">
                                <p>Total Staff</p>
                                <p>{{ $shop_report['total_staff'] }}</p>
                                <span class="badge badge-success">Details</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <a href="{{ url('admin/customer-list') }}">
                                <p>Total Customer</p>
                                <p>{{ $shop_report['total_customer'] }}</p>
                                <span class="badge badge-success">Details</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <a href="{{ url('admin/account/paid-list') }}">
                                <p>Total Paid Customer</p>
                                <p>{{ $shop_report['paid_customer']['total_customer'] }} (৳{{ $shop_report['paid_customer']['total_amount'] }})</p>
                                <span class="badge badge-success">Details</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <a href="{{ url('admin/account/due-list') }}">
                                <p>Total Due Customer</p>
                                <p>{{ $shop_report['due_customer']['total_customer'] }} (৳{{ $shop_report['due_customer']['total_amount'] }})</p>
                                <span class="badge badge-success">Details</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <p>Total Purchase Due</p>
                            <p>৳{{ $shop_report['total_purchase_due'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="custom_card">
                            <a href="{{ url('admin/product-stock-alert') }}">
                                <p>Low Stock Products</p>
                                <p>{{count(Helpers::productStockAlert())}}</p>
                                <span class="badge badge-success">Details</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection