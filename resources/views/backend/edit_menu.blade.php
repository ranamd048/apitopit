@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Edit Menu</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/menu-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
                    @if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/update_menu') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="menu_id" value="{{ $menu_data->id }}">
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Menu Title * </label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" value="{{ $menu_data->name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Parent Menu</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="parent_id">
                                    <option @if($menu_data->parent_id == 0) {{ 'selected' }} @endif value="0">Don't Need Parent</option>
                                    @if(count($menu_list) != 0)
                                    	@foreach($menu_list as $menu)
                                			<option @if($menu_data->parent_id == $menu->id) {{ 'selected' }} @endif value="{{ $menu->id }}">{{ $menu->name }}</option>
                                		@endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Menu Type</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="menu_type" id="menu_type">
                                    <option @if($menu_data->is_custom == 0) {{ 'selected' }} @endif value="page">Page</option>
                                    <option @if($menu_data->is_custom == 1) {{ 'selected' }} @endif value="custom">Custom</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="page_menu" @if($menu_data->is_custom == 1) style="display: none;" @endif>
                            <label class="col-sm-2 text-right control-label col-form-label">Select Page</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="page_link">
                                	@if(count($page_list) != 0)
                                		@foreach($page_list as $page)
                                			<option @if($menu_data->menu_url == 'page/'.$page->slug) {{ 'selected' }} @endif value="page/{{ $page->slug }}">{{ $page->title }}</option>
                                		@endforeach
                                	@else
                                		<option>At First Create Some Page</option>
                                	@endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="custom_menu" @if($menu_data->is_custom == 0) style="display: none;" @endif>
                            <label class="col-sm-2 text-right control-label col-form-label">Custom Link</label>
                            <div class="col-sm-10">
                                <input type="text" name="custom_link" class="form-control" value="{{ $menu_data->menu_url }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Sort Order </label>
                            <div class="col-sm-10">
                                <input type="text" name="sort_order" class="form-control" value="{{ $menu_data->sort_order }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option @if($menu_data->status == 1) {{ 'selected' }} @endif value="1">Active</option>
                                    <option @if($menu_data->status == 0) {{ 'selected' }} @endif value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Update</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
		$("#menu_type").change(function(){
			var menu_type = $(this).val();
			if(menu_type == 'page'){
				$("#page_menu").show();
				$("#custom_menu").hide();
			} else {
				$("#page_menu").hide();
				$("#custom_menu").show();
			}
			
		});
	});
</script>
@endsection