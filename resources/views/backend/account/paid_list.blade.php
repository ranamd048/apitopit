@extends('backend.layout')

@section('maincontent')

<div class="row">
    <input type="hidden" id="file_name" value="account.paid_invoice_list">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Paid Order List</h4>
            </div>
            <div class="card-block">
                @include('backend.account.account_filter')
                @if (Session::has('message')) 
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                <div id="load_invoice_list"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var shop_id = $('#current_shop_id').val();
        //shop customer
        function get_shop_customers(shop_id) {
            var url = APP_URL + '/admin/account/get_shop_customer/' + shop_id;
            $.ajax({
                url: url,
                method: "GET",
                success: function(response) {
                    $('#customer_list').html(response);
                },
            });
        }

        //load invoice list
        function load_invoices(shop_id, status = 0, type = '', user_id = '' ) {
            var file_name = $('#file_name').val();
            var url = APP_URL + '/admin/account/get_user_invoice';
            var loading = '<div class="col-md-12 text-center"><i class="fa fa-spinner fa-spin"></i></div>';
            $('#load_invoice_list').html(loading);
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { file_name: file_name, shop_id: shop_id, type: type, user_id: user_id, status: status },
                cache: false,
                success: function(response) {
                    $('#load_invoice_list').html(response);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        load_invoices(shop_id, 1);
        get_shop_customers(shop_id);

        //search by shop
        $('#shop_list').change(function() {
            var id = $(this).val();
            if (id != '') {
                $('#current_shop_id').val(id);
                load_invoices(id, 1);
                get_shop_customers(id);
            }
        });

        //search by customer
        $('#customer_list').change(function() {
            var shop_id = $('#current_shop_id').val();
            var id = $(this).val();
            if (id == '') {
                load_invoices(shop_id, 1);
            } else {
                load_invoices(shop_id, 1, 'customer', id);
            }
        });
    });
</script>
@endsection