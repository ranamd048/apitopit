<?php $array_collection = array(); ?>
@if($private_settings->sms_service == 1 && count($invoice_list) > 0)
<div class="text-right" style="margin-bottom: 15px;">
    <a href="{{url('admin/account/send_sms_to_all')}}"  class="btn btn-info"><i class="fa fa-send"></i> Send SMS to ALL Customer</a>
</div>
@endif

<div id="responsive_table">
@if(count($invoice_list) > 0)
    <form action="{{ url('admin/account/send_sms_to_selected') }}" method="POST">
        {{csrf_field()}}
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Total Due</th>
                    <th>Action</th>
                    @if($private_settings->sms_service == 1)
                    <th>Select</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach($invoice_list as $row)
                <?php 
                    $i++;
                    $array_item = array('shop_id' => $row->store_id, 'user_id' => $row->user_id, 'due' => $row->total_due_amount);
                    array_push($array_collection, $array_item);
                ?>
                <tr>
                    <td data-title="Sl">{{$i}}</td>
                    <td data-title="Name">{{ $row->name }}</td>
                    <td data-title="Phone">{{ $row->phone }}</td>
                    <td data-title="Email">{{ $row->email }}</td>
                    <td data-title="Address">{{ $row->address }}</td>
                    <td data-title="Total Due">৳ {{ number_format($row->total_due_amount, 2) }}</td>
                    <td data-title="Action">
                        {{-- <a target="_blank" href="{{url('admin/account/customer-due-list/'.$row->store_id.'/'.$row->user_id)}}" class="btn btn-success" title="View Due List"><i class="fa fa-eye"></i></a> --}}
                        <a href="{{url('admin/account/send-notification/'.$row->store_id.'/'.$row->user_id.'?due='.$row->total_due_amount)}}" class="btn btn-primary" title="Send Notification" onclick="return confirm('Are you sure want to send notification?')"><i class="fa fa-send"></i></a>
                    </td>
                    @if($private_settings->sms_service == 1)
                    <td data-title="Select">
                        <input type="hidden" name="shop_id_{{$row->user_id}}" value="{{$row->store_id}}">
                        <input type="hidden" name="due_{{$row->user_id}}" value="{{$row->total_due_amount}}">
                        <input type="checkbox" name="user_id[]" value="{{$row->user_id}}">
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
        @if($private_settings->sms_service == 1)
        <div class="text-right">
            <button type="submit" class="btn btn-success">Send SMS to Selected Customer</button>
        </div>
        @endif
    </form>
    @else
    <div class="text-center"><h4>Not Found...</h4></div>
    @endif
</div>
<?php Session::put('due_customer_list', $array_collection); ?>