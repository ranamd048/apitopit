@extends('backend.layout')

@section('maincontent')
<?php $shop_info = Helpers::singleStoreInfo($shop_id); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Customer Due Order List -> {{$shop_info->store_name}}</h4>
            </div>
            <div class="card-block">
                @if (Session::has('message')) 
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                <div class="table-responsive">
                    @if(count($invoice_list) > 0)
                    <form action="{{url('admin/account/pay_invoice_bill')}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="user_id" value="{{$user_id}}">
                        <div id="responsive_table">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" onchange="checkAll(this)" /></th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Due</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($invoice_list as $row)
                                <?php $date = new DateTime($row->modify_time); ?>
                                <tr>
                                    <td data-title=""><input type="checkbox" name="invoice_id[]" value="{{$row->id}}"></td>
                                    <td data-title="Name">{{ $row->name }}</td>
                                    <td data-title="Phone">{{ $row->phone }}</td>
                                    <td data-title="Address">{{ $row->address }}</td>
                                    <td data-title="Due">৳ {{ number_format($row->due_amount, 2) }}</td>
                                    <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                    <td data-title="Action">
                                    <a target="_blank" title="View Order Items" href="{{url('admin/order-items/'.$row->id.'/'.'?type=invoice')}}" class="btn btn-info"><i class="fa fa-list"></i></a>
                                    <button title="Pay Now" type="button" class="btn btn-success" data-toggle="modal" data-target=".pay_now_modal" data-id="{{$row->id}}" data-name="{{ $row->name }}" data-phone="{{ $row->phone }}" data-due="{{ $row->due_amount }}" data-user="{{ $row->user_id }}"><i class="fa fa-dollar"></i> Pay</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                        <button type="submit" style="float: right;" class="btn btn-info">Pay All</button>
                    </form>
                    @else
                    <div class="text-center"><h4>Not Found...</h4></div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
<!--Pay Now Modal-->
<div class="modal fade pay_now_modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form action="{{ url('admin/account/pay_invoice_due') }}" method="POST">
      {{csrf_field()}}
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pay Order Bill</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Due Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="modal_customer_name"></td>
                    <td id="modal_customer_phone"></td>
                    <td id="modal_due_amount"></td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="invoice_id" id="invoice_id">
        <input type="hidden" name="payment_method" value="cod">
        <input type="hidden" name="due_amount" id="due_amount">
        <input type="hidden" name="user_id" id="user_id">
          <div class="form-group">
            <label for="" class="col-form-label">Paid Amount</label>
            <input type="number" class="form-control" name="paid_amount" id="paid_amount" required="">
          </div>
          <div class="form-group">
            <label for="" class="col-form-label">Discount Amount</label>
            <input type="number" class="form-control" name="discount_amount">
          </div>
          <div class="form-group">
            <label for="" class="col-form-label">Note</label>
            <textarea name="note" class="form-control"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </div>
    </form>
  </div>
</div>

<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }
    $(document).ready(function() {
        //pay now modal
        $('.pay_now_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes
            var name = button.data('name') // Extract info from data-* attributes
            var phone = button.data('phone') // Extract info from data-* attributes
            var due = button.data('due') // Extract info from data-* attributes
            var user_id = button.data('user') // Extract info from data-* attributes
            var modal = $(this)
            modal.find('td#modal_customer_name').text(name);
            modal.find('td#modal_customer_phone').text(phone);
            modal.find('td#modal_due_amount').text('৳ '+due);
            modal.find('.modal-body input#invoice_id').val(id);
            modal.find('.modal-body input#paid_amount').val(due);
            modal.find('.modal-body input#due_amount').val(due);
            modal.find('.modal-body input#user_id').val(user_id);
        });
    });
</script>
@endsection