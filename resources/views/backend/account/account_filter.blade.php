<style>
    .product-filter {
        overflow: hidden;
        background: #F2F7F8;
        padding: 15px 15px 0 15px;
        margin-bottom: 15px;
        border-radius: 5px;
    }
    .product-filter label {
        font-weight: 500;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="product-filter">
            <input type="hidden" id="current_shop_id" value="{{$shop_id}}">
            <div class="form-group row">
                <?php $col = 12; ?>
                @if(!Session::has('store_id'))
                <?php $col = 6; ?>
                <div class="col-md-{{$col}}">
                    <label for="">Shop List</label>
                    <select id="shop_list" class="form-control">
                        @foreach($shop_list as $row)
                            <option value="{{$row->id}}">{{$row->store_name}}</option>
                        @endforeach
                    </select>
                </div>
                @endif
                <div class="col-md-{{$col}}">
                    <label for="">Search by Customer</label>
                    <select id="customer_list" class="form-control">
                        <option value="">Select Customer</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#customer_list').select2();
    });
</script>