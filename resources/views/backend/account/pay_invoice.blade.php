@extends('backend.layout')

@section('maincontent')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Pay Order Due</h4>
            </div>
            <div class="card-block">
                @if (Session::has('message')) 
                    <div class="alert alert-danger">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <form action="{{url('admin/account/paid_invoice_bill')}}" method="POST">
                            {{csrf_field()}}
                            <input type="hidden" name="user_id" value="{{$user_id}}">
                            <input type="hidden" name="payment_method" value="cod">
                            @foreach($invoice_array as $invoice)
                            <input type="hidden" name="invoice_id[]" value="{{$invoice['id']}}">
                            <input type="hidden" name="due_amount[{{$invoice['id']}}]" value="{{$invoice['due']}}">
                            @endforeach
                            <div class="form-group row">
                                <label class="col-sm-4 text-right control-label col-form-label">Payable Amount</label>
                                <div class="col-sm-8">
                                    <input type="text" name="paid_amount" class="form-control" value="{{$total_due_amount}}" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 text-right control-label col-form-label">Payment Note</label>
                                <div class="col-sm-8">
                                    <textarea name="payment_note" class="form-control"></textarea>
                                </div>
                            </div>
                            <div lass="form-group row">
                                <div class="offset-sm-4 col-sm-8">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection