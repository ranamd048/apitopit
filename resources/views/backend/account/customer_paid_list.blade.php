@extends('backend.layout')

@section('maincontent')
<?php $shop_info = Helpers::singleStoreInfo($shop_id); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Customer Paid Order List -> {{$shop_info->store_name}}</h4>
            </div>
            <div class="card-block">
                @if (Session::has('message')) 
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div> 
                @endif
                <div id="responsive_table">
                    @if(count($invoice_list) > 0)
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer Name</th>
                                    <th>Customer Phone</th>
                                    <th>Address</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Discount</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                                @foreach($invoice_list as $row)
                                <?php $i++; $date = new DateTime($row->modify_time); ?>
                                <tr>
                                    <td data-title="Sl">{{$i}}</td>
                                    <td data-title="Name">{{ $row->name }}</td>
                                    <td data-title="Phone">{{ $row->phone }}</td>
                                    <td data-title="Address">{{ $row->address }}</td>
                                    <td data-title="Total">৳ {{ number_format($row->total_amount, 2) }}</td>
                                    <td data-title="Paid">৳ {{ number_format($row->paid_amount, 2) }}</td>
                                    <td data-title="Discount">৳ {{ number_format($row->discount, 2) }}</td>
                                    <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                    <div class="text-center"><h4>Not Found...</h4></div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>

@endsection