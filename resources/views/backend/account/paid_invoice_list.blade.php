<div id="responsive_table">
    @if(count($invoice_list) > 0)
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Customer Name</th>
                <th>Customer Phone</th>
                <th>Address</th>
                <th>Total Paid</th>
                <th>Total Discount</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 0; ?>
            @foreach($invoice_list as $row)
            <?php $i++; ?>
            <tr>
                <td data-title="Sl">{{$i}}</td>
                <td data-title="Name">{{ $row->name }}</td>
                <td data-title="Phone">{{ $row->phone }}</td>
                <td data-title="Address">{{ $row->address }}</td>
                <td data-title="Total Paid">৳ {{ number_format($row->total_paid_amount, 2) }}</td>
                <td data-title="Total Discount">৳ {{ number_format($row->total_discount, 2) }}</td>
                <td data-title="Action">
                    <a target="_blank" href="{{url('admin/account/customer-paid-list/'.$row->store_id.'/'.$row->user_id)}}" class="btn btn-success" title="View Paid List"><i class="fa fa-eye"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="text-center"><h4>Not Found...</h4></div>
    @endif
</div>