@extends('backend.layout')

@section('maincontent')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="card-title">Add New Page</h4>
                <div class="back-button">
                    <a href="{{ url('/admin/page-list') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
                </div>
            </div>
            <div class="card-block">
                <div class="form">
					@if (Session::has('message')) 
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div> 
                    @endif
                    <form class="form-horizontal" action="{{ url('/admin/page_save') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="page_type" value="custom">
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Page title * </label>
                            <div class="col-sm-10">
                                <input type="text" name="page_title" class="form-control" required placeholder="Page Title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Page Content</label>
                            <div class="col-sm-10">
                                <textarea name="page_content" class="form-control" placeholder="Page Content"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div lass="form-group row">
                            <div class="offset-sm-2 col-sm-8">
                                <button type="submit" class="btn btn-info btn-default">Create</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/') }}assets/plugins/ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'page_content' );
</script>

@endsection