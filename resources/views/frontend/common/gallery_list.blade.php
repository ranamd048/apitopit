<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Gallery List
@endsection

@section('main_content')
<!--breadcrumb area start-->
<div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs text-center">
                    <h1>Gallery List</h1>
                    <ul>
                        <li><a href="{{url('/')}}">home</a></li>
                        <li>/</li>
                        <li>Gallery List Page</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(count($gallery_list) > 0) : ?>
<div class="gallery-section section-padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="gallery-filter-menu">
                    <ul>
                        <li class="filter" data-filter="all">All</li>
                        <?php foreach($type_list as $row) : ?>
                        <?php
                        $filter_type = str_replace(' ', '-', $row->type_title);
                        ?>
                        <li class="filter" data-filter=".<?php echo strtolower($filter_type); ?>"><?php echo $row->type_title; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="gallerys" id="gallery_filter">
                    <?php foreach($gallery_list as $row) : ?>
                    <?php
                    $filter_query = DB::table('gallery_type_relation')
                        ->join('gallery_types', 'gallery_type_relation.gallery_type_id', '=', 'gallery_types.types_id')
                        ->select('gallery_types.type_title')
                        ->where('gallery_type_relation.gallery_id', $row->id)
                        ->get();
                    ?>
                    <div class="single-gallery mix <?php
                    foreach($filter_query as $value){
                        $filter_name = str_replace(' ', '-',$value->type_title);
                        echo strtolower($filter_name.' ');
                    }
                    ?>">
                        <img src="{{ asset('public/uploads/gallery/'.$row->gallery_image) }}" alt="">
                        <div class="overly-bg"></div>
                        <div class="s2g-popup-icon popup-icon">
                            <a href="{{ asset('public/uploads/gallery/'.$row->gallery_image) }}"><i class="fa fa-search-plus"></i></a>
                        </div>
                        <div class="project-title">
                            Test Caption
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>

        </div>

    </div>
</div>
<?php endif; ?>
@endsection
