<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Checkout Page
@endsection

@section('main_content')
<style>
.payment-method {
    text-align: left;
    overflow: hidden;
}
.payment-method h4 {
    font-size: 18px;
    padding-bottom: 8px;
}
.payment-method h4 ul li a.nav-link {
    font-size: 14px;
}
.payment-method ul li a.nav-link {
    font-size: 14px;
}
</style>
    <div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>Checkout</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            <li>Checkout Page</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- cart view start-->
    <section class="cart-view-section section-padding mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-lg-0 mb-5">
                    <div class="cart-item-details">
                        @if($cart_items->count() > 0)
                            <ul>
                                @foreach($cart_items as $row)
        <?php
            $product_price = Helpers::is_promo_or_normal_product($row->id);
            $shop_info = Helpers::shop_info($row->attributes->store_id);
        ?>
                                    <li class="c-data text-uppercase mb-4 shadow cart-item">
                                        <div class="row p-3">
                                            <div class="col-7 d-flex">
                                                <div class="cart-item custom-control">
                                                    <div class="d-flex">
                                                        <div class="c-thumb">
                                                            <img src="{{ asset('public/uploads/products/thumbnail/'.$row->attributes->image) }}" alt="" class="img-fluid">
                                                        </div>
                                                        <span class="c-info ml-2">
                                                <p class="mb-0">{{$row->name}}</p>
                                                <p class="shop_name"><a target="_blank" href="{{url('shop/'.$shop_info->slug)}}" class="badge badge-success">{{$shop_info->store_name}}</a></p>
                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-5 text-center">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <?php
                                                        $total_price = ($row->price * $row->quantity);
                                                        ?>
                                                        <p class="font-weight-bold text-danger m-0">৳ {{number_format($total_price, 2)}}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="c-qty d-flex mt-2">
                                                            <span class="m-auto" id="qty_value_{{$row->id}}">{{$row->quantity}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <h4>Cart Item Not found</h4>
                        @endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="cart-totals z-depth-1 rounded">
                    @if(Session::has('distributor_id'))
                        <div class="order-discount p-2">
                            <span class="font-weight-bold text-capitalize">order discount</span>
                            <div class="form-group">
                                <select id="discount_type">
                                    <option value="flat">Flat</option>
                                    <option value="percentage">Percentage</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input id="discount_amount" name="discount_amount" type="number" placeholder="Discount">
                            </div>
                        </div>
                    @endif
                        @if(Session::has('customer_id'))
                        <div class="ch-address p-3 pb-0">
                            <div class="d-flex clearfix">
                                <i class="fa fa-map-marker my-auto"></i>
                                <h6 class="ml-2 text-capitalize font-weight-bold">{{Auth::user()->name}}</h6>
                            </div>
                            <p class="m-0">{{Auth::user()->address}}</p>
                        </div>
                        @endif
                        <ul class="cart p-2">
                            <span class="font-weight-bold text-capitalize">order summary</span>

                            <li class="text-capitalize p-2">total items<span class="float-right">{{$cart_items->count()}} items</span></li>
                            <li class="text-capitalize p-2">total Amount<span class="float-right">৳ {{number_format($total_amount, 2)}}</span></li>
                            <?php $total_shipping_cost = Helpers::get_total_shipping_cost(); ?>
                            <li class="text-capitalize p-2">Shipping cost<span class="float-right">৳ {{number_format($total_shipping_cost, 2)}}</span></li>
                            <!-- {{--<li class="mt-3 mb-3">--}}
                                {{--<div class="coupon-form">--}}
                                    {{--<form action="#">--}}
                                        {{--<div class="d-flex">--}}
                                            {{--<input type="text" class="form-control border m-0 mr-1 pl-2" placeholder="Enter Voucher Code" style="height: initial;">--}}
                                            {{--<button type="submit" class="btn btn-primary m-0">Apply</button>--}}
                                        {{--</div>--}}
                                    {{--</form>--}}
                                {{--</div>--}}
                            {{--</li>--}} -->
                            <?php $grand_total = ($total_amount + $total_shipping_cost); ?>
                            <input type="hidden" id="grand_total_amount" value="{{$grand_total}}">
                            <div id="show_discount_amount"></div>
                            <li class="text-capitalize p-2">grand total<span class="float-right" id="show_grand_total">৳ {{number_format($grand_total, 2)}}</span></li>
                            @if(Session::has('customer_id') || Session::has('distributor_id'))
                                <div class="text-center">
                                    <form id="order_place_form">
                                        @if(Session::has('distributor_id'))
                                        <div class="form-group">
                                            <input type="text" id="search_customer" placeholder="Search Customer by Phone No" class="form-control">
                                            <input type="hidden" name="customer_id" id="show_customer_id" value="0">
                                            <div id="show-customer-list"></div>
                                        </div>
                                        <input type="hidden" name="biller_id" value="{{Session::get('distributor_id')}}">
                                        @else
                                            <input type="hidden" name="biller_id" value="{{Auth::user()->reference_id}}">
                                            <input type="hidden" name="customer_id" value="{{Auth::user()->id}}">
                                        @endif
                                        <input type="hidden" name="order_type" value="cart_order">
                                        <input type="hidden" name="total_amount" value="{{$total_amount}}">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="address" placeholder="Shipping Address">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="note" placeholder="Order Note">
                                        </div>
                                        <input type="hidden" name="sub_total" id="grand_total_value" value="{{$grand_total}}">
                                        <input type="hidden" name="discount" id="discount_value" value="0">
                                        <input type="hidden" name="delivery_charge" value="{{$total_shipping_cost}}">
                                        <input type="hidden" name="payment_method" id="show_payment_method" value="cod">
                                        <div class="payment-method">
                                            <h4>Payment Method:</h4>
                                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                <li class="nav-item font-weight-bold text-capitalize">
                                                    <a class="nav-link active" data-method="cod" id="other-tab" data-toggle="tab" href="#products_tab"
                                                    role="tab"
                                                    aria-controls="other"
                                                    aria-selected="true">CASH</a>
                                                </li>
                                                <li class="nav-item font-weight-bold text-capitalize">
                                                    <a class="nav-link" data-method="bkash" id="home-tab" data-toggle="tab" href="#shops_tab"
                                                    role="tab"
                                                    aria-controls="home"
                                                    aria-selected="true">BKASH</a>
                                                </li>
                                                
                                                <li class="nav-item font-weight-bold text-capitalize">
                                                    <a class="nav-link" data-method="rocket" id="review-tab" data-toggle="tab" href="#service_tab" role="tab"
                                                    aria-controls="review"
                                                    aria-selected="false">ROCKET</a>
                                                </li>
                                            </ul>


                        <div class="tab-content border border-top-0 p-3" id="myTabContent">
                            <div class="tab-pane fade show active" id="products_tab" role="tabpanel"
                                aria-labelledby="other-tab">
                                <span>Cash On  Delivery</span>
                            </div>
                            <div class="tab-pane fade" id="shops_tab" role="tabpanel"
                                aria-labelledby="home-tab">
                                <div class="form-group">
                                    <label for="">Paid Amount</label>
                                    <input type="text" readonly name="bkash_paid_amount" class="form-control paid_amount" value="{{$grand_total}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Payment Note</label>
                                    <input type="text" name="bkash_payment_note" class="form-control">
                                </div>
                            </div>
                            
                            <div class="tab-pane fade" id="service_tab" role="tabpanel" aria-labelledby="review-tab">
                                <div class="form-group">
                                    <label for="">Paid Amount</label>
                                    <input type="text" readonly name="rocket_paid_amount" class="form-control paid_amount" value="{{$grand_total}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Payment Note</label>
                                    <input type="text" name="rocket_payment_note" class="form-control">
                                </div>
                            </div>
                        </div>



                                        </div>

                                        
                                        <button type="submit" id="place_order_btn" class="w-100 waves-effect waves-light btn-small white-text mt-4 font-weight-bold z-depth-1 rounded">place order</button>
                                        @if(Session::has('customer_id'))
                                        <button type="button" id="save_order_btn" class="w-100 waves-effect waves-light btn-small white-text mt-4 font-weight-bold z-depth-1 rounded">save order</button>
                                        @endif
                                    </form>
                                </div>
                            @else
                                <div class="text-center">
                                    <a href="{{url('login')}}" class="w-100 waves-effect waves-light btn-small white-text mt-4 font-weight-bold z-depth-1 rounded">Login</a>
                                </div>
                                <div class="text-center">
                                    <p style="margin-top: 15px; margin-bottom: 0;">OR</p>
                                </div>
                                <div class="text-center">
                                    <a href="{{url('registration')}}" class="w-100 waves-effect waves-light btn-small white-text mt-4 font-weight-bold z-depth-1 rounded">Registration</a>
                                </div>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cart view end-->
    <script>
        $(document).ready(function() {
            $('.payment-method a.nav-link').click(function(e) {
                e.preventDefault();
                var method = $(this).data('method');
                $('#show_payment_method').val(method);
            });

            var discount_type = $('#discount_type').val();
            var total_amount = $('#grand_total_amount').val();
            $(document).on('change', '#discount_type', function() {
                $('#discount_amount').val('');
                $('#show_discount_amount').html('');
                $('#show_grand_total').html('৳ '+ parseFloat(total_amount).toFixed(2));
                $('#grand_total_value').val(total_amount);
                $('.paid_amount').val(total_amount);
                $('#discount_value').val(0);
            });
            $(document).on('change', '#discount_amount', function() {
                discount_type = $('#discount_type').val();
                var discount = $(this).val();
                if(discount_type == 'flat') {
                    var grand_total = (total_amount - discount);
                } else {
                    var discount = (total_amount * discount) / 100;
                    grand_total = (total_amount - discount);
                }
                $('#grand_total_value').val(grand_total);
                $('.paid_amount').val(grand_total);
                $('#discount_value').val(discount);
                discount = parseFloat(discount).toFixed(2);
                $('#show_grand_total').html('৳ '+ parseFloat(grand_total).toFixed(2));
                var discount_html = '<li class="text-capitalize p-2">discount amount <span class="float-right">৳ '+discount+'</span></li>';
                $('#show_discount_amount').html(discount_html);
            });
        })
    </script>
@endsection