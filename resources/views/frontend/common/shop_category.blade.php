<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    {{$category->name}}
@endsection

@section('main_content')
    <div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>{{$category->name}}</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            @if(!empty($parent_category))
                            <li><a href="{{url('/shop_category/'.$parent_category->slug)}}">{{$parent_category->name}}</a></li>
                            <li>/</li>
                            @endif
                            <li>{{$category->name}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <!--shop page content section start-->
    <section class="ssp-product-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    @include('frontend.common.product_sidebar')
                </div>
                <div class="col-md-9">
                @if(count($sub_categories) > 0)
                <div class="sub-categories-section shop-list-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title styled">
                                    <h3 class="text-capitalize">Sub Category for {{$category->name}}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            <div class="shop-lists">
                                <div class="row">
                                    @foreach($sub_categories as $row)
                                        <?php
                                        if(!empty($row->image)) {
                                            $category_image = asset('public/uploads/category/'.$row->image);
                                        } else {
                                            $category_image = asset('frontend_assets/images/cat-icon.jpg');
                                        }
                                        ?>
                                    <div class="col-md-3 col-4">
                                        <div class="single-shop-list white z-depth-1 p-2">
                                            <a href="{{ url('/shop_category/'.$row->slug) }}">
                                                <div class="shop-thumb">
                                                    <img src="{{$category_image}}" alt="{{ $row->name }}" class="img-fluid">
                                                </div>
                                                <div class="shop-name">
                                                    <h5>{{ $row->name }}</h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                    <div class="ssp-products">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title styled">
                                    <h3 class="text-capitalize">Products</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @if(count($product_list) > 0)
                                @foreach($product_list as $row)
                                    <div class="col-md-4 col-6">
                                    @include('frontend.common.product_item')
                                    </div>
                                @endforeach
                                <div class="col-md-12">
                                    {{ $product_list->links() }}
                                </div>
                            @else
                                <div class="col-md-12 text-center">
                                    <h3>Not Found...</h3>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--shop page content section end-->
@endsection