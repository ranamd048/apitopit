<?php $layout_file = 'frontend.' . $site_settings->theme . '.layout'; ?>
@extends($layout_file)
@section('title')
    {{ $service->service_name }}
@endsection

@section('main_content')
@section('meta_content')
    <meta name="description" content="{{ mb_substr(strip_tags($service->service_description), 0, 200) }}" />
    <meta property="og:title" content="{{ $service->service_name }}" />
    <meta property="og:url" content="{{ url('/service/'.$service->id.'/'.$service->service_slug) }}" />
    <meta property="og:site_name" content="{{ url('/') }}" />
    <meta property="og:description" content="{{ mb_substr(strip_tags($service->service_description), 0, 200) }}" />
    <meta property="og:image" content="{{ asset('public/uploads/services/'.$service->service_image) }}" />
    <meta property="og:image:width" content="480">
    <meta property="og:image:height" content="360">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{ url('/') }}">
    <meta name="twitter:title" content="{{ $service->service_name }}">
    <meta name="twitter:description" content="{{ mb_substr(strip_tags($service->service_description), 0, 200) }}">
    <meta name="twitter:image" content="{{ asset('public/uploads/services/'.$service->service_image) }}">
    <link rel="canonical" href="{{ url('/service/'.$service->id.'/'.$service->service_slug) }}">
@endsection

<style>
    .modal-window {
        position: fixed;
        background-color: rgba(255, 255, 255, 0.25);
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        visibility: hidden;
        opacity: 0;
        pointer-events: none;
        transition: all 0.3s;
    }
    .modal-window .modal_content {
        width: 700px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 2em;
        background: #ffffff;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.1);
    }
    .modal-window.open {
        visibility: visible;
        opacity: 1;
        pointer-events: auto;
    }
    .modal-close {
        color: #ffffff;
        line-height: 35px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 40px;
        text-decoration: none;
        background: red;
    }
    .modal-close:hover {
        color: #ffffff;
    }
    textarea.form-control {
        height: 120px;
    }
    @media only screen and (max-width: 767px) {
        .modal-window .modal_content {
            width: 90%;
            height: 100vh;
            overflow: auto;
        }
    }
</style>
    <div class="breadcrumb-area"
         style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>Service Details</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            <li>{{ $service->service_name }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if(isset($service->store->logo)) {
        $shop_logo = asset('public/uploads/store/'.$service->store->logo);
    } else {
        $shop_logo = asset('frontend_assets/images/shop_logo.png');
    }
    ?>

    <section class="ssp-hero-wrapper service-details-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="service-details-wrapper">
                        <div class="row">

                            <div class="col-md-5">
                                <div class="ssp-hero-thumb shop-category-tab baner-video">
                                    @if(!empty($service->service_image) || !empty($service->video_link))
                                        @if($service->video_link)
                                            <div class="tab-menu shop-details">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li>
                                                        <a class="active" data-toggle="tab" href="#baner_image" role="tab"  aria-selected="true">Service Photo</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#shop_video" role="tab" aria-selected="true">Service Video</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane fade show active" id="baner_image" role="tabpanel" aria-labelledby="baner_image-tab" style="border: 1px solid #000;margin: 6px;border-radius: 4px;padding: 6px;">
                                                    <img src="{{ asset('public/uploads/services/'.$service->service_image) }}" alt="" class="w-100">
                                                </div>
                                                <div class="tab-pane fade" id="shop_video" role="tabpanel" aria-labelledby="shop_video-tab">
                                                    <iframe src="{{$service->video_link}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%;min-height: 250px;"></iframe>
                                                </div>
                                            </div>
                                        @else
                                            <img style="border: 1px solid #000;margin: 0px;border-radius: 4px;padding: 0px;" src="{{ asset('public/uploads/services/'.$service->service_image) }}" alt="" class="w-100">
                                        @endif
                                    @else
                                        <div class="service-details">
                                            <h2>{{$service->service_name}}</h2>
                                            {!! html_entity_decode($service->service_description) !!}
                                            <div class="share-items clearfix">

                                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b61cf9c0ea30730"></script>

                                                <!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox" data-url="{{url('service/'.$service->id.'/'.$service->service_slug)}}" data-title="{{$service->service_name}}" data-description="<?php echo mb_substr(strip_tags($service->service_description), 0, 200); ?>" style="clear: both;"><div id="atstbx" class="at-resp-share-element at-style-responsive addthis-smartlayers addthis-animated at4-show" aria-labelledby="at-3ae9c96f-60bb-4fba-bfc4-0275552cbefe" role="region"><span id="at-3ae9c96f-60bb-4fba-bfc4-0275552cbefe" class="at4-visually-hidden">AddThis Sharing Buttons</span><div class="at-share-btn-elements"><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-facebook" style="background-color: rgb(59, 89, 152); border-radius: 0px;"><span class="at4-visually-hidden">Share to Facebook</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1" class="at-icon at-icon-facebook" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-facebook-1">Facebook</title><g><path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-twitter" style="background-color: rgb(29, 161, 242); border-radius: 0px;"><span class="at4-visually-hidden">Share to Twitter</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2" class="at-icon at-icon-twitter" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-twitter-2">Twitter</title><g><path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-whatsapp" style="background-color: rgb(77, 194, 71); border-radius: 0px;"><span class="at4-visually-hidden">Share to WhatsApp</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-whatsapp-3" class="at-icon at-icon-whatsapp" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-whatsapp-3">WhatsApp</title><g><path d="M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-email" style="background-color: rgb(132, 132, 132); border-radius: 0px;"><span class="at4-visually-hidden">Share to Email</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-email-4" class="at-icon at-icon-email" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-email-4">Email</title><g><g fill-rule="evenodd"></g><path d="M27 22.757c0 1.24-.988 2.243-2.19 2.243H7.19C5.98 25 5 23.994 5 22.757V13.67c0-.556.39-.773.855-.496l8.78 5.238c.782.467 1.95.467 2.73 0l8.78-5.238c.472-.28.855-.063.855.495v9.087z"></path><path d="M27 9.243C27 8.006 26.02 7 24.81 7H7.19C5.988 7 5 8.004 5 9.243v.465c0 .554.385 1.232.857 1.514l9.61 5.733c.267.16.8.16 1.067 0l9.61-5.733c.473-.283.856-.96.856-1.514v-.465z"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-compact" style="background-color: rgb(255, 101, 80); border-radius: 0px;"><span class="at4-visually-hidden">Share to More</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-addthis-5" class="at-icon at-icon-addthis" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-addthis-5">AddThis</title><g><path d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z" fill-rule="evenodd"></path></g></svg></span></a></div></div></div>

                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-7">
                                <div class="details-section p-3">
                                    <div class="shop-name">
                                        <h3 style="font-size: 1.8em;margin: 6px 0;">{{ $service->service_name }}</h3>
                                    </div>
                                    <a href="{{ url('shop/'.$service->store->slug) }}" target="_blank">
                                        <div class="product-title">
                                            <h6 class="text-capitalize font-weight-bold" style="margin-bottom: 10px;">{{ $service->store->store_name }}</h6>
                                        </div>
                                    </a>
                                    <p title="owner name"><i class="material-icons">person</i> {{ Helpers::storePropiterName($service->store->id) }}</p>
                                    <p><i class="material-icons">phone</i> {{ $service->store->phone }}</p>
                                    <p><i class="material-icons">location_city</i> {{ $service->store->address }}</p>
                                    @if($service->price)
                                        <p><i class="material-icons">monetization_on</i> Service Charge : ৳ {{ $service->price }}</p>
                                    @endif
                                    @if($service->experience)
                                        <p><i class="material-icons">build</i> Experience : {{ $service->experience }}</p>
                                    @endif
                                    <div class="call-us-btn">
                                        <a href="#" id="open_service_request_modal"><i class="material-icons">send</i> Send Request</a>
                                        <a href="#" class="add_to_favourite" data-id="{{$service->id}}" data-type="service"><i class="fa fa-heart"></i> Favourite</a>
                                    </div>

                                    <div class="share-items clearfix">

                                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b61cf9c0ea30730"></script>

                                        <!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox" data-url="{{url('service/'.$service->id.'/'.$service->service_slug)}}" data-title="{{$service->service_name}}" data-description="<?php echo mb_substr(strip_tags($service->service_description), 0, 200); ?>" style="clear: both;"><div id="atstbx" class="at-resp-share-element at-style-responsive addthis-smartlayers addthis-animated at4-show" aria-labelledby="at-3ae9c96f-60bb-4fba-bfc4-0275552cbefe" role="region"><span id="at-3ae9c96f-60bb-4fba-bfc4-0275552cbefe" class="at4-visually-hidden">AddThis Sharing Buttons</span><div class="at-share-btn-elements"><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-facebook" style="background-color: rgb(59, 89, 152); border-radius: 0px;"><span class="at4-visually-hidden">Share to Facebook</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1" class="at-icon at-icon-facebook" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-facebook-1">Facebook</title><g><path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-twitter" style="background-color: rgb(29, 161, 242); border-radius: 0px;"><span class="at4-visually-hidden">Share to Twitter</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2" class="at-icon at-icon-twitter" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-twitter-2">Twitter</title><g><path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-whatsapp" style="background-color: rgb(77, 194, 71); border-radius: 0px;"><span class="at4-visually-hidden">Share to WhatsApp</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-whatsapp-3" class="at-icon at-icon-whatsapp" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-whatsapp-3">WhatsApp</title><g><path d="M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-email" style="background-color: rgb(132, 132, 132); border-radius: 0px;"><span class="at4-visually-hidden">Share to Email</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-email-4" class="at-icon at-icon-email" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-email-4">Email</title><g><g fill-rule="evenodd"></g><path d="M27 22.757c0 1.24-.988 2.243-2.19 2.243H7.19C5.98 25 5 23.994 5 22.757V13.67c0-.556.39-.773.855-.496l8.78 5.238c.782.467 1.95.467 2.73 0l8.78-5.238c.472-.28.855-.063.855.495v9.087z"></path><path d="M27 9.243C27 8.006 26.02 7 24.81 7H7.19C5.988 7 5 8.004 5 9.243v.465c0 .554.385 1.232.857 1.514l9.61 5.733c.267.16.8.16 1.067 0l9.61-5.733c.473-.283.856-.96.856-1.514v-.465z"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-compact" style="background-color: rgb(255, 101, 80); border-radius: 0px;"><span class="at4-visually-hidden">Share to More</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-addthis-5" class="at-icon at-icon-addthis" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-addthis-5">AddThis</title><g><path d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z" fill-rule="evenodd"></path></g></svg></span></a></div></div></div>

                                    </div>

                                </div>
                            </div>

                        </div>
                        @if(!empty($service->service_image))
                            <div class="row p-4">
                                <div class="col-md-12">
                                    <div class="p-tab">
                                        <?php $active_class = 'active'; ?>
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item font-weight-bold text-capitalize">
                                                <a class="nav-link {{$active_class}}" id="home-tab" data-toggle="tab" href="#description"
                                                   role="tab"
                                                   aria-controls="home"
                                                   aria-selected="true">Details</a>
                                            </li>
                                            <li class="nav-item font-weight-bold text-capitalize">
                                                <a class="nav-link" id="other-tab" data-toggle="tab" href="#other_products"
                                                   role="tab"
                                                   aria-controls="other"
                                                   aria-selected="true">Related Service</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content border border-top-0 p-3" id="myTabContent">
                                            <div class="tab-pane fade {{$active_class}} show" id="description" role="tabpanel"
                                                 aria-labelledby="home-tab">
                                                {!! html_entity_decode($service->service_description) !!}
                                            </div>

                                            <div class="tab-pane fade" id="other_products" role="tabpanel"
                                                 aria-labelledby="other-tab">
                                                <div class="row">
                                                    @foreach($related_services as $row)
                                                        <div class="col-md-4">

                                                            <a href="{{ url('/service/'.$row->id.'/'.$row->service_slug) }}">
                                                                <div class="single-product service-item">
                                                                    <div class="product-thumb">
                                                                        <img src="{{ asset('public/uploads/services/'.$row->service_image) }}"
                                                                             class="img-fluid" alt="{{ $row->service_name }}">
                                                                    </div>
                                                                    <div class="product-info text-center">
                                                                        <h5>{{ $row->service_name }}</h5>
                                                                        <!--<p class="weight">1 kg</p>-->
                                                                        <p class="price">৳
                                                                            <strong>{{ number_format($row->price, 2) }}</strong>
                                                                        </p>
                                                                        <p class="cart-btn"><i
                                                                                class="fa fa-eye"></i> view details</p>

                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(count($related_services) > 0)
        <section class="related-service-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title styled">
                            <h3 class="text-capitalize">Related Service</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="ssp-products">
                            <div class="row">
                                @foreach($related_services as $row)
                                    <div class="col-md-4">

                                        <a href="{{ url('/service/'.$row->id.'/'.$row->service_slug) }}">
                                            <div class="single-product service-item">
                                                <div class="product-thumb">
                                                    <img src="{{ asset('public/uploads/services/'.$row->service_image) }}"
                                                         class="img-fluid" alt="{{ $row->service_name }}">
                                                </div>
                                                <div class="product-info text-center">
                                                    <h5>{{ $row->service_name }}</h5>
                                                    <!--<p class="weight">1 kg</p>-->
                                                    <p class="price">৳
                                                        <strong>{{ number_format($row->price, 2) }}</strong>
                                                    </p>
                                                    <p class="cart-btn"><i
                                                                class="fa fa-eye"></i> view details</p>

                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    <!--Send Request Modal-->
    <div id="service-request-modal" class="modal-window">
        <div class="modal_content">
            <a href="#" title="Close" class="modal-close"><i class="fa fa-times"></i></a>
            <div class="request-form">
                <form action="" id="service_request_form">
                    <input type="hidden" name="service_id" value="{{$service->id}}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="why_need" class="form-control" placeholder="Why Need ?">
                            </div>
                            <div class="form-group">
                                <input type="text" name="when_need" class="form-control" placeholder="When Need ?">
                            </div>
                            <div class="form-group">
                                <input type="text" name="contact_no" class="form-control" placeholder="Contact Number *">
                            </div>
                            <div class="form-group">
                                <textarea name="other_details" class="form-control" placeholder="Details"></textarea>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" id="submit_service_request" class="btn btn-success">Send</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.modal-close').click(function(e) {
                e.preventDefault();
                $('.modal-window').removeClass('open');
            });
        });
    </script>
@endsection
