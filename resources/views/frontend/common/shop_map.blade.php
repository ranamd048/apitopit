<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)

@section('title')
    {{$shop->store_name}} Map View
@endsection

@section('main_content')
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

<!--breadcrumb area start-->
<div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs text-center">
                    <h1>Map View</h1>
                    <ul>
                        <li><a href="{{url('/')}}">home</a></li>
                        <li>/</li>
                        <li>{{$shop->store_name}} Map View</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--breadcrumb area end-->

<!--contact section start-->
<section class="contact-us-section">
    <div>
        @if(!empty($shop->map_lat) && !empty($shop->map_long))
            <div id="googleMap" style="width:100%;height:600px;"></div>
        @else
            <h1 style="text-align: center;color: red;">NOT FOUND MAP!</h1>
        @endif
    </div>
</section>
<!--contact section end-->

@if(!empty($shop->map_lat) && !empty($shop->map_long))
<!--=========== BEGIN GOOGLE MAP SECTION ================-->
<script>
    function myMap() {
        var mypointval = new google.maps.LatLng({{$shop->map_lat}}, {{$shop->map_long}});
        var mapProp= {
            center:mypointval,
            zoom:15,
        };
        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
        var marker = new google.maps.Marker({
            position:mypointval,
            animation: google.maps.Animation.DROP,
            title: "{{$shop->store_name}}",
            //label: "ABC",
            icon: {
                path: "M10.453 14.016l6.563-6.609-1.406-1.406-5.156 5.203-2.063-2.109-1.406 1.406zM12 2.016q2.906 0 4.945 2.039t2.039 4.945q0 1.453-0.727 3.328t-1.758 3.516-2.039 3.070-1.711 2.273l-0.75 0.797q-0.281-0.328-0.75-0.867t-1.688-2.156-2.133-3.141-1.664-3.445-0.75-3.375q0-2.906 2.039-4.945t4.945-2.039z",
                fillColor: "blue",
                fillOpacity: 1,
                strokeWeight: 0,
                rotation: 0,
                scale: 2,
                anchor: new google.maps.Point(15, 30),
            },
        });
        marker.setMap(map);

        google.maps.event.addListener(marker,'click',function() {
            var pos = map.getZoom();
            map.setZoom(20);
            map.setCenter(marker.getPosition());
            window.setTimeout(function() {map.setZoom(pos);},10000);
        });

    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_dfqKrCLCARDGVnYEo_pWYmRaOyM2Da0&callback=myMap"></script>
<!--=========== END GOOGLE MAP SECTION ================-->
@endif

@endsection
