<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    {{$brand->name}}
@endsection

@section('main_content')
    <div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>{{$brand->name}}</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            <li>{{$brand->name}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--shop page content section start-->
    <section class="ssp-product-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    @include('frontend.common.product_sidebar')
                </div>
                <div class="col-md-9">
                    <div class="ssp-products">
                        <div class="row">
                            @if(count($product_list) > 0)
                                @foreach($product_list as $row)
                                    <div class="col-md-4 col-6">
                                    @include('frontend.common.product_item')
                                    </div>
                                @endforeach
                                <div class="col-md-12">
                                    {{ $product_list->links() }}
                                </div>
                            @else
                                <div class="col-md-12 text-center">
                                    <h3>Not Found...</h3>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--shop page content section end-->
@endsection