<a href="{{ url('/shop/'.$row->slug) }}">
    <div class="single-product">
        <div class="product-thumb">
            @if(!empty($row->logo))
                <img src="{{ asset('public/uploads/store/'.$row->logo) }}" style="border-radius: 4px;" class="img-fluid" alt="{{ $row->store_name }}" />
            @else
                <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid" style="opacity: 0.05;">
            @endif
        </div>
        <div class="product-info text-center">
            <h5>{{ $row->store_name }}</h5>
            <p style="margin: 0;font-weight: 900;">{{ $row->districts_name }}</p>
        </div>
    </div>
</a>
