@if(count($service_list) > 0)
@foreach($service_list as $row)
    <div class="col-md-4 col-6">
        <a href="{{ url('/service/'.$row->id.'/'.$row->service_slug) }}">
            <div class="single-product service-item">
                @if(!empty($row->service_image))
                <div class="product-thumb">
                    <img src="{{ asset('public/uploads/services/'.$row->service_image) }}"
                            class="img-fluid" alt="{{ $row->service_name }}">
                </div>
                @endif
                <div class="product-info text-center">
                    <h5>{{ $row->service_name }}</h5>
                    <!--<p class="weight">1 kg</p>-->
                    <p class="price">৳ <strong>{{ number_format($row->price, 2) }}</strong></p>
                    <p class="cart-btn"><i
                                class="fa fa-eye"></i> view details</p>

                </div>
            </div>
        </a>
    </div>
@endforeach
@else
<div class="col-md-12 text-center">
    <h3>Service Not Found...</h3>
</div>
@endif