<?php $layout_file = 'frontend.' . $site_settings->theme . '.layout'; ?>
@extends($layout_file)
@section('title')
    {{ $shop->store_name }}
@endsection

@section('main_content')
@section('meta_content')
    <meta name="description" content="{{$shop->address}}" />
    <meta property="og:title" content="{{ $shop->store_name }}" />
    <meta property="og:url" content="{{ url('/shop/'.$shop->slug) }}" />
    <meta property="og:site_name" content="{{ url('/') }}" />
    <meta property="og:description" content="{{$shop->address}}" />
    <meta property="og:image" content="{{ asset('public/uploads/store/'.$shop->baner_image) }}" />
    <!-- <meta property="og:image:width" content="480">
    <meta property="og:image:height" content="360"> -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{ url('/') }}">
    <meta name="twitter:title" content="{{ $shop->store_name }}">
    <meta name="twitter:description" content="{{$shop->address}}">
    <meta name="twitter:image" content="{{ asset('public/uploads/store/'.$shop->baner_image) }}">
    <link rel="canonical" href="{{ url('/shop/'.$shop->slug) }}">
@endsection
    <?php
        if(isset($shop->logo)) {
            $shop_logo = asset('public/uploads/store/'.$shop->logo);
        } else {
           $shop_logo = asset('frontend_assets/images/shop_logo.png');
        }
    ?>
    <!--shop page content section start-->
    <section class="ssp-hero-wrapper section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="ssp-hero">
                        <div class="ssp-hero-thumb shop-category-tab baner-video">
                        @if($shop->video_link)
                            <div class="tab-menu shop-details">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li>
                                        <a class="active" data-toggle="tab" href="#baner_image" role="tab"  aria-selected="true">Shop Baner</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#shop_video" role="tab" aria-selected="true">Shop Video</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="baner_image" role="tabpanel" aria-labelledby="baner_image-tab">
                                    <img src="{{ asset('public/uploads/store/'.$shop->baner_image) }}" alt="" class="w-100">
                                </div>
                                <div class="tab-pane fade" id="shop_video" role="tabpanel" aria-labelledby="shop_video-tab">
                                    <iframe src="https://www.youtube.com/embed/{{$shop->video_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                        @else
                            <img src="{{ asset('public/uploads/store/'.$shop->baner_image) }}" alt="" class="w-100">
                        @endif
                            
                        </div>
                        <div class="ssp-hero-info">
                            <div class="shop-logo-name">
                                <div class="shop-logo">
                                    <img src="{{ $shop_logo }}" alt="{{ $shop->store_name }}">
                                </div>
                                <div class="shop-name">
                                    <h3>{{ $shop->store_name }}</h3>
                                </div>
                            </div>
                            <p title="owner name"><i class="material-icons">person</i> {{ Helpers::storePropiterName($shop->id) }}</p>
                            @if($shop->show_phone_number == 1)
                            <p><i class="material-icons">phone</i> {{ $shop->phone }}</p>
                            @endif
                            <p><i class="material-icons">location_on</i> {{ $shop->address }}</p>
                            {{-- @if($shop->home_delivery != 'No')
                            <p><i class="material-icons">monetization_on</i> In District: ৳ {{$shop->shipping_cost }}</p>
                            <p><i class="material-icons">monetization_on</i> Out District:  ৳ {{ $shop->other_shipping_cost }} </p>
                            @endif --}}

                            @if(!empty( $shop->opening_time))
                            <p><i class="material-icons">access_alarms</i> {{ $shop->opening_time }}</p>
                            @endif
                            @if(!empty( $shop->close_day))
                            <p><i class="material-icons">close</i> {{$shop->close_day}} Off Day</p>
                            @endif
                            @if(!empty($shop->home_delivery))
                            <p><i class="material-icons">home</i> Home delivery : {{ $shop->home_delivery }}</p>
                            @endif
                            <div class="call-us-btn">
                                <a target="_blank" href="{{url('shop_map_view/'.$shop->slug)}}" title="Shop Location Map"><i class="material-icons">location_on</i> Map</a>
                                <a href="tel:{{ $shop->phone }}" title="Call Us"><i class="material-icons">phone</i> Call</a>
                                <a href="#" class="add_to_favourite" data-id="{{$shop->id}}" data-type="shop" title="Add Favourit"><i class="fa fa-heart"></i> Favourite</a>
                            </div>
                        </div>
                    </div>
                    <div class="social-share" style="overflow: hidden; width: 100%">
                    <div class="share-items clearfix">
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b61cf9c0ea30730"></script>

                                <!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox" data-url="{{url('shop/'.$shop->slug)}}" data-title="{{$shop->store_name}}" data-description="{{$shop->address}}" style="clear: both;"><div id="atstbx" class="at-resp-share-element at-style-responsive addthis-smartlayers addthis-animated at4-show" aria-labelledby="at-3ae9c96f-60bb-4fba-bfc4-0275552cbefe" role="region"><span id="at-3ae9c96f-60bb-4fba-bfc4-0275552cbefe" class="at4-visually-hidden">AddThis Sharing Buttons</span><div class="at-share-btn-elements"><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-facebook" style="background-color: rgb(59, 89, 152); border-radius: 0px;"><span class="at4-visually-hidden">Share to Facebook</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1" class="at-icon at-icon-facebook" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-facebook-1">Facebook</title><g><path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-twitter" style="background-color: rgb(29, 161, 242); border-radius: 0px;"><span class="at4-visually-hidden">Share to Twitter</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2" class="at-icon at-icon-twitter" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-twitter-2">Twitter</title><g><path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-whatsapp" style="background-color: rgb(77, 194, 71); border-radius: 0px;"><span class="at4-visually-hidden">Share to WhatsApp</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-whatsapp-3" class="at-icon at-icon-whatsapp" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-whatsapp-3">WhatsApp</title><g><path d="M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-email" style="background-color: rgb(132, 132, 132); border-radius: 0px;"><span class="at4-visually-hidden">Share to Email</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-email-4" class="at-icon at-icon-email" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-email-4">Email</title><g><g fill-rule="evenodd"></g><path d="M27 22.757c0 1.24-.988 2.243-2.19 2.243H7.19C5.98 25 5 23.994 5 22.757V13.67c0-.556.39-.773.855-.496l8.78 5.238c.782.467 1.95.467 2.73 0l8.78-5.238c.472-.28.855-.063.855.495v9.087z"></path><path d="M27 9.243C27 8.006 26.02 7 24.81 7H7.19C5.988 7 5 8.004 5 9.243v.465c0 .554.385 1.232.857 1.514l9.61 5.733c.267.16.8.16 1.067 0l9.61-5.733c.473-.283.856-.96.856-1.514v-.465z"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-compact" style="background-color: rgb(255, 101, 80); border-radius: 0px;"><span class="at4-visually-hidden">Share to More</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-addthis-5" class="at-icon at-icon-addthis" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-addthis-5">AddThis</title><g><path d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z" fill-rule="evenodd"></path></g></svg></span></a></div></div></div>



                    <!-- Share items end -->



                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="shop-category-tab section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if($shop->shop_type == 'both')
                    <div class="tab-menu shop-details">
                        <ul class="nav nav-tabs" id="category_shop_tab" role="tablist">
                            <li>
                                <a class="active" id="products-tab" data-toggle="tab" href="#products_tab" role="tab" aria-controls="products_tab" aria-selected="true">Our Products</a>
                            </li>
                            <li>
                                <a id="services-tab" data-toggle="tab" href="#services_tab" role="tab" aria-controls="services_tab" aria-selected="true">Our Services</a>
                            </li>
                        </ul>
                    </div>
                    @endif
                    <div class="tab-content shop-list-section" id="shop_products_services">
                        @if($shop->shop_type == 'both')
                            <div class="tab-pane fade show active" id="products_tab" role="tabpanel" aria-labelledby="products-tab">
                                @include('frontend.common.shop_product_tab')
                            </div>
                            <div class="tab-pane fade" id="services_tab" role="tabpanel" aria-labelledby="services-tab">
                                @include('frontend.common.shop_service_tab')
                            </div>
                        @endif
                        @if($shop->shop_type == 'shop')
                            <div class="tab-pane fade show active" id="products_tab" role="tabpanel" aria-labelledby="products-tab">
                                @include('frontend.common.shop_product_tab')
                            </div>
                        @endif
                        @if($shop->shop_type == 'service')
                            <div class="tab-pane fade show active" id="services_tab" role="tabpanel" aria-labelledby="services-tab">
                                @include('frontend.common.shop_service_tab')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--shop page content section end-->
    <script>
        $(document).ready(function () {
            function load_shop_products(category_id = '', category = '', keyword = '') {
                var url = APP_URL + '/get_shop_products';
                var shopId = '{{ $shop->id }}';
                var loading = '<div class="col-md-12 text-center"><h4>Loading...</h4></div>';
                $('#load_shop_products').html(loading);
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {shop_id: shopId, category_id: category_id, category: category, keyword: keyword},
                    cache: false,
                    success: function (response) {
                        $('#load_shop_products').html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            function load_shop_services(category_id = '', category = '', keyword = '') {
                var url = APP_URL + '/get_shop_services';
                var shopId = '{{ $shop->id }}';
                var loading = '<div class="col-md-12 text-center"><h4>Loading...</h4></div>';
                $('#load_shop_services').html(loading);
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {shop_id: shopId, category_id: category_id, category: category, keyword: keyword},
                    cache: false,
                    success: function (response) {
                        $('#load_shop_services').html(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            load_shop_products();
            load_shop_services();

            $('.shop_category_item').click(function (e) {
                
                e.preventDefault();
                $('.sidenav').sidenav('close');
                var category_name = $(this).text();
                var categoryId = $(this).data('categoryid');
                var category = $(this).data('category');
                $('.cat_name').html(category_name);
                load_shop_products(categoryId, category);
            });

            $('#shop_product_search').keyup(function () {
                var keyword = $(this).val();
                if(keyword == '') {
                    $('.cat_name').html('All Products');
                    load_shop_products();
                } else {
                    $('.cat_name').html('Search By: '+keyword);
                    load_shop_products('', '', keyword);
                }
            });

            $('.service_category_item').click(function (e) {
                
                e.preventDefault();
                $('.sidenav').sidenav('close');
                var category_name = $(this).text();
                var categoryId = $(this).data('categoryid');
                var category = $(this).data('category');
                $('.service_cat_name').html(category_name);
                load_shop_services(categoryId, category);
            });

            $('#shop_service_search').keyup(function () {
                var keyword = $(this).val();
                if(keyword == '') {
                    $('.service_cat_name').html('All Products');
                    load_shop_services();
                } else {
                    $('.service_cat_name').html('Search By: '+keyword);
                    load_shop_services('', '', keyword);
                }
            });

            $('.mobile-sidebar .ssp-sidebar ul li a i.submenu_show').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                $(this).toggleClass('fa-angle-down');
                $('#category_id_'+id).toggleClass('show');
            });
        });
    </script>
@endsection