<ul class="cart p-2">
    <li class="text-capitalize p-2">total items<span class="float-right">{{$cart_item->count()}} items</span></li>
    <li class="text-capitalize p-2">total Amount<span class="float-right">৳ {{number_format($total_amount, 2)}}</span></li>
    <?php $total_shipping_cost = Helpers::get_total_shipping_cost() ?>
    <li class="text-capitalize p-2">Shipping cost <span class="float-right">৳ {{number_format($total_shipping_cost, 2)}}</span></li>
    {{--<li class="mt-3 mb-3">--}}
        {{--<div class="coupon-form">--}}
            {{--<form action="#">--}}
                {{--<div class="d-flex">--}}
                    {{--<input type="text" class="form-control border m-0 mr-1 pl-2" placeholder="Enter Voucher Code" style="height: initial;">--}}
                    {{--<button type="submit" class="btn btn-primary m-0">Apply</button>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</div>--}}
    {{--</li>--}}
    <?php $grand_total = ($total_amount + $total_shipping_cost); ?>
    <li class="text-capitalize p-2">grand total<span class="float-right">৳ {{number_format($grand_total, 2)}}</span></li>
    <div class="text-center">
        <a href="{{url('/checkout')}}" class="w-100 waves-effect waves-light btn-small white-text mt-4 font-weight-bold z-depth-1 rounded">proceed to checkout</a>
    </div>
</ul>