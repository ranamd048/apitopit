<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Blog List
@endsection

@section('main_content')
<!--breadcrumb area start-->
<div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs text-center">
                    <h1>Blog List</h1>
                    <ul>
                        <li><a href="{{url('/')}}">home</a></li>
                        <li>/</li>
                        <li>Blog List Page</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--breadcrumb area end-->
<?php if(count($blog_list) > 0) : ?>
<!-- blog-area start -->
<div class="blog-area section-padding">
    <div class="container">
        <div class="row">
            <?php foreach($blog_list as $row) : ?>
            <div class="col-md-4">
                <div class="single-post">
                    <div class="post-thumb">
                        <img src="{{ asset('public/uploads/blog/'.$row->image_url) }}" alt="">
                        <div class="overlay on-hover"></div>
                        <!--<div class="post-date">
                            <p>feb <span>21</span></p>
                        </div>-->
                    </div>
                    <div class="post-content-area">
                        <div class="post-meta">
                            <ul>
                                <li><i class="fa fa-user"></i>{{$row->created_by}}</li>
                                <li><i class="fa fa-calendar"></i><?php
                                    $date = new DateTime($row->created_at);
                                    echo $date->format('M d Y'); ?></li>
                            </ul>
                        </div>
                        <div class="post-title">
                            <a href="<?php echo url('blog_details/'.$row->id); ?>"><h3><?php echo $row->title; ?></h3></a>
                        </div>
                        <div class="post-excerpt">
                            <p>
                                <?php echo mb_substr(strip_tags($row->description), 0, 110); ?><a href="<?php echo url('blog_details/'.$row->id); ?>"> read more</a>
                            </p>
                        </div>
                    </div>
                    <!-- <div class="theme-btn float-right">
                         <a href="blog-details.html">read more</a>
                     </div>-->
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{$blog_list->links()}}
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<!-- blog-area end -->
@endsection