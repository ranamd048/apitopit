@if(count($product_list) > 0)
    @foreach($product_list as $row)
        <?php
        $check_promo = Helpers::is_promo_product($row->id);
        ?>
        <div class="col-md-4 col-6">

            <a href="{{ url('/product/'.$row->id.'/'.$row->product_slug) }}">
                <div class="single-product">
                    @if($row->is_used == 1)
                    <div class="used-product-badge">
                        used
                    </div>
                    @endif
                    @if(Helpers::showProductStock($row->store_id))
                    <?php echo Helpers::productStockBadge($row->id); ?>
                    @endif
                    @if(!empty($row->product_image))
                    <div class="product-thumb">
                        <img src="{{ asset('public/uploads/products/thumbnail/'.$row->product_image) }}"
                             class="img-fluid" alt="{{ $row->product_name }}">
                    </div>
                    @endif
                    <div class="product-info text-center">
                        <h5>{{ $row->product_name }}</h5>
                        <!--<p class="weight">1 kg</p>-->
                        @if($check_promo)
                            <?php $product_price = $row->promotion_price; ?>
                            @if(Helpers::showProductPrice($row->store_id) && $row->show_per_price == 1)
                            <p class="price">৳ <strong>{{ $row->promotion_price }}</strong> 
                                <span class="ml-2 red-text"><del>৳ {{ $row->price }}</del></span> / {{Helpers::unitName($row->unit_id)}}
                            </p>
                            @endif
                        @else
                            <?php $product_price = $row->price; ?>
                            @if(Helpers::showProductPrice($row->store_id) && $row->show_per_price == 1)
                            <p class="price">৳ <strong>{{ $row->price }}</strong> / {{Helpers::unitName($row->unit_id)}}</p>
                            @endif
                        @endif
                        @if(Helpers::showProductPrice($row->store_id) && $row->show_per_price == 1)
                        <div class="wishlist-cart-btn">
                            <p class="cart-btn add_to_cart" data-id="{{$row->id}}" data-shop_slug="NO" data-price="{{$product_price}}"><i class="fa fa-shopping-bag"></i> Add</p>
                            @if($private_settings->wishlist_option == 1)
                            <p class="wishlist-btn add_to_wishlist" data-productid="{{$row->id}}"><i class="fa fa-heart"></i> wishlist</p>
                            @else
                            <p class="wishlist-btn add_to_favourite" data-id="{{$row->id}}" data-type="product"><i class="fa fa-heart"></i></p>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
            </a>
        </div>
    @endforeach
@else
    <div class="col-md-12 text-center">
        <h3>Product Not Found...</h3>
    </div>
@endif