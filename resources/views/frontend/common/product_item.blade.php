<?php
    $check_promo = Helpers::is_promo_product($row->id);
?>
<a href="{{ url('/product/'.$row->id.'/'.$row->product_slug) }}">
    <div class="single-product">
        @if($private_settings->product_hover == 1)
        <div class="product-useful-info">
            <p>{{$row->store_name}}</p>
            <p>{{Helpers::districtName($row->district)}}</p>
        </div>
        @endif
        @if($row->is_used == 1)
        <div class="used-product-badge">
            used
        </div>
        @endif
        @if($row->show_stock == 1)
        <?php echo Helpers::productStockBadge($row->id); ?>
        @endif

        <div class="product-thumb">
            @if(!empty($row->product_image))
                <img src="{{ asset('public/uploads/products/thumbnail/'.$row->product_image) }}" class="img-fluid" alt="{{ $row->product_name }}" />
            @else
                <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" style="opacity: 0.05;" alt="" class="img-fluid">
            @endif
        </div>
        <div class="product-info text-center">
            <h5>{{ $row->product_name }}</h5>
            <!--<p class="weight">1 kg</p>-->
            @if($check_promo)
                <?php $product_price = $row->promotion_price; ?>
                @if($row->show_price == 1 && $row->show_per_price == 1)
                <p class="price">৳ <strong>{{ $row->promotion_price }}</strong>
                    <span class="ml-2 red-text"><del>৳ {{ $row->price }}</del></span> / {{Helpers::unitName($row->unit_id)}}
                </p>
                @endif
            @else
                <?php $product_price = $row->price; ?>
                @if($row->show_price == 1 && $row->show_per_price == 1)
                <p class="price">৳ <strong>{{ $row->price }}</strong> / {{Helpers::unitName($row->unit_id)}}</p>
                @endif
            @endif
            <div class="wishlist-cart-btn">
            @if($row->show_price == 1 && $row->show_per_price == 1)
                <p class="cart-btn add_to_cart" data-id="{{$row->id}}" data-shop_slug="{{$private_settings->multiple_shop_cart == 0 ? $row->slug : 'NO'}}" data-price="{{$product_price}}"><i
                            class="fa fa-shopping-bag"></i> Add</p>
                @if($private_settings->wishlist_option == 1)
                <p class="wishlist-btn add_to_wishlist" data-productid="{{$row->id}}"><i class="fa fa-heart"></i> wishlist</p>
                @else
                <p class="wishlist-btn add_to_favourite" data-id="{{$row->id}}" data-type="product"><i class="fa fa-heart"></i></p>
                @endif
            @endif
                <div class="district_name">
                <p>{{$row->store_name}} / {{Helpers::districtName($row->district)}}</p>
                </div>
            </div>

        </div>
    </div>
</a>
