<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Shop Registration
@endsection

@section('main_content')
    <style>
        .section-title.styled {
            margin-bottom: 40px;
        }
        .section-title.styled h2 {
            color: #fff;
        }
        i.toggle-view-password {
            position: absolute;
            bottom: 15px;
            right: 10px;
            cursor: pointer;
        }
    </style>
    <!--registration section start-->
    <section class="register-section section-padding mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                    <div class="form-area z-depth-1 p-3 rounded">
                        <div class="section-title styled text-center">
                            <h2 class="text-capitalize mt-3">Shop registration</h2>
                        </div>
                        <div class="l-form pl-3 pr-3">

                            @if($errors->any())
                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach ($errors->all() as $error)
                                            <p class="alert alert-danger">{{$error}}</p>
                                        @endforeach
                                    </div>
                                </div>
                                <br>
                            @endif
                            @if(Session::has('message'))
                                <p class="alert alert-success">{{ Session::get('message') }}</p><br>
                            @endif
                            <form action="{{ url('/store_registration') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <?php $col = '12'; ?>
                                    @if(count($shop_categories) > 0)
                                    <?php $col = '6'; ?>
                                    <div class="col-md-{{$col}}">
                                        <select name="shop_category" class="validate" required="">
                                            <option value="">Select Shop Category</option>
                                            @foreach ($shop_categories as $row)
                                                <option {{ (old("shop_category") == $row->id ? "selected":"") }} value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        </select>
                                        <label>Shop Category *</label>
                                    </div>
                                    @endif
                                    <div class="col-md-{{$col}}">
                                        <select name="shop_type" class="validate" required="">
                                            <option {{ (old("shop_type") == "shop" ? "selected":"") }} value="shop">Shop</option>
                                            <option {{ (old("shop_type") == "service" ? "selected":"") }} value="service">Service</option>
                                            <option {{ (old("shop_type") == "both" ? "selected":"") }} value="both">Shop & Service</option>
                                        </select>
                                        <label>Shop Type *</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-field">
                                            <input id="shop_name" name="store_name" type="text" class="validate" required="" value="{{ old('store_name') }}">
                                            <label for="shop_name">Shop Name *</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="file-field input-field">
                                            <div class="btn">
                                                <span>File</span>
                                                <input type="file" name="shop_logo">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path" type="text"
                                                       placeholder="Upload Shop Logo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="file-field input-field">
                                            <div class="btn">
                                                <span>File</span>
                                                <input type="file" name="baner_image" class="validate" required="">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text"
                                                       placeholder="Upload Shop Baner Image *">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <input id="name" name="name" type="text" class="validate" required="" value="{{ old('name') }}">
                                            <label for="name">Your Name *</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <input id="address" name="address" type="text" class="validate" required="" value="{{ old('address') }}">
                                            <label for="address">Shop Address *</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-field">
                                            <select name="division" id="division_list" class="validate">
                                                <option value="">Select Division *</option>
                                                @foreach($divisions as $row)
                                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-field">
                                            <select name="district" class="validate"  id="district_list">
                                                <option value="">Select District *</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-field">
                                            <select name="upazila" class="validate" id="upazila_list">
                                                <option value="">Select Upazila *</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-field">
                                            <input id="map_lat" name="map_lat" type="text" value="{{ old('map_lat') }}">
                                            <label for="map_lat" class="latlong">Latitude </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-field">
                                            <input id="map_long" name="map_long" type="text" value="{{ old('map_long') }}">
                                            <label for="map_long" class="latlong">Longitude </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 ">
                                        <button onclick="getLocation(event)" class="btn btn-primary" style="margin-top: 20px;font-size:14px">Use Current Location</button>
                                        <div id="demo"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <input id="email" name="email" type="email" value="{{ old('email') }}">
                                            <label for="email">Email</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <input id="phone" name="phone" type="number" class="validate" required="" value="{{ old('phone') }}">
                                            <label for="phone">Phone Ex:01779221842 *</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <input type="text" name="opening_time" id="opening_time" value="{{ old('opening_time') }}">
                                            <label for="opening_time">Opening/Closing Time</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <select name="close_day[]" id="close_day" multiple>
                                                <option value="Friday">Friday</option>
                                                <option value="Sataurday">Sataurday</option>
                                                <option value="Sunday">Sunday</option>
                                                <option value="Monday">Monday</option>
                                                <option value="Tuesday">Tuesday</option>
                                                <option value="Wednesday">Wednesday</option>
                                                <option value="Thursday">Thursday</option>
                                            </select>
                                            <label for="close_day">Select Close Days</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <i toggle="#password" class="toggle-view-password fa fa-eye"></i>
                                            <input id="password" name="password" type="password" class="validate" required="">
                                            <label for="password">Password *</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <i toggle="#confirm_password" class="toggle-view-password fa fa-eye"></i>
                                            <input id="confirm_password" name="password_confirmation" type="password" class="validate" required="">
                                            <label for="confirm_password">Confirm Password *</label>
                                        </div>
                                        <p>
                                            <label>
                                                <input type="checkbox" id="check_terms_condition" class="filled-in" checked="checked" />
                                                <span>Are you agree to accept <a target="_blank" href="{{url('page/terms-and-condition')}}">Terms & Condition ?</a></span>
                                            </label>
                                        </p>
                                    </div>

                                    <div class="col-12 mt-4">
                                        <div class="text-right w-100">
                                            <a href="{{ url('/') }}"
                                               class="waves-effect waves-light btn-large left white-text">Home
                                                <i class="material-icons left"
                                                   style="-webkit-transform: rotate(180deg);-moz-transform: rotate(180deg);-ms-transform: rotate(180deg);-o-transform: rotate(180deg);transform: rotate(180deg);">send</i></a>
                                            <button id="shop_registration_btn" class="btn btn-large waves-effect waves-light" type="submit"
                                                    name="action">
                                                register
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--registration section end-->
    <script>
        var x = document.getElementById("demo");

        function getLocation(e) {
            e.preventDefault();
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else { 
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) { 
            $(".latlong").addClass("active");
            $("#map_lat").val(position.coords.latitude);
            $("#map_long").val(position.coords.longitude);
        } 
    
        $(document).ready(function() {
            $('#check_terms_condition').change(function() {
                if ($('#check_terms_condition').is(":checked")) {
                    $('#shop_registration_btn').prop("disabled", false);
                } else {
                   $('#shop_registration_btn').prop("disabled", true);
                }
            });

            $("i.toggle-view-password").click(function() {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));

                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
        });
    </script>
@endsection