<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    {{$page_data->title}}
@endsection

@section('main_content')
    <div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>{{$page_data->title}}</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            <li>{{$page_data->title}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- cart view start-->
    <section class="custom-page-section section-padding mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            {{$page_data->title}}
                        </div>
                        <div class="card-body">
                            {!! html_entity_decode($page_data->page_content) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cart view end-->
@endsection