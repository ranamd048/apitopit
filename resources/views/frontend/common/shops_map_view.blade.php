<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    All Shop Map View
@endsection
@section('custom_scripts')
<script src="http://maps.google.com/maps/api/js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
@endsection

@section('main_content')
<style type="text/css">
    	#mymap {
      		border:1px solid red;
      		width: 100%;
      		height: 500px;
    	}
  	</style>
<div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs text-center">
                    <h1>Shop Map</h1>
                    <ul>
                        <li><a href="{{url('/')}}">home</a></li>
                        <li>/</li>
                        <li>All Shop Map View</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="mymap"></div>


  <script type="text/javascript">
    $(document).ready(function() {
        var locations = <?php print_r(json_encode($shop_locations)) ?>;
        //console.log(locations)
        var mymap = new GMaps({
            el: '#mymap',
            lat: 23.6850,
            lng: 90.3563,
            zoom:7
        });

        $.each( locations, function( index, value ){
            mymap.addMarker({
            lat: value.map_lat,
            lng: value.map_long,
            title: value.store_name,
            click: function(e) {
                alert('This is '+value.store_name+', Shop');
            }
            });
        });
    });
  </script>
@endsection