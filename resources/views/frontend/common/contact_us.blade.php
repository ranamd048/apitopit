<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Contact Us
@endsection

@section('main_content')
<!--breadcrumb area start-->
<div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs text-center">
                    <h1>Contact Us</h1>
                    <ul>
                        <li><a href="{{url('/')}}">home</a></li>
                        <li>/</li>
                        <li>Contact Us Page</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--breadcrumb area end-->

<!--contact section start-->
<section class="contact-us-section">
    <div class="contact-form">
        <form>
            <div class="text-center">
                <h3 class="text-capitalize text-white mb-3">Contact Us</h3>
            </div>
            <input type="hidden" id="system_email" value="{{$site_settings->email}}">
            <div class="input-field">
                <input id="name" type="text" class="validate">
                <label for="name">Your Name *</label>
            </div>
            <div class="input-field">
                <input id="email" type="email" class="validate">
                <label for="email">Your Email *</label>
            </div>
            <div class="form-group">
                <textarea name="message" id="message" class="form-control" cols="10" rows="5"
                          placeholder="Your Message... *"></textarea>
            </div>
            <button type="submit" id="send_msg" class="btn float-right">Send</button>
        </form>
    </div>
    <div class="gmap">
        <iframe src="{{$site_settings->maps_iframe}}" frameborder="0" style="border: 0;" allowfullscreen=""></iframe>
    </div>
</section>
<!--contact section end-->

@endsection