<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Shop List
@endsection

@section('main_content')
    <div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>Shop List</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            <li>Shop List Page</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $division_list = Helpers::getDivisionList(); ?>
    <!--shop list section start-->
    <section class="shop-list-section section-padding">
        <div class="container">
            <div class="shop-lists">
                <div class="row">
                    <div class="col-md-12">
                        <div class="shop-search">
                            <form action="{{ url('shop_list') }}" method="GET">
                                <div class="row">
                                    <div class="col-md-3 more_filter" style="display: none;">
                                        <div class="input-field">
                                            <input type="text" name="store_name" class="form-control" placeholder="Enter Shop Name" />
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-field">
                                            <select name="division" id="division_list" class="validate">
                                                <option value="">Search by Division</option>
                                                @foreach($division_list as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-field">
                                            <select name="district" class="validate"  id="district_list">
                                                <option value="">Search by District</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-field">
                                            <select name="upazila" class="validate" id="upazila_list">
                                                <option value="">Search by Upazila</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 more_filter" style="display: none;">
                                        <div class="input-field">
                                            <select name="shop_category_id" id="shop_category_id" class="validate">
                                                <option value="">Search by Category</option>
                                                @foreach($category_list as $row)
                                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 more_filter" style="display: none;">
                                        <div class="input-field">
                                            <select name="shop_type" class="validate"  id="shop_type">
                                                <option value="">Search by Shop Type</option>
                                                <option value="shop">Shop</option>
                                                <option value="service">Service</option>
                                                <option value="both">Shop &amp; Service</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3" style="align-items: center;display: inline-flex;">
                                        <button class="btn btn-md waves-effect waves-light" type="submit" style="margin-right: 6px;">
                                            Search
                                            <i class="material-icons right">search</i>
                                        </button>

                                        <button class="btn btn-sm waves-effect waves-light" id="more_filter_btn" type="button">
                                            More
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @if(count($result) > 0)
                        <div class="shop-search-result">
                            <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                @if(isset($result['division']))
                                <li class="breadcrumb-item">{{Helpers::divisionName($result['division'])}}</li>
                                @endif
                                @if(isset($result['district']))
                                <li class="breadcrumb-item">{{Helpers::districtName($result['district'])}}</li>
                                @endif
                                @if(isset($result['thana']))
                                <li class="breadcrumb-item">{{Helpers::upazilaName($result['thana'])}}</li>
                                @endif
                            </ol>
                            </nav>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    @if(count($shop_list) > 0)
                        @foreach($shop_list as $row)
                            <?php
                            if(isset($row->logo)) {
                                $shop_logo = asset('public/uploads/store/'.$row->logo);
                            } else {
                                $shop_logo = asset('frontend_assets/images/shop_logo.png');
                            }
                            ?>
                            <div class="col-lg-2 col-md-3 col-6">
                                <div class="single-shop-list white z-depth-1 p-2">
                                    <a href="{{ url('/shop/'.$row->slug) }}">
                                        <div class="shop-thumb">
                                            <img src="{{ $shop_logo }}"
                                                 alt="{{ $row->store_name }}" class="img-fluid">
                                        </div>
                                        <div class="shop-name">
                                            <h5>{{ $row->store_name }}</h5>
                                        </div>
                                    </a>
                                    <div class="shop-district_name">
                                        <span class="badge badge-success">
                                            {{Helpers::districtName($row->district)}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                            <div class="col-md-12">
                                {{ $shop_list->links() }}
                            </div>
                    @else
                        <div class="col-md-12 text-center">
                            <h3>Not Found...</h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!--shop list section end-->
@endsection

@section('footer_scripts')
<script>
    $(function () {
        $('#more_filter_btn').click(function () {
            $('.more_filter').slideDown();
            $(this).hide();
        });
    });
</script>
@stop
