<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    {{$blog->title}}
@endsection

@section('main_content')
<style>
    .top-rated-text {
        margin-top: -12px;
    }
    .top-rated-img img {
        width: 100px;
    }
    .sidebar-widget.sidebar-overflow {
        overflow: hidden;
    }
    .product-size li {
        display: inline-block;
        margin-right: 16px;
    }
    .product-size li a {
        color: #888888;
        font-size: 15px;
        font-weight: 600;
        text-transform: uppercase;
    }
    .product-size li a:hover {
        color: var(--theme-color);
    }
    .single-top-rated {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }
    .top-rated-text > h4 {
        color: #6c6c6c;
        font-size: 14px;
        font-weight: 600;
        margin: 0;
    }
    .top-rated-rating li {
        display: inline-block;
    }
    .top-rated-rating li {
        display: inline-block;
        margin-right: 5px;
    }
    .top-rated-rating li i {
        color: #000000;
        font-size: 16px;
    }
    .top-rated-rating li i.reting-color {
        color: var(--theme-color);
        font-size: 16px;
    }
    .top-rated-text > span {
        color: #8b8b8b;
        font-weight: 600;
    }
    .top-rated-text {
        margin-left: 20px;
    }
    .top-rated-rating {
        line-height: 1;
        margin: 9px 0 10px;
    }
    .sidebar-load {
        padding-top: 7px;
    }
    .blog-area.section-padding {
        overflow: hidden;
    }
    .sidebar-top-rated {
        margin-bottom: 30px;
    }
    h3.sidebar-title {
        font-size: 25px;
        margin-top: 5px;
    }
    .blog-sidebar {
        background: #fff;
        padding: 10px;
        overflow: hidden;
    }
    .blog-details-style {
        background: #fff;
    }

    .blog-part img {
        width: 100%;
        height: auto;
        margin-bottom: 20px;
    }
    .blog-info-details {
        padding: 15px;
    }
    .blog-info-details h3 {
        margin-top: 0;
        font-size: 30px;
    }
</style>
<!--breadcrumb area start-->
<div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs text-center">
                    <h1>Blog Details</h1>
                    <ul>
                        <li><a href="{{url('/')}}">home</a></li>
                        <li>/</li>
                        <li><?php echo $blog->title; ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--breadcrumb area end-->

<!-- blog-area start -->
<div class="blog-area section-padding">
    <div class="container">
        <div class="row">

            <div class="col-lg-8">
                <div class="blog-details-style">
                    <div class="blog-part">
                        <img src="{{ asset('public/uploads/blog/'.$blog->image_url) }}" alt="news">
                        <div class="blog-info-details mt-20">
                            <h3><?php echo $blog->title; ?></h3>
                            <?php echo $blog->description; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog-sidebar">

                    <div class="sidebar-widget mb-50">
                        <h3 class="sidebar-title">Recent Posts</h3>
                        <div class="sidebar-top-rated-all">
                            <?php foreach($latest_blog as $row) : ?>
                            <div class="sidebar-top-rated mb-30">
                                <div class="single-top-rated">
                                    <div class="top-rated-img">
                                        <a href="<?php echo url('blog_details/'.$row->id) ?>"><img src="{{ asset('public/uploads/blog/'.$row->image_url) }}" alt=""></a>
                                    </div>
                                    <div class="top-rated-text">
                                        <span><?php
                                            $date = new DateTime($row->created_at);
                                            echo $date->format('M d, Y'); ?></span>
                                        <h4><a href="<?php echo url('blog_details/'.$row->id) ?>"><?php echo $row->title; ?></a></h4>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog-area end -->
@endsection