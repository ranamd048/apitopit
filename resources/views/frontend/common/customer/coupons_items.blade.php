<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
 Coupon Code List
@endsection

@section('main_content')
    <style> .badge {color: #fff !important;} </style>
    <section class="customer-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h3>Coupon Code List</h3>
                            @if(count($coupons_items) > 0)
                            <table class="table table-hover">
                                <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Coupon Code</th>
                                    <th>Recive Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0; ?>
                                @foreach($coupons_items as $row)
                                    <?php
                                    $i++;
                                    $date = new DateTime($row->recive_date);
                                    ?>
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{$row->coupon_code}}</td>
                                        <td>{{$date->format('d-F-Y')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                            <p>Coupon Not Found...</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection