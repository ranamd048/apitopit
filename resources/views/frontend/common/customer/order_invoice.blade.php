<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Order Invoice
@endsection

@section('main_content')
    <style> .badge {color: #fff !important;} </style>
    <section class="customer-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h3>Order Invoice</h3>
                            <div id="responsive_table">
                            <table class="table table-hover">
                                <thead class="thead-dark">
                                <tr>
                                    <th>S/N</th>
                                    <th>Shop</th>
                                    <th>Total</th>
                                    <th>Delivery Charge</th>
                                    <th>Invoice Status</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0; ?>
                                @foreach($order_list as $row)
                                    <?php
                                    $i++;
                                    $date = new DateTime($row->created_at);
                                    if($row->invoice_status == 0) {
                                        $invoice_status = '<span class="badge badge-primary">Pending</span>';
                                    }
                                    if($row->invoice_status == 1) {
                                        $invoice_status = '<span class="badge badge-info">Processing</span>';
                                    }
                                    if($row->invoice_status == 2) {
                                        $invoice_status = '<span class="badge badge-success">Completed</span>';
                                    }
                                    if($row->invoice_status == 3) {
                                        $invoice_status = '<span class="badge badge-danger">Canceled</span>';
                                    }
                                    if($row->payment_status == 0) {
                                        $payment_status = '<span class="badge badge-danger">Due</span>';
                                    }
                                    if($row->payment_status == 1) {
                                        $payment_status = '<span class="badge badge-success">Paid</span>';
                                    }
                                    ?>
                                    <tr>
                                        <td data-title="S/N">{{ $i }}</td>
                                        <td data-title="Shop"><a target="_blank" href="{{url('shop/'.$row->slug)}}">{{$row->store_name}}</a></td>
                                        <td data-title="Total">৳ {{number_format($row->total_amount, 2)}}</td>
                                        <td data-title="Delivery Charge">৳ {{number_format($row->delivery_charge, 2)}}</td>
                                        <td data-title="Status"><?php echo $invoice_status; ?></td>
                                        <td data-title="Date">{{$date->format('d-F-Y')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection