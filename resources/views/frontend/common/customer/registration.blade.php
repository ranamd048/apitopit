<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Customer Registration
@endsection

@section('main_content')
    <style>
        .section-title.styled h2 {
            color: #fff;
        }
        div#ref_result p.matched {
            color: green;
        }
        div#ref_result p.not_matched {
            color: red;
        }
    </style>
    <!--registration section start-->
    <section class="register-section section-padding mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                    <div class="form-area z-depth-1 p-3 rounded">
                        <div class="section-title styled text-center">
                            <h2 class="text-capitalize mt-3">Customer registration</h2>
                        </div>
                        <div class="l-form pl-3 pr-3">

                            @if($errors->any())
                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach ($errors->all() as $error)
                                            <p class="alert alert-danger">{{$error}}</p>
                                        @endforeach
                                    </div>
                                </div>
                                <br>
                            @endif
                            @if(Session::has('message'))
                                <p class="alert alert-success">{{ Session::get('message') }}</p><br>
                            @endif
                            <form action="{{ url('/customer_registration') }}" method="POST" id="registration_form">
                                {{ csrf_field() }}
                                <div class="row">
                                    <?php $col_size = '12'; ?>

                                    @if($private_settings->reference_registration == 1)
                                    @if(Session::has('distributor_id'))
                                    <input type="hidden" name="reference_id" value="{{Session::get('distributor_id')}}">
                                    @else
                                        <?php $col_size = '6'; ?>
                                        <div class="col-md-6">
                                            <div class="input-field">
                                                <input id="reference_id" type="text" class="validate">
                                                <label for="reference_id">Reference</label>
                                                <div id="ref_result"></div>
                                                <input type="hidden" id="reference_id_value" name="reference_id" value="0">
                                            </div>

                                        </div>
                                    @endif
                                    @endif

                                    <div class="col-md-{{$col_size}}">
                                        <div class="input-field">
                                            <input id="name" name="name" type="text" class="validate" value="{{old('name')}}" required="">
                                            <label for="name">Your Name *</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <input id="email" name="email" type="email" value="{{old('email')}}">
                                            <label for="email">Email</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <input id="phone" name="phone" type="number" class="validate" required="" value="{{old('phone')}}">
                                            <label for="phone">Phone Ex:01779221842 *</label>
                                        </div>
                                    </div>
                                    <pre></pre>
                                    <div class="col-md-4">
                                        <div class="input-field">
                                            <select name="division" id="division_list" class="validate">
                                                <option value="">Select Division</option>
                                                @foreach($divisions as $row)
                                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-field">
                                            <select name="district" class="validate"  id="district_list">
                                                <option value="">Select District</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-field">
                                            <select name="upazila" class="validate" id="upazila_list">
                                                <option value="">Select Upazila</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-field">
                                            <input id="address" name="address" type="text" required="" class="validate" value="{{old('address')}}">
                                            <label for="address">Address *</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <select name="gender" class="validate" required="">
                                            <option value="1" {{ (old('gender') == '1') ? 'selected' : ''}}>Male</option>
                                            <option value="2" {{ (old('gender') == '2') ? 'selected' : ''}}>Female</option>
                                        </select>
                                        <label>Gender *</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <input id="password" name="password" type="password" class="validate" required="">
                                            <label for="password">Password *</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-field">
                                            <input id="confirm_password" name="password_confirmation" type="password" class="validate" required="">
                                            <label for="confirm_password">Confirm Password *</label>
                                        </div>
                                    </div>

                                    <div class="col-12 mt-4">
                                        <div class="text-right w-100">
                                            <a href="{{ url('/login') }}"
                                               class="waves-effect waves-light btn-large left white-text">Login
                                                <i class="material-icons left"
                                                   style="-webkit-transform: rotate(180deg);-moz-transform: rotate(180deg);-ms-transform: rotate(180deg);-o-transform: rotate(180deg);transform: rotate(180deg);">send</i></a>
                                            <button class="btn btn-large waves-effect waves-light" type="submit"
                                                    name="action">
                                                register
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--registration section end-->
    <script>
        $(document).ready(function() {
            // $('#registration_form').submit(function () {
            //     return false;
            // });
            <?php if(old('division')):?>
                const division_id_old = {{ old('division') }};
                const district_id_old = {{ old('district') }};
                const upazila_id_old = {{ old('upazila') }};
                if(division_id_old) {
                    $('#division_list').val(division_id_old);
                    get_selected_district_upazila_cp(division_id_old, 'district_list' , district_id_old);
                    setTimeout(() => {
                        get_selected_district_upazila_cp(district_id_old, 'upazila_list' , upazila_id_old)
                    }, 50)
                }
            <?php endif;?>

            $('#reference_id').keyup(function() {
                var reference_id = $(this).val();
                var url = "{{ url('customer/check_reference_id') }}";
                $.ajax({
                    method: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {reference_id: reference_id},
                    cache: false,
                    success: function (response) {
                        if(response == 0) {
                            $('#ref_result').html('<p class="not_matched">Reference Not matched</p>');
                            $('#reference_id_value').val('0');
                        } else {
                            $('#ref_result').html('<p class="matched">Reference matched</p>');
                            $('#reference_id_value').val(response);
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            });
        });

        function get_selected_district_upazila_cp(id, type, default_val = null) {
            var url = APP_URL + '/get_district_upazila';
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { id: id, type: type, data_format: 'json' },
                cache: false,
                success: function(response) {
                    var Options = "<option value=''>Select ...</option>";

                    $.each(response, function(i, val) {
                        if(default_val != null && val.id) {
                            Options = Options + "<option value='" + val.id + "' selected>" + val.name + "</option>";
                        } else {
                            Options = Options + "<option value='" + val.id + "'>" + val.name + "</option>";
                        }
                    });
                    $('#' + type).html(Options);
                    $("#" + type).formSelect();
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    </script>
@endsection
