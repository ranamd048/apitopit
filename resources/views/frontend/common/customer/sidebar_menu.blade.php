<style>
    .my-account-sidebar .profile-picture {
    overflow: hidden;
    text-align: center;
}
.my-account-sidebar .profile-picture img {
    width: 120px;
    height: 120px;
    border: 3px solid var(--theme-color);
    border-radius: 50%;
}
.my-account-sidebar .profile-picture h5 {
    font-size: 18px;
}
.back-button {
    float: right;
    margin-top: -50px;
    overflow: hidden;
}
</style>
<div class="list-group my-account-sidebar">
@if(!Session::has('distributor_id'))
    <?php
        if(!empty(Auth::user()->photo)) {
            $photo = asset('public/uploads/customer/'. Auth::user()->photo);
        } else {
            $photo = asset('frontend_assets/images/user_profile.png');
        }
    ?>
    <div class="profile-picture">
        <img src="{{$photo}}" alt="">
        <h5>{{Auth::user()->name}}</h5>
    </div>
    @endif
    <a href="{{url('customer_profile')}}" class="list-group-item list-group-item-action {{ (request()->is('customer_profile')) ? 'active' : '' }}">My Profile</a>
    <a href="{{url('orders')}}" class="list-group-item list-group-item-action {{ (request()->is('orders')) ? 'active' : '' }}">My Orders</a>
    <a href="{{url('my_services')}}" class="list-group-item list-group-item-action {{ (request()->is('my_services')) ? 'active' : '' }}">My Services</a>
    @if($private_settings->wishlist_option == 1)
    <a href="{{url('wishlist')}}" class="list-group-item list-group-item-action {{ (request()->is('wishlist')) ? 'active' : '' }}">Wishlist</a>
    @else
    <a href="{{url('favourites')}}" class="list-group-item list-group-item-action {{ (request()->is('favourites')) ? 'active' : '' }}">Favourites</a>
    @endif
    @if(Session::has('customer_id'))
        <a href="{{url('save_order_items')}}" class="list-group-item list-group-item-action {{ (request()->is('save_order_items')) ? 'active' : '' }}">Save Orders</a>
    @endif
    @if($private_settings->coupon_option == 1)
    <a href="{{url('coupons')}}" class="list-group-item list-group-item-action {{ (request()->is('coupons')) ? 'active' : '' }}">Coupons</a>
    @endif
    <?php $message_count = Helpers::messageCount(); ?>
    <a href="{{url('customer/messages')}}" class="list-group-item list-group-item-action {{ (request()->is('customer/messages')) ? 'active' : '' }}">Messages @if($message_count > 0) <span class="badge badge-danger text-white">{{ $message_count }}</span> @endif</a>
    <a href="{{url('customer_logout')}}" class="list-group-item list-group-item-action">Logout</a>
</div>
