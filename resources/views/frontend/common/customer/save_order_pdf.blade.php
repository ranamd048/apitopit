<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>{{ $title }}</title>
 <style>
body,table {
	font-family: 'bangla', sans-serif;
}
 table {
  border-collapse: collapse;
  width: 100%;
}
table td, table th {
  border: 1px solid #ddd;
  padding: 8px;
}
table tr:nth-child(even){background-color: #f2f2f2;}
table tr:hover {background-color: #ddd;}
table th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>
    <h1>{{ $heading}}</h1>
    <table>
        <tr>
            <th>SL</th>
            <th>Shop</th>
            <th>Product</th>
            <th>Quantity</th>
            <th>Price</th>
            <th align="right">Subtotal</th>
        </tr>
        @if($save_order_items->count() > 0)
        <?php $sl = 0; $total = 0; ?>
        @foreach($save_order_items as $row)
        <?php
            $sl++;
            $product_price = Helpers::is_promo_or_normal_product($row->product_id);
            $shop_info = Helpers::shop_info($row->store_id);
            $subtotal = ($product_price * $row->quantity);
            $total += $subtotal;
        ?>
        <tr>
            <td>{{$sl}}</td>
            <td>{{$shop_info->store_name}}</td>
            <td>{{$row->product_name}}</td>
            <td>{{$row->quantity}}</td>
            <td>৳ {{number_format($product_price, 2)}}</td>
            <td align="right">৳ {{number_format($subtotal, 2)}}</td>
        </tr>
        @endforeach
        @endif
    </table>
    <div class="total" style="text-align: right;">
        <h3>Total = ৳ {{number_format($total, 2)}}</h3>
    </div>
</body>
</body>
</html>