<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Order Details
@endsection

@section('main_content')
    <style> .badge {color: #fff !important;} </style>
    <section class="customer-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h3>Order Details</h3>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>User Name :</th>
                                        <td>{{$result->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>User Email :</th>
                                        <td>{{$result->email}}</td>
                                    </tr>
                                    <tr>
                                        <th>User Phone :</th>
                                        <td>{{$result->phone}}</td>
                                    </tr>
                                    <tr>
                                        <th>Total :</th>
                                        <td>৳ {{number_format($result->total_amount, 2)}}</td>
                                    </tr>
                                    <tr>
                                        <th>Delivery Charge :</th>
                                        <td>৳ {{number_format($result->delivery_charge, 2)}}</td>
                                    </tr>
                                    <tr>
                                        <th>Discount :</th>
                                        <td>৳ {{number_format($result->discount, 2)}}</td>
                                    </tr>
                                    <tr>
                                        <th>Paid Amount :</th>
                                        <td>৳ {{number_format($result->paid_amount, 2)}}</td>
                                    </tr>
                                    <tr>
                                        <th>Due Amount :</th>
                                        <td>৳ {{number_format($result->due_amount, 2)}}</td>
                                    </tr>
                                    <?php
                                    $date = new DateTime($result->created_at);
                                    if($result->invoice_status == 0) {
                                        $order_status = '<span class="badge badge-primary">Pending</span>';
                                    }
                                    if($result->invoice_status == 1) {
                                        $order_status = '<span class="badge badge-info">Processing</span>';
                                    }
                                    if($result->invoice_status == 2) {
                                        $order_status = '<span class="badge badge-success">Completed</span>';
                                    }
                                    if($result->invoice_status == 3) {
                                        $order_status = '<span class="badge badge-danger">Canceled</span>';
                                    }
                                    ?>
                                    <tr>
                                        <th>Order Date :</th>
                                        <td>{{$date->format('d-F-Y')}}</td>
                                    </tr>
                                    <tr>
                                        <th>Note :</th>
                                        <td>{{$result->customer_comments}}</td>
                                    </tr>
                                    <tr>
                                        <th>Shipping Address :</th>
                                        <td>{{ Helpers::orderShippingAddress($result->order_id) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Order Status :</th>
                                        <td><?php echo $order_status; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection