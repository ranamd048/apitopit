<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    My Wishlist
@endsection

@section('main_content')
    <section class="customer-profile ssp-product-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="ssp-products">
                        <div class="row">
                            @if(count($wishlist_items) > 0)
                                @foreach($wishlist_items as $row)
                                    <?php
                                    $check_promo = Helpers::is_promo_product($row->id);
                                    ?>
                                    <div class="col-md-4 col-6" id="wishlist_item_id_{{$row->wishlist_id}}">
                                    <a href="{{ url('/product/'.$row->id.'/'.$row->product_slug) }}">
                                        <div class="single-product">
                                            <div class="product-useful-info">
                                                <p>Shop : {{$row->store_name}}</p>
                                                <p>Unit : {{Helpers::unitName($row->unit_id)}}</p>
                                                <p>District : {{Helpers::districtName($row->district)}}</p>
                                            </div>
                                            @if($row->is_used == 1)
                                            <div class="used-product-badge">
                                                used
                                            </div>
                                            @endif
                                            <?php echo Helpers::productStockBadge($row->id); ?>
                                            <div class="product-thumb">
                                                <img src="{{ asset('public/uploads/products/thumbnail/'.$row->product_image) }}"
                                                        class="img-fluid" alt="{{ $row->product_name }}">
                                            </div>
                                            <div class="product-info text-center">
                                                <h5>{{ $row->product_name }}</h5>
                                                <!--<p class="weight">1 kg</p>-->
                                                @if($check_promo)
                                                    <?php $product_price = $row->promotion_price; ?>
                                                    <p class="price">৳ <strong>{{ $row->promotion_price }}</strong>
                                                        <span class="ml-2 red-text"><del>৳ {{ $row->price }}</del></span>
                                                    </p>
                                                @else
                                                    <?php $product_price = $row->price; ?>
                                                    <p class="price">৳ <strong>{{ $row->price }}</strong></p>
                                                @endif
                                                <div class="wishlist-cart-btn">
                                                <p class="cart-btn add_to_cart" data-id="{{$row->id}}" data-price="{{$product_price}}"><i
                                                            class="fa fa-shopping-bag"></i> add cart</p>
                                                <p class="cart-btn delete_wishlist_item" data-id="{{$row->wishlist_id}}"><i class="fa fa-trash"></i> Delete</p>
                                                </div>

                                            </div>
                                        </div>
                                    </a>
                                    </div>
                                @endforeach
                            @else
                                <div class="col-md-12 text-center">
                                    <h4>Wishlist Item Not Found...</h4>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection