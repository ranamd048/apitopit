<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Verification Code
@endsection

@section('main_content')
    <style>
        .section-title.styled {
            margin-bottom: 40px;
        }
        .section-title.styled h2 {
            color: #fff;
        }
    </style>
    <!--registration section start-->
    <section class="register-section section-padding mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                    <div class="form-area z-depth-1 p-3 rounded">
                        <div class="section-title styled text-center">
                            <h2 class="text-capitalize mt-3">Verification Code</h2>
                        </div>
                        <div class="l-form pl-3 pr-3">

                            @if(Session::has('error'))
                                <p class="alert alert-danger">{{ Session::get('error') }}</p><br>
                            @endif
                            @if(Session::has('message'))
                                <p class="alert alert-success">{{ Session::get('message') }}</p><br>
                            @endif

                            <form action="{{ url('forgot_password/verify_code') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-field">
                                            <input id="code" name="code" type="text" required="">
                                            <label for="code">Enter Verification Code *</label>
                                        </div>
                                    </div>

                                    <div class="col-12 mt-4">
                                        <div class="text-right w-100">
                                            <a href="{{ url('/forgot_password') }}"
                                               class="waves-effect waves-light btn-large left white-text">Back
                                                <i class="material-icons left"
                                                   style="-webkit-transform: rotate(180deg);-moz-transform: rotate(180deg);-ms-transform: rotate(180deg);-o-transform: rotate(180deg);transform: rotate(180deg);">send</i></a>
                                            <button class="btn btn-large waves-effect waves-light" type="submit"
                                                    name="action">
                                                Submit
                                                <i class="material-icons right">send</i>
                                            </button>
                                           
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--registration section end-->
@endsection