<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    My Orders
@endsection

@section('main_content')
    <style>
        .badge {color: #fff !important;}
        .single-order-item p strong {
            font-weight: 700;
            float: left;
            margin-right: 5px;
        }
        .single-order-item p span {
            float: left;
        }
        .single-order-item a.btn {
            padding: 2px 8px;
        }
        .single-order-item {
            position: relative;
        }
        .single-order-item .action {
            position: absolute;
            top: 0;
            right: 0;
        }
        .btn.submit_btn {
            margin-top: 40px;
        }
        .shop_account_summary {
            overflow: hidden;
        }
        .shop_account_summary h4 {
            font-size: 18px;
            text-align: center;
            margin: 0;
            text-transform: uppercase;
        }
        .shop_account_summary ul {
            margin-top: 20px;
            padding: 0;
            display: block;
        }
        .shop_account_summary ul li {
            display: inline-block;
            float: left;
            width: 33.3%;
            text-align: center;
            border-right: 1px solid #ddd;
        }
        .shop_account_summary ul li p {
            margin-bottom: 0;
        }
        .shop_account_summary ul li p strong {font-weight: 600;}
        .shop_account_summary ul li:last-child {
            border-right: 0;
        }
        .form-control, label {
            margin-bottom: 0 !important;
        }
    </style>
    <section class="customer-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="">
                                        <div class="row">
                                            <div class="col-md-3 col-6">
                                                <div class="form-group">
                                                    <label for="start_date">From Date *</label>
                                                    <input type="date" name="start_date" id="start_date" class="form-control" value="{{isset($_GET['start_date']) ? $_GET['start_date'] : ''}}" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="form-group">
                                                    <label for="end_date">To Date *</label>
                                                    <input type="date" name="end_date" id="end_date" class="form-control" value="{{isset($_GET['end_date']) ? $_GET['end_date'] : ''}}" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-8">
    <?php $selected_shop = isset($_GET['shop_id']) ? $_GET['shop_id'] : ''; ?>
                                                <div class="form-group">
                                                    <label for="shop_id">Shop</label>
                                                    <select name="shop_id" id="shop_id">
                                                        <option value="">Select Shop</option>
                                                        @foreach($ordered_shops as $ordered_shop)
                                                        <option @if($selected_shop == $ordered_shop->id) selected="" @endif value="{{ $ordered_shop->id }}">{{ $ordered_shop->store_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-4">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-default btn-small submit_btn">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(!empty($selected_shop))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <?php $shop_info = Helpers::shop_info($selected_shop); ?>
                                <div class="card-body shop_account_summary">
                                    <h4>{{ $shop_info->store_name }} account summary</h4>
                                    <ul>
                                        <li>
                                            <p><strong>৳{{ number_format($total_paid_amount, 2) }}</strong></p>
                                            <p>Total Paid</p>
                                        </li>
                                        <li>
                                            <p><strong>৳{{ number_format($total_due_amount, 2) }}</strong></p>
                                            <p>Total Due</p>
                                        </li>
                                        <li>
                                            <p><strong>৳{{ number_format($current_balance, 2) }}</strong></p>
                                            <p>Current Balance</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        @if(count($order_list) > 0)
                            @foreach($order_list as $row)
                                <?php
                                $date = new DateTime($row->created_at);
                                if($row->invoice_status == 0) {
                                    $order_status = '<span class="badge badge-primary">Pending</span>';
                                }
                                if($row->invoice_status == 1) {
                                    $order_status = '<span class="badge badge-info">Processing</span>';
                                }
                                if($row->invoice_status == 2) {
                                    $order_status = '<span class="badge badge-success">Completed</span>';
                                }
                                if($row->invoice_status == 3) {
                                    $order_status = '<span class="badge badge-danger">Canceled</span>';
                                }
                                ?>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body single-order-item">
                                    <p><strong>Date:</strong> {{$date->format('d-F-Y')}}</p>
                                    <p><strong>Total:</strong> ৳ {{number_format($row->total_amount, 2)}}</p>
                                    <p><strong>Status:</strong> <?php echo $order_status; ?></p>
                                    <div class="action">
                                        <div class="dropdown">
                                            <a class="btn btn-info dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a target="_blank" class="dropdown-item" href="{{url('print_order/'.$row->id.'?type=orders_invoice')}}">Print Order</a>
                                                <a class="dropdown-item" href="{{url('order_items/'.$row->id)}}">Order Items</a>
                                                <a class="dropdown-item" href="{{url('order_details/'.$row->id)}}">Order Details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endforeach
                                @if($pagination)
                                    <div class="col-md-12">
                                        {{$order_list->links()}}
                                    </div>
                                @endif
                        @else
                           <div class="col-md-12">
                               <h5>Order Not Found...</h5>
                           </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection