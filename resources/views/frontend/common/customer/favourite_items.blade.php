<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Favourite Items
@endsection

@section('main_content')
<style>
.single-product.service-item.favourites .product-thumb {
    height: 170px;
}
.single-shop-list p.delete_favourite_item {
    position: absolute;
    top: 5px;
    left: 5px;
    background: var(--theme-color);
    padding: 1px 10px;
    color: #fff;
    border-radius: 3px;
    cursor: pointer;
}
</style>
    <section class="customer-profile ssp-product-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="p-tab">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item font-weight-bold text-capitalize">
                                <a class="nav-link active" id="other-tab" data-toggle="tab" href="#products_tab"
                                role="tab"
                                aria-controls="other"
                                aria-selected="true">PRODUCTS</a>
                            </li>
                            <li class="nav-item font-weight-bold text-capitalize">
                                <a class="nav-link" id="home-tab" data-toggle="tab" href="#shops_tab"
                                role="tab"
                                aria-controls="home"
                                aria-selected="true">SHOPS</a>
                            </li>
                            
                            <li class="nav-item font-weight-bold text-capitalize">
                                <a class="nav-link" id="review-tab" data-toggle="tab" href="#service_tab" role="tab"
                                aria-controls="review"
                                aria-selected="false">SERVICES</a>
                            </li>
                        </ul>
                        <div class="tab-content border border-top-0 p-3" id="myTabContent">
                        <div class="tab-pane fade show active" id="products_tab" role="tabpanel"
                                aria-labelledby="other-tab">
                                <div class="ssp-products">
                                    <div class="row">
                                        @if(count($favourite_products) > 0)
                                            @foreach($favourite_products as $row)
                                                <?php
                                                $check_promo = Helpers::is_promo_product($row->id);
                                                ?>
                                                <div class="col-md-4 col-6" id="favourite_item_id_{{$row->favourite_id}}">
                                                <a href="{{ url('/product/'.$row->id.'/'.$row->product_slug) }}">
                                                    <div class="single-product">
                                                        @if($private_settings->product_hover == 1)
                                                        <div class="product-useful-info">
                                                            <p>Shop : {{$row->store_name}}</p>
                                                            <p>District : {{Helpers::districtName($row->district)}}</p>
                                                        </div>
                                                        @endif
                                                        @if($row->is_used == 1)
                                                        <div class="used-product-badge">
                                                            used
                                                        </div>
                                                        @endif
                                                        <?php echo Helpers::productStockBadge($row->id); ?>
                                                        <div class="product-thumb">
                                                            <img src="{{ asset('public/uploads/products/thumbnail/'.$row->product_image) }}"
                                                                    class="img-fluid" alt="{{ $row->product_name }}">
                                                        </div>
                                                        <div class="product-info text-center">
                                                            <h5>{{ $row->product_name }}</h5>
                                                            <!--<p class="weight">1 kg</p>-->
                                                            @if($check_promo)
                                                                <?php $product_price = $row->promotion_price; ?>
                                                                <p class="price">৳ <strong>{{ $row->promotion_price }}</strong> / {{Helpers::unitName($row->unit_id)}}
                                                                    <span class="ml-2 red-text"><del>৳ {{ $row->price }}</del></span>
                                                                </p>
                                                            @else
                                                                <?php $product_price = $row->price; ?>
                                                                <p class="price">৳ <strong>{{ $row->price }}</strong> / {{Helpers::unitName($row->unit_id)}}</p>
                                                            @endif
                                                            <div class="wishlist-cart-btn">
                                                            <p class="cart-btn add_to_cart" data-id="{{$row->id}}" data-price="{{$product_price}}"><i
                                                                        class="fa fa-shopping-bag"></i> add</p>
                                                            <p class="cart-btn delete_favourite_item" data-id="{{$row->favourite_id}}"><i class="fa fa-trash"></i></p>
                                                            <div class="district_name">
                                                            <p>{{$row->store_name}} / {{Helpers::districtName($row->district)}}</p>
                                                            </div>
                                                        </div>

                                                        </div>
                                                    </div>
                                                </a>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="col-md-12 text-center">
                                                <h4>Product Not Found...</h4>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="shops_tab" role="tabpanel"
                                aria-labelledby="home-tab">
                                <div class="shop-list-section shop-lists">
                                    <div class="row">
                                        @if(count($favourite_shops) > 0)
                                            @foreach($favourite_shops as $row)
                                                <?php
                                                if(isset($row->logo)) {
                                                    $shop_logo = asset('public/uploads/store/'.$row->logo);
                                                } else {
                                                    $shop_logo = asset('frontend_assets/images/shop_logo.png');
                                                }
                                                ?>
                                                <div class="col-md-3 col-4" id="favourite_item_id_{{$row->id}}">
                                                    <div class="single-shop-list white z-depth-1 p-2">
                                                        <a href="{{ url('/shop/'.$row->slug) }}">
                                                            <div class="shop-thumb">
                                                                <img src="{{ $shop_logo }}"
                                                                    alt="{{ $row->store_name }}" class="img-fluid">
                                                            </div>
                                                            <div class="shop-name">
                                                                <h5>{{ $row->store_name }}</h5>
                                                            </div>
                                                        </a>
                                                        <div class="shop-district_name">
                                                            <span class="badge badge-success">
                                                                {{Helpers::districtName($row->district)}}
                                                            </span>
                                                        </div>
                                                        <p class="delete_favourite_item" data-id="{{$row->id}}"><i class="fa fa-trash"></i></p>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="col-md-12 text-center">
                                                <h5>Shop Not Found...</h5>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="tab-pane fade" id="service_tab" role="tabpanel" aria-labelledby="review-tab">
                            <div class="row">
                                @if(count($favourite_services) > 0)
                                    @foreach($favourite_services as $row)
                                    <div class="col-md-4 col-6" id="favourite_item_id_{{$row->id}}">

                                        <a href="{{ url('/service/'.$row->item_id.'/'.$row->service_slug) }}">
                                            <div class="single-product service-item favourites">
                                                <div class="product-thumb">
                                                    <img src="{{ asset('public/uploads/services/'.$row->service_image) }}"
                                                         class="img-fluid" alt="{{ $row->service_name }}">
                                                </div>
                                                <div class="product-info text-center">
                                                    <h5>{{ $row->service_name }}</h5>
                                                    <!--<p class="weight">1 kg</p>-->
                                                    <p class="price">৳ <strong>{{ number_format($row->price, 2) }}</strong></p>
                                                    <div class="wishlist-cart-btn">
                                                        <p class="cart-btn"><i class="fa fa-eye"></i> View</p>
                                                        <p class="cart-btn delete_favourite_item" data-id="{{$row->id}}"><i class="fa fa-trash"></i></p>
                                                    </div>
                                                    <div class="service_district_name p-0">
                                                    <?php $shop_info = Helpers::singleStoreInfo($row->store_id) ?>
                                                        <p>{{$shop_info->store_name}} / {{Helpers::districtName($shop_info->district)}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="col-md-12">
                                        <h4>Service Not Found...</h4>
                                    </div>
                                @endif
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection