<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    {{$distributor->name}} | Profile
@endsection

@section('main_content')
    <section class="customer-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Your Profile</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    @if(Session::has('message'))
                                        <p class="alert alert-success">{{ Session::get('message') }}</p><br>
                                    @endif
                                    <form action="{{url('update_distributor_profile')}}" method="POST">
                                        {{csrf_field()}}
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label">Name</label>
                                            <div class="col-8">
                                                <input name="name" value="{{$distributor->name}}" class="form-control here" type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label">Email</label>
                                            <div class="col-8">
                                                <input name="email" value="{{$distributor->email}}" class="form-control here" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="offset-4 col-8">
                                                <button name="submit" type="submit" class="btn btn-primary">Update Profile</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection