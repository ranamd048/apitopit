<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    {{Auth::user()->name}} | Profile
@endsection

@section('main_content')
    <?php $customer = Auth::user(); ?>
    <section class="customer-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Your Profile</h4>
                                    <div class="back-button">
                                        <a class="btn btn-small" href="{{url('product_list')}}"><i class="fa fa-arrow-left"></i> Back</a>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    @if(Session::has('message'))
                                        <p class="alert alert-success">{{ Session::get('message') }}</p><br>
                                    @endif
                                    <form action="{{url('update_customer_profile')}}" method="POST" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="existing_photo" value="{{$customer->photo}}">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label">Name</label>
                                            <div class="col-8">
                                                <input name="name" value="{{$customer->name}}" class="form-control here" type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label">Email</label>
                                            <div class="col-8">
                                                <input name="email" value="{{$customer->email}}" class="form-control here" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label">Address</label>
                                            <div class="col-8">
                                                <input name="address" value="{{$customer->address}}" class="form-control here" type="text" required="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-4 col-form-label">Post Code</label>
                                            <div class="col-8">
                                                <input name="post_code" value="{{$customer->post_code}}" class="form-control here" type="text" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label">Update Profile Picture</label>
                                            <div class="col-8">
                                                <input name="photo" class="form-control here" type="file">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label">About Yourself</label>
                                            <div class="col-8">
                                                <textarea name="about_me" cols="40" rows="4" class="form-control">{{$customer->about_me}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="offset-4 col-8">
                                                <button name="submit" type="submit" class="btn btn-primary">Update Profile</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection