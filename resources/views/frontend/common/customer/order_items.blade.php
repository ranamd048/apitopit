<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Order Items
@endsection

@section('main_content')
    <style> .badge {color: #fff !important;} </style>
    <section class="customer-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h3>Order Items</h3>
                            <div id="responsive_table">
                                <table class="table table-hover">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>S/N</th>
                                        <th>Shop</th>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Subtotal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0; ?>
                                    @foreach($order_items as $row)
                                        <?php
                                        $i++;
                                        ?>
                                        <tr>
                                            <td data-title="S/N">{{$i}}</td>
                                            <td data-title="Shop"><a href="{{url('shop/'.$row->slug)}}">{{$row->store_name}}</a></td>
                                            <td data-title="Product"><a href="{{url('product/'.$row->product_id.'/'.$row->product_slug)}}">{{$row->product_name}}</a></td>
                                            <td data-title="Price">৳ {{number_format($row->price, 2)}}</td>
                                            <td data-title="Quantity">{{$row->quantity}}</td>
                                            <td data-title="Subtotal">৳ {{number_format($row->sub_total, 2)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection