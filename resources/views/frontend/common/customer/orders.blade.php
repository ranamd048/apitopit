<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    My Orders
@endsection

@section('main_content')
    <style>
        .badge {color: #fff !important;}
        .single-order-item p strong {
            font-weight: 700;
            float: left;
            margin-right: 5px;
        }
        .single-order-item p span {
            float: left;
        }
        .single-order-item a.btn {
            padding: 2px 8px;
        }
        .single-order-item {
            position: relative;
        }
        .single-order-item .action {
            position: absolute;
            top: 0;
            right: 0;
        }
    </style>
    <section class="customer-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="">
                                        <div class="row">
                                            <div class="col-md-5 col-12">
                                                <div class="form-group">
                                                    <label for="start_date">From Date</label>
                                                    <input type="date" name="start_date" id="start_date" class="form-control" value="{{isset($_GET['start_date']) ? $_GET['start_date'] : ''}}">
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-8">
                                                <div class="form-group">
                                                    <label for="end_date">To Date</label>
                                                    <input type="date" name="end_date" id="end_date" class="form-control" value="{{isset($_GET['end_date']) ? $_GET['end_date'] : ''}}">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-4">
                                                <div class="form-group">
                                                    <button type="submit" style="margin-top: 40px" class="btn btn-default btn-small">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if(count($order_list) > 0)
                            @foreach($order_list as $row)
                                <?php
                                $date = new DateTime($row->order_date);
                                if($row->order_status == 0) {
                                    $order_status = '<span class="badge badge-primary">Pending</span>';
                                }
                                if($row->order_status == 1) {
                                    $order_status = '<span class="badge badge-info">Processing</span>';
                                }
                                if($row->order_status == 2) {
                                    $order_status = '<span class="badge badge-success">Completed</span>';
                                }
                                if($row->order_status == 3) {
                                    $order_status = '<span class="badge badge-danger">Canceled</span>';
                                }
                                ?>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body single-order-item">
                                    <p><strong>Date:</strong> {{$date->format('d-F-Y')}}</p>
                                    <p><strong>Total:</strong> ৳ {{number_format($row->total, 2)}}</p>
                                    <p><strong>Subtotal:</strong> ৳ {{number_format($row->subtotal, 2)}}</p>
                                    <p><strong>Status:</strong> <?php echo $order_status; ?></p>
                                    <div class="action">
                                        <div class="dropdown">
                                            <a class="btn btn-info dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a target="_blank" class="dropdown-item" href="{{url('print_order/'.$row->id)}}">Print Order</a>
                                                <a class="dropdown-item" href="{{url('order_items/'.$row->id)}}">Order Items</a>
                                                <a class="dropdown-item" href="{{url('order_details/'.$row->id)}}">Order Details</a>
                                                <a class="dropdown-item" href="{{url('order_invoice/'.$row->id)}}">Order Invoice</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endforeach
                                @if($pagination)
                                    <div class="col-md-12">
                                        {{$order_list->links()}}
                                    </div>
                                @endif
                        @else
                           <div class="col-md-12">
                               <h5>Order Not Found...</h5>
                           </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection