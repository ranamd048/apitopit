<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Order Items
@endsection

@section('main_content')
    <style> .badge {color: #fff !important;} </style>
    <section class="customer-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('frontend.common.customer.sidebar_menu')
                </div>
                <div class="col-md-9">
                    <!-- cart view start-->
    <section class="cart-view-section section-padding mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-lg-0 mb-5">
                    <div class="cart-item-details">
                        <?php $total_amount = 0; ?>
                        @if($save_order_items->count() > 0)
                            <ul>
                                @foreach($save_order_items as $row)
        <?php
            $product_price = Helpers::is_promo_or_normal_product($row->product_id);
            $shop_info = Helpers::shop_info($row->store_id);
            $total_amount += $product_price;
        ?>
                                    <li class="c-data text-uppercase mb-4 shadow cart-item">
                                        <a target="_blank" href="{{url('shop/'.$shop_info->slug)}}" class="shop-name badge badge-success">
                                            {{$shop_info->store_name}} ({{Helpers::shop_shipping_cost($row->store_id)}})
                                        </a>
                                        <div class="row p-3">
                                            <div class="col-7 d-flex">
                                                <div class="cart-item custom-control">
                                                    <div class="d-flex">
                                                        <div class="c-thumb">
                                                            <img src="{{ asset('public/uploads/products/thumbnail/'.$row->image) }}" alt="" class="img-fluid">
                                                        </div>
                                                        <span class="c-info ml-2">
                                                <p class="mb-0">{{$row->product_name}}</p>
                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-5 text-center">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <?php
                                                        $total_price = ($product_price * $row->quantity);
                                                        ?>
                                                        <p class="font-weight-bold text-danger m-0">৳ {{number_format($product_price, 2)}}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="c-qty d-flex mt-2">
                                                            <span class="m-auto" id="qty_value_{{$row->id}}">{{$row->quantity}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <h4>Cart Item Not found</h4>
                        @endif
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="cart-totals z-depth-1 rounded">
                        <div class="ch-address p-3 pb-0">
                            <div class="d-flex clearfix">
                                <i class="fa fa-map-marker my-auto"></i>
                                <h6 class="ml-2 text-capitalize font-weight-bold">{{Auth::user()->name}}</h6>
                            </div>
                            <p class="m-0">{{Auth::user()->address}}</p>
                        </div>
                        <ul class="cart p-2">
                            <span class="font-weight-bold text-capitalize">order summary</span>
                            <li class="text-capitalize p-2">total items<span class="float-right">{{count($save_order_items)}} items</span></li>
                            <li class="text-capitalize p-2">total Amount<span class="float-right">৳ {{number_format($total_amount, 2)}}</span></li>
                            <?php $total_shipping_cost = Helpers::save_order_shipping_cost($save_order_items); ?>
                            <li class="text-capitalize p-2">Shipping cost<span class="float-right">৳ {{number_format($total_shipping_cost, 2)}}</span></li>
                            <?php $grand_total = ($total_amount + $total_shipping_cost); ?>
                            <li class="text-capitalize p-2">grand total<span class="float-right">৳ {{number_format($grand_total, 2)}}</span></li>
                            <div class="text-center">
                                <form id="order_place_form">
                                    <input type="hidden" name="order_type" value="save_order">
                                    <input type="hidden" name="biller_id" value="{{Auth::user()->reference_id}}">
                                    <input type="hidden" name="customer_id" value="{{Auth::user()->id}}">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="address" placeholder="Shipping Address">
                                    </div>
                                    <input type="hidden" name="total_amount" value="{{$total_amount}}">
                                    <input type="hidden" name="sub_total" value="{{$grand_total}}">
                                    <input type="hidden" name="delivery_charge" value="{{$total_shipping_cost}}">
                                    <a href="{{url('save_order_pdf')}}" class="btn btn-success w-100 mt-3">Download as PDF</a>
                                    <button type="submit" id="place_order_btn" class="w-100 waves-effect waves-light btn-small white-text mt-4 font-weight-bold z-depth-1 rounded">place order</button>
                                    
                                </form>
                            </div>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- cart view end-->
                </div>
            </div>
        </div>
    </section>
@endsection