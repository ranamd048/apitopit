<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Print</title>
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <style>
        .print-section-wrapper .header {
            text-align: center;
        }
        .print-section-wrapper .header img {
            width: 150px;
            height: auto;
        }
    </style>
</head>
<body>
    <div class="print-section-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="order-summary">
                        <h3>Order Summary</h3>
                        <table class="table table-bordered">
                            <thead class="thead-default">
                            <tr>
                                <th>Order ID</th>
                                <th>Customer Name</th>
                                <th>Customer Phone</th>
                                <th>Total</th>
                                <th>Subtotal</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $date = new DateTime($order_details->order_date); ?>
                                <tr>
                                    <td>{{$order_details->id}}</td>
                                    <td>{{$order_details->name}}</td>
                                    <td>{{$order_details->phone}}</td>
                                    <td>৳ {{number_format($order_details->total, 2)}}</td>
                                    <td>৳ {{number_format($order_details->subtotal, 2)}}</td>
                                    <td>{{$date->format('d-F-Y')}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="order-summary">
                        <h3>Order Items</h3>
                        <table class="table table-bordered">
                            <thead class="thead-default">
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($order_items as $row)
                                <?php
                                $i++;
                                ?>
                                <tr>
                                    <th>{{$i}}</th>
                                    <td>{{$row->product_name}}</td>
                                    <td>৳ {{number_format($row->price, 2)}}</td>
                                    <td>{{$row->quantity}}</td>
                                    <td>৳ {{number_format($row->sub_total, 2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.print();
    </script>
</body>
</html>