<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    {{ $product->product_name }}
@endsection

@section('meta_content')
    <meta name="description" content="{{ mb_substr(strip_tags($product->product_description), 0, 200) }}" />
    <meta property="og:title" content="{{ $product->product_name }}" />
    <meta property="og:url" content="{{ url('/product/'.$product->id.'/'.$product->product_slug) }}" />
    <meta property="og:site_name" content="{{ url('/') }}" />
    <meta property="og:description" content="{{ mb_substr(strip_tags($product->product_description), 0, 200) }}" />
    <meta property="og:image" content="{{ asset('public/uploads/products/'.$product->product_image) }}" />
    <meta property="og:image:width" content="480">
    <meta property="og:image:height" content="360">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{ url('/') }}">
    <meta name="twitter:title" content="{{ $product->product_name }}">
    <meta name="twitter:description" content="{{ mb_substr(strip_tags($product->product_description), 0, 200) }}">
    <meta name="twitter:image" content="{{ asset('public/uploads/products/'.$product->product_image) }}">
    <link rel="canonical" href="{{ url('/product/'.$product->id.'/'.$product->product_slug) }}">
@endsection

@section('main_content')
<?php $shop_info = Helpers::singleStoreInfo($product->store_id) ?>
    <div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>Product Details</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            <li>{{ $product->product_name }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--package details section start-->
    <section class="package-details-section product-details-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        @if(!empty($product->product_image))
                        @php $col = '5'; @endphp
                        <div class="col-md-{{$col}}">
                            <div class="exzoom hidden" id="product-zoom">
                                <!-- Images -->
                                <div class="exzoom_img_box">
                                    <ul class='exzoom_img_ul'>
                                        <li>
                                            <img src="{{ asset('public/uploads/products/'.$product->product_image) }}" width="100%" />
                                        </li>
                                        @if(count($product->productGalleryItems) > 0)
                                            @foreach($product->productGalleryItems as $row)
                                                <li>
                                                    <img src="{{ asset('public/uploads/products/gallery/'.$row->image_url) }}"/>
                                                </li>
                                            @endforeach
                                        @endif

                                    </ul>
                                </div>
                                <!-- <a href="https://www.jqueryscript.net/tags.php?/Thumbnail/">Thumbnail</a> Nav-->
                                <div class="exzoom_nav"></div>
                                <!-- Nav Buttons -->
                                <p class="exzoom_btn">
                                    <a href="javascript:void(0);" class="exzoom_prev_btn"><i
                                                class="fa fa-angle-left"></i></a>
                                    <a href="javascript:void(0);" class="exzoom_next_btn"><i
                                                class="fa fa-angle-right"></i></a>
                                </p>
                            </div>
                        </div>
                        @else
                        @php $col = '12'; @endphp
                        @endif
                        <div class="col-md-{{$col}}">
                            <div class="details-section p-3">
                                <div class="product-title">
                                    <h6 class="text-capitalize font-weight-bold">{{ $product->product_name }}</h6>
                                </div>
                                <div class="sort-desc">
                                    <p><?php echo mb_substr(strip_tags($product->product_description), 0, 200); ?></p>
                                </div>
                                <?php $check_promo = Helpers::is_promo_product($product->id); ?>
                                @if($check_promo)
                                    <?php
                                    $discount_amount = ($product->price - $product->promotion_price);
                                    $discount_percentange = ($discount_amount * 100) / $product->price;
                                    $product_price = $product->promotion_price;
                                    ?>
                                    @if(Helpers::showProductPrice($product->store_id)  && $product->show_per_price == 1)
                                    <div class="product-price">
                                        <p class="text-capitalize"><span class="current-price">price: <strong
                                                        class="font-weight-bold mr-2">৳ {{ $product->promotion_price }}</strong> / <?php echo Helpers::unitName($product->unit_id); ?> </span>
                                            <span
                                                    class="regular-price"><del>৳ {{ $product->price }}</del></span>
                                            <span
                                                    class="discount red white-text p-2 rounded z-depth-1 ml-2"> {{ number_format($discount_percentange) }}
                                                % off</span>
                                        </p>
                                        <p class="text-capitalize font-italic red-text" style="font-size: 0.87em">This
                                            offer
                                            will expire on <span class="font-weight-bold"><?php
                                                $date = new DateTime($check_promo->promotion_end_date);
                                                echo $date->format('d M Y')
                                                ?></span></p>
                                    </div>
                                    @endif
                                @else
                                    <?php $product_price = $product->price; ?>
                                    @if(Helpers::showProductPrice($product->store_id) && $product->show_per_price == 1)
                                    <div class="product-price">
                                        <p class="text-capitalize"><span class="current-price">price: <strong
                                                        class="font-weight-bold mr-2">৳ {{ $product->price }}</strong> / <?php echo Helpers::unitName($product->unit_id); ?> </span>
                                        </p>
                                    </div>
                                    @endif
                                @endif

                                @if(Helpers::showProductStock($product->store_id))
                                <div class="in-stock">
                                    <p class="text-capitalize">availability: <?php echo Helpers::productStockBadge($product->id); ?>
                                    </p>
                                </div>
                                @endif
                                <div class="in-stock">
                                    <p class="text-capitalize">View Shop: <a class="btn btn-success" target="_blank" href="{{url('shop/'.$shop_info->slug)}}">{{$shop_info->store_name}}</a>
                                    </p>
                                </div>
                                <div class="in-stock">
                                    <p class="text-capitalize">Shop Location : <?php echo Helpers::divisionName($shop_info->division). ', '.Helpers::districtName($shop_info->district). ', '.Helpers::upazilaName($shop_info->thana); ?> <a target="_blank" href="{{url('shop_map_view/'.$shop_info->slug)}}" class="btn btn-success btn-small"><i class="fa fa-map-marker"></i></a>
                                    </p>
                                </div>
                                <!-- <div class="quantity" style="width: 150px;">
                                    <form>
                                        <div class="form-group d-flex">
                                            <p class="text-capitalize font-weight-bold my-auto mr-2">qty: </p>
                                            <input type="number" class="border font-weight-bold text-center"
                                                   id="cart_qty_number" value="1">
                                        </div>
                                    </form>
                                </div> -->
                            @if(Helpers::showProductPrice($product->store_id) && $product->show_per_price == 1)
                                <div class="item-counter quantity">
                                    <form>
                                        <div class="minus qty_button">
                                            <i class="fa fa-minus"></i>
                                        </div>
                                        <div class="number-show cart-number">
                                            <div class="form-field">
                                                <input type="text" id="cart_qty_number" class="form-control item_count" value="1" name="qtybutton">
                                            </div>
                                        </div>
                                        <div class="plus qty_button">
                                            <i class="fa fa-plus"></i>
                                        </div>
                                    </form>
                                </div>
                                <div class="buy-area">
                                    <div class="row">
                                        <a class="ml-3 waves-light btn-small mr-2 white-text add_to_cart_details_page" data-shop_slug="{{ $private_settings->multiple_shop_cart == 0 ? $shop_info->slug : 'NO' }}" data-id="{{$product->id}}" data-price="{{$product_price}}"><i
                                                    class="material-icons left">local_grocery_store</i>add to cart</a>
                                        @if($private_settings->wishlist_option == 1)
                                        <a class="waves-effect waves-light btn-small mr-2 white-text add_to_wishlist" data-productid="{{$product->id}}"><i
                                                    class="material-icons left">favorite</i>add to wishlist</a>
                                        @else
                                        <a class="waves-effect waves-light btn-small mr-2 white-text add_to_favourite" data-id="{{$product->id}}" data-type="product"><i
                                                    class="material-icons left">favorite</i>favourite</a>
                                        @endif
                                    </div>
                                </div>
                            @endif




                                <div class="share-items clearfix">



                <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b61cf9c0ea30730"></script>



							  <!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox" data-url="{{url('product/'.$product->id.'/'.$product->product_slug)}}" data-title="{{$product->product_name}}" data-description="<?php echo mb_substr(strip_tags($product->product_description), 0, 200); ?>" style="clear: both;"><div id="atstbx" class="at-resp-share-element at-style-responsive addthis-smartlayers addthis-animated at4-show" aria-labelledby="at-3ae9c96f-60bb-4fba-bfc4-0275552cbefe" role="region"><span id="at-3ae9c96f-60bb-4fba-bfc4-0275552cbefe" class="at4-visually-hidden">AddThis Sharing Buttons</span><div class="at-share-btn-elements"><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-facebook" style="background-color: rgb(59, 89, 152); border-radius: 0px;"><span class="at4-visually-hidden">Share to Facebook</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1" class="at-icon at-icon-facebook" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-facebook-1">Facebook</title><g><path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-twitter" style="background-color: rgb(29, 161, 242); border-radius: 0px;"><span class="at4-visually-hidden">Share to Twitter</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2" class="at-icon at-icon-twitter" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-twitter-2">Twitter</title><g><path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-whatsapp" style="background-color: rgb(77, 194, 71); border-radius: 0px;"><span class="at4-visually-hidden">Share to WhatsApp</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-whatsapp-3" class="at-icon at-icon-whatsapp" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-whatsapp-3">WhatsApp</title><g><path d="M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-email" style="background-color: rgb(132, 132, 132); border-radius: 0px;"><span class="at4-visually-hidden">Share to Email</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-email-4" class="at-icon at-icon-email" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-email-4">Email</title><g><g fill-rule="evenodd"></g><path d="M27 22.757c0 1.24-.988 2.243-2.19 2.243H7.19C5.98 25 5 23.994 5 22.757V13.67c0-.556.39-.773.855-.496l8.78 5.238c.782.467 1.95.467 2.73 0l8.78-5.238c.472-.28.855-.063.855.495v9.087z"></path><path d="M27 9.243C27 8.006 26.02 7 24.81 7H7.19C5.988 7 5 8.004 5 9.243v.465c0 .554.385 1.232.857 1.514l9.61 5.733c.267.16.8.16 1.067 0l9.61-5.733c.473-.283.856-.96.856-1.514v-.465z"></path></g></svg></span></a><a role="button" tabindex="0" class="at-icon-wrapper at-share-btn at-svc-compact" style="background-color: rgb(255, 101, 80); border-radius: 0px;"><span class="at4-visually-hidden">Share to More</span><span class="at-icon-wrapper" style="line-height: 32px; height: 32px; width: 32px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-addthis-5" class="at-icon at-icon-addthis" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><title id="at-svg-addthis-5">AddThis</title><g><path d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z" fill-rule="evenodd"></path></g></svg></span></a></div></div></div>



              <!-- Share items end -->



              </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-5">
                        <div class="col-md-12">
                            <div class="p-tab">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <?php $active_class = 'active'; ?>
                                @if(!empty($product->product_video))
                                <?php $active_class = ''; ?>
                                    <li class="nav-item font-weight-bold text-capitalize">
                                        <a class="nav-link active" id="video-tab" data-toggle="tab" href="#product_video"
                                           role="tab"
                                           aria-controls="home"
                                           aria-selected="true">Video</a>
                                    </li>
                                @endif
                                    <li class="nav-item font-weight-bold text-capitalize">
                                        <a class="nav-link {{$active_class}}" id="home-tab" data-toggle="tab" href="#description"
                                           role="tab"
                                           aria-controls="home"
                                           aria-selected="true">Details</a>
                                    </li>
                                    <li class="nav-item font-weight-bold text-capitalize">
                                        <a class="nav-link" id="other-tab" data-toggle="tab" href="#other_products"
                                           role="tab"
                                           aria-controls="other"
                                           aria-selected="true">Shop Related Products</a>
                                    </li>
                                    <li class="nav-item font-weight-bold text-capitalize">
                                        <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab"
                                        aria-controls="review"
                                        aria-selected="false">Review</a>
                                    </li>
                                </ul>
                                <div class="tab-content border border-top-0 p-3" id="myTabContent">
                                <?php $active_class = 'show active'; ?>
                                @if(!empty($product->product_video))
                                <?php $active_class = ''; ?>
                                    <div class="tab-pane fade show active" id="product_video" role="tabpanel"
                                         aria-labelledby="video-tab">
                                         {{$product->video_link}}
                                         <iframe width="100%" src="https://www.youtube.com/embed/{{$product->product_video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                @endif
                                    <div class="tab-pane fade {{$active_class}}" id="description" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        {!! html_entity_decode($product->product_description) !!}
                                    </div>
                                    <div class="tab-pane fade" id="other_products" role="tabpanel"
                                         aria-labelledby="other-tab">
                                         @if(count($other_products) > 0)
                                            <div class="row">
                                                @foreach($other_products as $row)
                                                    <div class="col-lg-3 col-md-4 col-6">
                                                    @include('frontend.common.product_item')
                                                    </div>
                                                @endforeach
                                            </div>
                                        @else
                                        <h5>Not Found...</h5>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
                                        @if(Session::has('customer_id'))
                                        <div class="add-review">
                                            <h4 class="text-capitalize">add review</h4>
                                            <div class="review-form w-50">
                                                <form action="" id="product_review_form">
                                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="feedback-star">
                                                                <div class="rating">
                                                                <label>
                                                                    <input type="radio" name="stars" value="1" />
                                                                    <span class="icon">★</span>
                                                                </label>
                                                                <label>
                                                                    <input type="radio" name="stars" value="2" />
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                </label>
                                                                <label>
                                                                    <input type="radio" name="stars" value="3" />
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                </label>
                                                                <label>
                                                                    <input type="radio" name="stars" value="4" />
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                </label>
                                                                <label>
                                                                    <input type="radio" name="stars" value="5" />
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                    <span class="icon">★</span>
                                                                </label>
                                                                </div>
                                                            </div>
                                                            <div class="input-field">
                                                                <input id="comment" name="comment" type="text" class="validate" required="">
                                                                <label for="comment">Your Comment <span
                                                                        class="red-text">*</span></label>
                                                            </div>
                                                            <button class="btn btn-large waves-effect waves-light" type="submit" id="submit_review">
                                                                Submit
                                                                <i class="material-icons right">send</i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        @else
                                        <a href="{{url('login')}}" class="btn btn-primary">Login Your Account</a>
                                        @endif
                                        @if(count($product_reviews) > 0)
                                        <div class="product-review-show">
                                            @foreach($product_reviews as $row)
                                <?php
                                    if(!empty($row->photo)) {
                                        $user_photo = asset('public/uploads/customer/'. $row->photo);
                                    } else {
                                        $user_photo = asset('frontend_assets/images/user_profile.png');
                                    }
                                    $date = new DateTime($row->created_at);
                                ?>
                                            <div class="review-list">
                                                <div class="author">
                                                    <img src="{{$user_photo}}" alt="{{$row->name}}">
                                                    <h4>{{$row->name}}</h4>
                                                    <p>{{$date->format('d F Y')}}</p>
                                                </div>
                                                <div class="comment">
                                                    <div class="star">
                                                        @for($i=0; $row->rating > $i; $i++)
                                                        <i class="fa fa-star"></i>
                                                        @endfor
                                                    </div>
                                                    <p>{{$row->comment}}</p>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(count($related_products) > 0)
            <div class="container">
                <div class="related-product-wrapper ssp-product-section pt-5 pb-5">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title styled">
                                <h3 class="text-capitalize">Related Products</h3>
                            </div>
                            <div class="related-product">
                                <div class="row">
                                    @foreach($related_products as $row)
                                        <div class="col-lg-3 col-md-4 col-6">
                                        @include('frontend.common.product_item')
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </section>
    <!--package details section end-->
    <script src="{{ asset('frontend_assets') }}/common/js/jquery.exzoom.js"></script>
    <script>
        $(function () {
            $("#product-zoom").exzoom({
                // autoplay
                "autoPlay": false,
            });
        });
        $(document).ready(function() {
            /* Cart Minus */
            $(document).on('click', '.minus.qty_button', function () {
                var oldValue = $('.number-show.cart-number input').val();
                if (oldValue > 1) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 1;
                }
                $('.number-show.cart-number input').val(newVal);
            });
            /* Cart Plus */
            $(document).on('click', '.plus.qty_button', function () {
                var oldValue = $('.number-show.cart-number input').val();
                    var newVal = parseFloat(oldValue) + 1;
                $('.number-show.cart-number input').val(newVal);
            });
        });
    </script>
@endsection
