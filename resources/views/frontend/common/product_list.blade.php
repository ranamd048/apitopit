<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Product List
@endsection

@section('main_content')
    <div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>Product List</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            <li>Product List Page</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $division_list = Helpers::getDivisionList(); ?>
    <!--shop page content section start-->
    <section class="ssp-product-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    @include('frontend.common.product_sidebar')
                </div>
                <div class="col-md-9">
                    <div class="product-search" style="background: #fff;margin-bottom: 13px;padding: 0px 2px 0px 16px;border-radius: 5px;">
                        <form action="{{ url('product_list') }}" method="GET">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="input-field">
                                        <select name="division" id="division_list" class="validate">
                                            <option value="">Search by Division</option>
                                            @foreach($division_list as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-field">
                                        <select name="district" class="validate"  id="district_list">
                                            <option value="">Search by District</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-field">
                                        <select name="upazila" class="validate" id="upazila_list">
                                            <option value="">Search by Upazila</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3" style="align-items: center;display: inline-flex;">
                                    <button class="btn btn-md waves-effect waves-light" type="submit" style="margin-right: 6px;">
                                        Search
                                        <i class="material-icons right">search</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ssp-products">
                        <div class="row">
                            @if(count($product_list) > 0)
                                @foreach($product_list as $row)
                                    <div class="col-md-4 col-6">
                                    @include('frontend.common.product_item')
                                    </div>
                                @endforeach
                                <div class="col-md-12">
                                    {{ $product_list->links() }}
                                </div>
                            @else
                                <div class="col-md-12 text-center">
                                    <h3>Not Found...</h3>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--shop page content section end-->
@endsection
