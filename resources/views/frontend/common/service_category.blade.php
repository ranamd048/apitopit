<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    {{$category->name}}
@endsection

@section('main_content')
    <div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>{{$category->name}}</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            @if(!empty($parent_category))
                                <li><a href="{{url('/service_category/'.$parent_category->slug)}}">{{$parent_category->name}}</a></li>
                                <li>/</li>
                            @endif
                            <li>{{$category->name}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <!--shop page content section start-->
    <section class="ssp-product-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    @include('frontend.common.service_sidebar')
                </div>
                <div class="col-md-9">
                @if(count($sub_categories) > 0)
                <div class="sub-categories-section shop-list-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title styled">
                                    <h3 class="text-capitalize">Sub Category for {{$category->name}}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            <div class="shop-lists">
                                <div class="row">
                                    @foreach($sub_categories as $row)
                                        <?php
                                        if(!empty($row->image)) {
                                            $category_image = asset('public/uploads/category/'.$row->image);
                                        } else {
                                            $category_image = asset('frontend_assets/images/cat-icon.jpg');
                                        }
                                        ?>
                                    <div class="col-md-3 col-4">
                                        <div class="single-shop-list white z-depth-1 p-2">
                                            <a href="{{ url('/service_category/'.$row->slug) }}">
                                                <div class="shop-thumb">
                                                    <img src="{{$category_image}}" alt="{{ $row->name }}" class="img-fluid">
                                                </div>
                                                <div class="shop-name">
                                                    <h5>{{ $row->name }}</h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                    <div class="row">
                        @if(count($service_list) > 0)
                            <div class="col-md-12">
                                <div class="section-title styled">
                                    <h3 class="text-capitalize">Services</h3>
                                </div>
                            </div>
                            @foreach($service_list as $row)
                                <div class="col-md-12">
                                    <div class="service-item-list">
                                        <div class="row">
                                            @if(!empty($row->service_image))
                                            <div class="col-md-4">
                                                <img src="{{ asset('public/uploads/services/'.$row->service_image) }}"
                                                     alt="">
                                            </div>
                                            @endif
                                            <div class="col-md-<?php echo empty($row->service_image) ? '12' : '8'; ?>">
                                                <div class="service-info">
                                                    <a href="{{ url('/service/'.$row->id.'/'.$row->service_slug) }}">
                                                        <h3>{{ $row->service_name }} <span class="badge badge-success">৳ {{number_format($row->price, 2)}}</span></h3>
                                                    </a>
                                                    <p><?php echo mb_substr(strip_tags($row->service_description), 0, 125); ?></p>
                                                    <div class="view-details">
                                                        <a href="{{ url('/service/'.$row->id.'/'.$row->service_slug) }}"><i
                                                                    class="fa fa-eye"></i> View Details</a>
                                                        <div class="service_district_name">
                                                            <p>{{$row->store_name}} / {{Helpers::districtName($row->district)}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="col-md-12">
                                {{$service_list->links()}}
                            </div>
                        @else
                            <div class="col-md-12">
                                <h4>Service Not Found...</h4>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--shop page content section end-->
@endsection