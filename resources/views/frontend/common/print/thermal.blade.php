<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Print</title>
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <style>
        body {
            font-size: 15px !important;
            font-weight: 700 !important;
            line-height: 20px;
        }
        .print-section-wrapper {
            width: 260px;
            padding: 5px;
        }

        /*.system-logo {*/
        /*    display: grid;*/
        /*    grid-template-columns: 40% 1fr;*/
        /*    align-items: center;*/
        /*    grid-gap: 5px;*/
        /*}*/
        .system-logo img {
            width: 105px;
            height: auto;
            margin-bottom: 10px;
            margin-top: 10px;
        }
        .system-logo h1 {
            font-size: 1em;
            margin: 0;
        }
        table.table.table-bordered {
            font-size: 0.75em;
        }

        .table td, .table th {
            padding: 0.2rem;
        }
        .nit-price p {
            text-align: left;
            font-size: 0.8em;
            font-weight: 700;
            margin-bottom: 5px;
        }

        .customer-details {
            margin-bottom: 15px;
        }

        p.in-word {
            margin-top: 15px;
            font-size: 0.8em;
            font-weight: 700;
            margin-bottom: 40px;
        }

        .signature-wrapper p {
            margin-bottom: 30px;
            font-size: 0.8em;
        }

        .signature-wrapper {
            text-align: center;
        }
        .signature-wrapper p {
            font-weight: 700;
            border-top: 1px dashed #333;
        }
        .customer-details p {
            font-size: 0.8em;
            font-weight: 700;
            border-bottom: 1px dashed #333;
        }
        .invoice-text{
            text-align: center;
        }
        .invoice-text p {
            font-weight: 700;
            display: inline-block;
            color: #4a4a4a;
            padding: 10px 30px;
            font-size: 1.2em;
            text-transform: uppercase;
            margin-bottom: 0;
        }
        .system-info{
            margin: 15px 0;
        }
        .system-info p {
            font-size: 0.8em;
            font-weight: 700;
            margin-bottom: 5px;
        }
        .print-section-wrapper .header {
            margin-bottom: 10px;
            text-align: center;
        }
        .table td {
            font-weight: 700;
        }
        button.print_btn {
            float: right;
            top: 10px;
            cursor: pointer;
            background: #555;
            color: #fff;
            border: 0;
            padding: 3px 10px;
            left: 10px;
            position: fixed;
            text-transform: uppercase;
            z-index: 99999999;
        }
        @media print {
            button.print_btn {
                display: none;
            }
        }
    </style>
</head>
<body>

<div class="print-section-wrapper">
    <button class="print_btn" onclick="window.print()">Print</button>
    <div class="header">
        <div class="system-logo">
            @if($print_type == 'order')
            <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid">
            <h1>{{$site_settings->web_title}}</h1>
            @else
            <img src="{{ asset('public/uploads/store/'.$order_details->logo) }}" style="opacity: 0.05;" alt="" class="img-fluid">
            <h1>{{$order_details->store_name}}</h1>
            @endif
        </div>
        <!--@if($print_type == 'invoice')-->
        <!--<div class="system-info">-->
        <!--    <p>TIN NO : {{$order_details->tin_no}}</p>-->
        <!--    <p>SR NO : {{$order_details->serial_no}}</p>-->
        <!--    <p>License NO : {{$order_details->license_no}}</p>-->
        <!--</div>-->
        <!--@endif-->
        <div class="system-info">

            @if($print_type == 'invoice')
            <p>Mobile Number : {{$order_details->store_phone_number}}</p>
            <?php $date_time = new DateTime($order_details->created_at); ?>
            <p>Date : {{$date_time->format('d F Y h:i a')}}</p>
            @else
            <p>Mobile Number : {{$site_settings->phone_number}}</p>
            <?php $date_time = new DateTime($order_details->order_date); ?>
            <p>Date : {{$date_time->format('d F Y h:i a')}}</p>
            @endif
        </div>
        <div class="invoice-text">
            <p>Invoice No : {{$order_details->id}}</p>
        </div>
    </div>
    <div class="text-center">
        <p><strong>Customer:</strong> {{ $order_details->name }}, {{ $order_details->phone }}, {{ !empty($order_details->email) ? $order_details->email." , " : '' }} {{ $order_details->address }} </p>

    </div>
    <div class="order-summary">
        <table class="table table-bordered">
            <thead class="thead-default">
            <tr>
                <th>Product Name</th>
                <th class="text-center">Qty</th>
                <th class="text-right">Rate</th>
                <th class="text-right">SubTotal</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 0; $subtotal = 0; ?>
            @foreach($order_items as $row)
            <?php
                                $i++; $subtotal += $row->sub_total;
            ?>
            <tr>
                <td>{{$row->product_name}}</td>
                <td class="text-center">{{$row->quantity}}</td>
                <td class="text-right">{{number_format($row->price, 2)}}</td>
                <td class="text-right">{{number_format($row->sub_total, 2)}}</td>
            </tr>
            @endforeach
            </tbody>
            <?php $nit_price = (($subtotal + $order_details->delivery_charge) - $order_details->discount); ?>
            <tfoot>
                <tr>
                    <td class="text-right" colspan="3"><strong>Subtotal</strong></td>
                    <td class="text-right"><strong>৳{{number_format($subtotal,2 )}}</strong></td>
                </tr>
                <tr>
                    <td class="text-right" colspan="3"><strong>Discount</strong></td>
                    <td class="text-right"><strong>৳{{number_format($order_details->discount,2 )}}</strong></td>
                </tr>
                @if($order_details->delivery_charge > 0)
                <tr>
                    <td class="text-right" colspan="3"><strong>Delivery Charge</strong></td>
                    <td class="text-right"><strong>৳{{$order_details->delivery_charge}}</strong></td>
                </tr>
                @endif
                <tr>
                    <td class="text-right" colspan="3"><strong>Grand Total</strong></td>
                    <td class="text-right"><strong>৳{{number_format($nit_price,2 )}}</strong></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <p class="in-word text-capitalize">In word : {{ Helpers::convertNumberToWord($nit_price) }}</p>
    <div class="signature-wrapper">
        <p>Signature of Customer</p>
    </div>
    <div class="signature-wrapper">
        <p>Signature of Salesman</p>
    </div>
</div>
<script>
    window.print();
</script>
</body>
</html>
