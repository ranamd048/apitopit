<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Print</title>
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <style>
        .system-logo img {
            width: 100px !important;
        }
        .system-logo {overflow: hidden;margin-top: 15px;margin-bottom: 20px;}
        .system-logo h1 {
            display: inline-block;
            margin-left: 20px;
            font-size: 32px;
            text-transform: uppercase;
            font-weight: 700;
        }
        .print-section-wrapper {
            padding: 20px;
            overflow: hidden;
        }
        .system-info h1 {
            font-size: 42px;
            text-transform: uppercase;
        }
        .print-section-wrapper .header {
            overflow: hidden;
            margin-bottom: 40px;
        }
        .print-section-wrapper .header img {
            width: 100%;
            height: auto;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #333 !important;
        }
        .system-info p {
            margin-bottom: 5px;
            font-weight: 700;
        }
        .invoice-text {
            text-align: center;
        }
        .invoice-text p {
            background-color: #333;
            display: inline-block;
            color: #fff;
            padding: 10px 30px;
            text-transform: uppercase;
        }
        .customer-details {
            margin-bottom: 30px;
            overflow: hidden;
        }
        .customer-details p {
            font-weight: 700;
            border-bottom: 1px dashed #333;
            display: block;
        }
        .nit-price {
            overflow: hidden;
        }
        .nit-price p {
            font-weight: 700;
            text-align: right;
            margin-bottom: 5px;
        }
        p.in-word {
            font-weight: 700;
            margin-top: 40px;
        }
        .signature-wrapper {
            overflow: hidden;
            margin-top: 40px;
        }
        .signature-wrapper p {
            font-weight: 700;
            border-top: 1px dashed #333;
            display: inline-block;
        }
		.system-logo p {
			margin-left: 105px;
			font-size: 18px;
			margin-top: -32px;
		}
		.row.system-info {
			margin-bottom: 15px;
		}
        @media print {
            body {background-color: #FFF4AA !important; -webkit-print-color-adjust: exact;}
        }
    </style>
</head>
<body>
    <div class="print-section-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <h6 class="text-center">Bismillahir Rahmanir Rahim</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="system-logo">
                                <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid">
                                @if($print_type == 'order')
                                <h1>{{$site_settings->web_title}}</h1>
                                @else
                                    <h1>{{$order_details->store_name}}</h1>
                                @endif
								<p><i class="fa fa-envelope"></i> {{$site_settings->email}}, <i class="fa fa-phone"></i> {{$site_settings->phone_number}}</p>
                                </div>
                                
                            </div>
                        </div>

                        @if($print_type == 'invoice')
                        <div class="row system-info">
                            <div class="col-md-4 text-left">
                            <p>TIN NO : {{$order_details->tin_no}}</p>
                            </div>
                            <div class="col-md-4 text-center">
                            <p>SR NO : {{$order_details->serial_no}}</p>
                            </div>
                            <div class="col-md-4 text-right">
                            <p>SR NO : {{$order_details->serial_no}}</p>
                            </div>
                        </div>
                        @endif
                        
                        <div class="invoice-text">
                            <p>Invoice No : {{$order_details->id}}</p> <br>
							@if($print_type == 'invoice')
                            <?php $date_time = new DateTime($order_details->created_at); ?>
                            <span style="font-size: 18px">Date : {{$date_time->format('d F Y h:i a')}}</span>
                            @else
                            <?php $date_time = new DateTime($order_details->order_date); ?>
                            <span style="font-size: 18px">Date : {{$date_time->format('d F Y h:i a')}}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="customer-details">
                <div class="row">
                    <div class="col-md-6">
                        <p>Customer's Name : {{$order_details->name}}</p>
                    </div>
                    <?php
                        $length = 6;
                        $char = 0;
                        $type = 'd';
                        $format = "%{$char}{$length}{$type}"; // or "$010d";
                        $code_no = sprintf($format, $order_details->user_id);
                        $sr_tm_code = Helpers::sr_tm_code($order_details->user_id);
                        $sr_code = sprintf($format, $sr_tm_code['sr_code']);
                        $tm_code = sprintf($format, $sr_tm_code['tm_code']);
                    ?>
                    <div class="col-md-6">
                        <p>Code No : <?php echo $code_no; ?></p>
                    </div>
                    <div class="col-md-12">
                        <p>Address : {{$order_details->address}}</p>
                    </div>
                    <div class="col-md-6">
                        <p>S R Code : <?php echo $sr_code; ?></p>
                    </div>
                    <div class="col-md-6">
                        <p>T M Code : <?php echo $tm_code; ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="order-summary">
                        <table class="table table-bordered">
                            <thead class="thead-default">
                            <tr>
                                <th>Sr.</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th style="text-align: right !important">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; $subtotal = 0; ?>
                            @foreach($order_items as $row)
                                <?php
                                $i++; $subtotal += $row->sub_total;
                                ?>
                                <tr>
                                    <th>{{$i}}</th>
                                    <td>{{$row->product_name}}</td>
                                    <td>{{$row->quantity}}</td>
                                    <td>৳ {{number_format($row->price, 2)}}</td>
                                    <td align="right">৳ {{number_format($row->sub_total, 2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php $nit_price = ($subtotal - $order_details->discount); ?>
                <div class="col-md-6">
                    <p class="in-word text-capitalize">In word of Nit Price : {{ Helpers::convertNumberToWord($nit_price) }} Only</p>
                </div>
                <?php $discount_percentange = ($order_details->discount * 100) / $subtotal; ?>
                <div class="col-md-6 nit-price">
                    <p>Total = ৳ {{number_format($subtotal,2 )}}</p>
                    <p class="border-bottom">Discount {{ number_format($discount_percentange) }} % = ৳ {{number_format($order_details->discount,2 )}}</p>
                    <p>Grand Total = ৳ {{number_format($nit_price,2 )}}</p>
                </div>
            </div>
            <div class="row signature-wrapper">
                <div class="col-md-6">
                    <p>Signature of Salesman</p>
                </div>
                <div class="col-md-6 text-right">
                    <p>Signature of Customer</p>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.print();
    </script>
</body>
</html>