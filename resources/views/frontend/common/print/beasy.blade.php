<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Print</title>
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <style>
        .system-logo img {
            width: 160px !important;
        }
        .system-logo {overflow: hidden;margin-top: 15px;margin-bottom: 20px;}
        .system-logo h1 {
            display: inline-block;
            margin-left: 20px;
            font-size: 26px;
            text-transform: uppercase;
            font-weight: 700;
        }
        .print-section-wrapper {
            padding: 20px;
            overflow: hidden;
        }
        .system-info h1 {
            font-size: 42px;
            text-transform: uppercase;
        }
        .print-section-wrapper .header {
            overflow: hidden;
            margin-bottom: 40px;
        }
        .print-section-wrapper .header img {
            width: 100%;
            height: auto;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #333 !important;
        }
        .system-info p {
            margin-bottom: 5px;
            font-weight: 700;
        }
        .invoice-text {
            text-align: center;
            margin-top: 20px;
        }
        .invoice-text p {
            background-color: #333;
            display: inline-block;
            color: #fff;
            padding: 10px 30px;
            text-transform: uppercase;
        }
        .customer-details {
            margin-bottom: 30px;
            overflow: hidden;
        }
        .customer-details p {
            font-weight: 700;
            border-bottom: 1px dashed #333;
            display: block;
        }
        .nit-price {
            overflow: hidden;
        }
        .nit-price p {
            font-weight: 700;
            text-align: right;
            margin-bottom: 5px;
        }
        p.in-word {
            font-weight: 700;
            margin-top: 40px;
        }
        .signature-wrapper {
            overflow: hidden;
            margin-top: 40px;
        }
        .signature-wrapper p {
            font-weight: 700;
            border-top: 1px dashed #333;
            display: inline-block;
        }
        button.print_btn {
            float: right;
            top: 10px;
            cursor: pointer;
            background: #555;
            color: #fff;
            border: 0;
            padding: 3px 10px;
            left: 10px;
            position: fixed;
            text-transform: uppercase;
            z-index: 999999999;
        }
        @media print {
            body {background-color: #FFF4AA !important; -webkit-print-color-adjust: exact;}
            button.print_btn {
                display: none;
            }
        }
        @media only screen and (max-width: 767px) {
            .print-section-wrapper {
                overflow: scroll;
            }
            .row.system-info .text-right {
                text-align: left !important;
            }
            .nit-price p {
                text-align: left;
            }

        }
    </style>
</head>
<body>
    <div class="print-section-wrapper">
        <button class="print_btn" onclick="window.print()">Print</button>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <h6 class="text-center">Bismillahir Rahmanir Rahim</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="system-logo">
                                @if($print_type == 'order')
                                    <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid">
                                    <h1>{{$site_settings->web_title}}</h1>
                                @else
                                    <img src="{{ asset('public/uploads/store/'.$order_details->logo) }}" style="opacity: 0.05;" alt="" class="img-fluid">
                                    <h1>{{$order_details->store_name}}</h1>
                                @endif
                                </div>
                            </div>
                        </div>
                        @if($print_type == 'invoice')
                        <div class="row system-info">
                            <div class="col-md-4 text-left">
                            <p>TIN NO : {{$order_details->tin_no}}</p>
                            </div>
                            <div class="col-md-4 text-center">
                            <p>SR NO : {{$order_details->serial_no}}</p>
                            </div>
                            <div class="col-md-4 text-right">
                            <p>License NO : {{$order_details->license_no}}</p>
                            </div>
                        </div>
                        @endif
                        <div class="row system-info">
                            <div class="col-md-6 text-left">
                                @if($print_type == 'invoice')
                                    <p>Email Address : {{$order_details->email}}</p>
                                @else
                                    <p>Email Address : {{$site_settings->email}}</p>
                                @endif
                            </div>
                            <div class="col-md-6 text-right">
                            @if($print_type == 'invoice')
                            <p>Mobile Number : {{$order_details->store_phone_number}}</p>
                            <?php $date_time = new DateTime($order_details->created_at); ?>
                            <p>Date : {{$date_time->format('d F Y h:i a')}}</p>
                            @else
                            <p>Mobile Number : {{$site_settings->phone_number}}</p>
                            <?php $date_time = new DateTime($order_details->order_date); ?>
                            <p>Date : {{$date_time->format('d F Y h:i a')}}</p>
                            @endif
                            </div>
                        </div>
                        <div class="invoice-text">
                            <p>Invoice No : {{$order_details->id}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="customer-details">
                <div class="row">
                    <div class="col-md-6">
                        <p>Customer's Name : {{$order_details->name}}</p>
                    </div>
                    <?php
                        $length = 6;
                        $char = 0;
                        $type = 'd';
                        $format = "%{$char}{$length}{$type}"; // or "$010d";
                        $code_no = sprintf($format, $order_details->user_id);
                        $sr_tm_code = Helpers::sr_tm_code($order_details->user_id);
                        $sr_code = sprintf($format, $sr_tm_code['sr_code']);
                        $tm_code = sprintf($format, $sr_tm_code['tm_code']);
                    ?>
                    <div class="col-md-6">
                        <p>Code No : <?php echo $code_no; ?></p>
                    </div>
                    <div class="col-md-12">
                        <p>Address : {{$order_details->address}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="order-summary">
                        <table class="table table-bordered">
                            <thead class="thead-default">
                            <tr>
                                <th>Sr.</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Amount</th>
                                <th>Discount</th>
                                <th>SubTotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; $subtotal = 0; $total_qty = 0; $total_rate = 0; $_total_amount = 0; $_total_discount = 0; $_total_per_subtotal = 0; ?>
                            @foreach($order_items as $row)
                                <?php
                                $i++; $subtotal += $row->sub_total;
                                $product_discount_percent = round((($row->discount_amount * $row->quantity) * 100) / ($row->price * $row->quantity), 2);

                                $total_qty += $row->quantity;
                                $total_rate += $row->price;
                                $_total_amount += ($row->quantity * $row->price);
                                $_total_discount += ($row->discount_amount * $row->quantity);
                                $_total_per_subtotal += $row->sub_total;
                                ?>
                                <tr>
                                    <th>{{$i}}</th>
                                    <td>{{$row->product_name}}</td>
                                    <td>{{$row->quantity}}</td>
                                    <td>৳{{number_format($row->price, 2)}}</td>
                                    <td>৳{{number_format($row->price * $row->quantity, 2)}}</td>
                                    <td>{{ $product_discount_percent }}% (৳{{number_format($row->discount_amount * $row->quantity, 2)}})</td>
                                    <td>৳{{number_format($row->sub_total, 2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2">Total <span style="float: right">{{ $i }}</span></th>
                                    <th>{{ $total_qty}}</th>
                                    <th>৳{{ number_format($total_rate, 2)}}</th>
                                    <th>৳{{ number_format($_total_amount, 2)}}</th>
                                    <th>৳{{ number_format($_total_discount, 2)}}</th>
                                    <th>৳{{ number_format($_total_per_subtotal, 2)}}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <?php $nit_price = (($subtotal + $order_details->delivery_charge) - $order_details->discount); ?>
                <div class="col-md-6">
                    <p class="in-word text-capitalize">In word of Nit Price : {{ Helpers::convertNumberToWord($nit_price) }}</p>
                </div>
                <?php $order_discount_percent =  round(($order_details->discount * 100) / $subtotal, 2); ?>
                <div class="col-md-6 nit-price">
                    <p>Total Amount of all Product = ৳{{number_format($subtotal,2 )}}</p>
                    <p class="border-bottom">Discount Amount = {{$order_discount_percent}}% (৳{{number_format($order_details->discount,2 )}})</p>
                    @if($order_details->delivery_charge > 0)
                        <p class="border-bottom">Delivery Charge = ৳{{$order_details->delivery_charge}}</p>
                    @endif
                    <p>Nit Price = ৳{{number_format($nit_price,2 )}}</p>
                </div>
            </div>
            <div class="row signature-wrapper">
                <div class="col-md-6">
                    <p>Signature of Salesman</p>
                </div>
                <div class="col-md-6 text-right">
                    <p>Signature of Customer</p>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.print();
    </script>
</body>
</html>
