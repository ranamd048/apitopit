<div class="ssp-product-section">
            <div class="row">
            <div class="col-md-3">
<div class="mobile-sidebar d-md-none d-block mb-5">
    <a href="#" data-target="category-nav" class="sidenav-trigger d-flex">
        <i class="material-icons">menu</i>
        <span class="my-auto text-capitalize font-weight-bold ml-2 cat_name">All Products</span>
    </a>
    <div id="category-nav" class="sidenav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ssp-sidebar">
                        <!-- <div class="sidebar-search">
                            <div class="input-field">
                                <input type="text" id="mssp-search">
                                <label for="ssp-search">Search</label>
                            </div>
                            <button type="submit"><i class="material-icons">search</i></button>
                        </div> -->
@if(count($shop_categories) > 0)
<ul>
<li><a href=""><span class="shop_category_item">All Products</span></a></li>
<!-- @foreach($shop_categories as $row)
    <li><a href="#" class="shop_category_item" data-categoryid="{{$row->id}}">{{ $row->name }} <i class="fa fa-angle-right"></i></a></li>
@endforeach -->
@foreach($shop_categories as $row)
<?php $sub_categories = Helpers::getShopSubCategories($shop->id, $row->id);?>
<li><a href="#"><span class="shop_category_item" data-categoryid="{{ $row->id }}" data-category="category_id">{{$row->name}}</span> @if(count($sub_categories) > 0) <i class="fa fa-angle-right submenu_show" data-id="{{$row->id}}"></i> @endif</a>
    @if(count($sub_categories) > 0)
        <ul class="mobile_subcategory" id="category_id_{{$row->id}}">
            @foreach($sub_categories as $sub_cat)
                <?php $second_sub_cats = Helpers::getShopSubCategories($shop->id, $sub_cat->id, 'sub_subcategory_id');?>
            <li><a href="#"> @if(count($second_sub_cats) > 0) <i class="fa fa-angle-right submenu_show" data-id="{{$sub_cat->id}}"></i> @endif <span class="shop_category_item" data-categoryid="{{ $sub_cat->id }}" data-category="subcategory_id">{{$sub_cat->name}}</span></a>
                @if(count($second_sub_cats) > 0)
                <ul class="second mobile_subcategory" id="category_id_{{$sub_cat->id}}">
                    @foreach($second_sub_cats as $second_cat)
                    <li><a href="#"><span class="shop_category_item" data-categoryid="{{ $second_cat->id }}" data-category="sub_subcategory_id">{{$second_cat->name}}</span></a></li>
                    @endforeach
                </ul>
                @endif
            </li>
            @endforeach
        </ul>
    @endif
</li>
@endforeach
</ul>
@endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ssp-sidebar d-md-block d-none">
    <div class="sidebar-search">
        <div class="input-field">
            <input type="text" id="shop_product_search">
            <label for="ssp-search">Search</label>
        </div>
        <button type="submit"><i class="material-icons">search</i></button>
    </div>
    @if(count($shop_categories) > 0)
        <ul>
            <li><a href="" class="shop_category_item">ALL PRODUCTS</a></li>
            @foreach($shop_categories as $row)
            <?php $sub_categories = Helpers::getShopSubCategories($shop->id, $row->id);?>
            <li><a href="#" class="shop_category_item" data-categoryid="{{ $row->id }}" data-category="category_id">{{$row->name}} @if(count($sub_categories) > 0) <i class="fa fa-angle-right"></i> @endif</a>
                @if(count($sub_categories) > 0)
                    <ul class="subcategory">
                        @foreach($sub_categories as $sub_cat)
                            <?php $second_sub_cats = Helpers::getShopSubCategories($shop->id, $sub_cat->id, 'sub_subcategory_id');?>
                        <li><a href="#" class="shop_category_item" data-categoryid="{{ $sub_cat->id }}" data-category="subcategory_id"> @if(count($second_sub_cats) > 0) <i class="fa fa-angle-right"></i> @endif {{$sub_cat->name}}</a>
                            @if(count($second_sub_cats) > 0)
                            <ul class="second_sub">
                                @foreach($second_sub_cats as $second_cat)
                                <li><a href="#" class="shop_category_item" data-categoryid="{{ $second_cat->id }}" data-category="sub_subcategory_id">{{$second_cat->name}}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                @endif
            </li>
            @endforeach
        </ul>
    @endif
</div>
</div>
<div class="col-md-9">
<div class="ssp-products">
    <div class="row">
        <div class="col-md-12">
            <div class="cat_name">All Products</div>
        </div>
    </div>
    <div class="row" id="load_shop_products"></div>
</div>
</div>
            </div>
        </div>