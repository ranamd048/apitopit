<?php $category_list = Helpers::getServiceParentCategory(); ?>
<div class="mobile-sidebar d-md-none d-block mb-5">
    <a href="#" data-target="category-nav" class="sidenav-trigger d-flex">
        <i class="material-icons">menu</i>
        <span class="my-auto text-capitalize font-weight-bold ml-2">Filter</span>
    </a>
    <div id="category-nav" class="sidenav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ssp-sidebar">
                        <div class="sidebar-search">
                            <form action="{{url('search')}}" method="GET">
                                <input type="hidden" name="search_type" value="product">
                                <div class="input-field">
                                    <input type="text" id="keyword" name="keyword">
                                    <label for="ssp-search">Search</label>
                                </div>
                                <button type="submit"><i class="material-icons">search</i></button>
                            </form>
                        </div>
                        @if(count($category_list) > 0)
                        <ul>
                            @foreach($category_list as $row)
                                <?php $sub_categories = Helpers::getSubCategories($row->id);?>
                            <li><a href="{{url('service_category/'.$row->slug)}}">{{$row->name}} @if(count($sub_categories) > 0) <i class="fa fa-angle-right submenu_show" data-id="{{$row->id}}"></i> @endif</a>
                                @if(count($sub_categories) > 0)
                                <ul class="mobile_subcategory" id="category_id_{{$row->id}}">
                                    @foreach($sub_categories as $sub_cat)
                                        <?php $second_sub_cats = Helpers::getSubCategories($sub_cat->id);?>
                                        <li><a href="{{url('service_category/'.$sub_cat->slug)}}">{{$sub_cat->name}} @if(count($second_sub_cats) > 0) <i class="fa fa-angle-right submenu_show" data-id="{{$sub_cat->id}}"></i> @endif</a>
                                            @if(count($second_sub_cats) > 0)
                                                <ul class="second mobile_subcategory" id="category_id_{{$sub_cat->id}}">
                                                    @foreach($second_sub_cats as $second_cat)
                                                        <li><a href="{{url('service_category/'.$second_cat->slug)}}">{{$second_cat->name}}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ssp-sidebar d-md-block d-none">
    <div class="sidebar-search">
        <form action="{{url('search')}}" method="GET">
            <input type="hidden" name="search_type" value="service">
            <div class="input-field">
                <input type="text" id="keyword" name="keyword">
                <label for="ssp-search">Search</label>
            </div>
            <button type="submit"><i class="material-icons">search</i></button>
        </form>
    </div>
    @if(count($category_list) > 0)
    <ul>
        @foreach($category_list as $row)
            <?php $sub_categories = Helpers::getSubCategories($row->id);?>
            <li><a href="{{url('service_category/'.$row->slug)}}">{{$row->name}} @if(count($sub_categories) > 0) <i class="fa fa-angle-right"></i> @endif</a>
                @if(count($sub_categories) > 0)
                    <ul class="subcategory">
                        @foreach($sub_categories as $sub_cat)
                            <?php $second_sub_cats = Helpers::getSubCategories($sub_cat->id);?>
                        <li><a href="{{url('service_category/'.$sub_cat->slug)}}"> @if(count($second_sub_cats) > 0) <i class="fa fa-angle-right"></i> @endif {{$sub_cat->name}}</a>
                            @if(count($second_sub_cats) > 0)
                            <ul class="second_sub">
                                @foreach($second_sub_cats as $second_cat)
                                <li><a href="{{url('service_category/'.$second_cat->slug)}}">{{$second_cat->name}}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
    @endif
</div>

<script>
    $(document).ready(function() {
        $('.mobile-sidebar .ssp-sidebar ul li a i.submenu_show').click(function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            $(this).toggleClass('fa-angle-down');
            $('#category_id_'+id).toggleClass('show');
        });
    });
</script>