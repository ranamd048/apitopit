<?php $layout_file = 'frontend.'.$site_settings->theme.'.layout'; ?>
@extends($layout_file)
@section('title')
    Cart Page
@endsection

@section('main_content')
    <div class="breadcrumb-area" style="background-image: url('{{ asset("frontend_assets/images/single-shop-banner.jpg") }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs text-center">
                        <h1>Cart</h1>
                        <ul>
                            <li><a href="{{url('/')}}">home</a></li>
                            <li>/</li>
                            <li>Cart Page</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- cart view start-->
    <section class="cart-view-section section-padding mb-5">
        <div class="container">
            <div class="row">
            @if($cart_items->count() > 0)
                <div class="col-lg-8 mb-lg-0 mb-5">
                    <div class="cart-item-details">
                        
                        <ul>
                            @foreach($cart_items as $row)
                             <?php
                                $product_price = Helpers::is_promo_or_normal_product($row->id);
                                $shop_info = Helpers::shop_info($row->attributes->store_id);
                             ?>
                            <li class="c-data text-uppercase mb-4 shadow cart-item" id="cart_item_{{$row->id}}">
                                <div class="row p-3">
                                    <div class="col-7 d-flex">
                                        <div class="cart-item custom-control">
                                            <div class="d-flex">
                                                <div class="c-thumb">
                                                    <img src="{{ asset('public/uploads/products/thumbnail/'.$row->attributes->image) }}" alt="" class="img-fluid">
                                                </div>
                                                <span class="c-info ml-2">
                                                <p class="mb-0">{{$row->name}}</p>
                                                <p class="shop_name"><a target="_blank" href="{{url('shop/'.$shop_info->slug)}}" class="badge badge-success">{{$shop_info->store_name}}</a></p>
                                                <span class="c-action">
                                                    <i class="fa fa-heart mr-3 add_to_wishlist" data-productid="{{$row->id}}"></i>
                                                    <i class="fa fa-trash remove-cart-item" data-rowid="{{$row->id}}"></i>
                                                </span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5 text-center">
                                        <div class="row">
                                            <div class="col-6">
                                                <?php
                                                $total_price = ($row->price * $row->quantity);
                                                ?>
                                                <p class="font-weight-bold text-danger m-0" id="cart_price_{{$row->id}}">৳ {{number_format($total_price, 2)}}</p>
                                            </div>
                                            <div class="col-6">
                                                <div class="c-qty d-flex mt-2 cart-plus-minus">
                                                    <i class="fa fa-minus m-auto bg-light p-2 minus-quantity" data-id="{{$row->id}}" data-price="{{$product_price}}"></i>
                                                    <span class="m-auto" id="qty_value_{{$row->id}}">{{$row->quantity}}</span>
                                                    <i class="fa fa-plus m-auto bg-light p-2 plus-quantity" data-id="{{$row->id}}" data-price="{{$product_price}}"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="cart-totals z-depth-1 rounded" id="cart_summary"></div>
                </div>
                @else
                    <h4>Your cart is empty! <a class="btn btn-default" href="{{url('product_list')}}">Back to Product Page</a></h4>
                @endif
            </div>
        </div>
    </section>
    <!-- cart view end-->
@endsection