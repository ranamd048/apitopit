<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>@yield('title')</title>
    @yield('meta_content')
    <link rel="shortcut icon" href="{{ asset('public/uploads/'.$site_settings->favicon) }}">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/material_icon.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/materialize.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/magnific-popup.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/solaimanlipi.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/jquery.exzoom.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/custom.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/responsive.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/common.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/new_style.css?<?php echo time(); ?>">

    <style>
        :root {
            --theme-color: <?php echo (!empty($site_settings->theme_color)) ? $site_settings->theme_color: '#FF5F00'; ?>;
        }
        nav {
            background: transparent;
            box-shadow: none;
        }
        nav .pagination li a {
            display: block;
        }
        nav .pagination li.page-item.active span.page-link {
            cursor: default;
        }
        .pre-loader {
            background-color: #fff;
            position: fixed;
            height: 100%;
            width: 100%;
            left: 0;
            top: 0;
            z-index: 999999999;
        }
        .sk-fading-circle {
            width: 40px;
            height: 40px;
            position: relative;
            top: 50%;
            left: 50%;
            margin-left: -20px;
            margin-top: -20px;
        }
        .sk-fading-circle .sk-circle {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
        }
        .sk-fading-circle .sk-circle:before {
            content: '';
            display: block;
            margin: 0 auto;
            width: 15%;
            height: 15%;
            background-color: var(--theme-color);
            border-radius: 100%;
            -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
            animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
        }
        .sk-fading-circle .sk-circle2 {
            -webkit-transform: rotate(30deg);
            -ms-transform: rotate(30deg);
            transform: rotate(30deg);
        }
        .sk-fading-circle .sk-circle3 {
            -webkit-transform: rotate(60deg);
            -ms-transform: rotate(60deg);
            transform: rotate(60deg);
        }
        .sk-fading-circle .sk-circle4 {
            -webkit-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg);
        }
        .sk-fading-circle .sk-circle5 {
            -webkit-transform: rotate(120deg);
            -ms-transform: rotate(120deg);
            transform: rotate(120deg);
        }
        .sk-fading-circle .sk-circle6 {
            -webkit-transform: rotate(150deg);
            -ms-transform: rotate(150deg);
            transform: rotate(150deg);
        }
        .sk-fading-circle .sk-circle7 {
            -webkit-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
        }
        .sk-fading-circle .sk-circle8 {
            -webkit-transform: rotate(210deg);
            -ms-transform: rotate(210deg);
            transform: rotate(210deg);
        }
        .sk-fading-circle .sk-circle9 {
            -webkit-transform: rotate(240deg);
            -ms-transform: rotate(240deg);
            transform: rotate(240deg);
        }
        .sk-fading-circle .sk-circle10 {
            -webkit-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            transform: rotate(270deg);
        }
        .sk-fading-circle .sk-circle11 {
            -webkit-transform: rotate(300deg);
            -ms-transform: rotate(300deg);
            transform: rotate(300deg);
        }
        .sk-fading-circle .sk-circle12 {
            -webkit-transform: rotate(330deg);
            -ms-transform: rotate(330deg);
            transform: rotate(330deg);
        }
        .sk-fading-circle .sk-circle2:before {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }
        .sk-fading-circle .sk-circle3:before {
            -webkit-animation-delay: -1s;
            animation-delay: -1s;
        }
        .sk-fading-circle .sk-circle4:before {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }
        .sk-fading-circle .sk-circle5:before {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }
        .sk-fading-circle .sk-circle6:before {
            -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s;
        }
        .sk-fading-circle .sk-circle7:before {
            -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s;
        }
        .sk-fading-circle .sk-circle8:before {
            -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s;
        }
        .sk-fading-circle .sk-circle9:before {
            -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s;
        }
        .sk-fading-circle .sk-circle10:before {
            -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s;
        }
        .sk-fading-circle .sk-circle11:before {
            -webkit-animation-delay: -0.2s;
            animation-delay: -0.2s;
        }
        .sk-fading-circle .sk-circle12:before {
            -webkit-animation-delay: -0.1s;
            animation-delay: -0.1s;
        }
        @-webkit-keyframes sk-circleFadeDelay {
            0%, 39%, 100% {
                opacity: 0;
            }
            40% {
                opacity: 1;
            }
        }
        @keyframes sk-circleFadeDelay {
            0%, 39%, 100% {
                opacity: 0;
            }
            40% {
                opacity: 1;
            }
        }
        nav ul.pagination {
            display: block !important;
        }
        nav ul.pagination li {
            height: 36px !important;
        }
    </style>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
    <script src="{{ asset('frontend_assets') }}/common/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('frontend_assets') }}/common/js/materialize.min.js"></script>
    <!--LAZY LOADER CDN-->
    <script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js"></script>
    @yield('custom_scripts')
    <script type="text/javascript">
        var APP_URL = {!! json_encode(url('/')) !!};
    </script>
</head>
<body>
{{--<div class="pre-loader">--}}
{{--  <div class="sk-fading-circle">--}}
{{--    <div class="sk-circle1 sk-circle"></div>--}}
{{--    <div class="sk-circle2 sk-circle"></div>--}}
{{--    <div class="sk-circle3 sk-circle"></div>--}}
{{--    <div class="sk-circle4 sk-circle"></div>--}}
{{--    <div class="sk-circle5 sk-circle"></div>--}}
{{--    <div class="sk-circle6 sk-circle"></div>--}}
{{--    <div class="sk-circle7 sk-circle"></div>--}}
{{--    <div class="sk-circle8 sk-circle"></div>--}}
{{--    <div class="sk-circle9 sk-circle"></div>--}}
{{--    <div class="sk-circle10 sk-circle"></div>--}}
{{--    <div class="sk-circle11 sk-circle"></div>--}}
{{--    <div class="sk-circle12 sk-circle"></div>--}}
{{--  </div>--}}
{{--</div>--}}
<!--header section start-->

<?php if($site_settings->logo):?>
<div style="width: 100%;border-bottom: 1px solid #ddd;">
    <img src="{{ asset('public/uploads/'.$site_settings->banner)  }}" style="width: 100%;min-height: 40px;" />
</div>
<?php endif;?>

<header class="header-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 pr-0">
                <div class="d-flex">
                    <div class="logo">
                        <a href="{{url('/')}}">
                            <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="main-search">
                    <form action="{{ url('/search') }}" method="GET">
                        <input type="search" name="keyword" class="form-control" id="search_header_input" placeholder="Search For...">
                        <select name="search_type" id="search_by_cat_head">
                            <option value="">Search By</option>
                            <option value="product">Product</option>
                            <option value="shop">Shop</option>
                            <option value="service">Service</option>
                        </select>
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="col-md-2 pl-0">
                <div class="user-wrapper">
                    <ul>
                        <li>
                            <a href="{{url('cart')}}">
                                <span class="badge badge-success top_total_cart_amount"></span>
                                <span class="cart">
                                    <i class="material-icons">local_grocery_store</i>
                                    <span class="item-count top_cart_count"></span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <div class="header-navbar-active">
                                <span class="account-access">
                                @if(Session::has('customer_id'))
                                <?php
                                    if(!empty(Auth::user()->photo)) {
                                        $user_photo = asset('public/uploads/customer/'. Auth::user()->photo);
                                    } else {
                                        $user_photo = asset('frontend_assets/images/user_profile.png');
                                    }
                                ?>
                                <img src="{{$user_photo}}" alt="">
                                @else
                                <i class="material-icons">account_circle</i>
                                @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!--header section end-->
<section class="header-bottom-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <div class="navigation mobile-view">
                    <i data-target="header-navigation" class="material-icons sidenav-trigger tooltipped"
                       data-position="right" data-tooltip="Click me to navigate">menu</i>
                    <div id="header-navigation" class="sidenav">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="logo">
                                        <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid">

                                        <span class="sidenav-close float-right p-3">
                                            <i class="material-icons tooltipped" data-position="left"
                                               data-tooltip="Close">close</i>
                                        </span><br>
                                    </div>
                                    <div class="sidenav-wrapper">
                                        <div class="page-list">
                                            <ul>
                                                @if(count($site_menu_items) > 0)
                                                    @foreach($site_menu_items as $menu)
                                                        <?php $sub_menus = DB::table('menu')->select('*')->where('parent_id', '=', $menu->id)->where('status', '=', 1)->get();
                                                        $sub_menus_class = 'nav-item';
                                                        if (count($sub_menus) > 0) {
                                                            $sub_menus_class = 'nav-item dropdown';
                                                        }
                                                        ?>
                                                        <li class="{{$sub_menus_class}}">
                                                            @if(count($sub_menus) > 0)
                                                                <a class="dropdown-toggle" href="#" role="button"
                                                                   data-toggle="dropdown" aria-haspopup="true"
                                                                   aria-expanded="false">
                                                                    {{ $menu->name }}
                                                                </a>
                                                                <div class="dropdown-menu"
                                                                     aria-labelledby="navbarDropdown">
                                                                    @foreach($sub_menus as $sub_menu)
                                                                        <a class="dropdown-item"
                                                                           href="@if($sub_menu->is_custom == 1) {{ $sub_menu->menu_url }} @else {{ url($sub_menu->menu_url) }} @endif">{{ $sub_menu->name }}</a>
                                                                    @endforeach
                                                                </div>
                                                            @else
                                                                <a href="@if($menu->is_custom == 1) {{$menu->menu_url}} @else {{ url($menu->menu_url) }} @endif">{{ $menu->name }}</a>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="secondary-navbar">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="{{url('shop_list')}}">Shops</a></li>
                        <li><a href="{{url('product_list')}}">Products</a></li>
                        <li><a href="{{url('service_list')}}">Services</a></li>
                        <li><a href="{{url('gallery')}}">Gallery</a></li>
                        <li><a href="{{url('blog-list')}}">News</a></li>
                        <li><a href="{{url('registration')}}">Sign Up</a></li>
                        <li><a href="{{url('shop_registration')}}">Shop Registration</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="cur-lang-acc-active">
    <div class="wrap-sidebar">
        <div class="sidebar-nav-icon">
            <button class="op-sidebar-close"><i class="material-icons">close</i></button>
        </div>
        <div class="cur-lang-acc-all">
            <div class="single-currency-language-account">
                <div class="cur-lang-acc-dropdown">
                    <ul>
                        @if(Session::has('customer_id'))
                        <?php
                                if(!empty(Auth::user()->photo)) {
                                    $user_photo = asset('public/uploads/customer/'. Auth::user()->photo);
                                } else {
                                    $user_photo = asset('frontend_assets/images/user_profile.png');
                                }
                            ?>
                            <div class="user">
                                <img src="{{$user_photo}}" alt="{{Auth::user()->name}}">
                                <h4>{{Auth::user()->name}}</h4>
                            </div>
                        @else
                        <div class="user">
                            <img src="{{ asset('frontend_assets/images/user_profile.png') }}" alt="">
                            <h4>User Panel</h4>
                        </div>
                        @endif
                        @if(Session::has('customer_id') || Session::has('distributor_id'))

                            <li><a href="{{ url('customer_profile') }}">My Profile</a></li>
                            <li><a href="{{ url('orders') }}">My Orders</a></li>
                            <li><a href="{{ url('my_services') }}">My Services</a></li>
                            @if($private_settings->wishlist_option == 1)
                            <li><a href="{{ url('wishlist') }}">Wishlist</a></li>
                            @else
                            <li><a href="{{ url('favourites') }}">Favourites</a></li>
                            @endif
                            @if(Session::has('customer_id'))
                            <li><a href="{{url('save_order_items')}}">Save Orders</a></li>
                            @endif
                            <li>
                                <?php $message_count = Helpers::messageCount(); ?>
                                <a id="live_message" href="{{url('customer/messages')}}">Messages @if($message_count > 0) <span class="badge badge-danger text-white">{{ $message_count }}</span> @endif</a>
                            </li>
                            <li><a href="{{ url('customer_logout') }}">Logout</a></li>
                        @else
                            <li><a href="{{ url('registration') }}">Sign Up</a></li>
                            <li><a href="{{ url('login') }}">Login </a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@yield('main_content')

<div class="mobile-padding"></div>
<!--footer section start-->
<footer class="footer-section">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-3">
                <div class="footer-widget">
                    <div class="f-w-title">
                        <img style="max-width: 120px;height: auto;margin-top: 15px;" src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid">
                        <p>
                            {{ $site_settings->address }} <br>Email: {{ $site_settings->email }} <br>Contact no: {{ $site_settings->phone_number }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-widget footer-menu">
                    <div class="f-w-title">
                        <h5 style="font-weight: 900;">Quick Links</h5>
                        <ul>
                            <li><a href="{{ url('shop_list') }}">Shops</a></li>
                            <li><a href="{{ url('product_list') }}">Products</a></li>
                            <li><a href="{{ url('service_list') }}">Services</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-widget footer-menu">
                    <div class="f-w-title">
                        <h5 style="font-weight: 900;">Quick Links</h5>
                        <ul>
                            <li><a href="{{ url('contact_us') }}">Contact Us</a></li>
                            <li><a href="{{ url('page/privacy-policy') }}">Privacy Policy</a></li>
                            <li><a href="{{ url('page/terms-and-condition') }}">Terms & Condition</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-widget">
                    <div class="f-w-title">
                        <h5 style="font-weight: 900;">Get in Touch</h5>
                        <div class="f-social-link">
                            <a href="{{url('beasy-app.apk')}}">
                                <img style="max-width: 156px;height: auto;margin-bottom: 10px;" src="{{ asset('frontend_assets') }}/images/app.png" alt=""class="img-fluid">
                            </a>
                            <ul class="d-flex">
                                <li>
                                    <a href="{{ $site_settings->fb_link }}"><img
                                                src="{{ asset('frontend_assets') }}/images/Facebook.png" alt=""
                                                class="img-fluid"></a>
                                    <a href="{{ $site_settings->youtube_link }}"><img
                                                src="{{ asset('frontend_assets') }}/images/Youtube.png" alt=""
                                                class="img-fluid"></a>
                                    <a href="{{ $site_settings->twitter_link }}"><img
                                                src="{{ asset('frontend_assets') }}/images/twitter.png" alt=""
                                                class="img-fluid"></a>
                                    <a href="#!"><img src="{{ asset('frontend_assets') }}/images/Instagram.png" alt=""
                                                      class="img-fluid"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom text-center">
        <p class="m-0">
            {{ $site_settings->copyright_text }}
            developed by
            <a href="https://apitopit.com/" style="color: #ddd;">
                Apitopit.com
            </a>
        </p>
    </div>
</footer>

<!-- main-search start -->
<div class="moblie-search-section">
    <div class="close-icon">
        <i class="material-icons">close</i>
    </div>
    <div class="main-search search-form">
        <form action="{{ url('/search') }}" method="GET">
            <input type="search" name="keyword" class="form-control" placeholder="Search For...">
            <select name="search_type" id="">
                <option value="">Search By</option>
                <option value="product">Product</option>
                <option value="shop">Shop</option>
                <option value="service">Service</option>
            </select>
            <button type="submit"><i class="fa fa-search"></i> Search</button>
        </form>
    </div>
</div>

<!--mobile footer start-->
<section class="mobile-footer d-md-none d-block">
    <div class="d-flex">

        <a href="{{url('/product_list')}}" class="mcart-wrapper">
            <i class="fa fa-home"></i>
        </a>
        <button class="mcart-wrapper sidebar-trigger-search">
            <i class="fa fa-search"></i>
        </button>
        <a href="{{url('cart')}}" class="shopping-btn mchat-wrapper">
            <i class="fa fa-shopping-bag"></i>
            <span class="top_cart_count"></span>
            <span class="badge badge-success top_total_cart_amount"></span>
        </a>
        <div class="mchat-wrapper">
            <div class="header-navbar-active">
            <span class="account-access">
                @if(Session::has('customer_id'))
                <?php
                    if(!empty(Auth::user()->photo)) {
                        $user_photo = asset('public/uploads/customer/'. Auth::user()->photo);
                    } else {
                        $user_photo = asset('frontend_assets/images/user_profile.png');
                    }
                ?>
                <img src="{{$user_photo}}" alt="">
                @else
                <i class="material-icons">account_circle</i>
                @endif
            </span>
            </div>
        </div>
    </div>
</section>
<!--footer section end-->

<script src="{{ asset('frontend_assets') }}/js/proper.min.js"></script>
<script src="{{ asset('frontend_assets') }}/js/bootstrap.min.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/sweetalert.min.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/jquery.magnific-popup.min.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/mixitup.js"></script>
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
<script src="{{ asset('frontend_assets') }}/js/owl.carousel.min.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/jquery.exzoom.js"></script>
<script src="{{ asset('frontend_assets') }}/js/custom.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/common.js?<?php echo time(); ?>"></script>
@yield ('footer_scripts')
<script>
    $(document).ready(function () {
        function sidebarNav() {
            var navbarTrigger = $('.header-navbar-active'),
                endTrigger = $('.op-sidebar-close'),
                container = $('.cur-lang-acc-active'),
                wrapper = $('body');

            wrapper.prepend('<div class="body-overlay"></div>');

            navbarTrigger.on('click', function() {
                container.addClass('inside');
                wrapper.addClass('overlay-active');
            });

            endTrigger.on('click', function() {
                container.removeClass('inside');
                wrapper.removeClass('overlay-active');
            });

            $('.body-overlay').on('click', function() {
                container.removeClass('inside');
                wrapper.removeClass('overlay-active');
            });
        };
        sidebarNav();

        $('#search_header_input').autocomplete({
            source: function( request, response ) {
                $.getJSON( "<?=url('/search_product_header');?>", {
                    term: extractLast(request.term),
                    type: $('#search_by_cat_head').val(),
                    search_data: $('#search_header_input').val(),
                }, response );
            },
            minLength: 2
        });

        $('.sidebar-trigger-search').click(function(e) {
            e.preventDefault();
            $('.moblie-search-section').slideDown();
        });
        $('.moblie-search-section .close-icon i').click(function() {
            $('.moblie-search-section').slideUp();
        });

        $(window).scroll(function(e) {
            var newScroll = $(document).scrollTop();
            if (newScroll > 10) {
                $('.header-bottom-section .navigation i').css('top', '22px')
            } else {
                $('.header-bottom-section .navigation i').css('top', '65px')
            }
        });
    });

    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }
</script>
<script>

    (function($){
    'use strict';
        lazyload();
        $(window).on('load', function () {
            if ($(".pre-loader").length > 0)
            {
                $(".pre-loader").fadeOut("slow");
            }
        });
    })(jQuery)
</script>

@if(Session::has('admin_id') || Session::has('customer_id'))
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
    @if(Session::has('customer_id'))
    let channel_name = 'user_' + {{ Session::get('customer_id') }};
    @elseif(Session::has('admin_id') && Session::get('role') == 0)
    let channel_name = 'admin';
    @elseif(Session::has('admin_id') &&  Session::get('role') == 1)
    let channel_name = 'shop_' + {{ Session::get('store_id') }};
    @endif

    var pusher = new Pusher('b2c9e350e5a95fe56339', {
        cluster: 'ap1'
    });

    var channel = pusher.subscribe(channel_name);
    channel.bind('live-message', function(data) {
        you_message(data.message);
    });

    function you_message (obj) {
        let message = '<li class="you">'+
            '<div class="entete">'+
            '<h2>'+obj.name+'</h2> '+
            '<h3>'+obj.time+'</h3>'+
            '</div>'+
            '<div class="message">'+
            obj.message +
            '</div>'+
            '</li>';

        if($('#chat').length) {
            setTimeout(function(){
                var d = $("#chat");
                d.scrollTop(d[0].scrollHeight);
            }, 1);
            $('#chat').append(message);
        } else {
            let is_span = $('#live_message span').length;
            if (is_span) {
                let len = parseFloat($('#live_message span').text());
                $('#live_message span').text(len+1);
            } else {
                $('#live_message').append('<span class="badge badge-danger text-white">1</span>');
            }
            notifyMe(obj.name, obj.message, obj.photo);
        }
        new Audio('{{ asset('frontend_assets/tone') }}' + '/tone_1.wav').play();
    }

    function notifyMe(title, message, icon) {
        if (Notification.permission !== 'granted') {
            if(Notification.requestPermission()) {
                notifier (title, message, icon);
            } else {
                $.toast({
                    heading: title,
                    text: message,
                    icon: 'info',
                    loader: true,
                    loaderBg: '#9EC600',
                    hideAfter: 5000
                })
            }
        } else {
            notifier (title, message, icon);
        }
    }

    function notifier (title, message, icon) {
        let notification = new Notification(title, {
            icon: icon,
            body: message,
        });
        notification.onclick = function() {
            window.location.href = "{{url('customer/messages')}}";
        };
    }
</script>
@endif

</body>
</html>
