@extends('frontend.default.layout')
@section('title')
    Home Page
@endsection

@section('main_content')
    @if(count($sliders) > 0)
    <!--slider section start-->
    <section class="slider-section section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" style="padding-left: 0; padding-right: 0;">
                    <div class="sliders owl-carousel sliders-height">
                        @foreach($sliders as $row)
                            <div class="single-slide-item">
                                <img class="lazyload" src="{{ asset('public/uploads/thumbnail/'.$row->slider_image) }}" data-src="{{ asset('public/uploads/'.$row->slider_image) }}" alt="">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--slider section end-->
    @endif

    @if(count($advertisements) > 0)
    <section class="shop-advertisements-section section-padding">
        <div class="container-fluid">
            <div class="row">
                @foreach($advertisements as $row)
                <div class="col-md-{{ (int) 12/count($advertisements) }} col-{{ (int) 12/count($advertisements) }}" style="padding-left: 3px;padding-right: 3px;">
                    <a class="list" href="{{$row->shop_link}}" target="_blank">
                        <img class="lazyload" src="{{asset('public/uploads/thumbnail/'.$row->image_url)}}" data-src="{{asset('public/uploads/'.$row->image_url)}}" alt="">
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif

    <!--tab section start-->
    <section class="shop-category-tab">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 set-bg">
                    <div class="tab-menu">
                        <h3 class="text-capitalize categories_h3">Categories</h3>
                        <ul class="nav nav-tabs" id="category_shop_tab" role="tablist">
                            <li>
                                <a @if($site_settings->active_tab == 'categories') class="active" @endif id="categories-tab" data-toggle="tab" href="#categories" role="tab" aria-controls="categories" aria-selected="true">Products</a>
                            </li>
                            <li>
                                <a @if($site_settings->active_tab == 'brands') class="active" @endif id="brands-tab" data-toggle="tab" href="#brands" role="tab" aria-controls="brands" aria-selected="true">Brands</a>
                            </li>
                            <li>
                                <a @if($site_settings->active_tab == 'services') class="active" @endif id="services-tab" data-toggle="tab" href="#services" role="tab" aria-controls="services" aria-selected="true">Services</a>
                            </li>
                            <li>
                                <a  @if($site_settings->active_tab == 'shops') class="active" @endif id="shops-tab" data-toggle="tab" href="#shops" role="tab" aria-controls="shops" aria-selected="false">Shops</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content shop-list-section " id="categoryShopTabContent">
                        <div class="tab-pane fade  @if($site_settings->active_tab == 'categories') show active @endif" id="categories" role="tabpanel" aria-labelledby="categories-tab">
                            <div class="shop-lists">
                                <div class="row">
                                    @foreach($shop_categories as $row)
                                        <?php
                                        if(!empty($row->image)) {
                                            $category_image = asset('public/uploads/category/'.$row->image);
                                            $category_image_thumbnail = asset('public/uploads/category/thumbnail/'.$row->image);
                                        } else {
                                            $category_image = $category_image_thumbnail = asset('frontend_assets/images/cat-icon.jpg');
                                        }
                                        ?>
                                    <div class="col-lg-2 col-md-3 col-4">
                                        <div class="single-shop-list white z-depth-1 p-2" style="min-height: 9rem;">
                                            <a href="{{ url('/shop_category/'.$row->slug) }}">
                                                <div class="shop-thumb" style="overflow: hidden;">
                                                    <img class="lazyload" src="{{$category_image_thumbnail}}" data-src="{{$category_image}}" alt="{{ $row->name }}" class="img-fluid">
                                                </div>
                                                <div class="shop-name">
                                                    @if(strlen($row->name) != strlen(utf8_decode($row->name)) && strlen($row->name) > 50)
                                                        <h5 style="font-size: 11px;margin-top: 3px;">{{ $row->name }}</h5>
                                                    @elseif(strlen($row->name) == strlen(utf8_decode($row->name)) && strlen($row->name) > 16)
                                                        <h5 style="font-size: 11px;margin-top: 3px;">{{ $row->name }}</h5>
                                                    @else
                                                        <h5>{{ $row->name }}</h5>
                                                    @endif
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade @if($site_settings->active_tab == 'brands') show active @endif" id="brands" role="tabpanel" aria-labelledby="brands-tab">
                            <?php $brand_list = Helpers::getBrandList(); ?>
                            @if(count($brand_list) > 0)
                            <div class="shop-lists">
                                <div class="row">
                                    @foreach($brand_list as $row)
                                        <?php
                                        if(!empty($row->image)) {
                                            $brand_image = asset('public/uploads/brands/'.$row->image);
                                            $brand_image_thumbnail = asset('public/uploads/brands/thumbnail/'.$row->image);
                                        } else {
                                            $brand_image = $brand_image_thumbnail = asset('frontend_assets/images/cat-icon.jpg');
                                        }
                                        ?>
                                    <div class="col-lg-2 col-md-3 col-4">
                                        <div class="single-shop-list white z-depth-1 p-2" style="min-height: 9rem;">
                                            <a href="{{ url('/brand/'.$row->slug) }}">
                                                <div class="shop-thumb" style="overflow: hidden;">
                                                    <img class="lazyload" src="{{$brand_image_thumbnail}}" data-src="{{$brand_image}}" alt="{{ $row->name }}" class="img-fluid">
                                                </div>
                                                <div class="shop-name">
                                                    @if(strlen($row->name) != strlen(utf8_decode($row->name)) && strlen($row->name) > 50)
                                                        <h5 style="font-size: 11px;margin-top: 3px;">{{ $row->name }}</h5>
                                                    @elseif(strlen($row->name) == strlen(utf8_decode($row->name)) && strlen($row->name) > 16)
                                                        <h5 style="font-size: 11px;margin-top: 3px;">{{ $row->name }}</h5>
                                                    @else
                                                        <h5>{{ $row->name }}</h5>
                                                    @endif
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="tab-pane fade @if($site_settings->active_tab == 'services') show active @endif" id="services" role="tabpanel" aria-labelledby="services-tab">
                            <div class="shop-lists">
                                <div class="row">
                                    @foreach($service_categories as $row)
                                        <?php
                                        if(!empty($row->image)) {
                                            $service_image = asset('public/uploads/category/'.$row->image);
                                            $service_image_thumbnail = asset('public/uploads/category/thumbnail/'.$row->image);
                                        } else {
                                            $service_image = $service_image_thumbnail = asset('frontend_assets/images/shop-thumb.jpg');
                                        }
                                        ?>
                                        <div class="col-lg-2 col-md-3 col-4">
                                            <div class="single-shop-list white z-depth-1 p-2" style="min-height: 9rem;">
                                                <a href="{{ url('/service_category/'.$row->slug) }}">
                                                    <div class="shop-thumb" style="overflow: hidden;">
                                                        <img class="lazyload" src="{{$service_image_thumbnail}}" data-src="{{$service_image}}" alt="{{ $row->name }}" class="img-fluid">
                                                    </div>
                                                    <div class="shop-name">
                                                        @if(strlen($row->name) != strlen(utf8_decode($row->name)) && strlen($row->name) > 50)
                                                            <h5 style="font-size: 11px;margin-top: 3px;">{{ $row->name }}</h5>
                                                        @elseif(strlen($row->name) == strlen(utf8_decode($row->name)) && strlen($row->name) > 16)
                                                            <h5 style="font-size: 11px;margin-top: 3px;">{{ $row->name }}</h5>
                                                        @else
                                                            <h5>{{ $row->name }}</h5>
                                                        @endif
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade @if($site_settings->active_tab == 'shops') show active @endif" id="shops" role="tabpanel" aria-labelledby="shops-tab">
                            <div class="shop-lists">
                                <div class="row">
                                    @foreach($shop_categories_list as $row)
                                        <?php
                                        if(!empty($row->icon)) {
                                            $category_image = asset('public/uploads/store_category/'.$row->icon);
                                        } else {
                                            $category_image = $category_image_thumbnail = asset('frontend_assets/images/cat-icon.jpg');
                                        }
                                        ?>
                                        <div class="col-lg-2 col-md-3 col-4">
                                            <div class="single-shop-list white z-depth-1 p-2" style="min-height: 9rem;">
                                                <a href="{{ url('/shop_list?shop_category_id='.$row->id) }}">
                                                    <div class="shop-thumb" style="overflow: hidden;">
                                                        <img class="lazyload" src="{{$category_image}}" alt="{{ $row->name }}" class="img-fluid">
                                                    </div>
                                                    <div class="shop-name">
                                                        @if(strlen($row->name) != strlen(utf8_decode($row->name)) && strlen($row->name) > 50)
                                                            <h5 style="font-size: 0.71rem;margin-top: 3px;">{{ $row->name }}</h5>
                                                        @elseif(strlen($row->name) == strlen(utf8_decode($row->name)) && strlen($row->name) > 16)
                                                            <h5 style="font-size: 11px;margin-top: 3px;">{{ $row->name }}</h5>
                                                        @else
                                                            <h5>{{ $row->name }}</h5>
                                                        @endif
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--tab section end-->

    @if(count($new_shop_list) > 0)
        <!--Featured product section start-->
        <section class="featured-product-section section-padding">
            <div class="container-fluid">
                <div class="row set-bg">
                    <div class="col-md-12">
                        <div class="section-title styled">
                            <h3 class="text-capitalize">Shops</h3>
                            <div class="view-all">
                                <a href="{{url('shop_list')}}">view all <i class="material-icons right">chevron_right</i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="ssp-products">
                            <div class="row">
                                @foreach($new_shop_list as $row)
                                    <div class="col-md-3 col-6">
                                        @include('frontend.common.shop_item')
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Featured product section End-->
    @endif

    @if(count($featured_products) > 0)
        <!--Featured product section start-->
        <section class="featured-product-section">
            <div class="container-fluid">
                <div class="row set-bg">
                    <div class="col-md-12">
                        <div class="section-title styled">
                            <h3 class="text-capitalize">Products</h3>
                            <div class="view-all">
                                <a href="{{url('product_list')}}">view all <i class="material-icons right">chevron_right</i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="ssp-products">
                            <div class="row">
                                @foreach($featured_products as $row)
                                    <div class="col-md-3 col-6">
                                        @include('frontend.common.product_item')
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Featured product section End-->
    @endif

    @if(count($service_list) > 0)
        <section class="service-list section-padding">
            <div class="container-fluid">
                <div class="row set-bg">
                    <div class="col-md-12">
                        <div class="section-title styled">
                            <h3 class="text-capitalize">Our Services</h3>
                            <div class="view-all">
                                <a href="{{url('service_list')}}">view all <i class="material-icons right">chevron_right</i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="ssp-products">
                            <div class="row">
                                @foreach($service_list as $row)
                                    <div class="col-md-4 col-6">

                                        <a href="{{ url('/service/'.$row->id.'/'.$row->service_slug) }}">
                                            <div class="single-product service-item">
                                                <div class="product-thumb">
                                                    @if($row->service_image)
                                                    <img class="lazyload" src="{{ asset('public/uploads/services/thumbnail/'.$row->service_image) }}" data-src="{{ asset('public/uploads/services/'.$row->service_image) }}"
                                                         class="img-fluid" alt="{{ $row->service_name }}">
                                                    @else
                                                        <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" style="opacity: 0.05;" alt="" class="img-fluid">
                                                    @endif
                                                </div>
                                                <div class="product-info text-center">
                                                    <h5>{{ $row->service_name }}</h5>
                                                    <!--<p class="weight">1 kg</p>-->
                                                    <p class="price">৳ <strong>{{ number_format($row->price, 2) }}</strong></p>
                                                    <p class="cart-btn"><i
                                                            class="fa fa-eye"></i> view details</p>
                                                    <div class="service_district_name p-0">
                                                        <p>{{$row->store_name}} / {{Helpers::districtName($row->district)}}</p>
                                                    </div>

                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if(count($blog_data) > 0)
        <section class="service-list section-padding">
            <div class="container-fluid">
                <div class="row set-bg">
                    <div class="col-md-12">
                        <div class="section-title styled">
                            <h3 class="text-capitalize">Latest News</h3>
                            <div class="view-all">
                                <a href="{{url('blog-list')}}">view all <i class="material-icons right">chevron_right</i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="ssp-products">
                            <div class="row">
                                @foreach($blog_data as $row)
                                    <div class="col-md-4">
                                        <div class="single-post">
                                            <div class="post-thumb">
                                                @if($row->image_url)
                                                <img class="lazyload" src="{{ asset('public/uploads/blog/thumbnail/'.$row->image_url) }}" data-src="{{ asset('public/uploads/blog/'.$row->image_url) }}" alt="">
                                                @else
                                                    <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" style="opacity: 0.05;" alt="" class="img-fluid">
                                                @endif
                                                <div class="overlay on-hover"></div>
                                                <!--<div class="post-date">
                                                    <p>feb <span>21</span></p>
                                                </div>-->
                                            </div>
                                            <div class="post-content-area">
                                                <div class="post-meta">
                                                    <ul>
                                                        <li><i class="fa fa-user"></i>{{$row->created_by}}</li>
                                                        <li><i class="fa fa-calendar"></i><?php
                                                            $date = new DateTime($row->created_at);
                                                            echo $date->format('M d Y'); ?></li>
                                                    </ul>
                                                </div>
                                                <div class="post-title">
                                                    <a href="{{ url('blog_details/'.$row->id) }}"><h3><?php echo $row->title; ?></h3></a>
                                                </div>
                                                <div class="post-excerpt">
                                                    <p>
                                                        <?php echo mb_substr(strip_tags($row->description), 0, 100); ?> <a href="{{ url('blog_details/'.$row->id) }}"> read more</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- <div class="theme-btn float-right">
                                                 <a href="blog-details.html">read more</a>
                                             </div>-->
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    <!--promotions section start-->
    {{--<section class="promo-section section-padding">--}}
        {{--<div class="container-fluid">--}}
            {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                    {{--<div class="section-title text-center">--}}
                        {{--<h3 class="text-capitalize">Why People <span><i class="fa fa-heart"></i></span> beEasy</h3>--}}
                    {{--</div>--}}
                    {{--<div class="promo-items d-lg-flex d-block text-center">--}}
                        {{--<div class="single-promo first">--}}
                            {{--<div class="promo-thumb">--}}
                                {{--<img src="{{ asset('frontend_assets') }}/images/lp-features-1.png" alt="" class="img-fluid">--}}
                            {{--</div>--}}
                            {{--<div class="promo-cont">--}}
                                {{--<h4>Convenient & Quick</h4>--}}
                                {{--<p>--}}
                                    {{--No waiting in traffic, no haggling, no worries carrying groceries, they're delivered--}}
                                    {{--right at--}}
                                    {{--your door.--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="single-promo second">--}}
                            {{--<div class="promo-thumb">--}}
                                {{--<img src="{{ asset('frontend_assets') }}/images/lp-features-2.png" alt="" class="img-fluid">--}}
                            {{--</div>--}}
                            {{--<div class="promo-cont">--}}
                                {{--<h4>Freshly Picked</h4>--}}
                                {{--<p>--}}
                                    {{--Our fresh produce is sourced every morning, you get the best from us.--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="single-promo third">--}}
                            {{--<div class="promo-thumb">--}}
                                {{--<img src="{{ asset('frontend_assets') }}/images/lp-features-3.png" alt="" class="img-fluid">--}}
                            {{--</div>--}}
                            {{--<div class="promo-cont">--}}
                                {{--<h4>A wide range of Products</h4>--}}
                                {{--<p>--}}
                                    {{--With 4000+ Products to choose from, forget scouring those aisles for hours.--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    <!--promotions section end-->

    <!--contact section start-->
    <!--<section class="contact-us-section section-padding wow fadeIn">
        <div class="contact-form">
            <form action="#">
                <div class="text-center">
                    <h3 class="text-capitalize text-white mb-3">Contact Us</h3>
                </div>
                <div class="input-field">
                    <input id="name" type="text" class="validate">
                    <label for="name">Your Name</label>
                </div>
                <div class="input-field">
                    <input id="email" type="email" class="validate">
                    <label for="email">Your Email</label>
                </div>
                <div class="form-group">
                <textarea name="message" id="message" class="form-control" cols="10" rows="5"
                          placeholder="Your Message..."></textarea>
                </div>
                <button class="btn float-right">Send</button>
            </form>
        </div>
        <div class="gmap">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3647.8154083642294!2d89.14911481486337!3d23.896163489109448!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39fe979e6b07ac57%3A0x6aa0662e3cf36e65!2sGiniLab%20Bangladesh!5e0!3m2!1sen!2sbd!4v1577455417015!5m2!1sen!2sbd"
                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </section>-->
    <!--contact section end-->
@endsection
