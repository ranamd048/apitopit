@extends('frontend.light.layout')
@section('title')
    Home Page
@endsection

@section('main_content')
<style>
.custom-offer-popup {
    background: #fff;
    top: 50%;
    left: 50%;
    width: 50%;
    z-index: 999999999999999;
    position: fixed;
    transform: translate(-50%, -50%);
    box-shadow: 0 0 10px #C8D6E5;
    border-radius: 5px;
    border: 1px solid var(--theme-color);
    display: none;
}
.custom-offer-popup .popup-header {
    background: var(--theme-color);
    padding: 10px;
}
.custom-offer-popup .popup-header h3 {
    color: #fff;
    font-size: 25px;
}
.custom-offer-popup .popup-content {
    padding: 15px;
    overflow: hidden;
}
.custom-offer-popup .popup-footer {
    overflow: hidden;
}
.custom-offer-popup .popup-footer a {
    float: right;
    background: var(--theme-color);
    color: #fff;
    font-size: 14px;
    padding: 5px 20px;
    cursor: pointer;
}
.custom-offer-popup .popup-header h3 span {
    float: right;
    color: red;
    cursor: pointer;
}
@media only screen and (max-width: 767px) {
    .custom-offer-popup {
        width: 90%;
        height: 80%;
        overflow: auto;
    }
}
</style>
<?php $current_offer = Helpers::get_current_offer(); ?>
    @if(count($sliders) > 0)
    <!-- slider area start -->
    <section class="slider-section">
        <div class="all-slide-item owl-carousel" id="template-2-slider">
            @foreach($sliders as $row)
            <div class="single-slide-item" style="background-image: url({{ asset('public/uploads/'.$row->slider_image) }})"></div>
            @endforeach
        </div>
    </section>
    <!-- slider area end -->
    @endif

    <section class="history-of-islam-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 my-auto">
                    <div class="history-content-area">
                        <div class="section-title">
                            <h2><?php echo $site_settings->about_title; ?></h2>
                        </div>
                        <div class="history-thumb d-md-none d-block">
                            <img src="<?php echo asset('public/uploads/' . $site_settings->about_img); ?>" alt="">
                        </div>
                        <div class="history-content">
                            <?php echo $site_settings->about_desc; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-md-block d-none">
                    <div class="history-thumb">
                        <img src="<?php echo asset('public/uploads/' . $site_settings->about_img); ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(count($shop_categories) > 0)
    <!-- promotions area start -->
    <section class="promotions-section section-padding">
        <div class="container">
            <div class="promotion-items">
                <div class="row">
                    <?php foreach ($shop_categories as $category) : ?>
                    <div class="col-md-4">
                        <a href="{{ url('shop_category/'.$category->slug) }}">
                            <div class="single-promotion-item">
                                <div class="promo-thumb">
                                    <?php
                                        if(!empty($category->image)) {
                                            $category_image = $category->image;
                                        } else {
                                            $category_image = 'category_image1578821371.jpg';
                                        }
                                    ?>
                                    <img src="{{asset('public/uploads/category/'.$category_image)}}" alt="">
                                </div>
                                <div class="promo-cont">
                                    <h3><?= $category->name; ?></h3>
                                    <p>
                                        <?= $category->description; ?>
                                    </p>
                                    <div class="btn-hover slider-btn-style shop-btn">
                                        shop now
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
    <!-- promotions area end -->
    @endif

    <!-- blog area start -->
    <section class="blog-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center">
                        <h1>latest from blog</h1>
                    </div>
                </div>
            </div>
            <?php if(count($blog_data) > 0) : ?>
            <div class="row">
                <div class="blog-posts owl-carousel">
                    <?php foreach($blog_data as $row) : ?>
                    <div class="single-post">
                        <div class="post-thumb">
                            <img src="{{ asset('public/uploads/blog/'.$row->image_url) }}" alt="">
                            <div class="overlay on-hover"></div>
                            <!--<div class="post-date">
                                <p>feb <span>21</span></p>
                            </div>-->
                        </div>
                        <div class="post-content-area">
                            <div class="post-meta">
                                <ul>
                                    <li><i class="fa fa-calendar"></i><?php
                                        $date = new DateTime($row->created_at);
                                        echo $date->format('M d Y'); ?></li>
                                    <li><i class="fa fa-comment"></i>25 comments</li>
                                </ul>
                            </div>
                            <div class="post-title">
                                <a href="{{ url('blog_details/'.$row->id) }}"><h3><?php echo $row->title; ?></h3></a>
                            </div>
                            <div class="post-excerpt">
                                <p>
                                    <?php echo mb_substr(strip_tags($row->description), 0, 110); ?> <a href="{{ url('blog_details/'.$row->id) }}">read more</a>
                                </p>
                            </div>
                        </div>
                        <!-- <div class="theme-btn float-right">
                             <a href="blog-details.html">read more</a>
                         </div>-->
                    </div>
                    <?php endforeach; ?>


                </div>
                <div class="view-all-btn">
                    <div class="theme-btn text-right">
                        <a href="{{ url('blog-list') }}">view all</a>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </section>
    <!-- blog area end -->    
  </div>
</div>

@if($current_offer)
<!-- Custom Offer Popup -->
<div class="custom-offer-popup">
    <div class="popup-header">
        <h3>{{$current_offer->cupon_title}} <span class="popup-close" onclick="close_popup();"><i class="ion-android-close"></i></span></h3>
    </div>
    <div class="popup-content">
        <p><?php echo $current_offer->cupon_desc; ?></p>
    </div>
    <div class="popup-footer">
        <a href="{{url('product_list')}}">Continue Shopping</a>
    </div>
</div>
<script>
    function close_popup() {
        $('.custom-offer-popup').hide();
        $('.wrapper').removeClass('overlay-active');
    }
    $(document).ready(function() {
        $('.custom-offer-popup').show();
    });
</script>
@endif
@endsection