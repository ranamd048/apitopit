<?php
if(Request::is('/')) {
    $home_class = '';
} else {
    $home_class = 'other-page';
}
?>
<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>
    @yield('meta_content')
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('public/uploads/'.$site_settings->favicon) }}">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/material_icon.css">

    <!-- all css here -->
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/materialize.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/animate.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/magnific-popup.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/slinky.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/slick.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/solaimanlipi.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/bundle.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/style.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/custom.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/css/responsive.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/jquery.exzoom.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/style.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/custom.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/responsive.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/common.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/new_style.css?<?php echo time(); ?>">
    <script src="{{ asset('frontend_assets') }}/light/js/vendor/modernizr-2.8.3.min.js"></script>


    <!-- template 2 assets start -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/template-2/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/template-2/css/style.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/template-2/css/custom.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/light/template-2/css/responsive.css">
    <!-- template 2 assets end -->

    <!-- Jquery File Call -->
    <script src="{{ asset('frontend_assets') }}/common/js/jquery-3.3.1.min.js"></script>
    <style>
        :root {
            --theme-color: <?php echo (!empty($site_settings->theme_color)) ? $site_settings->theme_color: '#0B9444'; ?>;
        }
        .header-area.other-page {
            position: relative;
        }
        .main-menu ul li a {
            font-size: 14px;
        }
        .logo img {width: 100px;}
    </style>
    <script type="text/javascript">
        var APP_URL = {!! json_encode(url('/')) !!};
    </script>
</head>

<body>
<!-- header start -->
<?php 
    $current_offer = Helpers::get_current_offer();
    $overlay_active = '';
    if(request()->is('/')) {
        if($current_offer) {
            $overlay_active = 'overlay-active';
        }
    }
?>
<div class="wrapper {{$overlay_active}}">
    <!--mobile menu bar start-->
    <div class="mobile_view">
        <div class="mobile-menu-area">
            <ul class="mobile-menu">
                <div class="logo text-center mt-2">
                    <a href="{{url('/')}}">
                        <!-- <h1>logo</h1> -->
                        <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="Logo">
                    </a>
                </div>
                <li><i class="fa fa-times" id="close"></i></li>
                @if(count($site_menu_items) > 0)
                    @foreach($site_menu_items as $menu)
                        <?php $sub_menus = DB::table('menu')->select('*')->where('parent_id', '=', $menu->id)->where('status', '=', 1)->get();
                        $sub_menus_class = '';
                        $is_parent = "parent_menu";
                        if(count($sub_menus) > 0) {
                            $sub_menus_class = 'has-submenu';
                            $is_parent = "";
                        }
                        ?>
                        <li class="{{$sub_menus_class}}"><a class="nav-link {{$is_parent}}" href="@if($menu->is_custom == 1) {{$menu->menu_url}} @else {{ url($menu->menu_url) }} @endif">{{ $menu->name }} @if(count($sub_menus) > 0) <i class="fa fa-caret-down"></i> @endif</a>
                            @if(count($sub_menus) > 0)
                                <ul class="mobile-dropdown">
                                    @foreach($sub_menus as $sub_menu)
                                        <li><a class="parent_menu" href="@if($sub_menu->is_custom == 1) {{ $sub_menu->menu_url }} @else {{ url($sub_menu->menu_url) }} @endif">{{ $sub_menu->name }}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                @endif
                <li class="has-submenu"><a class="nav-link" href="#">My Account <i class="fa fa-caret-down"></i></a>
                        <ul class="mobile-dropdown">
                            @if(Session::has('customer_id') || Session::has('distributor_id'))
                                <li><a class="parent_menu" href="{{ url('customer_profile') }}">My Profile</a></li>
                                <li><a class="parent_menu" href="{{ url('orders') }}">My Orders</a></li>
                                <li><a class="parent_menu" href="{{ url('my_services') }}">My Services</a></li>
                                <li><a class="parent_menu" href="{{ url('wishlist') }}">Wishlist</a></li>
                                <li><a class="parent_menu" href="{{ url('customer_logout') }}">Logout</a></li>
                            @else
                                <li><a class="parent_menu" href="{{ url('registration') }}">Registration</a></li>
                                <li><a class="parent_menu" href="{{ url('login') }}">Sign In </a></li>
                            @endif
                            @if(Session::has('distributor_id'))
                            <li><a class="parent_menu" href="{{ url('registration') }}">Create User </a></li>
                            @endif
                        </ul>

                </li>
            </ul>
        </div>
        <div class="sticky-area-mobile-view">
            <a href="{{url('/')}}" class="mobile-home-btn text-center mobile-sticky-bar-common-style">
                <i class="fa fa-home"></i>
                <p>home</p>
            </a>
            <div class=" mobile-user-btn text-center mobile-sticky-bar-common-style">
                <button class="sidebar-trigger-search">
                    <i class="fa fa-search"></i>
                    <p>search</p>
                </button>
            </div>
            <a href="tel:{{$site_settings->phone_number}}" class="mobile-call-btn text-center mobile-sticky-bar-common-style">
                <i class="fa fa-phone"></i>
                <p>call</p>
            </a>
            <a href="{{url('cart')}}" class="mobile-cart-btn text-center mobile-sticky-bar-common-style header-cart">
                <i class="fa fa-shopping-cart"></i>
                <p>Cart</p>
                <span class="top_cart_count">0</span>
            </a>
            <div class="mobile-menu-btn text-center mobile-sticky-bar-common-style">
                <i class="fa fa-bars"></i>
                <p>more</p>
            </div>
        </div>
    </div>
    <!--mobile menu bar end-->
    <header>
        <div class="header-area header-area-padding <?php echo $home_class; ?>">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-6">
                        <div class="logo">
                            <a href="{{url('/')}}">
                                <!-- <h1>logo</h1> -->
                                <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 menu-center">
                        <div class="main-menu">
                            @if(count($site_menu_items) > 0)
                                <ul>
                                <?php $service_categories = Helpers::getServiceParentCategory(); ?>
                                @if(count($service_categories) > 0)
                                <li>
                                    <a href="#">Services <i class="fa fa-caret-down"></i></a>
                                    <ul class="dropdown">
                                        @foreach($service_categories as $row)
                                        <li><a href="{{ url('service_category/'.$row->slug) }}">{{ $row->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endif
                                @foreach($site_menu_items as $menu)
                                    <?php $sub_menus = DB::table('menu')->select('*')->where('parent_id', '=', $menu->id)->where('status', '=', 1)->get(); ?>
                                        <li><a href="@if($menu->is_custom == 1) {{$menu->menu_url}} @else {{ url($menu->menu_url) }} @endif">{{ $menu->name }} @if(count($sub_menus) > 0) <i class="fa fa-caret-down"></i> @endif</a>
                                            @if(count($sub_menus) > 0)
                                            <ul class="dropdown">
                                                @foreach($sub_menus as $sub_menu)
                                                <li><a href="@if($sub_menu->is_custom == 1) {{ $sub_menu->menu_url }} @else {{ url($sub_menu->menu_url) }} @endif">{{ $sub_menu->name }}</a></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2 col-6">
                        <div class="header-search-cart">
                            <div class="header-search common-style">
                                <button class="sidebar-trigger-search">
                                    <span class="ion-ios-search-strong"></span>
                                </button>
                            </div>

                            <a href="{{url('cart')}}" class="header-cart common-style">
                                <button class="sidebar-trigger">
                                    <span class="ion-bag"></span>
                                    <span class="item-count top_cart_count"></span>
                                    <!--<span class="badge bg-danger">৳</span>-->
                                </button>
                            </a>
                            <div class="header-sidebar common-style">
                                <button class="header-navbar-active">
                                    <span class="ion-navicon"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header end -->
    <!-- main-search start -->
    <div class="main-search-active">
        <div class="sidebar-search-icon">
            <button class="search-close"><span class="ion-android-close"></span></button>
        </div>
        <div class="sidebar-search-input">
            <form action="{{url('search')}}" method="GET">
                <input type="hidden" name="search_type" value="product">
                <div class="form-search">
                    <input id="keyword" class="input-text" name="keyword" value="" placeholder="Search Product ..." type="search">
                    <button type="submit">
                        <i class="ion-ios-search-strong"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <!-- main-search end -->
    <div class="cur-lang-acc-active">
        <div class="wrap-sidebar">
            <div class="sidebar-nav-icon">
                <button class="op-sidebar-close"><span class="ion-android-close"></span></button>
            </div>
            <div class="cur-lang-acc-all">
                <div class="single-currency-language-account">
                    <div class="cur-lang-acc-title">
                        <h4>My Account:</h4>
                    </div>
                    <div class="cur-lang-acc-dropdown">
                        <ul>
                            @if(Session::has('customer_id') || Session::has('distributor_id'))
                                <li><a href="{{ url('customer_profile') }}">My Profile</a></li>
                                <li><a href="{{ url('orders') }}">My Orders</a></li>
                                <li><a href="{{ url('my_services') }}">My Services</a></li>
                                @if($private_settings->wishlist_option == 1)
                                    <li><a href="{{ url('wishlist') }}">Wishlist</a></li>
                                @else
                                    <li><a href="{{ url('favourites') }}">Favourites</a></li>
                                @endif
                                @if(Session::has('distributor_id'))
                                    <li><a href="{{ url('registration') }}">Create User </a></li>
                                @endif
                                <li><a href="{{ url('customer_logout') }}">Logout</a></li>
                            @else
                                <li><a href="{{ url('registration') }}">Registration</a></li>
                                <li><a href="{{ url('login') }}">Sign In </a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @yield('main_content')

    <style>
        .latest-item a h4 {
            color: #fff;
            font-size: 12px;
            line-height: 18px;
        }
        .latest-item {
            margin-bottom: 15px;
        }
    </style>
    <div class="mobile-menu-bar-margin"></div>
    <footer class="footer-section">
        <div class="footer-bg-overly"></div>
        <div class="footer-top section-padding">
            <div class="container">
                <div class="row wow fadeInUp">
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget-one widget-box-shadow">
                            <div class="footer-common-title">
                                <h5>get in touch</h5>
                            </div>

                            <div class="contact-info">
                                <p><i class="fa fa-phone"></i>{{ $site_settings->phone_number }}</p>
                                <p><i class="fa fa-envelope"></i> {{ $site_settings->email }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget-two widget-box-shadow">
                            <div class="footer-common-title">
                                <h5>Categories</h5>
                            </div>
                            <?php
                            $categories = DB::table('categories')->select('id', 'name', 'slug')->where(['parent_id' => 0, 'status' => 1])->orderBy('id', 'DESC')->take(10)->get();
                            ?>
                            <div class="link-menu">
                                <ul>
                                    <?php foreach($categories as $category) : ?>
                                    <li><a href="<?php echo url('shop_category/'.$category->slug); ?>"><?php echo $category->name; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget-three widget-box-shadow">
                            <div class="footer-common-title">
                                <h5>Latest News</h5>
                            </div>
                            <?php
                            $latest_blog = DB::table('blogs')->where('type', 'news')->orderBy('id', 'DESC')->take(5)->get();
                            ?>
                            <div class="latest-items">
                                <?php foreach($latest_blog as $row) : ?>
                                <div class="latest-item">
                                    <div class="latest-thumbnail">
                                        <a href="<?php echo url('blog_details/'.$row->id); ?>"><img src="{{asset('public/uploads/blog/'.$row->image_url)}}" alt=""></a>
                                    </div>
                                    <div class="latest-widget-content">
                                        <a href="<?php echo url('blog_details/'.$row->id); ?>"><h4><?php echo $row->title; ?></h4></a>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget-four widget-box-shadow">
                            <div class="footer-common-title">
                                <h5>gallery picture</h5>
                            </div>
                            <?php
                            $gallery_list = DB::table('gallery')->orderBy('id', 'DESC')->take(9)->get();
                            ?>
                            <div class="popular-item-thumb">
                                <?php foreach($gallery_list as $row) : ?>
                                <a href="#"><img src="{{asset('public/uploads/gallery/'.$row->gallery_image)}}" alt="">
                                </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer-bottom-border-top"></div>
                        <div class="footer-social-links wow fadeInLeft">
                            <?php if (!empty($site_settings->fb_link)) { ?>
                            <a target="_blank" href="<?= $site_settings->fb_link; ?>"><i class="fa fa-facebook"></i></a>
                            <?php } if (!empty($site_settings->youtube_link)) { ?>
                            <a target="_blank" href="<?= $site_settings->youtube_link; ?>"><i class="fa fa-youtube"></i></a>
                            <?php } if (!empty($site_settings->twitter_link)) { ?>
                            <a target="_blank" href="<?= $site_settings->twitter_link; } ?>"><i class="fa fa-twitter"></i></a>
                        </div>
                        <div class="copy-right-text wow fadeInRight">
                            <p>{{ $site_settings->copyright_text }}</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?php $latest_notice = DB::table('blogs')->where('type', 'notice')->orderBy('id', 'DESC')->take(5)->get(); ?>
    @if(count($latest_notice) > 0)
    <div class="sticky-latest-news">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <span class="la">Latest Notice :</span>
                    <marquee>
                        @foreach($latest_notice as $row)
                        <a target="blank" href="{{url('blog_details/'.$row->id)}}">
                            {{$row->title}} -> {{strip_tags($row->description)}} ||
                        </a>
                        @endforeach

                    </marquee>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<!-- all js here -->
<script src="{{ asset('frontend_assets') }}/light/js/popper.js"></script>
<script src="{{ asset('frontend_assets') }}/light/js/bootstrap.min.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/sweetalert.min.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/materialize.min.js"></script>
<script src="{{ asset('frontend_assets') }}/light/js/jquery.magnific-popup.min.js"></script>
<script src="{{ asset('frontend_assets') }}/light/js/isotope.pkgd.min.js"></script>
<script src="{{ asset('frontend_assets') }}/light/js/imagesloaded.pkgd.min.js"></script>
<script src="{{ asset('frontend_assets') }}/light/js/jquery.counterup.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<!-- <script src="{{ asset('frontend_assets') }}/light/js/waypoints.min.js"></script> -->
<script src="{{ asset('frontend_assets') }}/light/js/slinky.min.js"></script>
<script src="{{ asset('frontend_assets') }}/light/js/owl.carousel.min.js"></script>
<script src="{{ asset('frontend_assets') }}/light/js/plugins.js"></script>
<script src="{{ asset('frontend_assets') }}/light/js/mixitup.js"></script>
<script src="{{ asset('frontend_assets') }}/light/js/main.js"></script>


<!-- template-2 js -->
<script src="{{ asset('frontend_assets') }}/common/js/jquery.exzoom.js"></script>
<script src="{{ asset('frontend_assets') }}/light/template-2/js/activation.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/common.js?<?php echo time(); ?>"></script>
</body>

</html>