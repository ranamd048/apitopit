<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>@yield('title')</title>
    @yield('meta_content')
    <link rel="shortcut icon" href="{{ asset('public/uploads/'.$site_settings->favicon) }}">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/material_icon.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/materialize.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/jquery.exzoom.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/style.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/custom.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/css/responsive.css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/common.css?<?php echo time(); ?>">

    <style>
        :root {
            --theme-color: <?php echo (!empty($site_settings->theme_color)) ? $site_settings->theme_color: '#FF5F00'; ?>;
        }
    </style>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
    <script src="{{ asset('frontend_assets') }}/common/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        var APP_URL = {!! json_encode(url('/')) !!};
    </script>
</head>
<body>
<!--header section start-->
<header class="header-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 pr-0">
                <div class="d-flex">
                    <div class="logo">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="navigation">
                        <i data-target="header-navigation" class="material-icons sidenav-trigger tooltipped"
                           data-position="right" data-tooltip="Click me to navigate">menu</i>
                        <div id="header-navigation" class="sidenav">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <span class="sidenav-close float-right p-3">
                                            <i class="material-icons tooltipped" data-position="left"
                                               data-tooltip="Close">close</i>
                                        </span><br>
                                        <div class="sidenav-wrapper">
                                            <div class="page-list">
                                                <h5>navigation link</h5>
                                                <ul>
                                                    @if(count($site_menu_items) > 0)
                                                        @foreach($site_menu_items as $menu)
                                                            <?php $sub_menus = DB::table('menu')->select('*')->where('parent_id', '=', $menu->id)->where('status', '=', 1)->get();
                                                            $sub_menus_class = 'nav-item';
                                                            if (count($sub_menus) > 0) {
                                                                $sub_menus_class = 'nav-item dropdown';
                                                            }
                                                            ?>
                                                            <li class="{{$sub_menus_class}}">
                                                                @if(count($sub_menus) > 0)
                                                                    <a class="dropdown-toggle" href="#" role="button"
                                                                       data-toggle="dropdown" aria-haspopup="true"
                                                                       aria-expanded="false">
                                                                        {{ $menu->name }}
                                                                    </a>
                                                                    <div class="dropdown-menu"
                                                                         aria-labelledby="navbarDropdown">
                                                                        @foreach($sub_menus as $sub_menu)
                                                                            <a class="dropdown-item"
                                                                               href="@if($sub_menu->is_custom == 1) {{ $sub_menu->menu_url }} @else {{ url($sub_menu->menu_url) }} @endif">{{ $sub_menu->name }}</a>
                                                                        @endforeach
                                                                    </div>
                                                                @else
                                                                    <a href="@if($menu->is_custom == 1) {{$menu->menu_url}} @else {{ url($menu->menu_url) }} @endif">{{ $menu->name }}</a>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                            <hr>
                                            <div class="cat-list">
                                                <h5>My Account</h5>
                                                <ul>
                                                    @if(Session::has('customer_id') || Session::has('distributor_id'))
                                                        <li><a href="{{ url('customer_profile') }}">My Profile</a></li>
                                                        <li><a href="{{ url('orders') }}">My Orders</a></li>
                                                        <li><a href="{{ url('wishlist') }}">Wishlist</a></li>
                                                        <li><a href="{{ url('customer_logout') }}">Logout</a></li>
                                                    @else
                                                        <li><a href="{{ url('registration') }}">Registration</a></li>
                                                        <li><a href="{{ url('login') }}">Sign In </a></li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="main-search">
                    <form action="{{ url('/search') }}" method="GET">
                        <div class="cat-search">
                            <div class="input-field">
                                <select name="search_type">
                                    <option value="">Search By</option>
                                    <option value="product">Product</option>
                                    <option value="shop">Shop</option>
                                </select>
                            </div>
                        </div>
                        <div class="main-search-wrapper">
                            <div class="input-field">
                                <input type="text" name="keyword" class="form-control" placeholder="Search...">
                            </div>
                            <button type="submit" class="waves-effect tooltipped ml-auto my-auto border-0"
                                    data-position="right" data-tooltip="Search"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3 pl-0">
                <div class="user-wrapper">
                    <ul>
                        <li>
                            <a href="{{url('cart')}}">
                                <span class="cart">
                                    <i class="material-icons">local_grocery_store</i>
                                    <span class="item-count top_cart_count"></span>
                                </span>
                            </a>
                        </li>
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<span class="account-access">--}}
                                    {{--<i class="material-icons">account_circle</i>--}}
                                {{--</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!--header section end-->
@yield('main_content')
<!--footer section start-->
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-12">
                        <div class="service-info d-flex">
                            <div class="d-time">
                                <p><img src="{{ asset('frontend_assets') }}/images/1-hour.png" alt="" class="img-fluid">
                                    1 hour delivery</p>
                            </div>
                            <div class="d-offer">
                                <p><img src="{{ asset('frontend_assets') }}/images/cash-on-delivery.png" alt=""
                                        class="img-fluid"> cash on
                                    delivery</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="about-info">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At consequatur, dolores
                                inventore ipsa optio quisquam saepe sunt? A accusantium ad aperiam asperiores blanditiis
                                culpa eligendi itaque, odio quaerat quam quis repellat, reprehenderit, tempora totam
                                velit? Et impedit omnis vel. Beatae expedita modi nobis reprehenderit suscipit. Culpa
                                minus molestias rerum saepe.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="footer-widget">
                            <div class="f-w-title">
                                <h5>Customer Service</h5>
                                <hr>
                                <ul>
                                    <li><a href="#!">Contact Us</a></li>
                                    <li><a href="#!">FAQ</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-widget">
                            <div class="f-w-title">
                                <h5>About Chaldal</h5>
                                <hr>
                                <ul>
                                    <li><a href="#!">Privacy Policy</a></li>
                                    <li><a href="#!">Terms of Use</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-widget">
                            <div class="f-w-title">
                                <h5>For Business</h5>
                                <hr>
                                <ul>
                                    <li><a href="#!">Corporate</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-right">
                <div class="row">
                    <div class="col-12">
                        <div class="payment-option">
                            <ul>
                                <li class="my-auto"><span>pay with:</span></li>
                                <li><a href="#"><img src="{{ asset('frontend_assets') }}/images/Amex.png" alt=""
                                                     class="img-fluid"></a></li>
                                <li><a href="#"><img src="{{ asset('frontend_assets') }}/images/mastercard.png" alt=""
                                                     class="img-fluid"></a></li>
                                <li><a href="#"><img src="{{ asset('frontend_assets') }}/images/bkash.png" alt=""
                                                     class="img-fluid"></a>
                                </li>
                                <li><a href="#"><img src="{{ asset('frontend_assets') }}/images/COD.png" alt=""
                                                     class="img-fluid"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="apps-info d-inline-flex float-right">
                            <a href="#">
                                <img src="{{ asset('frontend_assets') }}/images/google_play_store.png" alt=""
                                     class="img-fluid">
                            </a>
                            <a href="#">
                                <img src="{{ asset('frontend_assets') }}/images/app_store.png" alt=""
                                     class="img-fluid ml-auto my-auto">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="c-info">
                            <p class="phone"><i class="material-icons">call</i> {{ $site_settings->phone_number }}
                            </p>
                            <p><span>or email</span> {{ $site_settings->email }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="f-social-link float-right">
                            <ul class="d-flex">
                                <li>
                                    <a href="{{ $site_settings->fb_link }}"><img
                                                src="{{ asset('frontend_assets') }}/images/Facebook.png" alt=""
                                                class="img-fluid"></a>
                                    <a href="{{ $site_settings->youtube_link }}"><img
                                                src="{{ asset('frontend_assets') }}/images/Youtube.png" alt=""
                                                class="img-fluid"></a>
                                    <a href="{{ $site_settings->twitter_link }}"><img
                                                src="{{ asset('frontend_assets') }}/images/twitter.png" alt=""
                                                class="img-fluid"></a>
                                    <a href="#!"><img src="{{ asset('frontend_assets') }}/images/Instagram.png" alt=""
                                                      class="img-fluid"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom text-center">
        <p class="m-0">
            {{ $site_settings->copyright_text }}
            developed by
            <a href="http://ginilab.com/">
                <img src="{{ asset('frontend_assets') }}/images/developer-logo.png" alt="" class="img-fluid">
            </a>
        </p>
    </div>
</footer>
<!--mobile footer start-->
<section class="mobile-footer d-md-none d-block">
    <div class="d-flex">
        <div class="mchat-wrapper">
            <i class="fa fa-comments"></i>
        </div>
        <div class="shopping-btn" onclick="mobileopenNav()">
            <a href="{{url('product_list')}}">start shopping</a>
        </div>
        <a href="{{url('cart')}}" class="mcart-wrapper">
            <i class="fa fa-shopping-bag"></i>
            <span class="top_cart_count"></span>
        </a>
    </div>
</section>
<!--footer section end-->
<script src="{{ asset('frontend_assets') }}/js/proper.min.js"></script>
<script src="{{ asset('frontend_assets') }}/js/bootstrap.min.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/sweetalert.min.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/materialize.min.js"></script>
<script src="{{ asset('frontend_assets') }}/js/owl.carousel.min.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/jquery.exzoom.js"></script>
<script src="{{ asset('frontend_assets') }}/js/custom.js"></script>
<script src="{{ asset('frontend_assets') }}/common/js/common.js"></script>
</body>
</html>