@extends('frontend.primary.layout')
@section('title')
    Home Page
@endsection

@section('main_content')
    @if(count($sliders) > 0)
    <!--slider section start-->
    <section class="slider-section">
        <div class="sliders owl-carousel">
            @foreach($sliders as $row)
            <div class="single-slide-item">
                <img src="{{ asset('public/uploads/'.$row->slider_image) }}" alt="">
            </div>
            @endforeach
        </div>
    </section>
    <!--slider section end-->
    @endif
    @if(count($shop_list) > 0)
    <!--shop list section start-->
    <section class="shop-list-section section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center">
                        <h3 class="text-capitalize">Our shop lists</h3>
                    </div>
                </div>
            </div>
            <div class="shop-lists">
                <div class="row">
                    @foreach($shop_list as $row)
                        <?php
                        if(isset($row->logo)) {
                            $shop_logo = asset('public/uploads/store/'.$row->logo);
                        } else {
                            $shop_logo = asset('frontend_assets/images/shop_logo.png');
                        }
                        ?>
                    <div class="col-lg-2 col-md-3 col-6">
                        <div class="single-shop-list white z-depth-1 p-2">
                            <a href="{{ url('/shop/'.$row->slug) }}">
                                <div class="shop-thumb" style="overflow: hidden;">
                                    <img src="{{ $shop_logo }}" class="img-fluid">
                                </div>
                                <div class="shop-name">
                                    <h5>{{ $row->store_name }}</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-12">
                        <a href="{{ url('/shop_list') }}" class="btn float-right">view all <i
                                    class="material-icons right">chevron_right</i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--shop list section end-->
    @endif

    @if(count($shop_categories) > 0)
    <!--featured category section start-->
    <section class="feat-cat-list section-padding">
        <div class="feat-categories">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title text-center">
                            <h3 class="text-capitalize">Our Product Categories</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach($shop_categories as $row)
                    <div class="col-lg-4 col-md-6 mb-3">
                        <div class="single-cat border">
                            <a href="{{ url('/shop_category/'.$row->slug) }}" class="text-center d-block">{{ $row->name }} <span class="float-right pr-2"><i class="fa fa-check"></i></span></a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--featured category section end-->
    @endif
    <!--promotions section start-->
    <section class="promo-section section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center">
                        <h3 class="text-capitalize">Why People <span><i class="fa fa-heart"></i></span> beEasy</h3>
                    </div>
                    <div class="promo-items d-lg-flex d-block text-center">
                        <div class="single-promo first">
                            <div class="promo-thumb">
                                <img src="{{ asset('frontend_assets') }}/images/lp-features-1.png" alt="" class="img-fluid">
                            </div>
                            <div class="promo-cont">
                                <h4>Convenient & Quick</h4>
                                <p>
                                    No waiting in traffic, no haggling, no worries carrying groceries, they're delivered
                                    right at
                                    your door.
                                </p>
                            </div>
                        </div>
                        <div class="single-promo second">
                            <div class="promo-thumb">
                                <img src="{{ asset('frontend_assets') }}/images/lp-features-2.png" alt="" class="img-fluid">
                            </div>
                            <div class="promo-cont">
                                <h4>Freshly Picked</h4>
                                <p>
                                    Our fresh produce is sourced every morning, you get the best from us.
                                </p>
                            </div>
                        </div>
                        <div class="single-promo third">
                            <div class="promo-thumb">
                                <img src="{{ asset('frontend_assets') }}/images/lp-features-3.png" alt="" class="img-fluid">
                            </div>
                            <div class="promo-cont">
                                <h4>A wide range of Products</h4>
                                <p>
                                    With 4000+ Products to choose from, forget scouring those aisles for hours.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--promotions section end-->
    <!--contact section start-->
    <!--<section class="contact-us-section section-padding wow fadeIn">
        <div class="contact-form">
            <form action="#">
                <div class="text-center">
                    <h3 class="text-capitalize text-white mb-3">Contact Us</h3>
                </div>
                <div class="input-field">
                    <input id="name" type="text" class="validate">
                    <label for="name">Your Name</label>
                </div>
                <div class="input-field">
                    <input id="email" type="email" class="validate">
                    <label for="email">Your Email</label>
                </div>
                <div class="form-group">
                <textarea name="message" id="message" class="form-control" cols="10" rows="5"
                          placeholder="Your Message..."></textarea>
                </div>
                <button class="btn float-right">Send</button>
            </form>
        </div>
        <div class="gmap">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3647.8154083642294!2d89.14911481486337!3d23.896163489109448!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39fe979e6b07ac57%3A0x6aa0662e3cf36e65!2sGiniLab%20Bangladesh!5e0!3m2!1sen!2sbd!4v1577455417015!5m2!1sen!2sbd"
                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </section>-->
    <!--contact section end-->
@endsection
