@if(Session::has('admin_id'))
<?php $layout_file = 'backend.layout'; $maincontent = 'maincontent'; $title = ''; ?>
@endif

@if(Session::has('customer_id'))
<?php 
    $layout_file = 'frontend.'.$site_settings->theme.'.layout';
    $maincontent = 'main_content'; $title = 'Messages'; 
?>
@endif

@extends($layout_file)
@section('title') {{ $title }} @endsection

@section($maincontent)
<link rel="stylesheet" href="{{ asset('resources/views/messages/style.css?'.time()) }}">

@if(Session::has('admin_id'))
    @include('messages.inbox')
@endif

@if(Session::has('customer_id'))
<link href="{{ asset('/') }}assets/css/select2.min.css" rel="stylesheet">
<script src="{{ asset('/') }}assets/js/select2.min.js"></script>
<style>
.select2-container .select2-selection--single {
    height: 37px;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 32px;
}
textarea{
    border: 1px solid #555555 !important;
}
</style>
<section class="customer-profile section-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 ">
                @include('frontend.common.customer.sidebar_menu')
            </div>
            <div class="col-md-9">
                @include('messages.inbox')
            </div>
        </div>
    </div>
</section>
@endif

@endsection
