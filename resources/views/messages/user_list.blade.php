<ul>
  @if(count($result) > 0)
    @foreach($result as $row)
      @if($filter_type == $row['user_type'])
        <li class="view_chat_message" id="chat_id_{{ $row['id'] }}" data-chatid="{{ $row['id'] }}" data-userid="{{ $row['user_id'] }}" data-usertype="{{ $row['user_type'] }}">
          <div class="user-photo">
            <img src="{{ $row['user_photo'] }}" alt="{{ $row['user_name'] }}">
          </div>
          <div class="user-name {{ ($row['sender'] !== $sender_id && $row['is_read'] == 0) ? 'unread' : 'read' }}">
            <h6>{{ $row['user_name'] }}</h6>
            <p>{{ $row['message'] }} <span> {{ date_format(new DateTime($row['message_time']), 'h:i a d-M-Y') }}</span></p>
          </div>
        </li>
        @endif
        @if($filter_type == 'all')
        <li class="view_chat_message" id="chat_id_{{ $row['id'] }}" data-chatid="{{ $row['id'] }}" data-userid="{{ $row['user_id'] }}" data-usertype="{{ $row['user_type'] }}">
          <div class="user-photo">
            <img src="{{ $row['user_photo'] }}" alt="{{ $row['user_name'] }}">
          </div>
          <div class="user-name {{ ($row['sender'] !== $sender_id && $row['is_read'] == 0) ? 'unread' : 'read' }}">
            <h6>{{ $row['user_name'] }}</h6>
            <p>{{ $row['message'] }} <span> {{ date_format(new DateTime($row['message_time']), 'h:i a d-M-Y') }}</span></p>
          </div>
        </li>
        @endif
    @endforeach
    @endif
  </ul>