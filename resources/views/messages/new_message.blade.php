<div id="new_message_form">
    <div class="form-group">
        <div class="back_button">
            <i class="fa fa-arrow-left"></i> Back
          </div>
    </div>
    <input type="hidden" id="is_new" value="yes">
    <div class="form-group">
        <label for="">Message to</label>
        <select id="reciver_type" class="form-control">
            <option value="">Select One</option>
            @if(Session::has('admin_id') && Session::get('role') == 0)
            <option value="shop_admin">Shop Admin</option>
            <option value="customer">Customer</option>
            @endif
            @if(Session::has('admin_id') && Session::get('role') == 1)
            <option value="shop_admin">Shop Admin</option>
            <option value="super_admin">Super Admin</option>
            <option value="customer">Customer</option>
            @endif
            @if(Session::has('customer_id'))
            <option value="shop_admin">Shop Admin</option>
            <option value="super_admin">Super Admin</option>
            @endif
        </select>
    </div>
    <div id="user_search_result"></div>
</div>