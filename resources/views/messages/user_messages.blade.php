@if(Session::has('customer_id'))
<style>
  @media only screen and (max-width: 767px) {
    .header-section {
      display: none !important;
      visibility: hidden;
    }
}
</style>
@endif
<header>
    <img src="{{ $user_info['photo_path'] }}" alt="{{ $user_info['name'] }}">
    <div>
      <h2>{{ $user_info['name'] }}</h2>
    </div>
    <div class="back_button">
      <i class="fa fa-arrow-left"></i> Back
    </div>
  </header>
  <ul id="chat">
    @if(count($messages) > 0)
      @foreach($messages as $row)
        @if($row->sender == $sender_id)
        <li class="me">
          <div class="entete">
            <h2>You</h2>
            <h3>{{ date_format(new DateTime($row->created_at), 'h:i a d-M-Y') }}</h3>
          </div>
          <div class="message">
            {{ $row->message }}
          </div>
        </li>
        @else
        <li class="you">
          <div class="entete">
            <h2>{{ $user_info['name'] }}</h2>
            <h3>{{ date_format(new DateTime($row->created_at), 'h:i a d-M-Y') }}</h3>
          </div>
          <div class="message">
            {{ $row->message }}
          </div>
        </li>
        @endif
      @endforeach
    @endif
  </ul>
  <div class="chatbox-footer">
    <input type="hidden" id="reciver_id" value="{{ $reciver_id }}">
    <input type="hidden" id="reciver_type" value="{{ $reciver_type }}">
    <input type="hidden" id="is_new" value="no">
    <textarea placeholder="Type your message" id="message_body"></textarea>
    <button id="send_new_message" type="submit">SEND</button>
  </div>