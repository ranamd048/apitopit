<div id="chatbox-container">
    <div class="chatbox-sidebar">
      <header>
        <a href="#" id="new-message"><i class="fa fa-plus"></i> New Message</a>
        <div>
            <select id="filter_user_type" class="form-control">
                <option value="all">All</option>
                @if(Session::has('admin_id') && Session::get('role') == 0)
                <option value="shop_admin">Shop Admin</option>
                <option value="customer">Customer</option>
                @endif
                @if(Session::has('admin_id') &&  Session::get('role') == 1)
                <option value="shop_admin">Shop Admin</option>
                <option value="super_admin">Super Admin</option>
                <option value="customer">Customer</option>
                @endif
                @if(Session::has('customer_id'))
                <option value="shop_admin">Shop Admin</option>
                <option value="super_admin">Super Admin</option>
                @endif
            </select>
        </div>
      </header>
      <div id="user-list"></div>
    </div>
    <div class="chatbox-messages" id="user_messages"></div>
</div>

<script>
    $(document).ready(function() {
        function user_list(type='all') {
            var url = APP_URL + '/chat/user_list/'+type;
            $.ajax({
                method: "GET",
                url: url,
                success: function(response) {
                    $('#user-list').html(response);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        function user_messages(chat_id, user_id, user_type) {
            var url = APP_URL + '/chat/user_messages?chat_id='+chat_id+'&user_id='+user_id+'&user_type='+user_type;
            $.ajax({
                method: "GET",
                url: url,
                success: function(response) {
                    $('#user_messages').html(response);
                    setTimeout(function(){
                        var d = $("#chat");
                        d.scrollTop(d[0].scrollHeight);
                    }, 1);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        function new_message() {
            var url = APP_URL + '/chat/new_message';
            $.ajax({
                method: "GET",
                url: url,
                success: function(response) {
                    $('#user_messages').html(response);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        user_list();
        new_message();

        $('#new-message').click(function(e) {
          e.preventDefault();
          new_message();
            $('.chatbox-sidebar').removeClass('mobile_show');
            $('#user_messages').removeClass('mobile_hide');

            $('.chatbox-sidebar').addClass('mobile_hide');
            $('#user_messages').addClass('mobile_show');
        });

        $(document).on('click', '.view_chat_message', function(e) {
            e.preventDefault();
            $('.chatbox-sidebar').removeClass('mobile_show');
            $('#user_messages').removeClass('mobile_hide');

            $('.chatbox-sidebar').addClass('mobile_hide');
            $('#user_messages').addClass('mobile_show');

            var chat_id = $(this).data('chatid');
            var user_id = $(this).data('userid');
            var user_type = $(this).data('usertype');
            $('.view_chat_message').removeClass('active');
            $('#chat_id_'+chat_id).addClass('active');
            user_messages(chat_id, user_id, user_type);
        })

        $(document).on('click', '.back_button', function() {
            $('.chatbox-sidebar').removeClass('mobile_hide');
            $('#user_messages').removeClass('mobile_show');

            $('.chatbox-sidebar').addClass('mobile_show');
            $('#user_messages').addClass('mobile_hide');
        });

        $(document).on('change', '#reciver_type', function() {
            var type = $(this).val();
            if(type !== '') {
              var url = APP_URL + '/chat/search_user/'+type;
              $.ajax({
                  method: "GET",
                  url: url,
                  success: function(response) {
                      $('#user_search_result').html(response);
                      $('#reciver_id').select2();
                  },
                  error: function(error) {
                      console.log(error);
                  }
              });
            }
        });

        $(document).on('click', '#send_new_message', function(e) {
          e.preventDefault();
            var type = $('#reciver_type').val();
            var reciver_id = $('#reciver_id').val();
            var message_body = $('#message_body').val();
            var is_new = $('#is_new').val();
            $.ajax({
              method: "POST",
              url: APP_URL + '/chat/send_new_message',
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
              data: { type: type, reciver_id: reciver_id, message_body: message_body, is_new: is_new},
              cache: false,
              success: function(response) {
                if(response.is_new == 'yes') {
                    var filter_type = $('#filter_user_type').val();
                    user_list(filter_type);
                    user_messages(response.chat_id, response.user_id, response.user_type);
                } else {
                    user_messages(response.chat_id, response.user_id, response.user_type);
                }
              },
              error: function(error) {
                  console.log(error);
              }
            });
        });

        $(document).on('change', '#filter_user_type', function() {
            var filter_type = $(this).val();
            user_list(filter_type);
        });


    });
  </script>
