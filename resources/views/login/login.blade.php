<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin Login</title>

    <!-- Global stylesheets -->
    <link href="{{ asset('frontend_assets') }}/css/login/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('frontend_assets') }}/common/css/bootstrap.min.css">
    <link href="{{ asset('frontend_assets') }}/css/login/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('frontend_assets') }}/css/login/layout.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('frontend_assets') }}/css/login/components.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <style>
        .logo {
            width: 60%;
            margin: 0 auto;
            overflow: hidden;
        }
        .logo img {
            width: 100%;
            height: auto;
        }
    </style>
</head>

<body class="bg-slate-800">

<!-- Page content -->
<div class="page-content"
     style="background: url({{ asset('frontend_assets/images/login_bg/'.$site_settings->login_bg) }}) no-repeat; background-size: cover;">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">

            <form class="login-form" action="{{ url('/admin/login') }}" method="POST">

                <div class="card mb-0">
                    <div class="card-body">
                        @csrf
                        <div class="text-center mb-3">
                            <div class="logo">
                                <img src="{{ asset('public/uploads/'.$site_settings->logo) }}" />
                            </div>
                            <hr>
                            <h5 class="mb-0">Login to your account</h5>
                            @if(Session::get('error'))
                                <br>
                                <p class="alert alert-danger">{{ Session::get('error') }}</p>
                            @endif
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="text" name="email" class="form-control" placeholder="Username" required="">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="password" name="password" class="form-control" placeholder="Password"
                                   required="">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                        </div>


                        <span class="form-text text-center text-muted">Developed By  <a href="https://ginilab.com">Ginilab Ltd.</a></span>

                    </div>
                </div>
            </form>

        </div>
        <!-- /content area -->

    </div>

</div>
<!-- /page content -->

<script>
	localStorage.removeItem('pos_discount');
	localStorage.removeItem('pos_delivery');
	localStorage.removeItem('grand_total');
	localStorage.removeItem('discount_type');
	localStorage.removeItem('discount_value');
</script>
</body>
</html>
