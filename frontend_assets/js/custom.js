(function ($) {
    $(document).ready(function () {
        /*sticky header on scroll*/
        $(window).on('scroll', function () {
            var scroll = $(window).scrollTop();
            if (scroll > 0) {
                $("header.header-section").addClass("fixed-top");
            } else {
                $("header.header-section").removeClass("fixed-top");
            }
        });
        //hero slider
        $('.sliders').owlCarousel({
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            loop: true,
            margin: 0,
            autoplay: true,
            autoplayHoverPause: true,
            responsiveClass: true,
            nav: true,
            dots: false,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: true
                },
                1000: {
                    items: 1,
                    nav: true,
                    loop: true
                }
            }
        });
    });
})(jQuery);