$(document).ready(function() {
    //sweetalert message
    function sweet_alert(icon, title, time = 1500) {
        Swal.fire({
            icon: icon,
            title: title,
            showConfirmButton: true,
            timer: time
        });
    }

    function redirect_sweet_alert(icon, title, redirect_link) {
        Swal.fire({
            icon: icon,
            title: title,
            showConfirmButton: true,
        }).then(function() {
            window.location.href = redirect_link;
        });
    }

    //load cart summary
    function load_cart_summary() {
        var url = APP_URL + '/cart_summary';
        $.ajax({
            method: "GET",
            url: url,
            success: function(response) {
                $('#cart_summary').html(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    load_cart_summary();

    //load total cart item number
    function load_cart_count_number() {
        var url = APP_URL + '/cart_count_number';
        $.ajax({
            method: "GET",
            url: url,
            success: function(response) {
                $('.top_cart_count').html(response.count);
                $('.top_total_cart_amount').html(response.subtotal);
            },
            error: function(error) {
                console.log(error);
            }
        });
    }
    load_cart_count_number();

    //add to cart
    $(document).on('click', '.add_to_cart', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var shop_slug = $(this).data('shop_slug');
        var price = $(this).data('price');
        var url = APP_URL + '/cart_add';
        $(this).html('<i class="fa fa-spinner fa-spin"></i>');
        $.ajax({
            method: "POST",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: { id: id, price: price, quantity: 1 },
            cache: false,
            context: this,
            success: function(response) {
                $(this).html('<i class="fa fa-shopping-bag"></i> Cart');
                if (response.status == 'DONE') {
                    load_cart_count_number();
                    if(shop_slug == 'NO') {
                        sweet_alert('success', 'Added To Cart');
                    } else {
                        var redirect = APP_URL + '/shop/'+shop_slug;
                        redirect_sweet_alert('success', 'Added To Cart', redirect);
                    }
                } else if (response.status == 'WARNING') {
                    sweet_alert('error', 'You have already added multiple shop product!', 90000);
                } else if(response.status == 'OUT_OF_STOCK') {
                    sweet_alert('error', 'Out of Stock');
                } else {
                    redirect_sweet_alert('error', 'You can not buy multiple shop product once time.', response.redirect_shop);
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //add to cart details page
    $(document).on('click', '.add_to_cart_details_page', function(e) {
        e.preventDefault();
        var shop_slug = $(this).data('shop_slug');
        var quantity = parseInt($('#cart_qty_number').val());
        var id = $(this).data('id');
        var price = $(this).data('price');
        var url = APP_URL + '/cart_add';
        $.ajax({
            method: "POST",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: { id: id, price: price, quantity: quantity },
            cache: false,
            success: function(response) {
                if (response.status == 'DONE') {
                    load_cart_count_number();
                    if(shop_slug == 'NO') {
                        sweet_alert('success', 'Added To Cart');
                    } else {
                        var redirect = APP_URL + '/shop/'+shop_slug;
                        redirect_sweet_alert('success', 'Added To Cart', redirect);
                    }
                } else if (response.status == 'WARNING') {
                    sweet_alert('error', 'You have already added multiple shop product!', 90000);
                } else if(response.status == 'OUT_OF_STOCK') {
                    sweet_alert('error', 'Out of Stock');
                } else {
                    redirect_sweet_alert('error', 'You can not buy multiple shop product once time.', response.redirect_shop);
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
    //remove cart item
    $(document).on('click', '.remove-cart-item', function() {
        var id = $(this).data('rowid');
        var url = APP_URL + '/remove_cart_item/' + id;
        $.ajax({
            method: "GET",
            url: url,
            success: function(response) {
                if (response == 'DONE') {
                    $('#cart_item_' + id).remove();
                    load_cart_count_number();
                    load_cart_summary();
                    sweet_alert('success', 'Remove Cart Item');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //update cart quantity
    function update_cart_quantity(id, quantity, price, action) {
        var url = APP_URL + '/update_cart_quantity';
        var totalPrice = (quantity * price);
        $.ajax({
            method: "POST",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: { id: id, action: action },
            cache: false,
            success: function(response) {
                if (response == 'DONE') {
                    load_cart_count_number();
                    load_cart_summary();
                    $('#qty_value_' + id).html(quantity);
                    totalPrice = '৳ ' + totalPrice.toFixed(2);
                    $('#cart_price_' + id).html(totalPrice);
                    sweet_alert('success', 'Updated Cart item');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    //cart quantity plus
    $(document).on('click', '.plus-quantity', function() {
        var id = $(this).data('id');
        var price = parseFloat($(this).data('price'));
        var quantity = $('#qty_value_' + id).text();
        quantity = parseInt(quantity) + 1;
        update_cart_quantity(id, quantity, price, 'plus');

    });
    //cart quantity minus
    $(document).on('click', '.minus-quantity', function() {
        var id = $(this).data('id');
        var price = parseFloat($(this).data('price'));
        var quantity = $('#qty_value_' + id).text();
        if (parseInt(quantity) == 1) {
            sweet_alert('error', 'You can not update quantity less than 1');
        } else {
            quantity = parseInt(quantity) - 1;
            update_cart_quantity(id, quantity, price, 'minus');
        }
    });

    //place order
    $(document).on('click', '#place_order_btn', function(e) {
        e.preventDefault();
        var url = APP_URL + '/place_order';
        $.ajax({
            method: "POST",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: $('#order_place_form').serialize(),
            success: function(response) {
                redirect_sweet_alert('success', 'Order Place Successfully', response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //save order
    $(document).on('click', '#save_order_btn', function(e) {
        e.preventDefault();
        var url = APP_URL + '/save_order';
        $.ajax({
            method: "GET",
            url: url,
            success: function(response) {
                redirect_sweet_alert('success', 'Order Save Successfully', response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //add to wishlist
    $(document).on('click', '.add_to_wishlist', function(e) {
        e.preventDefault();
        var id = $(this).data('productid');
        var url = APP_URL + '/add_to_wishlist/' + id;
        $.ajax({
            method: "GET",
            url: url,
            success: function(response) {
                sweet_alert(response.type, response.message);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //delete wishlist
    $(document).on('click', '.delete_wishlist_item', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var url = APP_URL + '/delete_wishlist_item/' + id;
        $.ajax({
            method: "GET",
            url: url,
            success: function(response) {
                sweet_alert(response.type, response.message);
                $('#wishlist_item_id_' + id).remove();
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //delete wishlist
    $(document).on('click', '.delete_favourite_item', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var url = APP_URL + '/delete_favourite_item/' + id;
        $.ajax({
            method: "GET",
            url: url,
            success: function(response) {
                sweet_alert(response.type, response.message);
                $('#favourite_item_id_' + id).remove();
            },
            error: function(error) {
                console.log(error);
            }
        });
    });


    //materialize css activation
    $('.tooltipped').tooltip();
    $('.sidenav').sidenav();
    $('select').formSelect();

    //search customer
    $(document).on('keyup', '#search_customer', function() {
        var value = $(this).val();
        if ($.trim(value) == '') {
            $('#show-customer-list').html('');
        } else {
            var url = APP_URL + '/search_customer_by_phone';
            $.ajax({
                method: "POST",
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { value: value },
                cache: false,
                success: function(response) {
                    $('#show-customer-list').html(response);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    });

    $(document).on('click', '.customer-single-item', function() {
        var customer_id = $(this).data('id');
        var customer_name = $(this).text();
        $('#show_customer_id').val(customer_id);
        $('#show-customer-list').html('');
        $('#search_customer').val(customer_name);
    });

    // magnific popup activation
    $('.popup-icon').magnificPopup({
        delegate: 'a',
        gallery: {
            enabled: true
        },
        type: 'image'
    });
    /*gallery area*/
    $('#gallery_filter').mixItUp();

    //get district/upazila
    function get_selected_district_upazila(id, type) {
        var url = APP_URL + '/get_district_upazila';
        $.ajax({
            method: "POST",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: { id: id, type: type, data_format: 'json' },
            cache: false,
            success: function(response) {
                var Options = "<option value=''>Select ...</option>";

                $.each(response, function(i, val) {
                    Options = Options + "<option value='" + val.id + "'>" + val.name + "</option>";
                });
                $('#' + type).html(Options);
                $("#" + type).formSelect();
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    $('#division_list').change(function() {
        var division_id = $(this).val();
        if (division_id == '') {
            $('#district_list').html('<option value="">Select District</option>');
            $("#district_list").formSelect();
        } else {
            get_selected_district_upazila(division_id, 'district_list');
        }
        $('#upazila_list').html('<option value="">Select Upazila</option>');
        $("#upazila_list").formSelect();
    });

    $('#district_list').change(function() {
        var district_id = $(this).val();
        get_selected_district_upazila(district_id, 'upazila_list');
    });

    //send message
    $(document).on('click', 'button#send_msg', function(e) {
        e.preventDefault();

        var name = $('input#name').val();
        var email = $('input#email').val();
        var message = $('textarea#message').val();
        var system_email = $('input#system_email').val();
        var url = APP_URL + '/send_contact_messge';
        $.ajax({
            method: "POST",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {
                name: name,
                email: email,
                message: message,
                system_email: system_email
            },
            cache: false,
            success: function(response) {
                sweet_alert(response.type, response.message);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //open service request modal
    $(document).on('click', '#open_service_request_modal', function(e) {
        e.preventDefault();
        var url = APP_URL + '/check_user_login';
        $.ajax({
            method: "GET",
            url: url,
            success: function(response) {
                if (response.type == 'error') {
                    sweet_alert(response.type, response.message);
                } else {
                    $('#service-request-modal').addClass('open');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //submit service request
    $(document).on('click', 'button#submit_service_request', function(e) {
        e.preventDefault();

        var formdata = $("#service_request_form").serialize();
        var url = APP_URL + '/send_service_request';
        $.ajax({
            method: "POST",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: formdata,
            cache: false,
            success: function(response) {
                sweet_alert(response.type, response.message);
                $('#service_request_form').trigger("reset");
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //play youtube video
    $('.popup-youtube').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    //submit product review
    $(document).on('click', 'button#submit_review', function(e) {
        e.preventDefault();

        var formdata = $("#product_review_form").serialize();
        var url = APP_URL + '/submit_product_review';
        $.ajax({
            method: "POST",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: formdata,
            cache: false,
            success: function(response) {
                sweet_alert(response.type, response.message);
                $('#product_review_form').trigger("reset");
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    //add to favourite
    $(document).on('click', '.add_to_favourite', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var type = $(this).data('type');
        var url = APP_URL + '/add_to_favourite/' + id + '/' + type;
        $.ajax({
            method: "GET",
            url: url,
            success: function(response) {
                sweet_alert(response.type, response.message);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

});