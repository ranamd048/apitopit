//shop categories
function get_shop_categories(shop_id) {
    var url = APP_URL + '/admin/get_shop_categories/' + shop_id;
    $.ajax({
        url: url,
        method: "GET",
        success: function(response) {
            $('#category_list').html(response);
        },
    });
}

//shop categories
function get_shop_sub_categories(shop_id, category_id) {
    var url = APP_URL + '/admin/get_shop_categories/' + shop_id + '?parent_id=' + category_id;
    $.ajax({
        url: url,
        method: "GET",
        success: function(response) {
            $('#sub_category_list').html(response);
        },
    });
}

//load product list
function load_products(shop_id, type = '', value = '', category_type = '', category_id = '') {
    var file_name = $('#file_name').val();
    var url = APP_URL + '/admin/get_all_products';

    var loading = '<div class="col-md-12 text-center"><i class="fa fa-spinner fa-spin"></i></div>';
    $('#load_all_products').html(loading);
    $.ajax({
        method: "POST",
        url: url,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        data: { file_name: file_name, shop_id: shop_id, type: type, value: value, category_type: category_type, category_id: category_id},
        cache: false,
        success: function(response) {
            $('#load_all_products').html(response);
        },
        error: function(error) {
            console.log(error);
        }
    });
}

$(document).ready(function() {
    //search by shop
    $('#shop_list').change(function() {
        var id = $(this).val();
        if (id != '') {
            $('#current_shop_id').val(id);
            load_products(id);
            get_shop_categories(id);
        }
    });

    //search by keyword
    $('#search_query_string').keyup(function() {
        var shop_id = $('#current_shop_id').val();
        var category = $('#category_list').val();
        var subcategory = $('#sub_category_list').val();
        var category_type = '';
        var category_id = '';
        if(category !== '' && subcategory !== '') {
            category_type = 'subcategory';
            category_id = subcategory;
        } else if(category !== '') {
            category_type = 'category';
            category_id = category;
        }

        var value = $(this).val();
        value = $.trim(value);
        if (value == '') {
            load_products(shop_id, '', '', category_type, category_id);
        } else {
            load_products(shop_id, 'search', value, category_type, category_id);
        }
    });

    //search by location
    $('#search_by_location').keyup(function() {
        var shop_id = $('#current_shop_id').val();
        var category = $('#category_list').val();
        var subcategory = $('#sub_category_list').val();
        var category_type = '';
        var category_id = '';
        if(category !== '' && subcategory !== '') {
            category_type = 'subcategory';
            category_id = subcategory;
        } else if(category !== '') {
            category_type = 'category';
            category_id = category;
        }

        var value = $(this).val();
        value = $.trim(value);
        if (value == '') {
            load_products(shop_id, '', '', category_type, category_id);
        } else {
            load_products(shop_id, 'product_location', value, category_type, category_id);
        }
    });

    //search by category
    $('#category_list').change(function() {
        var shop_id = $('#current_shop_id').val();
        var id = $(this).val();
        if (id == '') {
            var html = '<option value="">Select Sub Category</option>';
            $('#sub_category_list').html(html);
            load_products(shop_id);
        } else {
            get_shop_sub_categories(shop_id, id);
            load_products(shop_id, 'category', id);
        }
    });

    //search by sub category
    $('#sub_category_list').change(function() {
        var shop_id = $('#current_shop_id').val();
        var id = $(this).val();
        var category_id = $('#category_list').val();
        if (id == '') {
            load_products(shop_id, 'category', category_id);
        } else {
            load_products(shop_id, 'sub_category', id);
        }
    });

});