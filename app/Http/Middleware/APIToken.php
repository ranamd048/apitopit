<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\DB;

class APIToken
{
    use HelperTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $access_token = $request->header('Authorization');
        if($access_token){
            $check_token = DB::table('admins')->select('api_token')->where('api_token', $access_token)->first();
            if($check_token) {
                return $next($request);
            } else {
                return $this->apiResponse(false, 'Authentication Failed!', [], 401);
            }
        }
        return $this->apiResponse(false, 'Authentication Failed!', [], 401);
    }
}
