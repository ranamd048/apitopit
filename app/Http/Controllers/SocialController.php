<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use Socialite;

class SocialController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider)
    {
        $userSocial =   Socialite::driver($provider)->stateless()->user();
        $users      =   User::where(['email' => $userSocial->getEmail()])->first();
        if($users){
            Auth::attempt(['email' => $users->email, 'password' => $users->provider_id ]);
            Session::put('customer_id', $users->id);
            return redirect('product_list');
        }else{
            $user = User::create([
                'name'          => $userSocial->getName(),
                'email'         => $userSocial->getEmail(),
                'photo'         => $userSocial->getAvatar(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
                'password'      => bcrypt($userSocial->getId()),
            ]);
            
            Session::put('customer_id', $user->id);
            Auth::attempt(['email' => $user->email, 'password' => $user->provider_id ]);
            return redirect('product_list');
            
        }
    }
}
