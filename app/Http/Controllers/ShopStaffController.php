<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;

class ShopStaffController extends Controller
{
    public function index() {
        $data['result'] = DB::table('shop_staffs')->where('store_id', Session::get('store_id'))->get();
        return view('backend.shop_staffs.list', $data);
    }

    public function addStaff() {
        return view('backend.shop_staffs.add');
    }

    public function saveStaff(Request $request) {
        $destinationPath = public_path('uploads/');
        if ($request->hasFile('profile_picture')) {
            $profile_picture = $request->file('profile_picture');
            $profile_picture_name = 'shop_staff_photo' . time() . '.' . $profile_picture->getClientOriginalExtension();
            $profile_picture->move($destinationPath, $profile_picture_name);
        } else {
            $profile_picture_name = '';
        }
        DB::table('shop_staffs')->insert([
            'store_id' => Session::get('store_id'),
            'name' => trim($request->name),
            'contact' => trim($request->contact),
            'address' => trim($request->address),
            'profile_picture' => $profile_picture_name
        ]);

        return redirect('admin/shop-staffs')->with('message', 'Successfully Added');
    }

    public function editStaff($id) {
        $data['result'] = DB::table('shop_staffs')->where('id', $id)->first();
        return view('backend.shop_staffs.edit', $data);
    }

    public function updateStaff(Request $request) {
        $destinationPath = public_path('uploads/');
        if ($request->hasFile('profile_picture')) {
            $profile_picture = $request->file('profile_picture');
            $profile_picture_name = 'shop_staff_photo' . time() . '.' . $profile_picture->getClientOriginalExtension();
            $profile_picture->move($destinationPath, $profile_picture_name);
        } else {
            $profile_picture_name = $request->existing_photo;
        }
        DB::table('shop_staffs')->where('id', $request->staff_id)->update([
            'name' => trim($request->name),
            'contact' => trim($request->contact),
            'address' => trim($request->address),
            'profile_picture' => $profile_picture_name
        ]);

        return redirect('admin/shop-staffs')->with('message', 'Successfully Updated');

    }

    public function attendance() {
        if(isset($_GET['from_date']) && isset($_GET['to_date']) && isset($_GET['staff_id'])) {
            $data['result'] = DB::table('shop_staffs')
                ->join('staff_attendance', 'shop_staffs.id', '=', 'staff_attendance.staff_id')
                ->select('staff_attendance.*', 'shop_staffs.name')
                ->where(['shop_staffs.store_id' => Session::get('store_id'), 'shop_staffs.id' => $_GET['staff_id']])
                ->whereBetween('staff_attendance.attendance_date', [$_GET['from_date'], $_GET['to_date']])
                ->orderBy('staff_attendance.attendance_date', 'ASC')
                ->get();
        } else {
            $data['result'] = DB::table('shop_staffs')
                ->join('staff_attendance', 'shop_staffs.id', '=', 'staff_attendance.staff_id')
                ->select('staff_attendance.*', 'shop_staffs.name')
                ->where('shop_staffs.store_id', Session::get('store_id'))
                ->whereDate('staff_attendance.attendance_date', Carbon::today())
                ->get();
        }
        $data['staff_list'] = DB::table('shop_staffs')->select('id', 'name')->where('store_id', Session::get('store_id'))->get();
        return view('backend.shop_staffs.attendance', $data);
    }

    public function takeAttendance() {
        $data['result'] = DB::table('shop_staffs')->select('id', 'name')->where('store_id', Session::get('store_id'))->get();
        return view('backend.shop_staffs.take_attendance', $data);
    }

    public function saveAttendance(Request $request) {
        if(is_array($request->staffs)) {
            foreach($request->staffs as $staff_id) {
                DB::table('staff_attendance')->insert([
                    'staff_id' => $staff_id,
                    'status' => $request->status[$staff_id],
                    'in_time' => trim($request->in_time[$staff_id]),
                    'out_time' => trim($request->out_time[$staff_id]),
                    'note' => trim($request->note[$staff_id]),
                    'attendance_date' => $request->attendance_date
                ]);
            }
            return redirect()->back()->with('message', 'Attendance Taken Successfully');
        }
    }

    public function editAttendance($id) {
        $data['result'] = DB::table('shop_staffs')
            ->join('staff_attendance', 'shop_staffs.id', '=', 'staff_attendance.staff_id')
            ->select('staff_attendance.*', 'shop_staffs.name')
            ->where('staff_attendance.id', $id)
            ->first();
        return view('backend.shop_staffs.edit_attendance', $data);
    }

    public function updateAttendance(Request $request) {
        DB::table('staff_attendance')->where(['id' => $request->attendance_id, 'staff_id' => $request->staff_id])->update([
            'status' => $request->status,
            'in_time' => trim($request->in_time),
            'out_time' => trim($request->out_time),
            'note' => trim($request->note),
            'attendance_date' => $request->attendance_date
        ]);
        return redirect()->back()->with('message', 'Attendance Updated Successfully');
    }

    public function staffPayments() {
        if(isset($_GET['from_date']) && isset($_GET['to_date']) && isset($_GET['staff_id'])) {
            $data['payments'] = DB::table('shop_staffs')
                ->join('staff_payments', 'shop_staffs.id', '=', 'staff_payments.staff_id')
                ->select('staff_payments.*', 'shop_staffs.name')
                ->whereBetween('staff_payments.created_at', [$_GET['from_date'], $_GET['to_date']])
                ->where(['shop_staffs.store_id' => Session::get('store_id'), 'shop_staffs.id' => $_GET['staff_id']])
                ->orderBy('staff_payments.created_at', 'DESC')
                ->get();
        } else {
            $data['payments'] = DB::table('shop_staffs')
                ->join('staff_payments', 'shop_staffs.id', '=', 'staff_payments.staff_id')
                ->select('staff_payments.*', 'shop_staffs.name')
                ->where('shop_staffs.store_id', Session::get('store_id'))
                ->orderBy('staff_payments.created_at', 'DESC')
                ->get();
        }
        $data['staff_list'] = DB::table('shop_staffs')->select('id', 'name')->where('store_id', Session::get('store_id'))->get();
        return view('backend.shop_staffs.payments', $data);
    }

    public function addPayment() {
        $data['staff_list'] = DB::table('shop_staffs')->select('id', 'name')->where('store_id', Session::get('store_id'))->get();
        return view('backend.shop_staffs.add_payment', $data);
    }

    public function savePayment(Request $request) {
        //save also expense table
        $staff_category = DB::table('expense_category')->where(['store_id' => Session::get('store_id'), 'name' => 'Staff'])->first();
        if($staff_category) {
            $staff_category_id = $staff_category->id;
        } else {
            DB::table('expense_category')->insert([
                'store_id' => Session::get('store_id'),
                'name' => 'Staff'
            ]);
            $staff_category_id = DB::getPdo()->lastInsertId();
        }
        $check_staff_payment = DB::table('expenses')->where('category_id', $staff_category_id)->whereDate('date', Carbon::today())->first();
        if($check_staff_payment) {
            DB::table('expenses')->where('id', $check_staff_payment->id)->update([
                'amount' => ($check_staff_payment->amount + trim($request->amount))
            ]);
        } else {
            DB::table('expenses')->insert([
                'category_id' => $staff_category_id,
                'amount' => trim($request->amount),
                'created_by' => Session::get('admin_id')
            ]);
        }

        DB::table('staff_payments')->insert([
            'staff_id' => $request->staff_id,
            'amount' => trim($request->amount),
            'note' => trim($request->note),
            'paid_by' => Session::get('admin_id'),
        ]);
        return redirect()->back()->with('message', 'Payment Added Successfully');
    }

    public function editPayment($id) {
        $data['result'] = DB::table('shop_staffs')
            ->join('staff_payments', 'shop_staffs.id', '=', 'staff_payments.staff_id')
            ->select('staff_payments.*', 'shop_staffs.name')
            ->where('staff_payments.id', $id)
            ->first();
        return view('backend.shop_staffs.edit_payment', $data);
    }

    public function updatePayment(Request $request) {
        DB::table('staff_payments')->where(['id' => $request->payment_id, 'staff_id' => $request->staff_id])->update([
            'amount' => trim($request->amount),
            'note' => trim($request->note),
            'paid_by' => Session::get('admin_id'),
        ]);
        
        $staff_category = DB::table('expense_category')->where(['store_id' => Session::get('store_id'), 'name' => 'Staff'])->first();
        $check_staff_payment = DB::table('expenses')->where('category_id', $staff_category->id)->whereDate('date', Carbon::today())->first();
        if($check_staff_payment) {
            $total_amount = DB::table('shop_staffs')
            ->join('staff_payments', 'shop_staffs.id', '=', 'staff_payments.staff_id')
            ->select(DB::raw("SUM(staff_payments.amount) as total_amount"))
            ->where('shop_staffs.store_id', Session::get('store_id'))
            ->whereDate('staff_payments.created_at', Carbon::today())
            ->first();
            DB::table('expenses')->where('id', $check_staff_payment->id)->update([
                'amount' => $total_amount->total_amount
            ]);
        }
        return redirect()->back()->with('message', 'Payment Updated Successfully');
    }

    public function deleteStaffModal($id) {
        $data['result'] = DB::table('shop_staffs')->select('id', 'name')->where('id', $id)->first();
        return view('backend.shop_staffs.delete', $data);
    }

    public function deleteStaff(Request $request) {
        $check_password = DB::table('admins')->where(['id' => Session::get('admin_id'), 'password' => md5($request->password)])->first();
        if($check_password) {
            DB::table('staff_payments')->where('staff_id', $request->staff_id)->delete();
            DB::table('staff_attendance')->where('staff_id', $request->staff_id)->delete();
            DB::table('shop_staffs')->where('id', $request->staff_id)->delete();
            return redirect()->back()->with('message', 'Shop Staff Deleted Successfully');
        } else {
            return redirect()->back()->with('error_message', 'Sorry! Wrong Password');
        }
    }



}
