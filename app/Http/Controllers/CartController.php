<?php

namespace App\Http\Controllers;

use App\Http\Traits\HelperTrait;
use Illuminate\Http\Request;
use App\Model\Product;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    use HelperTrait;

    public function add(Request $request) {
        $system_settings = DB::table('system_settings')->select('multiple_shop_cart')->where('id', 1)->first();
        $check_stock = $this->checkProductStock($request->id);
        if(!$check_stock) {
            return ['status' => 'OUT_OF_STOCK'];
        }

        $product = Product::find($request->id);

        if($system_settings->multiple_shop_cart == 0) {
            if($this->multipleStoreBuyProtect($product->store_id) !== Session::get('cart_first_product_store_id')) {
                $shop = DB::table('stores')->select('slug')->where('id', Session::get('cart_first_product_store_id'))->first();
                return ['status' => 'ANOTHER_SHOP', 'redirect_shop' => url('shop/'.$shop->slug)];
            }
        } else {
            //multiple store warning
            if(!Session::has('multiple_store_warning')) {
                if($this->multipleStoreWarning($product->store_id)) {
                    Session::put('multiple_store_warning', 'yes');
                    return ['status' => 'WARNING'];
                }
            }
        }
        
        \Cart::add(array(
            'id' => $product->id,
            'name' => $product->product_name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'attributes' => array(
                'slug' => $product->product_slug,
                'image' => $product->product_image,
                'store_id' => $product->store_id
            ) 
        ));

        return ['status' => 'DONE'];
    }

    public function cart_count_number() {
        $content = \Cart::getContent();
        $subtotal = \Cart::getTotal();
        $subtotal = '৳ '.number_format($subtotal, 2);
        return response()->json(['count' => $content->count(), 'subtotal' => $subtotal]);
    }

    public function remove($id) {
        $content = \Cart::getContent();
        if($content->count() == 1) {
            Session::forget('multiple_store_warning');
            Session::forget('cart_first_store_id');
        }
        \Cart::remove($id);
        $cart_item = \Cart::getContent();
        if($cart_item->count() == 0) {
            Session::forget('cart_first_product_store_id');
        }
        return 'DONE';
    }

    public function update(Request $request) {
        if($request->action == 'plus') {
            $quantity = 1;
        } else {
            $quantity = -1;
        }
        \Cart::update($request->id, array(
            'quantity' => $quantity
        ));
        return 'DONE';
    }

    public function cart_summary() {
        $data['cart_item'] = \Cart::getContent();
        $data['total_amount'] = \Cart::getTotal();
        return view('frontend.common.cart_summary', $data);
    }

    public function check() {
        $content = \Cart::getContent();
        return response()->json($content);
    }

}
