<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\Validator;
use App\Model\Product;
use App\Model\Service;

class CommonController extends Controller
{
    use HelperTrait;

    public function getSimilarItems() {
        $type = isset($_GET['type']) && !empty($_GET['type']) ? $_GET['type'] : null;
        $id = isset($_GET['id']) && !empty($_GET['id']) ? $_GET['id'] : null;
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : 15;

        if($type && $id) {
            if($type == 'shop') {
                $shop_info = DB::table('stores')->select('shop_category_id')->where('id', $id)->first();
                if($shop_info && $shop_info->shop_category_id) {
                    if($per_page == 0) {
                        $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where(['stores.shop_category_id' => $shop_info->shop_category_id, 'stores.status' => 1])->where('stores.id', '!=', $id)->get();
                        $result = array('data' => $result);
                    } else {
                        $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where(['stores.shop_category_id' => $shop_info->shop_category_id, 'stores.status' => 1])->where('stores.id', '!=', $id)->paginate($per_page);
                    }
                    if(count($result) > 0) {
                        return $this->apiResponse(true, 'Similar Shop List', $result);
                    } else {
                        return $this->apiResponse(false, 'Similar Shop Not Found', [], 404);
                    }
                } else {
                    return $this->apiResponse(false, 'Wrong ID');
                }
            } elseif($type == 'product') {
                $product = DB::table('products')->where('id', $id)->first();
                if($product) {
                    if($product->sub_subcategory_id) {
                        $related_condition = array('sub_subcategory_id' => $product->sub_subcategory_id, 'status' => 1);
                    } elseif($product->subcategory_id) {
                        $related_condition = array('subcategory_id' => $product->subcategory_id, 'status' => 1);
                    } else {
                        $related_condition = array('category_id' => $product->category_id, 'status' => 1);
                    }
                    if($per_page == 0) {
                        $result = Product::with(array('store' => function($query) {
                            $query->select('id','store_name');
                        }))->where($related_condition)->where('id', '!=', $id)->get();
                        $result = array('data' => $result);
                    } else {
                        $result = Product::with(array('store' => function($query) {
                            $query->select('id','store_name');
                        }))->where($related_condition)->where('id', '!=', $id)->paginate($per_page);
                    }
                    if(count($result) > 0) {
                        return $this->apiResponse(true, 'Similar Product List', $result);
                    } else {
                        return $this->apiResponse(false, 'Similar Product Not Found', [], 404);
                    }
                } else {
                    return $this->apiResponse(false, 'Wrong ID');
                }
            } elseif($type == 'service') {
                $service = DB::table('services')->where('id', $id)->first();
                if($service) {
                    if($service->sub_subcategory_id) {
                        $related_condition = array('sub_subcategory_id' => $service->sub_subcategory_id, 'status' => 1);
                    } elseif($service->subcategory_id) {
                        $related_condition = array('subcategory_id' => $service->subcategory_id, 'status' => 1);
                    } else {
                        $related_condition = array('category_id' => $service->category_id, 'status' => 1);
                    }
                    if($per_page == 0) {
                        $result = Service::with(array('store' => function($query) {
                            $query->select('id','store_name');
                        }))->where($related_condition)->where('id', '!=', $id)->get();
                        $result = array('data' => $result);
                    } else {
                        $result = Service::with(array('store' => function($query) {
                            $query->select('id','store_name');
                        }))->where($related_condition)->where('id', '!=', $id)->paginate($per_page);
                    }
                    if(count($result) > 0) {
                        return $this->apiResponse(true, 'Similar Service List', $result);
                    } else {
                        return $this->apiResponse(false, 'Similar Service Not Found', [], 404);
                    }
                } else {
                    return $this->apiResponse(false, 'Wrong ID');
                }
            }
        } else {
            return $this->apiResponse(false, 'Required param type & id');
        }
    }

    public function divisionList() {
        $result = DB::table('divisions')->get();
        $result = array('data' => $result);
        return $this->apiResponse(true, 'Division List', $result);
    }

    public function districtList() {
        $result = DB::table('districts')->get();
        $result = array('data' => $result);
        return $this->apiResponse(true, 'District List', $result);
    }

    public function upazilaList() {
        $result = DB::table('upazilas')->get();
        $result = array('data' => $result);
        return $this->apiResponse(true, 'Upazila List', $result);
    }

    public function districtUpazilaList($id, $type) {
        if($type !== 'districts' && $type !== 'upazilas') {
            return $this->apiResponse(false, 'Invalid Type. Type should be districts or upazilas');
        }

        $condition = $type == 'districts' ? ['division_id' => $id] : ['district_id' => $id];
        $result = DB::table($type)->where($condition)->get();
        $result = array('data' => $result);
        return $this->apiResponse(true, ucfirst($type).' List by ID', $result);
    }

    public function unitList() {
        $result = DB::table('product_units')->select('id', 'unit_name')->where('unit_status', 1)->get();
        $result = array('data' => $result);
        return $this->apiResponse(true, 'Unit List', $result);
    }

    public function brandList() {
        $result = DB::table('brands')->select('id', 'name')->where('status', 1)->get();
        $result = array('data' => $result);
        return $this->apiResponse(true, 'Brand List', $result);
    }

    public function sendMessage(Request $request) {
        $validator = Validator::make($request->all(), [
            'shop_id' => 'required',
            'phone_number' => 'required|numeric',
            'message_body' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $shop_id = trim($request->shop_id);
        $phone_number = trim($request->phone_number);
        $message_body = trim($request->message_body);

        $shop = DB::table('stores')->select('store_name', 'due_sms_text', 'sms_limit')->where('id', $shop_id)->first();

        if(!$shop) {
            return $this->apiResponse(false, 'Sorry! Shop ID Not Found');
        }

        if($shop->sms_limit > 0) {
            $this->sendSms(array($phone_number), $message_body);
            DB::table('stores')->where('id', $shop_id)->update([
                'sms_limit' => ($shop->sms_limit - 1)
            ]);
            return $this->apiResponse(true, 'Message Send Successfully.');
        } else {
            return $this->apiResponse(false, 'Sorry! Your SMS Limit Has Been Finished.');
        }
    }

    public function passwordReset(Request $request) {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        $user = DB::table('admins')->select('id', 'phone', 'email')->where('phone', $request->phone_number)->first();
        if(!$user) {
            return $this->apiResponse(false, 'Sorry! Wrong Phone Number');
        }
        $random_password = rand(100000, 999999);

        DB::table('admins')->where('id', $user->id)->update([
            'password' => md5($random_password)
        ]);
        $message_body = 'Password Updated Successfully. New Password '.$random_password;
        $this->sendSms(array($user->phone), $message_body);
        //send email
        // $system_name = DB::table('site_information')->select('web_title', 'email')->where('id', 1)->first();
        // if(!empty($user->email)) {
        //     $data = array(
        //         'email' => $user->email,
        //         'message_body' => $message_body,
        //         'system_email' => $system_name->email,
        //         'system_name' => $system_name->web_title,
        //     );
        //     Mail::send('emails.reset_password', $data, function($message) use ($data){
        //         $message->to($data['email']);
        //         $message->subject('Updated Password '.$data['system_name']);
        //         $message->from($data['system_email']);
        //     });
        // }

        return $this->apiResponse(true, $message_body);
    }

    public function submitOtpPhone(Request $request) {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required',
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        $user = DB::table('admins')->select('id', 'phone')->where('phone', $request->phone_number)->first();
        if(!$user) {
            return $this->apiResponse(false, 'Sorry! Wrong Phone Number');
        }
        $message_body = 'OTP Request Successfull. OTP='.$request->otp;
        $this->sendSms(array($user->phone), $message_body);
        return $this->apiResponse(true, $message_body);
    }

    public function forgotPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        $user = DB::table('admins')->select('id', 'phone')->where('phone', $request->phone_number)->first();
        if(!$user) {
            return $this->apiResponse(false, 'Sorry! Wrong Phone Number');
        }
        DB::table('admins')->where('id', $user->id)->update([
            'password' => md5($request->password)
        ]);
        return $this->apiResponse(true, 'Password Updated Successfully Done');
    }


}
