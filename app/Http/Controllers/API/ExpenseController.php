<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class ExpenseController extends Controller
{
    use HelperTrait;

    public function getExpenseCategories() {
        $per_page = 15;
        $condition = array();
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if(isset($_GET['shop_id']) && !empty($_GET['shop_id'])) {
            $condition = array('store_id' => $_GET['shop_id']);
        }
        if($per_page == 0) {
            $result = DB::table('expense_category')->where($condition)->get();
            $result = array('data' => $result);
        } else {
            $result = DB::table('expense_category')->where($condition)->paginate($per_page);
        }
        
        if(count($result) > 0) {
            return $this->apiResponse(true, 'Expense Categories', $result);
        } else {
            return $this->apiResponse(false, 'Expense Categories Not Found', [], 404);
        }
    }

    public function storeExpenseCategories(Request $request) {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        DB::table('expense_category')->insert([
	    	'store_id' => $request->store_id,
	    	'name' => trim($request->name),
        ]);
        $expense_category = $request->all();
        $expense_category['id'] = DB::getPdo()->lastInsertId();
        return $this->apiResponse(true, 'Expense Category Added Successfully', $expense_category, 201);
    }

    public function getExpenses() {
        $per_page = 15;
        $condition = array();
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if(isset($_GET['shop_id']) && !empty($_GET['shop_id'])) {
            $condition = array('expense_category.store_id' => $_GET['shop_id']);
        }

        if($per_page == 0) {
            $result = DB::table('expenses')
            ->join('expense_category', 'expenses.category_id', '=', 'expense_category.id')
			->join('admins', 'expenses.created_by', '=', 'admins.id')
			->where($condition)
			->select('expenses.*', 'expense_category.name as category_name', 'admins.name as user_name')
			->orderBy('expenses.id', 'DESC')
			->get();
            $result = array('data' => $result);
        } else {
            $result = DB::table('expenses')
            ->join('expense_category', 'expenses.category_id', '=', 'expense_category.id')
			->join('admins', 'expenses.created_by', '=', 'admins.id')
			->where($condition)
			->select('expenses.*', 'expense_category.name as category_name', 'admins.name as user_name')
			->orderBy('expenses.id', 'DESC')
			->paginate($per_page);
        }
        
        if(count($result) > 0) {
            return $this->apiResponse(true, 'Expense List', $result);
        } else {
            return $this->apiResponse(false, 'Expenses Not Found', [], 404);
        }
    }

    public function storeExpenses(Request $request) {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'amount' => 'required',
            'created_by' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        if (!file_exists(public_path('uploads/expense_attachments'))) {
			mkdir(public_path('uploads/expense_attachments'), 0777, true);
		}
		$destinationPath = public_path('uploads/expense_attachments/');
        if (!empty($request->attachment)) {
            $attachment_image_name = 'attachment'.time().'.' . explode('/', explode(':', substr($request->attachment, 0, strpos($request->attachment, ';')))[1])[1];
            Image::make($request->attachment)->save($destinationPath .$attachment_image_name);
        } else {
            $attachment_image_name = null;
        }
        DB::table('expenses')->insert([
	    	'category_id' => $request->category_id,
	    	'amount' => trim($request->amount),
	    	'note' => trim($request->note),
	    	'created_by' => $request->created_by,
	    	'attachment' => $attachment_image_name,
	    	'date' => $request->date
	    ]);
        $expense_id = DB::getPdo()->lastInsertId();
        $expenses = $request->all();
        $expenses['attachment'] = $attachment_image_name;
        $expenses['id'] = $expense_id;
        return $this->apiResponse(true, 'Expense Added Successfull', $expenses, 201);
    }


}
