<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    use HelperTrait;

    public function userList() {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if($per_page == 0) {
            $users = User::all();
            $users = array('data' => $users);
        } else {
            $users = User::paginate($per_page);
        }

        return $this->apiResponse(true, 'User List', $users);
    }

    public function getShopUserList($shop_id) {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if($per_page == 0) {
            $users = DB::table('users')
            ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
            ->select('users.*', 'shop_customers.shop_id')
            ->where('shop_customers.shop_id', $shop_id)
            ->get();
            $users = array('data' => $users);
        } else {
            $users = DB::table('users')
            ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
            ->select('users.*', 'shop_customers.shop_id')
            ->where('shop_customers.shop_id', $shop_id)
            ->paginate($per_page);
        }

        if(count($users) > 0) {
            $success = true;
            $message = 'Shop User List';
            $code = 200;
        } else {
            $success = false;
            $message = 'Shop User Not found';
            $code = 404;
        }
        return $this->apiResponse($success, $message, $users, $code);
    }

    public function userRegistration(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'phone' => 'required|numeric|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'gender' => 'required|numeric',
            'address' => 'required|string',
            'thana' => 'required|string',
            'district' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        $user = User::create([
            'name' => trim($request->name),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'password' => bcrypt($request->password),
            'gender' => $request->gender,
            'address' => trim($request->address),
            'thana' => trim($request->thana),
            'district' => trim($request->district),
        ]);
        return $this->apiResponse(true, 'Registration Successfully Done', $user, 201);
    }

    public function userLogin(Request $request) {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        // if(Auth::attempt(['phone' => request('phone'), 'password' => request('password')])){
        //     $user = Auth::user();
        //     if($user->status == 0)
        //     {
        //         return $this->apiResponse(false, 'Inactive User');
        //     }
        //     $data['token'] =  'Bearer '.$user->createToken('MyApp')->accessToken;
        //     $data['user'] = ['id' => $user->id, 'name' => $user->name, 'phone' => $user->phone];
        //     return $this->apiResponse(true, 'Login Successfully Done', $data);
        // }
        // else{
        //     return $this->apiResponse(false, 'Login Failed', ['error' => 'Username/Password Not match'], 401);
        // }

        if(Auth::attempt(['phone' => request('phone'), 'password' => request('password')])){
            $user = Auth::user();
            if($user->status == 0)
            {
                return $this->apiResponse(false, 'Inactive User');
            }
            $payload = md5($user->id.$user->phone);
            $api_unique_token = $payload.time();
            DB::table('users')->where('id', $user->id)->update([
                'api_token' => $api_unique_token
            ]);
            $user->access_token = $api_unique_token;
            $user_data = $user;
            unset($user->reference_id);
            unset($user->store_id);
            unset($user->reset_code);
            unset($user->api_token);
            $response_data = array('data' => $user_data);
            return $this->apiResponse(true, 'Login Successfull', $response_data);
        }
        else{
            return $this->apiResponse(false, 'Login Failed', ['error' => 'Username/Password Not match'], 401);
        }
    }

    public function userProfile() {
        $headers = apache_request_headers();
        $token = $headers['api-key'];
        $data= $this->getDataFromToken($token, 'customer');
        return $this->apiResponse(true, 'User Data', $data);
    }

    public function userProfileUpdate(Request $request) {
        $user = Auth::user();
        if(trim($request->phone) == $user->phone) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'address' => 'required|string',
                'thana' => 'required|string',
                'district' => 'required|string'
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'phone' => 'required|numeric|unique:users',
                'address' => 'required|string',
                'thana' => 'required|string',
                'district' => 'required|string'
            ]);
        }

        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $destinationPath = public_path('uploads/user/');
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photo_path = 'profile_photo'.time().'.'.$photo->getClientOriginalExtension();
            $photo->move($destinationPath, $photo_path);
        } else {
            $photo_path = '';
        }

        User::where('id', $user->id)->update([
            'name' => trim($request->name),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'address' => trim($request->address),
            'thana' => trim($request->thana),
            'district' => trim($request->district),
            'post_code' => trim($request->post_code),
            'photo' => $photo_path,
            'about_me' => trim($request->about_me),
        ]);
        return $this->apiResponse(true, 'User Data Updated Successfully');
    }

    public function userLogout() {
        // $user = Auth::user()->token();
        // $user->revoke();
        // return $this->apiResponse(true, 'Logout Successfully Done');
        $headers = apache_request_headers();
        $token = $headers['api-key'];
        DB::table('users')->where('api_token', $token)->update([
            'api_token' => null
        ]);
        return $this->apiResponse(true, 'Logout Successfull');
    }

    public function getOrderInvoices() {
        $headers = apache_request_headers();
        $token = $headers['api-key'];
        $data= $this->getDataFromToken($token, 'customer');
        $user_id = $data->id;
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if(isset($_GET['shop_id'])) {
            if($per_page == 0) {
                $result = DB::table('orders_invoice')->where(['store_id' => $_GET['shop_id'], 'user_id' => $user_id])->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('orders_invoice')->where(['store_id' => $_GET['shop_id'], 'user_id' => $user_id])->paginate($per_page);
            }
        } else {
            if($per_page == 0) {
                $result = DB::table('orders_invoice')->where(['user_id' => $user_id])->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('orders_invoice')->where(['user_id' => $user_id])->paginate($per_page);
            }
        }

        if(count($result) > 0) {
            return $this->apiResponse(true, 'Incoice List', $result);
        } else {
            return $this->apiResponse(false, 'Incoice Not Found', [], 404);
        }
    }

    public function getInvoiceItems($invoice_id) {
        $result = DB::table('order_items')->where(['invoice_id' => $invoice_id])->get();

        if(count($result) > 0) {
            $result = array('data' => $result);
            return $this->apiResponse(true, 'Incoice Items', $result);
        } else {
            return $this->apiResponse(false, 'Incoice Items Not Found', [], 404);
        }
    }

    public function customerPayment(Request $request) {
        $validator = Validator::make($request->all(), [
            'shop_id' => 'required',
            'user_id' => 'required',
            'total_amount' => 'required',
			'payment_method' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $shop_id = trim($request->shop_id);
        $user_id = trim($request->user_id);
        $total_amount = trim($request->total_amount);
        $payment_method = trim($request->payment_method);
        $payment_note = trim($request->payment_note);
        $user_balance = DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $shop_id])->first();
        if($user_balance) {
            $available_amount = ($total_amount + $user_balance->current_balance);
        } else {
            $available_amount = $total_amount;
        }

        $user_due_orders = DB::table('orders_invoice')->select('id', 'user_id', 'due_amount')->where(['store_id' => $shop_id, 'user_id' => $user_id, 'payment_status' => 0])->get();
        if(count($user_due_orders) > 0) {
            foreach($user_due_orders as $due_order) {
                $invoice_due = $due_order->due_amount;
                $invoice_id = $due_order->id;
                if($available_amount > 0) {
                    if($available_amount >= $invoice_due) {
                        $paid_amount = $invoice_due;
                        $due_amount = 0;
                        $available_amount = ($available_amount - $invoice_due);
                    } else {
                        $paid_amount = $available_amount;
                        $due_amount = ($invoice_due - $available_amount);
                        $available_amount = 0;
                    }
                    DB::table('payments')->insert([
                        'store_id' => $shop_id,
                        'invoice_id' => $invoice_id,
                        'user_id' => $user_id,
                        'paid_amount' => $paid_amount,
                        'payment_method' => $payment_method,
                        'payment_note' => $payment_note
                    ]);
                    //current paid amount
                    $already_paid = DB::table('orders_invoice')->select('paid_amount')->where('id', $invoice_id)->first();
                    DB::table('orders_invoice')->where('id', $invoice_id)->update([
                        'paid_amount' => ($already_paid->paid_amount + $paid_amount),
                        'due_amount' => $due_amount,
                        'payment_status' => ($due_amount == 0) ? 1 : 0,
                    ]);
                }
            }
        }
        //save available amount on payment table
        if($available_amount > 0 && count($user_due_orders) > 0) {
            DB::table('payments')->insert([
                'store_id' => $shop_id,
                'user_id' => $user_id,
                'paid_amount' => $available_amount,
                'payment_method' => $payment_method,
                'payment_note' => $payment_note
            ]);
        }
        if(count($user_due_orders) == 0) {
            DB::table('payments')->insert([
                'store_id' => $shop_id,
                'user_id' => $user_id,
                'paid_amount' => $total_amount,
                'payment_method' => $payment_method,
                'payment_note' => $payment_note
            ]);
        }
        //update customer balance
        if($user_balance) {
            DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $shop_id])->update(['current_balance' => $available_amount]);
        } else {
            DB::table('customer_shop_balance')->insert(['user_id' => $user_id, 'shop_id' => $shop_id,'current_balance' => $available_amount]);
        }
        return $this->apiResponse(true, 'Payment Successfully Completed', [], 200);
    }

    public function singleInvoicePayment(Request $request) {
        $validator = Validator::make($request->all(), [
            'invoice_id' => 'required',
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        $invoice_id = trim($request->invoice_id);
        $user_id = trim($request->user_id);
        $invoice = DB::table('orders_invoice')->select('paid_amount', 'due_amount', 'payment_status')->where(['id' => $invoice_id, 'user_id' => $user_id])->first();
        if($invoice) {
            if($invoice->due_amount == 0 && $invoice->payment_status == 1) {
                return $this->apiResponse(false, 'Invoice Already Paid', [], 400);
            } else {
                DB::table('orders_invoice')->where(['id' => $invoice_id, 'user_id' => $user_id])->update([
                    'paid_amount' => ($invoice->paid_amount + $invoice->due_amount),
                    'due_amount' => 0,
                    'payment_status' => 1
                ]);
                return $this->apiResponse(true, 'Invoice Successfully Paid');
            }
        } else {
            return $this->apiResponse(false, 'Invoice Not Found', [], 404);
        }
    }

    public function customerPaymentHistory($user_id, $shop_id) {
        $customer = DB::table('users')->select('users.id', 'users.name', 'users.email', 'users.phone', 'users.address', 'users.thana', 'users.district', 'users.division', 'users.photo')->where(['id' => $user_id])->first();

        if(!$customer) {
            return $this->apiResponse(false, 'Customer Not Found', [], 404);
        }

		$customer_balance = DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $shop_id])->first();
		if($customer_balance) {
			$customer->current_balance = $customer_balance->current_balance;
			$customer->shop_id = $customer_balance->shop_id;
		} else {
			$customer->current_balance = 0;
			$customer->shop_id = $shop_id;
		}

        $payment_summary = DB::table('orders_invoice')
            ->select(DB::raw("SUM(orders_invoice.due_amount) as total_due_amount"), DB::raw("SUM(orders_invoice.paid_amount) as total_paid_amount"))
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $user_id])
            ->first();

        $payment_history = DB::table('orders_invoice')
            ->join('payments', 'orders_invoice.id', '=', 'payments.invoice_id')
            ->select('payments.*')
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $user_id])
            ->orderByDesc('payments.id')
            ->get();

		if($payment_summary) {
			$customer->total_due_amount = $payment_summary->total_due_amount ? $payment_summary->total_due_amount : 0;
			$customer->total_paid_amount = $payment_summary->total_paid_amount ? $payment_summary->total_paid_amount : 0;
		} else {
			$customer->total_due_amount = 0;
			$customer->total_paid_amount = 0;
		}


        $result = array(
            'data' => array(
                'customer' => $customer,
                'payment_history' => $payment_history
            )
        );

        return $this->apiResponse(true, 'Customer Payment Summary', $result);
    }

    public function socialRegistration(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'provider_id' => 'required|numeric',
            'provider' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        $user = User::where('provider_id', trim($request->provider_id))
            ->where('provider', trim($request->provider))
            ->where('email', trim($request->email))
            ->first();
        $msg = "";
        if ($user) {
            $msg = "Successfully Login";
        } else {
            $user = User::create([
                'name' => trim($request->name),
                'provider_id' => trim($request->provider_id),
                'provider' => trim($request->provider),
                'email' => trim($request->email),
                'password' => bcrypt($request->provider_id),
                'photo' => trim($request->avatar),
            ]);
            $msg = "Registration Successfully Done";
        }
        return $this->apiResponse(true, $msg, $user, 201);
    }

    public function getUserShopOrderList() {
        $headers = apache_request_headers();
        $token = $headers['api-key'];
        $data= $this->getDataFromToken($token, 'customer');
        $user_id = $data->id;

        $shop_orders = DB::table('orders_invoice')
        ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
        ->where(['orders_invoice.user_id' => $user_id, 'is_pos' => 0])
        ->select(DB::raw('orders_invoice.id, orders_invoice.user_id, orders_invoice.store_id, SUM(orders_invoice.total_amount) as total_amount, SUM(orders_invoice.paid_amount) as paid_amount, SUM(orders_invoice.due_amount) as due_amount, stores.store_name, stores.address'))
        ->groupBy('orders_invoice.store_id')
        ->orderByDesc('orders_invoice.id')
        ->get();

        if(count($shop_orders) > 0) {
            return $this->apiResponse(true, 'User Shop Order List', ['data' => $shop_orders]);
        } else {
            return $this->apiResponse(false, 'User Shop Order Not found');
        }
        
    }

    public function getUserShopInvoiceList($shop_id) {
        $headers = apache_request_headers();
        $token = $headers['api-key'];
        $data= $this->getDataFromToken($token, 'customer');
        $user_id = $data->id;

        $shop_invoices = DB::table('orders_invoice')
        ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
        ->where(['orders_invoice.user_id' => $user_id, 'orders_invoice.store_id' => $shop_id, 'is_pos' => 0])
        ->select('orders_invoice.*', 'stores.store_name', 'stores.address')
        ->orderByDesc('orders_invoice.id')
        ->get();

        if(count($shop_invoices) > 0) {
            return $this->apiResponse(true, 'User Shop Invoice List', ['data' => $shop_invoices]);
        } else {
            return $this->apiResponse(false, 'User Shop Invoice Not found');
        }
    }

    public function saveServiceRequest(Request $request) {
        $validator = Validator::make($request->all(), [
            'service_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $headers = apache_request_headers();
        $token = $headers['api-key'];
        $data= $this->getDataFromToken($token, 'customer');
        $user_id = $data->id;
        
        DB::table('service_requests')->insert([
            'user_id' => $user_id,
            'service_id' => $request->service_id,
            'why_need' => isset($request->why_need) ? trim($request->why_need) : null,
            'when_need' => isset($request->when_need) ? trim($request->when_need) : null,
            'contact_no' => isset($request->contact_no) ? trim($request->contact_no) : null,
            'other_details' => isset($request->other_details) ? trim($request->other_details) : null
        ]);

        return $this->apiResponse(true, 'Serice Request Added Successfully', [], 201);
    }



}

