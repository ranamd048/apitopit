<?php

namespace App\Http\Controllers\API;

use App\Http\Traits\HelperTrait;
use App\Model\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    use HelperTrait;

    public function getAllServiceList() {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }

        if(isset($_GET['shop_id'])) {
            $condition = ['services.store_id' => $_GET['shop_id'], 'services.status' => 1];
            if($per_page == 0) {
                $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->get();
                $servicetList = array('data' => $servicetList);
            } else {
                $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->paginate($per_page);
            }
        }
        else if(isset($_GET['category_id'])) {
            //filter with category & location 
            if(isset($_GET['category_id']) && isset($_GET['location_type']) && isset($_GET['location_id'])) {
                $servicetList = $this->filterCategoryWithLocation('services', 'category_id', $_GET['category_id'], $_GET['location_type'], $_GET['location_id'], $per_page);
            } else {
                $condition = ['services.category_id' => $_GET['category_id'], 'services.status' => 1];
                if($per_page == 0) {
                    $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->get();
                    $servicetList = array('data' => $servicetList);
                } else {
                    $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->paginate($per_page);
                }
            }
        } else if(isset($_GET['subcategory_id'])) {
            //filter with category & location 
            if(isset($_GET['subcategory_id']) && isset($_GET['location_type']) && isset($_GET['location_id'])) {
                $servicetList = $this->filterCategoryWithLocation('services', 'subcategory_id', $_GET['subcategory_id'], $_GET['location_type'], $_GET['location_id'], $per_page);
            } else {
                $condition = ['services.subcategory_id' => $_GET['subcategory_id'], 'services.status' => 1];
                if($per_page == 0) {
                    $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->get();
                    $servicetList = array('data' => $servicetList);
                } else {
                    $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->paginate($per_page);
                }
            }
        } else if(isset($_GET['sub_subcategory_id'])) {
            //filter with category & location 
            if(isset($_GET['sub_subcategory_id']) && isset($_GET['location_type']) && isset($_GET['location_id'])) {
                $servicetList = $this->filterCategoryWithLocation('services', 'sub_subcategory_id', $_GET['sub_subcategory_id'], $_GET['location_type'], $_GET['location_id'], $per_page);
            } else {
                $condition = ['services.sub_subcategory_id' => $_GET['sub_subcategory_id'], 'services.status' => 1];
                if($per_page == 0) {
                    $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->get();
                    $servicetList = array('data' => $servicetList);
                } else {
                    $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->paginate($per_page);
                }
            }
        } else if(isset($_GET['location_type']) && isset($_GET['location_id'])) {
            if($_GET['location_type'] == 'division') {
                $condition = array('stores.division' => $_GET['location_id'], 'services.status' => 1);
            } else if($_GET['location_type'] == 'district') {
                $condition = array('stores.district' => $_GET['location_id'], 'services.status' => 1);  
            } else {
                $condition = array('stores.thana' => $_GET['location_id'], 'services.status' => 1);
            }

            if($per_page == 0) {
                $servicetList = DB::table('stores')
                ->join('services', 'stores.id', '=', 'services.store_id')
                ->where($condition)
                ->select('services.*', 'stores.store_name')
                ->get();
                $servicetList = array('data' => $servicetList);
            } else {
                $servicetList = DB::table('stores')
                ->join('services', 'stores.id', '=', 'services.store_id')
                ->where($condition)
                ->select('services.*', 'stores.store_name')
                ->paginate($per_page);
            }
        } else {
            $condition = ['services.status' => 1];
            if($per_page == 0) {
                $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->get();
                $servicetList = array('data' => $servicetList);
            } else {
                $servicetList = DB::table('services')->join('stores', 'services.store_id', '=', 'stores.id')->where($condition)->select('services.*', 'stores.store_name')->paginate($per_page);
            }
        }
        if(count($servicetList) > 0) {
            return $this->apiResponse(true, 'Service List', $servicetList);
        } else {
            return $this->apiResponse(false, 'Service Not Found', [], 404);
        }
    }

    public function getShopServiceList($shop_id) {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }

        if(isset($_GET['category_id'])) {
            $condition = ['store_id' => $shop_id, 'category_id' => $_GET['category_id'], 'status' => 1];
            if($per_page == 0) {
                $servicetList =  DB::table('services')->where($condition)->get();
                $servicetList = array('data' => $servicetList);
            } else {
                $servicetList = DB::table('services')->where($condition)->paginate($per_page);
            }
        } else if(isset($_GET['subcategory_id'])) {
            $condition = ['store_id' => $shop_id, 'subcategory_id' => $_GET['subcategory_id'], 'status' => 1];
            if($per_page == 0) {
                $servicetList =  DB::table('services')->where($condition)->get();
                $servicetList = array('data' => $servicetList);
            } else {
                $servicetList = DB::table('services')->where($condition)->paginate($per_page);
            }
        } else if(isset($_GET['sub_subcategory_id'])) {
            $condition = ['store_id' => $shop_id, 'sub_subcategory_id' => $_GET['sub_subcategory_id'], 'status' => 1];
            if($per_page == 0) {
                $servicetList =  DB::table('services')->where($condition)->get();
                $servicetList = array('data' => $servicetList);
            } else {
                $servicetList = DB::table('services')->where($condition)->paginate($per_page);
            }
        } else {
            $condition = ['store_id' => $shop_id, 'status' => 1];
            if($per_page == 0) {
                $servicetList =  DB::table('services')->where($condition)->get();
                $servicetList = array('data' => $servicetList);
            } else {
                $servicetList = DB::table('services')->where($condition)->paginate($per_page);
            }
        }

        if(count($servicetList) > 0) {
            return $this->apiResponse(true, 'Shop Service List', $servicetList);
        } else {
            return $this->apiResponse(false, 'Shop Service Not Found', [], 404);
        }
    }

    public function saveService(Request $request) {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required|numeric',
            'service_name' => 'required',
            'service_image' => 'required',
            'price' => 'required|numeric',
            'category_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
		
		$destinationPath = public_path('uploads/services/');
        if (!empty($request->service_image)) {
            $service_image_path = 'service_image'.time().'.' . explode('/', explode(':', substr($request->service_image, 0, strpos($request->service_image, ';')))[1])[1];
            Image::make($request->service_image)->save($destinationPath .$service_image_path);
        } else {
            $service_image_path = '';
        }

        $service = Service::create([
            'store_id' => $request->store_id,
            'service_name' => trim($request->service_name),
            'service_slug' => '',
            'service_image' => $service_image_path,
            'price' => trim($request->price),
            'category_id' => $request->category_id,
            'subcategory_id' => $request->subcategory_id,
            'service_description' => trim($request->service_description),
            'status' => ($request->status) ? $request->status : 1,
        ]);
        $service_id = $service->id;

        //add slug
        $slug = $this->makeUniqueSlug('services', $service_id, 'service_slug', $request->service_name);
        Service::where('id', $service_id)->update(['service_slug' => $slug]);

        return $this->apiResponse(true, 'Service Added Successfully', $service, 201);
    }

    public function updateService(Request $request, $id) {
        $service = Service::find($id);
        if($service) {
            $validator = Validator::make($request->all(), [
                'service_name' => 'required',
                'service_image' => 'required',
                'price' => 'required|numeric',
                'category_id' => 'required|numeric'
            ]);
            if ($validator->fails()) {
                return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
            }
			
			$destinationPath = public_path('uploads/services/');
			if (!empty($request->service_image)) {
				if(!empty($service->service_image)) {
                    if(file_exists(public_path('/uploads/services/'.$service->service_image))) {
                        unlink(public_path('/uploads/services/'.$service->service_image));
                    }
                }
				
				$service_image_path = 'service_image'.time().'.' . explode('/', explode(':', substr($request->service_image, 0, strpos($request->service_image, ';')))[1])[1];
				Image::make($request->service_image)->save($destinationPath .$service_image_path);
			} else {
				$service_image_path = $service->service_image;
			}

            $slug = $this->makeUniqueSlug('services', $id, 'service_slug', $request->service_name);

            Service::where('id', $id)->update([
                'service_name' => trim($request->service_name),
                'service_slug' => $slug,
                'service_image' => $service_image_path,
                'price' => trim($request->price),
                'category_id' => $request->category_id,
                'subcategory_id' => $request->subcategory_id,
                'service_description' => trim($request->service_description),
                'status' => ($request->status) ? $request->status : 1,
            ]);
            return $this->apiResponse(true, 'Service Updated Successfully', [], 200);
        } else {
            return $this->apiResponse(false, 'Service ID Not Found', [], 404);
        }

    }

    public function deleteService($id) {
        $service = Service::find($id);
        if($service) {
            if (!empty($service->service_image)) {
                if (file_exists(public_path('/uploads/services/' . $service->service_image))) {
                    unlink(public_path('/uploads/services/' . $service->service_image));
                }
            }

            $service->delete();
            return $this->apiResponse(true, 'Service Deleted Successfully', [], 200);
        } else {
            return $this->apiResponse(false, 'Service ID Not Found', [], 404);
        }
    }

    public function getServiceDetails($id) {
        $service = Service::find($id);
        if($service) {
            $service = array('data' => $service);
            return $this->apiResponse(true, 'Service Details', $service, 200);
        } else {
            return $this->apiResponse(false, 'Service ID Not Found', [], 404);
        }
    }

    public function getAllServiceRequest() {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }

        if(isset($_GET['shop_id'])) {
            if($per_page == 0) {
                $serviceRequestList = DB::table('service_requests')
                ->join('services', 'service_requests.service_id', '=', 'services.id')
                ->join('users', 'service_requests.user_id', '=', 'users.id')
                ->join('stores', 'services.store_id', '=', 'stores.id')
                ->select('service_requests.*', 'services.service_name', 'services.service_slug', 'users.name', 'stores.id as shop_id', 'stores.store_name')
                ->where(['services.store_id' => $_GET['shop_id']])
                ->orderBy('service_requests.id', 'DESC')
                ->get();
                $result = array('data' => $serviceRequestList);
            } else {
                $serviceRequestList = DB::table('service_requests')
                ->join('services', 'service_requests.service_id', '=', 'services.id')
                ->join('users', 'service_requests.user_id', '=', 'users.id')
                ->join('stores', 'services.store_id', '=', 'stores.id')
                ->select('service_requests.*', 'services.service_name', 'services.service_slug', 'users.name','stores.id as shop_id', 'stores.store_name')
                ->where(['services.store_id' => $_GET['shop_id']])
                ->orderBy('service_requests.id', 'DESC')
                ->paginate($per_page);
                $result = $serviceRequestList;
            }
        } else {
            if($per_page == 0) {
                $serviceRequestList = DB::table('service_requests')
                ->join('services', 'service_requests.service_id', '=', 'services.id')
                ->join('users', 'service_requests.user_id', '=', 'users.id')
                ->join('stores', 'services.store_id', '=', 'stores.id')
                ->select('service_requests.*', 'services.service_name', 'services.service_slug', 'users.name', 'stores.id as shop_id', 'stores.store_name')
                ->orderBy('service_requests.id', 'DESC')
                ->get();
                $result = array('data' => $serviceRequestList);
            } else {
                $serviceRequestList = DB::table('service_requests')
                ->join('services', 'service_requests.service_id', '=', 'services.id')
                ->join('users', 'service_requests.user_id', '=', 'users.id')
                ->join('stores', 'services.store_id', '=', 'stores.id')
                ->select('service_requests.*', 'services.service_name', 'services.service_slug', 'users.name', 'stores.id as shop_id', 'stores.store_name')
                ->orderBy('service_requests.id', 'DESC')
                ->paginate($per_page);
                $result = $serviceRequestList;
            }
        }

        
        if(count($serviceRequestList) > 0) {
            return $this->apiResponse(true, 'Service Request List', $result);
        } else {
            return $this->apiResponse(false, 'Service Request Not Found', [], 404);
        }
    }

    public function serviceRequestDeails($id) {
        $result = DB::table('service_requests')
                ->join('services', 'service_requests.service_id', '=', 'services.id')
                ->join('users', 'service_requests.user_id', '=', 'users.id')
                ->join('stores', 'services.store_id', '=', 'stores.id')
                ->select('service_requests.*', 'services.service_name', 'services.service_slug', 'users.name', 'stores.id as shop_id', 'stores.store_name')
                ->where('service_requests.id', $id)
                ->first();
        if($result) {
            $result = array('data' => $result);
            return $this->apiResponse(true, 'Service Request Details', $result);
        } else {
            return $this->apiResponse(false, 'Service Request Id Not Found', [], 404);
        }
    }

    public function updateServiceRequestStatus(Request $request, $id) {
        $serviceRequest = DB::table('service_requests')->where('id', $id)->first();
        if($serviceRequest) {
            DB::table('service_requests')->where('id', $id)->update([
                'status' => $request->status
            ]);
            return $this->apiResponse(true, 'Service Request Status Updated');
        } else {
            return $this->apiResponse(false, 'Service Request Id Not Found', [], 404);
        }
    }




}
