<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    use HelperTrait;

    public function blogList() {
        $per_page = 15;
        $search_keyword = '';
        if(isset($_GET['per_page'])) {
            $per_page = trim($_GET['per_page']);
        }
        if(isset($_GET['search'])) {
            $search_keyword = trim($_GET['search']);
        }

        if(empty($search_keyword)) {
            if($per_page == 0) {
                $result = DB::table('blogs')->orderBy('id', 'DESC')->get();
            } else {
                $result = DB::table('blogs')->orderBy('id', 'DESC')->paginate($per_page);
            }
        } else {
            $result = DB::table('blogs')->where('title', 'like', '%' . $search_keyword . '%')->orderBy('id', 'DESC')->get();
        }
        

        if(count($result) > 0) {
            $result = ($per_page == 0 || !empty($search_keyword)) ? array('data' => $result) : $result;
            return $this->apiResponse(true, 'Blog List', $result);
        } else {
            return $this->apiResponse(false, 'Blog Not Found', [], 404);
        }
    }

    public function blogDetails($id) {
        $result = DB::table('blogs')->where('id', $id)->first();
        if($result) {
            $result = array('data' => $result);
            return $this->apiResponse(true, 'Blog Details', $result);
        } else {
            return $this->apiResponse(false, 'Blog Not Found', [], 404);
        }
    }

    public function blogComments($id) {
        $result = DB::table('blog_comments')
            ->join('users', 'blog_comments.user_id', '=', 'users.id')
            ->select('blog_comments.*', 'users.name as user_name', 'users.photo as user_photo')
            ->where('blog_comments.blog_id', $id)
            ->get();
        
        if(count($result) > 0) {
            $result = array('data' => $result);
            return $this->apiResponse(true, 'Blog Comments', $result);
        } else {
            return $this->apiResponse(false, 'Blog Comment Not Found', [], 404);
        }
    }

    public function saveBlogComment(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'comment' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        DB::table('blog_comments')->insert([
            'blog_id' => $id,
            'user_id' => $request->user_id,
            'comment' => trim($request->comment)
        ]);
        $comment_id = DB::getPdo()->lastInsertId();
        $inserted_comment = DB::table('blog_comments')->where('id', $comment_id)->first();
        return $this->apiResponse(true, 'Comment Added Successfully', $inserted_comment , 201);
    }

}
