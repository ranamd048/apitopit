<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    use HelperTrait;

    public function getAllCategoryList() {
        $per_page = 15;
        $type = 'shop';
        if(isset($_GET['per_page'])) {
            $per_page = trim($_GET['per_page']);
        }
        if(isset($_GET['type'])) {
            $type = trim($_GET['type']);
        }

        if(isset($_GET['parent_id']) && isset($_GET['shop_id'])) {
            if($per_page == 0) {
                $result = Category::with('children')->where(['parent_id' => $_GET['parent_id'], 'category_type' => $type, 'store_id' => $_GET['shop_id']])->get();
                $result = array('data' => $result);
            } else {
                $result = Category::with('children')->where(['parent_id' => $_GET['parent_id'], 'category_type' => $type, 'store_id' => $_GET['shop_id']])->paginate($per_page);
            }
        } elseif (isset($_GET['parent_id'])) {
            if($per_page == 0) {
                $result = Category::with('children')->where(['parent_id' => $_GET['parent_id'], 'category_type' => $type, 'store_id' => '0'])->get();
                $result = array('data' => $result);
            } else {
                $result = Category::with('children')->where(['parent_id' => $_GET['parent_id'], 'category_type' => $type, 'store_id' => '0'])->paginate($per_page);
            }
        } elseif(isset($_GET['shop_id'])) {
            if($per_page == 0) {
                $result = Category::with('children')->where(['category_type' => $type, 'store_id' => $_GET['shop_id']])->get();
                $result = array('data' => $result);
            } else {
                $result = Category::with('children')->where(['category_type' => $type, 'store_id' => $_GET['shop_id']])->paginate($per_page);
            }
        } else {
            if($per_page == 0) {
                $result = Category::with('children')->where(['category_type' => $type, 'store_id' => '0'])->get();
                $result = array('data' => $result);
            } else {
                $result = Category::with('children')->where(['category_type' => $type, 'store_id' => '0'])->paginate($per_page);
            }
        }
        if(isset($result) && count($result) > 0) {
            return $this->apiResponse(true, 'Category List', $result);
        } else {
            return $this->apiResponse(false, 'Category Not Found', [], 404);
        }

    }

    // public function getShopCategories($shop_id) {
    //     $shop_categories = DB::table('products')
    //             ->join('categories', 'products.category_id', '=', 'categories.id')
    //             ->select('categories.*')
    //             ->where(['products.store_id' => $shop_id, 'products.status' => 1])
    //             ->groupBy('products.category_id')
    //             ->get();
    //     $shop_subcategories = DB::table('products')
    //             ->join('categories', 'products.subcategory_id', '=', 'categories.id')
    //             ->select('categories.*')
    //             ->where(['products.store_id' => $shop_id, 'products.status' => 1])
    //             ->groupBy('products.subcategory_id')
    //             ->get();
    //     $shop_sub_subcategories = DB::table('products')
    //             ->join('categories', 'products.sub_subcategory_id', '=', 'categories.id')
    //             ->select('categories.*')
    //             ->where(['products.store_id' => $shop_id, 'products.status' => 1])
    //             ->groupBy('products.sub_subcategory_id')
    //             ->get();
    //     $all_categories = array();
    //     if(count($shop_categories) > 0) {
    //         array_push($all_categories, ...$shop_categories);
    //     }
    //     if(count($shop_subcategories) > 0) {
    //         array_push($all_categories, ...$shop_subcategories);
    //     }
    //     if(count($shop_sub_subcategories) > 0) {
    //         array_push($all_categories, ...$shop_sub_subcategories);
    //     }
    //     $result = array('data' => $all_categories);
    //     if(count($all_categories) > 0) {
    //         return $this->apiResponse(true, 'Shop Category List', $result);
    //     } else {
    //         return $this->apiResponse(false, 'Shop Category Not Found', [], 404);
    //     }
    // }

    // public function getShopSubCategories($shop_id, $cat_id, $sub_type = 'subcategory_id') {
    //     $table = 'products';
    //     if($sub_type == 'subcategory_id') {
    //         $parent_category = 'category_id';
    //     } else {
    //         $parent_category = 'subcategory_id';
    //     }
    //     $shop_categories = DB::table($table)
    //     ->join('categories', $table.'.'.$sub_type, '=', 'categories.id')
    //     ->select('categories.id', 'categories.name')
    //     ->where([$table.'.store_id' => $shop_id, $table.'.'.$parent_category => $cat_id, $table.'.status' => 1])
    //     ->groupBy($table.'.'.$sub_type)
    //     ->get();
    //     $result = array('data' => $shop_categories);
    //     if(count($shop_categories) > 0) {
    //         return $this->apiResponse(true, 'Shop Sub Category List', $result);
    //     } else {
    //         return $this->apiResponse(false, 'Shop Sub Category Not Found', [], 404);
    //     }
    // }

    public function getShopCategories() {
        $table = isset($_GET['table']) && !empty($_GET['table']) ? $_GET['table'] : 'products';
        $shop_id = isset($_GET['shop_id']) && !empty($_GET['shop_id']) ? $_GET['shop_id'] : null;
        if(!$shop_id) {
            return $this->apiResponse(false, 'Shop ID Missing');
        }

        $parent_id = isset($_GET['parent_id']) && !empty($_GET['parent_id']) ? $_GET['parent_id'] : 0;
        $type = isset($_GET['type']) && !empty($_GET['type']) ? $_GET['type'] : 'parent';
        if($type == 'parent') {
            $type = 'category_id';
            $next_type = 'sub';
        } elseif($type == 'sub') {
            $type = 'subcategory_id';
            $next_type = 'sub_sub';
        } elseif($type == 'sub_sub') {
            $type = 'sub_subcategory_id';
            $next_type = "false";
        } else {
            return $this->apiResponse(false, 'Incorrect type value. Correct values (parent, sub, sub_sub)');
        }

        $shop_categories = DB::table($table)
            ->join('categories', $table.'.'.$type, '=', 'categories.id')
            ->select('categories.*')
            ->where([$table.'.store_id' => $shop_id, 'categories.parent_id' => $parent_id, $table.'.status' => 1])->groupBy($table.'.'.$type);

        $shop_categories = $shop_categories->addSelect(DB::raw("'$next_type' as type"))->get();


        $result = array('data' => $shop_categories);
        if(count($shop_categories) > 0) {
            return $this->apiResponse(true, 'Shop Category List', $result);
        } else {
            return $this->apiResponse(false, 'Shop Category Not Found', [], 404);
        }
    }

    public function saveCategory(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg',
            'category_type' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $destinationPath = public_path('uploads/category/');
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = 'category_image'.time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
        } else {
            $image_name = '';
        }

        $category = Category::create([
            'parent_id' => ($request->parent_id) ? $request->parent_id : 0,
            'name' => trim($request->name),
            'slug' => '',
            'image' => $image_name,
            'category_type' => $request->category_type,
            'store_id' => ($request->store_id) ? $request->store_id : 0,
            'status' => ($request->status) ? $request->status : 1,
        ]);
        $category_id = $category->id;
        $slug = $this->makeUniqueSlug('categories', $category_id, 'slug', $request->name);
        Category::where('id', $category_id)->update(['slug' => $slug]);
        return $this->apiResponse(true, 'Category Added Successfully', $category, 201);
    }

    public function updateCategory(Request $request, $id) {
        $category = Category::find($id);
        if($category) {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'image' => 'image|mimes:jpeg,png,jpg',
                'category_type' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
            }

            $destinationPath = public_path('uploads/category/');
            if ($request->hasFile('image')) {
                if(!empty($category->image)) {
                    if(file_exists(public_path('/uploads/category/'.$category->image))) {
                        unlink(public_path('/uploads/category/'.$category->image));
                    }
                }
                $image = $request->file('image');
                $image_name = 'category_image'.time().'.'.$image->getClientOriginalExtension();
                $image->move($destinationPath, $image_name);
            } else {
                $image_name = $category->image;
            }

            $slug = $this->makeUniqueSlug('categories', $id, 'slug', $request->name, 'update');

            $category = Category::where('id', $id)->update([
                'name' => trim($request->name),
                'slug' => $slug,
                'image' => $image_name,
                'category_type' => $request->category_type,
                'status' => ($request->status) ? $request->status : 1,
            ]);

            return $this->apiResponse(true, 'Category Updated Successfully', [], 200);
        } else {
            return $this->apiResponse(false, 'Product ID Not Found', [], 404);
        }

    }

    public function deleteCategory($id) {
        $category = Category::find($id);
        if($category) {
            if(!empty($category->image)) {
                if(file_exists(public_path('/uploads/category/'.$category->image))) {
                    unlink(public_path('/uploads/category/'.$category->image));
                }
            }
            $category->delete();
            return $this->apiResponse(true, 'Category Deleted Successfully', [], 200);
        } else {
            return $this->apiResponse(false, 'Category ID Not Found', [], 404);
        }
    }

    public function getCategoryDetails($id) {
        $category = Category::with('children')->find($id);
        if($category) {
            return $this->apiResponse(true, 'Category Details', $category, 200);
        } else {
            return $this->apiResponse(false, 'Category ID Not Found', [], 404);
        }
    }

    public function shopCategoryList() {
        $result = DB::table('shop_category')->orderBY('sort_order', 'ASC')->get();
        $result = array('data' => $result);
        return $this->apiResponse(true, 'Shop Category List', $result);
    }

}
