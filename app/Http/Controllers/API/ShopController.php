<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Store;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Carbon;

class ShopController extends Controller
{
    use HelperTrait;

    public function getAllShopList() {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }

        if(isset($_GET['category']) && isset($_GET['location_type']) && isset($_GET['location_id'])) {
			if($_GET['location_type'] == 'division') {
                $condition = array('stores.shop_category_id' => $_GET['category'], 'stores.division' => $_GET['location_id'], 'stores.status' => 1);
            } else if($_GET['location_type'] == 'district') {
                $condition = array('stores.shop_category_id' => $_GET['category'], 'stores.district' => $_GET['location_id'], 'stores.status' => 1);
            } else {
                $condition = array('stores.shop_category_id' => $_GET['category'], 'stores.thana' => $_GET['location_id'], 'stores.status' => 1);
            }

            if($per_page == 0) {
                $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where($condition)->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where($condition)->paginate($per_page);
            }
		} else if(isset($_GET['location_type']) && isset($_GET['location_id'])) {
            if($_GET['location_type'] == 'division') {
                $condition = array('stores.division' => $_GET['location_id'], 'stores.status' => 1);
            } else if($_GET['location_type'] == 'district') {
                $condition = array('stores.district' => $_GET['location_id'], 'stores.status' => 1);
            } else {
                $condition = array('stores.thana' => $_GET['location_id'], 'stores.status' => 1);
            }

            if($per_page == 0) {
                $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where($condition)->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where($condition)->paginate($per_page);
            }
        } elseif(isset($_GET['category'])) {
            if($per_page == 0) {
                $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where(['stores.shop_category_id' => $_GET['category'], 'stores.status' => 1])->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where(['stores.shop_category_id' => $_GET['category'], 'stores.status' => 1])->paginate($per_page);
            }
        } else {
            if($per_page == 0) {
                $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where('stores.status', 1)->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->select('stores.*', 'admins.name as propiter_name')->where('stores.status', 1)->paginate($per_page);
            }
        }

        if(count($result) > 0) {
            return $this->apiResponse(true, 'Shop List', $result);
        } else {
            return $this->apiResponse(false, 'Shop Not Found', [], 404);
        }
    }

    public function saveShop(Request $request) {
        $validator = Validator::make($request->all(), [
            'store_name' => 'required',
            'shop_type' => 'required',
            'baner_image' => 'required|image|mimes:jpeg,png,jpg',
            'address' => 'required',
            'phone' => 'required|unique:stores|numeric'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $destinationPath = public_path('uploads/store/');
        if ($request->hasFile('baner_image')) {
            $baner_image = $request->file('baner_image');
            $baner_image_name = 'baner_image' . time() . '.' . $baner_image->getClientOriginalExtension();
            $baner_image->move($destinationPath, $baner_image_name);
        } else {
            $baner_image_name = '';
        }

        $shop = Store::create([
            'store_name' => trim($request->store_name),
            'slug' => '',
            'shop_type' => $request->shop_type,
            'baner_image' => $baner_image_name,
            'address' => trim($request->address),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'buy_package' => $request->buy_package,
            'expiry_date' => $request->expiry_date,
            'fb_link' => trim($request->fb_link),
            'youtube_link' => trim($request->youtube_link),
            'maps_iframe' => trim($request->maps_iframe),
            'status' => ($request->status) ? $request->status : 0
        ]);
        $shop_id = $shop->id;
        $slug = $this->makeUniqueSlug('stores', $shop_id, 'slug', $request->store_name);
        Store::where('id', $shop_id)->update(['slug' => $slug]);
        return $this->apiResponse(true, 'Shop Added Successfully', $shop, 201);
    }

    public function shopRegistration(Request $request) {
        $validator = Validator::make($request->all(), [
            'shop_type' => 'required',
            'store_name' => 'required|string',
            'baner_image' => 'required',
            'shop_logo' => 'required',
            'address' => 'required|string',
            'upazila' => 'required|numeric',
            'district' => 'required|numeric',
            'division' => 'required|numeric',
            'name' => 'required|string',
            'phone' => 'required|unique:stores|numeric',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $slug = str_replace('/', ' ', $request->store_name);
        $slug = preg_replace('/\s+/u', '-', trim($slug));
        $slug = strtolower($slug);
        $check_slug = DB::table('stores')->select('slug')->where('slug', $slug)->first();
        if ($check_slug) {
            $slug = $slug . rand(1000, 10000);
        }
        $destinationPath = public_path('uploads/store/');
        if (!empty($request->baner_image)) {
            $baner_image_name = 'baner_image_'.time().'.' . explode('/', explode(':', substr($request->baner_image, 0, strpos($request->baner_image, ';')))[1])[1];
            Image::make($request->baner_image)->save($destinationPath .$baner_image_name);
        } else {
            $baner_image_name = '';
        }
        if (!empty($request->shop_logo)) {
            $shop_logo_name = 'shop_logo'.time().'.' . explode('/', explode(':', substr($request->shop_logo, 0, strpos($request->shop_logo, ';')))[1])[1];
            Image::make($request->shop_logo)->save($destinationPath .$shop_logo_name);
        } else {
            $shop_logo_name = '';
        }

        $shops = DB::table('stores')->insert([
            'store_name' => trim($request->store_name),
            'slug' => $slug,
            'shop_type' => $request->shop_type,
            'baner_image' => $baner_image_name,
            'logo' => $shop_logo_name,
            'address' => trim($request->address),
            'thana' => $request->upazila,
            'district' => $request->district,
            'division' => $request->division,
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'expiry_date' => Carbon::now()->addMonths(1)
        ]);
        $store_id = DB::getPdo()->lastInsertId();

        DB::table('admins')->insert([
            'name' => trim($request->name),
            'user_name' => trim($request->phone),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'role' => 1,
            'store_id' => $store_id,
            'password' => md5(trim($request->password)),
            'thana' => $request->upazila,
            'district' => $request->district,
            'division' => $request->division,
        ]);

        return $this->apiResponse(true, 'Shop Registration Successfull', $shops, 201);
    }

    public function shopLoginProcess(Request $request) {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $login = DB::table('admins')->where(['user_name' => $request->phone, 'password' => md5($request->password)])->first();
        if ($login) {
            if ($login->store_id) {
                $store = DB::table('stores')->where('id', $login->store_id)->first();
                if ($store->status == 0) {
                    return $this->apiResponse(false, 'Inactive Shop', [], 200);
                } else {
                    $payload = md5($login->id.$login->phone.$login->store_id);
                    $api_unique_token = $payload.time();
                    DB::table('admins')->where('id', $login->id)->update([
                        'api_token' => $api_unique_token
                    ]);
                    $user_data = array('id' => $login->id, 'name' => $login->name, 'phone' => $login->phone, 'shop_id' => $login->store_id, 'shop_name' => $store->store_name, 'shop_baner_image' => $store->baner_image, 'shop_logo' => $store->logo, 'store_address' => $store->address, 'shop_phone' => $store->phone, 'access_token' => $api_unique_token);
                    $response_data = array('data' => $user_data);
                    return $this->apiResponse(true, 'Login Successfull', $response_data);
                }
            }
        } else {
            return $this->apiResponse(false, 'Phone Number or Password Not Match', [], 200);
        }
    }

    public function shopCheck() {
        $headers = apache_request_headers();
        $token = $headers['Authorization'];
        $data= $this->getDataFromToken($token);
        return response()->json($data);
    }

    public function shopLogoutProcess() {
        $headers = apache_request_headers();
        $token = $headers['Authorization'];
        DB::table('admins')->where('api_token', $token)->update([
            'api_token' => null
        ]);
        return $this->apiResponse(true, 'Logout Successfull');
    }

    public function updateShop(Request $request, $id) {
        $shop = Store::find($id);
        if($shop) {
            $validator = Validator::make($request->all(), [
                'store_name' => 'required',
                'shop_type' => 'required',
                'baner_image' => 'image|mimes:jpeg,png,jpg',
                'address' => 'required',
                'phone' => 'required|numeric'
            ]);
            if ($validator->fails()) {
                return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
            }

            $destinationPath = public_path('uploads/store/');
            if ($request->hasFile('baner_image')) {
                if(!empty($shop->baner_image)) {
                    if(file_exists(public_path('/uploads/store/'.$shop->baner_image))) {
                        unlink(public_path('/uploads/store/'.$shop->baner_image));
                    }
                }
                $baner_image = $request->file('baner_image');
                $baner_image_name = 'baner_image' . time() . '.' . $baner_image->getClientOriginalExtension();
                $baner_image->move($destinationPath, $baner_image_name);
            } else {
                $baner_image_name = $shop->baner_image;
            }

            $slug = $this->makeUniqueSlug('stores', $id, 'slug', $request->store_name, 'update');

            $shop = Store::where('id', $id)->update([
                'store_name' => trim($request->store_name),
                'slug' => $slug,
                'shop_type' => $request->shop_type,
                'baner_image' => $baner_image_name,
                'address' => trim($request->address),
                'email' => trim($request->email),
                'phone' => trim($request->phone),
                'buy_package' => $request->buy_package,
                'expiry_date' => $request->expiry_date,
                'fb_link' => trim($request->fb_link),
                'youtube_link' => trim($request->youtube_link),
                'maps_iframe' => trim($request->maps_iframe),
                'status' => ($request->status) ? $request->status : 0
            ]);

            return $this->apiResponse(true, 'Shop Updated Successfully');
        } else {
            return $this->apiResponse(false, 'Shop ID Not Found', [], 404);
        }
    }

    public function deleteShop($id) {
        $shop = Store::find($id);
        if($shop) {
            if(!empty($shop->baner_image)) {
                if(file_exists(public_path('/uploads/store/'.$shop->baner_image))) {
                    unlink(public_path('/uploads/store/'.$shop->baner_image));
                }
            }
            $shop->delete();
            return $this->apiResponse(true, 'Shop Deleted Successfully');
        } else {
            return $this->apiResponse(false, 'Shop ID Not Found', [], 404);
        }
    }

    public function getShopDetails($id) {
        $shop = DB::table('stores')->join('admins', 'stores.id', '=', 'admins.store_id')->where('stores.id', $id)->select('stores.*', 'admins.name as propiter_name')->first();
        if($shop) {
            $shop = array('data' => $shop);
            return $this->apiResponse(true, 'Shop Details', $shop);
        } else {
            return $this->apiResponse(false, 'Shop Not Found', [], 404);
        }
    }

	public function shopOverviewReport($shop_id) {
		$shop = Store::find($shop_id);
		if(!$shop) {
            return $this->apiResponse(false, 'Shop Not Found', [], 404);
        }

        $daily_report = $this->daily_summary_report($shop_id);
        $weekly_report = $this->weekly_summary_report($shop_id);
        $monthly_report = $this->monthly_summary_report($shop_id);

		$report_array = array(
			'daily' => $daily_report,
			'weekly' => $weekly_report,
			'monthly' => $monthly_report
		);

		$result = array('data' => $report_array);
		return $this->apiResponse(true, 'Shop Overview Report', $result);
	}


}
