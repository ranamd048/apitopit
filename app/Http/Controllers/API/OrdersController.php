<?php

namespace App\Http\Controllers\API;

use App\Http\Traits\HelperTrait;
use App\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class OrdersController extends Controller
{
    use HelperTrait;

    public function getAllOrderList() {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if($per_page == 0) {
            $orderList = DB::table('orders')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->select('orders.*', 'users.name as user_name')
            ->get();
            $orderList = array('data' => $orderList);
        } else {
            $orderList = DB::table('orders')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->select('orders.*', 'users.name as user_name')
            ->paginate($per_page);
        }

        if(count($orderList) > 0) {
            return $this->apiResponse(true, 'Order List', $orderList);
        } else {
            return $this->apiResponse(false, 'Order Not Found', [], 404);
        }
    }

    public function getShopOrderList($shop_id) {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if($per_page == 0) {
            $shopOrders = DB::table('orders_invoice')
            ->join('users', 'orders_invoice.user_id', '=', 'users.id')
            ->select('orders_invoice.*', 'users.name as customer_name')
            ->where(['orders_invoice.store_id' => $shop_id])
            ->orderByDesc('orders_invoice.id')
            ->get();
            $shopOrders = array('data' => $shopOrders);
        } else {
            $shopOrders = DB::table('orders_invoice')
            ->join('users', 'orders_invoice.user_id', '=', 'users.id')
            ->select('orders_invoice.*', 'users.name as customer_name')
            ->where(['orders_invoice.store_id' => $shop_id])
            ->orderByDesc('orders_invoice.id')
            ->paginate($per_page);
        }

        if(count($shopOrders) > 0) {
            $success = true;
            $message = 'Shop Order List';
            $code = 200;
        } else {
            $success = false;
            $message = 'Shop Order Not found';
            $code = 404;
        }
        return $this->apiResponse($success, $message, $shopOrders, $code);
    }

    public function getUserOrderList($user_id) {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if($per_page == 0) {
            $userOrders = Order::where('user_id', $user_id)->get();
            $userOrders = array('data' => $userOrders);
        } else {
            $userOrders = Order::where('user_id', $user_id)->paginate($per_page);
        }

        if(count($userOrders) > 0) {
            $success = true;
            $message = 'User Order List';
            $code = 200;
        } else {
            $success = false;
            $message = 'User Order Not found';
            $code = 404;
        }
        return $this->apiResponse($success, $message, $userOrders, $code);
    }

    public function saveOrder(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            //'total_amount' => 'required',
            //'sub_total' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $user_id = $request->user_id;
        $biller_id = ($request->biller_id) ? $request->biller_id : 0;
        // $total_amount = $request->total_amount;
        // $sub_total = $request->sub_total;
        $shipping_address = $request->shipping_address;
        $customer_comments = $request->customer_comments;
        $discount = isset($request->discount) ? $request->discount : 0;
        $paid_amount = isset($request->paid_amount) ? $request->paid_amount : 0;
        $delivery_charge = isset($request->delivery_charge) ? $request->delivery_charge : 0;

        $order = Order::create([
            'user_id' => $user_id,
            'biller_id' => $biller_id,
            'total' => 0,
            'discount' => $discount,
            'delivery_charge' => $delivery_charge,
            'subtotal' => 0,
            'shipping_address' => $shipping_address,
            'order_status' => 2,
            'pos' => 1,
            'paid_amount' => $paid_amount
        ]);

        $single_shop_products = array();
        foreach ( $request->items as $item ) {
            $single_shop_products[$item['store_id']][] = $item;
        }

        //single shop invoice
        $total_invoice_amount = 0;
        foreach($single_shop_products as $store_id => $items) {
            //registerd shop customer
            $this->shopCustomerRegistration($user_id, $store_id);
            //get shop delivery charge
            //$delivery_charge = $this->getShopShippingCost($store_id);
            DB::table('orders_invoice')->insert([
                'order_id' => $order->id,
                'store_id' => $store_id,
                'user_id' => $user_id,
                'delivery_charge' => $delivery_charge,
                'discount' => $discount,
                'biller_id' => $biller_id,
                'total_amount' => 0,
                'paid_amount' => $paid_amount,
                'payment_status' => 0,
                'invoice_status' => 2,
                'customer_comments' => $customer_comments
            ]);
            $invoice_id = DB::getPdo()->lastInsertId();
            //save order items
            foreach($items as $row) {
                //manage stock
                $this->productStockManage($row['id'], $row['quantity']);

                $sub_total = ($row['price'] * $row['quantity']);
                DB::table('order_items')->insert([
                    'order_id' => $order->id,
                    'invoice_id' => $invoice_id,
                    'product_id' => $row['id'],
                    'product_name' => $row['name'],
                    'price' => $row['price'],
                    'discount_amount' => 0,
                    'sub_total' => $sub_total,
                    'quantity' => $row['quantity'],
                    'product_slug' => $row['slug'],
                ]);
                $total_invoice_amount += $sub_total;
            }
            //update invoice total amount
            $total_invoice_amount_discount = ($total_invoice_amount - $discount);
            $due_amount = ($total_invoice_amount_discount - $paid_amount);
            //check user current_balance
            $user_balance = DB::table('customer_shop_balance')->select('current_balance')->where(['user_id' => $user_id, 'shop_id' => $store_id])->first();
            if($user_balance) {
                if($user_balance->current_balance > 0) {
                    if($user_balance->current_balance > $due_amount) {
                        $pay_amount = $due_amount;
                        $due_amount = 0;
                        $available_balance = ($user_balance->current_balance - $due_amount);
                    } else {
                        $pay_amount = $user_balance->current_balance;
                        $due_amount = ($due_amount - $user_balance->current_balance);
                        $available_balance = 0;
                    }
                    //insert new payment
                    DB::table('payments')->insert([
                        'invoice_id' => $invoice_id,
                        'user_id' => $user_id,
                        'paid_amount' => $pay_amount,
                        'payment_method' => 'pre_balance',
                        'payment_note' => 'Paid from customer pre balance',
                        'status' => 1
                    ]);
                    //update customer balance
                    DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $store_id])->update(['current_balance' => $available_balance]);
                }
            }
            $payment_status = ($due_amount == 0) ? 1 : 0;
            DB::table('orders_invoice')->where('id', $invoice_id)->update([
                'total_amount' => $total_invoice_amount,
                'due_amount' => $due_amount,
                'payment_status' => $payment_status
            ]);
        }

        DB::table('orders')->where('id', $order->id)->update([
            'total' => $total_invoice_amount,
            'subtotal' => ($total_invoice_amount + $delivery_charge)
        ]);

        $order = DB::table('orders_invoice')->where('id', $invoice_id)->first();

        return $this->apiResponse(true, 'Order Added Successfully', $order, 201);
    }

    public function updateOrder(Request $request, $id) {
        $order = Order::find($id);
        if($order) {
            $validator = Validator::make($request->all(), [
                'order_status' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
            }

            Order::where('id', $id)->update([
                'order_status' => $request->order_status,
            ]);
            return $this->apiResponse(true, 'Order Updated Successfully', [], 200);
        } else {
            return $this->apiResponse(false, 'Order ID Not Found', [], 404);
        }
    }

    public function deleteOrder($id) {
        $order = Order::find($id);
        if($order) {
            $order->delete();
            return $this->apiResponse(true, 'Order Deleted Successfully', [], 200);
        } else {
            return $this->apiResponse(false, 'Order ID Not Found', [], 404);
        }
    }

    public function getOrderDetails($id) {
        $order = Order::find($id);
        if($order) {
            $order = array('data' => $order);
            return $this->apiResponse(true, 'Order Details', $order, 200);
        } else {
            return $this->apiResponse(false, 'Order ID Not Found', [], 404);
        }
    }

    public function orderInvoice($id) {
        $order_invoice = DB::table('orders_invoice')
        ->join('users', 'orders_invoice.user_id', '=', 'users.id')
        ->select('orders_invoice.*', 'users.name as customer_name')
        ->where(['orders_invoice.order_id' => $id])
        ->first();//DB::table('orders_invoice')->where('order_id', $id)->first();
        if($order_invoice) {
            $order_items = DB::table('order_items')->where('invoice_id', $order_invoice->id)->get();
            $result = array(
                'data' => array(
                    'order_invoice' => $order_invoice,
                    'order_items' => $order_items
                )
            );
            return $this->apiResponse(true, 'Order Invoice Details', $result, 200);
        } else {
            return $this->apiResponse(false, 'Order Invoice Not Found', [], 404);
        }
    }

    public function shopCustomerOrders($shop_id, $user_id) {
        $result = array();
        $order_invoices = DB::table('orders_invoice')
        ->join('users', 'orders_invoice.user_id', '=', 'users.id')
        ->select('orders_invoice.*', 'users.name')
        ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $user_id])
        ->get();
        if(count($order_invoices) > 0) {
            foreach($order_invoices as $item) {
                $result[$item->id]['invoice'] = $item;
                $result[$item->id]['invoice_items'] = DB::table('order_items')->where('invoice_id', $item->id)->get();
            }
            $result = array('data' => $result);
            return $this->apiResponse(true, 'Order List', $result, 200);
        } else {
            return $this->apiResponse(false, 'Orders Not Found', [], 404);
        }
    }




}

