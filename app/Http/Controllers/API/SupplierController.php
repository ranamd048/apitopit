<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    use HelperTrait;

    public function index() {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if(isset($_GET['shop_id'])) {
            if($per_page == 0) {
                $result = DB::table('users')->select('id' ,'store_id', 'name', 'email', 'phone', 'division', 'district', 'thana', 'address')->where(['store_id' => $_GET['shop_id'], 'user_type' => 'supplier'])->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('users')->select('id' ,'store_id', 'name', 'email', 'phone', 'division', 'district', 'thana', 'address')->where(['store_id' => $_GET['shop_id'], 'user_type' => 'supplier'])->paginate($per_page);
            }
        } else {
            if($per_page == 0) {
                $result = DB::table('users')->select('id' ,'store_id', 'name', 'email', 'phone', 'division', 'district', 'thana', 'address')->where(['user_type' => 'supplier'])->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('users')->select('id' ,'store_id', 'name', 'email', 'phone', 'division', 'district', 'thana', 'address')->where(['user_type' => 'supplier'])->paginate($per_page);
            }
        }
        if(count($result) > 0) {
            return $this->apiResponse(true, 'Supplier List', $result);
        } else {
            return $this->apiResponse(false, 'Supplier Not Found', [], 404);
        }
    }

    public function details($id) {
        $result = DB::table('users')->select('id' ,'store_id', 'name', 'email', 'phone', 'division', 'district', 'thana', 'address')->where(['id' => $id, 'user_type' => 'supplier'])->first();
        if($result) {
            $result = array('data' => $result);
            return $this->apiResponse(true, 'Supplier Details', $result, 200);
        } else {
            return $this->apiResponse(false, 'Supplier ID Not Found', [], 404);
        }
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'name' => 'required',
			'phone' => 'required|unique:users|numeric'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        DB::table('users')->insert([
			'store_id' => $request->store_id,
			'user_type' => 'supplier',
	    	'name' => trim($request->name),
	    	'email' => trim($request->email),
	    	'phone' => trim($request->phone),
	    	'division' => $request->division,
	    	'district' => $request->district,
	    	'thana' => $request->upazila,
	    	'address' => trim($request->address),
        ]);
        
        return $this->apiResponse(true, 'Supplier Added Successfully', [], 201);
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        DB::table('users')->where(['id' => $id, 'user_type' => 'supplier'])->update([
	    	'name' => trim($request->name),
	    	'email' => trim($request->email),
	    	'phone' => trim($request->phone),
	    	'division' => $request->division,
	    	'district' => $request->district,
	    	'thana' => $request->upazila,
	    	'address' => trim($request->address),
        ]);
        return $this->apiResponse(true, 'Supplier Updated Successfully', [], 200);
    }

    public function delete($id) {
        DB::table('users')->where(['id' => $id, 'user_type' => 'supplier'])->delete();
        return $this->apiResponse(true, 'Supplier Deleted Successfully', [], 200);
    }


}