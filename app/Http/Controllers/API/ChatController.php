<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\HelperTrait;

class ChatController extends Controller
{
    use HelperTrait;

    public function search_users() {
        $user_id = isset($_GET['user_id']) && !empty($_GET['user_id']) ? $_GET['user_id'] : null;
        $type = isset($_GET['type']) && !empty($_GET['type']) ? $_GET['type'] : null;
        $name = isset($_GET['name']) && !empty($_GET['name']) ? $_GET['name'] : null;
        $phone = isset($_GET['phone']) && !empty($_GET['phone']) ? $_GET['phone'] : null;
        $address = isset($_GET['address']) && !empty($_GET['address']) ? $_GET['address'] : null;

        if(!$type) {
            return $this->apiResponse(false, 'Please enter type as a query param');
        }

        if(!$user_id) {
            return $this->apiResponse(false, 'Please enter user id');
        }

        $site_url = url('/');

        $query_result = [];
        if($type == 'shop_admin') {
            $query = DB::table('stores as s')->where('status', 1)->select('s.id', 's.store_name as name', DB::Raw('CONCAT("'.$site_url.'/public/uploads/store/", s.logo) as photo'), 's.phone', 's.address', 'd.name as division_name', 'dis.name as districts_name', 'z.name as thana_name', 'c.id as chat_id');
            $query->join('divisions as d', 'd.id', '=', 's.division', 'left');
            $query->join('districts as dis', 'dis.id', '=', 's.district', 'left');
            $query->join('upazilas as z', 'z.id', '=', 's.thana', 'left');
            $query->leftJoin('chats as c', function ($join) use ($user_id, $type) {
                $join->on('c.reciver_id', '=', 's.id')
                    ->where('c.sender_id', '=', $user_id);
                $join->orOn('c.sender_id', '=', 's.id')
                    ->where('c.reciver_id', '=', $user_id);
            });
            if (!empty($name)) {
                $query->where('s.store_name', 'like', "%{$name}%");
            }
            if (!empty($phone)) {
                $query->where('s.phone', 'like', "%{$phone}%");
            }
            if (!empty($address)) {
                $query->orWhere('s.address', 'like', "%{$address}%");
                $query->orWhere('d.name', 'like', "%{$address}%");
                $query->orWhere('dis.name', 'like', "%{$address}%");
                $query->orWhere('z.name', 'like', "%{$address}%");
            }
            $query_result = $query->get();
        }
        if($type == 'super_admin') {
            $query = DB::table('admins')->where('role', 0)->select('id', 'name', 'phone', DB::Raw('CONCAT("'.$site_url.'/public/uploads/", profile_photo) as photo'));

            if (!empty($name)) {
                $query->where('name', 'like', "%{$name}%");
            }
            if (!empty($phone)) {
                $query->where('phone', 'like', "%{$phone}%");
            }
            $query_result = $query->get();
        }
        $query_result['type'] = $type;

        $res = [];
        foreach ($query_result as $item) {
            $arr = (array) $item;
            $arr['type'] = $type;
            $res[] = $arr;
        }

        $result = array('data' => $res);
        if(count($res) > 0) {
            return $this->apiResponse(true, 'User List', $result);
        } else {
            return $this->apiResponse(false, 'User Not Found', [], 404);
        }
    }

    public function previous_message_users() {
        $name = isset($_GET['name']) && !empty($_GET['name']) ? $_GET['name'] : null;
        $phone = isset($_GET['phone']) && !empty($_GET['phone']) ? $_GET['phone'] : null;
        $user_id = isset($_GET['user_id']) && !empty($_GET['user_id']) ? $_GET['user_id'] : null;

        $filter_type = isset($_GET['filter_type']) && !empty($_GET['filter_type']) ? $_GET['filter_type'] : 'all';
        $customer_id = isset($_GET['customer_id']) && !empty($_GET['customer_id']) ? $_GET['customer_id'] : null;
        $sender_type = 'customer';

        if(!$customer_id) {
            return $this->apiResponse(false, 'Please enter customer_id as a query param');
        }

        $messaged_users = DB::table('chats')->where(function($query) use ($customer_id, $sender_type, $name, $user_id) {
            $query->where(['sender_id' => $customer_id, 'sender_type' => $sender_type]);
            if (!empty($name)) {
                $query->where('reciver_name', 'like', "%{$name}%");
            }
            if (!empty($user_id)) {
                $query->where('reciver_id', $user_id);
            }
        })->orWhere(function($query) use ($customer_id, $sender_type, $name, $user_id) {
            $query->where(['reciver_id' => $customer_id, 'reciver_type' => $sender_type]);
            if (!empty($name)) {
                $query->where('sender_name', 'like', "%{$name}%");
            }
            if (!empty($user_id)) {
                $query->where('sender_id', $user_id);
            }
        })->orderByDesc('updated_at')->get();

        $users_array = [];
        if(count($messaged_users) > 0) {
            foreach($messaged_users as $user) {
                //get last meessage
                $last_query = DB::table('chat_messages as c')->select('c.chat_id', 'c.sender', 'c.message', 'c.is_read', 'c.created_at')->where('c.chat_id', $user->id)->orderByDesc('c.id');

                if($user->sender_id == $customer_id) {
                    if ($user->reciver_type == 'shop_admin') {
                        $last_query->join('stores as s', 's.id', '=', 'c.sender', 'left');
                        $last_query->join('divisions as d', 'd.id', '=', 's.division', 'left');
                        $last_query->join('districts as dis', 'dis.id', '=', 's.district', 'left');
                        $last_query->join('upazilas as z', 'z.id', '=', 's.thana', 'left');
                        $last_query->addSelect(['s.phone', 's.email', 's.address', 'd.name as division_name', 'dis.name as districts_name', 'z.name as thana_name']);

                        if (!empty($phone)) {
                            //$last_query->where('s.phone', "{$phone}");
                        }
                    } else {
                        $last_query->join('admins as u', 'u.id', '=', 'c.sender', 'left');
                        $last_query->addSelect(['u.phone', 'u.email', 'u.division as address', 'u.division as division_name', 'u.district as districts_name', 'u.thana as thana_name']);
                        if (!empty($phone)) {
                            //$last_query->where('u.phone', "{$phone}");
                        }
                    }
                } else {
                    if ($user->sender_type == 'shop_admin') {
                        $last_query->join('stores as s', 's.id', '=', 'c.sender', 'left');
                        $last_query->join('divisions as d', 'd.id', '=', 's.division', 'left');
                        $last_query->join('districts as dis', 'dis.id', '=', 's.district', 'left');
                        $last_query->join('upazilas as z', 'z.id', '=', 's.thana', 'left');
                        $last_query->addSelect(['s.phone', 's.email', 's.address', 'd.name as division_name', 'dis.name as districts_name', 'z.name as thana_name']);
                        if (!empty($phone)) {
                            //$last_query->where('s.phone', "{$phone}");
                        }
                    } else {
                        $last_query->join('admins as u', 'u.id', '=', 'c.sender', 'left');
                        $last_query->addSelect(['u.phone', 'u.email', 'u.division as address', 'u.division as division_name', 'u.district as districts_name', 'u.thana as thana_name']);
                        if (!empty($phone)) {
                            //$last_query->where('u.phone', "{$phone}");
                        }
                    }
                }

                $last_message = $last_query->first();
                //dd($last_message);

                if($filter_type == 'shop_admin' && ($user->reciver_type == 'shop_admin' || $user->sender_type == 'shop_admin')) {
                    if($user->sender_id == $customer_id) {
                        $new_array = ['id' => $user->id, 'user_id' => $user->reciver_id, 'user_name' => $user->reciver_name, 'user_photo' => $user->reciver_photo, 'user_type' => $user->reciver_type, 'phone' => $last_message->phone, 'email' => $last_message->email, 'address' => $last_message->address, 'division' => $last_message->division_name, 'district' => $last_message->districts_name, 'thana' => $last_message->thana_name, 'message' => $last_message->message, 'is_read' => $last_message->is_read, 'message_time' => $last_message->created_at];
                    }
                    if($user->reciver_id == $customer_id) {
                        $new_array = ['id' => $user->id, 'user_id' => $user->sender_id, 'user_name' => $user->sender_name, 'user_photo' => $user->sender_photo, 'user_type' => $user->sender_type, 'phone' => $last_message->phone, 'email' => $last_message->email, 'address' => $last_message->address, 'division' => $last_message->division_name, 'district' => $last_message->districts_name, 'thana' => $last_message->thana_name, 'message' => $last_message->message, 'is_read' => $last_message->is_read, 'message_time' => $last_message->created_at];
                    }
                    array_push($users_array, $new_array);
                } elseif( $filter_type == 'super_admin' && ($user->reciver_type == 'super_admin' || $user->sender_type == 'super_admin')) {
                    if($user->sender_id == $customer_id) {
                        $new_array = ['id' => $user->id, 'user_id' => $user->reciver_id, 'user_name' => $user->reciver_name, 'user_photo' => $user->reciver_photo, 'user_type' => $user->reciver_type, 'phone' => $last_message->phone, 'email' => $last_message->email, 'address' => $last_message->address, 'division' => $last_message->division_name, 'district' => $last_message->districts_name, 'thana' => $last_message->thana_name, 'message' => $last_message->message, 'is_read' => $last_message->is_read, 'message_time' => $last_message->created_at];
                        array_push($users_array, $new_array);
                    }
                    if($user->reciver_id == $customer_id) {
                        $new_array = ['id' => $user->id, 'user_id' => $user->sender_id, 'user_name' => $user->sender_name, 'user_photo' => $user->sender_photo, 'user_type' => $user->sender_type, 'phone' => $last_message->phone, 'email' => $last_message->email, 'address' => $last_message->address, 'division' => $last_message->division_name, 'district' => $last_message->districts_name, 'thana' => $last_message->thana_name, 'message' => $last_message->message, 'is_read' => $last_message->is_read, 'message_time' => $last_message->created_at];
                        array_push($users_array, $new_array);
                    }
                } elseif( $filter_type == 'all') {
                    if($user->sender_id == $customer_id) {
                        $new_array = ['id' => $user->id, 'user_id' => $user->reciver_id, 'user_name' => $user->reciver_name, 'user_photo' => $user->reciver_photo, 'user_type' => $user->reciver_type, 'phone' => $last_message->phone, 'email' => $last_message->email, 'address' => $last_message->address, 'division' => $last_message->division_name, 'district' => $last_message->districts_name, 'thana' => $last_message->thana_name, 'message' => $last_message->message, 'is_read' => $last_message->is_read, 'message_time' => $last_message->created_at];
                    }
                    if($user->reciver_id == $customer_id) {
                        $new_array = ['id' => $user->id, 'user_id' => $user->sender_id, 'user_name' => $user->sender_name, 'user_photo' => $user->sender_photo, 'user_type' => $user->sender_type, 'phone' => $last_message->phone, 'email' => $last_message->email, 'address' => $last_message->address, 'division' => $last_message->division_name, 'district' => $last_message->districts_name, 'thana' => $last_message->thana_name, 'message' => $last_message->message, 'is_read' => $last_message->is_read, 'message_time' => $last_message->created_at];
                    }
                    array_push($users_array, $new_array);
                }
            }
        }
        $result = array('data' => $users_array);
        if(count($users_array) > 0) {
            return $this->apiResponse(true, 'Messaged User List', $result);
        } else {
            return $this->apiResponse(false, 'Messaged User Not Found', [], 404);
        }
    }

    public function send_new_message(Request $request) {
        $sender_id=isset($request->sender_id) && !empty($request->sender_id) ? $request->sender_id : null;
        $sender_type = 'customer';
        $reciver_id = isset($request->reciver_id) && !empty($request->reciver_id) ? $request->reciver_id : null;
        $reciver_type = isset($request->reciver_type) && !empty($request->reciver_type) ? $request->reciver_type : null;
        $message_body = isset($request->message_body) && !empty($request->message_body) ? $request->message_body : null;

        if(!$sender_id || !$reciver_id || !$reciver_type || !$message_body) {
            return $this->apiResponse(false, 'sender_id, reciver_id, reciver_type, message_body required');
        }

        $is_chat = DB::table('chats')->where(function($query) use ($sender_id, $reciver_id, $sender_type, $reciver_type) {
            $query->where(['sender_id' => $sender_id, 'reciver_id' => $reciver_id, 'sender_type' => $sender_type, 'reciver_type' => $reciver_type]);
        })->orWhere(function($query) use ($sender_id, $reciver_id, $sender_type, $reciver_type) {
            $query->where(['sender_id' => $reciver_id, 'reciver_id' => $sender_id, 'sender_type' => $reciver_type, 'reciver_type' => $sender_type]);
        })->first();

        if($is_chat) {
            $chat_id = $is_chat->id;
            DB::table('chats')->where('id', $chat_id)->update([
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            DB::table('chat_messages')->insert([
                'chat_id' => $chat_id,
                'sender' => $sender_id,
                'message' => trim($message_body),
            ]);
        } else {
            $sender_info = $this->chat_user_info($sender_id, $sender_type);
            $reciver_info = $this->chat_user_info($reciver_id, $reciver_type);
            DB::table('chats')->insert([
                'reciver_id' => $reciver_id,
                'sender_id' => $sender_id,
                'sender_name' => $sender_info['name'],
                'sender_photo' => $sender_info['photo_path'],
                'reciver_name' => $reciver_info['name'],
                'reciver_photo' => $reciver_info['photo_path'],
                'reciver_type' => $reciver_type,
                'sender_type' => $sender_type
            ]);
            $chat_id = DB::getPdo()->lastInsertId();
            DB::table('chat_messages')->insert([
                'chat_id' => $chat_id,
                'sender' => $sender_id,
                'message' => trim($message_body)
            ]);
        }

        $data['user_info'] = $this->chat_user_info($reciver_id, $reciver_type);
        $data['messages'] = DB::table('chat_messages')->where('chat_id', $chat_id)->get();
        $result = array('data' => $data);
        return $this->apiResponse(true, 'User Messages', $result);
    }

    public function messages() {
        $chat_id = isset($_GET['chat_id']) && !empty($_GET['chat_id']) ? $_GET['chat_id'] : null;
        $user_id = isset($_GET['user_id']) && !empty($_GET['user_id']) ? $_GET['user_id'] : null;
        $user_type = isset($_GET['user_type']) && !empty($_GET['user_type']) ? $_GET['user_type'] : null;

        if(!$chat_id || !$user_id || !$user_type) {
            return $this->apiResponse(false, 'Please enter chat_id, user_id, user_type as a query param');
        }

        $data['user_info'] = $this->chat_user_info($user_id, $user_type);
        $data['messages'] = DB::table('chat_messages')->where('chat_id', $chat_id)->get();

        $result = array('data' => $data);
        return $this->apiResponse(true, 'User Messages', $result);
    }

    private function chat_user_info($user_id, $type) {
        $default_photo = asset('public/uploads/admin.jpg');
        if($type == 'super_admin') {
            $result = DB::table('admins')->select('name', 'profile_photo')->where('id', $user_id)->first();
            $photo_path = $result->profile_photo ? asset('public/uploads/'.$result->profile_photo) : $default_photo;
        }
        if($type == 'shop_admin') {
            $result = DB::table('stores')->select('store_name as name', 'logo')->where('id', $user_id)->first();
            $photo_path = $result->logo ? asset('public/uploads/store/'.$result->logo) : $default_photo;
        }
        if($type == 'customer') {
            $result = DB::table('users')->select('name', 'photo')->where('id', $user_id)->first();
            $photo_path = $result->photo ? asset('public/uploads/customer/'.$result->photo) : $default_photo;
        }
        return ['name' => $result->name, 'photo_path' => $photo_path];
    }



}
