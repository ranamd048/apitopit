<?php

namespace App\Http\Controllers\API;

use App\Http\Traits\HelperTrait;
use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class ProductController extends Controller
{
    use HelperTrait;

    public function getAllProductList() {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }

        if(isset($_GET['shop_id'])) {
            $condition = ['products.store_id' => $_GET['shop_id'], 'products.status' => 1];
            if($per_page == 0) {
                $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->get();
                $productList = array('data' => $productList);
            } else {
                $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->paginate($per_page);
            }
        }
        else if(isset($_GET['category_id'])) {
            //filter with category & location 
            if(isset($_GET['category_id']) && isset($_GET['location_type']) && isset($_GET['location_id'])) {
                $productList = $this->filterCategoryWithLocation('products', 'category_id', $_GET['category_id'], $_GET['location_type'], $_GET['location_id'], $per_page);
            } else {
                $condition = ['products.category_id' => $_GET['category_id'], 'products.status' => 1];
                if($per_page == 0) {
                    $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->get();
                    $productList = array('data' => $productList);
                } else {
                    $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->paginate($per_page);
                }
            }
        } else if(isset($_GET['subcategory_id'])) {
            //filter with category & location 
            if(isset($_GET['subcategory_id']) && isset($_GET['location_type']) && isset($_GET['location_id'])) {
                $productList = $this->filterCategoryWithLocation('products', 'subcategory_id', $_GET['subcategory_id'], $_GET['location_type'], $_GET['location_id'], $per_page);
            } else {
                $condition = ['products.subcategory_id' => $_GET['subcategory_id'], 'products.status' => 1];
                if($per_page == 0) {
                    $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->get();
                    $productList = array('data' => $productList);
                } else {
                    $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->paginate($per_page);
                }
            }
        } else if(isset($_GET['sub_subcategory_id'])) {
            //filter with category & location 
            if(isset($_GET['sub_subcategory_id']) && isset($_GET['location_type']) && isset($_GET['location_id'])) {
                $productList = $this->filterCategoryWithLocation('products', 'sub_subcategory_id', $_GET['sub_subcategory_id'], $_GET['location_type'], $_GET['location_id'], $per_page);
            } else {
                $condition = ['products.sub_subcategory_id' => $_GET['sub_subcategory_id'], 'products.status' => 1];
                if($per_page == 0) {
                    $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->get();
                    $productList = array('data' => $productList);
                } else {
                    $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->paginate($per_page);
                }
            }
        }
        else if(isset($_GET['location_type']) && isset($_GET['location_id'])) {
            if($_GET['location_type'] == 'division') {
                $condition = array('stores.division' => $_GET['location_id'], 'products.status' => 1);
            } else if($_GET['location_type'] == 'district') {
                $condition = array('stores.district' => $_GET['location_id'], 'products.status' => 1);  
            } else {
                $condition = array('stores.thana' => $_GET['location_id'], 'products.status' => 1);
            }

            if($per_page == 0) {
                $productList = DB::table('stores')
                ->join('products', 'stores.id', '=', 'products.store_id')
                ->where($condition)
                ->select('products.*', 'stores.store_name')
                ->get();
                $productList = array('data' => $productList);
            } else {
                $productList = DB::table('stores')
                ->join('products', 'stores.id', '=', 'products.store_id')
                ->where($condition)
                ->select('products.*', 'stores.store_name')
                ->paginate($per_page);
            }
        } else {
            $condition = ['products.status' => 1];
            if($per_page == 0) {
                $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->get();
                $productList = array('data' => $productList);
            } else {
                $productList = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')->where($condition)->select('products.*', 'stores.store_name')->paginate($per_page);
            }
        }
        if(count($productList) > 0) {
            foreach($productList as $row) {
                $product_unit = DB::table('product_units')->select('unit_name')->where('id', $row->unit_id)->first();
                $row->unit_name = $product_unit ? $product_unit->unit_name : '';
            }
            return $this->apiResponse(true, 'Product List', $productList);
        } else {
            return $this->apiResponse(false, 'Product Not Found', [], 404);
        }
    }

    public function getShopProductList($shop_id) {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }

        if(isset($_GET['product_type'])) {
            if($_GET['product_type'] == 'featured') {
                $product_type_condition = ['store_id' => $shop_id, 'featured' => 1, 'status' => 1];
            } elseif($_GET['product_type'] == 'offer') {
                if($per_page == 0) {
                    $productList =  DB::table('products')->where('promotion_start_date', '<=', Carbon::now())
                    ->where('promotion_end_date', '>=', Carbon::now())
                    ->where(['store_id' => $shop_id, 'status' => 1])->get();
                    $productList = array('data' => $productList);
                } else {
                    $productList = DB::table('products')->where('promotion_start_date', '<=', Carbon::now())
                    ->where('promotion_end_date', '>=', Carbon::now())
                    ->where(['store_id' => $shop_id, 'status' => 1])->paginate($per_page);
                }
            } elseif($_GET['product_type'] == 'is_home_delivery') {
                $product_type_condition = ['store_id' => $shop_id, 'is_home_delivery' => 1, 'status' => 1];
            } elseif($_GET['product_type'] == 'is_used') {
                $product_type_condition = ['store_id' => $shop_id, 'is_used' => 1, 'status' => 1];
            } else {
                $product_type_condition = ['store_id' => $shop_id, 'status' => 1];
            }
            
            if($_GET['product_type'] !== 'offer') {
                if($per_page == 0) {
                    $productList =  DB::table('products')->where($product_type_condition)->get();
                    $productList = array('data' => $productList);
                } else {
                    $productList = DB::table('products')->where($product_type_condition)->paginate($per_page);
                }
            }
        } else if(isset($_GET['category_id'])) {
            $condition = ['store_id' => $shop_id, 'category_id' => $_GET['category_id'], 'status' => 1];
            if($per_page == 0) {
                $productList =  DB::table('products')->where($condition)->get();
                $productList = array('data' => $productList);
            } else {
                $productList = DB::table('products')->where($condition)->paginate($per_page);
            }
        } else if(isset($_GET['subcategory_id'])) {
            $condition = ['store_id' => $shop_id, 'subcategory_id' => $_GET['subcategory_id'], 'status' => 1];
            if($per_page == 0) {
                $productList =  DB::table('products')->where($condition)->get();
                $productList = array('data' => $productList);
            } else {
                $productList = DB::table('products')->where($condition)->paginate($per_page);
            }
        } else if(isset($_GET['sub_subcategory_id'])) {
            $condition = ['store_id' => $shop_id, 'sub_subcategory_id' => $_GET['sub_subcategory_id'], 'status' => 1];
            if($per_page == 0) {
                $productList =  DB::table('products')->where($condition)->get();
                $productList = array('data' => $productList);
            } else {
                $productList = DB::table('products')->where($condition)->paginate($per_page);
            }
        } else {
            $condition = ['store_id' => $shop_id, 'status' => 1];
            if($per_page == 0) {
                $productList =  DB::table('products')->where($condition)->get();
                $productList = array('data' => $productList);
            } else {
                $productList = DB::table('products')->where($condition)->paginate($per_page);
            }
        }

        if(count($productList) > 0) {
            foreach($productList as $row) {
                $product_unit = DB::table('product_units')->select('unit_name')->where('id', $row->unit_id)->first();
                $row->unit_name = $product_unit ? $product_unit->unit_name : '';
            }
            return $this->apiResponse(true, 'Shop Product List', $productList);
        } else {
            return $this->apiResponse(false, 'Shop Product Not Found', [], 404);
        }
    }

    public function saveProduct(Request $request) {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required|numeric',
            'unit_id' => 'required',
            'product_name' => 'required',
            'product_image' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'category_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }

        $destinationPath = public_path('uploads/products/');
        $thumbDestinationPath = public_path('uploads/products/thumbnail/');
        if (!empty($request->product_image)) {
            $product_image_path = 'product_image'.time().'.' . explode('/', explode(':', substr($request->product_image, 0, strpos($request->product_image, ';')))[1])[1];
            Image::make($request->product_image)->save($destinationPath .$product_image_path);
            Image::make($request->product_image)->save($thumbDestinationPath .$product_image_path);

            // $thumbnail_image = Image::make($product_image->getRealPath());
            // $thumbnail_image->resize(220, 220, function ($constraint) {
            //     $constraint->aspectRatio();
            // })->save($thumbDestinationPath . '/' . $product_image_path);

            // $product_image->move($destinationPath, $product_image_path);
        } else {
            $product_image_path = '';
        }

        $product = Product::create([
            'store_id' => $request->store_id,
            'unit_id' => $request->unit_id,
            'brand_id' => isset($request->brand_id) ? $request->brand_id : '',
            'product_name' => trim($request->product_name),
            'product_slug' => '',
            'product_image' => $product_image_path,
            'price' => trim($request->price),
            'product_video' => isset($request->product_video) ? $request->product_video: '',
            'quantity' => ($request->quantity) ? $request->quantity : 1,
            'category_id' => $request->category_id,
            'subcategory_id' => isset($request->subcategory_id) ? $request->subcategory_id : '',
            'sub_subcategory_id' => isset($request->sub_subcategory_id) ? $request->sub_subcategory_id : '',
            'product_description' => trim($request->product_description),
            'product_code' => $request->product_code,
            'cost' => ($request->cost) ? $request->cost : 0,
            'is_checking_stock' => ($request->is_checking_stock) ? 1 : 0,
            'promotion_start_date' => $request->promotion_start_date,
            'promotion_end_date' => $request->promotion_end_date,
            'promotion_price' => ($request->promotion_price) ? $request->promotion_price : 0,
            'featured' => ($request->featured) ? 1 : 0,
            'is_used' => ($request->is_used) ? 1 : 0,
            'show_per_price' => ($request->show_per_price) ? 1 : 0,
            'status' => ($request->status) ? $request->status : 1,
        ]);
        $product_id = $product->id;

        //add slug
        $product_slug = $this->makeUniqueSlug('products', $product_id, 'product_slug', $request->product_name);
        Product::where('id', $product_id)->update(['product_slug' => $product_slug]);

        return $this->apiResponse(true, 'Product Added Successfully', $product, 201);
    }

    public function updateProduct(Request $request, $product_id) {
        $product = Product::find($product_id);
        if($product) {
            $validator = Validator::make($request->all(), [
                'store_id' => 'required|numeric',
                'unit_id' => 'required',
                'product_name' => 'required',
                'product_image' => 'required',
                'price' => 'required|numeric',
                'quantity' => 'required|numeric',
                'category_id' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
            }

            $destinationPath = public_path('uploads/products/');
            $thumbDestinationPath = public_path('uploads/products/thumbnail/');
            if (!empty($request->product_image)) {
                // if (!empty($product->product_image)) {
                //     if (file_exists(public_path('/uploads/products/' . $product->product_image))) {
                //         unlink(public_path('/uploads/products/' . $product->product_image));
                //     }
                //     if (file_exists(public_path('/uploads/products/thumbnail/' . $product->product_image))) {
                //         unlink(public_path('/uploads/products/thumbnail/' . $product->product_image));
                //     }
                // }

                // $product_image = $request->file('product_image');
                // $product_image_path = 'product_image' . str_random(10) . time() . '.' . $product_image->getClientOriginalExtension();

                // $thumbnail_image = Image::make($product_image->getRealPath());
                // $thumbnail_image->resize(220, 220, function ($constraint) {
                //     $constraint->aspectRatio();
                // })->save($thumbDestinationPath . '/' . $product_image_path);

                // $product_image->move($destinationPath, $product_image_path);
                $product_image_path = 'product_image'.time().'.' . explode('/', explode(':', substr($request->product_image, 0, strpos($request->product_image, ';')))[1])[1];
                Image::make($request->product_image)->save($destinationPath .$product_image_path);
                Image::make($request->product_image)->save($thumbDestinationPath .$product_image_path);
            } else {
                $product_image_path = $product->product_image;
            }

            $product_slug = $this->makeUniqueSlug('products', $product_id, 'product_slug', $request->product_name, 'update');

            Product::where('id', $product_id)->update([
                'unit_id' => $request->unit_id,
                'brand_id' => isset($request->brand_id) ? $request->brand_id : '',
                'product_name' => trim($request->product_name),
                'product_slug' => '',
                'product_image' => $product_image_path,
                'price' => trim($request->price),
                'product_video' => isset($request->product_video) ? $request->product_video: '',
                'quantity' => ($request->quantity) ? $request->quantity : 1,
                'category_id' => $request->category_id,
                'subcategory_id' => isset($request->subcategory_id) ? $request->subcategory_id : '',
                'sub_subcategory_id' => isset($request->sub_subcategory_id) ? $request->sub_subcategory_id : '',
                'product_description' => trim($request->product_description),
                'product_code' => $request->product_code,
                'cost' => ($request->cost) ? $request->cost : 0,
                'is_checking_stock' => ($request->is_checking_stock) ? 1 : 0,
                'promotion_start_date' => $request->promotion_start_date,
                'promotion_end_date' => $request->promotion_end_date,
                'promotion_price' => ($request->promotion_price) ? $request->promotion_price : 0,
                'featured' => ($request->featured) ? 1 : 0,
                'is_used' => ($request->is_used) ? 1 : 0,
                'show_per_price' => ($request->show_per_price) ? 1 : 0,
                'status' => ($request->status) ? $request->status : 1,
            ]);
            return $this->apiResponse(true, 'Product Updated Successfully', [], 200);
        } else {
            return $this->apiResponse(false, 'Product ID Not Found', [], 404);
        }

    }

    public function deleteProduct($id) {
        $product = Product::find($id);
        if($product) {
            if (!empty($product->product_image)) {
                if (file_exists(public_path('/uploads/products/' . $product->product_image))) {
                    unlink(public_path('/uploads/products/' . $product->product_image));
                }
                if (file_exists(public_path('/uploads/products/thumbnail/' . $product->product_image))) {
                    unlink(public_path('/uploads/products/thumbnail/' . $product->product_image));
                }
            }

            $product->delete();
            return $this->apiResponse(true, 'Product Deleted Successfully', [], 200);
        } else {
            return $this->apiResponse(false, 'Product ID Not Found', [], 404);
        }
    }

    public function getProductDetails($id) {
        $product = DB::table('products')->join('product_units', 'products.unit_id', '=', 'product_units.id')->where('products.id', $id)->select('products.*', 'product_units.unit_name')->first();
        if($product) {
            $product = array('data' => $product);
            return $this->apiResponse(true, 'Product Details', $product, 200);
        } else {
            return $this->apiResponse(false, 'Product ID Not Found', [], 404);
        }
    }




}
