<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class PurchaseController extends Controller
{
    use HelperTrait;

    public function index() {
        $per_page = 15;
        if(isset($_GET['per_page'])) {
            $per_page = $_GET['per_page'];
        }
        if(isset($_GET['shop_id'])) {
            if($per_page == 0) {
                $result = DB::table('purchases')
                ->join('users', 'purchases.supplier_id', '=', 'users.id')
                ->select('purchases.*', 'users.name')
                ->where('purchases.store_id', $_GET['shop_id'])
                ->orderByDesc('purchases.id')
                ->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('purchases')
                ->join('users', 'purchases.supplier_id', '=', 'users.id')
                ->select('purchases.*', 'users.name')
                ->where('purchases.store_id', $_GET['shop_id'])
                ->orderByDesc('purchases.id')
                ->paginate($per_page);
            }
        } else {
            if($per_page == 0) {
                $result = DB::table('purchases')
                ->join('users', 'purchases.supplier_id', '=', 'users.id')
                ->select('purchases.*', 'users.name')
                ->orderByDesc('purchases.id')
                ->get();
                $result = array('data' => $result);
            } else {
                $result = DB::table('purchases')
                ->join('users', 'purchases.supplier_id', '=', 'users.id')
                ->select('purchases.*', 'users.name')
                ->orderByDesc('purchases.id')
                ->paginate($per_page);
            }
        }
        if(count($result) > 0) {
            return $this->apiResponse(true, 'Purchase List', $result);
        } else {
            return $this->apiResponse(false, 'Purchase Not Found', [], 404);
        }
    }

    public function details($id) {
        $result = array();
        $purchase = DB::table('purchases')
        ->join('users', 'purchases.supplier_id', '=', 'users.id')
        ->join('admins', 'purchases.created_by', '=', 'admins.id')
        ->select('purchases.*', 'users.name', 'admins.name as admin_name')
        ->where('purchases.id', $id)
		->first();
        $purchase_items = DB::table('purchase_items')->where('purchase_id', $id)->get();
        $result['purchase'] = $purchase;
        $result['purchase_items'] = $purchase_items;
        if($purchase) {
            $result = array('data' => $result);
            return $this->apiResponse(true, 'Purchase Details', $result, 200);
        } else {
            return $this->apiResponse(false, 'Purchase ID Not Found', [], 404);
        }
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'supplier_id' => 'required',
            'created_by' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->apiResponse(false, 'Validation Errors', $validator->errors(), 401);
        }
        if(!$request->items) {
            return $this->apiResponse(false, 'Please Add Some Purchase Items', [], 400);
        }
        if (!file_exists(public_path('uploads/purchase_attachments'))) {
			mkdir(public_path('uploads/purchase_attachments'), 0777, true);
		}
        $destinationPath = public_path('uploads/purchase_attachments/');
        if (!empty($request->attachment)) {
            $attachment_name = 'attachment'.time().'.' . explode('/', explode(':', substr($request->attachment, 0, strpos($request->attachment, ';')))[1])[1];
            Image::make($request->attachment)->save($destinationPath .$attachment_name);
        } else {
            $attachment_name = '';
        }

        DB::table('purchases')->insert([
			'store_id' => $request->store_id,
			'supplier_id' => $request->supplier_id,
	    	'total' => 0,
	    	'discount' => $request->discount ? $request->discount : 0,
	    	'grand_total' => 0,
	    	'note' => $request->note,
	    	'attachment' => $attachment_name,
	    	'shipping' => $request->shipping,
	    	'status' => $request->status,
	    	'created_by' => $request->created_by,
	    	'created_at' => $request->date,
		]);
        $purchase_id = DB::getPdo()->lastInsertId();
        if($request->items) {
			$total = 0;
			foreach($request->items as $item) {
				$total += ($item['quantity'] * $item['cost']);
				DB::table('purchase_items')->insert([
					'purchase_id' => $purchase_id,
					'product_id' => $item['id'],
					'product_name' => $item['name'],
					'sale_price' => $item['price'],
					'cost_price' => $item['cost'],
					'quantity' => $item['quantity'],
					'subtotal' => ($item['quantity'] * $item['cost'])
				]);
				//update main product
				DB::table('products')->where('id', $item['id'])->update(['price' => $item['price'], 'quantity' => ($item['quantity'] + $item['stock']), 'cost' => $item['cost']]);
			}
			DB::table('purchases')->where('id', $purchase_id)->update([
				'total' => $total,
				'grand_total' => $request->discount ? ($total - $request->discount) : $total
			]);
		}
        
        return $this->apiResponse(true, 'Purchase Added Successfully', [], 201);
    }

    public function delete($id) {
        DB::table('purchase_items')->where(['purchase_id' => $id])->delete();
        DB::table('purchases')->where(['id' => $id])->delete();
        return $this->apiResponse(true, 'Purchase  Deleted Successfully', [], 200);
    }


}