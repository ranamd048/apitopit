<?php

namespace App\Http\Controllers;

use App\Events\UserMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{

    public function index() {
        return view('messages.index');
    }

    public function user_list($filter_type) {
        if(Session::has('admin_id')) {
            $sender_id = Session::get('role') == 0 ? Session::get('admin_id') : Session::get('store_id');
            $sender_type = Session::get('role') == 0 ? 'super_admin' : 'shop_admin';
        }
        if(Session::has('customer_id')) {
            $sender_id = Session::get('customer_id');
            $sender_type = 'customer';
        }

        $messaged_users = DB::table('chats')->where(function($query) use ($sender_id, $sender_type) {
            $query->where(['sender_id' => $sender_id, 'sender_type' => $sender_type]);
        })->orWhere(function($query) use ($sender_id, $sender_type) {
            $query->where(['reciver_id' => $sender_id, 'reciver_type' => $sender_type]);
        })->orderByDesc('updated_at')->get();

        $users_array = [];
        if(count($messaged_users) > 0) {
            foreach($messaged_users as $user) {
                //get last meessage
                $last_message = DB::table('chat_messages')->select('sender', 'message', 'is_read', 'created_at')->where('chat_id', $user->id)->orderByDesc('id')->first();
                if($user->sender_id == $sender_id) {
                    $new_array = ['id' => $user->id, 'user_id' => $user->reciver_id, 'user_name' => $user->reciver_name, 'user_photo' => $user->reciver_photo, 'user_type' => $user->reciver_type, 'sender' => $last_message->sender, 'message' => $last_message->message, 'is_read' => $last_message->is_read, 'message_time' => $last_message->created_at];
                    array_push($users_array, $new_array);
                }
                if($user->reciver_id == $sender_id) {
                    $new_array = ['id' => $user->id, 'user_id' => $user->sender_id, 'user_name' => $user->sender_name, 'user_photo' => $user->sender_photo, 'user_type' => $user->sender_type, 'sender' => $last_message->sender, 'message' => $last_message->message, 'is_read' => $last_message->is_read, 'message_time' => $last_message->created_at];
                    array_push($users_array, $new_array);
                }
            }
        }
        $data['result'] = $users_array;
        $data['sender_id'] = $sender_id;
        $data['filter_type'] = $filter_type;

        return view('messages.user_list', $data);
    }

    public function user_messages() {
        if(Session::has('admin_id')) {
            $sender_id = Session::get('role') == 0 ? Session::get('admin_id') : Session::get('store_id');
        }
        if(Session::has('customer_id')) {
            $sender_id = Session::get('customer_id');
        }
        $chat_id = $_GET['chat_id'];
        $user_id = $_GET['user_id'];
        $user_type = $_GET['user_type'];
        $data['user_info'] = $this->chat_user_info($user_id, $user_type);
        $data['messages'] = DB::table('chat_messages')->where('chat_id', $chat_id)->get();
        $data['sender_id'] = $sender_id;
        $data['reciver_id'] = $user_id;
        $data['reciver_type'] = $user_type;
        //read message
        DB::table('chat_messages')->where(['chat_id' => $chat_id, 'is_read' => 0])->where('sender', '!=', $sender_id)->update(['is_read' => 1]);

        return view('messages.user_messages', $data);
    }

    public function new_message() {
        return view('messages.new_message');
    }

    public function search_user($type) {
        if(Session::has('admin_id')) {
            $sender_id = Session::get('role') == 0 ? Session::get('admin_id') : Session::get('store_id');
        }
        if(Session::has('customer_id')) {
            $sender_id = Session::get('customer_id');
        }

        $search_result = '';
        $query_result = [];
        if(!empty($type)) {
            if($type == 'shop_admin') {
                $query_result = DB::table('stores')->where('status', 1)->where('id', '!=', $sender_id)->select('id', 'store_name as name', 'phone')->get();
            }
            if($type == 'super_admin') {
                $query_result = DB::table('admins')->where('role', 0)->where('id', '!=', $sender_id)->select('id', 'name', 'phone')->get();
            }
            if($type == 'customer') {
                $query_result = DB::table('users')->where(['user_type' => 'customer', 'status' => 1])->where('id', '!=', $sender_id)->select('id', 'name', 'phone')->get();
            }
        }

        if(count($query_result) > 0) {
            $search_result .= '<div class="form-group">';
            $search_result .= '<select class="form-control" id="reciver_id">';
            foreach($query_result as $row) {
                $search_result .= '<option value="'.$row->id.'">'.$row->name.' -> '.$row->phone.'</option>';
            }
            $search_result .= '</select></div>';
            $search_result .= '<div class="form-group"><textarea class="form-control" id="message_body" placeholder="Write Message..."></textarea></div>';
            $search_result .= '<div class="form-group"><button class="btn btn-info" id="send_new_message">Send Message</button></div>';
        }

        return $search_result;
    }

    public function send_new_message(Request $request) {
        if (isset($request->sender_id)) {
            $sender_id = $request->sender_id;
            $sender_type = $request->type;
        } else {
            if(Session::has('admin_id')) {
                $sender_id = Session::get('role') == 0 ? Session::get('admin_id') : Session::get('store_id');
                $sender_type = Session::get('role') == 0 ? 'super_admin' : 'shop_admin';
            }
            if(Session::has('customer_id')) {
                $sender_id = Session::get('customer_id');
                $sender_type = 'customer';
            }
        }

        if (isset($request->sender_type)) {
            $sender_id = $request->sender_id;
            $sender_type = $request->sender_type;
            $reciver_id = $request->reciver_id;
            $reciver_type = $request->reciver_type;
        } else {        
            $reciver_id = $request->reciver_id;
            $reciver_type = $request->type;
        }


        $is_chat = DB::table('chats')->where(function($query) use ($sender_id, $reciver_id, $sender_type, $reciver_type) {
            $query->where(['sender_id' => $sender_id, 'reciver_id' => $reciver_id, 'sender_type' => $sender_type, 'reciver_type' => $reciver_type]);
        })->orWhere(function($query) use ($sender_id, $reciver_id, $sender_type, $reciver_type) {
            $query->where(['sender_id' => $reciver_id, 'reciver_id' => $sender_id, 'sender_type' => $reciver_type, 'reciver_type' => $sender_type]);
        })->first();
        $sender_info = $this->chat_user_info($sender_id, $sender_type);
        if($is_chat) {
            $chat_id = $is_chat->id;
            DB::table('chats')->where('id', $chat_id)->update([
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            DB::table('chat_messages')->insert([
                'chat_id' => $chat_id,
                'sender' => $sender_id,
                'message' => trim($request->message_body),
            ]);
        } else {
            $reciver_info = $this->chat_user_info($reciver_id, $reciver_type);
            DB::table('chats')->insert([
                'reciver_id' => $reciver_id,
                'sender_id' => $sender_id,
                'sender_name' => $sender_info['name'],
                'sender_photo' => $sender_info['photo_path'],
                'reciver_name' => $reciver_info['name'],
                'reciver_photo' => $reciver_info['photo_path'],
                'reciver_type' => $reciver_type,
                'sender_type' => $sender_type
            ]);
            $chat_id = DB::getPdo()->lastInsertId();
            DB::table('chat_messages')->insert([
                'chat_id' => $chat_id,
                'sender' => $sender_id,
                'message' => trim($request->message_body)
            ]);
        }

        if ($sender_type == 'customer') {
            $channel = ($request->type == 'super_admin') ? 'admin' : 'shop_' . $request->reciver_id;
        } else {
            if (Session::get('role') == 1) {
                $channel = ($request->type == 'super_admin') ? 'admin' : 'user_' . $request->reciver_id;
            } else {
                $channel = ($request->type == 'shop_admin') ? 'shop_' . $request->reciver_id : 'user_' . $request->reciver_id;
            }
        }

        event(new UserMessage(['message' => trim($request->message_body), 'name' => $sender_info['name'], 'time' => date('h:i a d-M-Y'), 'chat_id' => $chat_id, 'photo' => $sender_info['photo_path']], $channel));

        return response()->json(['is_new' => $request->is_new, 'chat_id' => $chat_id, 'user_id' => $reciver_id, 'user_type' => $reciver_type]);
    }

    private function chat_user_info($user_id, $type) {
        $default_photo = asset('public/uploads/admin.jpg');
        if($type == 'super_admin') {
            $result = DB::table('admins')->select('name', 'profile_photo')->where('id', $user_id)->first();
            $photo_path = isset($result->profile_photo) ? asset('public/uploads/'.$result->profile_photo) : $default_photo;
        }
        if($type == 'shop_admin') {
            $result = DB::table('stores')->select('store_name as name', 'logo')->where('id', $user_id)->first();
            $photo_path = isset($result->logo) ? asset('public/uploads/store/'.$result->logo) : $default_photo;
        }
        if($type == 'customer') {
            $result = DB::table('users')->select('name', 'photo')->where('id', $user_id)->first();
            $photo_path = isset($result->photo) ? asset('public/uploads/customer/'.$result->photo) : $default_photo;
        }
        return ['name' => (isset($result->name) ? $result->name : ''), 'photo_path' => $photo_path];
    }


}
