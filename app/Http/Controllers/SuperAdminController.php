<?php

namespace App\Http\Controllers;

use App\Http\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class SuperAdminController extends Controller
{

    use HelperTrait;

    public function create_store()
    {
        $data['shop_categories'] = DB::table('shop_category')->orderBy('sort_order', 'ASC')->get();
        return view('backend.store.add', $data);
    }

    public function save_store_information(Request $request)
    {
        $request->validate([
            'shop_type' => 'required',
            'store_name' => 'required|string',
            'baner_image' => 'required|image|mimes:jpeg,png,jpg',
            'store_logo' => 'image|mimes:jpeg,png,jpg',
            'address' => 'required|string',
            'name' => 'required|string',
            'phone' => 'required|unique:stores|numeric',
            'phone' => 'unique:admins',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);

        $slug = $this->make_slug($request->store_name);
        $check_slug = DB::table('stores')->select('slug')->where('slug', $slug)->first();
        if ($check_slug) {
            $slug = $slug . rand(1000, 10000);
        }

        $destinationPath = public_path('uploads/store/');
        if ($request->hasFile('baner_image')) {
            $baner_image = $request->file('baner_image');
            $baner_image_name = 'baner_image' . time() . '.' . $baner_image->getClientOriginalExtension();
            $baner_image->move($destinationPath, $baner_image_name);
        } else {
            $baner_image_name = '';
        }

        $destinationPath = public_path('uploads/store/');
        if ($request->hasFile('store_logo')) {
            $store_logo = $request->file('store_logo');
            $store_logo_name = 'store_logo' . time() . '.' . $store_logo->getClientOriginalExtension();
            $store_logo->move($destinationPath, $store_logo_name);
        } else {
            $store_logo_name = '';
        }

        DB::table('stores')->insert([
            'shop_category_id' => $request->shop_category,
            'store_name' => trim($request->store_name),
            'slug' => $slug,
            'shop_type' => $request->shop_type,
            'baner_image' => $baner_image_name,
            'logo' => $store_logo_name,
            'address' => trim($request->address),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'expiry_date' => Carbon::now()->addMonths(1),
            'status' => 1,
        ]);
        $store_id = DB::getPdo()->lastInsertId();

        DB::table('admins')->insert([
            'name' => trim($request->name),
            'user_name' => trim($request->phone),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'role' => 1,
            'store_id' => $store_id,
            'password' => md5(trim($request->password)),
        ]);

        return redirect('admin/store-list')->with('message', 'Store Created Successfull');
    }

    public function password_check_for_delete(Request $request) {
        $check_password = DB::table('admins')->where(['id' => Session::get('admin_id'), 'password' => md5($request->password)])->first();
        if($check_password) {
            if(empty($request->delete_id) || empty($request->delete_for)) {
                return redirect()->back()->with('error', 'Sorry! Delete Failed.');
            } else {
                DB::table($request->delete_for)->where('id', $request->delete_id)->delete();
                return redirect()->back()->with('message', 'Deleted Successfully!');
            }
        } else {
            return redirect()->back()->with('error', 'Sorry! You have entered wrong password.');
        }
    }

    public function get_all_store_list()
    {
        return view('backend.store.list');
    }

    public function load_store_list(Request $request) {
        if(empty($request->id) && empty($request->type)) {
            $data['store_list'] = DB::table('stores')->get();
        }
        if($request->type == 'division') {
            $data['store_list'] = DB::table('stores')->where('division', $request->id)->get();
        }
        if($request->type == 'district') {
            $data['store_list'] = DB::table('stores')->where('district', $request->id)->get();
        }
        if($request->type == 'upazila') {
            $data['store_list'] = DB::table('stores')->where('thana', $request->id)->get();
        }
        
        return view('backend.store.shop_list', $data);
    }

    public function edit_store($id)
    {
        $data = [];
        $data['store'] = DB::table('stores')->where('id', $id)->first();
        $data['shop_categories'] = DB::table('shop_category')->orderBy('sort_order', 'ASC')->get();
        return view('backend.store.edit', $data);
    }

    public function delete_store($id)
    {
        DB::table('stores')->where('id', $id)->delete();
        return redirect()->back()->with('message', 'Deleted Successfully');
    }

    public function update_store_info(Request $request)
    {
        $request->validate([
            'shop_type' => 'required',
            'store_name' => 'required|string',
            'baner_image' => 'image|mimes:jpeg,png,jpg',
            'logo' => 'image|mimes:jpeg,png,jpg',
            'address' => 'required|string',
            'phone' => 'numeric',
        ]);

        $destinationPath = public_path('uploads/store/');
        if ($request->hasFile('baner_image')) {
            if (!empty($request->existing_baner_img)) {
                if (file_exists(public_path('/uploads/store/' . $request->existing_baner_img))) {
                    unlink(public_path('/uploads/store/' . $request->existing_baner_img));
                }
            }

            $baner_image = $request->file('baner_image');
            $baner_image_name = 'baner_image' . time() . '.' . $baner_image->getClientOriginalExtension();
            $baner_image->move($destinationPath, $baner_image_name);
        } else {
            $baner_image_name = $request->existing_baner_img;
        }

        if ($request->hasFile('logo')) {
            if (!empty($request->existing_logo)) {
                if (file_exists(public_path('/uploads/store/' . $request->existing_logo))) {
                    unlink(public_path('/uploads/store/' . $request->existing_logo));
                }
            }

            $logo = $request->file('logo');
            $logo_name = 'logo_' . time() . '.' . $logo->getClientOriginalExtension();
            $logo->move($destinationPath, $logo_name);
        } else {
            $logo_name = $request->existing_logo;
        }

        if(is_array($request->close_day)) {
            $close_days = implode(",",$request->close_day);
        } else {
            $close_days = '';
        }

        DB::table('stores')->where('id', $request->store_id)->update([
            'shop_category_id' => isset($request->shop_category) ? $request->shop_category : null,
            'store_name' => trim($request->store_name),
            'shop_type' => $request->shop_type,
            'baner_image' => $baner_image_name,
            'video_link' => trim($request->video_link),
            'due_sms_text' => trim($request->due_sms_text),
            'logo' => $logo_name,
            'address' => trim($request->address),
            'thana' => $request->thana,
            'district' => $request->district,
            'division' => $request->division,
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'min_order_amount' => trim($request->min_order_amount),
            'shipping_cost' => trim($request->shipping_cost),
            'other_shipping_cost' => trim($request->other_shipping_cost),
            'opening_time' => trim($request->opening_time),
            'close_day' => $close_days,
            'home_delivery' => trim($request->home_delivery),
            'tin_no' => trim($request->tin_no),
            'serial_no' => trim($request->serial_no),
            'license_no' => trim($request->license_no),
            'fb_link' => trim($request->fb_link),
            'youtube_link' => trim($request->youtube_link),
            'maps_iframe' => trim($request->maps_iframe),
            'map_lat' => trim($request->map_lat),
            'map_long' => trim($request->map_long),
            'show_phone_number' => $request->show_phone_number,
            'show_price' => $request->show_price,
            'note' => trim($request->note),
        ]);

        //only can update superadmin
        if (Session::get('role') == 0) {
            DB::table('stores')->where('id', $request->store_id)->update([
                'upload_permission' => $request->upload_permission,
                'image_required' => $request->image_required,
                'product_qty' => $request->product_qty,
                'product_image' => $request->product_image,
                'service_qty' => $request->service_qty,
                'expiry_date' => $request->expiry_date,
                'is_featured' => $request->is_featured,
                'sms_limit' => $request->sms_limit,
                'print_type' => $request->print_type,
                'status' => $request->status,
            ]);
        }
        
        if(Session::has('store_id')) {
            Session::put('store_type', $request->shop_type);
        }
        if (Session::get('role') == 0) {
            return redirect('admin/store-list')->with('message', 'Store Updated Successfully');
        } else {
            return redirect()->back()->with('message', 'Store Updated Successfully');
        }

    }

    public function create_store_category() {
        return view('backend.store_category.add');
    }

    public function save_store_category(Request $request) {
        $this->validate($request, [
			'name' => 'required',
	        'image' => 'image|mimes:jpeg,png,jpg'
	    ]);

		$destinationPath = public_path('uploads/store_category/');
	    if ($request->hasFile('image')) {
	        $image = $request->file('image');
	        $image_name = 'store_category'.time().'.'.$image->getClientOriginalExtension();
	        $image->move($destinationPath, $image_name);
	    } else {
	    	$image_name = '';
	    }

	    DB::table('shop_category')->insert([
	    	'name' => trim($request->name),
	    	'icon' => $image_name,
	    	'sort_order' => $request->sort_order,
	    ]);
	    return redirect('admin/store_categories')->with('message', 'Created Successfully');
    }

    public function store_category_list() {
        $data['store_categories'] = DB::table('shop_category')->orderBy('sort_order', 'ASC')->get();
		return view('backend.store_category.list', $data);
    }

    public function edit_store_category($id) {
        $data['store_category'] = DB::table('shop_category')->where('id', $id)->first();
		return view('backend.store_category.edit', $data);
    }

    public function update_store_category(Request $request) {
        $this->validate($request, [
			'name' => 'required',
	        'image' => 'image|mimes:jpeg,png,jpg'
	    ]);

		$destinationPath = public_path('uploads/store_category/');
	    if ($request->hasFile('image')) {
	    	if(!empty($request->existing_img)) {
	    		if(file_exists(public_path('/uploads/store_category/'.$request->existing_img))) {
	    			unlink(public_path('/uploads/store_category/'.$request->existing_img));
	    		}
	    	}
	        $image = $request->file('image');
	        $image_name = 'store_category'.time().'.'.$image->getClientOriginalExtension();
	        $image->move($destinationPath, $image_name);
	    } else {
	    	$image_name = $request->existing_img;
	    }

	    DB::table('shop_category')->where('id', $request->store_category_id)->update([
	    	'name' => trim($request->name),
	    	'icon' => $image_name,
			'sort_order' => $request->sort_order
		]);
		
		return redirect('admin/store_categories')->with('message', 'Updated Successfully');
    }

    public function delete_store_category($id) {
        DB::table('shop_category')->where('id', $id)->delete();
		return redirect()->back()->with('message', 'Deleted Successfully');
    }

    public function create_unit()
    {
        return view('backend.unit.add');
    }

    public function save_unit_information(Request $request)
    {
        DB::table('product_units')->insert([
            'unit_name' => trim($request->unit_name),
            'unit_status' => $request->unit_status,
        ]);
        return redirect('admin/unit-list')->with('message', 'Created Successfully');
    }

    public function get_all_unit_list()
    {
        $data = [];
        $data['unit_list'] = DB::table('product_units')->get();
        return view('backend.unit.list', $data);
    }

    public function edit_unit($id)
    {
        $data = [];
        $data['unit'] = DB::table('product_units')->where('id', $id)->first();
        return view('backend.unit.edit', $data);
    }

    public function update_unit_info(Request $request)
    {
        DB::table('product_units')->where('id', $request->unit_id)->update([
            'unit_name' => trim($request->unit_name),
            'unit_status' => $request->unit_status,
        ]);
        return redirect('admin/unit-list')->with('message', 'Updated Successfully');
    }

    public function delete_unit($id)
    {
        DB::table('product_units')->where('id', $id)->delete();
        return redirect()->back()->with('message', 'Deleted Successfully');
    }

    public function get_all_price_groups() {
        $data['price_group_list'] = DB::table('price_groups')->get();
        return view('backend.price_groups.list', $data);
    }

    public function save_price_groups(Request $request) {
        DB::table('price_groups')->insert(['name' => $request->name]);
        return redirect()->back()->with('message', 'Added Successfully');
    }

    public function update_price_group(Request $request) {
        DB::table('price_groups')->where('id', $request->price_group_id)->update(['name' => $request->name]);
        return redirect()->back()->with('message', 'Updated Successfully');
    }

    public function delete_price_group($id)
    {
        DB::table('price_groups')->where('id', $id)->delete();
        return redirect()->back()->with('message', 'Deleted Successfully');
    }

    public function createNewStaff()
    {
        if (Session::has('store_id')) {
            $data['shop_admins'] = DB::table('admins')->select('id', 'name')->where(['store_id' => Session::get('store_id'), 'role' => 1])->get();
        }
        $data['admin_positions'] = DB::table('admin_position')->orderBy('sort_order', 'ASC')->get();
        $data['divisions'] = DB::table('divisions')->get();
        $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->get();
        return view('backend.staff.add', $data);
    }

    public function getShopOwnerList(Request $request)
    {
        if (empty($request->shop_id)) {
            return '<option value="">Please Select a Shop at First</option>';
        }

        $shop_list = DB::table('admins')->select('id', 'name', 'phone')->where(['store_id' => $request->shop_id, 'role' => 1])->get();

        if (count($shop_list) > 0) {
            $output = '<option value="">Select Reference</option>';
            foreach ($shop_list as $row) {
                $output .= '<option value="' . $row->id . '">' . $row->name .' ('.$row->phone.')'.'</option>';
            }
            return $output;
        } else {
            return '<option value="">Not Found...</option>';
        }

    }

    public function saveStaff(Request $request)
    {
        $request->validate([
            'store_id' => 'required',
            'position' => 'required',
            'staff_role' => 'required',
            'name' => 'required|string',
            'phone' => 'required|unique:admins|numeric',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);

        DB::table('admins')->insert([
            'parent_id' => ($request->parent_id) ? $request->parent_id : 0,
            'name' => trim($request->name),
            'user_name' => trim($request->phone),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'role' => $request->staff_role,
            'position_id' => $request->position,
            'is_main' => 0,
            'store_id' => $request->store_id,
            'password' => md5(trim($request->password)),
            'thana' => $request->thana,
            'district' => $request->district,
            'division' => $request->division,
            'product_access' => ($request->product_access) ? 1 : 0,
            'shop_settings' => ($request->shop_settings) ? 1 : 0,
            'order_access' => ($request->order_access) ? 1 : 0,
            'service_access' => ($request->service_access) ? 1 : 0,
        ]);

        return redirect('admin/staff-list')->with('message', 'Staff Created Successfull');
    }

    public function staffList()
    {
        if (Session::has('store_id') && Session::get('admin_type') == 1) {
            $data['result'] = DB::table('admins')
                ->join('stores', 'admins.store_id', '=', 'stores.id')
                ->select('admins.*', 'stores.store_name')
                ->where(['admins.store_id' => Session::get('store_id'), 'admins.is_main' => 0])
                ->get();
        } elseif (Session::has('store_id') && Session::get('admin_type') == 0) {
            $parent_ids = $this->extractParentid(Session::get('admin_id'));
            if(count($parent_ids) == 0) {
                $parent_ids = [Session::get('admin_id')];
            } else {
                array_push($parent_ids, Session::get('admin_id'));
            }
            $data['result'] = DB::table('admins')
                ->join('stores', 'admins.store_id', '=', 'stores.id')
                ->select('admins.*', 'stores.store_name')
                ->whereIn('parent_id',$parent_ids)
                ->get();
        } else {
            $data['result'] = DB::table('admins')
                ->join('stores', 'admins.store_id', '=', 'stores.id')
                ->select('admins.*', 'stores.store_name')
                ->where('admins.is_main', 0)
                ->get();
        }
        return view('backend.staff.list', $data);
    }

    public function editStaff($id)
    {
        if (Session::has('store_id')) {
            $data['shop_admins'] = DB::table('admins')->select('id', 'name')->where(['store_id' => Session::get('store_id'), 'role' => 1])->get();
        }

        $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->get();
        $data['admin_positions'] = DB::table('admin_position')->orderBy('sort_order', 'ASC')->get();
        $data['result'] = DB::table('admins')->where('id', $id)->first();
        $data['divisions'] = DB::table('divisions')->get();
        return view('backend.staff.edit', $data);
    }

    public function updateStaff(Request $request)
    {
        DB::table('admins')->where('id', $request->staff_id)->update([
            'parent_id' => ($request->parent_id) ? $request->parent_id : 0,
            'name' => trim($request->name),
            'email' => trim($request->email),
            'thana' => $request->thana,
            'district' => $request->district,
            'division' => $request->division,
            'product_access' => ($request->product_access) ? 1 : 0,
            'shop_settings' => ($request->shop_settings) ? 1 : 0,
            'order_access' => ($request->order_access) ? 1 : 0,
            'service_access' => ($request->service_access) ? 1 : 0,
        ]);
        if(!empty($request->password)) {
            DB::table('admins')->where('id', $request->staff_id)->update([
                'password' => md5(trim($request->password)),
            ]);
        }

        return redirect('admin/staff-list')->with('message', 'Updated Successfull');
    }

    // public function customerList()
    // {
    //     if(Session::has('store_id')) {
    //         $data['shop_id'] = Session::get('store_id');
    //     } else {
    //         $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
    //         $data['shop_id'] = $data['shop_list'][0]->id;
    //     }

    //     return view('backend.staff.customer_list', $data);
    // }

    public function customerList()
    {
        if(!Session::has('store_id') && Session::get('role') == 0) {
            if(isset($_GET['name']) || isset($_GET['mobile_number'])) {
                $data['customer_list'] = DB::table('users')->where('name', 'like', '%' . $_GET['name'] . '%')->where('phone', 'like', '%' . $_GET['mobile_number'] . '%')->get();
            } else {
                $data['customer_list'] = DB::table('users')->get();
            }
            return view('backend.customer.list', $data);
        }
        $shop_id = Session::get('store_id');
        if(isset($_GET['name']) || isset($_GET['mobile_number'])) {
            $data['customer_list'] = DB::table('users')
            ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
            ->select('users.*')
            ->where(['shop_customers.shop_id' => $shop_id])
            ->where('users.name', 'like', '%' . $_GET['name'] . '%')
            ->where('users.phone', 'like', '%' . $_GET['mobile_number'] . '%')
            ->groupBy('shop_customers.user_id')
            ->get();
        } else {
            $data['customer_list'] = DB::table('users')
            ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
            ->select('users.*')
            ->where(['shop_customers.shop_id' => $shop_id])
            ->groupBy('shop_customers.user_id')
            ->get();
        }

        return view('backend.staff.customer_list', $data);
    }

    public function editCustomer($id)
    {
        $data['customer'] = DB::table('users')->where('id', $id)->first();
        return view('backend.staff.edit_customer', $data);
    }

    public function customerDetails($user_id) {
        $shop_id = Session::get('store_id');
        $customer = DB::table('users')->select('id', 'name', 'email', 'phone', 'address', 'thana', 'district', 'division', 'photo')->where(['id' => $user_id])->first();
		
		$customer_balance = DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $shop_id])->first();
		if($customer_balance) {
			$customer->current_balance = $customer_balance->current_balance;
			$customer->shop_id = $customer_balance->shop_id;
		} else {
			$customer->current_balance = 0;
			$customer->shop_id = $shop_id;
		}

        $payment_summary = DB::table('orders_invoice')
            ->select(DB::raw("SUM(orders_invoice.due_amount) as total_due_amount"), DB::raw("SUM(orders_invoice.paid_amount) as total_paid_amount"))
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $user_id])
            ->first();
          
		if($payment_summary) {
			$customer->total_due_amount = $payment_summary->total_due_amount ? $payment_summary->total_due_amount : 0;
			$customer->total_paid_amount = $payment_summary->total_paid_amount ? $payment_summary->total_paid_amount : 0;
		} else {
			$customer->total_due_amount = 0;
			$customer->total_paid_amount = 0;
		}
        //echo "<pre>"; print_r($customer); exit();
        $data['result'] = $customer;
        return view('backend.staff.customer_details', $data);
    }

    public function customerPayNow() {
        $data['due_amount'] = $_GET['due'];
        $data['user_id'] = $_GET['user_id'];
        return view('backend.staff.pay_now', $data); 
    }

    public function saveCustomerPayment(Request $request) {
        $shop_id = Session::get('store_id');
        $user_id = $request->user_id;
        $total_amount = trim($request->total_amount);
        $payment_method = $request->payment_method;
        $payment_note = trim($request->payment_note);
        $user_balance = DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $shop_id])->first();
        if($user_balance) {
            $available_amount = ($total_amount + $user_balance->current_balance);
        } else {
            $available_amount = $total_amount;
        }

        $user_due_orders = DB::table('orders_invoice')->select('id', 'user_id', 'due_amount')->where(['store_id' => $shop_id, 'user_id' => $user_id, 'payment_status' => 0])->get();
        if(count($user_due_orders) > 0) {
            foreach($user_due_orders as $due_order) {
                $invoice_due = $due_order->due_amount;
                $invoice_id = $due_order->id;
                if($available_amount > 0) {
                    if($available_amount >= $invoice_due) {
                        $paid_amount = $invoice_due;
                        $due_amount = 0;
                        $available_amount = ($available_amount - $invoice_due);
                    } else {
                        $paid_amount = $available_amount;
                        $due_amount = ($invoice_due - $available_amount);
                        $available_amount = 0;
                    }
                    DB::table('payments')->insert([
                        'store_id' => $shop_id,
                        'invoice_id' => $invoice_id,
                        'user_id' => $user_id,
                        'paid_amount' => $paid_amount,
                        'payment_method' => $payment_method,
                        'payment_note' => $payment_note
                    ]);
                    //current paid amount
                    $already_paid = DB::table('orders_invoice')->select('paid_amount')->where('id', $invoice_id)->first();
                    DB::table('orders_invoice')->where('id', $invoice_id)->update([
                        'paid_amount' => ($already_paid->paid_amount + $paid_amount),
                        'due_amount' => $due_amount,
                        'payment_status' => ($due_amount == 0) ? 1 : 0,
                    ]);
                }
            }
        }
        //save available amount on payment table
        if($available_amount > 0 && count($user_due_orders) > 0) {
            DB::table('payments')->insert([
                'store_id' => $shop_id,
                'user_id' => $user_id,
                'paid_amount' => $available_amount,
                'payment_method' => $payment_method,
                'payment_note' => $payment_note
            ]);
        }
        if(count($user_due_orders) == 0) {
            DB::table('payments')->insert([
                'store_id' => $shop_id,
                'user_id' => $user_id,
                'paid_amount' => $total_amount,
                'payment_method' => $payment_method,
                'payment_note' => $payment_note
            ]);
        }
        //update customer balance
        if($user_balance) {
            DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $shop_id])->update(['current_balance' => $available_amount]);
        } else {
            DB::table('customer_shop_balance')->insert(['user_id' => $user_id, 'shop_id' => $shop_id,'current_balance' => $available_amount]);
        }
        return redirect()->back()->with('message', 'Payment Successfully Completed');
    }

    public function viewCustomerPaymentHistory($id) {
        if(Session::has('store_id')) {
            $shop_id = Session::get('store_id');
        } else {
            $shop = DB::table('stores')->select('id')->first();
            $shop_id = $shop->id;
        }
        
        $data['customer'] = DB::table('users')->where('id', $id)->first();
        $data['payment_summary'] = DB::table('orders_invoice')
            ->select(DB::raw("SUM(orders_invoice.due_amount) as total_due_amount"), DB::raw("SUM(orders_invoice.paid_amount) as total_paid_amount"))
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $id])
            ->first();

        $data['payment_history'] = DB::table('payments')->where(['store_id' => $shop_id, 'user_id' => $id])->orderByDesc('id')->get();

        return view('backend.staff.customer_payment_history', $data);
    }

    public function customerBalanceStatement($id) {
        if(Session::has('store_id')) {
            $shop_id = Session::get('store_id');
        } else {
            $shop = DB::table('stores')->select('id')->first();
            $shop_id = $shop->id;
        }

        $data['customer'] = DB::table('users')->where('id', $id)->first();

        if((isset($_GET['start_date']) && !empty($_GET['start_date'])) && (isset($_GET['end_date']) && !empty($_GET['end_date']))) {
            $pre_balance = $this->preBalanceCalculate($id, $_GET['start_date'], $data['customer']->balance);
            $data['pre_statement'] = $pre_balance;
            $data['customer_order_list'] = DB::table('orders')
            ->join('orders_invoice', 'orders.id', '=', 'orders_invoice.order_id')
            ->select('orders.id', 'orders.subtotal', 'orders.order_date', 'orders_invoice.id as invoice_id')
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $id])
            ->whereBetween('orders.order_date', [$_GET['start_date'], $_GET['end_date']])
            ->get();
        } else {
            $data['pre_statement'] = array('pre_debit' => 0, 'pre_credit' => $data['customer']->balance, 'pre_balance' => $data['customer']->balance);
            $data['customer_order_list'] = DB::table('orders')
            ->join('orders_invoice', 'orders.id', '=', 'orders_invoice.order_id')
            ->select('orders.id', 'orders.subtotal', 'orders.order_date', 'orders_invoice.id as invoice_id')
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $id])
            ->get();
        }
        return view('backend.staff.c_balance_statement', $data);
    }

    public function printBalanceStatement($id) {
        if(Session::has('store_id')) {
            $shop_id = Session::get('store_id');
        } else {
            $shop = DB::table('stores')->select('id')->first();
            $shop_id = $shop->id;
        }
        
        $data['customer'] = DB::table('users')->where('id', $id)->first();
        $data['shop_details'] = DB::table('stores')->where('id', $shop_id)->first();

        if((isset($_GET['start_date']) && !empty($_GET['start_date'])) && (isset($_GET['end_date']) && !empty($_GET['end_date']))) {
            $pre_balance = $this->preBalanceCalculate($id, $_GET['start_date'], $data['customer']->balance);
            $data['pre_statement'] = $pre_balance;
            $data['customer_order_list'] = DB::table('orders')
            ->join('orders_invoice', 'orders.id', '=', 'orders_invoice.order_id')
            ->select('orders.id', 'orders.subtotal', 'orders.order_date', 'orders_invoice.id as invoice_id')
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $id])
            ->whereBetween('orders.order_date', [$_GET['start_date'], $_GET['end_date']])
            ->get();
            $data['from_date'] = $_GET['start_date'];
            $data['to_date'] = $_GET['end_date'];
        } else {
            $data['pre_statement'] = array('pre_debit' => 0, 'pre_credit' => $data['customer']->balance, 'pre_balance' => $data['customer']->balance);
            $data['customer_order_list'] = DB::table('orders')
            ->join('orders_invoice', 'orders.id', '=', 'orders_invoice.order_id')
            ->select('orders.id', 'orders.subtotal', 'orders.order_date', 'orders_invoice.id as invoice_id')
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $id])
            ->get();
            $data['from_date'] = '';
            $data['to_date'] = '';
        }
        return view('backend.staff.print_balance_statement', $data);
    }

    // public function loadCustomerList(Request $request) {
    //     if(empty($request->id) && empty($request->type)) {
    //         $condition = array();
    //     }
    //     if($request->type == 'division') {
    //         $condition = array('users.division' => $request->id);
    //     }
    //     if($request->type == 'district') {
    //         $condition = array('users.district' => $request->id);
    //     }
    //     if($request->type == 'upazila') {
    //         $condition = array('users.thana' => $request->id);
    //     }

    //     $data['customer_list'] = DB::table('users')
    //     ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
    //     ->select('users.*')
    //     ->where(['shop_customers.shop_id' => $request->shop_id])
    //     ->where($condition)
    //     ->groupBy('shop_customers.user_id')
    //     ->get();
        
    //     return view('backend.staff.load_customer', $data);
    // }

    public function updateCustomerInfo(Request $request) {
        $customer_id = $request->customer_id;
        if(!empty($request->password)) {
            DB::table('users')->where('id', $customer_id)->update([
                'password' => bcrypt(trim($request->password))
            ]);
        }
        DB::table('users')->where('id', $customer_id)->update([
            'email' => trim($request->email),
            'status' => trim($request->status),
        ]);

        return redirect()->back()->with('message', 'Updated Successfully');
    }

    public function orderList()
    {
        if (Session::get('role') == 0) {
            $data['shop_list'] = DB::table('stores')->select('id', 'store_name', 'thana', 'district')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
            $data['customer_list'] = DB::table('users')->select('id', 'name', 'phone')->where('status', 1)->get();
            if(isset($_GET['shop_id']) && empty($_GET['customer_id'])) {
                $data['paginate'] = false;
                $data['order_list'] = DB::table('orders')
                    ->join('orders_invoice', 'orders.id', '=', 'orders_invoice.order_id')
                    ->join('users', 'orders.user_id', '=', 'users.id')
                    ->select('orders.*', 'users.name', 'users.phone')
                    ->where(['orders_invoice.store_id' => $_GET['shop_id'], 'orders.pos' => 0])
                    ->orderBy('orders.id', 'DESC')
                    ->get();
            } else if(isset($_GET['shop_id']) && !empty($_GET['customer_id'])) {
                $data['paginate'] = false;
                $data['order_list'] = DB::table('orders')
                    ->join('orders_invoice', 'orders.id', '=', 'orders_invoice.order_id')
                    ->join('users', 'orders.user_id', '=', 'users.id')
                    ->select('orders.*', 'users.name', 'users.phone')
                    ->where(['orders_invoice.store_id' => $_GET['shop_id'], 'orders.user_id' => $_GET['customer_id'], 'orders.pos' => 0])
                    ->orderBy('orders.id', 'DESC')
                    ->get();
            } else {
                $data['paginate'] = true;
                $data['order_list'] = DB::table('orders')
                    ->join('users', 'orders.user_id', '=', 'users.id')
                    ->select('orders.*', 'users.name', 'users.phone')
                    ->where('orders.pos', 0)
                    ->orderBy('orders.id', 'DESC')
                    ->paginate(50);
            }
            return view('backend.orders.list', $data);
        }
        if (Session::get('role') == 1 && Session::get('admin_type') == 1) {
            $data['customer_list'] = DB::table('shop_customers')
                ->join('users', 'shop_customers.user_id', '=', 'users.id')
                ->select('users.id', 'users.name', 'users.phone')
                ->where(['shop_customers.shop_id' => Session::get('store_id'), 'users.status' => 1])
                ->get();
            if(isset($_GET['from_date']) && isset($_GET['to_date']) && isset($_GET['order_status']) && empty($_GET['customer_id'])) {
                $data['paginate'] = false;
                $data['order_list'] = DB::table('orders_invoice')
                    ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                    ->select('orders_invoice.*', 'users.name', 'users.phone')
                    ->orderBy('orders_invoice.id', 'DESC')
                    ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.invoice_status' => $_GET['order_status'], 'orders_invoice.is_pos' => 0])
                    ->whereBetween('orders_invoice.created_at', [$_GET['from_date'], $_GET['to_date']])
                    ->get();
            } else if(isset($_GET['from_date']) && isset($_GET['to_date']) && isset($_GET['order_status']) && !empty($_GET['customer_id'])) {
                $data['paginate'] = false;
                $data['order_list'] = DB::table('orders_invoice')
                    ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                    ->select('orders_invoice.*', 'users.name', 'users.phone')
                    ->orderBy('orders_invoice.id', 'DESC')
                    ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.user_id' => $_GET['customer_id'], 'orders_invoice.invoice_status' => $_GET['order_status'], 'orders_invoice.is_pos' => 0])
                    ->whereBetween('orders_invoice.created_at', [$_GET['from_date'], $_GET['to_date']])
                    ->get();
            } else {
                $data['paginate'] = true;
                $data['order_list'] = DB::table('orders_invoice')
                    ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                    ->select('orders_invoice.*', 'users.name', 'users.phone')
                    ->orderBy('orders_invoice.id', 'DESC')
                    ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.is_pos' => 0])
                    ->paginate(50);
            }
            return view('backend.orders.shop_orders', $data);
        }
        if (Session::get('role') == 1 && Session::get('admin_type') == 0) {
            $parent_ids = $this->extractParentid(Session::get('admin_id'));
            if(count($parent_ids) == 0) {
                $parent_ids = [Session::get('admin_id')];
            }
            $data['order_list'] = DB::table('orders_invoice')
                ->join('admins', 'orders_invoice.biller_id', '=', 'admins.id')
                ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                ->select('orders_invoice.*', 'users.name', 'users.phone')
                ->orderBy('orders_invoice.id', 'DESC')
                // ->where(['orders_invoice.store_id' => Session::get('store_id'), 'admins.parent_id' => Session::get('admin_id')])
                ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.is_pos' => 0])
                ->whereIn('admins.parent_id', $parent_ids)
                ->paginate(15);
            return view('backend.orders.shop_orders', $data);
        }
    }

    public function posOrderList()
    {
        if (Session::get('role') == 0) {
            $data['shop_list'] = DB::table('stores')->select('id', 'store_name', 'thana', 'district')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
            $data['customer_list'] = DB::table('users')->select('id', 'name', 'phone')->where('status', 1)->get();
            if(isset($_GET['shop_id']) && empty($_GET['customer_id'])) {
                $data['paginate'] = false;
                $data['order_list'] = DB::table('orders')
                    ->join('orders_invoice', 'orders.id', '=', 'orders_invoice.order_id')
                    ->join('users', 'orders.user_id', '=', 'users.id')
                    ->select('orders.*', 'users.name', 'users.phone')
                    ->where(['orders_invoice.store_id' => $_GET['shop_id'], 'orders.pos' => 1])
                    ->orderBy('orders.id', 'DESC')
                    ->get();
            } else if(isset($_GET['shop_id']) && !empty($_GET['customer_id'])) {
                $data['paginate'] = false;
                $data['order_list'] = DB::table('orders')
                    ->join('orders_invoice', 'orders.id', '=', 'orders_invoice.order_id')
                    ->join('users', 'orders.user_id', '=', 'users.id')
                    ->select('orders.*', 'users.name', 'users.phone')
                    ->where(['orders_invoice.store_id' => $_GET['shop_id'], 'orders.user_id' => $_GET['customer_id'], 'orders.pos' => 1])
                    ->orderBy('orders.id', 'DESC')
                    ->get();
            } else {
                $data['paginate'] = true;
                $data['order_list'] = DB::table('orders')
                    ->join('users', 'orders.user_id', '=', 'users.id')
                    ->select('orders.*', 'users.name', 'users.phone')
                    ->where('orders.pos', 1)
                    ->orderBy('orders.id', 'DESC')
                    ->paginate(50);
            }
            return view('backend.orders.pos_list', $data);
        }
        if (Session::get('role') == 1 && Session::get('admin_type') == 1) {
            $data['customer_list'] = DB::table('shop_customers')
                ->join('users', 'shop_customers.user_id', '=', 'users.id')
                ->select('users.id', 'users.name', 'users.phone')
                ->where(['shop_customers.shop_id' => Session::get('store_id'), 'users.status' => 1])
                ->get();
            if(isset($_GET['from_date']) && isset($_GET['to_date']) && isset($_GET['order_status']) && empty($_GET['customer_id'])) {
                $data['paginate'] = false;
                $data['order_list'] = DB::table('orders_invoice')
                    ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                    ->select('orders_invoice.*', 'users.name', 'users.phone')
                    ->orderBy('orders_invoice.id', 'DESC')
                    ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.invoice_status' => $_GET['order_status'], 'orders_invoice.is_pos' => 1])
                    ->whereBetween('orders_invoice.created_at', [$_GET['from_date'], $_GET['to_date']])
                    ->get();
            } else if(isset($_GET['from_date']) && isset($_GET['to_date']) && isset($_GET['order_status']) && !empty($_GET['customer_id'])) {
                $data['paginate'] = false;
                $data['order_list'] = DB::table('orders_invoice')
                    ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                    ->select('orders_invoice.*', 'users.name', 'users.phone')
                    ->orderBy('orders_invoice.id', 'DESC')
                    ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.user_id' => $_GET['customer_id'], 'orders_invoice.invoice_status' => $_GET['order_status'], 'orders_invoice.is_pos' => 1])
                    ->whereBetween('orders_invoice.created_at', [$_GET['from_date'], $_GET['to_date']])
                    ->get();
            } else {
                $data['paginate'] = true;
                $data['order_list'] = DB::table('orders_invoice')
                    ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                    ->select('orders_invoice.*', 'users.name', 'users.phone')
                    ->orderBy('orders_invoice.id', 'DESC')
                    ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.is_pos' => 1])
                    ->paginate(50);
            }
            return view('backend.orders.shop_pos_orders', $data);
        }
        if (Session::get('role') == 1 && Session::get('admin_type') == 0) {
            $parent_ids = $this->extractParentid(Session::get('admin_id'));
            if(count($parent_ids) == 0) {
                $parent_ids = [Session::get('admin_id')];
            }
            $data['order_list'] = DB::table('orders_invoice')
                ->join('admins', 'orders_invoice.biller_id', '=', 'admins.id')
                ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                ->select('orders_invoice.*', 'users.name', 'users.phone')
                ->orderBy('orders_invoice.id', 'DESC')
                // ->where(['orders_invoice.store_id' => Session::get('store_id'), 'admins.parent_id' => Session::get('admin_id')])
                ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.is_pos' => 1])
                ->whereIn('admins.parent_id', $parent_ids)
                ->paginate(15);
            return view('backend.orders.shop_pos_orders', $data);
        }
    }

    public function orderInvoiceList($order_id)
    {
        $data['order_list'] = DB::table('orders_invoice')
            ->join('users', 'orders_invoice.user_id', '=', 'users.id')
            ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
            ->select('orders_invoice.*', 'users.name', 'users.phone', 'stores.store_name', 'stores.slug')
            ->orderBy('orders_invoice.id', 'DESC')
            ->where('orders_invoice.order_id', $order_id)
            ->paginate(15);
        return view('backend.orders.shop_orders', $data);
    }

    public function orderItemList($id)
    {
        if (isset($_GET['type']) && $_GET['type'] == 'invoice') {
            $condition = array('order_items.invoice_id' => $id);
        } else {
            $condition = array('order_items.order_id' => $id);
        }

        $data['order_items'] = DB::table('order_items')
            ->join('orders_invoice', 'order_items.invoice_id', '=', 'orders_invoice.id')
            ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
            ->select('order_items.*', 'stores.store_name', 'stores.slug')
            ->where($condition)
            ->get();
        return view('backend.orders.order_items', $data);
    }

    public function updateOrderStatus(Request $request)
    {
        DB::table('orders_invoice')->where('id', $request->invoice_id)->update([
            'invoice_status' => $request->order_status,
            'modified_by' => Session::get('admin_id')
        ]);
        $condition = array('invoice_id' => $request->invoice_id);

        if ($request->order_status == 3) {
            $get_products = DB::table('order_items')->select('product_id', 'quantity')->where($condition)->get();
            foreach ($get_products as $product) {
                $this->productStockManage($product->product_id, $product->quantity, 'minus');
            }
        }
        if($request->order_status == 2) {
            $this->checkCouponCondition($request->invoice_id);
        }
        $this->changeParentOrderStatus($request->order_id);

        return redirect()->back()->with('message', 'Update Order Status');
    }

    public function viewOrder($id)
    {
        $data['result'] = DB::table('orders')->where('id', $id)->first();
        return view('backend.orders.view_order', $data);
    }

    public function viewInvoice($id)
    {
        $data['result'] = DB::table('orders_invoice')->where('id', $id)->first();
        return view('backend.orders.view_invoice', $data);
    }

    public function product_review_list($type) {
        if($type == 'pending') {
            $condition = array('product_reviews.status' => 0);
        } else {
            $condition = array('product_reviews.status' => 1);
        }
        $data['product_reviews'] = DB::table('product_reviews')
        ->join('users', 'product_reviews.user_id', '=', 'users.id')
        ->join('products', 'product_reviews.product_id', '=', 'products.id')
        ->select('product_reviews.*', 'users.name', 'products.product_name', 'products.product_slug')
        ->where($condition)
        ->get();
        return view('backend.reviews.'.$type, $data);
    }

    public function approve_product_review($id) {
        DB::table('product_reviews')->where('id', $id)->update([
            'status' => 1
        ]);

        return redirect()->back()->with('message', 'Review Approved Successfully');
    }

    public function delete_product_review($id) {
        DB::table('product_reviews')->where('id', $id)->delete();
        return redirect()->back()->with('message', 'Review Deleted Successfully');
    }

    public function authorityList() {
        $data['authority_list'] = DB::table('admins')->where(['role' => 0, 'is_main' => 0])->orderByDesc('id')->get();
        return view('backend.authority.list', $data);
    }

    public function authorityAdd() {
        return view('backend.authority.add');
    }

    public function authoritySave(Request $request) {
        $destinationPath = public_path('uploads/');
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photo_name = 'admin_' . time() . '.' . $photo->getClientOriginalExtension();
            $photo->move($destinationPath, $photo_name);
        } else {
            $photo_name = '';
        }

        DB::table('admins')->insert([
            'name' => trim($request->name),
            'user_name' => trim($request->phone),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'profile_photo' => $photo_name,
            'role' => 0,
            'is_main' => 0,
            'password' => md5(trim($request->password)),
            'thana' => $request->thana,
            'district' => $request->district,
            'division' => $request->division,
            'product_access' => ($request->product_access) ? 1 : 0,
            'shop_settings' => ($request->shop_settings) ? 1 : 0,
            'order_access' => ($request->order_access) ? 1 : 0,
            'service_access' => ($request->service_access) ? 1 : 0,
            'people_access' => ($request->people_access) ? 1 : 0,
            'report_access' => ($request->report_access) ? 1 : 0,
            'coupon_access' => ($request->coupon_access) ? 1 : 0,
            'website_settings_access' => ($request->website_settings_access) ? 1 : 0,
            'product_review_access' => ($request->product_review_access) ? 1 : 0,
        ]);

        return redirect('admin/authority/list')->with('message', 'Authority Added Successfull');
    }

    public function authorityEdit($id) {
        $data['result'] = DB::table('admins')->where('id', $id)->first();
        return view('backend.authority.edit', $data);
    }

    public function authorityUpdate(Request $request) {
        $destinationPath = public_path('uploads/');
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photo_name = 'admin_' . time() . '.' . $photo->getClientOriginalExtension();
            $photo->move($destinationPath, $photo_name);
        }else {
            $photo_name = $request->existing_image;
        }

        DB::table('admins')->where('id', $request->authority_id)->update([
            'name' => trim($request->name),
            'user_name' => trim($request->phone),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'profile_photo' => $photo_name,
            'thana' => $request->thana,
            'district' => $request->district,
            'division' => $request->division,
            'product_access' => ($request->product_access) ? 1 : 0,
            'shop_settings' => ($request->shop_settings) ? 1 : 0,
            'order_access' => ($request->order_access) ? 1 : 0,
            'service_access' => ($request->service_access) ? 1 : 0,
            'people_access' => ($request->people_access) ? 1 : 0,
            'report_access' => ($request->report_access) ? 1 : 0,
            'coupon_access' => ($request->coupon_access) ? 1 : 0,
            'website_settings_access' => ($request->website_settings_access) ? 1 : 0,
            'product_review_access' => ($request->product_review_access) ? 1 : 0,
        ]);

        if(!empty($request->password)) {
            DB::table('admins')->where('id', $request->authority_id)->update([
                'password' => md5(trim($request->password))
            ]); 
        }

        return redirect('admin/authority/list')->with('message', 'Authority Updated Successfull');
    }

    public function authorityDelete($id) {
        DB::table('admins')->where('id', $id)->delete();
        return redirect('admin/authority/list')->with('message', 'Authority Deleted Successfull');
    }

    public function addCustomer() {
        $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->get();
        return view('backend.customer.add', $data);
    }

    public function customerDataExport() {
        return Excel::download(new UsersExport, 'customer_numbers.xlsx');
    }

    public function saveCustomer(Request $request) {
        $request->validate([
            'name' => 'required|string',
            'phone' => 'required|numeric|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'gender' => 'required|numeric',
            'address' => 'required|string',
            'upazila' => 'required',
            'district' => 'required',
            'division' => 'required',
        ]);

        DB::table('users')->insert([
            'reference_id' => $request->reference_id,
            'name' => trim($request->name),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'password' => bcrypt($request->password),
            'gender' => $request->gender,
            'address' => trim($request->address),
            'thana' => $request->upazila,
            'district' => $request->district,
            'division' => $request->division,
        ]);
        $customer_id = DB::getPdo()->lastInsertId();
        //customer pre due amount
        if($request->due_amount > 0) {
            DB::table('customer_shop_balance')->insert([
                'user_id' => $customer_id,
                'shop_id' => $request->store_id,
                'current_balance' => '-'.$request->due_amount,
            ]);
        }
        //registerd shop customer
        $this->shopCustomerRegistration($customer_id, $request->store_id);

        return redirect('admin/customer-list')->with('message', 'Successfully Done!');
    }

    public function updateCustomer(Request $request) {
        DB::table('users')->where('id', $request->customer_id)->update([
            'name' => trim($request->name),
            'email' => trim($request->email),
            'address' => trim($request->address),
            'thana' => $request->upazila,
            'district' => $request->district,
            'division' => $request->division,
        ]);
        return redirect('admin/customer-list')->with('message', 'Successfully Updated!');
    }

    public function dateExpireShopList() {
        $data['shop_list'] = DB::table('stores')->whereDate('expiry_date', '<=', date('Y-m-d'))->get();
        return view('backend.store.date_expire_shop', $data);
    }

    public function inactiveShopList() {
        $data['shop_list'] = DB::table('stores')->where('status', 0)->get();
        return view('backend.store.inactive_shop', $data);
    }

    public function confirmShopActive($id) {
        DB::table('stores')->where('id', $id)->update(['status' => 1]);
        return redirect()->back()->with('message', 'Shop Active Successfully');
    }

    public function make_slug($string)
    {
        $string = str_replace('/', ' ', $string);
        $slug = preg_replace('/\s+/u', '-', trim($string));
        return strtolower($slug);

    }

    public function check_edit_permission($table, $key, $id)
    {
        $store = DB::table($table)->select('store_id')->where($key, $id)->first();
        if (Session::get('role') == 0) {
            return true;
        } else if (Session::get('store_id') == $store->store_id) {
            return true;
        } else {
            return false;
        }
    }

}