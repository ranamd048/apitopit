<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    public function index() {
        $data['result'] = DB::table('users')->where(['store_id' => Session::get('store_id'), 'user_type' => 'supplier'])->get();
		return view('backend.supplier.list', $data);
	}

	public function add() {
		return view('backend.supplier.add');
	}

	public function save(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'phone' => 'required|unique:users|numeric'
		]);
		
	    DB::table('users')->insert([
			'store_id' => Session::get('store_id'),
			'user_type' => 'supplier',
	    	'name' => trim($request->name),
	    	'email' => trim($request->email),
	    	'phone' => trim($request->phone),
	    	'division' => $request->division,
	    	'district' => $request->district,
	    	'thana' => $request->upazila,
	    	'address' => trim($request->address),
	    ]);

	    return redirect()->back()->with('message', 'Supplier Added Successfully');
	}

	public function edit($id) {
		$data['result'] = DB::table('users')->where('id', $id)->first();
		return view('backend.supplier.edit', $data);
	}

	public function details($user_id) {
        $shop_id = Session::get('store_id');
        $customer = DB::table('users')->select('id', 'name', 'email', 'phone', 'address', 'thana', 'district', 'division', 'photo')->where(['id' => $user_id, 'user_type' => 'supplier'])->first();
		
		$customer_balance = DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $shop_id, 'type' => 'supplier'])->first();
		if($customer_balance) {
			$customer->current_balance = $customer_balance->current_balance;
			$customer->shop_id = $customer_balance->shop_id;
		} else {
			$customer->current_balance = 0;
			$customer->shop_id = $shop_id;
		}

        $payment_summary = DB::table('purchases')
            ->select(DB::raw("SUM(purchases.due_amount) as total_due_amount"), DB::raw("SUM(purchases.paid_amount) as total_paid_amount"))
            ->where(['purchases.store_id' => $shop_id, 'purchases.supplier_id' => $user_id])
            ->first();
          
		if($payment_summary) {
			$customer->total_due_amount = $payment_summary->total_due_amount ? $payment_summary->total_due_amount : 0;
			$customer->total_paid_amount = $payment_summary->total_paid_amount ? $payment_summary->total_paid_amount : 0;
		} else {
			$customer->total_due_amount = 0;
			$customer->total_paid_amount = 0;
		}
        //echo "<pre>"; print_r($customer); exit();
        $data['result'] = $customer;
        return view('backend.supplier.details', $data);
    }

	public function update(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'phone' => 'required|numeric'
		]);
		
	    DB::table('users')->where('id', $request->supplier_id)->update([
	    	'name' => trim($request->name),
	    	'email' => trim($request->email),
	    	'phone' => trim($request->phone),
	    	'division' => $request->division,
	    	'district' => $request->district,
	    	'thana' => $request->upazila,
	    	'address' => trim($request->address),
	    ]);

	    return redirect()->back()->with('message', 'Supplier Updated Successfully');
	}

	public function payNow() {
        $data['due_amount'] = $_GET['due'];
        $data['user_id'] = $_GET['user_id'];
        return view('backend.supplier.pay_now', $data); 
    }

	public function savePayment(Request $request) {
        $shop_id = Session::get('store_id');
        $user_id = $request->user_id;
        $total_amount = trim($request->total_amount);
        $payment_method = $request->payment_method;
        $payment_note = trim($request->payment_note);
        $user_balance = DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $shop_id, 'type' => 'supplier'])->first();
        if($user_balance) {
            $available_amount = ($total_amount + $user_balance->current_balance);
        } else {
            $available_amount = $total_amount;
        }

        $user_due_purchases = DB::table('purchases')->select('id', 'supplier_id', 'due_amount')->where(['store_id' => $shop_id, 'supplier_id' => $user_id, 'payment_status' => 0])->get();
        if(count($user_due_purchases) > 0) {
            foreach($user_due_purchases as $due_order) {
                $invoice_due = $due_order->due_amount;
                $invoice_id = $due_order->id;
                if($available_amount > 0) {
                    if($available_amount >= $invoice_due) {
                        $paid_amount = $invoice_due;
                        $due_amount = 0;
                        $available_amount = ($available_amount - $invoice_due);
                    } else {
                        $paid_amount = $available_amount;
                        $due_amount = ($invoice_due - $available_amount);
                        $available_amount = 0;
                    }
                    DB::table('purchase_payments')->insert([
                        'store_id' => $shop_id,
                        'purchase_id' => $invoice_id,
                        'supplier_id' => $user_id,
                        'paid_amount' => $paid_amount,
                        'payment_method' => $payment_method,
                        'payment_note' => $payment_note
                    ]);
                    //current paid amount
                    $already_paid = DB::table('purchases')->select('paid_amount')->where('id', $invoice_id)->first();
                    DB::table('purchases')->where('id', $invoice_id)->update([
                        'paid_amount' => ($already_paid->paid_amount + $paid_amount),
                        'due_amount' => $due_amount,
                        'payment_status' => ($due_amount == 0) ? 1 : 0,
                    ]);
                }
            }
        }
        //save available amount on payment table
        if($available_amount > 0 && count($user_due_purchases) > 0) {
            DB::table('purchase_payments')->insert([
                'store_id' => $shop_id,
                'supplier_id' => $user_id,
                'paid_amount' => $available_amount,
                'payment_method' => $payment_method,
                'payment_note' => $payment_note
            ]);
        }
        if(count($user_due_purchases) == 0) {
            DB::table('purchase_payments')->insert([
                'store_id' => $shop_id,
                'supplier_id' => $user_id,
                'paid_amount' => $total_amount,
                'payment_method' => $payment_method,
                'payment_note' => $payment_note
            ]);
        }

        //update customer balance
        if($user_balance) {
            DB::table('customer_shop_balance')->where(['user_id' => $user_id, 'shop_id' => $shop_id, 'type' => 'supplier'])->update(['current_balance' => $available_amount]);
        } else {
            DB::table('customer_shop_balance')->insert(['user_id' => $user_id, 'shop_id' => $shop_id,'current_balance' => $available_amount, 'type' => 'supplier']);
        }
        return redirect()->back()->with('message', 'Payment Successfully Completed');
    }

	public function paymentHistory($id) {
        $shop_id = Session::get('store_id');
        $data['supplier'] = DB::table('users')->where(['id' => $id, 'user_type' => 'supplier'])->first();
        $data['payment_summary'] = DB::table('purchases')
            ->select(DB::raw("SUM(purchases.due_amount) as total_due_amount"), DB::raw("SUM(purchases.paid_amount) as total_paid_amount"))
            ->where(['purchases.store_id' => $shop_id, 'purchases.supplier_id' => $id])
            ->first();
		$user_balance = DB::table('customer_shop_balance')->where(['user_id' => $id, 'shop_id' => $shop_id, 'type' => 'supplier'])->first();
		if($user_balance) {
            $data['advance_paid'] = $user_balance->current_balance;
        } else {
            $data['advance_paid'] = 0;
        }

        $data['payment_history'] = DB::table('purchase_payments')->where(['store_id' => $shop_id, 'supplier_id' => $id])->orderByDesc('id')->get();
        return view('backend.supplier.payment_history', $data);
	}

	public function delete($id) {
		$check_permission = $this->check_access_permission('users', 'id', $id);
		if(!$check_permission) {
			return redirect('admin/supplier-list')->with('error', "You don't have permission to delete this item.");
		}
		DB::table('users')->where('id', $id)->delete();
		return redirect()->back()->with('message', 'Supplier Deleted Successfully');
	}

	private function check_access_permission($table, $key, $id) {
		$store = DB::table($table)->select('store_id')->where($key, $id)->first();
		if($store) {
			if(Session::get('role') == 0) {
        		return true;
	        } else if(Session::get('store_id') == $store->store_id) {
	        	return true;
	        }
		}
		return false;
	}


}
