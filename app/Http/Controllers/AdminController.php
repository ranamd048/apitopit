<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\FrontendModel\SiteInformation;
use App\FrontendModel\Page;
use App\FrontendModel\Menu;
use App\FrontendModel\Slider;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller {

	use HelperTrait;

	public function dashboard(Request $request){
		if(isset($_GET['store_id']) && !empty($request->store_id)) {
			Session::put('store_id', $request->store_id);
			$store = DB::table('stores')->where('id', $request->store_id)->select('slug', 'shop_type')->first();
				Session::put('store_type', $store->shop_type);
				Session::put('store_upload_directory', $store->slug);
			\Cart::clear();
		}
		if(isset($_GET['store_id']) && empty($request->store_id)) {
			Session::forget('store_id');
			Session::forget('store_type');
			Session::forget('store_upload_directory');
		}

		$data = [];
		$data['store_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->get();
		return view('backend.dashboard', $data);
	}

	public function report_summary() {
		if(Session::has('store_id') && (Session::get('store_type') == 'both' || Session::get('store_type') == 'shop')) {
			if(isset($_GET['from_date']) && isset($_GET['to_date'])) {
				$data['report_summary'] = $this->date_range_summary_report(Session::get('store_id'), $_GET['from_date'], $_GET['to_date']);
				$data['report_summary_title'] = 'From '.date("d-M-Y", strtotime($_GET['from_date'])).' To '.date("d-M-Y", strtotime($_GET['to_date']));
			} else {
				$data['report_summary'] = $this->today_summary_report(Session::get('store_id'));
				$data['report_summary_title'] = 'Today Report Summary';
			}
			$data['shop_report'] = $this->all_time_shop_report(Session::get('store_id'));
			return view('backend.report_summary', $data);
		} else {
			return redirect('admin/dashboard');
		}
	}

	public function adminProfile() {
		$data['result'] = DB::table('admins')->where('id', Session::get('admin_id'))->first();
		return view('backend.profile', $data);
	}

	public function change_password(){
		return view('backend.change_password');
	}

	public function update_profile(Request $request) {
		$this->validate($request, [
	        'profile_photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
	    ]);

	    $destinationPath = public_path('uploads/');

	    if ($request->hasFile('profile_photo')) {
	    	if(!empty($request->existing_profile)) {
	    		if(file_exists(public_path('/uploads/'.$request->existing_profile))) {
	    			unlink(public_path('/uploads/'.$request->existing_profile));
	    		}
	    	}

	        $profile_photo = $request->file('profile_photo');
	        $profile_photo_name = 'profile_photo'.time().'.'.$profile_photo->getClientOriginalExtension();
	        $profile_photo->move($destinationPath, $profile_photo_name);
	    } else {
	    	$profile_photo_name = $request->existing_profile;
		}

		DB::table('admins')->where('id', Session::get('admin_id'))->update([
			'name' => $request->name,
			'email' => $request->email,
			'profile_photo' => $profile_photo_name,
		]);

		return redirect()->back()->with('success', 'Updated Successfully');
	}

	public function update_password(Request $request){
		$old_password = trim($request->old_password);
		$new_password = trim($request->new_password);
		$confirm_password = trim($request->confirm_password);

		$check_password = DB::table('admins')->where(['password' => md5($old_password), 'id' => Session::get('admin_id')])->first();
		if($check_password) {
			if($new_password == $confirm_password) {
				DB::table('admins')->where('id', Session::get('admin_id'))->update(['password' => md5($new_password)]);
				return redirect()->back()->with('success', 'Password Updated Successfully');
			} else {
				return redirect()->back()->with('error', 'New Password And Confirm Password Not Match');
			}
		} else {
			return redirect()->back()->with('error', 'Old Password Not Match');
		}
	}

	public function logout(){
		Session::flush();
		return redirect('/login');
	}

	public function index(){
		$data = [];
		$data['theme_settings'] = SiteInformation::find(1);
		return view('backend.settings', $data);
	}

	public function updateSiteInformation(Request $request){
		$this->validate($request, [
	        'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	    ]);

	    $destinationPath = public_path('uploads/');

	    if ($request->hasFile('logo')) {
	    	if(!empty($request->existing_logo)) {
	    		if(file_exists(public_path('/uploads/'.$request->existing_logo))) {
	    			unlink(public_path('/uploads/'.$request->existing_logo));
	    		}
	    	}

	        $logo = $request->file('logo');
	        $logo_name = 'logo'.time().'.'.$logo->getClientOriginalExtension();
	        $logo->move($destinationPath, $logo_name);
	    } else {
	    	$logo_name = $request->existing_logo;
	    }

        if ($request->hasFile('favicon')) {
            $favicon = $request->file('favicon');
            $favicon_name = 'favicon'.time().'.'.$favicon->getClientOriginalExtension();
            $favicon->move($destinationPath, $favicon_name);
        } else {
            $favicon_name = $request->existing_favicon;
        }

        if ($request->hasFile('banner')) {
            if(!empty($request->existing_banner)) {
                if(file_exists(public_path('/uploads/'.$request->existing_banner))) {
                    unlink(public_path('/uploads/'.$request->existing_banner));
                }
            }

            $banner = $request->file('banner');
            $banner_name = 'banner_'.time().'.'.$banner->getClientOriginalExtension();
            $banner->move($destinationPath, $banner_name);
        } else {
            $banner_name = $request->existing_banner;
        }

        if ($request->hasFile('about_img')) {
            if(!empty($request->existing_about_img)) {
                if(file_exists(public_path('/uploads/'.$request->existing_about_img))) {
                    unlink(public_path('/uploads/'.$request->existing_about_img));
                }
            }

            $about_img = $request->file('about_img');
            $about_img_name = 'about_img'.time().'.'.$about_img->getClientOriginalExtension();
            $about_img->move($destinationPath, $about_img_name);
        } else {
            $about_img_name = $request->existing_about_img;
        }

		$site_info = SiteInformation::find($request->settings_id);
		$site_info->web_title = trim($request->web_title);
		$site_info->address = trim($request->address);
		$site_info->email = trim($request->email);
		$site_info->phone_number = trim($request->phone_number);
		$site_info->fb_link = trim($request->fb_link);
		$site_info->twitter_link = trim($request->twitter_link);
		$site_info->googlePlus_link = trim($request->googlePlus_link);
		$site_info->youtube_link = trim($request->youtube_link);
		$site_info->linkedin_link = trim($request->linkedin_link);
		$site_info->instagram_link = trim($request->instagram_link);
		$site_info->maps_iframe = trim($request->maps_iframe);
		$site_info->logo = $logo_name;
		$site_info->favicon = $favicon_name;
		$site_info->banner = $banner_name;
		$site_info->theme_color = trim($request->theme_color);
		$site_info->about_title = trim($request->about_title);
        $site_info->about_img = $about_img_name;
		$site_info->about_desc = trim($request->about_desc);
		$site_info->active_tab = $request->active_tab;
		$site_info->copyright_text = trim($request->copyright_text);
		$site_info->save();

		return redirect()->back()->with('message', 'Updated Successfully');
	}

	public function shopAdvertisement() {
		$data['shop_advertisements'] = DB::table('shop_advertisements')->orderBy('sort_order', 'DESC')->get();
		return view('backend.shop_advertisement', $data);
	}

	public function addAdvertisement() {
		return view('backend.add_advertisement');
	}

	public function saveAdvertisement(Request $request) {
		$this->validate($request, [
	        'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	    ]);

	    $destinationPath = public_path('uploads/');

	    if ($request->hasFile('image')) {
	        $image = $request->file('image');
	        $image_name = 'advertisement_'.time().'.'.$image->getClientOriginalExtension();
	        $image->move($destinationPath, $image_name);
	    } else {
	    	$image_name = '';
	    }

		DB::table('shop_advertisements')->insert([
			'shop_link' => trim($request->shop_link),
			'image_url' => $image_name,
			'sort_order' => trim($request->sort_order),
			'status' => trim($request->status),
		]);

		return redirect('admin/shop_advertisement')->with('message', 'Added Successfully');
	}

	public function editAdvertisement($id) {
		$data['result'] = DB::table('shop_advertisements')->where('id', $id)->first();
		return view('backend.edit_advertisement', $data);
	}

	public function updateAdvertisement(Request $request) {
		$this->validate($request, [
	        'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	    ]);

	    $destinationPath = public_path('uploads/');
		if ($request->hasFile('image')) {
			if(!empty($request->existing_image)) {
	    		if(file_exists(public_path('/uploads/'.$request->existing_image))) {
	    			unlink(public_path('/uploads/'.$request->existing_image));
	    		}
			}
	        $image = $request->file('image');
	        $image_name = 'advertisement_'.time().'.'.$image->getClientOriginalExtension();
	        $image->move($destinationPath, $image_name);
	    } else {
	    	$image_name = $request->existing_image;
		}

		DB::table('shop_advertisements')->where('id', $request->advertisement_id)->update([
			'shop_link' => trim($request->shop_link),
			'image_url' => $image_name,
			'sort_order' => trim($request->sort_order),
			'status' => trim($request->status),
		]);

		return redirect('admin/shop_advertisement')->with('message', 'Updated Successfully');

	}

	public function deleteAdvertisement($id) {
		DB::table('shop_advertisements')->where('id', $id)->delete();
		return redirect('admin/shop_advertisement')->with('message', 'Deleted Successfully');
	}


	public function shop_settings() {
		$store_id = Session::get('store_id');
		$data = [];
		$data['store'] = DB::table('stores')->where('id', $store_id)->first();
		$data['shop_categories'] = DB::table('shop_category')->orderBy('name', 'ASC')->get();
		return view('backend.store.edit', $data);
	}

	public function send_offer_sms(Request $request) {
		$product_name = $request->product_name;
		$offer_price = trim($request->offer_price);
		$offer_text = trim($request->offer_text);
		$message_body = $offer_text.'. '.$product_name. ', '.$offer_price;

		$shop_customers = DB::table('users')
        ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
        ->select('users.id', 'users.phone')
        ->where('shop_customers.shop_id', Session::get('store_id'))
        ->groupBy('shop_customers.user_id')
		->get();

		if(count($shop_customers) > 0) {
			$phone_numbers = array();
			foreach($shop_customers as $row) {
				array_push($phone_numbers, $row->phone);
			}
			$this->sendSms($phone_numbers, $message_body);
			return redirect()->back()->with('message', 'Offer SMS Send Successfully');
		} else {
			return redirect()->back()->with('message', 'Not Found Customers');
		}
	}

	public function addNewBrand() {
		if(Session::get('role') != 0) {
            return redirect('/admin/brand-list');
        }
		return view('backend.brand.add');
	}

	public function saveBrand(Request $request) {
		$this->validate($request, [
			'name' => 'required',
	        'image' => 'image|mimes:jpeg,png,jpg'
	    ]);

		$destinationPath = public_path('uploads/brands/');
	    if ($request->hasFile('image')) {
	        $image = $request->file('image');
	        $image_name = 'brand_image'.time().'.'.$image->getClientOriginalExtension();
	        $image->move($destinationPath, $image_name);
	    } else {
	    	$image_name = '';
	    }

	    DB::table('brands')->insert([
	    	'name' => trim($request->name),
	    	'slug' => '',
	    	'image' => $image_name,
	    	'sort_order' => $request->sort_order,
	    	'status' => $request->status,
	    ]);

        $brand_id = DB::getPdo()->lastInsertId();
        //add slug
        $slug = $this->makeUniqueSlug('brands', $brand_id, 'slug', $request->name);
        DB::table('brands')->where('id', $brand_id)->update([
            'slug' => $slug
        ]);

	    return redirect('admin/brand-list')->with('message', 'Created Successfully');
	}

	public function brandList() {
		$data['brand_list'] = DB::table('brands')->orderBy('sort_order', 'DESC')->get();
		return view('backend.brand.list', $data);
	}

	public function editBrand($id) {
		if(Session::get('role') != 0) {
            return redirect('/admin/brand-list');
		}

		$data['brand'] = DB::table('brands')->where('id', $id)->first();
		return view('backend.brand.edit', $data);
	}

	public function updateBrand(Request $request) {
		$this->validate($request, [
			'name' => 'required',
	        'image' => 'image|mimes:jpeg,png,jpg'
	    ]);

		$destinationPath = public_path('uploads/brands/');
	    if ($request->hasFile('image')) {
	    	if(!empty($request->existing_img)) {
	    		if(file_exists(public_path('/uploads/brands/'.$request->existing_img))) {
	    			unlink(public_path('/uploads/brands/'.$request->existing_img));
	    		}
	    	}
	        $image = $request->file('image');
	        $image_name = 'brand_image'.time().'.'.$image->getClientOriginalExtension();
	        $image->move($destinationPath, $image_name);
	    } else {
	    	$image_name = $request->existing_img;
	    }

        $slug = $this->makeUniqueSlug('brands', $request->brand_id, 'slug', $request->name, 'update');
	    DB::table('brands')->where('id', $request->brand_id)->update([
	    	'name' => trim($request->name),
	    	'slug' => $slug,
	    	'image' => $image_name,
			'sort_order' => $request->sort_order,
	    	'status' => $request->status,
		]);

		return redirect('admin/brand-list')->with('message', 'Updated Successfully');
	}

	public function createNewSlider(){
		return view('backend.slider.create_slider');
	}

	public function saveSliderData(Request $request){
		$this->validate($request, [
	        'slider_image' => 'required|image|mimes:jpeg,png,jpg|max:1024'
	    ]);
		$destinationPath = public_path('uploads/');
	    if ($request->hasFile('slider_image')) {
	        $slider_image = $request->file('slider_image');
	        $slider_image_name = 'slider_image'.time().'.'.$slider_image->getClientOriginalExtension();
	        $slider_image->move($destinationPath, $slider_image_name);
	    } else {
	    	$slider_image_name = '';
	    }

	    $slider_model = new Slider();
	    $slider_model->slider_title = trim($request->slider_title);
	    $slider_model->slider_image = $slider_image_name;
	    $slider_model->sort_order = $request->sort_order;
	    $slider_model->status = $request->status;
	    $slider_model->save();
	    return redirect('admin/slider-list')->with('message', 'Created Successfully');
	}

	public function sliderList(){
		$data = [];
		$data['slider_list'] = Slider::orderBY('sort_order', 'ASC')->get();
		return view('backend.slider.slider_list', $data);
	}

	public function sliderEditForm($id){
		$data = [];
		$data['slider_data'] = Slider::find($id);
		return view('backend.slider.edit_slider', $data);
	}

	public function updateSliderData(Request $request){
		$this->validate($request, [
	        'slider_image' => 'image|mimes:jpeg,png,jpg|max:1024'
	    ]);

	    $destinationPath = public_path('uploads/');
	    if ($request->hasFile('slider_image')) {
	    	if(!empty($request->existing_slider_img)) {
	    		if(file_exists(public_path('/uploads/'.$request->existing_slider_img))) {
	    			unlink(public_path('/uploads/'.$request->existing_slider_img));
	    		}
	    	}

	        $slider_image = $request->file('slider_image');
	        $slider_image_name = 'slider_image'.time().'.'.$slider_image->getClientOriginalExtension();
	        $slider_image->move($destinationPath, $slider_image_name);
	    } else {
	    	$slider_image_name = $request->existing_slider_img;
	    }

	    $slider_model = Slider::find($request->slider_id);
	    $slider_model->slider_title = trim($request->slider_title);
	    $slider_model->slider_image = $slider_image_name;
	    $slider_model->sort_order = $request->sort_order;
	    $slider_model->status = $request->status;
	    $slider_model->save();
	    return redirect('/admin/slider-list')->with('message', 'Updated Successfully');
	}

	public function deleteSliderData($id) {
		DB::table('sliders')->where('id', $id)->delete();
		return redirect()->back()->with('message', 'Deleted Successfully');
	}

	public function createNewPage(){
		return view('backend.create_page');
	}

	public function pageDataSave(Request $request){
		$slug = preg_replace('/\s+/u', '-', trim($request->page_title));
		$slug = strtolower($slug);
		$check_slug = Page::where('slug', $slug)->get();
		if(count($check_slug) > 0) {
			$slug = $slug.'-'.time();
		}
		$page_model = new Page();
		$page_model->title = trim($request->page_title);
		$page_model->page_type = trim($request->page_type);
		$page_model->page_template = trim($request->page_template);
		$page_model->page_content = trim($request->page_content);
		$page_model->slug = $slug;
		$page_model->status = $request->status;
		$page_model->save();
 		return redirect()->back()->with('message', 'Page Created Successfully');
	}

	public function pageList(){
		$data = [];
		$data['page_list'] = Page::all();
		return view('backend.page_list', $data);
	}

	public function pageEditForm($id){
		$data = [];
		$data['page_data'] = Page::find($id);
		return view('backend.edit_page', $data);
	}

	public function pageDataUpdate(Request $request){
		$page_id = $request->page_id;
		$page_model = Page::find($page_id);
		$page_model->title = trim($request->page_title);
		$page_model->page_type = trim($request->page_type);
		$page_model->page_template = trim($request->page_template);
		$page_model->page_content = trim($request->page_content);
		$page_model->status = $request->status;
		$page_model->save();
		return redirect('admin/page-list')->with('message', 'Updated Successfully');
	}

	public function deletePage($id){
		DB::table('page')->where('id', $id)->delete();
		return redirect()->back()->with('message', 'Deleted Successfully');
	}

	public function createNewMenu(){
		$data = [];
		$data['page_list'] = Page::where(['status' => 1])->get();
		$data['menu_list'] = Menu::where(array('status'=>1, 'parent_id'=>0))->get();
		return view('backend.create_menu', $data);
	}

	public function saveMenuData(Request $request){
		if($request->menu_type == 'page'){
			$menu_url = trim($request->page_link);
			$is_custom = 0;
		} else {
			$menu_url = trim($request->custom_link);
			$is_custom = 1;
		}

		$menu_model = new Menu();
		$menu_model->parent_id = $request->parent_id;
		$menu_model->menu_url = $menu_url;
		$menu_model->status = $request->status;
		$menu_model->sort_order = $request->sort_order;
		$menu_model->name = trim($request->name);
		$menu_model->is_custom = $is_custom;
		$menu_model->save();
		return redirect('/admin/menu-list')->with('message', 'Menu Created Successfully');
	}

	public function menuList(){
		$data = [];
		$data['menu_list'] = Menu::orderBY('sort_order', 'ASC')->get();
		return view('backend.menu_list', $data);
	}

	public function menuEditForm($id){
		$data = [];
		$data['page_list'] =Page::where(['status' => 1])->get();
		$data['menu_list'] = Menu::where(array('status'=>1, 'parent_id'=>0))->get();
		$data['menu_data'] = Menu::find($id);
		return view('backend.edit_menu', $data);
	}

	public function updateMenuData(Request $request){
		if($request->menu_type == 'page'){
			$menu_url = trim($request->page_link);
			$is_custom = 0;
		} else {
			$menu_url = trim($request->custom_link);
			$is_custom = 1;
		}

		$menu_model = Menu::find($request->menu_id);
		$menu_model->parent_id = $request->parent_id;
		$menu_model->menu_url = $menu_url;
		$menu_model->status = $request->status;
		$menu_model->sort_order = $request->sort_order;
		$menu_model->name = trim($request->name);
		$menu_model->is_custom = $is_custom;
		$menu_model->save();
		return redirect('/admin/menu-list')->with('message', 'Updated Successfully');
	}

	public function deleteMenu($id){
		DB::table('menu')->where('id', $id)->delete();
		return redirect()->back()->with('message', 'Deleted Successfully');
	}

    public function createNewNews()
    {
        $data = [];
        return view('backend.blog.add');
    }

    public function saveNewsData(Request $request)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg'
        ]);

        $destinationPath = public_path('uploads/blog/');

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = 'blog_image' . time() . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
        } else {
            $image_name = '';
        }
        DB::table('blogs')->insert([
            'type' => $request->type,
            'title' => trim($request->title),
            'created_by' => Session::get('admin_name'),
            'image_url' => $image_name,
            'description' => trim($request->news_description)
        ]);
        return redirect('/admin/blog-list')->with('message', 'Created Successfully');

    }

    public function newsList()
    {
        $data = [];
        $data['news_list'] = DB::table('blogs')->orderBy('id', 'DESC')->get();
        return view('backend.blog.list', $data);
    }

    public function newsEditForm($id)
    {
        $data = [];
        $data['news'] = DB::table('blogs')->where('id', $id)->first();
        return view('backend.blog.edit', $data);
    }

    public function updateNewsData(Request $request)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg'
        ]);

        $destinationPath = public_path('uploads/blog/');
        if ($request->hasFile('image')) {
            if (!empty($request->news_image)) {
                if (file_exists(public_path('/uploads/blog/' . $request->news_image))) {
                    unlink(public_path('/uploads/blog/' . $request->news_image));
                }
            }

            $image = $request->file('image');
            $image_name = 'blog_image' . time() . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
        } else {
            $image_name = $request->news_image;
        }

        DB::table('blogs')->where('id', $request->news_id)->update([
            'type' => $request->type,
            'title' => trim($request->title),
            'created_by' => Session::get('admin_name'),
            'image_url' => $image_name,
            'description' => trim($request->news_description)
        ]);

        return redirect('/admin/blog-list')->with('message', 'Updated Successfully');
    }

    public function deleteNewsData($id)
    {
        DB::table('blogs')->where('id', $id)->delete();
        return redirect('/admin/blog-list')->with('message', 'Deleted Successfully');
    }

    public function galleryTypesList()
    {
        $data = [];
        $data['gallery_types'] = DB::table('gallery_types')->select('*')->get();
        return view('backend.gallery.types_list', $data);
    }

    public function addGalleryType()
    {
        return view('backend.gallery.add_type');
    }

    public function saveGalleryType(Request $request)
    {
        DB::table('gallery_types')->insert(
            ['type_title' => trim($request->type_title)]
        );
        return redirect('/admin/gallery-types')->with('message', 'Created Successfully');
    }

    public function deleteGalleryType($id)
    {
        DB::table('gallery_types')->where('types_id', $id)->delete();
        DB::table('gallery_type_relation')->where('gallery_type_id', $id)->delete();
        return redirect('/admin/gallery-types')->with('message', 'Deleted Successfully');
    }

    public function createNewGallery()
    {
        $data = [];
        $data['gallery_types'] = DB::table('gallery_types')->select('*')->get();
        return view('backend.gallery.create_gallery', $data);
    }

    public function saveGalleryImage(Request $request)
    {
        $this->validate($request, [
            'gallery_image' => 'image|mimes:jpeg,png,jpg'
        ]);
        $gallery_type = $request->gallery_type;
        if(!is_array($gallery_type)) {
            return redirect()->back()->with('message', 'Please Select Type');
        }

        $destinationPath = public_path('uploads/gallery/');
        if ($request->hasFile('gallery_image')) {
            $gallery_image = $request->file('gallery_image');
            $gallery_image_name = 'gallery_image' . time() . '.' . $gallery_image->getClientOriginalExtension();
            $gallery_image->move($destinationPath, $gallery_image_name);
        } else {
            $gallery_image_name = '';
        }

        DB::table('gallery')->insert([
            'gallery_image' => $gallery_image_name,
        ]);

        $gallery_id = DB::getPdo()->lastInsertId();

        for ($i = 0; $i < count($gallery_type); $i++) {
            DB::table('gallery_type_relation')->insert(
                ['gallery_id' => $gallery_id, 'gallery_type_id' => $gallery_type[$i]]
            );
        }

        return redirect('/admin/gallery-list')->with('message', 'Created Successfully');
    }

    public function galleryList()
    {
        $data = [];
        $data['gallery_list'] = DB::table('gallery')->get();
        return view('backend.gallery.gallery_list', $data);
    }

    public function deleteGalleryImage($id)
    {
        DB::table('gallery')->where('id', $id)->delete();
        DB::table('gallery_type_relation')->where('gallery_id', $id)->delete();
        return redirect()->back()->with('message', 'Deleted Successfully');
    }

    public function uploadDocuments($table, $id) {
	    $data['document_list'] = DB::table('documents')
            ->join('admins', 'documents.uploaded_by', '=', 'admins.id')
            ->select('documents.*', 'admins.name')
            ->where('documents.row_id', $id)
            ->orderBy('documents.sort_order', 'ASC')
            ->get();

	    $data['table'] = $table;
	    $data['row_id'] = $id;
        return view('backend.upload_documents', $data);
    }

    public function saveDocumentItems(Request $request) {
	    $table_name = $request->table_name;
	    $row_id = $request->row_id;
	    $doc_numbers = $request->doc_number;

	    if(is_array($doc_numbers)) {
	        foreach($doc_numbers as $number) {
                $destinationPath = public_path('uploads/documents/');
                if ($request->hasFile('doc_file_'.$number)) {
                    $doc = $request->file('doc_file_'.$number);
                    $doc_name = 'documents_'.$table_name.$row_id.time() . '.' . $doc->getClientOriginalExtension();
                    $doc->move($destinationPath, $doc_name);
                } else {
                    $doc_name = '';
                }
                $doc_title = 'doc_title_'.$number;
                $sort_order = 'sort_order_'.$number;

                DB::table('documents')->insert([
                    'table_name' => $table_name,
                    'row_id' => $row_id,
                    'doc_title' => $request->$doc_title,
                    'doc_url' => $doc_name,
                    'uploaded_by' => Session::get('admin_id'),
                    'sort_order' => $request->$sort_order
                ]);
            }
        }

        return redirect()->back()->with('message', 'Documents Uploaded Successfully');
	}

	public function salesRepresntiveOrders() {
		if(Session::has('store_id') && Session::get('admin_type') == 0) {
            $parent_ids = $this->extractParentid(Session::get('admin_id'));
            if(count($parent_ids) == 0) {
                $parent_ids = [Session::get('admin_id')];
            } else {
                array_push($parent_ids, Session::get('admin_id'));
            }
			$data['sales_representive_list'] = DB::table('admins')->select('id', 'name', 'phone')->where('role', 2)->whereIn('parent_id',$parent_ids)->get();
		} else {
			$data['sales_representive_list'] = DB::table('admins')->select('id', 'name', 'phone')->where('role', 2)->get();
		}

		if(isset($_GET['sales_representive']) && !empty($_GET['sales_representive'])) {
			if((isset($_GET['start_date']) && !empty($_GET['start_date'])) && (isset($_GET['end_date']) && !empty($_GET['end_date']))) {
				$data['sales_representive_orders'] = DB::table('orders')
				->join('users', 'orders.user_id', '=', 'users.id')
				->select('orders.id', 'orders.subtotal', 'orders.order_date', 'orders.order_status', 'users.name', 'users.phone', 'users.address')
				->where('orders.biller_id', $_GET['sales_representive'])
				->whereBetween('orders.order_date', [$_GET['start_date'], $_GET['end_date']])
				->get();
			} else {
				$data['sales_representive_orders'] = DB::table('orders')
				->join('users', 'orders.user_id', '=', 'users.id')
				->select('orders.id', 'orders.subtotal', 'orders.order_date', 'orders.order_status', 'users.name', 'users.phone', 'users.address')
				->where('orders.biller_id', $_GET['sales_representive'])
				->get();
			}
		} else {
			$data['sales_representive_orders'] = [];
		}

		return view('backend.sales_representive_orders', $data);
	}

	public function exportPhoneNumber($table, $field) {
		$result = DB::table($table)->select($field)->get();
		if(count($result) > 0) {
			$filename = $table.time().'.txt';
			$file = public_path('export_data/'.$filename);
			fopen($file,'a');
			$numItems=count($result);
			$i = 0;
			foreach($result as $row)
			{
				if(++$i === $numItems) {
					$phonenumber = $row->$field;
				} else {
					$phonenumber = $row->$field.',';
				}
				file_put_contents($file, $phonenumber . PHP_EOL, FILE_APPEND | LOCK_EX);
			}
			$headers = ['Content-Type: application/txt'];
			$newName = $table."_numbers.txt";
			return response()->download($file, $newName, $headers);
		} else {
			return redirect()->back()->with('message', 'Data Not Found!');
		}
	}

	public function smsPage() {
		$data['sms_report'] = DB::table('stores')->select('sms_limit', 'total_sms_sent')->where('id', Session::get('store_id'))->first();

		$data['shop_customers'] = DB::table('users')
        ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
        ->select('users.id', 'users.name', 'users.phone')
        ->where('shop_customers.shop_id', Session::get('store_id'))
        ->groupBy('shop_customers.user_id')
		->get();

		$data['due_customer_list'] = DB::table('users')
            ->join('orders_invoice', 'users.id', '=', 'orders_invoice.user_id')
            ->select('users.name', 'users.phone', DB::raw("SUM(orders_invoice.due_amount) as total_due_amount"))
            ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.payment_status' => 0])->groupBy('orders_invoice.user_id')->get();

		return view('backend.sms_page', $data);
	}

	public function sendOfferSmsNew(Request $request) {
		$shop = DB::table('stores')->select('sms_limit', 'total_sms_sent')->where('id', Session::get('store_id'))->first();
		$product_name = $request->product_name;
		$offer_price = trim($request->offer_price);
		$offer_text = trim($request->offer_text);
		$message_body = $offer_text.'. '.$product_name. ', '.$offer_price;

		if(!empty($request->offer_customers)) {
			$sms_count = count($request->offer_customers);

			if($shop->sms_limit > ($sms_count - 1)) {
				$this->sendSms($request->offer_customers, $message_body);
				DB::table('stores')->where('id', Session::get('store_id'))->update([
					'sms_limit' => ($shop->sms_limit - $sms_count),
					'total_sms_sent' => ($shop->total_sms_sent + $sms_count)
				]);
				return redirect()->back()->with('success', 'Offer SMS Send Successfully');
			} else {
				return redirect()->back()->with('error', 'Sorry! Your SMS Limit Has Been Finished.');
			}
		} else {
			return redirect()->back()->with('error', 'Please Select Customer');
		}
	}

	public function sendDueSmsNew(Request $request) {
		//check sms format
		$due_sms_text = trim($request->due_text);
		if(empty($due_sms_text) || strpos($due_sms_text, '##') == false) {
			return redirect()->back()->with('error', 'Invalid SMS Text');
		}

		$shop = DB::table('stores')->select('sms_limit', 'total_sms_sent')->where('id', Session::get('store_id'))->first();

		if(!empty($request->due_customers)) {
			$sms_count = count($request->due_customers);

			if($shop->sms_limit > ($sms_count - 1)) {
				$explode_sms_text = explode('##', $due_sms_text);
				foreach($request->due_customers as $due_customer) {
					$explode = explode('#', $due_customer);
					$message_body = $explode_sms_text[0].' '.number_format($explode[1], 2).' '.$explode_sms_text[1];
					$this->sendSms(array($explode[0]), $message_body);

					DB::table('stores')->where('id', Session::get('store_id'))->update([
						'sms_limit' => ($shop->sms_limit - $sms_count),
						'total_sms_sent' => ($shop->total_sms_sent + $sms_count)
					]);
					return redirect()->back()->with('success', 'Due SMS Send Successfully');
				}

			} else {
				return redirect()->back()->with('error', 'Sorry! Your SMS Limit Has Been Finished.');
			}
		} else {
			return redirect()->back()->with('error', 'Please Select Due Customer');
		}
	}

	private function check_delete_permission($table, $key, $id) {
        $store = DB::table($table)->select('store_id')->where($key, $id)->first();
        if(Session::get('role') == 0) {
        	DB::table($table)->where($key, $id)->delete();
        } else if(Session::get('store_id') == $store->store_id) {
        	DB::table($table)->where($key, $id)->delete();
        } else {
        	return false;
        }
        return true;
	}

	private function check_edit_permission($table, $key, $id) {
		$store = DB::table($table)->select('store_id')->where($key, $id)->first();
        if(Session::get('role') == 0) {
        	return true;
        } else if(Session::get('store_id') == $store->store_id) {
        	return true;
        } else {
        	return false;
        }
	}

	public function not_found() {
		return view('backend.not_found');
	}

}
