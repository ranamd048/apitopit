<?php

namespace App\Http\Controllers;

use App\Http\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;

class ProductController extends Controller
{
    use HelperTrait;

    public function createNewProduct()
    {
        $store_id = Session::get('store_id');
        if(Session::get('role') == 1) {
            $check_permission = DB::table('stores')->select('upload_permission')->where('id', Session::get('store_id'))->first();
            if($check_permission->upload_permission == 0) {
                return redirect('admin/product-list')->with('error', 'You can not upload product. Contact to support');
            }
        }

        $data = [];
        $data['store_list'] = DB::table('stores')->where(['status' => 1])->whereIn('shop_type', ['shop', 'both'])->get();
        $data['unit_list'] = DB::table('product_units')->where('unit_status', 1)->get();
        if ($store_id) {
            $data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'shop', 'status' => 1])->whereIn('store_id', [0, $store_id])->orderBy('id', 'DESC')->get();
        } else {
            $data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'shop', 'status' => 1])->get();
        }
        $data['brand_list'] = DB::table('brands')->select('id', 'name')->where('status', 1)->orderBy('sort_order', 'DESC')->get();

        return view('backend.product.add', $data);
    }

    public function save_product(Request $request)
    {
        if (Session::has('store_id')) {
            $store_id = Session::get('store_id');
        } else {
            $store_id = $request->store_id;
        }
        //check product duplicate
        $check_product_name = DB::table('products')->select('product_name')->where(['store_id' => $store_id, 'product_name' => trim($request->product_name)])->first();
        if($check_product_name) {
            return redirect()->back()->with('error', $request->product_name.' already added');
        }

        $shop_settings = DB::table('stores')->select('product_qty', 'product_image')->where('id', $store_id)->first();
        $current_products = DB::table('products')->select('id')->where('store_id', $store_id)->count();
        if($shop_settings->product_qty <= $current_products) {
            return redirect('admin/product-list')->with('error', 'Already you have uploaded maximum number products');
        }

        $this->validate($request, [
            'product_image' => 'image|mimes:jpeg,png,jpg|max:1024'
        ]);
        if(Session::get('role') == 0) {
            $product_status = $request->status;
            $message = 'Product Added Successfully';
        } else {
           $product_status = 0;
           $message = 'Product added successfully and product under review now. Product  will be  live on 24 hours';
        }

        $destinationPath = public_path('uploads/products/');
        $thumbDestinationPath = public_path('uploads/products/thumbnail');
        //$waterMarkUrl = public_path('uploads/watermark.png');
        if ($request->hasFile('product_image')) {
            $product_image = $request->file('product_image');
            $product_image_path = 'product_image' . time() . '.' . $product_image->getClientOriginalExtension();

            $thumbnail_image = Image::make($product_image->getRealPath());
            $thumbnail_image->resize(220, 220, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbDestinationPath . '/' . $product_image_path);
            //$thumbnail_image->insert($waterMarkUrl, 'center', 5, 5);

            $product_image->move($destinationPath, $product_image_path);
        } else {
            $product_image_path = '';//'default_image.png';
        }

        DB::table('products')->insert([
            'store_id' => $store_id,
            'unit_id' => $request->unit_id,
            'brand_id' => $request->brand_id,
            'product_name' => trim($request->product_name),
            'product_slug' => '',
            'product_image' => $product_image_path,
            'price' => trim($request->price),
            'product_video' => trim($request->product_video),
            'quantity' => ($request->quantity) ? $request->quantity : 1,
            'alert_quantity' => $request->alert_quantity,
            'category_id' => $request->category_id,
            'subcategory_id' => $request->subcategory_id,
            'sub_subcategory_id' => $request->sub_subcategory_id,
            'product_description' => trim($request->product_description),
            'product_location' => $request->product_location,
            'product_code' => $request->product_code,
            'cost' => ($request->cost) ? $request->cost : 0,
            'is_checking_stock' => ($request->is_checking_stock) ? 1 : 0,
            'promotion_start_date' => $request->promotion_start_date,
            'promotion_end_date' => $request->promotion_end_date,
            'promotion_price' => ($request->promotion_price) ? $request->promotion_price : 0,
            'featured' => ($request->featured) ? 1 : 0,
            'is_used' => ($request->is_used) ? 1 : 0,
            'is_home_delivery' => ($request->is_home_delivery) ? 1 : 0,
            'show_per_price' => ($request->show_per_price) ? 1 : 0,
            'status' => $product_status,
        ]);

        $product_id = DB::getPdo()->lastInsertId();
        //add slug
        $product_slug = $this->makeUniqueSlug('products', $product_id, 'product_slug', $request->product_name);
        DB::table('products')->where('id', $product_id)->update([
            'product_slug' => $product_slug
        ]);

        if ($request->hasFile('product_gallery')) {
            $image_serial = 1;
            $destinationPath = public_path('uploads/products/gallery/');
            $images = $request->file('product_gallery');
            foreach ($images as $image) {
                if($image_serial == $shop_settings->product_image) {
                    break;
                }

                $image_path = 'image_' . $product_id . time() . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $image_path);

                DB::table('product_gallery')->insert(['product_id' => $product_id, 'image_url' => $image_path]);
                $image_serial++;
            }
        }


        return redirect('admin/product-list')->with('message', $message);
    }

    public function productList()
    {
        $data = [];
        if(Session::has('store_id')) {
            $data['shop_id'] = Session::get('store_id');
        } else {
            $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
            $data['shop_id'] = $data['shop_list'][0]->id;
        }
        return view('backend.product.list', $data);
    }

    public function productStockManage()
    {
        $data = [];
        if(Session::has('store_id')) {
            $data['shop_id'] = Session::get('store_id');
        } else {
            $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
            $data['shop_id'] = $data['shop_list'][0]->id;
        }
        return view('backend.product.stock', $data);
    }

    public function todaySaleReport() {
        $data['today_sale_report'] = DB::table('orders_invoice')
        ->join('order_items', 'orders_invoice.id', '=', 'order_items.invoice_id')
        ->join('products', 'order_items.product_id', '=', 'products.id')
        ->select('order_items.product_id', 'order_items.product_name', 'order_items.price', 'order_items.discount_amount', 'products.cost', DB::raw("SUM(order_items.quantity) as total_quantity"))
        ->where(['orders_invoice.store_id' => Session::get('store_id')])
        ->whereDate('orders_invoice.created_at', Carbon::today())
        ->groupBy('order_items.product_id')
        ->get();
        return view('backend.reports.today_sale_report', $data);
    }

    public function getShopCategories($shop_id) {
        if(empty($shop_id)) {
            return false;
        }

        if(isset($_GET['parent_id']) && !empty($_GET['parent_id'])) {
            $parent_id = $_GET['parent_id'];
            $shop_categories = DB::table('products')
            ->join('categories', 'products.subcategory_id', '=', 'categories.id')
            ->select('categories.id', 'categories.name')
            ->where(['products.store_id' => $shop_id, 'categories.parent_id' => $parent_id, 'products.status' => 1])
            ->groupBy('products.subcategory_id')
            ->get();
        } else {
            $shop_categories = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->select('categories.id', 'categories.name')
            ->where(['products.store_id' => $shop_id, 'products.status' => 1])
            ->groupBy('products.category_id')
            ->get();
        }

        $output = '<option value="">Select ...</option>';
        if(count($shop_categories) > 0) {
            foreach($shop_categories as $row) {
                $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
            }
        }
        return $output;
    }

    public function productStockAlert() {
        return view('backend.product.alert_stock');
    }

    public function getAllProductList(Request $request)
    {
        $file_name = $request->file_name;
        $shop_id = $request->shop_id;
        if(empty($shop_id)) {
            return false;
        }
        $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
        $data['shop_id'] = $shop_id;

        if (!empty($request->type) && $request->type == 'category') {
            $data['products'] = DB::table('products')->where(['store_id' => $shop_id, 'category_id' => $request->value])->orderBy('product_name', 'DESC')->get();
        } else if (!empty($request->type) && $request->type == 'sub_category') {
            $data['products'] = DB::table('products')->where(['store_id' => $shop_id, 'subcategory_id' => $request->value, 'status' => 1])->orderBy('product_name', 'DESC')->get();
        } else if (!empty($request->type) && $request->type == 'search') {
            if(!empty($request->category_type) && !empty($request->category_id)) {
                if($request->category_type == 'category') {
                    $extra_condition = ['category_id' => $request->category_id];
                } else {
                    $extra_condition = ['subcategory_id' => $request->category_id];
                }
                $data['products'] = DB::table('products')->where(['store_id' => $shop_id])->where($extra_condition)->where('product_name', 'like', '%' . $request->value . '%')->orderBy('product_name', 'DESC')->get();
            } else {
                $data['products'] = DB::table('products')->where(['store_id' => $shop_id])->where('product_name', 'like', '%' . $request->value . '%')->orderBy('product_name', 'DESC')->get();
            }
        } else if (!empty($request->type) && $request->type == 'product_location') {
            if(!empty($request->category_type) && !empty($request->category_id)) {
                if($request->category_type == 'category') {
                    $extra_condition = ['category_id' => $request->category_id];
                } else {
                    $extra_condition = ['subcategory_id' => $request->category_id];
                }
                $data['products'] = DB::table('products')->where(['store_id' => $shop_id])->where($extra_condition)->where('product_location', 'like', '%' . $request->value . '%')->orderBy('product_name', 'DESC')->get();
            } else {
                $data['products'] = DB::table('products')->where(['store_id' => $shop_id])->where('product_location', 'like', '%' . $request->value . '%')->orderBy('product_name', 'DESC')->get();
            }
        } else {
            if(!empty($request->category_type) && !empty($request->category_id)) {
                if($request->category_type == 'category') {
                    $extra_condition = ['category_id' => $request->category_id];
                } else {
                    $extra_condition = ['subcategory_id' => $request->category_id];
                }
                $data['products'] = DB::table('products')->where('store_id', $shop_id)->where($extra_condition)->orderBy('product_name', 'DESC')->get();
            } else {
                $data['products'] = DB::table('products')->where('store_id', $shop_id)->orderBy('product_name', 'DESC')->get();
            }
        }
        return view('backend.'.$file_name, $data);
    }

    public function updateProductStock(Request $request) {
        $current_stock = DB::table('products')->select('quantity')->where('id', $request->product_id)->first();
        $update_stock = ($current_stock->quantity + $request->quantity);
        DB::table('products')->where('id', $request->product_id)->update([
			'price' => trim($request->sell_price),
			'cost' => trim($request->cost_price),
            'quantity' => $update_stock
        ]);

        return redirect()->back()->with('message', 'Product Stock Update Successfully');
    }

    public function edit_product($id)
    {
        $check_permission = $this->check_access_permission('products', 'id', $id);
        if (!$check_permission) {
            return redirect('admin/not_found');
        }

        $store_id = Session::get('store_id');

        $data = [];
        $data['price_groups'] = DB::table('price_groups')->get();
        $data['store_list'] = DB::table('stores')->where(['status' => 1])->whereIn('shop_type', ['shop', 'both'])->get();
        $data['unit_list'] = DB::table('product_units')->where('unit_status', 1)->get();
        if ($store_id) {
            $data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'shop', 'status' => 1])->whereIn('store_id', [0, $store_id])->orderBy('id', 'DESC')->get();
        } else {
            $data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'shop', 'status' => 1])->get();
        }
        $data['brand_list'] = DB::table('brands')->select('id', 'name')->where('status', 1)->orderBy('sort_order', 'DESC')->get();
        $data['product'] = DB::table('products')->where('id', $id)->first();
        $data['gallery_images'] = DB::table('product_gallery')->where('product_id', $id)->get();
        return view('backend.product.edit', $data);
    }

    public function update_product(Request $request)
    {
        $this->validate($request, [
            'product_image' => 'image|mimes:jpeg,png,jpg|max:1024'
        ]);

        if(Session::get('role') == 0) {
            $product_status = $request->status;
        } else {
            //$current_status = DB::table('products')->where('id', $request->product_id)->select('status')->first();
           $product_status = 0;//$current_status->status;
        }

        $destinationPath = public_path('uploads/products/');
        $thumbDestinationPath = public_path('uploads/products/thumbnail');
        //$waterMarkUrl = public_path('uploads/watermark.png');
        if ($request->hasFile('product_image')) {
            // if (!empty($request->existing_img)) {
            //     if($request->existing_img !== 'default_image.png') {
            //         if (file_exists(public_path('/uploads/products/' . $request->existing_img))) {
            //             unlink(public_path('/uploads/products/' . $request->existing_img));
            //         }
            //         if (file_exists(public_path('/uploads/products/thumbnail/' . $request->existing_img))) {
            //             unlink(public_path('/uploads/products/thumbnail/' . $request->existing_img));
            //         }
            //     }
            // }

            $product_image = $request->file('product_image');
            $product_image_path = 'product_image' . time() . '.' . $product_image->getClientOriginalExtension();

            $thumbnail_image = Image::make($product_image->getRealPath());
            $thumbnail_image->resize(220, 220, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbDestinationPath . '/' . $product_image_path);
            //$thumbnail_image->insert($waterMarkUrl, 'center', 5, 5);

            $product_image->move($destinationPath, $product_image_path);
        } else {
            $product_image_path = $request->existing_img;
        }

        if (Session::has('store_id')) {
            $store_id = Session::get('store_id');
        } else {
            $store_id = $request->store_id;
        }

        $product_slug = $this->makeUniqueSlug('products', $request->product_id, 'product_slug', $request->product_name, 'update');

        DB::table('products')->where('id', $request->product_id)->update([
            'store_id' => $store_id,
            'unit_id' => $request->unit_id,
            'brand_id' => $request->brand_id,
            'product_name' => trim($request->product_name),
            'product_slug' => $product_slug,
            'product_image' => $product_image_path,
            'price' => trim($request->price),
            'product_video' => trim($request->product_video),
            'quantity' => ($request->quantity) ? $request->quantity : 1,
            'alert_quantity' => $request->alert_quantity,
            'category_id' => $request->category_id,
            'subcategory_id' => $request->subcategory_id,
            'sub_subcategory_id' => $request->sub_subcategory_id,
            'product_description' => trim($request->product_description),
            'product_location' => $request->product_location,
            'product_code' => $request->product_code,
            'cost' => ($request->cost) ? $request->cost : 0,
            'is_checking_stock' => ($request->is_checking_stock) ? 1 : 0,
            'promotion_start_date' => $request->promotion_start_date,
            'promotion_end_date' => $request->promotion_end_date,
            'promotion_price' => ($request->promotion_price) ? $request->promotion_price : 0,
            'featured' => ($request->featured) ? 1 : 0,
            'is_used' => ($request->is_used) ? 1 : 0,
            'is_home_delivery' => ($request->is_home_delivery) ? 1 : 0,
            'show_per_price' => ($request->show_per_price) ? 1 : 0,
            'status' => $product_status,
        ]);
        
        $shop_settings = DB::table('stores')->select('product_image')->where('id', $store_id)->first();
        $gallery_image_count = DB::table('product_gallery')->where('product_id',  $request->product_id)->count();
        if ($request->hasFile('product_gallery')) {
            $image_serial = ($gallery_image_count + 1);
            $destinationPath = public_path('uploads/products/gallery/');
            $images = $request->file('product_gallery');
            foreach ($images as $image) {
                if($image_serial == $shop_settings->product_image) {
                    break;
                }

                $image_path = 'image_' . $request->product_id . time() . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $image_path);

                DB::table('product_gallery')->insert(['product_id' => $request->product_id, 'image_url' => $image_path]);
                $image_serial++;
            }
        }

        //update price group
        if(is_array($request->PriceGroup)) {
            foreach($request->PriceGroup as $group_id => $price) {
                if(DB::table('product_group_prices')->where(['product_id' => $request->product_id, 'price_group_id' => $group_id])->first()) {
                    DB::table('product_group_prices')->where(['product_id' => $request->product_id, 'price_group_id' => $group_id])->update([
                        'price' => $price,
                    ]);
                } else {
                    DB::table('product_group_prices')->insert([
                        'product_id' => $request->product_id,
                        'price_group_id' => $group_id,
                        'price' => $price,
                    ]);
                }
            }
        }

        return redirect('admin/product-list')->with('message', 'Product Updated Successfully');
    }

    public function delete_product($id)
    {
        $check_permission = $this->check_access_permission('products', 'id', $id);
        if (!$check_permission) {
            return redirect('admin/not_found');
        }
        DB::table('products')->where('id', $id)->delete();
        return redirect('admin/product-list')->with('message', 'Product Deleted Successfully');
    }

    public function get_subcategory_items($id)
    {
        $storeId = Session::get('store_id');
        if ($storeId) {
            $subcategory_items = DB::table('categories')->select('id', 'name')->where(['parent_id' => $id])->whereIn('store_id', [0, $storeId])->orderBy('id', 'DESC')->get();
        } else {
            $subcategory_items = DB::table('categories')->select('id', 'name')->where('parent_id', $id)->get();
        }


        $output = '<option value="">Select ...</option>';
        if (count($subcategory_items) > 0) {
            foreach ($subcategory_items as $item) {
                $output .= '<option value="' . $item->id . '">' . $item->name . '</option>';
            }
        }
        echo $output;
    }

    public function get_selected_subcategory($id)
    {
        $result = DB::table('categories')->select('id', 'name')->where('id', $id)->first();
        $output = '<option value="' . $result->id . '">' . $result->name . '</option>';
        echo $output;
    }

    public function delete_gallery_image($id) {
        DB::table('product_gallery')->where('id', $id)->delete();
        return 'DONE';
    }

    public function inactiveProductList() {
        $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
        if(isset($_GET['shop_id'])) {
            $data['product_list'] = DB::table('products')
                                ->join('stores', 'products.store_id', '=', 'stores.id')
                                ->select('products.*', 'stores.store_name')
                                ->where(['products.store_id' => $_GET['shop_id'],'products.status' => 0])
                                ->orderByDesc('products.id')
                                ->get();
        } else {
            $data['product_list'] = DB::table('products')
                                ->join('stores', 'products.store_id', '=', 'stores.id')
                                ->select('products.*', 'stores.store_name')
                                ->where('products.status', 0)
                                ->orderByDesc('products.id')
                                ->get();
        }
        return view('backend.product.inactive_product', $data);
    }

    public function publishProduct($id) {
        DB::table('products')->where('id', $id)->update(['status' => 1]);
        return redirect()->back()->with('message', 'Product Has Been Published');
    }

    public function copy_products(Request $request) {
        if(!$request->product_id) {
            return redirect()->back()->with('error', 'Please select products');
        }
        if(!$request->copy_to_shop_id) {
            return redirect()->back()->with('error', 'Please select shop');
        }
        foreach($request->product_id as $product_id) {
            $product = DB::table('products')->where(['id' => $product_id, 'store_id' => $request->copy_from_shop_id])->first();
            $copy_product = [
                'store_id' => $request->copy_to_shop_id,
                'unit_id' => $product->unit_id,
                'brand_id' => $product->brand_id,
                'product_name' => $product->product_name,
                'product_slug' => $product->product_slug,
                'product_image' => $product->product_image,
                'price' => $product->price,
                'product_video' => $product->product_video,
                'quantity' => $product->quantity,
                'category_id' => $product->category_id,
                'subcategory_id' => $product->subcategory_id,
                'sub_subcategory_id' => $product->sub_subcategory_id,
                'product_description' => $product->product_description,
                'product_code' => $product->product_code,
                'cost' => $product->cost,
                'video_link' => $product->video_link,
                'featured' => $product->featured,
                'is_used' => $product->is_used,
                'status' => $product->status,
            ];
            DB::table('products')->insert($copy_product);
            $last_product_id = DB::getPdo()->lastInsertId();
            //check gallery images
            $product_images = DB::table('product_gallery')->where('product_id', $product_id)->get();
            if(count($product_images) > 0) {
                foreach($product_images as $image) {
                    DB::table('product_gallery')->insert([
                        'product_id' => $last_product_id,
                        'image_url' => $image->image_url
                    ]);
                }
            }
        }
        return redirect()->back()->with('message', 'Products Successfully Copied');
    }

    private function check_access_permission($table, $key, $id)
    {
        $store = DB::table($table)->select('store_id')->where($key, $id)->first();
        if ($store) {
            if (Session::get('role') == 0) {
                return true;
            } else if (Session::get('store_id') == $store->store_id) {
                return true;
            }
        }
        return false;
    }

}