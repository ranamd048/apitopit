<?php

namespace App\Http\Controllers;

use App\Http\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class AccountController extends Controller
{
    use HelperTrait;

    public function dueInvoiceList() {
        $data = [];
        if(Session::has('store_id')) {
            $data['shop_id'] = Session::get('store_id');
        } else {
            $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
            $data['shop_id'] = $data['shop_list'][0]->id;
        }
        return view('backend.account.due_list', $data);
    }

    public function getShopCustomer($shop_id) {
        $customer_list = DB::table('users')
        ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
        ->select('users.id', 'users.name', 'users.phone')
        ->where('shop_customers.shop_id', $shop_id)
        ->groupBy('shop_customers.user_id')
        ->get();
        $output = '<option value="">Select ...</option>';
        if(count($customer_list) > 0) {
            foreach($customer_list as $row) {
                $output .= '<option value="'.$row->id.'">'.$row->name.' - '.$row->phone.'</option>';
            }
        }
        return $output;
    }

    public function getUserInvoiceList(Request $request)
    {
        $file_name = $request->file_name;
        $shop_id = $request->shop_id;
        $user_id = $request->user_id;
        if(empty($shop_id)) {
            return false;
        }

        if (!empty($request->type) && $request->type == 'customer') {
            $data['invoice_list'] = DB::table('users')
            ->join('orders_invoice', 'users.id', '=', 'orders_invoice.user_id')
            ->select('orders_invoice.*', 'users.name', 'users.phone', 'users.email', 'users.address', DB::raw("SUM(orders_invoice.due_amount) as total_due_amount"), DB::raw("SUM(orders_invoice.paid_amount) as total_paid_amount"), DB::raw("SUM(orders_invoice.discount) as total_discount"))
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $user_id, 'orders_invoice.payment_status' => $request->status])
            ->groupBy('orders_invoice.user_id')
            ->get();
        } else {
            $data['invoice_list'] = DB::table('users')
            ->join('orders_invoice', 'users.id', '=', 'orders_invoice.user_id')
            ->select('orders_invoice.*', 'users.name', 'users.phone', 'users.email', 'users.address', DB::raw("SUM(orders_invoice.due_amount) as total_due_amount"), DB::raw("SUM(orders_invoice.paid_amount) as total_paid_amount"), DB::raw("SUM(orders_invoice.discount) as total_discount"))
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.payment_status' => $request->status])
            ->groupBy('orders_invoice.user_id')
            ->get();
        }
        Session::forget('due_customer_list');
        return view('backend.'.$file_name, $data);
    }

    public function payInvoiceBill(Request $request) {
        if(is_array($request->invoice_id)) {
            $total_due_amount = 0;
            $invoice_array = array();
            foreach($request->invoice_id as $invoice_id) {
                $invoice_amount = DB::table('orders_invoice')->select('due_amount')->where('id', $invoice_id)->first();
                $total_due_amount += $invoice_amount->due_amount;
                $invoice_item = array('id' => $invoice_id, 'due' => $invoice_amount->due_amount);
                array_push($invoice_array, $invoice_item);
            }
            $data['user_id'] = $request->user_id;
            $data['total_due_amount'] = $total_due_amount;
            $data['invoice_array'] = $invoice_array;
            return view('backend.account.pay_invoice', $data);
        } else {
            return redirect()->back()->with('message', 'Please! Check Invoice Item');
        }
    }

    public function paidInvoiceBill(Request $request) {
        $available_amount = trim($request->paid_amount);
        if(is_array($request->invoice_id)) {
            foreach($request->invoice_id as $invoice_id) {
                $invoice_due = $request->due_amount[$invoice_id];
                if($available_amount > 0) {
                    if($available_amount >= $invoice_due) {
                        $paid_amount = $invoice_due;
                        $due_amount = 0;
                        $available_amount = ($available_amount - $invoice_due);
                    } else {
                        $paid_amount = $available_amount;
                        $due_amount = ($invoice_due - $available_amount);
                        $available_amount = 0;
                    }
                    DB::table('payments')->insert([
                        'invoice_id' => $invoice_id,
                        'user_id' => $request->user_id,
                        'paid_amount' => $paid_amount,
                        'payment_method' => $request->payment_method,
                        'payment_note' => trim($request->payment_note)
                    ]);
                    //current paid amount
                    $already_paid = DB::table('orders_invoice')->select('paid_amount')->where('id', $invoice_id)->first();
                    DB::table('orders_invoice')->where('id', $invoice_id)->update([
                        'paid_amount' => ($already_paid->paid_amount + $paid_amount),
                        'due_amount' => $due_amount,
                        'payment_status' => ($due_amount == 0) ? 1 : 0,
                        'modified_by' => Session::get('admin_id')
                    ]);
                }
            }
            return redirect('admin/account/due-list')->with('message', 'Payment Successfully Done');
        } else {
            return redirect()->back()->with('message', 'Error Occured!');
        }
    }

    public function paidInvoiceList() {
        $data = [];
        if(Session::has('store_id')) {
            $data['shop_id'] = Session::get('store_id');
        } else {
            $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
            $data['shop_id'] = $data['shop_list'][0]->id;
        }
        return view('backend.account.paid_list', $data);
    }

    public function getInvoicePayment(Request $request) {
        $user_id = $request->user_id;
        $shop_id = $request->shop_id;

        $data['payment_list'] = DB::table('orders_invoice')
        ->join('payments', 'orders_invoice.id', '=', 'payments.invoice_id')
        ->join('users', 'payments.user_id', '=', 'users.id')
        ->select('payments.*', 'users.name', 'users.phone')
        ->where(['orders_invoice.store_id' => $shop_id, 'payments.user_id' => $user_id])
        ->get();

        return view('backend.account.payment_list', $data);
    }

    public function payInvoiceDueAmount(Request $request) {
        $due_amount = $request->due_amount;
        $paid_amount = trim($request->paid_amount);
        $discount_amount = (!empty($request->discount_amount)) ? trim($request->discount_amount) : 0;
        $total_amount = ($paid_amount + $discount_amount);

        DB::table('payments')->insert([
            'invoice_id' => $request->invoice_id, 
            'user_id' => $request->user_id, 
            'paid_amount' => $paid_amount,
            'payment_method' => $request->payment_method,
            'payment_note' => trim($request->note),
        ]);
        //check invoice payment status
        if($total_amount >= $due_amount) {
            $current_due_amount = 0;
            $payment_status = 1;
        } else if($total_amount == $due_amount) {
            $current_due_amount = 0;
            $payment_status = 1;
        } else {
            $current_due_amount = ($due_amount - $total_amount);
            $payment_status = 0;
        }
        $invoice = DB::table('orders_invoice')->select('discount', 'paid_amount')->where('id', $request->invoice_id)->first();
        DB::table('orders_invoice')->where('id', $request->invoice_id)->update([
            'discount' => ($invoice->discount + $discount_amount),
            'paid_amount' => ($invoice->paid_amount + $paid_amount),
            'due_amount' => $current_due_amount,
            'payment_status' => $payment_status,
            'modified_by' => Session::get('admin_id')
        ]);
        return redirect()->back()->with('message', 'Payment Successfully Done!');
    }

    public function customerDueList($shop_id, $user_id) {
        $data['invoice_list'] = DB::table('users')
                ->join('orders_invoice', 'users.id', '=', 'orders_invoice.user_id')
                ->select('orders_invoice.*', 'users.name', 'users.phone', 'users.address')
                ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $user_id, 'orders_invoice.payment_status' => 0])
                ->get();
        $data['user_id'] = $user_id;
        $data['shop_id'] = $shop_id;
        return view('backend.account.customer_due_list', $data);
    }

    public function customerPaidList($shop_id, $user_id) {
        $data['invoice_list'] = DB::table('users')
                ->join('orders_invoice', 'users.id', '=', 'orders_invoice.user_id')
                ->select('orders_invoice.*', 'users.name', 'users.phone', 'users.address')
                ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $user_id, 'orders_invoice.payment_status' => 1])
                ->get();
        $data['shop_id'] = $shop_id;
        return view('backend.account.customer_paid_list', $data);
    }

    public function sendNotification($shop_id, $user_id) {
        $due_amount = $_GET['due'];
        $system_name = DB::table('site_information')->select('web_title', 'email')->where('id', 1)->first();
        $system_settings = DB::table('system_settings')->select('sms_service')->first();
        $customer = DB::table('users')->select('name', 'email', 'phone')->where('id', $user_id)->first();
        $shop = DB::table('stores')->select('store_name', 'due_sms_text', 'sms_limit')->where('id', $shop_id)->first();
        if($shop->due_sms_text) {
            $message_body = $shop->due_sms_text.' ৳'.number_format($due_amount, 2);
        } else {
            $message_body = 'Hi '.$customer->name.', You have due ৳'.number_format($due_amount, 2). ' in '.$shop->store_name.' shop at '.$system_name->web_title.' platform.';
        }

        if($system_settings->sms_service == 1 && $shop->sms_limit > 0) {
            $this->sendSms(array($customer->phone), $message_body);
            DB::table('stores')->where('id', $shop_id)->update([
                'sms_limit' => ($shop->sms_limit - 1)
            ]);
        }
        //send email
        if(!empty($customer->email)) {
            $data = array(
                'email' => $customer->email,
                'message_body' => $message_body,
                'system_email' => $system_name->email,
                'system_name' => $system_name->web_title,
            );
            Mail::send('emails.due_message', $data, function($message) use ($data){
                $message->to($data['email']);
                $message->subject('Due Notification From '.$data['system_name']);
                $message->from($data['system_email']);
            });
        }
        if($shop->sms_limit == 0) {
            return redirect()->back()->with('error', 'Sorry! Your SMS Limit Has Been Finished.');
        }

        return redirect()->back()->with('message', 'Notification Send Successfully Done');
    }

    public function sendSmsToAllCustomer() {
        $error = false;
        $due_customer_list = Session::get('due_customer_list');
        foreach($due_customer_list as $row) {
            if(!$this->sendSmsNotification($row['shop_id'], $row['user_id'], $row['due'])) {
                $error = true;
                break;
            } else {
                continue;
            }
        }
        Session::forget('due_customer_list');
        if($error) {
            return redirect()->back()->with('error', 'Sorry! Your SMS Limit Has Been Finished.');
        } else {
            return redirect()->back()->with('message', 'SMS Send Successfully to All Customer');
        }
    }

    public function sendSmsToSelectedCustomer(Request $request) {
        if(is_array($request->user_id)) {
            $error = false;
            foreach($request->user_id as $user_id) {
                $due_amount = 'due_'.$user_id;
                $shop_id = 'shop_id_'.$user_id;
                if(!$this->sendSmsNotification($request->$shop_id, $user_id, $request->$due_amount)) {
                    $error = true;
                    break;
                } else {
                    continue;
                }
            }
            if($error) {
                return redirect()->back()->with('error', 'Sorry! Your SMS Limit Has Been Finished.');
            } else {
                return redirect()->back()->with('message', 'SMS Send Successfully to Selected Customer');
            }
        } else {
            return redirect()->back()->with('error', 'Please Select Item to send SMS');
        }
    }

    public function sendSmsNotification($shop_id, $user_id, $due_amount) {
        $system_name = DB::table('site_information')->select('web_title', 'email')->where('id', 1)->first();
        $customer = DB::table('users')->select('name', 'email', 'phone')->where('id', $user_id)->first();
        $shop = DB::table('stores')->select('store_name', 'due_sms_text', 'sms_limit', 'total_sms_sent')->where('id', $shop_id)->first();
        if($shop->due_sms_text) {
            $message_body = $shop->due_sms_text.' ৳'.number_format($due_amount, 2);
        } else {
            $message_body = 'Hi '.$customer->name.', You have due ৳'.number_format($due_amount, 2). ' in '.$shop->store_name.' shop at '.$system_name->web_title.' platform.';
        }

        if($shop->sms_limit > 0) {
            $this->sendSms(array($customer->phone), $message_body);
            DB::table('stores')->where('id', $shop_id)->update([
                'sms_limit' => ($shop->sms_limit - 1),
                'total_sms_sent' => ($shop->total_sms_sent + 1),
            ]);
        }
        if($shop->sms_limit == 0) {
            return false;
        }
        return true;
    }

    public function incomeExpenseReport() {
        $date = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d');
        $prev_date = date('Y-m-d', strtotime($date .' -1 day'));
        $shop_id = Session::get('store_id');
        
        if(isset($_GET['from_date']) && isset($_GET['to_date'])) {
			$from_date = date('d-F-Y', strtotime($_GET['from_date']));
			$to_date = date('d-F-Y', strtotime($_GET['to_date']));
			$data['title'] = 'Income & Expense Report From '.$from_date.' To '.$to_date;
            $data['pre_balance'] = $this->incomeExpensePreBalance($prev_date);
            $income_report = DB::table('payments')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.paid_amount', 'payments.payment_date', 'users.name')
                ->where(['payments.store_id' => $shop_id])
                ->whereBetween('payments.payment_date', [$_GET['from_date'], $_GET['to_date']])
                ->get();
            $expenses = DB::table('expenses')
                ->join('expense_category', 'expenses.category_id', '=', 'expense_category.id')
                ->select('expenses.amount', 'expenses.date', 'expense_category.name')
                ->where(['expense_category.store_id' => $shop_id])
                ->whereBetween('expenses.date', [$_GET['from_date'], $_GET['to_date']])
                ->get();
            $purchase_expenses = DB::table('purchase_payments')
                ->join('users', 'purchase_payments.supplier_id', '=', 'users.id')
                ->select('purchase_payments.paid_amount', 'purchase_payments.created_at', 'users.name')
                ->where(['purchase_payments.store_id' => $shop_id])
                ->whereBetween('purchase_payments.created_at', [$_GET['from_date'], $_GET['to_date']])
                ->get();
		} else {
            $data['pre_balance'] = $this->incomeExpensePreBalance($prev_date);
			$data['title'] = 'Today Income & Expense Report';
            $income_report = DB::table('payments')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.paid_amount', 'payments.payment_date', 'users.name')
                ->where(['payments.store_id' => $shop_id])
                ->whereDate('payments.payment_date', Carbon::today())
                ->get();
            $expenses = DB::table('expenses')
                ->join('expense_category', 'expenses.category_id', '=', 'expense_category.id')
                ->select('expenses.amount', 'expenses.date', 'expense_category.name')
                ->where(['expense_category.store_id' => $shop_id])
                ->whereDate('expenses.date', Carbon::today())
                ->get();
            $purchase_expenses = DB::table('purchase_payments')
                ->join('users', 'purchase_payments.supplier_id', '=', 'users.id')
                ->select('purchase_payments.paid_amount', 'purchase_payments.created_at', 'users.name')
                ->where(['purchase_payments.store_id' => $shop_id])
                ->whereDate('purchase_payments.created_at', Carbon::today())
                ->get();
		}

        $income_expense_array = array();
        if(count($expenses) > 0) {
            foreach($expenses as $expense) {
                $array_item = array('type' => 'expense', 'title' => $expense->name, 'amount' => $expense->amount);
                array_push($income_expense_array, $array_item);
            }
        }
        if(count($income_report) > 0) {
            foreach($income_report as $income) {
                $array_item = array('type' => 'income', 'title' => $income->name, 'amount' => $income->paid_amount);
                array_push($income_expense_array, $array_item);
            }
        }
        if(count($purchase_expenses) > 0) {
            foreach($purchase_expenses as $purchase_expense) {
                $array_item = array('type' => 'expense', 'title' => $purchase_expense->name, 'amount' => $purchase_expense->paid_amount);
                array_push($income_expense_array, $array_item);
            }
        }
        $data['income_expense_reports'] = $income_expense_array;
        return view('backend.reports.income_expense', $data);
    }


}
