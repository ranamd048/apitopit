<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller {

	public function createNewService() {
		$store_id = Session::get('store_id');
		$data = [];
		$data['store_list'] = DB::table('stores')->where(['status' => 1])->whereIn('shop_type', ['service', 'both'])->get();
		if($store_id) {
			$data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'service', 'status' => 1])->whereIn('store_id', [0, $store_id])->orderBy('id', 'DESC')->get();
		} else {
			$data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'service', 'status' => 1])->get();
		}
		$data['unit_list'] = DB::table('product_units')->where('unit_status', 1)->get();
		return view('backend.service.add', $data);
	}

	public function save_service(Request $request) {
		if(Session::has('store_id')) {
	    	$store_id = Session::get('store_id');
	    } else {
	    	$store_id = $request->store_id;
	    }
		$shop_settings = DB::table('stores')->select('service_qty')->where('id', $store_id)->first();
        $current_services = DB::table('services')->select('id')->where('store_id', $store_id)->count();
        if($shop_settings->service_qty <= $current_services) {
            return redirect('admin/service-list')->with('error', 'Already you have uploaded maximum number services');
        }

		$destinationPath = public_path('uploads/services/');
	    if ($request->hasFile('service_image')) {
	        $service_image = $request->file('service_image');
	        $service_image_path = 'service_image_'.time().'.'.$service_image->getClientOriginalExtension();
	        $service_image->move($destinationPath, $service_image_path);
	    } else {
	    	$service_image_path = '';//'default_image.png';
	    }

	    $service_slug = $this->make_slug($request->service_name);
	    DB::table('services')->insert([
	    	'store_id' => $store_id,
	    	//'unit_id' => $request->unit_id,
	    	'service_name' => trim($request->service_name),
	    	'service_slug' => $service_slug,
	    	'service_image' => $service_image_path,
	    	'video_link' => trim($request->video_link),
	    	//'price' => $request->price,
	    	'category_id' => $request->category_id,
	    	'subcategory_id' => $request->subcategory_id,
	    	'sub_subcategory_id' => $request->sub_subcategory_id,
	    	'service_description' => trim($request->service_description),
	    	'experience' => trim($request->experience),
	    	'service_type' => $request->service_type,
	    	'status' => $request->status,
	    ]);

	    return redirect('admin/service-list')->with('message', 'Service Added Successfully');
	}

	public function serviceList() {
		if(Session::has('store_id')) {
			$condition = array('services.store_id' => Session::get('store_id'));
		} else {
			$condition = array();
		}	
		if(Session::has('store_id')) {
            $data['shop_id'] = Session::get('store_id');
        } else {
            $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['service', 'both'])->get();
            $data['shop_id'] = $data['shop_list'][0]->id;
        }
		return view('backend.service.list', $data);
	}

	public function getShopServiceCategories($shop_id) {
        if(empty($shop_id)) {
            return false;
        }

        if(isset($_GET['parent_id']) && !empty($_GET['parent_id'])) {
            $parent_id = $_GET['parent_id'];
            $shop_categories = DB::table('services')
            ->join('categories', 'services.subcategory_id', '=', 'categories.id')
            ->select('categories.id', 'categories.name')
            ->where(['services.store_id' => $shop_id, 'categories.parent_id' => $parent_id, 'services.status' => 1])
            ->groupBy('services.subcategory_id')
            ->get();
        } else {
            $shop_categories = DB::table('services')
            ->join('categories', 'services.category_id', '=', 'categories.id')
            ->select('categories.id', 'categories.name')
            ->where(['services.store_id' => $shop_id, 'services.status' => 1])
            ->groupBy('services.category_id')
            ->get();
        }

        $output = '<option value="">Select ...</option>';
        if(count($shop_categories) > 0) {
            foreach($shop_categories as $row) {
                $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
            }
        }
        return $output;
    }

	public function getAllServiceList(Request $request)
    {
        $shop_id = $request->shop_id;
        if(empty($shop_id)) {
            return false;
        }
        $data['shop_id'] = $shop_id;

        if (!empty($request->type) && $request->type == 'category') {
            $data['services'] = DB::table('services')->where(['store_id' => $shop_id, 'category_id' => $request->value])->orderBy('service_name', 'ASC')->get();
        } else if (!empty($request->type) && $request->type == 'sub_category') {
            $data['services'] = DB::table('services')->where(['store_id' => $shop_id, 'subcategory_id' => $request->value, 'status' => 1])->orderBy('service_name', 'ASC')->get();
        } else if (!empty($request->type) && $request->type == 'search') {
            if(!empty($request->category_type) && !empty($request->category_id)) {
                if($request->category_type == 'category') {
                    $extra_condition = ['category_id' => $request->category_id];
                } else {
                    $extra_condition = ['subcategory_id' => $request->category_id];
                }
                $data['services'] = DB::table('services')->where(['store_id' => $shop_id])->where($extra_condition)->where('service_name', 'like', '%' . $request->value . '%')->orderBy('service_name', 'ASC')->get();
            } else {
                $data['services'] = DB::table('services')->where(['store_id' => $shop_id])->where('service_name', 'like', '%' . $request->value . '%')->orderBy('service_name', 'ASC')->get();
            }
        } else {
            if(!empty($request->category_type) && !empty($request->category_id)) {
                if($request->category_type == 'category') {
                    $extra_condition = ['category_id' => $request->category_id];
                } else {
                    $extra_condition = ['subcategory_id' => $request->category_id];
                }
                $data['services'] = DB::table('services')->where('store_id', $shop_id)->where($extra_condition)->orderBy('service_name', 'ASC')->get();
            } else {
                $data['services'] = DB::table('services')->where('store_id', $shop_id)->orderBy('service_name', 'ASC')->get();
            }
        }
        return view('backend.service.service_list', $data);
    }

	public function serviceRequestList() {
		if(Session::has('store_id')) {
			$condition = array('services.store_id' => Session::get('store_id'));
		} else {
			$condition = array();
		}

		$data['request_list'] = DB::table('service_requests')
		->join('services', 'service_requests.service_id', '=', 'services.id')
		->join('users', 'service_requests.user_id', '=', 'users.id')
		->join('stores', 'services.store_id', '=', 'stores.id')
		->select('service_requests.*', 'services.service_name', 'services.service_slug', 'users.name', 'stores.store_name')
		->where($condition)
		->orderBy('service_requests.id', 'DESC')
		->get();
		return view('backend.service.request_list', $data);
	}

	public function viewServiceRequestList($id) {
		$data['request'] = DB::table('service_requests')
		->join('services', 'service_requests.service_id', '=', 'services.id')
		->join('users', 'service_requests.user_id', '=', 'users.id')
		->join('stores', 'services.store_id', '=', 'stores.id')
		->select('service_requests.*', 'services.service_name', 'services.service_slug', 'users.name', 'users.address', 'stores.store_name')
		->where('service_requests.id', $id)
		->first();
		return view('backend.service.view_request_list', $data);
	}

	public function updateServiceRequestStatus(Request $request) {
		DB::table('service_requests')->where('id', $request->request_id)->update([
			'status' => $request->status
		]);

		return redirect()->back()->with('message', 'Updated Successfully');
	}

	public function edit_service($id) {
		$check_permission = $this->check_access_permission('services', 'id', $id);
		if(!$check_permission) {
			return redirect('admin/not_found');
		}

	    $store_id = Session::get('store_id');
		$data = [];
		$data['store_list'] = DB::table('stores')->where(['status' => 1])->whereIn('shop_type', ['service', 'both'])->get();
		if($store_id) {
			$data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'service', 'status' => 1])->whereIn('store_id', [0, $store_id])->orderBy('id', 'DESC')->get();
		} else {
			$data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'service', 'status' => 1])->get();
		}

		$data['service'] = DB::table('services')->where('id', $id)->first();
		$data['unit_list'] = DB::table('product_units')->where('unit_status', 1)->get();
		return view('backend.service.edit', $data);
	}

	public function update_service(Request $request) {
		$this->validate($request, [
	        'service_image' => 'image|mimes:jpeg,png,jpg|dimensions:max_width=770,max_height=310|max:1024'
	    ]);

	    $destinationPath = public_path('uploads/services/');
	    if ($request->hasFile('service_image')) {
	    	if(!empty($request->existing_img) && $request->existing_img !== 'default_image.png') {
	    		if(file_exists(public_path('/uploads/services/'.$request->existing_img))) {
	    			unlink(public_path('/uploads/services/'.$request->existing_img));
	    		}
	    	}

	        $service_image = $request->file('service_image');
	        $service_image_path = 'service_image_'.time().'.'.$service_image->getClientOriginalExtension();
	        $service_image->move($destinationPath, $service_image_path);
	    } else {
	    	$service_image_path = $request->existing_img;
	    }

	    if(Session::has('store_id')) {
	    	$store_id = Session::get('store_id');
	    } else {
	    	$store_id = $request->store_id;
	    }

	    $service_slug = $this->make_slug($request->service_name);
	    DB::table('services')->where('id', $request->service_id)->update([
	    	'store_id' => $store_id,
	    	//'unit_id' => $request->unit_id,
	    	'service_name' => trim($request->service_name),
	    	'service_slug' => $service_slug,
			'service_image' => $service_image_path,
			'video_link' => trim($request->video_link),
	    	//'price' => $request->price,
	    	'category_id' => $request->category_id,
	    	'subcategory_id' => $request->subcategory_id,
	    	'sub_subcategory_id' => $request->sub_subcategory_id,
	    	'service_description' => trim($request->service_description),
	    	'experience' => trim($request->experience),
	    	'service_type' => $request->service_type,
	    	'status' => $request->status,
	    ]);

	    return redirect('admin/service-list')->with('message', 'Service Updated Successfully');
	}

	public function delete_service($id) {
		$check_permission = $this->check_access_permission('services', 'id', $id);
		if(!$check_permission) {
			return redirect('admin/not_found');
		}

		DB::table('services')->where('id', $id)->delete();

		return redirect('admin/service-list')->with('message', 'Service Deleted Successfully');
	}

	private function check_access_permission($table, $key, $id) {
		$store = DB::table($table)->select('store_id')->where($key, $id)->first();
		if($store) {
			if(Session::get('role') == 0) {
        		return true;
	        } else if(Session::get('store_id') == $store->store_id) {
	        	return true;
	        }
		}
		return false;
	}

	private function make_slug($string) {
        $string = str_replace('/', ' ', $string);
        $slug = preg_replace('/\s+/u', '-', trim($string));
        return strtolower($slug);
    }

}