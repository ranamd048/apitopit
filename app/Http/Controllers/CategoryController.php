<?php

namespace App\Http\Controllers;

use App\Http\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    use HelperTrait;
    public function createNewCategory() {
        if(Session::get('role') == 1 || Session::has('store_id')) {
            return redirect('/admin/category-list/shop');
        }
		return view('backend.category.add');
	}

	public function save_category(Request $request) {
        $level = $request->level;
		$this->validate($request, [
			'name' => 'required',
	        'image' => 'image|mimes:jpeg,png,jpg'
	    ]);

		$destinationPath = public_path('uploads/category/');
	    if ($request->hasFile('image')) {
	        $image = $request->file('image');
	        $image_name = 'category_image'.time().'.'.$image->getClientOriginalExtension();
	        $image->move($destinationPath, $image_name);
	    } else {
	    	$image_name = '';
	    }

	    if(Session::has('store_id')) {
            $store_id = Session::get('store_id');
	    } else {
	    	$store_id = 0;
	    }

	    $parent_id = $request->parent_id;

	    DB::table('categories')->insert([
	    	'parent_id' => $parent_id,
	    	'name' => trim($request->name),
	    	'slug' => '',
	    	'image' => $image_name,
	    	'category_type' => $request->category_type,
	    	'store_id' => $store_id,
	    	'sort_order' => $request->sort_order,
	    	'status' => $request->status,
	    ]);

        $category_id = DB::getPdo()->lastInsertId();
        //add slug
        $slug = $this->makeUniqueSlug('categories', $category_id, 'slug', $request->name);
        DB::table('categories')->where('id', $category_id)->update([
            'slug' => $slug
        ]);

	    if($parent_id == 0) {
	    	return redirect('admin/category-list/'.$request->category_type)->with('message', 'Created Successfully');
	    } else {
	    	return redirect('admin/sub'.$level.'-categories/'.$parent_id)->with('message', 'Created Successfully');
	    }
	}

	public function categoryList($type) {
		$store_id = Session::get('store_id');
		$data['type'] = $type;
		if(Session::get('role') == 0 && $store_id == null) {
			$data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => $type, 'store_id' => 0])->orderBy('sort_order', 'DESC')->get();
		} else {
			$data['categories'] = DB::table('categories')->where(['parent_id' => 0, 'category_type' => $type])->whereIn('store_id', [0, $store_id])->orderBy('sort_order', 'DESC')->get();
		}
		
		return view('backend.category.list', $data);
	}

	public function sub_category_list($id) {
        $store_id = Session::get('store_id');
        $data = [];
		$data['category'] = DB::table('categories')->where('id', $id)->first();

		if(Session::get('role') == 0 && $store_id == null) {
			$data['sub_categories'] = DB::table('categories')->where(['parent_id' => $id])->whereIn('store_id', [0])->orderBy('sort_order', 'DESC')->get();
		} else {
			$data['sub_categories'] = DB::table('categories')->where(['parent_id' => $id])->whereIn('store_id', [0, $store_id])->orderBy('sort_order', 'DESC')->get();
		}
        return view('backend.category.sub_list', $data);
	}

	public function sub_two_category_list($id) {
        $store_id = Session::get('store_id');
        $data = [];
		$data['category'] = DB::table('categories')->where('id', $id)->first();

		if(Session::get('role') == 0 && $store_id == null) {
			$data['sub_categories'] = DB::table('categories')->where(['parent_id' => $id])->whereIn('store_id', [0])->orderBy('sort_order', 'DESC')->get();
		} else {
			$data['sub_categories'] = DB::table('categories')->where(['parent_id' => $id])->whereIn('store_id', [0, $store_id])->orderBy('sort_order', 'DESC')->get();
		}
        return view('backend.category.sub2_list', $data);
    }

	public function add_sub_category($id) {
        $level = '';
        if(isset($_GET['level']) && !empty($_GET['level'])) {
            $level = $_GET['level'];
        }
		$data = [];
        $data['level'] = $level;
		$data['category'] = DB::table('categories')->where('id', $id)->first();
		return view('backend.category.add_sub', $data);
	}

	public function edit_category($id) {
		$check_permission = $this->check_access_permission('categories', 'id', $id);
		if(!$check_permission) {
			return redirect('admin/not_found');
		}

		$data = [];
		$data['category'] = DB::table('categories')->where('id', $id)->first();
		return view('backend.category.edit', $data);
	}

	public function edit_sub_category($id) {
		$check_permission = $this->check_access_permission('categories', 'id', $id);
		if(!$check_permission) {
			return redirect('admin/not_found');
		}

		$data = [];
		$data['category'] = DB::table('categories')->where('id', $id)->first();
		return view('backend.category.edit_sub', $data);
	}

	public function update_category(Request $request) {
		$this->validate($request, [
			'name' => 'required',
	        'image' => 'image|mimes:jpeg,png,jpg'
	    ]);

		$destinationPath = public_path('uploads/category/');
	    if ($request->hasFile('image')) {
	    	if(!empty($request->existing_img)) {
	    		if(file_exists(public_path('/uploads/category/'.$request->existing_img))) {
	    			unlink(public_path('/uploads/category/'.$request->existing_img));
	    		}
	    	}
	        $image = $request->file('image');
	        $image_name = 'category_image'.time().'.'.$image->getClientOriginalExtension();
	        $image->move($destinationPath, $image_name);
	    } else {
	    	$image_name = $request->existing_img;
	    }

        $slug = $this->makeUniqueSlug('categories', $request->category_id, 'slug', $request->name, 'update');

	    if(Session::has('store_id')) {
	    	$store_id = Session::get('store_id');
	    } else {
	    	$store_id = 0;
	    }

	    $parent_id = $request->parent_id;

	    DB::table('categories')->where('id', $request->category_id)->update([
	    	'parent_id' => $parent_id,
	    	'name' => trim($request->name),
	    	'slug' => $slug,
	    	'image' => $image_name,
	    	'category_type' => $request->category_type,
			'store_id' => $store_id,
			'sort_order' => $request->sort_order,
	    	'status' => $request->status,
	    ]);

	    if($parent_id == 0) {
	    	return redirect('admin/category-list/'.$request->category_type)->with('message', 'Updated Successfully');
	    } else {
	    	return redirect('admin/sub-categories/'.$parent_id)->with('message', 'Updated Successfully');
	    }
	    
	}

	public function delete_category($id) {
		$check_permission = $this->check_access_permission('categories', 'id', $id);
		if(!$check_permission) {
			return redirect('admin/not_found');
		}

		$check_subcategory = DB::table('categories')->where('parent_id', $id)->first();
		$type = DB::table('categories')->select('category_type')->where('id', $id)->first();
		if($check_subcategory) {
			$message = 'Firstly Delete Subcategory';
		} else {
			DB::table('categories')->where('id', $id)->delete();
			$message = 'Deleted Successfully';
		}
		return redirect('admin/category-list/'.$type->category_type)->with('message', $message);
	}

	private function check_access_permission($table, $key, $id) {
		$store = DB::table($table)->select('store_id')->where($key, $id)->first();
		if($store) {
			if(Session::get('role') == 0) {
        		return true;
	        } else if(Session::get('store_id') == $store->store_id) {
	        	return true;
	        }
		}
		return false;
	}


}
