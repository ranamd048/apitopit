<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PurchaseController extends Controller
{
    public function index() {
        $data['result'] = DB::table('purchases')
        ->join('users', 'purchases.supplier_id', '=', 'users.id')
        ->select('purchases.*', 'users.name')
        ->where('purchases.store_id', Session::get('store_id'))
		->orderBy('purchases.id', 'DESC')
        ->get();
		return view('backend.purchase.list', $data);
	}

	public function add() {
		$data['supplier_list'] = DB::table('users')->select('id', 'name')->where(['store_id' => Session::get('store_id'), 'user_type' => 'supplier'])->get();
		return view('backend.purchase.add', $data);
	}

	public function save(Request $request) {
		if(!$request->items) {
			return redirect()->back()->with('error', 'Please Add Some Items');
		}

		if (!file_exists(public_path('uploads/purchase_attachments'))) {
			mkdir(public_path('uploads/purchase_attachments'), 0777, true);
		}
		$destinationPath = public_path('uploads/purchase_attachments/');
	    if ($request->hasFile('attachment')) {
	        $attachment = $request->file('attachment');
	        $attachment_path = 'attachment'.time().'.'.$attachment->getClientOriginalExtension();
	        $attachment->move($destinationPath, $attachment_path);
	    } else {
	    	$attachment_path = null;
		}
		
	    DB::table('purchases')->insert([
			'store_id' => Session::get('store_id'),
			'supplier_id' => $request->supplier_id,
	    	'total' => 0,
	    	'discount' => $request->discount ? $request->discount : 0,
	    	'grand_total' => 0,
	    	'paid_amount' => 0,
	    	'due_amount' => 0,
	    	'payment_status' => 0,
	    	'note' => $request->note,
	    	'attachment' => $attachment_path,
	    	'shipping' => $request->shipping,
	    	'status' => $request->status,
	    	'created_by' => Session::get('admin_id'),
	    	'created_at' => $request->date,
		]);
		$purchase_id = DB::getPdo()->lastInsertId();
		if($request->items) {
			$total = 0;
			foreach($request->items as $item) {
				$total += ($item['quantity'] * $item['cost']);
				DB::table('purchase_items')->insert([
					'purchase_id' => $purchase_id,
					'product_id' => $item['id'],
					'product_name' => $item['name'],
					'sale_price' => $item['price'],
					'cost_price' => $item['cost'],
					'quantity' => $item['quantity'],
					'subtotal' => ($item['quantity'] * $item['cost'])
				]);
				//update main product
				DB::table('products')->where('id', $item['id'])->update(['price' => $item['price'], 'quantity' => ($item['quantity'] + $item['stock']), 'cost' => $item['cost']]);
			}
			$grand_total = $request->discount ? ($total - $request->discount) : $total;
			//payments
			if(!empty($request->paid_amount) && $request->paid_amount > 0) {
				$due_amount = ($grand_total - $request->paid_amount);
				DB::table('purchases')->where(['id' => $purchase_id])->update([
					'paid_amount' => $request->paid_amount,
					'due_amount' => $due_amount,
					'payment_status' => ($due_amount == 0) ? 1 : 0
				]);
				DB::table('purchase_payments')->insert([
					'store_id' => Session::get('store_id'),
					'purchase_id' => $purchase_id,
					'supplier_id' => $request->supplier_id,
					'paid_amount' => $request->paid_amount,
					'payment_method' => 'cod',
					'payment_note' => $request->payment_note
				]);
			} else {
				DB::table('purchases')->where(['id' => $purchase_id])->update([
					'paid_amount' => 0,
					'due_amount' => $grand_total,
					'payment_status' => 0
				]);
			}

			DB::table('purchases')->where('id', $purchase_id)->update([
				'total' => $total,
				'grand_total' => $grand_total
			]);
		}

	    return redirect('admin/purchase-list')->with('message', 'Purchase Added Successfully');
	}

	public function view($id) {
		$data['purchase'] = DB::table('purchases')
        ->join('users', 'purchases.supplier_id', '=', 'users.id')
        ->join('admins', 'purchases.created_by', '=', 'admins.id')
        ->select('purchases.*', 'users.name', 'admins.name as admin_name')
        ->where('purchases.id', $id)
		->first();
		$data['purchase_items'] = DB::table('purchase_items')->where('purchase_id', $id)->get();
		return view('backend.purchase.view', $data);
	}

	public function edit($id) {
		$data['supplier_list'] = DB::table('users')->select('id', 'name')->where(['store_id' => Session::get('store_id'), 'user_type' => 'supplier'])->get();
		$data['purchase'] = DB::table('purchases')->where('id', $id)->first();
		$data['purchase_items'] = DB::table('purchase_items')->where('purchase_id', $id)->get();
		return view('backend.purchase.edit', $data);
	}

	public function update(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'phone' => 'required|numeric'
		]);
		
	    DB::table('users')->where('id', $request->supplier_id)->update([
	    	'name' => trim($request->name),
	    	'email' => trim($request->email),
	    	'phone' => trim($request->phone),
	    	'division' => $request->division,
	    	'district' => $request->district,
	    	'thana' => $request->upazila,
	    	'address' => trim($request->address),
	    ]);

	    return redirect()->back()->with('message', 'Supplier Updated Successfully');
	}

	public function search(Request $request) {
		if(empty($request->value)) {
			return '';
		}
		$products = DB::table('products')->select('id', 'product_name', 'price', 'quantity', 'cost')->where(['store_id' => Session::get('store_id')])->where('product_name', 'like', '%' . $request->value . '%')->orderBy('id', 'DESC')->get();
        $output = '<ul>';
        if(count($products) > 0) {
            foreach($products as $row) {
                $output .= '<li class="add_item" data-id="'.$row->id.'" data-name="'.$row->product_name.'" data-price="'.$row->price.'" data-cost="'.$row->cost.'" data-stock="'.$row->quantity.'">'.$row->product_name.' ('.$row->price.')</li>';
            }
        } else {
            $output .= '<li>Product Not Found</li>';
        }
        $output .= '</ul>';
        return $output;
	}

	public function purchaseReport() {
		if(isset($_GET['from_date']) && isset($_GET['to_date'])) {
			$from_date = date('d-F-Y', strtotime($_GET['from_date']));
			$to_date = date('d-F-Y', strtotime($_GET['to_date']));
			$data['title'] = 'Purchase Report From '.$from_date.' To '.$to_date;
			$data['purchase_report'] = DB::table('purchases')
				->join('purchase_items', 'purchases.id', '=', 'purchase_items.purchase_id')
				->join('products', 'purchase_items.product_id', '=', 'products.id')
				->select('purchase_items.product_id', 'products.product_name', 'purchase_items.cost_price', DB::raw("SUM(purchase_items.quantity) as total_quantity"))
				->where(['purchases.store_id' => Session::get('store_id')])
				->whereBetween('purchases.created_at', [$_GET['from_date'], $_GET['to_date']])
				->groupBy('purchase_items.product_id')
				->get();
		} else {
			$data['title'] = 'Today Purchase Report';
			$data['purchase_report'] = DB::table('purchases')
				->join('purchase_items', 'purchases.id', '=', 'purchase_items.purchase_id')
				->join('products', 'purchase_items.product_id', '=', 'products.id')
				->select('purchase_items.product_id', 'products.product_name', 'purchase_items.cost_price', DB::raw("SUM(purchase_items.quantity) as total_quantity"))
				->where(['purchases.store_id' => Session::get('store_id')])
				->whereDate('purchases.created_at', Carbon::today())
				->groupBy('purchase_items.product_id')
				->get();
		}
        return view('backend.reports.purchase_report', $data);
	}

	public function delete($id) {
		$check_permission = $this->check_access_permission('purchases', 'id', $id);
		if(!$check_permission) {
			return redirect('admin/purchase-list')->with('error', "You don't have permission to delete this item.");
		}
		DB::table('purchases')->where('id', $id)->delete();
		DB::table('purchase_items')->where('purchase_id', $id)->delete();
		DB::table('purchase_payments')->where('purchase_id', $id)->delete();
		return redirect()->back()->with('message', 'Purchase Deleted Successfully');
	}

	private function check_access_permission($table, $key, $id) {
		$store = DB::table($table)->select('store_id')->where($key, $id)->first();
		if($store) {
			if(Session::get('role') == 0) {
        		return true;
	        } else if(Session::get('store_id') == $store->store_id) {
	        	return true;
	        }
		}
		return false;
	}


}
