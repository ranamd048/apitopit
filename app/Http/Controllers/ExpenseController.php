<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\NullableType;

class ExpenseController extends Controller {

	public function addExpenseCategory() {
		$data['store_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereDate('expiry_date', '>=', date('Y-m-d'))->get();
		return view('backend.expense.category.add', $data);
	}

    public function expenseCategoryList() {
        $store_id = Session::get('store_id');
		if($store_id) {
            $condition = array('store_id' => $store_id);
        } else {
            $condition = array();
		}
		$data['result'] = DB::table('expense_category')->where($condition)->orderBy('name', 'ASC')->get();
        return view('backend.expense.category.list', $data);
    }

    public function saveExpenseCategory(Request $request) {
        DB::table('expense_category')->insert([
	    	'store_id' => $request->store_id,
	    	'name' => trim($request->name),
	    ]);
	    return redirect()->back()->with('message', 'Expense Category Added Successfully');
	}
	
	public function editExpenseCategory($id) {
		$data['result'] = DB::table('expense_category')->where('id', $id)->first();
		return view('backend.expense.category.edit', $data);
	}

    public function updateExpenseCategory(Request $request) {
        DB::table('expense_category')->where('id', $request->category_id)->update([
	    	'name' => trim($request->name),
	    ]);
	    return redirect()->back()->with('message', 'Expense Category Updated Successfully');
	}
	
	public function deleteExpenseCategory($id) {
		DB::table('expense_category')->where('id', $id)->delete();
		return redirect()->back()->with('message', 'Expense Category Deleted Successfully');
	}

	public function addExpense() {
		$data['expense_categories'] = DB::table('expense_category')->select('id', 'name')->where('store_id', Session::get('store_id'))->orderBy('name', 'ASC')->get();
		return view('backend.expense.list.add', $data);
	}

	public function expenseList() {
		$store_id = Session::get('store_id');
		if($store_id) {
            $condition = array('expense_category.store_id' => $store_id);
        } else {
            $condition = array();
		}
		$data['expense_categories'] = DB::table('expense_category')->where(['store_id' => $store_id])->orderBy('name', 'ASC')->get();

		if(isset($_GET['from_date']) && isset($_GET['to_date']) && !empty($_GET['category_id'])) {
			$data['result'] = DB::table('expenses')
				->join('expense_category', 'expenses.category_id', '=', 'expense_category.id')
				->join('admins', 'expenses.created_by', '=', 'admins.id')
				->where($condition)
				->where('expenses.category_id', $_GET['category_id'])
				->whereBetween('expenses.date', [$_GET['from_date'], $_GET['to_date']])
				->select('expenses.*', 'expense_category.name', 'admins.name as user_name')
				->orderBy('expenses.id', 'DESC')
				->get();
		} else if(isset($_GET['from_date']) && isset($_GET['to_date']) && empty($_GET['category_id'])) {
			$data['result'] = DB::table('expenses')
				->join('expense_category', 'expenses.category_id', '=', 'expense_category.id')
				->join('admins', 'expenses.created_by', '=', 'admins.id')
				->where($condition)
				->whereBetween('expenses.date', [$_GET['from_date'], $_GET['to_date']])
				->select('expenses.*', 'expense_category.name', 'admins.name as user_name')
				->orderBy('expenses.id', 'DESC')
				->get();
		} else {
			$data['result'] = DB::table('expenses')
				->join('expense_category', 'expenses.category_id', '=', 'expense_category.id')
				->join('admins', 'expenses.created_by', '=', 'admins.id')
				->where($condition)
				->select('expenses.*', 'expense_category.name', 'admins.name as user_name')
				->orderBy('expenses.id', 'DESC')
				->get();
		}
		return view('backend.expense.list.list', $data);
	}

	public function saveExpense(Request $request) {
		if (!file_exists(public_path('uploads/expense_attachments'))) {
			mkdir(public_path('uploads/expense_attachments'), 0777, true);
		}
		$destinationPath = public_path('uploads/expense_attachments/');
	    if ($request->hasFile('attachment')) {
	        $attachment = $request->file('attachment');
	        $attachment_path = 'attachment'.time().'.'.$attachment->getClientOriginalExtension();
	        $attachment->move($destinationPath, $attachment_path);
	    } else {
	    	$attachment_path = null;
		}
		DB::table('expenses')->insert([
	    	'category_id' => $request->category_id,
	    	'amount' => trim($request->amount),
	    	'note' => trim($request->note),
	    	'created_by' => Session::get('admin_id'),
	    	'attachment' => $attachment_path,
	    	'date' => $request->date
	    ]);
	    return redirect()->back()->with('message', 'Expense Added Successfully');
	}

	public function editExpense($id) {
		$data['expense_categories'] = DB::table('expense_category')->select('id', 'name')->where('store_id', Session::get('store_id'))->orderBy('name', 'ASC')->get();
		$data['result'] = DB::table('expenses')->where('id', $id)->first();
		return view('backend.expense.list.edit', $data);
	}

	public function updateExpense(Request $request) {
		if (!file_exists(public_path('uploads/expense_attachments'))) {
			mkdir(public_path('uploads/expense_attachments'), 0777, true);
		}
		$destinationPath = public_path('uploads/expense_attachments/');
	    if ($request->hasFile('attachment')) {
	        $attachment = $request->file('attachment');
	        $attachment_path = 'attachment'.time().'.'.$attachment->getClientOriginalExtension();
	        $attachment->move($destinationPath, $attachment_path);
	    } else {
	    	$attachment_path = $request->existing_attachment ? $request->existing_attachment : null;
		}
		DB::table('expenses')->where('id', $request->expense_id)->update([
	    	'category_id' => $request->category_id,
	    	'amount' => trim($request->amount),
	    	'note' => trim($request->note),
	    	'created_by' => Session::get('admin_id'),
	    	'attachment' => $attachment_path,
	    	'date' => $request->date
	    ]);
	    return redirect()->back()->with('message', 'Expense Updated Successfully');
	}

	public function deleteExpense($id) {
		DB::table('expenses')->where('id', $id)->delete();
		return redirect()->back()->with('message', 'Expense Deleted Successfully');
	}

}