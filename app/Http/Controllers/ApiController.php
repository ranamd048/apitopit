<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use DB;
use Image;

class ApiController extends Controller {

	public function get_app_information(Request $request) {
		$check_key = $this->check_api_key($request->api_key);
		if(!$check_key) {
			return response()->json('Unauthorised', 401);
		}

		$result = DB::table('site_information')->where('id', 1)->first();
		return response()->json(['data' => $result], 200);
	}

	public function get_shop_list(Request $request) {
		$check_key = $this->check_api_key($request->api_key);
		if(!$check_key) {
			return response()->json('Unauthorised', 401);
		}

		$result = DB::table('stores')->where('status', 1)->get();
		return response()->json(['shops' => $result], 200);
	}

	public function get_category_list(Request $request) {
		$check_key = $this->check_api_key($request->api_key);
		if(!$check_key) {
			return response()->json('Unauthorised', 401);
		}

		$result = DB::table('categories')->where('status', 1)->get();
		return response()->json(['categories' => $result], 200);
	}

	public function shop_registration(Request $request) {
		$check_key = $this->check_api_key($request->api_key);
		if(!$check_key) {
			return response()->json('Unauthorised', 401);
		}

		$validator = Validator::make($request->all(), [
            'shop_type' => 'required',
			'store_name' => 'required|string',
			'baner_image' => 'required|image|mimes:jpeg,png,jpg',
			'address' => 'required|string',
			'name' => 'required|string',
			'phone' => 'required|unique:stores|numeric',
			'password' => 'required|min:6|confirmed',
			'password_confirmation' => 'required|min:6'
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

		$slug = preg_replace('/\s+/u', '-', trim($request->store_name));
        $slug = strtolower($slug);
		$check_slug = DB::table('stores')->select('slug')->where('slug', $slug)->first();
		if($check_slug) {
			$slug = $slug.rand(1000, 10000);
		}
	    $destinationPath = public_path('uploads/store/');
	    if ($request->hasFile('baner_image')) {
	        $baner_image = $request->file('baner_image');
	        $baner_image_name = 'baner_image'.Str::random(7).time().'.'.$baner_image->getClientOriginalExtension();
	        $baner_image->move($destinationPath, $baner_image_name);
	    } else {
	    	$baner_image_name = '';
	    }

	    DB::table('stores')->insert([
	    	'store_name' => trim($request->store_name),
	    	'slug' => $slug,
	    	'shop_type' => $request->shop_type,
	    	'baner_image' => $baner_image_name,
	    	'address' => trim($request->address),
	    	'email' => trim($request->email),
	    	'phone' => trim($request->phone),
	    ]);
	    $store_id = DB::getPdo()->lastInsertId();

	    DB::table('admins')->insert([
	    	'name' => trim($request->name),
	    	'user_name' => trim($request->phone),
	    	'email' => trim($request->email),
	    	'phone' => trim($request->phone),
	    	'role' => 1,
	    	'store_id' => $store_id,
	    	'password' => md5(trim($request->password)),
	    ]);

		return response()->json(['success'=>'Shop Registration Successfull'], 201);
	}

	public function save_product(Request $request) {
		$check_key = $this->check_api_key($request->api_key);
		if(!$check_key) {
			return response()->json('Unauthorised', 401);
		}

		$validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'product_name' => 'required',
			'product_image' => 'required|image|mimes:jpeg,png,jpg',
			'price' => 'required',
			'quantity' => 'required|numeric',
			'category_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

		$destinationPath = public_path('uploads/products/');
		$thumbDestinationPath = public_path('uploads/products/thumbnail');
	    if ($request->hasFile('product_image')) {
	        $product_image = $request->file('product_image');
	        $product_image_path = 'product_image'.str_random(10).time().'.'.$product_image->getClientOriginalExtension();

	        $thumbnail_image = Image::make($product_image->getRealPath());
	        $thumbnail_image->resize(220, 220, function($constraint){
		     	$constraint->aspectRatio();
		    })->save($thumbDestinationPath. '/' .$product_image_path);

	        $product_image->move($destinationPath, $product_image_path);
	    } else {
	    	$product_image_path = '';
	    }

	    $product_slug = $this->make_slug($request->product_name);
	    DB::table('products')->insert([
	    	'store_id' => $request->store_id,
	    	'unit_id' => $request->unit_id,
	    	'product_name' => trim($request->product_name),
	    	'product_slug' => $product_slug,
	    	'product_image' => $product_image_path,
	    	'price' => trim($request->price),
	    	'quantity' => trim($request->quantity),
	    	'category_id' => $request->category_id,
	    	'subcategory_id' => $request->subcategory_id,
	    	'product_description' => trim($request->product_description),
	    	'featured' => ($request->featured) ? 1 : 0,
	    	'status' => $request->status,
	    ]);

	    $product_id = DB::getPdo()->lastInsertId();

	    if ($request->hasFile('product_gallery')) {
	    	$destinationPath = public_path('uploads/products/gallery/');
	        $images = $request->file('product_gallery');
	        foreach($images as $image) {
	        	$image_path = 'image_'.$product_id.str_random(10).time().'.'.$image->getClientOriginalExtension();
		        $image->move($destinationPath, $image_path);

		        DB::table('product_gallery')->insert(['product_id' => $product_id, 'image_url' => $image_path]);
	        }
	    }

	    return response()->json(['success'=>'Product Added Successfully'], 201);
	}

	public function save_category(Request $request) {
		$check_key = $this->check_api_key($request->api_key);
		if(!$check_key) {
			return response()->json('Unauthorised', 401);
		}

	    $validator = Validator::make($request->all(), [
            'name' => 'required',
			'image' => 'image|mimes:jpeg,png,jpg',
			'category_type' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

		$destinationPath = public_path('uploads/category/');
	    if ($request->hasFile('image')) {
	        $image = $request->file('image');
	        $image_name = 'category_image'.str_random(5).time().'.'.$image->getClientOriginalExtension();
	        $image->move($destinationPath, $image_name);
	    } else {
	    	$image_name = '';
	    }
	    $slug = $this->make_slug($request->name);

	    DB::table('categories')->insert([
	    	'parent_id' => $request->parent_id,
	    	'name' => trim($request->name),
	    	'slug' => $slug,
	    	'image' => $image_name,
	    	'category_type' => $request->category_type,
	    	'store_id' => $request->store_id,
	    	'status' => $request->status,
	    ]);

	    return response()->json(['success'=>'Category Added Successfully'], 201);
	}

	private function check_api_key($key = null) {
		if(isset($key)) {
			$check_key = DB::table('app_api_keys')->where(['api_key' => $key, 'status' => 1])->first();
			if($check_key) {
				return true;
			}
		}
		return false;
	}

	public function make_slug($string) {
        $string = str_replace('/', ' ', $string);
        $slug = preg_replace('/\s+/u', '-', trim($string));
        return strtolower($slug);
    }

}
