<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CouponController extends Controller
{
    public function addCouponForm() {
        return view('backend.coupon.add');
    }

    public function saveCoupon(Request $request) {
        DB::table('coupons')->insert([
            'cupon_title' => trim($request->cupon_title),
            'cupon_desc' => trim($request->cupon_desc),
            'start_date' => trim($request->start_date),
            'end_date' => trim($request->end_date),
            'cupon_qty' => trim($request->cupon_qty),
            'coupon_price' => trim($request->coupon_price),
            'coupon_slug' => trim($request->coupon_slug),
            'cupon_instruction' => trim($request->cupon_instruction),
            'show_homepage' => trim($request->show_homepage),
            'created_by' => Session::get('admin_id'),
            'status' => trim($request->status),
        ]);

        return redirect('admin/coupon/list')->with('message', 'Coupon Added Successfully');
    }

    public function couponList() {
        $data['coupon_list'] = DB::table('coupons')->orderBy('id', 'DESC')->get();
        return view('backend.coupon.list', $data);
    }

    public function editCoupon($id) {
        $data['result'] = DB::table('coupons')->where('id', $id)->first();
        return view('backend.coupon.edit', $data);
    }

    public function updateCoupon(Request $request) {
        DB::table('coupons')->where('id', $request->coupon_id)->update([
            'cupon_title' => trim($request->cupon_title),
            'cupon_desc' => trim($request->cupon_desc),
            'start_date' => trim($request->start_date),
            'end_date' => trim($request->end_date),
            'cupon_qty' => trim($request->cupon_qty),
            'coupon_price' => trim($request->coupon_price),
            'coupon_slug' => trim($request->coupon_slug),
            'cupon_instruction' => trim($request->cupon_instruction),
            'show_homepage' => trim($request->show_homepage),
            'created_by' => Session::get('admin_id'),
            'status' => trim($request->status),
        ]);

        return redirect('admin/coupon/list')->with('message', 'Coupon Updated Successfully');
    }

    public function couponUserList($coupon_id) {
        $data['user_list'] = DB::table('users')
        ->join('user_coupons', 'users.id', '=', 'user_coupons.user_id')
        ->select('users.name', 'users.phone', 'users.address', 'user_coupons.*')
        ->where('user_coupons.coupon_id', $coupon_id)
        ->get();
        $data['coupon'] = DB::table('coupons')->select('cupon_title')->where('id', $coupon_id)->first();
        return view('backend.coupon.user_list', $data);
    }


}
