<?php

namespace App\Http\Controllers;

use App\Http\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class PosController extends Controller
{
    use HelperTrait;

    public function index()
    {
        if(Session::has('store_id')) {
            $data['shop_id'] = Session::get('store_id');
        } else {
            $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
            $data['shop_id'] = $data['shop_list'][0]->id;
        }
        return view('backend.pos.index', $data);
    }

    public function printOnly() {
        if(!isset($_GET['customer']) || empty($_GET['customer'])) {
            return redirect('admin/pos');
        }
        \Cart::session(Session::get('admin_id'));
        $data['selected_items'] = \Cart::getContent();
        $data['shop'] = DB::table('stores')->where('id', Session::get('store_id'))->first();
        $data['customer'] = DB::table('users')->where('id', $_GET['customer'])->first();
        //echo "<pre>"; print_r($data['selected_items']); exit();
        //return view('backend.pos.print', $data);

        $system_settings = DB::table('system_settings')->select('print_theme')->where('id', 1)->first();
        
        if(Session::has('store_id')) {
            $shop_print = DB::table('stores')->select('print_type')->where('id', Session::get('store_id'))->first();
            if($shop_print->print_type == 'thermal') {
                return view('backend.pos.print.thermal', $data);
            }
        }
        return view('backend.pos.print.'.$system_settings->print_theme, $data);
    }

    public function add_product(Request $request) {
        \Cart::session(Session::get('admin_id'));  
        \Cart::add(array(
            'id' => $request->id,
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'attributes' => array(
                'slug' => $request->slug,
                'image' => $request->image,
                'store_id' => $request->shop,
                'discount' => '',
                'discount_type' => '',
            ) 
        ));
        return 'DONE';
    }

    public function show_selected_items() {
        \Cart::session(Session::get('admin_id'));
        $data['selected_items'] = \Cart::getContent();
        return view('backend.pos.selcted_items', $data);
    }

    public function pos_summary() {
        \Cart::session(Session::get('admin_id'));
        $data['cart_item'] = \Cart::getContent();
        $total_amount = \Cart::getTotal();
        $data['unique_key'] = time();
        $total_product_discount = 0;
        if($data['cart_item']->count() > 0) {
            foreach($data['cart_item'] as $item) {
                if($item->attributes->discount !== '') {
                    $total_product_discount += ($item->attributes->discount * $item->quantity);
                }
            }
        }
        $data['product_discount'] = number_format($total_product_discount, 2);
        $data['total_amount'] = ($total_amount - $total_product_discount);
        return view('backend.pos.pos_summary', $data);
    }

    public function update_quantity(Request $request) {
        \Cart::session(Session::get('admin_id')); 
        if($request->action == 'plus') {
            \Cart::update($request->id, array(
                'quantity' => 1
            ));
        } elseif($request->action == 'minus') {
            \Cart::update($request->id, array(
                'quantity' => -1
            ));
        } else {
            \Cart::update($request->id, array(
                'quantity' => array(
                    'relative' => false,
                    'value' => trim($request->quantity)
                ),
            ));
        }
        return 'DONE';
    }

    public function product_discount(Request $request) {
        $discount_type = $request->discount_type;
        if($discount_type == 'flat') {
            $discount_amount = trim($request->discount);
        } else {
            $discount_amount = ($request->price * trim($request->discount)) / 100;
        }

        \Cart::session(Session::get('admin_id')); 
        $item = \Cart::getContent($request->id);
        \Cart::update($request->id, array(
            'attributes' => array(
                'slug' => $item[$request->id]->attributes->slug,
                'image' => $item[$request->id]->attributes->image,
                'store_id' => $item[$request->id]->attributes->store_id,
                'discount' => $discount_amount,
                'discount_type' => $discount_type
            ),
        ));
        return 'DONE';
    }

    public function search_products(Request $request) {
        $products = DB::table('products')->select('id', 'product_name', 'price', 'product_slug', 'product_image', 'store_id')->where(['store_id' => $request->shop_id])->where('product_name', 'like', '%' . $request->value . '%')->orderBy('id', 'DESC')->get();
        $output = '<ul>';
        if(count($products) > 0) {
            foreach($products as $row) {
                $output .= '<li class="add_to_cart" data-id="'.$row->id.'" data-name="'.$row->product_name.'" data-price="'.$row->price.'" data-slug="'.$row->product_slug.'" data-image="'.$row->product_image.'" data-shop="'.$row->store_id.'">'.$row->product_name.' ('.$row->price.')</li>';
            }
        } else {
            $output .= '<li>Product Not Found</li>';
        }
        $output .= '</ul>';
        return $output;
    }

    public function search_customer(Request $request) {
        $customer_list = DB::table('users')
        ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
        ->select('users.id', 'users.name', 'users.phone')
        ->where('users.name', 'like', '%' . $request->value . '%')
        ->where(['shop_customers.shop_id' => $request->shop_id])
        ->groupBy('shop_customers.user_id')
        ->get();
        $output = '<ul>';
        if(count($customer_list) > 0) {
            foreach($customer_list as $row) {
                $output .= '<li class="add_to_customer" data-id="'.$row->id.'">'.$row->name.' ('.$row->phone.')</li>';
            }
        } else {
            $output .= '<li>Customer Not Found</li>';
        }
        $output .= '</ul>';
        return $output;
    }

    public function place_order(Request $request) {
        if($request->old_order_id > 0) {
            $main_order_id = DB::table('orders_invoice')->select('order_id')->where('id', $request->old_order_id)->first();
            DB::table('orders')->where('id', $main_order_id->order_id)->delete();
            DB::table('orders_invoice')->where('id', $request->old_order_id)->delete();
        }

        \Cart::session(Session::get('admin_id')); 
        $total_amount = \Cart::getTotal();
        $user_id = $request->customer_id;

        DB::table('orders')->insert([
            'user_id' => $user_id,
            'biller_id' => 0,
            'total' => $total_amount,
            'discount' => $request->discount,
            'delivery_charge' => $request->delivery_charge,
            'subtotal' => $request->grand_total,
            'pos' => ($request->old_order_id == 0) ? 1 : 0,
            'shipping_address' => '',
            'order_status' => ($request->old_order_id == 0) ? 0 : 1,
        ]);
        $order_id = DB::getPdo()->lastInsertId();

        $single_shop_products = array();
        $cart_items = \Cart::getContent();
        foreach ($cart_items as $item) {
            $single_shop_products[$item['attributes']['store_id']][] = $item;
        }

        //single shop invoice
        foreach($single_shop_products as $store_id => $items) {
            DB::table('orders_invoice')->insert([
                'order_id' => $order_id,
                'store_id' => $store_id,
                'user_id' => $user_id,
                'delivery_charge' => $request->delivery_charge,
                'discount' => $request->discount,
                'biller_id' => 0,
                'total_amount' => $request->grand_total,
                'paid_amount' => 0,
                'due_amount' => $request->grand_total,
                'payment_status' => 0,
                'invoice_status' => ($request->old_order_id == 0) ? 0 : 1,
                'is_pos' => ($request->old_order_id == 0) ? 1 : 0,
                'customer_comments' => ''
            ]);
            $invoice_id = DB::getPdo()->lastInsertId();
            //save order items
            foreach($items as $row) {
                $this->productStockManage($row['id'], $row['quantity']);
                if($row['attributes']['discount'] == '') {
                    $per_price = $row['price'];
                } else {
                    $per_price = ($row['price'] - $row['attributes']['discount']);
                }
                $sub_total = ($per_price * $row['quantity']);
                DB::table('order_items')->insert([
                    'order_id' => $order_id,
                    'invoice_id' => $invoice_id,
                    'product_id' => $row['id'],
                    'product_name' => $row['name'],
                    'price' => $row['price'],
                    'discount_amount' => $row['attributes']['discount'] == '' ? 0 : $row['attributes']['discount'],
                    'sub_total' => $sub_total,
                    'quantity' => $row['quantity'],
                    'product_slug' => $row['attributes']['slug'],
                ]);
            }
        }
        //payments
        if(!empty($request->payment_amount) && $request->payment_amount > 0) {
            $due_amount = ($request->grand_total - $request->payment_amount);
            DB::table('orders_invoice')->where(['id' => $invoice_id, 'user_id' => $user_id])->update([
                'paid_amount' => $request->payment_amount,
                'due_amount' => $due_amount,
                'payment_status' => ($due_amount == 0) ? 1 : 0
            ]);
            DB::table('payments')->insert([
                'store_id' => Session::get('store_id'),
                'invoice_id' => $invoice_id,
                'user_id' => $user_id,
                'paid_amount' => $request->payment_amount,
                'payment_method' => 'cod',
                'payment_note' => $request->payment_note
            ]);
        }

        \Cart::clear();
        if($request->old_order_id == 0) {
            return 'DONE';
        } else {
            $redirect_link = url('admin/orders');
            return $redirect_link;
        }
    }

    public function save_customer(Request $request) {
        $request->validate([
            'name' => 'required|string',
            'phone' => 'required|numeric|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'gender' => 'required|numeric',
            'address' => 'required|string',
            'upazila' => 'required',
            'district' => 'required',
            'division' => 'required',
        ]);

        DB::table('users')->insert([
            'reference_id' => 0,
            'name' => trim($request->name),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'password' => bcrypt($request->password),
            'gender' => $request->gender,
            'address' => trim($request->address),
            'thana' => $request->upazila,
            'district' => $request->district,
            'division' => $request->division,
        ]);
        $customer_id = DB::getPdo()->lastInsertId();
        //customer pre due amount
        if($request->due_amount > 0) {
            DB::table('customer_shop_balance')->insert([
                'user_id' => $customer_id,
                'shop_id' => $request->store_id,
                'current_balance' => '-'.$request->due_amount,
            ]);
        }
        //registerd shop customer
        $this->shopCustomerRegistration($customer_id, $request->store_id);
        return redirect()->back()->with('message', 'Successfully Done!');
    }

    public function remove($id) {
        \Cart::session(Session::get('admin_id')); 
        \Cart::remove($id);
        return 'DONE';
    }

    public function cancel() {
        \Cart::session(Session::get('admin_id')); 
        \Cart::clear();
        return "DONE";
    }

    public function editOrderPos($id) {
        \Cart::session(Session::get('admin_id')); 
        \Cart::clear();

        $order_items = DB::table('orders_invoice')
            ->join('order_items', 'orders_invoice.id', '=', 'order_items.invoice_id')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->select('orders_invoice.store_id', 'order_items.*', 'products.product_image')
            ->where(['orders_invoice.id' => $id])
            ->get();
        if(count($order_items) > 0) {
            foreach($order_items as $item) {
                \Cart::session(Session::get('admin_id')); 
                \Cart::add(array(
                    'id' => $item->product_id,
                    'name' => $item->product_name,
                    'price' => $item->price,
                    'quantity' => $item->quantity,
                    'attributes' => array(
                        'slug' => $item->product_slug,
                        'image' => $item->product_image,
                        'store_id' => $item->store_id,
                        'discount' => $item->discount_amount,
                        'discount_type' => '',
                    ) 
                ));
            }
        }
        $data['selected_customer'] = DB::table('orders_invoice')
            ->join('users', 'orders_invoice.user_id', '=', 'users.id')
            ->select('orders_invoice.delivery_charge', 'users.id', 'users.name', 'users.phone')
            ->where(['orders_invoice.id' => $id])
            ->first();
        $data['old_order_id'] = $id;
        if(Session::has('store_id')) {
            $data['shop_id'] = Session::get('store_id');
        } else {
            $data['shop_list'] = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereIn('shop_type', ['shop', 'both'])->get();
            $data['shop_id'] = $data['shop_list'][0]->id;
        }
        return view('backend.pos.index', $data);
    }


}