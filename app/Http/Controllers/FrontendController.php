<?php

namespace App\Http\Controllers;

use App\Http\Traits\HelperTrait;
use App\Model\Category;
use App\Model\Product;
use App\Model\Service;
use App\Model\Slider;
use App\Model\Store;
use App\User;
use Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use PDF;

class FrontendController extends Controller
{
    use HelperTrait;


    private function c_image ($path, $new_path, $q = 3) {
        $image_path = public_path($path);
        $img = Image::make($image_path);
        $img->save(public_path($new_path), $q);
        return $img;
    }

    /**
     * index method for showing home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];
        $data['featured_products'] = DB::table('products')
                ->join('stores', 'products.store_id', '=', 'stores.id')
                ->select('products.*', 'stores.store_name', 'stores.slug', 'stores.district', 'stores.show_price', 'stores.show_stock')
                ->where(['products.featured' => 1, 'products.status' => 1, 'stores.status' => 1])
                ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
                ->orderBy('products.id', 'DESC')
                ->take(12)->get();
        $data['new_shop_list'] = Store::select('stores.id', 'stores.store_name', 'stores.slug', 'stores.baner_image', 'stores.logo', 'stores.district', 'districts.name as districts_name')->join('districts','districts.id','=','stores.district')->where(['is_featured' => 0, 'status' => 1])->whereIn('shop_type', ['shop', 'both'])->whereDate('expiry_date', '>=', date('Y-m-d'))->take(12)->get();
        $data['service_list'] = DB::table('services')
        ->join('stores', 'services.store_id', '=', 'stores.id')
        ->select('services.*', 'stores.store_name', 'stores.district')
        ->where(['services.status' => 1, 'stores.status' => 1])
        ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
        ->orderBy('services.id', 'DESC')
        ->take(6)->get();
        $data['sliders'] = Slider::where('status', 1)->orderBy('sort_order', 'ASC')->get();
        $data['shop_list'] = Store::select('id', 'store_name', 'slug', 'baner_image', 'logo', 'district')->where(['is_featured' => 1, 'status' => 1])->whereIn('shop_type', ['shop', 'both', 'service'])->whereDate('expiry_date', '>=', date('Y-m-d'))->take(12)->get();
        $data['shop_categories'] = Category::select('*')->where(['parent_id' => 0, 'category_type' => 'shop', 'status' => 1])->orderBy('sort_order', 'DESC')->get();
        $data['shop_categories_list'] = DB::table('shop_category')->select('*')->orderBy('sort_order', 'DESC')->get();
        $data['service_categories'] = Category::select('*')->where(['parent_id' => 0, 'category_type' => 'service', 'status' => 1])->orderBy('sort_order', 'DESC')->get();
        $data['blog_data'] = DB::table('blogs')->where('type', 'news')->orderBy('id', 'DESC')->take(3)->get();
        $data['advertisements'] = DB::table('shop_advertisements')->where('status', 1)->orderBy('sort_order', 'DESC')->get();
        return $this->viewConstruct('home', $data);
    }

    /**
     * searchResult method for searching shop and product
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchResult(Request $request)
    {
        if (empty($request->search_type) || $request->search_type == 'product') {
            // Product::where(['status' => 1])->where('product_name', 'like', '%' . $request->keyword . '%')->orderBy('id', 'DESC')->paginate(20);
            $data['product_list'] = DB::table('products')
                ->join('stores', 'products.store_id', '=', 'stores.id')
                ->select('products.*','stores.store_name', 'stores.slug', 'stores.district', 'stores.show_price', 'stores.show_stock')
                ->where(['products.status' => 1, 'stores.status' => 1])
                ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
                ->where('products.product_name', 'like', '%' . $request->keyword . '%')
                ->orderBy('products.id', 'DESC')
                ->paginate(20);
            return view('frontend.common.product_list', $data);
        }
        if ($request->search_type == 'shop') {
            $data['result'] = [];
            $data['shop_list'] = Store::select('id', 'store_name', 'slug', 'baner_image', 'logo', 'district')->where('status', 1)->where('store_name', 'like', '%' . $request->keyword . '%')->whereDate('expiry_date', '>=', date('Y-m-d'))->paginate(24);
            return view('frontend.common.shop_list', $data);
        }

        if ($request->search_type == 'service') {
            //Service::where('status', 1)->where('service_name', 'like', '%' . $request->keyword . '%')->paginate(12);
            $data['service_list'] = DB::table('services')
                ->join('stores', 'services.store_id', '=', 'stores.id')
                ->select('services.*', 'stores.store_name', 'stores.slug', 'stores.district')
                ->where(['services.status' => 1, 'stores.status' => 1])
                ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
                ->where('services.service_name', 'like', '%' . $request->keyword . '%')
                ->orderBy('services.id', 'DESC')
                ->paginate(12);
            return view('frontend.common.service_list', $data);
        }

    }

    public function pageDetails($slug)
    {
        $data['page_data'] = DB::table('page')->where('slug', $slug)->first();
        return view('frontend.common.custom_page', $data);
    }

    public function productList()
    {
        $conditions = ['products.status' => 1, 'stores.status' => 1];
        if (isset($_GET['division']) && !empty($_GET['division'])) {
            $conditions['stores.division'] = $_GET['division'];
        }
        if (isset($_GET['district']) && !empty($_GET['district'])) {
            $conditions['stores.district'] = $_GET['district'];
        }
        if (isset($_GET['upazila']) && !empty($_GET['upazila'])) {
            $conditions['stores.thana'] = $_GET['upazila'];
        }

        $data['product_list'] = DB::table('products')
        ->join('stores', 'products.store_id', '=', 'stores.id')
        ->select('products.*','stores.store_name', 'stores.slug', 'stores.district', 'stores.show_price', 'stores.show_stock')
        ->where($conditions)
        ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
        ->orderBy('products.id', 'DESC')
        ->paginate(20);
        return view('frontend.common.product_list', $data);
    }

    public function serviceList()
    {
        $conditions = ['services.status' => 1, 'stores.status' => 1];
        if (isset($_GET['division']) && !empty($_GET['division'])) {
            $conditions['stores.division'] = $_GET['division'];
        }
        if (isset($_GET['district']) && !empty($_GET['district'])) {
            $conditions['stores.district'] = $_GET['district'];
        }
        if (isset($_GET['upazila']) && !empty($_GET['upazila'])) {
            $conditions['stores.thana'] = $_GET['upazila'];
        }

        $data['service_list'] = DB::table('services')
        ->join('stores', 'services.store_id', '=', 'stores.id')
        ->select('services.*', 'stores.store_name', 'stores.district')
        ->where($conditions)
        ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
        ->orderBy('services.id', 'DESC')
        ->paginate(12);
        return view('frontend.common.service_list', $data);
    }

    public function shopCategoryProducts($slug)
    {
        $category = Category::where('slug', $slug)->first();
        if ($category) {
            if ($category->parent_id == 0) {
                $data['parent_category'] = '';
                $data['product_list'] = DB::table('products')
                ->join('stores', 'products.store_id', '=', 'stores.id')
                ->select('products.*' ,'stores.store_name', 'stores.slug', 'stores.district', 'stores.show_price', 'stores.show_stock')
                ->where(['products.category_id' => $category->id, 'products.status' => 1, 'stores.status' => 1])
                ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
                ->orderBy('products.id', 'DESC')
                ->paginate(20);
                //Product::where(['category_id' => $category->id, 'status' => 1])->orderBy('id', 'DESC')->paginate(20);
            } else {
                $data['parent_category'] = Category::select('name', 'slug')->where('id', $category->parent_id)->first();
                $data['product_list'] = DB::table('products')
                ->join('stores', 'products.store_id', '=', 'stores.id')
                ->select('products.*','stores.store_name', 'stores.slug', 'stores.district', 'stores.show_price', 'stores.show_stock')
                ->orWhere(['products.subcategory_id' => $category->id, 'products.sub_subcategory_id' => $category->id])
                ->where(['products.status' => 1, 'stores.status' => 1])
                ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
                ->orderBy('products.id', 'DESC')
                ->paginate(20);
                //Product::orWhere(['subcategory_id' => $category->id, 'sub_subcategory_id' => $category->id])->where('status', 1)->orderBy('id', 'DESC')->paginate(20);
            }
            $data['category'] = $category;
            $data['sub_categories'] = Category::where(['parent_id' => $category->id, 'category_type' => 'shop',
            'store_id' => 0, 'status' => 1])->get();
            return view('frontend.common.shop_category', $data);
        } else {
            return redirect('/');
        }
    }

    public function brandProducts($slug) {
        $brand = DB::table('brands')->where('slug', $slug)->first();
        if ($brand) {
            $data['product_list'] = DB::table('products')
                ->join('stores', 'products.store_id', '=', 'stores.id')
                ->select('products.*', 'stores.store_name', 'stores.slug', 'stores.district', 'stores.show_price', 'stores.show_stock')
                ->where(['products.brand_id' => $brand->id, 'products.status' => 1, 'stores.status' => 1])
                ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
                ->orderBy('products.id', 'DESC')
                ->paginate(20);

            $data['brand'] = $brand;
            return view('frontend.common.brand_product', $data);
        } else {
            return redirect('/');
        }
    }

    public function serviceCategoryItems($slug)
    {
        $category = Category::where('slug', $slug)->first();
        if ($category) {
            if ($category->parent_id == 0) {
                $data['parent_category'] = '';
                $data['service_list'] = DB::table('services')
                ->join('stores', 'services.store_id', '=', 'stores.id')
                ->select('services.*', 'stores.store_name', 'stores.district')
                ->where(['services.category_id' => $category->id, 'services.status' => 1, 'stores.status' => 1])
                ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
                ->orderBy('services.id', 'DESC')
                ->paginate(12);
                //Service::where(['category_id' => $category->id, 'status' => 1])->orderBy('id', 'DESC')->paginate(12);
            } else {
                $data['parent_category'] = Category::select('name', 'slug')->where('id', $category->parent_id)->first();
                $data['service_list'] = DB::table('services')
                ->join('stores', 'services.store_id', '=', 'stores.id')
                ->select('services.*', 'stores.store_name', 'stores.district')
                ->orWhere(['services.subcategory_id' => $category->id, 'services.sub_subcategory_id' => $category->id])
                ->where(['services.status' => 1, 'stores.status' => 1])
                ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
                ->orderBy('services.id', 'DESC')
                ->paginate(12);
                //Service::orWhere(['subcategory_id' => $category->id, 'sub_subcategory_id' => $category->id])->where('status', 1)->orderBy('id', 'DESC')->paginate(12);
            }
            $data['category'] = $category;
            //$data['sub_categories'] = Category::where(['parent_id' => $category->id, 'category_type' => 'service', 'store_id' => 0, 'status' => 1])->get();
            $data['sub_categories'] = Category::where(['parent_id' => $category->id, 'category_type' => 'service', 'status' => 1])->get();
            return view('frontend.common.service_category', $data);
        } else {
            return redirect('/');
        }
    }

    /**
     * productDetails method for showing product details by slug
     *
     * @param  string $slug Product Slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function productDetails($productId, $slug)
    {
        $data['product'] = Product::with('productGalleryItems')->where('id', $productId)->first();
        if (!$data['product']) {
            return redirect('/');
        }
        if($data['product']->sub_subcategory_id) {
            $related_condition = array('products.sub_subcategory_id' => $data['product']->sub_subcategory_id);
        } elseif($data['product']->subcategory_id) {
            $related_condition = array('products.subcategory_id' => $data['product']->subcategory_id);
        } else {
            $related_condition = array('products.category_id' => $data['product']->category_id);
        }
        $data['product_reviews'] = DB::table('product_reviews')
        ->join('users', 'product_reviews.user_id', '=', 'users.id')
        ->select('product_reviews.*', 'users.name', 'users.photo')
        ->where(['product_reviews.product_id' => $productId, 'product_reviews.status' => 1])
        ->orderBy('product_reviews.id', 'DESC')
        ->get();

        $data['related_products'] = DB::table('products')
        ->join('stores', 'products.store_id', '=', 'stores.id')
        ->select('products.*', 'stores.store_name', 'stores.slug', 'stores.district', 'stores.show_price', 'stores.show_stock')
        ->where(['products.status' => 1, 'stores.status' => 1])
        ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
        ->where($related_condition)
        ->where('products.id', '!=', $productId)
        ->orderBy('products.id', 'DESC')
        ->take(4)->get();

        $data['other_products'] = DB::table('products')
        ->join('stores', 'products.store_id', '=', 'stores.id')
        ->select('products.*', 'stores.store_name', 'stores.slug', 'stores.district', 'stores.show_price', 'stores.show_stock')
        ->where(['products.status' => 1, 'stores.id' => $data['product']->store_id, 'stores.status' => 1])
        ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
        ->where($related_condition)
        ->where('products.id', '!=', $productId)
        ->orderBy('products.id', 'DESC')
        ->take(4)->get();

        return view('frontend.common.product_details', $data);
    }

    public function serviceDetails($serviceId, $slug)
    {
        $data['service'] = Service::with('store')->where('id', $serviceId)->first();
        if (!$data['service']) {
            return redirect('/');
        }
        $data['related_services'] = DB::table('services')
        ->join('stores', 'services.store_id', '=', 'stores.id')
        ->select('services.*')
        ->where(['services.category_id' => $data['service']->category_id, 'services.status' => 1, 'stores.status' => 1])
        ->whereDate('stores.expiry_date', '>=', date('Y-m-d'))
        ->where('services.id', '!=', $serviceId)
        ->orderBy('services.id', 'DESC')
        ->take(6)->get();

        //Service::where(['category_id' => $data['service']->category_id, 'status' => 1])->where('id', '!=', $serviceId)->orderBy('id', 'DESC')->take(6)->get();
        return view('frontend.common.service_details', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function shopList()
    {
        if((isset($_GET['division']) && !empty($_GET['division'])) && (isset($_GET['district']) && !empty($_GET['district'])) && (isset($_GET['upazila']) && !empty($_GET['upazila']))) {
            $condition = array('thana' => $_GET['upazila'], 'district' => $_GET['district'], 'division' => $_GET['division'], 'status' => 1);
            $data['result'] = ['thana' => $_GET['upazila'], 'district' => $_GET['district'], 'division' => $_GET['division']];
        }
        elseif((isset($_GET['division']) && !empty($_GET['division'])) && (isset($_GET['district']) && !empty($_GET['district']))) {
            $condition = array('district' => $_GET['district'], 'division' => $_GET['division'], 'status' => 1);
            $data['result'] = ['district' => $_GET['district'], 'division' => $_GET['division']];
        }
        elseif((isset($_GET['division']) && !empty($_GET['division']))) {
            $condition = array('division' => $_GET['division'], 'status' => 1);
            $data['result'] = ['division' => $_GET['division']];
        }
        else {
            $condition = array('status' => 1);
            $data['result'] = [];
        }

        if (isset($_GET['store_name']) && !empty($_GET['store_name'])) {
            $condition['store_name'] = $_GET['store_name'];
            $data['result']['name'] = $_GET['store_name'];
        }

        if (isset($_GET['shop_category_id']) && !empty($_GET['shop_category_id'])) {
            $condition['shop_category_id'] = $_GET['shop_category_id'];
            $data['result']['category'] = $_GET['shop_category_id'];
        }

        if (isset($_GET['shop_type']) && !empty($_GET['shop_type'])) {
            $condition['shop_type'] = $_GET['shop_type'];
            $data['result']['type'] = $_GET['shop_type'];
        }

        $data['shop_list'] = Store::select('id', 'store_name', 'slug', 'baner_image', 'logo', 'district')->where($condition)->whereIn('shop_type', ['shop', 'both', 'service'])->whereDate('expiry_date', '>=', date('Y-m-d'))->paginate(24);
        $data['category_list'] = DB::table('shop_category')->get();


        return view('frontend.common.shop_list', $data);
    }

    /**
     * singleShopDetails method for showing shop details
     *
     * @param int $id Shop Id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function singleShopDetails($slug)
    {
        $data['shop'] = Store::where('slug', $slug)->whereDate('expiry_date', '>=', date('Y-m-d'))->first();
        if (!$data['shop']) {
            return view('errors.404');
        }
        if($data['shop']->shop_type == 'both') {
            $data['shop_categories'] = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->select('categories.id', 'categories.name')
                ->where(['products.store_id' => $data['shop']->id, 'products.status' => 1])
                ->groupBy('products.category_id')
                ->get();
            $data['shop_service_categories'] = DB::table('services')
                ->join('categories', 'services.category_id', '=', 'categories.id')
                ->select('categories.id', 'categories.name')
                ->where(['services.store_id' => $data['shop']->id, 'services.status' => 1])
                ->groupBy('services.category_id')
                ->get();
        }
        if($data['shop']->shop_type == 'shop') {
            $data['shop_categories'] = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->select('categories.id', 'categories.name')
                ->where(['products.store_id' => $data['shop']->id, 'products.status' => 1])
                ->groupBy('products.category_id')
                ->get();
        }
        if($data['shop']->shop_type == 'service') {
            $data['shop_service_categories'] = DB::table('services')
                ->join('categories', 'services.category_id', '=', 'categories.id')
                ->select('categories.id', 'categories.name')
                ->where(['services.store_id' => $data['shop']->id, 'services.status' => 1])
                ->groupBy('services.category_id')
                ->get();
        }
        return view('frontend.common.shop_details', $data);
    }

    public function shopMapView($slug) {
        $data['shop'] = Store::where('slug', $slug)->first();
        if (!$data['shop']) {
            return redirect('/');
        }
        return view('frontend.common.shop_map', $data);
    }

    public function getShopProducts(Request $request)
    {
        if (empty($request->category_id) && empty($request->keyword)) {
            $data['product_list'] = Store::find($request->shop_id)->products;
        } elseif (!empty($request->category_id) && empty($request->keyword)) {
            $category = $request->category;
            $data['product_list'] = Product::where(['store_id' => $request->shop_id, $category => $request->category_id, 'status' => 1])->get();
        } elseif (empty($request->category_id) && !empty($request->keyword)) {
            $data['product_list'] = Product::where(['store_id' => $request->shop_id, 'status' => 1])->where('product_name', 'like', '%' . $request->keyword . '%')->get();
        }

        return view('frontend.common.shop_product', $data);
    }

    public function getShopServices(Request $request)
    {
        if (empty($request->category_id) && empty($request->keyword)) {
            $data['service_list'] = Store::find($request->shop_id)->services;
        } elseif (!empty($request->category_id) && empty($request->keyword)) {
            $category = $request->category;
            $data['service_list'] = Service::where(['store_id' => $request->shop_id, $category => $request->category_id, 'status' => 1])->get();
        } elseif (empty($request->category_id) && !empty($request->keyword)) {
            $data['service_list'] = Service::where(['store_id' => $request->shop_id, 'status' => 1])->where('service_name', 'like', '%' . $request->keyword . '%')->get();
        }

        return view('frontend.common.shop_services', $data);
    }

    public function redirectAdminLoginForm()
    {
        return redirect('admin/login');
    }

    public function allShopMapView() {
        $data['shop_locations'] = DB::table('stores')->select('store_name', 'map_lat', 'map_long')->whereDate('expiry_date', '>=', date('Y-m-d'))->whereNotNull('map_lat')->whereNotNull('map_long')->get();
        if(count($data['shop_locations']) > 0) {
            return view('frontend.common.shops_map_view', $data);
        } else {
            return redirect('/');
        }
    }

    public function loginForm()
    {
        return view('login.login');
    }

    public function shopRegistration()
    {
        $data['divisions'] = DB::table('divisions')->get();
        $data['shop_categories'] = DB::table('shop_category')->orderBy('sort_order', 'ASC')->get();
        return view('frontend.common.store_registration', $data);
    }

    public function customerRegistration()
    {
        $data['divisions'] = DB::table('divisions')->get();
        return view('frontend.common.customer.registration', $data);
    }

    public function getDistrictUpazila(Request $request)
    {
        if ($request->type == 'district_list') {
            $result = DB::table('districts')->where('division_id', $request->id)->get();
        } elseif ($request->type == 'upazila_list') {
            $result = DB::table('upazilas')->where('district_id', $request->id)->get();
        }

        if($request->data_format == 'json') {
            return response()->json($result);
        } else {
            $output = '<option>Select...</option>';
            foreach($result as $row) {
                $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
            }
            return $output;
        }

    }

    public function storeCustomer(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'phone' => 'required|numeric|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'gender' => 'required|numeric',
            'address' => 'required|string',
            'upazila' => 'required',
            'district' => 'required',
            'division' => 'required',
        ]);

        $customer = User::create([
            'reference_id' => $request->reference_id,
            'name' => trim($request->name),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'password' => bcrypt($request->password),
            'gender' => $request->gender,
            'address' => trim($request->address),
            'thana' => $request->upazila,
            'district' => $request->district,
            'division' => $request->division,
        ]);

        return redirect()->back()->with('message', 'Registration Successfully Done!');
    }

    public function checkReferenceId(Request $request)
    {
        $result = DB::table('admins')->select('id')->where('user_name', $request->reference_id)->first();
        if ($result) {
            return $result->id;
        } else {
            return 0;
        }
    }

    public function contactUsPage() {
        return view('frontend.common.contact_us');
    }

    public function sendContactMessage(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string',
            'message' => 'required|string',
        ]);
        if ($validator->fails()) {
            return response()->json(['type' => 'error', 'message' => 'Please fillup all field', 'errors' => $validator->errors()]);
        }
        $data = array(
            'name' => trim($request->name),
            'email' => trim($request->email),
            'comment' => trim($request->message),
            'system_email' => trim($request->system_email)
        );
        Mail::send('emails.contact_message', $data, function($message) use ($data){
            $message->to($data['system_email']);
            $message->subject('Message From Website');
            $message->from($data['email']);
        });

        return response()->json(['type' => 'success', 'message' => 'Message Sent Successfully']);
    }

    public function customerLoginForm()
    {
        if(Session::has('distributor_id') || Session::has('customer_id')) {
            return redirect('product_list');
        }
        return view('frontend.common.customer.login');
    }

    public function customerLoginProcess(Request $request)
    {
        $cart_items = \Cart::getContent();

        $request->validate([
            'phone' => 'required',
            'password' => 'required',
        ]);
        if ($request->login_type == 'customer') {
            if (Auth::attempt(['phone' => request('phone'), 'password' => request('password')])) {
                $user = Auth::user();
                if ($user->status == 0) {
                    return redirect()->back()->with('error', 'Please activate your account');
                }
                Session::put('customer_id', $user->id);

                if ($cart_items->count() > 0) {
                    return redirect('checkout');
                } else {
                    return redirect('product_list');
                }
            } else {
                return redirect()->back()->with('error', 'Phone Number or Password Not match');
            }
        } elseif ($request->login_type == 'sales_representative') {
            $response = $this->systemAdminLoginProcess($request->phone, $request->password);
            if ($response['status'] == true) {
                return redirect('admin/dashboard');
            } else {
                return redirect()->back()->with('error', $response['message']);
            }
        } else {
            $login = DB::table('admins')->where(['user_name' => $request->phone, 'password' => md5($request->password), 'role' => 2])->first();
            if ($login) {
                Session::put('distributor_id', $login->id);
                Session::put('distributor_name', $login->name);

                if ($cart_items->count() > 0) {
                    return redirect('checkout');
                } else {
                    return redirect('customer_profile');
                }
            } else {
                return redirect()->back()->with('error', 'Phone Number or Password Not match');
            }
        }
    }

    public function customerProfile()
    {
        if (Session::has('customer_id')) {
            return view('frontend.common.customer.profile');
        } else {
            $data['distributor'] = DB::table('admins')->where('id', Session::get('distributor_id'))->first();
            return view('frontend.common.customer.distributor_profile', $data);
        }
    }

    public function updateCustomerProfile(Request $request)
    {
        $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg',
        ]);
        $destinationPath = public_path('uploads/customer/');
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photo_path = 'profile_photo' . time() . '.' . $photo->getClientOriginalExtension();
            $photo->move($destinationPath, $photo_path);
        } else {
            $photo_path = $request->existing_photo;
        }
        DB::table('users')->where('id', Auth::user()->id)->update([
            'name' => trim($request->name),
            'email' => trim($request->email),
            'address' => trim($request->address),
            'post_code' => trim($request->post_code),
            'photo' => $photo_path,
            'about_me' => trim($request->about_me),
        ]);

        return redirect()->back()->with('message', 'Updated Successfully');
    }

    public function updateDistributor(Request $request)
    {
        DB::table('admins')->where('id', Session::get('distributor_id'))->update([
            'name' => trim($request->name),
            'email' => trim($request->email),
        ]);

        return redirect()->back()->with('message', 'Updated Successfully');
    }

    public function myWishlistItems()
    {
        $data['wishlist_items'] = DB::table('products')
            ->join('wishlist_items', 'products.id', '=', 'wishlist_items.product_id')
            ->join('stores', 'products.store_id', '=', 'stores.id')
            ->select('products.*', 'wishlist_items.id as wishlist_id', 'stores.store_name', 'stores.district')
            ->where('wishlist_items.user_id', Session::get('customer_id'))
            ->orderBy('wishlist_items.id', 'DESC')
            ->get();

        return view('frontend.common.customer.wishlist_items', $data);
    }

    public function myFavouritesItems()
    {
        $data['favourite_shops'] = DB::table('stores')
        ->join('favourite_items', 'stores.id', '=', 'favourite_items.item_id')
        ->select('favourite_items.*', 'stores.store_name', 'stores.slug', 'stores.logo', 'stores.district')
        ->where(['favourite_items.user_id' => Session::get('customer_id'), 'favourite_items.item_type' => 'shop'])
        ->orderBy('favourite_items.id', 'DESC')
        ->get();
        $data['favourite_products'] = DB::table('products')
            ->join('favourite_items', 'products.id', '=', 'favourite_items.item_id')
            ->join('stores', 'products.store_id', '=', 'stores.id')
            ->select('products.*', 'favourite_items.id as favourite_id', 'stores.store_name', 'stores.district')
            ->where(['favourite_items.user_id' => Session::get('customer_id'), 'favourite_items.item_type' => 'product'])
            ->orderBy('favourite_items.id', 'DESC')
            ->get();
        $data['favourite_services'] = DB::table('services')
            ->join('favourite_items', 'services.id', '=', 'favourite_items.item_id')
            ->select('favourite_items.*', 'services.service_name', 'services.service_slug', 'services.service_image', 'services.price', 'services.service_description', 'services.store_id')
            ->where(['favourite_items.user_id' => Session::get('customer_id'), 'favourite_items.item_type' => 'service'])
            ->orderBy('favourite_items.id', 'DESC')
            ->get();

        return view('frontend.common.customer.favourite_items', $data);
    }

    public function myCouponsItems()
    {
        $data['coupons_items']=DB::table('user_coupons')->where('user_id', Session::get('customer_id'))->get();
        return view('frontend.common.customer.coupons_items', $data);
    }

    public function myOrderList()
    {
        if (Session::has('distributor_id')) {
            $condition = array('biller_id' => Session::get('distributor_id'));
        } else {
            $condition = array('user_id' => Session::get('customer_id'));
        }

        $system_settings = DB::table('system_settings')->where('id', 1)->first();
        if($system_settings->multiple_shop_cart == 0) {
            $table = 'orders_invoice';
            $date_filter = 'created_at';
            $data['ordered_shops'] = DB::table('orders_invoice')
                ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
                ->select('stores.id', 'stores.store_name')
                ->where($condition)
                ->groupBy('orders_invoice.store_id')
                ->get();
            if(isset($_GET['shop_id']) && !empty($_GET['shop_id'])) {
                $customer_balance = DB::table('customer_shop_balance')->where(['user_id' => Session::get('customer_id'), 'shop_id' => $_GET['shop_id']])->first();
                $data['current_balance'] = $customer_balance ? $customer_balance->current_balance : 0;
                $payment_summary = DB::table('orders_invoice')
                    ->select(DB::raw("SUM(orders_invoice.due_amount) as total_due_amount"), DB::raw("SUM(orders_invoice.paid_amount) as total_paid_amount"))
                    ->where(['orders_invoice.store_id' => $_GET['shop_id'], 'orders_invoice.user_id' => Session::get('customer_id')])
                    ->first();
                $data['total_due_amount'] = $payment_summary ? $payment_summary->total_due_amount : 0;
                $data['total_paid_amount'] = $payment_summary ? $payment_summary->total_paid_amount : 0;
            }
        } else {
            $table = 'orders';
            $date_filter = 'order_date';
        }

        if(isset($_GET['start_date']) && isset($_GET['end_date']) && empty($_GET['shop_id'])) {
            $data['order_list'] = DB::table($table)->where($condition)->whereBetween($date_filter, [$_GET['start_date'], $_GET['end_date']])->orderBy('id', 'DESC')->get();
            $data['pagination'] = false;
        }
        else if(isset($_GET['start_date']) && isset($_GET['end_date']) && !empty($_GET['shop_id'])) {
            $data['order_list'] = DB::table($table)->where($condition)->where('orders_invoice.store_id', $_GET['shop_id'])->whereBetween($date_filter, [$_GET['start_date'], $_GET['end_date']])->orderBy('id', 'DESC')->get();
            $data['pagination'] = false;
        } else {
            $data['order_list'] = DB::table($table)->where($condition)->orderBy('id', 'DESC')->paginate(12);
            $data['pagination'] = true;
        }
        return view('frontend.common.customer.'.$table, $data);
    }

    public function myServiceList() {
        if((isset($_GET['start_date']) && !empty($_GET['start_date'])) && (isset($_GET['end_date']) && !empty($_GET['end_date']))) {
            $data['my_services'] = DB::table('service_requests')
            ->join('services', 'service_requests.service_id', '=', 'services.id')
            ->select('service_requests.*', 'services.service_name', 'services.service_slug')
            ->where('service_requests.user_id', Session::get('customer_id'))
            ->whereBetween('service_requests.created_at', [$_GET['start_date'], $_GET['end_date']])
            ->orderByDesc('service_requests.id')
            ->get();
        } else {
            $data['my_services'] = DB::table('service_requests')
            ->join('services', 'service_requests.service_id', '=', 'services.id')
            ->select('service_requests.*', 'services.service_name', 'services.service_slug')
            ->where('service_requests.user_id', Session::get('customer_id'))
            ->orderByDesc('service_requests.id')
            ->get();
        }
        return view('frontend.common.customer.my_services', $data);
    }

    public function orderDetails($id)
    {
        $system_settings = DB::table('system_settings')->where('id', 1)->first();
        if($system_settings->multiple_shop_cart == 0) {
            $data['result'] = DB::table('orders_invoice')
            ->join('users', 'orders_invoice.user_id', '=', 'users.id')
            ->select('orders_invoice.*', 'users.name', 'users.email', 'users.phone')
            ->where('orders_invoice.id', $id)
            ->first();
            return view('frontend.common.customer.invoice_details', $data);
        } else {
            $data['result'] = DB::table('orders')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->select('orders.*', 'users.name', 'users.email', 'users.phone')
            ->where('orders.id', $id)
            ->first();
            return view('frontend.common.customer.order_details', $data);
        }
    }

    public function orderItems($id)
    {
        $system_settings = DB::table('system_settings')->where('id', 1)->first();
        if($system_settings->multiple_shop_cart == 0) {
            $data['order_items'] = DB::table('order_items')
            ->join('orders_invoice', 'order_items.invoice_id', '=', 'orders_invoice.id')
            ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
            ->select('order_items.*', 'stores.store_name', 'stores.slug')
            ->where('order_items.invoice_id', $id)
            ->get();
        } else {
            $data['order_items'] = DB::table('order_items')
            ->join('orders_invoice', 'order_items.invoice_id', '=', 'orders_invoice.id')
            ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
            ->select('order_items.*', 'stores.store_name', 'stores.slug')
            ->where('order_items.order_id', $id)
            ->get();
        }
        return view('frontend.common.customer.order_items', $data);
    }

    public function orderInvoice($id)
    {
        $data['order_list'] = DB::table('orders_invoice')
            ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
            ->select('orders_invoice.*', 'stores.store_name', 'stores.slug')
            ->orderBy('orders_invoice.id', 'DESC')
            ->where('orders_invoice.order_id', $id)
            ->get();
        return view('frontend.common.customer.order_invoice', $data);
    }

    public function orderPrint($id)
    {
        if (isset($_GET['type']) && $_GET['type'] == 'orders_invoice') {
            $data['print_type'] = 'invoice';
            $data['order_details'] = DB::table('orders_invoice')
                ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
                ->select('orders_invoice.*', 'users.id as user_id', 'users.name', 'users.email', 'users.phone', 'users.address', 'stores.store_name', 'stores.logo', 'stores.email', 'stores.phone as store_phone_number', 'stores.tin_no', 'stores.serial_no', 'stores.license_no')
                ->where('orders_invoice.id', $id)
                ->first();
            $data['order_items'] = DB::table('order_items')->where('invoice_id', $id)->get();
        } else {
            $data['print_type'] = 'order';
            $data['order_details'] = DB::table('orders')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->select('orders.*', 'users.id as user_id', 'users.name', 'users.email', 'users.phone', 'users.address')
                ->where('orders.id', $id)
                ->first();
            $data['order_items'] = DB::table('order_items')->where('order_id', $id)->get();
        }
        $system_settings = DB::table('system_settings')->select('print_theme')->where('id', 1)->first();

        if(Session::has('store_id')) {
            $shop_print = DB::table('stores')->select('print_type')->where('id', Session::get('store_id'))->first();
            if($shop_print->print_type == 'thermal') {
                return view('frontend.common.print.thermal', $data);
            }
        }
        return view('frontend.common.print.'.$system_settings->print_theme, $data);
    }

    public function customerLogout()
    {
        Session::flush();
        return redirect('login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function loginProcess(Request $request)
    {
        $email = trim($request->email);
        $password = trim($request->password);

        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $response = $this->systemAdminLoginProcess($email, $password);
        if ($response['status'] == true) {
            return redirect('/admin/dashboard');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function store_registration(Request $request)
    {
        $request->validate([
            'shop_type' => 'required',
            'store_name' => 'required|string',
            'baner_image' => 'required|image|mimes:jpeg,png,jpg',
            'address' => 'required|string',
            'upazila' => 'required|numeric',
            'district' => 'required|numeric',
            'division' => 'required|numeric',
            'name' => 'required|string',
            'phone' => 'required|unique:stores|numeric',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        //check shop phone number
        $is_exists_phone_number = DB::table('stores')->select('id')->where('phone', trim($request->phone))->first();
        if($is_exists_phone_number) {
            return redirect()->back()->with('message', 'Sorry! Phone number already taken');
        }

        $slug = str_replace('/', ' ', $request->store_name);
        $slug = preg_replace('/\s+/u', '-', trim($slug));
        $slug = strtolower($slug);
        $check_slug = DB::table('stores')->select('slug')->where('slug', $slug)->first();
        if ($check_slug) {
            $slug = $slug . rand(1000, 10000);
        }
        $destinationPath = public_path('uploads/store/');
        if ($request->hasFile('baner_image')) {
            $baner_image = $request->file('baner_image');
            $baner_image_name = 'baner_image' . time() . '.' . $baner_image->getClientOriginalExtension();
            $baner_image->move($destinationPath, $baner_image_name);
        } else {
            $baner_image_name = '';
        }
        if ($request->hasFile('shop_logo')) {
            $shop_logo = $request->file('shop_logo');
            $shop_logo_name = 'shop_logo' . time() . '.' . $shop_logo->getClientOriginalExtension();
            $shop_logo->move($destinationPath, $shop_logo_name);
        } else {
            $shop_logo_name = '';
        }
        if(is_array($request->close_day)) {
            $close_days = implode(",",$request->close_day);
        } else {
            $close_days = '';
        }

        DB::table('stores')->insert([
            'shop_category_id' => isset($request->shop_category) ? $request->shop_category : null,
            'store_name' => trim($request->store_name),
            'slug' => $slug,
            'shop_type' => $request->shop_type,
            'baner_image' => $baner_image_name,
            'logo' => $shop_logo_name,
            'address' => trim($request->address),
            'thana' => $request->upazila,
            'district' => $request->district,
            'division' => $request->division,
            'email' => trim($request->email),
            'map_lat' => trim($request->map_lat),
            'map_long' => trim($request->map_long),
            'phone' => trim($request->phone),
            'opening_time' => trim($request->opening_time),
            'close_day' => $close_days,
            'expiry_date' => Carbon::now()->addMonths(1)
        ]);
        $store_id = DB::getPdo()->lastInsertId();

        DB::table('admins')->insert([
            'name' => trim($request->name),
            'user_name' => trim($request->phone),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'role' => 1,
            'store_id' => $store_id,
            'password' => md5(trim($request->password)),
            'thana' => $request->upazila,
            'district' => $request->district,
            'division' => $request->division,
        ]);

        return redirect()->back()->with('message', 'Store Created Successfull');
    }

    public function blogDetails($id)
    {
        $data['blog'] = DB::table('blogs')->where('id', $id)->first();
        $data['latest_blog'] = DB::table('blogs')->orderBy('id', 'DESC')->take(5)->get();
        return view('frontend.common.blog_details', $data);
    }

    public function blogList()
    {
        $data['blog_list'] = DB::table('blogs')->where('type', 'news')->orderBy('id', 'DESC')->paginate(12);
        return view('frontend.common.blog_list', $data);
    }

    public function galleryList()
    {
        $data['type_list'] = DB::table('gallery_types')->get();
        $data['gallery_list'] = DB::table('gallery')->get();
        return view('frontend.common.gallery_list', $data);
    }

    public function cartPage()
    {
        $data['cart_items'] = \Cart::getContent();
        return view('frontend.common.cart', $data);
    }

    public function checkoutPage()
    {
        $cart_items = \Cart::getContent();
        if ($cart_items->count() == 0) {
            return redirect('product_list');
        }
        $data['cart_items'] = $cart_items;
        $data['total_amount'] = \Cart::getTotal();
        return view('frontend.common.checkout', $data);
    }

    public function placeOrder(Request $request)
    {
        $user_id = $request->customer_id;
        $biller_id = ($request->biller_id) ? $request->biller_id : 0;
        $total_amount = $request->total_amount;
        $discount = $request->discount;
        DB::table('orders')->insert([
            'user_id' => $user_id,
            'biller_id' => $biller_id,
            'total' => $total_amount,
            'discount' => $discount,
            'delivery_charge' => $request->delivery_charge,
            'subtotal' => $request->sub_total,
            'shipping_address' => $request->address,
        ]);
        $order_id = DB::getPdo()->lastInsertId();

        $single_shop_products = array();
        if($request->order_type == 'save_order') {
            $save_order_items = DB::table('save_orders')->where('user_id', Session::get('customer_id'))->get();
            foreach ( $save_order_items as $item ) {
                $single_shop_products[$item->store_id][] = $item;
            }
        } else {
            $cart_items = \Cart::getContent();
            foreach ($cart_items as $item) {
                $single_shop_products[$item['attributes']['store_id']][] = $item;
            }
        }

        //single shop invoice
        foreach ($single_shop_products as $store_id => $items) {
            //registerd shop customer
            $this->shopCustomerRegistration($user_id, $store_id);
            //get shop delivery charge
            $delivery_charge = $this->getShopShippingCost($store_id);
            $paid_amount = 0;
            DB::table('orders_invoice')->insert([
                'order_id' => $order_id,
                'store_id' => $store_id,
                'user_id' => $user_id,
                'delivery_charge' => $delivery_charge,
                'discount' => $discount,
                'biller_id' => $biller_id,
                'total_amount' => 0,
                'paid_amount' => $paid_amount,
                'payment_status' => 0,
                'invoice_status' => 0,
                'is_pos' => 0,
                'customer_comments' => trim($request->note),
            ]);
            $invoice_id = DB::getPdo()->lastInsertId();
            //save order items
            $total_invoice_amount = 0;
            foreach ($items as $row) {
                $product_id = ($request->order_type == 'save_order') ? $row->product_id : $row['id'];
                $product_name = ($request->order_type == 'save_order') ? $row->product_name : $row['name'];
                $product_slug = ($request->order_type == 'save_order') ? $row->slug : $row['attributes']['slug'];
                $quantity = ($request->order_type == 'save_order') ? $row->quantity : $row['quantity'];
                if($request->order_type == 'save_order') {
                    $product_price = Helpers::is_promo_or_normal_product($product_id);
                } else {
                    $product_price = $row['price'];
                }
                //manage stock
                $this->productStockManage($product_id, $quantity);
                $sub_total = ($product_price * $quantity);

                DB::table('order_items')->insert([
                    'order_id' => $order_id,
                    'invoice_id' => $invoice_id,
                    'product_id' => $product_id,
                    'product_name' => $product_name,
                    'price' => $product_price,
                    'discount_amount' => 0,
                    'sub_total' => $sub_total,
                    'quantity' => $quantity,
                    'product_slug' => $product_slug,
                ]);
                $total_invoice_amount += $sub_total;
            }
            $total_invoice_amount = ($total_invoice_amount + $delivery_charge);
            $total_invoice_amount = ($total_invoice_amount - $discount);
            //payment
            if($request->payment_method == 'cod') {
                $payment_data = array('invoice_id' => $invoice_id, 'user_id' => $user_id, 'paid_amount' => 0, 'payment_method' => 'cod');
            } else if($request->payment_method == 'bkash') {
                $paid_amount = $total_invoice_amount;
                $payment_data = array('invoice_id' => $invoice_id, 'user_id' => $user_id, 'paid_amount' => $total_invoice_amount, 'payment_method' => 'bkash', 'payment_note' => $request->bkash_payment_note);
            } else {
                $paid_amount = $total_invoice_amount;
                $payment_data = array('invoice_id' => $invoice_id, 'user_id' => $user_id, 'paid_amount' => $total_invoice_amount, 'payment_method' => 'rocket', 'payment_note' => $request->rocket_payment_note);
            }
            DB::table('payments')->insert($payment_data);

            //update invoice total/due amount
            $due_amount = ($total_invoice_amount - $paid_amount);
            DB::table('orders_invoice')->where('id', $invoice_id)->update([
                'total_amount' => $total_invoice_amount,
                'paid_amount' => $paid_amount,
                'due_amount' => $due_amount,
                'payment_status' => ($due_amount == 0) ? 1 : 0,
                'invoice_status' => ($due_amount == 0) ? 1 : 0,
            ]);

            //send mail to shop admin
            //            if(Session::has('customer_id')) {
            //                $this->sendOrderNotificationShopAdmin($store_id, $invoice_id);
            //            }
        }

        //send email
        //$this->sendOrderNotification($order_id, $user_id);

        \Cart::clear();
        Session::forget('multiple_store_warning');
        Session::forget('cart_first_store_id');
        Session::forget('cart_first_product_store_id');
        $redirect_url = url('orders');
        return $redirect_url;
    }

    public function searchCustomer(Request $request)
    {
        $customers = DB::table('users')->select('id', 'name', 'phone')->where(['status' => 1])->where('phone', 'like', '%' . $request->value . '%')->get();
        $output = '';
        if (count($customers) > 0) {
            $output = '<ul>';
            foreach ($customers as $row) {
                $output .= '<li class="customer-single-item" data-id="' . $row->id . '">' . $row->name . ' <span>(' . $row->phone . ')</span></li>';
            }
            $output .= '</ul>';
        } else {
            $output = '<ul><li>Result Not Found</li></ul>';
        }
        return $output;
    }

    public function addToWishlistProcess($id)
    {
        if (Session::has('customer_id')) {
            $check = DB::table('wishlist_items')->where(['user_id' => Session::get('customer_id'), 'product_id' => $id])->first();
            if ($check) {
                return response()->json(['type' => 'error', 'message' => 'Already Added']);
            } else {
                DB::table('wishlist_items')->insert([
                    'user_id' => Session::get('customer_id'),
                    'product_id' => $id,
                ]);
                return response()->json(['type' => 'success', 'message' => 'Successfully Added to Wishlist']);
            }
        } else {
            return response()->json(['type' => 'error', 'message' => 'Please at First Login']);
        }
    }

    public function addToFavourite($id, $type)
    {
        if (Session::has('customer_id')) {
            $check = DB::table('favourite_items')->where(['user_id' => Session::get('customer_id'), 'item_id' => $id, 'item_type' => $type])->first();
            if ($check) {
                return response()->json(['type' => 'error', 'message' => 'Already Added']);
            } else {
                DB::table('favourite_items')->insert([
                    'user_id' => Session::get('customer_id'),
                    'item_id' => $id,
                    'item_type' => $type
                ]);
                return response()->json(['type' => 'success', 'message' => 'Successfully Added']);
            }
        } else {
            return response()->json(['type' => 'error', 'message' => 'Please at First Login']);
        }
    }

    public function isUserLoggedIn() {
        if (Session::has('customer_id')) {
            return response()->json(['type' => 'success']);
        } else {
            return response()->json(['type' => 'error', 'message' => 'Please at First Login']);
        }
    }

    public function deleteWishlistItem($id) {
        DB::table('wishlist_items')->where('id', $id)->delete();
        return response()->json(['type' => 'success', 'message' => 'Wishlist Deleted Successfully']);
    }

    public function deleteFavouriteItem($id) {
        DB::table('favourite_items')->where('id', $id)->delete();
        return response()->json(['type' => 'success', 'message' => 'Deleted Successfully']);
    }

    public function sendServiceRequest(Request $request) {
        if(empty($request->contact_no)) {
            return response()->json(['type' => 'error', 'message' => 'Please Enter Contact Number']);
        }
        DB::table('service_requests')->insert([
            'user_id' => Session::get('customer_id'),
            'service_id' => $request->service_id,
            'why_need' => trim($request->why_need),
            'when_need' => trim($request->when_need),
            'contact_no' => trim($request->contact_no),
            'other_details' => trim($request->other_details)
        ]);
        return response()->json(['type' => 'success', 'message' => 'Request sent Successfully']);
    }

    public function submitProductReview(Request $request) {
        if(empty($request->comment)) {
            return response()->json(['type' => 'error', 'message' => 'Please Enter Your Comment']);
        }
        DB::table('product_reviews')->insert([
            'user_id' => Session::get('customer_id'),
            'product_id' => $request->product_id,
            'rating' => (empty($request->stars)) ? 5 : $request->stars,
            'comment' => trim($request->comment)
        ]);
        return response()->json(['type' => 'success', 'message' => 'Review added Successfully']);
    }

    public function forgotPassword() {
        Session::forget('password_reset_code');
        return view('frontend.common.customer.forgot_password');
    }

    public function checkPhoneNumber(Request $request) {
        $phone_number = trim($request->phone);
        if($request->forgot_for == 'customer') {
            $table = 'users';
        } else {
            $table = 'admins';
        }
        $result = DB::table($table)->select('id')->where('phone', $phone_number)->first();
        if($result) {
            $reset_code = $result->id.rand(10000, 99999);
            $system_settings = DB::table('system_settings')->select('sms_service')->first();
            if($system_settings->sms_service == 1) {
                $this->sendSms(array($phone_number), 'Verification Code: '.$reset_code);
            }
            DB::table($table)->where('id', $result->id)->update([
                'reset_code' => $reset_code
            ]);
            Session::put('password_reset_table', $table);
            return redirect('forgot_password/verify')->with('message', 'Please Check Your Phone. We have sent Verification Code');
        } else {
            return redirect()->back()->with('error', 'Number Not Found');
        }
    }

    public function verifyPhoneNumber() {
        return view('frontend.common.customer.verify');
    }

    public function verificationCode(Request $request) {
        $table = Session::get('password_reset_table');
        $check_code = DB::table($table)->select('reset_code')->where('reset_code', trim($request->code))->first();
        if($check_code) {
            Session::put('password_reset_code', $check_code->reset_code);
            return redirect('forgot_password/reset');
        } else {
            return redirect()->back()->with('error', 'Wrong Code');
        }
    }

    public function resetPasswordForm() {
        if(Session::has('password_reset_code')) {
            return view('frontend.common.customer.reset_password');
        } else {
            return redirect('forgot_password');
        }
    }

    public function resetPasswordStore(Request $request) {
        $new_password = trim($request->new_password);
        $confirm_password = trim($request->confirm_password);
        if($new_password == $confirm_password) {
            if(Session::has('password_reset_code')) {
                $table = Session::get('password_reset_table');
                if($table == 'users') {
                    $password = bcrypt($confirm_password);
                } else {
                    $password = md5($confirm_password);
                }
                DB::table($table)->where('reset_code', Session::get('password_reset_code'))->update([
                    'password' => $password,
                    'reset_code' => null
                ]);
                Session::forget('password_reset_code');
                Session::forget('password_reset_table');
                return redirect('login')->with('message', 'Password Changed Successfully');
            } else {
                return redirect()->back()->with('error', 'Something Went Wrong');
            }
        } else {
            return redirect()->back()->with('error', 'New/Confirm Password Not Match');
        }
    }

    public function saveOrder() {
        DB::table('save_orders')->where('user_id', Session::get('customer_id'))->delete();

        $order_items = \Cart::getContent();
        foreach($order_items as $row) {
            DB::table('save_orders')->insert([
                'user_id' => Session::get('customer_id'),
                'product_id' => $row->id,
                'store_id' => $row->attributes->store_id,
                'product_name' => $row->name,
                'product_name' => $row->name,
                'slug' => $row->attributes->slug,
                'image' => $row->attributes->image,
                'quantity' => $row->quantity
            ]);
        }
        \Cart::clear();
        $redirect_url = url('save_order_items');
        return $redirect_url;
    }

    public function saveOrderItems() {
        $data['save_order_items'] = DB::table('save_orders')->where('user_id', Session::get('customer_id'))->get();
        return view('frontend.common.customer.save_order', $data);
    }

    public function saveOrderConvertPDF() {
        $data = [
            'title' => 'Order Items',
            'heading' => 'Save Order Items',
        ];
        $data['save_order_items'] = DB::table('save_orders')->where('user_id', Session::get('customer_id'))->get();
        $pdf = PDF::loadView('frontend.common.customer.save_order_pdf', $data);
        return $pdf->download();
    }

    public function sendDueSmsAfterSomeTime($shop_id = null) {
        if($shop_id) {
            $customer_due_list = DB::table('users')
            ->join('orders_invoice', 'users.id', '=', 'orders_invoice.user_id')
            ->select('users.id', DB::raw("SUM(orders_invoice.due_amount) as total_due_amount"))
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.payment_status' => 0])
            ->groupBy('orders_invoice.user_id')
            ->get();
            if(count($customer_due_list) > 0) {
                $error = false;
                foreach($customer_due_list as $row) {
                    if(!$this->sendSmsNotification($shop_id, $row->id, $row->total_due_amount)) {
                        $error = true;
                        break;
                    } else {
                        continue;
                    }
                }
                if($error) {
                    return 'Sorry! Your SMS Limit Has Been Finished.';
                } else {
                    return 'SMS Send Successfully to All Customer';
                }
            } else {
                return 'Due Not Found';
            }

        } else {
            return 'Please enter shop id';
        }
    }

    public function sendSmsNotification($shop_id, $user_id, $due_amount) {
        $system_name = DB::table('site_information')->select('web_title', 'email')->where('id', 1)->first();
        $customer = DB::table('users')->select('name', 'email', 'phone')->where('id', $user_id)->first();
        $shop = DB::table('stores')->select('store_name', 'due_sms_text', 'sms_limit')->where('id', $shop_id)->first();
        if($shop->due_sms_text) {
            $message_body = $shop->due_sms_text.' ৳'.number_format($due_amount, 2);
        } else {
            $message_body = 'Hi '.$customer->name.', You have due ৳'.number_format($due_amount, 2). ' in '.$shop->store_name.' shop at '.$system_name->web_title.' platform.';
        }

        if($shop->sms_limit > 0) {
            $this->sendSms(array($customer->phone), $message_body);
            DB::table('stores')->where('id', $shop_id)->update([
                'sms_limit' => ($shop->sms_limit - 1)
            ]);
        }
        if($shop->sms_limit == 0) {
            return false;
        }
        return true;
    }

    public function searchHeaderInput(Request $request) {
        $res = ['NOT FOUND DATA!'];
        $shop_type = $request->type;
        $search_data = $request->search_data;

        if ($shop_type == 'product') {
            $res_data = DB::table('products')->orWhere('product_name', 'like' , "%{$search_data}%")->orderBy('product_name', 'asc')->select('product_name')->get();
            if (count($res_data)) {
                $res = [];
                foreach ($res_data as $item) {
                    array_push($res, $item->product_name);
                }
            }
        } elseif ($shop_type == 'shop') {
            $res_data = DB::table('stores')->orWhere('store_name', 'like' , "%{$search_data}%")->orderBy('store_name', 'asc')->select('store_name')->get();
            $res = [];
            foreach ($res_data as $item) {
                array_push($res, $item->store_name);
            }
        } elseif ($shop_type == 'service') {
            $res_data = DB::table('services')->orWhere('service_name', 'like' , "%{$search_data}%")->orderBy('service_name', 'asc')->select('service_name')->get();
            $res = [];
            foreach ($res_data as $item) {
                array_push($res, $item->service_name);
            }
        }
        echo json_encode($res);
    }






}
