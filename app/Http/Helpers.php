<?php

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class Helpers
{
    public static function is_promo_product($product_id)
    {
        $result = DB::table('products')->select('promotion_price', 'promotion_end_date')->where('promotion_start_date', '<=', Carbon::now())
            ->where('promotion_end_date', '>=', Carbon::now())
            ->where('id', $product_id)
            ->first();
        return $result;
    }

    public static function storePropiterName($store_id) {
        $result = DB::table('admins')->select('name')->where('store_id', $store_id)->first();
        if($result) {
            return $result->name;
        } else {
            return 'Beasy';
        }

    }

    public static function singleStoreInfo($store_id) {
        $result = DB::table('stores')->select('*')->where('id', $store_id)->first();
        if($result) {
            return $result;
        } else {
            return false;
        }

    }

    public static function getShopParentCategory() {
        $result = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'shop', 'status' => 1])->orderBy('sort_order', 'DESC')->get();
        return $result;
    }

    public static function getServiceParentCategory() {
        $result = DB::table('categories')->where(['parent_id' => 0, 'category_type' => 'service', 'status' => 1])->orderBy('sort_order', 'DESC')->get();
        return $result;
    }

    public static function is_promo_or_normal_product($product_id) {
        $promo_product = DB::table('products')->select('promotion_price')->where('promotion_start_date', '<=', Carbon::now())
            ->where('promotion_end_date', '>=', Carbon::now())
            ->where('id', $product_id)
            ->first();
        if($promo_product) {
            $result = $promo_product->promotion_price;
        } else {
            $non_promo = DB::table('products')->select('price')->where('id', $product_id)->first();
            $result = $non_promo->price;
        }
        return $result;
    }

    public static function shop_info($shop_id) {
        $result = DB::table('stores')->select('store_name', 'slug')->where('id', $shop_id)->first();
        return $result;
    }

    public static function shop_shipping_cost($shop_id) {
        $shop = DB::table('stores')->select('district', 'shipping_cost', 'other_shipping_cost')->where('id', $shop_id)->first();
        if(Session::has('customer_id')) {
            $customer_district = DB::table('users')->select('district')->where('id', Session::get('customer_id'))->first();
            if($shop->district != $customer_district->district) {
                return $shop->other_shipping_cost;
            }
        }
        return $shop->shipping_cost;
    }

    public static function get_total_shipping_cost() {
        $cart_total = \Cart::getTotal();
        $cart_items = \Cart::getContent();
        $single_shop_products = array();
        foreach ( $cart_items as $item ) {
            $single_shop_products[$item['attributes']['store_id']][] = $item;
        }
        
        $total_shipping_cost = 0;
        foreach($single_shop_products as $store_id => $items) {
            $shop = DB::table('stores')->select('district', 'min_order_amount', 'shipping_cost', 'other_shipping_cost', 'home_delivery')->where('id', $store_id)->first();

            if($cart_total >= $shop->min_order_amount && $shop->home_delivery != 'No') {
                if(Session::has('customer_id')) {
                    $customer_district = DB::table('users')->select('district')->where('id', Session::get('customer_id'))->first();
                    if($shop->district != $customer_district->district) {
                        $total_shipping_cost += $shop->other_shipping_cost;
                    } else {
                        $total_shipping_cost += $shop->shipping_cost;
                    }
                } else {
                    $total_shipping_cost += $shop->shipping_cost;
                }
            }
        }
        return $total_shipping_cost;
    }

    public static function save_order_shipping_cost($save_order_items) {
        $total_shipping_cost = 0;
        $single_shop_products = array();
        foreach ( $save_order_items as $item ) {
            $single_shop_products[$item->store_id][] = $item;
        }

        foreach($single_shop_products as $store_id => $items) {
            $shop = DB::table('stores')->select('district', 'shipping_cost', 'other_shipping_cost')->where('id', $store_id)->first();
            if(Session::has('customer_id')) {
                $customer_district = DB::table('users')->select('district')->where('id', Session::get('customer_id'))->first();
                if($shop->district != $customer_district->district) {
                    $total_shipping_cost += $shop->other_shipping_cost;
                } else {
                    $total_shipping_cost += $shop->shipping_cost;
                }
            } else {
                $total_shipping_cost += $shop->shipping_cost;
            }
        }
        return $total_shipping_cost;
    }

    public static function billerName($id) {
        if($id == 0) {
            return 'N/A';
        } else {
            $biller = DB::table('admins')->select('name', 'phone')->where('id', $id)->first();
            return $biller->name.'<br>'.$biller->phone;
        }
    }

    public static function getSubCategories($id) {
        $result = DB::table('categories')->where(['parent_id' => $id, 'store_id' => 0, 'status' => 1])->orderBy('sort_order', 'DESC')->get();
        return $result;
    }

    // public static function getSubCategories($id) {
    //     $result = DB::table('categories')->where(['parent_id' => $id, 'status' => 1])->orderBy('sort_order', 'DESC')->get();
    //     return $result;
    // }

    public static function getShopSubCategories($shop_id, $cat_id, $sub_type = 'subcategory_id', $table = 'products') {
        if($sub_type == 'subcategory_id') {
            $parent_category = 'category_id';
        } else {
            $parent_category = 'subcategory_id';
        }
        $result = DB::table($table)
        ->join('categories', $table.'.'.$sub_type, '=', 'categories.id')
        ->select('categories.id', 'categories.name')
        ->where([$table.'.store_id' => $shop_id, $table.'.'.$parent_category => $cat_id, $table.'.status' => 1])
        ->groupBy($table.'.'.$sub_type)
        ->get();
        return $result;
    }

    public static function divisionName($id) {
        if(empty($id)) {
            return 'N/A';
        }
        $result = DB::table('divisions')->select('name')->where('id', $id)->first();
        if($result) {
            return $result->name;
        } else {
            return 'N/A';
        }
    }

    public static function districtName($id) {
        if(empty($id)) {
            return 'N/A';
        }
        $result = DB::table('districts')->select('name')->where('id', $id)->first();
        if($result) {
            return $result->name;
        } else {
            return 'N/A';
        }
    }

    public static function upazilaName($id) {
        if(empty($id)) {
            return 'N/A';
        }
        $result = DB::table('upazilas')->select('name')->where('id', $id)->first();
        if($result) {
            return $result->name;
        } else {
            return 'N/A';
        }
    }

    public static function unitName($unit_id) {
        if(empty($unit_id)) {
            return 'N/A';
        }
        $result = DB::table('product_units')->select('unit_name')->where('id', $unit_id)->first();
        if($result) {
            return $result->unit_name;
        } else {
            return 'N/A';
        }
    }

    public static function productStockBadge($product_id)
    {
        if (empty($product_id)) {
            return false;
        }
        $product = DB::table('products')->select('quantity', 'is_checking_stock', 'sale_quantity')->where('id', $product_id)->first();
        if($product->is_checking_stock == 1) {
            if ($product->quantity > $product->sale_quantity) {
                return '<span class="badge badge-success stock-badge">In Stock</span>';
            } else {
                return '<span class="badge badge-danger stock-badge">Out of Stock</span>';;
            }
        } else {
            return '';
        }
    }

    public static function getCategoryName($id) {
        if(empty($id)) {
            return false;
        }

        $result = DB::table('categories')->select('name')->where('id', $id)->first();
        if($result) {
            return $result->name;
        } else {
            return false;
        }
    }

    public static function getBrandName($id) {
        if(empty($id)) {
            return false;
        }

        $result = DB::table('brands')->select('name')->where('id', $id)->first();
        if($result) {
            return $result->name;
        } else {
            return false;
        }
    }

    public static function getBrandList() {
        $result = DB::table('brands')->where('status', 1)->orderBy('sort_order', 'DESC')->get();
        return $result;
    }

    public static function getDivisionList() {
        $result = DB::table('divisions')->get();
        return $result;
    }
    
    public static function get_current_offer() {
        $result = DB::table('coupons')->where('start_date', '<=', Carbon::now())
        ->where('end_date', '>=', Carbon::now())->first();
        return $result;
    }

    public static function showProductPrice($shop_id) {
        if(empty($shop_id)) {
            return false;
        }
        $result = DB::table('stores')->select('show_price')->where('id', $shop_id)->first();
        if($result->show_price == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function showProductStock($shop_id) {
        if(empty($shop_id)) {
            return false;
        }
        $result = DB::table('stores')->select('show_stock')->where('id', $shop_id)->first();
        if($result->show_stock == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function pendingProductReviewCount() {
        $result = DB::table('product_reviews')->select('id')->where('status', 0)->count();
        if($result > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public static function inactiveProductCount() {
        $result = DB::table('products')->select('id')->where('status', 0)->count();
        if($result > 0) {
            return $result;
        } else {
            return 0;
        }
    }

    public static function inactiveShopCount() {
        $result = DB::table('stores')->select('id')->where('status', 0)->count();
        if($result > 0) {
            return $result;
        } else {
            return 0;
        }
    }

    public static function dateExpireShopCount() {
        $result = DB::table('stores')->select('id')->whereDate('expiry_date', '<=', date('Y-m-d'))->count();
        if($result > 0) {
            return $result;
        } else {
            return 0;
        }
    }

    public static function pendingOrderCount($order_type = 'online') {
        if($order_type == 'online') {
            $is_pos = 0;
        } else {
            $is_pos = 1;
        }

        if (Session::get('role') == 0) {
            $result = DB::table('orders')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->select('orders.id')
                ->where(['orders.order_status' => 0, 'orders.pos' => $is_pos])
                ->count();
        }
        if (Session::get('role') == 1 && Session::get('admin_type') == 1) {
            $result = DB::table('orders_invoice')
                ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                ->select('orders_invoice.id')
                ->where(['orders_invoice.store_id' => Session::get('store_id'), 'orders_invoice.invoice_status' => 0, 'orders_invoice.is_pos' => $is_pos])
                ->count();
        }
        if (Session::get('role') == 1 && Session::get('admin_type') == 0) {
            $result = DB::table('orders_invoice')
                ->join('admins', 'orders_invoice.biller_id', '=', 'admins.id')
                ->join('users', 'orders_invoice.user_id', '=', 'users.id')
                ->select('orders_invoice.id')
                ->where(['orders_invoice.store_id' => Session::get('store_id'), 'admins.parent_id' => Session::get('admin_id'), 'orders_invoice.invoice_status' => 0, 'orders_invoice.is_pos' => $is_pos])
                ->count();
        }
        if($result > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public static function pendingServiceRequestCount() {
        if(Session::has('store_id')) {
            $result = DB::table('service_requests')
                ->join('services', 'service_requests.service_id', '=', 'services.id')
                ->select('service_requests.id')
                ->where(['services.store_id' => Session::get('store_id'), 'service_requests.status' => 0])
                ->count();
        } else {
            $result = DB::table('service_requests')->select('id')->where('status', 0)->count();
        }
        return $result;
    }

    public static function authorityPosition($id) {
        if(empty($id)) {
            return 'N/A';
        }
        $result = DB::table('admin_position')->select('position')->where('id', $id)->first();
        return  $result->position;
    }

    public static function sr_tm_code($user_id) {
        $sr_id = DB::table('users')->where('id', $user_id)->select('reference_id')->first();
        $data['sr_code']  = ($sr_id) ? $sr_id->reference_id : 0;
        if($sr_id) {
            $se_id = DB::table('admins')->where('id',$sr_id->reference_id)->select('parent_id')->first();
            if($se_id) {
                $data['tm_code']  = $se_id->parent_id;
            } else {
                $data['tm_code']  = 0;
            }
        } else {
            $data['tm_code']  = 0;
        }
        return $data;
    }

    public static function monthlyCustomerCount($year, $month) {
        if(!empty($year) && !empty($month)) {
            $result = DB::table('users')
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->count();
        } else {
            $result = DB::table('users')
            ->whereYear('created_at', $year)
            ->count();
        }
        return $result;
    }

    public static function monthlyShopCount($year, $month) {
        if(!empty($year) && !empty($month)) {
            $result = DB::table('stores')
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->count();
        } else {
            $result = DB::table('stores')
            ->whereYear('created_at', $year)
            ->count();
        }
        return $result;
    }

    public static function monthlyOrderCount($year, $month) {
        if(!empty($year) && !empty($month)) {
            $result = DB::table('orders')
            ->whereYear('order_date', $year)
            ->whereMonth('order_date', $month)
            ->count();
        } else {
            $result = DB::table('orders')
            ->whereYear('order_date', $year)
            ->count();
        }
        return $result;
    }

    public static function convertNumberToWord($num = false) {
        $num = str_replace(array(',', ' '), '' , trim($num));
        if(! $num) {
            return false;
        }
        $num = (int) $num;
        $words = array();
        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int) ($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
            $tens = (int) ($num_levels[$i] % 100);
            $singles = '';
            if ( $tens < 20 ) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
            } else {
                $tens = (int)($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        } //end for loop
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }
        return implode(' ', $words);
    }

    public static function smsLimitCount() {
        $result = DB::table('stores')->select('sms_limit')->where('id', Session::get('store_id'))->first();
        return $result->sms_limit;
    }

    public static function getInvoicePaymentList($user_id, $invoice_id) {
        $result = DB::table('payments')->select('id', 'paid_amount', 'payment_method', 'payment_date')->where(['invoice_id' => $invoice_id, 'user_id' => $user_id])->get();
        return $result;
    }

    public static function getProductPriceGroupValue($product_id=null, $group_id=null) {
        if($product_id && $group_id) {
            $result = DB::table('product_group_prices')->select('price')->where(['product_id' => $product_id, 'price_group_id' => $group_id])->first();
            if($result) {
                return $result->price;
            }
        }
        return "";
    }

    public static function store_list() {
        $result = DB::table('stores')->select('id', 'store_name')->where('status', 1)->whereDate('expiry_date', '>=', date('Y-m-d'))->get();
        return $result;
    }

    public static function expense_categories() {
        $store_id = Session::get('store_id');
        $result = DB::table('expense_category')->select('id', 'name')->where('store_id', $store_id)->orderBy('name', 'ASC')->get();
        return $result;
    }

    public static function productStockAlert() {
        $store_id = Session::get('store_id');
        $query = DB::table('products')->select('id', 'product_name', 'product_image', 'quantity', 'alert_quantity', 'sale_quantity')->where(['store_id' => $store_id, 'status' => 1])->get();
        $result = collect($query)->filter(function($value){
            return $value->quantity - $value->sale_quantity <= (int) $value->alert_quantity;
            })->values()->all();
        return $result;
    }

    public static function todaySaleProductDiscount($product_id) {
        $product_discount = DB::table('orders_invoice')
            ->join('order_items', 'orders_invoice.id', '=', 'order_items.invoice_id')
            ->select('order_items.discount_amount', 'order_items.quantity')
            ->where(['orders_invoice.store_id' => Session::get('store_id'), 'order_items.product_id' => $product_id])
            ->whereDate('orders_invoice.created_at', Carbon::today())
            ->get();
        $total_discount = 0;
        if(count($product_discount) > 0) {
            foreach($product_discount as $row) {
                $total_discount += ($row->discount_amount * $row->quantity);
            }
        }
        return $total_discount;
    }

    public static function orderShippingAddress($order_id) {
        $result = DB::table('orders')->select('shipping_address')->where('id', $order_id)->first();
        return $result->shipping_address;
    } 
    
    public static function messageCount() {
        if(Session::has('admin_id')) {
            $sender_id = Session::get('role') == 0 ? Session::get('admin_id') : Session::get('store_id');
            $sender_type = Session::get('role') == 0 ? 'super_admin' : 'shop_admin';
        }
        if(Session::has('customer_id')) {
            $sender_id = Session::get('customer_id');
            $sender_type = 'customer';
        }

        $messaged_users = DB::table('chats')->where(function($query) use ($sender_id, $sender_type) {
            $query->where(['sender_id' => $sender_id, 'sender_type' => $sender_type]);
        })->orWhere(function($query) use ($sender_id, $sender_type) {
            $query->where(['reciver_id' => $sender_id, 'reciver_type' => $sender_type]);
        })->get();

        $message_count = 0;
        if(count($messaged_users) > 0) {
            foreach($messaged_users as $user) {
                $unread_message = DB::table('chat_messages')->select('id')->where(['chat_id' => $user->id, 'is_read' => 0])->where('sender', '!=', $sender_id)->first();
                if($unread_message) {
                    $message_count++;
                }
            }
        }

        return $message_count;
    }



}

?>