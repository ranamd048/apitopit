<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\DB;
use App\Mail\OrderMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

Trait HelperTrait
{
    private $parentIds = [];

    public function makeUniqueSlug($table, $id, $slug_key, $string, $action = 'add')
    {
        $string = str_replace('/', ' ', $string);
        $slug = preg_replace('/\s+/u', '-', trim($string));
        $slug = strtolower($slug);
        $check_slug_value = DB::table($table)->select($slug_key)->where($slug_key, $slug)->first();
        if ($action == 'add') {
            if ($check_slug_value) {
                return $slug . '-' . $id;
            } else {
                return $slug;
            }
        } else {
            $check_slug = DB::table($table)->select($slug_key)->where('id', $id)->first();
            if ($check_slug->$slug_key == $slug) {
                return $slug;
            } else {
                if ($check_slug_value) {
                    return $slug . '-' . $id;
                } else {
                    return $slug;
                }
            }
        }
    }

    public function apiResponse($success = true, $message = null, $data = array(), $status_code = 200)
    {
        if ($success) {
            return response()->json(['success' => $success, 'message' => $message, 'result' => $data], $status_code);
        } else {
            return response()->json(['success' => $success, 'message' => $message, 'error' => $data], $status_code);
        }
    }

    public function getDataFromToken($token, $type="admin") {
        if(empty($token)) {
            return false;
        }
        if($type == 'admin') {
            $token_data = DB::table('admins')->select('id', 'name', 'email', 'phone', 'store_id')->where('api_token', $token)->first();
            if($token_data) {
                return $token_data;
            }
        } else {
            $token_data = DB::table('users')->select('id', 'name', 'email', 'phone', 'gender', 'address')->where('api_token', $token)->first();
            if($token_data) {
                return $token_data;
            }
        }
        return false;
    }

    /**
     * @param $file
     */
    public function viewConstruct($file, $data = null)
    {
        $siteInfo = DB::table('site_information')->select('theme')->where('id', 1)->first();
        $theme = $siteInfo->theme;
        if ($data) {
            return view('frontend.' . $theme . '.' . $file, $data);
        } else {
            return view('frontend.' . $theme . '.' . $file);
        }
    }

    //product stock manage
    public function productStockManage($product_id, $quantity, $action = 'plus')
    {
        if (empty($product_id) || empty($quantity)) {
            return false;
        }

        $product = DB::table('products')->select('is_checking_stock', 'sale_quantity')->where('id', $product_id)->first();
        if ($product->is_checking_stock == 0) {
            return false;
        }
        if ($action == 'plus') {
            $sale_quantity = ($product->sale_quantity + $quantity);
        } else {
            $sale_quantity = ($product->sale_quantity - $quantity);
        }
        DB::table('products')->where('id', $product_id)->update([
            'sale_quantity' => $sale_quantity
        ]);
        return true;
    }

    //product stock availablity
    public function checkProductStock($product_id)
    {
        if (empty($product_id)) {
            return false;
        }
        $product = DB::table('products')->select('quantity', 'sale_quantity')->where('id', $product_id)->first();
        if ($product->quantity > $product->sale_quantity) {
            return true;
        } else {
            return false;
        }
    }

    public function sendOrderNotification($order_id, $user_id) {
        $admin = DB::table('admins')->select('email')->where('role', 0)->first();
        $user = DB::table('users')->select('email', 'reference_id')->where('id', $user_id)->first();

        if(Session::has('distributor_id')) {
            $parent_id = DB::table('admins')->select('parent_id')->where('id', Session::get('distributor_id'))->first();
            if($parent_id) {
                $exucutive = DB::table('admins')->select('email')->where('id', $parent_id->parent_id)->first();
                $emails = array(trim($exucutive->email), trim($admin->email), trim($user->email));
            } else {
                $emails = array(trim($admin->email), trim($user->email));
            }

        } else {
            if($user->reference_id) {
                $parent_id = DB::table('admins')->select('parent_id')->where('id', $user->reference_id)->first();

                $distributor = DB::table('admins')->select('email')->where('id', $user->reference_id)->first();

                if($parent_id) {
                    $exucutive = DB::table('admins')->select('email')->where('id', $parent_id->parent_id)->first();
                    $emails = array(trim($exucutive->email), trim($admin->email), trim($distributor->email));
                } else {
                    $emails = array(trim($admin->email), trim($distributor->email));
                }

            } else {
                $emails = array(trim($admin->email));
            }
        }

        $mail_data['order_type'] = 'orders';
        $mail_data['order_details'] = DB::table('orders')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->select('orders.*', 'users.name', 'users.phone')
            ->where('orders.id', $order_id)
            ->first();
        $mail_data['order_items'] = DB::table('order_items')
            ->join('orders_invoice', 'order_items.invoice_id', '=', 'orders_invoice.id')
            ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
            ->select('order_items.*', 'stores.store_name', 'stores.slug')
            ->where('order_items.order_id', $order_id)
            ->get();

            Mail::to($emails)
            ->send(new OrderMail($mail_data));

        return true;
    }

    public function sendOrderNotificationShopAdmin($shop_id, $invoice_id) {
        $shop = DB::table('stores')->select('email')->where('id', $shop_id)->first();
        $mail_data['order_type'] = 'invoice';
        $mail_data['order_details'] = DB::table('orders_invoice')
            ->join('users', 'orders_invoice.user_id', '=', 'users.id')
            ->select('orders_invoice.*',  'users.name', 'users.phone')
            ->where('orders_invoice.id', $invoice_id)
            ->first();

        $mail_data['order_items'] = DB::table('order_items')
            ->join('orders_invoice', 'order_items.invoice_id', '=', 'orders_invoice.id')
            ->join('stores', 'orders_invoice.store_id', '=', 'stores.id')
            ->select('order_items.*', 'stores.store_name', 'stores.slug')
            ->where('order_items.invoice_id', $invoice_id)
            ->get();

        $to_email = trim($shop->email);

        Mail::to($to_email)
            ->send(new OrderMail($mail_data));

        return true;
    }

    //registerd shop customer
    public function shopCustomerRegistration($user_id, $shop_id) {
        if(empty($user_id) || empty($shop_id)) {
            return false;
        }

        $check_registration = DB::table('shop_customers')->where(['user_id' => $user_id, 'shop_id' => $shop_id])->first();
        if($check_registration) {
            return false;
        } else {
            DB::table('shop_customers')->insert([
                'user_id' => $user_id,
                'shop_id' => $shop_id
            ]);
        }
        return true;
    }

    public function getShopShippingCost($shop_id) {
        if(empty($shop_id)) {
            return 0;
        }

        $shop = DB::table('stores')->select('district', 'shipping_cost', 'other_shipping_cost')->where('id', $shop_id)->first();
        if(Session::has('customer_id')) {
            $customer_district = DB::table('users')->select('district')->where('id', Session::get('customer_id'))->first();
            if($shop->district != $customer_district->district) {
                return $shop->other_shipping_cost;
            }
        }
        return $shop->shipping_cost;
    }

    public function changeParentOrderStatus($id) {
        if(empty($id)) {
            return false;
        }
        $invoice_list = DB::table('orders_invoice')->select('invoice_status')->where('order_id', $id)->get();

        $invoice_status = array();
        foreach($invoice_list as $invoice_item) {
            array_push($invoice_status, $invoice_item->invoice_status);
        }

        $values = array_count_values($invoice_status);
        arsort($values);
        $popular_status = array_slice(array_keys($values), 0, 1, true);

        DB::table('orders')->where('id', $id)->update([
            'order_status' => $popular_status[0],
        ]);
        return true;
    }

    public function systemAdminLoginProcess($username, $password) {
        if(empty($username) || empty($password)) {
            return array('status' => false, 'message' => 'Username and Password Required');
        }

        $login = DB::table('admins')->where(['user_name' => $username, 'password' => md5($password)])->whereIn('role', [0, 1])->first();
        if ($login) {
            Session::put('admin_id', $login->id);
            Session::put('admin_name', $login->name);
            Session::put('role', $login->role);
            Session::put('admin_type', $login->is_main);
            Session::put('store_id', $login->store_id);
            if($login->role == 0) {
                Session::put('is_image_required', 1);
            }
            if ($login->store_id) {
                $store = DB::table('stores')->where('id', $login->store_id)->select('slug', 'shop_type', 'image_required', 'status')->first();
                if ($store->status == 0) {
                    Session::flush();
                    return array('status' => false, 'message' => 'Sorry! Inactive Shop');
                } else {
                    Session::put('store_type', $store->shop_type);
                    Session::put('store_upload_directory', $store->slug);
                    Session::put('is_image_required', $store->image_required);
                }
            }
            return array('status' => true, 'message' => 'Login success');
        } else {
            return array('status' => false, 'message' => 'Sorry! Your Credentials Not Match');
        }
    }

    // public function checkCurrentCampaign() {
    //     $result = DB::table('coupons')->where('start_date', '<=', Carbon::now())
    //         ->where('end_date', '>=', Carbon::now())
    //         ->first();
    //     return $result;
    // }

    public function checkCouponCondition($invoice_id) {
        if(empty($invoice_id)) {
            return false;
        }
        $active_campaign = DB::table('coupons')->where('start_date', '<=', Carbon::now())
        ->where('end_date', '>=', Carbon::now())->first();
        if($active_campaign) {
            $invoice = DB::table('users')
                ->join('orders_invoice', 'users.id', '=', 'orders_invoice.user_id')
                ->select('users.offer_buy_amount', 'orders_invoice.user_id', 'orders_invoice.total_amount')
                ->where('orders_invoice.id', $invoice_id)
                ->first();
            //update user offer_buy_amount
            DB::table('users')->where('id', $invoice->user_id)->update([
                'offer_buy_amount' => ($invoice->offer_buy_amount + $invoice->total_amount)
            ]);
            //current offer_buy_amount
            $current_amount = DB::table('users')->select('offer_buy_amount')->where('id', $invoice->user_id)->first();

            if(($active_campaign->coupon_price <= $current_amount->offer_buy_amount) && ($active_campaign->cupon_qty > $active_campaign->used_qty)) {
                $make_coupon_code = $active_campaign->coupon_slug.$invoice->user_id.rand(10000, 100000);
                DB::table('user_coupons')->insert([
                    'coupon_id' => $active_campaign->id,
                    'user_id' => $invoice->user_id,
                    'coupon_code' => $make_coupon_code,
                    'buy_amount' => $active_campaign->coupon_price,
                ]);
                //update campaign used quantity
                DB::table('coupons')->where('id', $active_campaign->id)->update([
                    'used_qty' => ($active_campaign->used_qty + 1)
                ]);
                //reduce offer_buy_amount
                DB::table('users')->where('id', $invoice->user_id)->update([
                    'offer_buy_amount' => ($current_amount->offer_buy_amount - $active_campaign->coupon_price)
                ]);
            }

        } else {
            return false;
        }
    }

    public function multipleStoreWarning($store_id) {
        if(!Session::has('cart_first_store_id')) {
            Session::put('cart_first_store_id', $store_id);
        }
        // if(!Session::has('cart_second_store_id') && Session::get('cart_first_store_id') != $store_id) {
        //     Session::put('cart_second_store_id', $store_id);
        // }
        if(Session::has('cart_first_store_id')) {
            if(Session::get('cart_first_store_id') != $store_id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function multipleStoreBuyProtect($store_id) {
        if(!Session::has('cart_first_product_store_id')) {
            Session::put('cart_first_product_store_id', $store_id);
        }
        return $store_id;
    }

    public function extractParentid($id) {
        $result = [];
        $values = DB::table('admins')->select('id')->where(['parent_id' => $id, 'role' => 1])->get();
        if(count($values) > 0) {
            foreach($values as $value) {
                $result[$value->id] = $this->extractParentid($value->id);
                $this->parentIds[$value->id] = $value->id;
            }
        }

        return array_values($this->parentIds);
    }

    public function sendSms($to_phone,$text) {
		$aa = array();
		foreach($to_phone as $k=>$val){
		    array_push($aa,array($val,$text));
		}
	    $phone = serialize($aa);
		$token = "14575157";
		//$url = "https://bulksms.myitpal.com/onreceive/sent_data_sms/";
		$url = "https://smsbd.com.bd/onreceive/sent_data_sms/";
		$data= array(
			"to"=>$phone,
			"token"=>$token
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		return $smsresult = curl_exec($ch);
    }

    public function preBalanceCalculate($user_id=null, $from_date, $current_balance=0) {
        if(Session::has('store_id')) {
            $shop_id = Session::get('store_id');
        } else {
            $shop = DB::table('stores')->select('id')->first();
            $shop_id = $shop->id;
        }
        if($user_id) {
            $total_balance = $current_balance;
            $orders = DB::table('orders')
            ->join('orders_invoice', 'orders.id', '=', 'orders_invoice.order_id')
            ->select('orders.subtotal', 'orders_invoice.id as invoice_id')
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.user_id' => $user_id])
            ->whereDate('orders.order_date', '<', $from_date)
            ->get();
            if(count($orders) > 0) {
                $total_debit = 0;
                $total_credit = 0;
                foreach($orders as $order) {
                    $total_debit += $order->subtotal;
                    $total_credit += DB::table('payments')->where(['invoice_id' => $order->invoice_id, 'user_id' => $user_id])->sum('paid_amount');
                }
                $balance = ($total_debit - $total_credit);
                $balance = ($balance + $current_balance);
                $pre_credit = ($total_credit + $current_balance);
                return array('pre_debit' => $total_debit, 'pre_credit' => $pre_credit, 'pre_balance' => $balance);
            }
        }
        return array('pre_debit' => 0, 'pre_credit' => $current_balance, 'pre_balance' => $current_balance);
    }

    public function date_range_summary_report($shop_id, $from_date, $to_date) {
        $prev_date = date('Y-m-d', strtotime($from_date .' -1 day'));

        $sale_number = DB::table('orders_invoice')->select('id')->where('store_id', $shop_id)->whereBetween('created_at', [$from_date, $to_date])->get();

        $sale_product_number = DB::table('orders_invoice')
        ->join('order_items', 'orders_invoice.id', '=', 'order_items.invoice_id')
        ->where('orders_invoice.store_id', $shop_id)
        ->whereBetween('orders_invoice.created_at', [$from_date, $to_date])
        ->sum('order_items.quantity');

        $sale_price = DB::table('orders_invoice')->where('store_id', $shop_id)->whereBetween('created_at', [$from_date, $to_date])->sum('total_amount');

        $sale_paid = DB::table('payments')
        ->join('orders_invoice', 'payments.invoice_id', '=', 'orders_invoice.id')
        ->where('orders_invoice.store_id', $shop_id)
        ->whereBetween('payments.payment_date', [$from_date, $to_date])
        ->sum('payments.paid_amount');

        $sale_due = DB::table('orders_invoice')->where('store_id', $shop_id)->whereBetween('created_at', [$from_date, $to_date])->sum('due_amount');

        $purchase = DB::table('purchases')->select('id')->where('store_id', $shop_id)->whereBetween('created_at', [$from_date, $to_date])->get();

        $purchase_amount = DB::table('purchases')->where('store_id', $shop_id)->whereBetween('created_at', [$from_date, $to_date])->sum('grand_total');

        $purchase_paid_amount = DB::table('purchases')->where('store_id', $shop_id)->whereBetween('created_at', [$from_date, $to_date])->sum('paid_amount');

        $purchase_due_amount = DB::table('purchases')->where('store_id', $shop_id)->whereBetween('created_at', [$from_date, $to_date])->sum('due_amount');

        $purchase_item = DB::table('purchases')
        ->join('purchase_items', 'purchases.id', '=', 'purchase_items.purchase_id')
        ->where('purchases.store_id', $shop_id)
        ->whereBetween('purchases.created_at', [$from_date, $to_date])
        ->sum('purchase_items.quantity');

        $expenses = DB::table('expense_category')
        ->join('expenses', 'expense_category.id', '=', 'expenses.category_id')
        ->where('expense_category.store_id', $shop_id)
        ->whereBetween('expenses.date', [$from_date, $to_date])
        ->sum('amount');

        $customer = DB::table('shop_customers')->select('id')->where('shop_id', $shop_id)->whereBetween('created_at', [$from_date, $to_date])->get();

        $service_request = DB::table('services')
        ->join('service_requests', 'services.id', '=', 'service_requests.service_id')
        ->where('services.store_id', $shop_id)
        ->whereBetween('service_requests.created_at', [$from_date, $to_date])
        ->select('service_requests.id')->get();

        $result = array(
            'opening_balance' => $this->incomeExpensePreBalance($prev_date),
            'number_of_sale' => count($sale_number),
            'sale_product_number' => $sale_product_number,
            'total_sale_price' => $sale_price,
            'number_of_purchase' => count($purchase),
            'total_purchase_amount' => $purchase_amount,
            'purchase_paid_amount' => $purchase_paid_amount,
            'purchase_due_amount' => $purchase_due_amount,
            'number_of_purchase_product' => $purchase_item,
            'total_paid_amount' => $sale_paid,
            'total_due_amount' => $sale_due,
            'total_expense' => $expenses,
            'total_new_customer' => count($customer),
            'total_service_request' => count($service_request)
        );
        return $result;
    }

    public function today_summary_report($shop_id) {
        $prev_date = date('Y-m-d', strtotime(date('Y-m-d') .' -1 day'));

        $sale_number = DB::table('orders_invoice')->select('id')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->get();

        $sale_product_number = DB::table('orders_invoice')
        ->join('order_items', 'orders_invoice.id', '=', 'order_items.invoice_id')
        ->where('orders_invoice.store_id', $shop_id)
        ->whereDate('orders_invoice.created_at', Carbon::today())
        ->sum('order_items.quantity');;

        $sale_price = DB::table('orders_invoice')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->sum('total_amount');

        $sale_paid = DB::table('payments')
        ->join('orders_invoice', 'payments.invoice_id', '=', 'orders_invoice.id')
        ->where('orders_invoice.store_id', $shop_id)
        ->whereDate('payments.payment_date', Carbon::today())
        ->sum('payments.paid_amount');

        $sale_due = DB::table('orders_invoice')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->sum('due_amount');

        $purchase = DB::table('purchases')->select('id')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->get();

        $purchase_amount = DB::table('purchases')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->sum('grand_total');

        $purchase_paid_amount = DB::table('purchases')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->sum('paid_amount');

        $purchase_due_amount = DB::table('purchases')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->sum('due_amount');

        $purchase_item = DB::table('purchases')
        ->join('purchase_items', 'purchases.id', '=', 'purchase_items.purchase_id')
        ->where('purchases.store_id', $shop_id)
        ->whereDate('purchases.created_at', Carbon::today())
        ->sum('purchase_items.quantity');

        $expenses = DB::table('expense_category')
        ->join('expenses', 'expense_category.id', '=', 'expenses.category_id')
        ->where('expense_category.store_id', $shop_id)
        ->whereDate('expenses.date', Carbon::today())
        ->sum('amount');

        $customer = DB::table('shop_customers')->select('id')->where('shop_id', $shop_id)->whereDate('created_at', Carbon::today())->get();

        $service_request = DB::table('services')
        ->join('service_requests', 'services.id', '=', 'service_requests.service_id')
        ->where('services.store_id', $shop_id)
        ->whereDate('service_requests.created_at', Carbon::today())
        ->select('service_requests.id')->get();

        $result = array(
            'opening_balance' => $this->incomeExpensePreBalance($prev_date),
            'sale_product_number' => $sale_product_number,
            'number_of_sale' => count($sale_number),
            'total_sale_price' => $sale_price,
            'number_of_purchase' => count($purchase),
            'total_purchase_amount' => $purchase_amount,
            'purchase_paid_amount' => $purchase_paid_amount,
            'purchase_due_amount' => $purchase_due_amount,
            'number_of_purchase_product' => $purchase_item,
            'total_paid_amount' => $sale_paid,
            'total_due_amount' => $sale_due,
            'total_expense' => $expenses,
            'total_new_customer' => count($customer),
            'total_service_request' => count($service_request)
        );
        return $result;
    }

    public function all_time_shop_report($shop_id) {
        $total_products = DB::table('products')->select('id')->where('store_id', $shop_id)->get();
        $total_cost_price = DB::table('products')->where('store_id', $shop_id)->sum('cost');
        $total_sale_price = DB::table('products')->where('store_id', $shop_id)->sum('price');
        $total_staff = DB::table('shop_staffs')->select('id')->where('store_id', $shop_id)->get();
        $total_customer = DB::table('shop_customers')
        ->join('users', 'shop_customers.user_id', '=', 'users.id')
        ->where('shop_customers.shop_id', $shop_id)
        ->select('users.name')
        ->get();
        $paid_customer = $this->customer_due_paid_summary($shop_id, 1);
        $due_customer = $this->customer_due_paid_summary($shop_id, 0);
        $total_purchase_due = DB::table('purchases')->where('store_id', $shop_id)->sum('due_amount');

        $result = array(
            'total_products' => count($total_products),
            'total_cost_price' => $total_cost_price,
            'total_sale_price' => $total_sale_price,
            'total_profit' => ($total_sale_price - $total_cost_price),
            'total_staff' => count($total_staff),
            'total_customer' => count($total_customer),
            'paid_customer' => $paid_customer,
            'due_customer' => $due_customer,
            'total_purchase_due' => $total_purchase_due,
        );
        return $result;
    }

    private function customer_due_paid_summary($shop_id, $payment_status) {
        $result = DB::table('users')
            ->join('orders_invoice', 'users.id', '=', 'orders_invoice.user_id')
            ->select('users.name', DB::raw("SUM(orders_invoice.due_amount) as total_due_amount"), DB::raw("SUM(orders_invoice.paid_amount) as total_paid_amount"))
            ->where(['orders_invoice.store_id' => $shop_id, 'orders_invoice.payment_status' => $payment_status])
            ->groupBy('orders_invoice.user_id')
            ->get();

        $total_customer = 0;
        $total_amount = 0;
        if($payment_status == 0 && count($result) > 0) {
            $total_customer = count($result);
            foreach($result as $row) {
                $total_amount += $row->total_due_amount;
            }
        }
        if($payment_status == 1 && count($result) > 0) {
            $total_customer = count($result);
            foreach($result as $row) {
                $total_amount += $row->total_paid_amount;
            }
        }
        return array('total_customer' => $total_customer, 'total_amount' => $total_amount);
    }

    public function daily_summary_report($shop_id) {
        $sale_number = DB::table('orders_invoice')->select('id')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->get();
        $sale_price = DB::table('orders_invoice')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->sum('total_amount');
        $sale_paid = DB::table('payments')
        ->join('orders_invoice', 'payments.invoice_id', '=', 'orders_invoice.id')
        ->where('orders_invoice.store_id', $shop_id)
        ->whereDate('payments.payment_date', Carbon::today())
        ->sum('payments.paid_amount');
        $sale_due = DB::table('orders_invoice')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->sum('due_amount');
        $purchase = DB::table('purchases')->select('id')->where('store_id', $shop_id)->whereDate('created_at', Carbon::today())->get();
        $purchase_item = DB::table('purchases')
        ->join('purchase_items', 'purchases.id', '=', 'purchase_items.purchase_id')
        ->where('purchases.store_id', $shop_id)
        ->whereDate('purchases.created_at', Carbon::today())
        ->groupBy('purchase_items.product_id')
        ->select('purchase_items.id')->get();
        $expenses = DB::table('expense_category')
        ->join('expenses', 'expense_category.id', '=', 'expenses.category_id')
        ->where('expense_category.store_id', $shop_id)
        ->whereDate('expenses.date', Carbon::today())
        ->sum('amount');
        $customer = DB::table('shop_customers')->select('id')->where('shop_id', $shop_id)->whereDate('created_at', Carbon::today())->get();
        $service_request = DB::table('services')
        ->join('service_requests', 'services.id', '=', 'service_requests.service_id')
        ->where('services.store_id', $shop_id)
        ->whereDate('service_requests.created_at', Carbon::today())
        ->select('service_requests.id')->get();

        $result = array(
            'number_of_sale' => count($sale_number),
            'total_sale_price' => $sale_price,
            'number_of_purchase' => count($purchase),
            'number_of_purchase_product' => count($purchase_item),
            'total_paid_amount' => $sale_paid,
            'total_due_amount' => $sale_due,
            'total_expense' => $expenses,
            'total_new_customer' => count($customer),
            'total_service_request' => count($service_request)
        );
        return $result;
    }

    public function weekly_summary_report($shop_id) {
        $date = Carbon::now()->subDays(7);

        $sale_number = DB::table('orders_invoice')->select('id')->where('store_id', $shop_id)->where('created_at', '>=', $date)->get();
        $sale_price = DB::table('orders_invoice')->where('store_id', $shop_id)->where('created_at', '>=', $date)->sum('total_amount');
        $sale_paid = DB::table('payments')
        ->join('orders_invoice', 'payments.invoice_id', '=', 'orders_invoice.id')
        ->where('orders_invoice.store_id', $shop_id)
        ->where('payments.payment_date', '>=', $date)
        ->sum('payments.paid_amount');
        $sale_due = DB::table('orders_invoice')->where('store_id', $shop_id)->where('created_at', '>=', $date)->sum('due_amount');
        $purchase = DB::table('purchases')->select('id')->where('store_id', $shop_id)->where('created_at', '>=', $date)->get();
        $purchase_item = DB::table('purchases')
        ->join('purchase_items', 'purchases.id', '=', 'purchase_items.purchase_id')
        ->where('purchases.store_id', $shop_id)
        ->where('purchases.created_at', '>=', $date)
        ->groupBy('purchase_items.product_id')
        ->select('purchase_items.id')->get();
        $expenses = DB::table('expense_category')
        ->join('expenses', 'expense_category.id', '=', 'expenses.category_id')
        ->where('expense_category.store_id', $shop_id)
        ->where('expenses.date', '>=', $date)
        ->sum('amount');
        $customer = DB::table('shop_customers')->select('id')->where('shop_id', $shop_id)->where('created_at', '>=', $date)->get();
        $service_request = DB::table('services')
        ->join('service_requests', 'services.id', '=', 'service_requests.service_id')
        ->where('services.store_id', $shop_id)
        ->where('service_requests.created_at', '>=', $date)
        ->select('service_requests.id')->get();

        $result = array(
            'number_of_sale' => count($sale_number),
            'total_sale_price' => $sale_price,
            'number_of_purchase' => count($purchase),
            'number_of_purchase_product' => count($purchase_item),
            'total_paid_amount' => $sale_paid,
            'total_due_amount' => $sale_due,
            'total_expense' => $expenses,
            'total_new_customer' => count($customer),
            'total_service_request' => count($service_request)
        );
        return $result;
    }

    public function monthly_summary_report($shop_id) {
        $sale_number = DB::table('orders_invoice')->select('id')->where('store_id', $shop_id)->whereMonth('created_at', Carbon::now()->month)->whereYear('created_at', Carbon::now()->year)->get();
        $sale_price = DB::table('orders_invoice')->where('store_id', $shop_id)->whereMonth('created_at', Carbon::now()->month)->whereYear('created_at', Carbon::now()->year)->sum('total_amount');
        $sale_paid = DB::table('payments')
        ->join('orders_invoice', 'payments.invoice_id', '=', 'orders_invoice.id')
        ->where('orders_invoice.store_id', $shop_id)
        ->whereMonth('payments.payment_date', Carbon::now()->month)
        ->whereYear('payments.payment_date', Carbon::now()->year)
        ->sum('payments.paid_amount');
        $sale_due = DB::table('orders_invoice')->where('store_id', $shop_id)->whereMonth('created_at', Carbon::now()->month)->whereYear('created_at', Carbon::now()->year)->sum('due_amount');
        $purchase = DB::table('purchases')->select('id')->where('store_id', $shop_id)->whereMonth('created_at', Carbon::now()->month)->whereYear('created_at', Carbon::now()->year)->get();
        $purchase_item = DB::table('purchases')
        ->join('purchase_items', 'purchases.id', '=', 'purchase_items.purchase_id')
        ->where('purchases.store_id', $shop_id)
        ->whereMonth('purchases.created_at', Carbon::now()->month)
        ->whereYear('purchases.created_at', Carbon::now()->year)
        ->groupBy('purchase_items.product_id')
        ->select('purchase_items.id')->get();
        $expenses = DB::table('expense_category')
        ->join('expenses', 'expense_category.id', '=', 'expenses.category_id')
        ->where('expense_category.store_id', $shop_id)
        ->whereMonth('expenses.date', Carbon::now()->month)
        ->whereYear('expenses.date', Carbon::now()->year)
        ->sum('amount');
        $customer = DB::table('shop_customers')->select('id')->where('shop_id', $shop_id)->whereMonth('created_at', Carbon::now()->month)->whereYear('created_at', Carbon::now()->year)->get();
        $service_request = DB::table('services')
        ->join('service_requests', 'services.id', '=', 'service_requests.service_id')
        ->where('services.store_id', $shop_id)
        ->whereMonth('service_requests.created_at', Carbon::now()->month)
        ->whereYear('service_requests.created_at', Carbon::now()->year)
        ->select('service_requests.id')->get();

        $result = array(
            'number_of_sale' => count($sale_number),
            'total_sale_price' => $sale_price,
            'number_of_purchase' => count($purchase),
            'number_of_purchase_product' => count($purchase_item),
            'total_paid_amount' => $sale_paid,
            'total_due_amount' => $sale_due,
            'total_expense' => $expenses,
            'total_new_customer' => count($customer),
            'total_service_request' => count($service_request)
        );
        return $result;
    }

    public function incomeExpensePreBalance($date) {
        $shop_id = Session::get('store_id');
        $income_report = DB::table('payments')
            ->join('users', 'payments.user_id', '=', 'users.id')
            ->select(DB::raw("SUM(payments.paid_amount) as total_income_amount"))
            ->where(['payments.store_id' => $shop_id])
            ->whereDate('payments.payment_date', $date)
            ->first();
        $expenses = DB::table('expenses')
            ->join('expense_category', 'expenses.category_id', '=', 'expense_category.id')
            ->select(DB::raw("SUM(expenses.amount) as total_expenses_amount"))
            ->where(['expense_category.store_id' => $shop_id])
            ->whereDate('expenses.date', $date)
            ->first();
        $purchase_expenses = DB::table('purchase_payments')
            ->join('users', 'purchase_payments.supplier_id', '=', 'users.id')
            ->select(DB::raw("SUM(purchase_payments.paid_amount) as total_purchase_amount"))
            ->where(['purchase_payments.store_id' => $shop_id])
            ->whereDate('purchase_payments.created_at', $date)
            ->first();
        $total_expense = ($expenses->total_expenses_amount + $purchase_expenses->total_purchase_amount);
        $total_income = $income_report->total_income_amount;
        $pre_balance = ($total_income - $total_expense);
        return $pre_balance;
    }

    public function filterCategoryWithLocation($table, $category_type, $category_id, $location_type, $location_id, $per_page) {
        if($location_type == 'division') {
            $condition = array('stores.division' => $location_id, $table.'.'.$category_type => $category_id, $table.'.status' => 1);
        } else if($location_type == 'district') {
            $condition = array('stores.district' => $location_id, $table.'.'.$category_type => $category_id, $table.'.status' => 1);
        } else {
            $condition = array('stores.thana' => $location_id, $table.'.'.$category_type => $category_id, $table.'.status' => 1);
        }

        if($per_page == 0) {
            $result = DB::table('stores')
            ->join($table, 'stores.id', '=', $table.'.store_id')
            ->where($condition)
            ->select($table.'.*', 'stores.store_name')
            ->get();
            $result = array('data' => $result);
        } else {
            $result = DB::table('stores')
            ->join($table, 'stores.id', '=', $table.'.store_id')
            ->where($condition)
            ->select($table.'.*', 'stores.store_name')
            ->paginate($per_page);
        }

        return $result;
    }



}
