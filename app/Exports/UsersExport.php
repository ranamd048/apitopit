<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $shop_id = Session::get('store_id');
        if($shop_id) {
            return DB::table('users')
            ->join('shop_customers', 'users.id', '=', 'shop_customers.user_id')
            ->select('users.name', 'users.phone', 'users.address')
            ->where(['shop_customers.shop_id' => $shop_id])
            ->groupBy('shop_customers.user_id')
            ->get();
        } else {
            return DB::table('users')->select('name', 'phone', 'address')->get();
        }
    }
}
