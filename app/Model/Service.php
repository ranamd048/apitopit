<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function store() {
        return $this->belongsTo('App\Model\Store');
    }
}
