<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function products() {
        return $this->hasMany('App\Model\Product');
    }

    public function children() {
        return $this->hasMany('App\Model\Category','parent_id');
    }
}
