<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function category() {
        return $this->belongsTo('App\Model\Category');
    }

    public function productGalleryItems() {
        return $this->hasMany('App\Model\ProductGallery');
    }
    
    public function store() {
        return $this->belongsTo('App\Model\Store');
    }
}
