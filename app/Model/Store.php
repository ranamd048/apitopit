<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function products() {
        return $this->hasMany('App\Model\Product')->where('status', 1)->orderBy('id', 'DESC');
    }

    public function services() {
        return $this->hasMany('App\Model\Service')->where('status', 1)->orderBy('id', 'DESC');
    }
}
