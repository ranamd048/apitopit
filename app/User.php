<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */    protected $fillable = [
    'reference_id', 'provider', 'provider_id', 'name', 'email', 'phone', 'password', 'gender', 'address', 'thana', 'district', 'division', 'post_code','photo','balance','about_me','status'
];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
