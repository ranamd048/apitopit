<?php

namespace App\FrontendModel;

use Illuminate\Database\Eloquent\Model;

class SiteInformation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'site_information';
    public $timestamps = false;
}