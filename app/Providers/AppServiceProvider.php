<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $view->with('site_settings', DB::table('site_information')->where('id', 1)->first());
            $view->with('private_settings', DB::table('system_settings')->where('id', 1)->first());
            $view->with('site_menu_items', DB::table('menu')->where(array('status'=>1, 'parent_id'=>0))->orderBy('sort_order', 'ASC')->get());
        });

        Paginator::useBootstrap();
    }
}
