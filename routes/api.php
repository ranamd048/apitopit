<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/login/api/{service}', 'SocialController@redirect');
// Route::get('/login/api/{service}/callback', 'SocialController@callback');


Route::group(['middleware' => 'APISecurityToken'], function () {

    Route::post('app_info', 'App\Http\Controllers\ApiController@get_app_information');
    Route::post('shop_list', 'App\Http\Controllers\ApiController@get_shop_list');
    Route::post('category_list', 'App\Http\Controllers\ApiController@get_category_list');
    Route::post('shop_registration', 'App\Http\Controllers\ApiController@shop_registration');
    Route::post('add_product', 'App\Http\Controllers\ApiController@save_product');
    Route::post('add_category', 'App\Http\Controllers\ApiController@save_category');

    //Common API
    Route::get('similar_items', 'App\Http\Controllers\API\CommonController@getSimilarItems');
    Route::get('divisions', 'App\Http\Controllers\API\CommonController@divisionList');
    Route::get('districts', 'App\Http\Controllers\API\CommonController@districtList');
    Route::get('upazilas', 'App\Http\Controllers\API\CommonController@upazilaList');
    Route::get('district_upazila_by_id/{id}/{type}', 'App\Http\Controllers\API\CommonController@districtUpazilaList');
    Route::get('units', 'App\Http\Controllers\API\CommonController@unitList');
    Route::get('brands', 'App\Http\Controllers\API\CommonController@brandList');
    Route::post('send_sms', 'App\Http\Controllers\API\CommonController@sendMessage');
    Route::post('password_reset', 'App\Http\Controllers\API\CommonController@passwordReset');
    Route::post('submit_otp_phone', 'App\Http\Controllers\API\CommonController@submitOtpPhone');
    Route::post('forgot_password', 'App\Http\Controllers\API\CommonController@forgotPassword');

    //chat api
    Route::get('chat/search_users', 'App\Http\Controllers\API\ChatController@search_users');
    Route::get('chat/previous_message_users', 'App\Http\Controllers\API\ChatController@previous_message_users');
    Route::post('chat/new_message', 'App\Http\Controllers\MessageController@send_new_message');
    Route::get('chat/messages', 'App\Http\Controllers\API\ChatController@messages');

    //Users Module API
    Route::get('users', 'App\Http\Controllers\API\UserController@userList');
    Route::post('social_user', 'App\Http\Controllers\API\UserController@socialRegistration');
    Route::get('shops/{shop_id}/users', 'App\Http\Controllers\API\UserController@getShopUserList');
    Route::post('users/registration', 'App\Http\Controllers\API\UserController@userRegistration');
    Route::post('users/login', 'App\Http\Controllers\API\UserController@userLogin');
    Route::post('users/payment', 'App\Http\Controllers\API\UserController@customerPayment');
    Route::post('users/single_invoice_payment', 'App\Http\Controllers\API\UserController@singleInvoicePayment');
    Route::get('users/payment_history/{user_id}/{shop_id}', 'App\Http\Controllers\API\UserController@customerPaymentHistory');

    //blog api
    Route::get('blogs', 'App\Http\Controllers\API\BlogController@blogList');
    Route::get('blogs/{id}', 'App\Http\Controllers\API\BlogController@blogDetails');
    Route::get('blogs/{id}/comments', 'App\Http\Controllers\API\BlogController@blogComments');
    Route::post('blogs/{id}/create_comment', 'App\Http\Controllers\API\BlogController@saveBlogComment');

    //Authentic Endpoint
    Route::group(['middleware' => 'UserApiToken'], function () {
        Route::get('users/profile', 'App\Http\Controllers\API\UserController@userProfile');
        Route::get('users/logout', 'App\Http\Controllers\API\UserController@userLogout');
        //Route::put('users/update', 'App\Http\Controllers\API\UserController@userProfileUpdate');
        Route::get('users/orders_invoices', 'App\Http\Controllers\API\UserController@getOrderInvoices');

        Route::get('users/shop_orders', 'App\Http\Controllers\API\UserController@getUserShopOrderList');

        Route::get('users/shop_invoices/{shop_id}', 'App\Http\Controllers\API\UserController@getUserShopInvoiceList');

        Route::get('users/invoice_items/{invoice_id}', 'App\Http\Controllers\API\UserController@getInvoiceItems');

        Route::post('users/save_service_request', 'App\Http\Controllers\API\UserController@saveServiceRequest');
    });

    //authenticate shop admin endpoint
    Route::group(array('middleware' => 'APIToken'), function () {
        Route::get('shops/check', 'App\Http\Controllers\API\ShopController@shopCheck');
        Route::get('shops/logout', 'App\Http\Controllers\API\ShopController@shopLogoutProcess');
    });

    //Product Module API
    Route::get('products', 'App\Http\Controllers\API\ProductController@getAllProductList');
    Route::get('shops_products/{shop_id}', 'App\Http\Controllers\API\ProductController@getShopProductList');
    Route::get('products/{product_id}', 'App\Http\Controllers\API\ProductController@getProductDetails');
    Route::post('products', 'App\Http\Controllers\API\ProductController@saveProduct');
    Route::put('products/{product_id}', 'App\Http\Controllers\API\ProductController@updateProduct');
    Route::delete('products/{product_id}', 'App\Http\Controllers\API\ProductController@deleteProduct');

    //Category Module API
    Route::get('categories', 'App\Http\Controllers\API\CategoryController@getAllCategoryList');
    Route::get('categories/{id}', 'App\Http\Controllers\API\CategoryController@getCategoryDetails');
    Route::post('categories', 'App\Http\Controllers\API\CategoryController@saveCategory');
    Route::put('categories/{id}', 'App\Http\Controllers\API\CategoryController@updateCategory');
    Route::delete('categories/{id}', 'App\Http\Controllers\API\CategoryController@deleteCategory');
    //Route::get('shop_categories/{shop_id}', 'App\Http\Controllers\API\CategoryController@getShopCategories');
    //Route::get('shop_sub_categories/{shop_id}/{parent_id}/{field_name}', 'App\Http\Controllers\API\CategoryController@getShopSubCategories');
    Route::get('shop_categories', 'App\Http\Controllers\API\CategoryController@getShopCategories');
    Route::get('shop_category_list', 'App\Http\Controllers\API\CategoryController@shopCategoryList');

    //Service Module API
    Route::get('services', 'App\Http\Controllers\API\ServiceController@getAllServiceList');
    Route::get('shops_services/{shop_id}', 'App\Http\Controllers\API\ServiceController@getShopServiceList');
    Route::get('services/{id}', 'App\Http\Controllers\API\ServiceController@getServiceDetails');
    Route::post('services', 'App\Http\Controllers\API\ServiceController@saveService');
    Route::put('services/{id}', 'App\Http\Controllers\API\ServiceController@updateService');
    Route::delete('services/{id}', 'App\Http\Controllers\API\ServiceController@deleteService');
    //service request API
    Route::get('service_requests', 'App\Http\Controllers\API\ServiceController@getAllServiceRequest');
    Route::get('service_requests/{id}', 'App\Http\Controllers\API\ServiceController@serviceRequestDeails');
    Route::put('update_service_requests/{id}', 'App\Http\Controllers\API\ServiceController@updateServiceRequestStatus');
    //Shop Module API
    Route::get('shops', 'App\Http\Controllers\API\ShopController@getAllShopList');
    Route::get('shops/{id}', 'App\Http\Controllers\API\ShopController@getShopDetails');
    Route::post('shops', 'App\Http\Controllers\API\ShopController@saveShop');
    Route::post('shops/registration', 'App\Http\Controllers\API\ShopController@shopRegistration');
    Route::post('shops/login', 'App\Http\Controllers\API\ShopController@shopLoginProcess');
    Route::put('shops/{id}', 'App\Http\Controllers\API\ShopController@updateShop');
    Route::delete('shops/{id}', 'App\Http\Controllers\API\ShopController@deleteShop');
    Route::get('shop_overview_report/{shop_id}', 'App\Http\Controllers\API\ShopController@shopOverviewReport');
    //Orders Module API
    Route::get('orders', 'App\Http\Controllers\API\OrdersController@getAllOrderList');
    Route::get('shops/{shop_id}/orders', 'App\Http\Controllers\API\OrdersController@getShopOrderList');
    Route::get('users/{user_id}/orders', 'App\Http\Controllers\API\OrdersController@getUserOrderList');
    Route::get('orders/{id}', 'App\Http\Controllers\API\OrdersController@getOrderDetails');
    Route::get('order_invoice/{id}', 'App\Http\Controllers\API\OrdersController@orderInvoice');
    Route::post('orders', 'App\Http\Controllers\API\OrdersController@saveOrder');
    Route::put('orders/{id}', 'App\Http\Controllers\API\OrdersController@updateOrder');
    Route::delete('orders/{id}', 'App\Http\Controllers\API\OrdersController@deleteOrder');
    //Expenses Module API
    Route::get('expense_categories', 'App\Http\Controllers\API\ExpenseController@getExpenseCategories');
    Route::post('expense_categories', 'App\Http\Controllers\API\ExpenseController@storeExpenseCategories');
    Route::get('expenses', 'App\Http\Controllers\API\ExpenseController@getExpenses');
    Route::post('expenses', 'App\Http\Controllers\API\ExpenseController@storeExpenses');
    //Supplier Module API
    Route::get('suppliers', 'App\Http\Controllers\API\SupplierController@index');
    Route::get('supplier/{id}', 'App\Http\Controllers\API\SupplierController@details');
    Route::post('supplier', 'App\Http\Controllers\API\SupplierController@store');
    Route::put('supplier/{id}', 'App\Http\Controllers\API\SupplierController@update');
    Route::delete('supplier/{id}', 'App\Http\Controllers\API\SupplierController@delete');
    //Purchase Module API
    Route::get('purchases', 'App\Http\Controllers\API\PurchaseController@index');
    Route::get('purchase/{id}', 'App\Http\Controllers\API\PurchaseController@details');
    Route::post('purchase', 'App\Http\Controllers\API\PurchaseController@store');
    Route::delete('purchase/{id}', 'App\Http\Controllers\API\PurchaseController@delete');
    //customer sales
    Route::get('shop_customer_orders/{shop_id}/{user_id}', 'App\Http\Controllers\API\OrdersController@shopCustomerOrders');
});
